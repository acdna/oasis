import React from 'react';
import Nav0 from "@/pages/Home/Nav0";
import {Nav00DataSource} from "@/pages/Home/data.source";
import {ConnectProps} from "@@/plugin-dva/connect";
import '@/pages/Home/less/antMotionStyle.less';

export interface BlankLayoutProps extends Partial<ConnectProps> {

}

const BlankLayout: React.FC<BlankLayoutProps> = (props) => {
  return (
    <div>
      <Nav0
        id="Nav0_0"
        key="Nav0_0"
        dataSource={Nav00DataSource}
      />
      {/*<div style={{marginTop: 60}}>*/}
      {props.children}
      {/*</div>*/}


      {/*<Footer1*/}
      {/*  style={{*/}
      {/*    position: 'fixed',*/}
      {/*    bottom: 0,*/}
      {/*    zIndex: 100,*/}
      {/*  }}*/}
      {/*  id="Footer1_1"*/}
      {/*  key="Footer1_1"*/}
      {/*  dataSource={Footer11DataSource}*/}
      {/*/>*/}
    </div>
  );
}

export default BlankLayout;
