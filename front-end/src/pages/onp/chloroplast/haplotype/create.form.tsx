import { OnpChloroplastHaplotypeItem } from "@/pages/onp/chloroplast/haplotype/data";
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_CHLOROPLAST_HAPLOTYPE-创建记录表单的输入参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
interface OnpChloroplastHaplotypeCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpChloroplastHaplotypeItem) => void;
}

/**
 * ONP_CHLOROPLAST_HAPLOTYPE-创建记录表单
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
const OnpChloroplastHaplotypeCreateForm: React.FC<OnpChloroplastHaplotypeCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:37 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { ochId,ochChloroplastHaplotype,ochChr,ochChloroplastOnp,ochChloroplastHaplotypeSequence,ochFrequency,ochColumn1,ochColumn2,ochColumn3,ochOrder,ochSpecies,ochRemark,ochCreateDate,ochUpdateDate }=fieldValues;
    onSubmit({ ochId,ochChloroplastHaplotype,ochChr,ochChloroplastOnp,ochChloroplastHaplotypeSequence,ochFrequency,ochColumn1,ochColumn2,ochColumn3,ochOrder,ochSpecies,ochRemark,ochCreateDate,ochUpdateDate });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:37 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.create.onp.chloroplast.haplotype.form.title', defaultMessage: '创建' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.create.onp.chloroplast.haplotype.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.onp.chloroplast.haplotype.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ochChloroplastHaplotype"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotype.label',
                  defaultMessage: 'Chloroplast',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotype.tip',
                        defaultMessage: 'Chloroplast是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotype.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotype.placeholder',
                  defaultMessage: 'Chloroplast',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ochChr"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChr.label',
                  defaultMessage: 'Chr',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochChr.tip',
                        defaultMessage: 'Chr是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochChr.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChr.placeholder',
                  defaultMessage: 'Chr',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ochChloroplastOnp"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastOnp.label',
                  defaultMessage: 'Chloroplast',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastOnp.tip',
                        defaultMessage: 'Chloroplast是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastOnp.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastOnp.placeholder',
                  defaultMessage: 'Chloroplast',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ochChloroplastHaplotypeSequence"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.label',
                  defaultMessage: 'Chloroplast',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.tip',
                        defaultMessage: 'Chloroplast是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.placeholder',
                  defaultMessage: 'Chloroplast',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ochFrequency"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochFrequency.label',
                  defaultMessage: 'Frequency',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochFrequency.tip',
                        defaultMessage: 'Frequency是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochFrequency.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochFrequency.placeholder',
                  defaultMessage: 'Frequency',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ochColumn1"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochColumn1.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochColumn1.tip',
                        defaultMessage: 'Column1是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochColumn1.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochColumn1.placeholder',
                  defaultMessage: 'Column1',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ochColumn2"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochColumn2.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochColumn2.tip',
                        defaultMessage: 'Column2是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochColumn2.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochColumn2.placeholder',
                  defaultMessage: 'Column2',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ochColumn3"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochColumn3.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochColumn3.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochColumn3.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochColumn3.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ochOrder"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochOrder.label',
                  defaultMessage: 'ID',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochOrder.tip',
                        defaultMessage: 'ID是必填项!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochOrder.placeholder',
                  defaultMessage: 'ID',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ochSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochSpecies.label',
                  defaultMessage: 'Species',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochSpecies.tip',
                        defaultMessage: 'Species是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochSpecies.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochSpecies.placeholder',
                  defaultMessage: 'Species',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ochRemark"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochRemark.label',
                  defaultMessage: 'Remark',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.haplotype.ochRemark.tip',
                        defaultMessage: 'Remark是必填项!',
                      })),
                    },
                    {
                      max:128,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.haplotype.ochRemark.length.tip',
                         defaultMessage: '最多可输入128个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.haplotype.ochRemark.placeholder',
                  defaultMessage: 'Remark',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpChloroplastHaplotypeCreateForm;
