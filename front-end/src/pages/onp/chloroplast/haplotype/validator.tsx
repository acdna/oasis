import { OnpChloroplastHaplotypeService } from '@/services/onp/onp.chloroplast.haplotype.service';
import { RuleObject, StoreValue } from "rc-field-form/lib/interface";
import { OnpChloroplastHaplotypeParams } from "@/pages/onp/chloroplast/haplotype/data";
/**
 * ONP_CHLOROPLAST_HAPLOTYPE记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const ONP_CHLOROPLAST_HAPLOTYPE_FINDER = {
    /**
     * Chloroplast-字段唯一性验证
     * @param ochChloroplastHaplotype 输入框输入的Chloroplast
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHLOROPLAST_HAPLOTYPE: async (ochChloroplastHaplotype: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochChloroplastHaplotype,
        });
        return (res && res.ochChloroplastHaplotype == ochChloroplastHaplotype);
    },
    /**
     * Chr-字段唯一性验证
     * @param ochChr 输入框输入的Chr
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHR: async (ochChr: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochChr,
        });
        return (res && res.ochChr == ochChr);
    },
    /**
     * Chloroplast-字段唯一性验证
     * @param ochChloroplastOnp 输入框输入的Chloroplast
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHLOROPLAST_ONP: async (ochChloroplastOnp: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochChloroplastOnp,
        });
        return (res && res.ochChloroplastOnp == ochChloroplastOnp);
    },
    /**
     * Chloroplast-字段唯一性验证
     * @param ochChloroplastHaplotypeSequence 输入框输入的Chloroplast
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE: async (ochChloroplastHaplotypeSequence: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochChloroplastHaplotypeSequence,
        });
        return (res && res.ochChloroplastHaplotypeSequence == ochChloroplastHaplotypeSequence);
    },
    /**
     * Frequency-字段唯一性验证
     * @param ochFrequency 输入框输入的Frequency
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_FREQUENCY: async (ochFrequency: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochFrequency,
        });
        return (res && res.ochFrequency == ochFrequency);
    },
    /**
     * Column1-字段唯一性验证
     * @param ochColumn1 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_COLUMN1: async (ochColumn1: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochColumn1,
        });
        return (res && res.ochColumn1 == ochColumn1);
    },
    /**
     * Column2-字段唯一性验证
     * @param ochColumn2 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_COLUMN2: async (ochColumn2: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochColumn2,
        });
        return (res && res.ochColumn2 == ochColumn2);
    },
    /**
     * Column3-字段唯一性验证
     * @param ochColumn3 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_COLUMN3: async (ochColumn3: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochColumn3,
        });
        return (res && res.ochColumn3 == ochColumn3);
    },
    /**
     * Species-字段唯一性验证
     * @param ochSpecies 输入框输入的Species
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_SPECIES: async (ochSpecies: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochSpecies,
        });
        return (res && res.ochSpecies == ochSpecies);
    },
    /**
     * Remark-字段唯一性验证
     * @param ochRemark 输入框输入的Remark
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_REMARK: async (ochRemark: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        const res = await OnpChloroplastHaplotypeService.findOne({
            ...params,
            ochRemark,
        });
        return (res && res.ochRemark == ochRemark);
    },
};
/**
 * ONP_CHLOROPLAST_HAPLOTYPE验证器
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const ONP_CHLOROPLAST_HAPLOTYPE_VALIDATOR = {
    /**
     * Chloroplast-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHLOROPLAST_HAPLOTYPE: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_CHLOROPLAST_HAPLOTYPE(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Chr-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHR: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_CHR(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Chloroplast-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHLOROPLAST_ONP: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_CHLOROPLAST_ONP(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Chloroplast-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Frequency-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_FREQUENCY: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_FREQUENCY(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_COLUMN1: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_COLUMN1(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_COLUMN2: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_COLUMN2(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_COLUMN3: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_COLUMN3(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Species-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_SPECIES: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_SPECIES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Remark-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:37 PM
     **/
    OCH_REMARK: (error: string, params: Partial < OnpChloroplastHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_HAPLOTYPE_FINDER.OCH_REMARK(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
};