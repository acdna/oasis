import { useEffect } from 'react';
import { Dispatch } from 'umi';
/**
 * 剥离出ONP_CHLOROPLAST_HAPLOTYPE表相关的数据加载等逻辑，包装useEffect为方法，主要是为了提高复用性
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const ONP_CHLOROPLAST_HAPLOTYPE_EFFECTS = {
    /**
     * 加载ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表的所有数据信息
     * @author jiangbin
     * @date 4/29/2021, 1:57:37 PM
     **/
    LOAD_ALL: (dispatch: Dispatch, deps ? : any[]) => {
        useEffect(() => {
            dispatch({
                type: 'onp_chloroplast_haplotype/fetchAll',
            });
        }, deps || []);
    },
};