import {DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm} from 'antd';
import React, {useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import {OnpChloroplastHaplotypeService} from '@/services/onp/onp.chloroplast.haplotype.service';
import {OnpChloroplastHaplotypeItem} from '@/pages/onp/chloroplast/haplotype/data';
import {OnpChloroplastHaplotypeHandles} from '@/pages/onp/chloroplast/haplotype/handles';
import OnpChloroplastHaplotypeCreateForm from '@/pages/onp/chloroplast/haplotype/create.form';
import OnpChloroplastHaplotypeUpdateForm from '@/pages/onp/chloroplast/haplotype/update.form';
import {MLOCALES} from '@/common/locales/locale';
import * as moment from 'moment';

interface OnpChloroplastHaplotypeTableListProps {
  // dispatch: Dispatch;
}

/**
 * ONP_CHLOROPLAST_HAPLOTYPE-列表查询
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
const OnpChloroplastHaplotypeTableList: React.FC<OnpChloroplastHaplotypeTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<OnpChloroplastHaplotypeItem>();
  const [selectedRowsState, setSelectedRows] = useState<OnpChloroplastHaplotypeItem[]>([]);
  const handles = new OnpChloroplastHaplotypeHandles();
  //const { dispatch } = props;
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<OnpChloroplastHaplotypeItem>[] = [
    {
      dataIndex: 'ochId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChloroplastHaplotype.label"
                               defaultMessage="Chloroplast"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochChloroplastHaplotype.tip',
        defaultMessage: 'Chloroplast查询',
      })),
      dataIndex: 'ochChloroplastHaplotype',
      valueType: 'text',
      sorter: (source, target) => (source.ochChloroplastHaplotype || '').localeCompare(target.ochChloroplastHaplotype || ''),
      fixed: 'left',

      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChloroplastHaplotype.tip"
                                defaultMessage="Chloroplast"/>
            ),
          },
        ],
      },

      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChr.label" defaultMessage="Chr"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochChr.tip',
        defaultMessage: 'Chr查询',
      })),
      dataIndex: 'ochChr',
      valueType: 'text',
      sorter: (source, target) => (source.ochChr || '').localeCompare(target.ochChr || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChr.tip" defaultMessage="Chr"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChloroplastOnp.label"
                               defaultMessage="Chloroplast"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochChloroplastOnp.tip',
        defaultMessage: 'Chloroplast查询',
      })),
      dataIndex: 'ochChloroplastOnp',
      valueType: 'text',
      sorter: (source, target) => (source.ochChloroplastOnp || '').localeCompare(target.ochChloroplastOnp || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChloroplastOnp.tip"
                                defaultMessage="Chloroplast"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.label"
                               defaultMessage="Chloroplast"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.tip',
        defaultMessage: 'Chloroplast查询',
      })),
      dataIndex: 'ochChloroplastHaplotypeSequence',
      valueType: 'text',
      sorter: (source, target) => (source.ochChloroplastHaplotypeSequence || '').localeCompare(target.ochChloroplastHaplotypeSequence || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochChloroplastHaplotypeSequence.tip"
                                defaultMessage="Chloroplast"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochFrequency.label"
                               defaultMessage="Frequency"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochFrequency.tip',
        defaultMessage: 'Frequency查询',
      })),
      dataIndex: 'ochFrequency',
      valueType: 'text',
      sorter: (source, target) => (source.ochFrequency || '').localeCompare(target.ochFrequency || ''),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochFrequency.tip" defaultMessage="Frequency"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochColumn1.label" defaultMessage="Column1"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochColumn1.tip',
        defaultMessage: 'Column1查询',
      })),
      dataIndex: 'ochColumn1',
      valueType: 'text',
      sorter: (source, target) => (source.ochColumn1 || '').localeCompare(target.ochColumn1 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochColumn1.tip" defaultMessage="Column1"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochColumn2.label" defaultMessage="Column2"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochColumn2.tip',
        defaultMessage: 'Column2查询',
      })),
      dataIndex: 'ochColumn2',
      valueType: 'text',
      sorter: (source, target) => (source.ochColumn2 || '').localeCompare(target.ochColumn2 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochColumn2.tip" defaultMessage="Column2"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochColumn3.label" defaultMessage="Column3"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochColumn3.tip',
        defaultMessage: 'Column3查询',
      })),
      dataIndex: 'ochColumn3',
      valueType: 'text',
      sorter: (source, target) => (source.ochColumn3 || '').localeCompare(target.ochColumn3 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochColumn3.tip" defaultMessage="Column3"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochOrder.label" defaultMessage="ID"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochOrder.tip',
        defaultMessage: 'ID查询',
      })),
      dataIndex: 'ochOrder',
      valueType: 'digit',
      sorter: (source, target) => ((source.ochOrder) - (target.ochOrder)),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochOrder.tip" defaultMessage="ID"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochSpecies.label" defaultMessage="Species"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochSpecies.tip',
        defaultMessage: 'Species查询',
      })),
      dataIndex: 'ochSpecies',
      valueType: 'text',
      sorter: (source, target) => (source.ochSpecies || '').localeCompare(target.ochSpecies || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochSpecies.tip" defaultMessage="Species"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochRemark.label" defaultMessage="Remark"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochRemark.tip',
        defaultMessage: 'Remark查询',
      })),
      dataIndex: 'ochRemark',
      valueType: 'text',
      sorter: (source, target) => (source.ochRemark || '').localeCompare(target.ochRemark || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochRemark.tip" defaultMessage="Remark"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochCreateDate.label" defaultMessage="创建日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'ochCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochCreateDate.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochUpdateDate.label" defaultMessage="更新日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.haplotype.ochUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'ochUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.ochUpdateDate.tip" defaultMessage="更新日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.haplotype.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      align: 'center',
      width: 140,
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.onp.chloroplast.haplotype.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.onp.chloroplast.haplotype.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<OnpChloroplastHaplotypeItem>
        headerTitle={<FormattedMessage id='pages.list.onp.chloroplast.haplotype.form.title' defaultMessage='查询'/>}
        actionRef={actionRef}
        rowKey="ochId"
        scroll={{x: 1300}}
        search={
          {labelWidth: 120,}
        }
        toolBarRender={() => [
          // <Button type="primary" onClick={() => handleCreateVisible(true)}>
          //   <PlusOutlined/> <FormattedMessage id="pages.list.onp.chloroplast.haplotype.new" defaultMessage="新建"/>
          // </Button>,
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
            <PlusOutlined/> <FormattedMessage id="pages.list.onp.chloroplast.haplotype.upload" defaultMessage="上传"/>
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await OnpChloroplastHaplotypeService.findPage({...params, sorter, filter});
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 10,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.chosen" defaultMessage="已选择"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.onp.chloroplast.haplotype.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined/> <FormattedMessage id="pages.list.onp.chloroplast.haplotype.batch.delete"
                                                defaultMessage="批量删除"/>
          </Button>
        </FooterToolbar>
      )}

      <OnpChloroplastHaplotypeCreateForm onCancel={() => handleCreateVisible(false)}
                                         onSubmit={async (value) => {
                                           const success = await handles.add(value);
                                           if (success) {
                                             handleCreateVisible(false);
                                             if (actionRef.current) {
                                               actionRef.current.reload();
                                             }
                                           }
                                         }
                                         }
                                         visible={createVisible}>
      </OnpChloroplastHaplotypeCreateForm>

      {formValues && Object.keys(formValues).length > 0 ? (
        <OnpChloroplastHaplotypeUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.ochId && (
          <ProDescriptions<OnpChloroplastHaplotypeItem>
            column={2}
            title={row?.ochId}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                ochId: row?.ochId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default OnpChloroplastHaplotypeTableList;
