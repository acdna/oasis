/**
 * ONP_CHLOROPLAST_HAPLOTYPE-字段信息
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 */
export interface OnpChloroplastHaplotypeItem {
    ochId: string;
    ochChloroplastHaplotype: string;
    ochChr: string;
    ochChloroplastOnp: string;
    ochChloroplastHaplotypeSequence: string;
    ochFrequency: string;
    ochColumn1: string;
    ochColumn2: string;
    ochColumn3: string;
    ochOrder: number;
    ochSpecies: string;
    ochRemark: string;
    ochCreateDate: Date;
    ochUpdateDate: Date;
    cols ? : string[];
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-分页参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 */
export interface OnpChloroplastHaplotypePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-表格数据
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 */
export interface OnpChloroplastHaplotypeTableData {
    list: OnpChloroplastHaplotypeItem[];
    pagination: Partial < OnpChloroplastHaplotypePagination > ;
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-查询参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 */
export interface OnpChloroplastHaplotypeParams extends OnpChloroplastHaplotypeItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
    nochId ? : string;
    ochIdList ? : string[];
}