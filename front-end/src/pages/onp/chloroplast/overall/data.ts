/**
 * ONP_CHLOROPLAST_OVERALL-字段信息
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpChloroplastOverallItem {
    ocoId: string;
    ocoOnpId: string;
    ocoChr: string;
    ocoStart: string;
    ocoStop: string;
    ocoOnpSize: string;
    ocoLocation: string;
    ocoAllMarkers: string;
    ocoSnpMarkers: string;
    ocoIndelMarkers: string;
    ocoBlockMarkers: string;
    ocoTags: string;
    ocoGenotypes: string;
    ocoMaxGenotypesFreq: string;
    ocoMinGenotypesFreq: string;
    ocoPic: string;
    ocoColumn1: string;
    ocoColumn2: string;
    ocoColumn3: string;
    ocoOrder: number;
    ocoSpecies: string;
    ocoRemark: string;
    ocoCreateDate: Date;
    ocoUpdateDate: Date;
    cols ? : string[];
}
/**
 * ONP_CHLOROPLAST_OVERALL-分页参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpChloroplastOverallPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * ONP_CHLOROPLAST_OVERALL-表格数据
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpChloroplastOverallTableData {
    list: OnpChloroplastOverallItem[];
    pagination: Partial < OnpChloroplastOverallPagination > ;
}
/**
 * ONP_CHLOROPLAST_OVERALL-查询参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpChloroplastOverallParams extends OnpChloroplastOverallItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
    nocoId ? : string;
    ocoIdList ? : string[];
}