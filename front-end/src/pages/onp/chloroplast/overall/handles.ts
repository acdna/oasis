import { OnpChloroplastOverallItem } from '@/pages/onp/chloroplast/overall/data';
import { message } from 'antd';
import { OnpChloroplastOverallService } from '@/services/onp/onp.chloroplast.overall.service';
import { LocaleContext } from '@/common/locales/locale';
/**
 * ONP_CHLOROPLAST_OVERALL-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export class OnpChloroplastOverallHandles {
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: OnpChloroplastOverallItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.chloroplast.overall.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await OnpChloroplastOverallService.add({ ...fields });
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: OnpChloroplastOverallItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.chloroplast.overall.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await OnpChloroplastOverallService.update(fields);
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: OnpChloroplastOverallItem[]) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.chloroplast.overall.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await OnpChloroplastOverallService.deleteByIds(selectedRows.map(row => row.ocoId));
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: OnpChloroplastOverallItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.chloroplast.overall.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await OnpChloroplastOverallService.deleteById(record.ocoId);
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.chloroplast.overall.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};