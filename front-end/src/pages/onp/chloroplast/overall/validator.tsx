import { OnpChloroplastOverallService } from '@/services/onp/onp.chloroplast.overall.service';
import { RuleObject, StoreValue } from "rc-field-form/lib/interface";
import { OnpChloroplastOverallParams } from "@/pages/onp/chloroplast/overall/data";
/**
 * ONP_CHLOROPLAST_OVERALL记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export const ONP_CHLOROPLAST_OVERALL_FINDER = {
    /**
     * ONP ID-字段唯一性验证
     * @param ocoOnpId 输入框输入的ONP ID
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_ONP_ID: async (ocoOnpId: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoOnpId,
        });
        return (res && res.ocoOnpId == ocoOnpId);
    },
    /**
     * Chr-字段唯一性验证
     * @param ocoChr 输入框输入的Chr
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_CHR: async (ocoChr: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoChr,
        });
        return (res && res.ocoChr == ocoChr);
    },
    /**
     * Start-字段唯一性验证
     * @param ocoStart 输入框输入的Start
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_START: async (ocoStart: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoStart,
        });
        return (res && res.ocoStart == ocoStart);
    },
    /**
     * Stop-字段唯一性验证
     * @param ocoStop 输入框输入的Stop
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_STOP: async (ocoStop: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoStop,
        });
        return (res && res.ocoStop == ocoStop);
    },
    /**
     * ONP-字段唯一性验证
     * @param ocoOnpSize 输入框输入的ONP
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_ONP_SIZE: async (ocoOnpSize: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoOnpSize,
        });
        return (res && res.ocoOnpSize == ocoOnpSize);
    },
    /**
     * Location-字段唯一性验证
     * @param ocoLocation 输入框输入的Location
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_LOCATION: async (ocoLocation: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoLocation,
        });
        return (res && res.ocoLocation == ocoLocation);
    },
    /**
     * ALL-字段唯一性验证
     * @param ocoAllMarkers 输入框输入的ALL
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_ALL_MARKERS: async (ocoAllMarkers: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoAllMarkers,
        });
        return (res && res.ocoAllMarkers == ocoAllMarkers);
    },
    /**
     * SNP-字段唯一性验证
     * @param ocoSnpMarkers 输入框输入的SNP
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_SNP_MARKERS: async (ocoSnpMarkers: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoSnpMarkers,
        });
        return (res && res.ocoSnpMarkers == ocoSnpMarkers);
    },
    /**
     * INDEL-字段唯一性验证
     * @param ocoIndelMarkers 输入框输入的INDEL
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_INDEL_MARKERS: async (ocoIndelMarkers: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoIndelMarkers,
        });
        return (res && res.ocoIndelMarkers == ocoIndelMarkers);
    },
    /**
     * Block-字段唯一性验证
     * @param ocoBlockMarkers 输入框输入的Block
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_BLOCK_MARKERS: async (ocoBlockMarkers: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoBlockMarkers,
        });
        return (res && res.ocoBlockMarkers == ocoBlockMarkers);
    },
    /**
     * Tags-字段唯一性验证
     * @param ocoTags 输入框输入的Tags
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_TAGS: async (ocoTags: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoTags,
        });
        return (res && res.ocoTags == ocoTags);
    },
    /**
     * Genotypes-字段唯一性验证
     * @param ocoGenotypes 输入框输入的Genotypes
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_GENOTYPES: async (ocoGenotypes: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoGenotypes,
        });
        return (res && res.ocoGenotypes == ocoGenotypes);
    },
    /**
     * Max-字段唯一性验证
     * @param ocoMaxGenotypesFreq 输入框输入的Max
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_MAX_GENOTYPES_FREQ: async (ocoMaxGenotypesFreq: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoMaxGenotypesFreq,
        });
        return (res && res.ocoMaxGenotypesFreq == ocoMaxGenotypesFreq);
    },
    /**
     * Min-字段唯一性验证
     * @param ocoMinGenotypesFreq 输入框输入的Min
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_MIN_GENOTYPES_FREQ: async (ocoMinGenotypesFreq: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoMinGenotypesFreq,
        });
        return (res && res.ocoMinGenotypesFreq == ocoMinGenotypesFreq);
    },
    /**
     * PIC-字段唯一性验证
     * @param ocoPic 输入框输入的PIC
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_PIC: async (ocoPic: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoPic,
        });
        return (res && res.ocoPic == ocoPic);
    },
    /**
     * Column1-字段唯一性验证
     * @param ocoColumn1 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_COLUMN1: async (ocoColumn1: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoColumn1,
        });
        return (res && res.ocoColumn1 == ocoColumn1);
    },
    /**
     * Column2-字段唯一性验证
     * @param ocoColumn2 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_COLUMN2: async (ocoColumn2: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoColumn2,
        });
        return (res && res.ocoColumn2 == ocoColumn2);
    },
    /**
     * Column3-字段唯一性验证
     * @param ocoColumn3 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_COLUMN3: async (ocoColumn3: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoColumn3,
        });
        return (res && res.ocoColumn3 == ocoColumn3);
    },
    /**
     * Species-字段唯一性验证
     * @param ocoSpecies 输入框输入的Species
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_SPECIES: async (ocoSpecies: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoSpecies,
        });
        return (res && res.ocoSpecies == ocoSpecies);
    },
    /**
     * Remark-字段唯一性验证
     * @param ocoRemark 输入框输入的Remark
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_REMARK: async (ocoRemark: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        const res = await OnpChloroplastOverallService.findOne({
            ...params,
            ocoRemark,
        });
        return (res && res.ocoRemark == ocoRemark);
    },
};
/**
 * ONP_CHLOROPLAST_OVERALL验证器
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export const ONP_CHLOROPLAST_OVERALL_VALIDATOR = {
    /**
     * ONP ID-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_ONP_ID: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_ONP_ID(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Chr-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_CHR: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_CHR(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Start-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_START: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_START(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Stop-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_STOP: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_STOP(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * ONP-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_ONP_SIZE: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_ONP_SIZE(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Location-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_LOCATION: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_LOCATION(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * ALL-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_ALL_MARKERS: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_ALL_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * SNP-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_SNP_MARKERS: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_SNP_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * INDEL-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_INDEL_MARKERS: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_INDEL_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Block-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_BLOCK_MARKERS: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_BLOCK_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Tags-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_TAGS: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_TAGS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Genotypes-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_GENOTYPES: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_GENOTYPES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Max-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_MAX_GENOTYPES_FREQ: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_MAX_GENOTYPES_FREQ(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Min-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_MIN_GENOTYPES_FREQ: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_MIN_GENOTYPES_FREQ(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * PIC-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_PIC: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_PIC(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_COLUMN1: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_COLUMN1(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_COLUMN2: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_COLUMN2(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_COLUMN3: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_COLUMN3(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Species-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_SPECIES: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_SPECIES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Remark-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    OCO_REMARK: (error: string, params: Partial < OnpChloroplastOverallParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_CHLOROPLAST_OVERALL_FINDER.OCO_REMARK(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
};