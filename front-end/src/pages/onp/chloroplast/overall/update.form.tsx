import { OnpChloroplastOverallItem } from "@/pages/onp/chloroplast/overall/data";
import React, { useState,useEffect } from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_CHLOROPLAST_OVERALL-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
interface OnpChloroplastOverallUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpChloroplastOverallItem) => void;
  values: any;
}

/**
 * ONP_CHLOROPLAST_OVERALL-更新字典项信息表单
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpChloroplastOverallUpdateForm: React.FC<OnpChloroplastOverallUpdateFormProps> = (props) => {
  const { values, visible, onCancel, onSubmit } = props;
  const [record,setRecord]=useState(values);
  const intl = useIntl();
  const [form] = Form.useForm();

  //设置各控件的初始值
  useEffect(() => {
      form.setFieldsValue({
          ocoId: record.ocoId,
          ocoOnpId: record.ocoOnpId,
          ocoChr: record.ocoChr,
          ocoStart: record.ocoStart,
          ocoStop: record.ocoStop,
          ocoOnpSize: record.ocoOnpSize,
          ocoLocation: record.ocoLocation,
          ocoAllMarkers: record.ocoAllMarkers,
          ocoSnpMarkers: record.ocoSnpMarkers,
          ocoIndelMarkers: record.ocoIndelMarkers,
          ocoBlockMarkers: record.ocoBlockMarkers,
          ocoTags: record.ocoTags,
          ocoGenotypes: record.ocoGenotypes,
          ocoMaxGenotypesFreq: record.ocoMaxGenotypesFreq,
          ocoMinGenotypesFreq: record.ocoMinGenotypesFreq,
          ocoPic: record.ocoPic,
          ocoColumn1: record.ocoColumn1,
          ocoColumn2: record.ocoColumn2,
          ocoColumn3: record.ocoColumn3,
          ocoOrder: record.ocoOrder,
          ocoSpecies: record.ocoSpecies,
          ocoRemark: record.ocoRemark,
          ocoCreateDate: record.ocoCreateDate,
          ocoUpdateDate: record.ocoUpdateDate,
          
      });
  });

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:38 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...record, ...fieldValues };//合并数据
    setRecord(values);
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:38 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.update.onp.chloroplast.overall.form.title', defaultMessage: '修改' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.update.onp.chloroplast.overall.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.update.onp.chloroplast.overall.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoOnpId"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoOnpId.label',
                        defaultMessage: 'ONP ID',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoOnpId.tip',
                          defaultMessage: 'ONP ID是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoOnpId.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoOnpId.placeholder',
                             defaultMessage: '请输入ONP ID',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoChr"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoChr.label',
                        defaultMessage: 'Chr',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoChr.tip',
                          defaultMessage: 'Chr是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoChr.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoChr.placeholder',
                             defaultMessage: '请输入Chr',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoStart"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoStart.label',
                        defaultMessage: 'Start',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoStart.tip',
                          defaultMessage: 'Start是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoStart.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoStart.placeholder',
                             defaultMessage: '请输入Start',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoStop"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoStop.label',
                        defaultMessage: 'Stop',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoStop.tip',
                          defaultMessage: 'Stop是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoStop.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoStop.placeholder',
                             defaultMessage: '请输入Stop',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoOnpSize"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoOnpSize.label',
                        defaultMessage: 'ONP',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoOnpSize.tip',
                          defaultMessage: 'ONP是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoOnpSize.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoOnpSize.placeholder',
                             defaultMessage: '请输入ONP',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoLocation"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoLocation.label',
                        defaultMessage: 'Location',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoLocation.tip',
                          defaultMessage: 'Location是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoLocation.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoLocation.placeholder',
                             defaultMessage: '请输入Location',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoAllMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoAllMarkers.label',
                        defaultMessage: 'ALL',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoAllMarkers.tip',
                          defaultMessage: 'ALL是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoAllMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoAllMarkers.placeholder',
                             defaultMessage: '请输入ALL',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoSnpMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoSnpMarkers.label',
                        defaultMessage: 'SNP',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoSnpMarkers.tip',
                          defaultMessage: 'SNP是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoSnpMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoSnpMarkers.placeholder',
                             defaultMessage: '请输入SNP',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoIndelMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoIndelMarkers.label',
                        defaultMessage: 'INDEL',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoIndelMarkers.tip',
                          defaultMessage: 'INDEL是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoIndelMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoIndelMarkers.placeholder',
                             defaultMessage: '请输入INDEL',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoBlockMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoBlockMarkers.label',
                        defaultMessage: 'Block',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoBlockMarkers.tip',
                          defaultMessage: 'Block是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoBlockMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoBlockMarkers.placeholder',
                             defaultMessage: '请输入Block',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoTags"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoTags.label',
                        defaultMessage: 'Tags',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoTags.tip',
                          defaultMessage: 'Tags是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoTags.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoTags.placeholder',
                             defaultMessage: '请输入Tags',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoGenotypes"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoGenotypes.label',
                        defaultMessage: 'Genotypes',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoGenotypes.tip',
                          defaultMessage: 'Genotypes是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoGenotypes.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoGenotypes.placeholder',
                             defaultMessage: '请输入Genotypes',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoMaxGenotypesFreq"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoMaxGenotypesFreq.label',
                        defaultMessage: 'Max',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoMaxGenotypesFreq.tip',
                          defaultMessage: 'Max是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoMaxGenotypesFreq.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoMaxGenotypesFreq.placeholder',
                             defaultMessage: '请输入Max',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoMinGenotypesFreq"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoMinGenotypesFreq.label',
                        defaultMessage: 'Min',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoMinGenotypesFreq.tip',
                          defaultMessage: 'Min是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoMinGenotypesFreq.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoMinGenotypesFreq.placeholder',
                             defaultMessage: '请输入Min',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoPic"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoPic.label',
                        defaultMessage: 'PIC',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoPic.tip',
                          defaultMessage: 'PIC是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoPic.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoPic.placeholder',
                             defaultMessage: '请输入PIC',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoColumn1"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoColumn1.label',
                        defaultMessage: 'Column1',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoColumn1.tip',
                          defaultMessage: 'Column1是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoColumn1.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoColumn1.placeholder',
                             defaultMessage: '请输入Column1',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoColumn2"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoColumn2.label',
                        defaultMessage: 'Column2',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoColumn2.tip',
                          defaultMessage: 'Column2是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoColumn2.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoColumn2.placeholder',
                             defaultMessage: '请输入Column2',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoColumn3"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoColumn3.label',
                        defaultMessage: 'Column3',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoColumn3.tip',
                          defaultMessage: 'Column3是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoColumn3.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoColumn3.placeholder',
                             defaultMessage: '请输入Column3',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoOrder"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoOrder.label',
                        defaultMessage: 'ID',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoOrder.tip',
                          defaultMessage: 'ID是必填项!',
                        })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoOrder.placeholder',
                             defaultMessage: '请输入ID',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="ocoSpecies"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoSpecies.label',
                        defaultMessage: 'Species',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoSpecies.tip',
                          defaultMessage: 'Species是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoSpecies.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoSpecies.placeholder',
                             defaultMessage: '请输入Species',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="ocoRemark"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.chloroplast.overall.ocoRemark.label',
                        defaultMessage: 'Remark',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.chloroplast.overall.ocoRemark.tip',
                          defaultMessage: 'Remark是必填项!',
                        })),
                       },
                       {
                          max:128,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.chloroplast.overall.ocoRemark.length.tip',
                              defaultMessage: '最多可输入128个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.chloroplast.overall.ocoRemark.placeholder',
                             defaultMessage: '请输入Remark',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpChloroplastOverallUpdateForm;
