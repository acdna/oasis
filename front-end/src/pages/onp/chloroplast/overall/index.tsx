import {DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm} from 'antd';
import React, {useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import {OnpChloroplastOverallService} from '@/services/onp/onp.chloroplast.overall.service';
import {OnpChloroplastOverallItem} from '@/pages/onp/chloroplast/overall/data';
import {OnpChloroplastOverallHandles} from '@/pages/onp/chloroplast/overall/handles';
import OnpChloroplastOverallCreateForm from '@/pages/onp/chloroplast/overall/create.form';
import OnpChloroplastOverallUpdateForm from '@/pages/onp/chloroplast/overall/update.form';
import {MLOCALES} from '@/common/locales/locale';
import * as moment from 'moment';

interface OnpChloroplastOverallTableListProps {
  // dispatch: Dispatch;
}

/**
 * ONP_CHLOROPLAST_OVERALL-列表查询
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpChloroplastOverallTableList: React.FC<OnpChloroplastOverallTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<OnpChloroplastOverallItem>();
  const [selectedRowsState, setSelectedRows] = useState<OnpChloroplastOverallItem[]>([]);
  const handles = new OnpChloroplastOverallHandles();
  //const { dispatch } = props;
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<OnpChloroplastOverallItem>[] = [
    {
      dataIndex: 'ocoId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoOnpId.label" defaultMessage="ONP ID"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoOnpId.tip',
        defaultMessage: 'ONP ID查询',
      })),
      dataIndex: 'ocoOnpId',
      valueType: 'text',
      sorter: (source, target) => (source.ocoOnpId || '').localeCompare(target.ocoOnpId || ''),
      fixed: 'left',

      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoOnpId.tip" defaultMessage="ONP ID"/>
            ),
          },
        ],
      },

      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoChr.label" defaultMessage="Chr"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoChr.tip',
        defaultMessage: 'Chr查询',
      })),
      dataIndex: 'ocoChr',
      valueType: 'text',
      sorter: (source, target) => (source.ocoChr || '').localeCompare(target.ocoChr || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoChr.tip" defaultMessage="Chr"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoStart.label" defaultMessage="Start"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoStart.tip',
        defaultMessage: 'Start查询',
      })),
      dataIndex: 'ocoStart',
      valueType: 'text',
      sorter: (source, target) => (source.ocoStart || '').localeCompare(target.ocoStart || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoStart.tip" defaultMessage="Start"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoStop.label" defaultMessage="Stop"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoStop.tip',
        defaultMessage: 'Stop查询',
      })),
      dataIndex: 'ocoStop',
      valueType: 'text',
      sorter: (source, target) => (source.ocoStop || '').localeCompare(target.ocoStop || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoStop.tip" defaultMessage="Stop"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoOnpSize.label" defaultMessage="ONP"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoOnpSize.tip',
        defaultMessage: 'ONP查询',
      })),
      dataIndex: 'ocoOnpSize',
      valueType: 'text',
      sorter: (source, target) => (source.ocoOnpSize || '').localeCompare(target.ocoOnpSize || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoOnpSize.tip" defaultMessage="ONP"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoLocation.label" defaultMessage="Location"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoLocation.tip',
        defaultMessage: 'Location查询',
      })),
      dataIndex: 'ocoLocation',
      valueType: 'text',
      sorter: (source, target) => (source.ocoLocation || '').localeCompare(target.ocoLocation || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoLocation.tip" defaultMessage="Location"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoAllMarkers.label" defaultMessage="ALL"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoAllMarkers.tip',
        defaultMessage: 'ALL查询',
      })),
      dataIndex: 'ocoAllMarkers',
      valueType: 'text',
      sorter: (source, target) => (source.ocoAllMarkers || '').localeCompare(target.ocoAllMarkers || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoAllMarkers.tip" defaultMessage="ALL"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoSnpMarkers.label" defaultMessage="SNP"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoSnpMarkers.tip',
        defaultMessage: 'SNP查询',
      })),
      dataIndex: 'ocoSnpMarkers',
      valueType: 'text',
      sorter: (source, target) => (source.ocoSnpMarkers || '').localeCompare(target.ocoSnpMarkers || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoSnpMarkers.tip" defaultMessage="SNP"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoIndelMarkers.label" defaultMessage="INDEL"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoIndelMarkers.tip',
        defaultMessage: 'INDEL查询',
      })),
      dataIndex: 'ocoIndelMarkers',
      valueType: 'text',
      sorter: (source, target) => (source.ocoIndelMarkers || '').localeCompare(target.ocoIndelMarkers || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoIndelMarkers.tip" defaultMessage="INDEL"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoBlockMarkers.label" defaultMessage="Block"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoBlockMarkers.tip',
        defaultMessage: 'Block查询',
      })),
      dataIndex: 'ocoBlockMarkers',
      valueType: 'text',
      sorter: (source, target) => (source.ocoBlockMarkers || '').localeCompare(target.ocoBlockMarkers || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoBlockMarkers.tip" defaultMessage="Block"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoTags.label" defaultMessage="Tags"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoTags.tip',
        defaultMessage: 'Tags查询',
      })),
      dataIndex: 'ocoTags',
      valueType: 'text',
      sorter: (source, target) => (source.ocoTags || '').localeCompare(target.ocoTags || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoTags.tip" defaultMessage="Tags"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoGenotypes.label" defaultMessage="Genotypes"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoGenotypes.tip',
        defaultMessage: 'Genotypes查询',
      })),
      dataIndex: 'ocoGenotypes',
      valueType: 'text',
      sorter: (source, target) => (source.ocoGenotypes || '').localeCompare(target.ocoGenotypes || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoGenotypes.tip" defaultMessage="Genotypes"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoMaxGenotypesFreq.label" defaultMessage="Max"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoMaxGenotypesFreq.tip',
        defaultMessage: 'Max查询',
      })),
      dataIndex: 'ocoMaxGenotypesFreq',
      valueType: 'text',
      sorter: (source, target) => (source.ocoMaxGenotypesFreq || '').localeCompare(target.ocoMaxGenotypesFreq || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoMaxGenotypesFreq.tip" defaultMessage="Max"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoMinGenotypesFreq.label" defaultMessage="Min"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoMinGenotypesFreq.tip',
        defaultMessage: 'Min查询',
      })),
      dataIndex: 'ocoMinGenotypesFreq',
      valueType: 'text',
      sorter: (source, target) => (source.ocoMinGenotypesFreq || '').localeCompare(target.ocoMinGenotypesFreq || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoMinGenotypesFreq.tip" defaultMessage="Min"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoPic.label" defaultMessage="PIC"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoPic.tip',
        defaultMessage: 'PIC查询',
      })),
      dataIndex: 'ocoPic',
      valueType: 'text',
      sorter: (source, target) => (source.ocoPic || '').localeCompare(target.ocoPic || ''),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoPic.tip" defaultMessage="PIC"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoColumn1.label" defaultMessage="Column1"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoColumn1.tip',
        defaultMessage: 'Column1查询',
      })),
      dataIndex: 'ocoColumn1',
      valueType: 'text',
      sorter: (source, target) => (source.ocoColumn1 || '').localeCompare(target.ocoColumn1 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoColumn1.tip" defaultMessage="Column1"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoColumn2.label" defaultMessage="Column2"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoColumn2.tip',
        defaultMessage: 'Column2查询',
      })),
      dataIndex: 'ocoColumn2',
      valueType: 'text',
      sorter: (source, target) => (source.ocoColumn2 || '').localeCompare(target.ocoColumn2 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoColumn2.tip" defaultMessage="Column2"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoColumn3.label" defaultMessage="Column3"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoColumn3.tip',
        defaultMessage: 'Column3查询',
      })),
      dataIndex: 'ocoColumn3',
      valueType: 'text',
      sorter: (source, target) => (source.ocoColumn3 || '').localeCompare(target.ocoColumn3 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoColumn3.tip" defaultMessage="Column3"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoOrder.label" defaultMessage="ID"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoOrder.tip',
        defaultMessage: 'ID查询',
      })),
      dataIndex: 'ocoOrder',
      valueType: 'digit',
      sorter: (source, target) => ((source.ocoOrder) - (target.ocoOrder)),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoOrder.tip" defaultMessage="ID"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoSpecies.label" defaultMessage="Species"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoSpecies.tip',
        defaultMessage: 'Species查询',
      })),
      dataIndex: 'ocoSpecies',
      valueType: 'text',
      sorter: (source, target) => (source.ocoSpecies || '').localeCompare(target.ocoSpecies || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoSpecies.tip" defaultMessage="Species"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoRemark.label" defaultMessage="Remark"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoRemark.tip',
        defaultMessage: 'Remark查询',
      })),
      dataIndex: 'ocoRemark',
      valueType: 'text',
      sorter: (source, target) => (source.ocoRemark || '').localeCompare(target.ocoRemark || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoRemark.tip" defaultMessage="Remark"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoCreateDate.label" defaultMessage="创建日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'ocoCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoCreateDate.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoUpdateDate.label" defaultMessage="更新日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.chloroplast.overall.ocoUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'ocoUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.chloroplast.overall.ocoUpdateDate.tip" defaultMessage="更新日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.chloroplast.overall.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      align: 'center',
      width: 140,
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.onp.chloroplast.overall.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.onp.chloroplast.overall.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.onp.chloroplast.overall.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<OnpChloroplastOverallItem>
        headerTitle={<FormattedMessage id='pages.list.onp.chloroplast.overall.form.title' defaultMessage='查询'/>}
        actionRef={actionRef}
        rowKey="ocoId"
        scroll={{x: 1700}}
        search={
          {labelWidth: 120,}
        }
        toolBarRender={() => [
          // <Button type="primary" onClick={() => handleCreateVisible(true)}>
          //   <PlusOutlined/> <FormattedMessage id="pages.list.onp.chloroplast.overall.new" defaultMessage="新建"/>
          // </Button>,
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
               <PlusOutlined/> <FormattedMessage id="pages.list.onp.chloroplast.overall.upload" defaultMessage="上传"/>
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await OnpChloroplastOverallService.findPage({...params, sorter, filter});
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 10,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.onp.chloroplast.overall.chosen" defaultMessage="已选择"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.onp.chloroplast.overall.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined/> <FormattedMessage id="pages.list.onp.chloroplast.overall.batch.delete"
                                                defaultMessage="批量删除"/>
          </Button>
        </FooterToolbar>
      )}

      <OnpChloroplastOverallCreateForm onCancel={() => handleCreateVisible(false)}
                                       onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                           handleCreateVisible(false);
                                           if (actionRef.current) {
                                             actionRef.current.reload();
                                           }
                                         }
                                       }
                                       }
                                       visible={createVisible}>
      </OnpChloroplastOverallCreateForm>

      {formValues && Object.keys(formValues).length > 0 ? (
        <OnpChloroplastOverallUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.ocoId && (
          <ProDescriptions<OnpChloroplastOverallItem>
            column={2}
            title={row?.ocoId}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                ocoId: row?.ocoId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default OnpChloroplastOverallTableList;
