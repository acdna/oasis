import { OnpChloroplastOverallItem } from "@/pages/onp/chloroplast/overall/data";
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_CHLOROPLAST_OVERALL-创建记录表单的输入参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
interface OnpChloroplastOverallCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpChloroplastOverallItem) => void;
}

/**
 * ONP_CHLOROPLAST_OVERALL-创建记录表单
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpChloroplastOverallCreateForm: React.FC<OnpChloroplastOverallCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:38 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { ocoId,ocoOnpId,ocoChr,ocoStart,ocoStop,ocoOnpSize,ocoLocation,ocoAllMarkers,ocoSnpMarkers,ocoIndelMarkers,ocoBlockMarkers,ocoTags,ocoGenotypes,ocoMaxGenotypesFreq,ocoMinGenotypesFreq,ocoPic,ocoColumn1,ocoColumn2,ocoColumn3,ocoOrder,ocoSpecies,ocoRemark,ocoCreateDate,ocoUpdateDate }=fieldValues;
    onSubmit({ ocoId,ocoOnpId,ocoChr,ocoStart,ocoStop,ocoOnpSize,ocoLocation,ocoAllMarkers,ocoSnpMarkers,ocoIndelMarkers,ocoBlockMarkers,ocoTags,ocoGenotypes,ocoMaxGenotypesFreq,ocoMinGenotypesFreq,ocoPic,ocoColumn1,ocoColumn2,ocoColumn3,ocoOrder,ocoSpecies,ocoRemark,ocoCreateDate,ocoUpdateDate });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:38 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.create.onp.chloroplast.overall.form.title', defaultMessage: '创建' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.create.onp.chloroplast.overall.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.onp.chloroplast.overall.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoOnpId"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoOnpId.label',
                  defaultMessage: 'ONP ID',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoOnpId.tip',
                        defaultMessage: 'ONP ID是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoOnpId.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoOnpId.placeholder',
                  defaultMessage: 'ONP ID',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoChr"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoChr.label',
                  defaultMessage: 'Chr',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoChr.tip',
                        defaultMessage: 'Chr是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoChr.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoChr.placeholder',
                  defaultMessage: 'Chr',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoStart"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoStart.label',
                  defaultMessage: 'Start',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoStart.tip',
                        defaultMessage: 'Start是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoStart.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoStart.placeholder',
                  defaultMessage: 'Start',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoStop"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoStop.label',
                  defaultMessage: 'Stop',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoStop.tip',
                        defaultMessage: 'Stop是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoStop.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoStop.placeholder',
                  defaultMessage: 'Stop',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoOnpSize"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoOnpSize.label',
                  defaultMessage: 'ONP',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoOnpSize.tip',
                        defaultMessage: 'ONP是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoOnpSize.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoOnpSize.placeholder',
                  defaultMessage: 'ONP',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoLocation"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoLocation.label',
                  defaultMessage: 'Location',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoLocation.tip',
                        defaultMessage: 'Location是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoLocation.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoLocation.placeholder',
                  defaultMessage: 'Location',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoAllMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoAllMarkers.label',
                  defaultMessage: 'ALL',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoAllMarkers.tip',
                        defaultMessage: 'ALL是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoAllMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoAllMarkers.placeholder',
                  defaultMessage: 'ALL',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoSnpMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoSnpMarkers.label',
                  defaultMessage: 'SNP',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoSnpMarkers.tip',
                        defaultMessage: 'SNP是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoSnpMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoSnpMarkers.placeholder',
                  defaultMessage: 'SNP',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoIndelMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoIndelMarkers.label',
                  defaultMessage: 'INDEL',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoIndelMarkers.tip',
                        defaultMessage: 'INDEL是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoIndelMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoIndelMarkers.placeholder',
                  defaultMessage: 'INDEL',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoBlockMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoBlockMarkers.label',
                  defaultMessage: 'Block',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoBlockMarkers.tip',
                        defaultMessage: 'Block是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoBlockMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoBlockMarkers.placeholder',
                  defaultMessage: 'Block',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoTags"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoTags.label',
                  defaultMessage: 'Tags',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoTags.tip',
                        defaultMessage: 'Tags是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoTags.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoTags.placeholder',
                  defaultMessage: 'Tags',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoGenotypes"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoGenotypes.label',
                  defaultMessage: 'Genotypes',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoGenotypes.tip',
                        defaultMessage: 'Genotypes是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoGenotypes.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoGenotypes.placeholder',
                  defaultMessage: 'Genotypes',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoMaxGenotypesFreq"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoMaxGenotypesFreq.label',
                  defaultMessage: 'Max',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoMaxGenotypesFreq.tip',
                        defaultMessage: 'Max是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoMaxGenotypesFreq.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoMaxGenotypesFreq.placeholder',
                  defaultMessage: 'Max',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoMinGenotypesFreq"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoMinGenotypesFreq.label',
                  defaultMessage: 'Min',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoMinGenotypesFreq.tip',
                        defaultMessage: 'Min是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoMinGenotypesFreq.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoMinGenotypesFreq.placeholder',
                  defaultMessage: 'Min',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoPic"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoPic.label',
                  defaultMessage: 'PIC',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoPic.tip',
                        defaultMessage: 'PIC是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoPic.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoPic.placeholder',
                  defaultMessage: 'PIC',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoColumn1"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoColumn1.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoColumn1.tip',
                        defaultMessage: 'Column1是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoColumn1.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoColumn1.placeholder',
                  defaultMessage: 'Column1',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoColumn2"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoColumn2.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoColumn2.tip',
                        defaultMessage: 'Column2是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoColumn2.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoColumn2.placeholder',
                  defaultMessage: 'Column2',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoColumn3"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoColumn3.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoColumn3.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoColumn3.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoColumn3.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoOrder"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoOrder.label',
                  defaultMessage: 'ID',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoOrder.tip',
                        defaultMessage: 'ID是必填项!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoOrder.placeholder',
                  defaultMessage: 'ID',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="ocoSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoSpecies.label',
                  defaultMessage: 'Species',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoSpecies.tip',
                        defaultMessage: 'Species是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoSpecies.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoSpecies.placeholder',
                  defaultMessage: 'Species',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="ocoRemark"
                label={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoRemark.label',
                  defaultMessage: 'Remark',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.chloroplast.overall.ocoRemark.tip',
                        defaultMessage: 'Remark是必填项!',
                      })),
                    },
                    {
                      max:128,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.chloroplast.overall.ocoRemark.length.tip',
                         defaultMessage: '最多可输入128个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.chloroplast.overall.ocoRemark.placeholder',
                  defaultMessage: 'Remark',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpChloroplastOverallCreateForm;
