/**
 * ONP_NUCLEUS_HAPLOTYPE-字段信息
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpNucleusHaplotypeItem {
    onhId: string;
    onhHaplotypeIndex: string;
    onhChr: string;
    onhOnpId: string;
    onhHaplotypeSequence: string;
    onhHaplotypeTagSequence: string;
    onhFrequency: string;
    onhColumn1: string;
    onhColumn2: string;
    onhColumn3: string;
    onhOrder: number;
    onhSpecies: string;
    onhRemark: string;
    onhCreateDate: Date;
    onhUpdateDate: Date;
    cols ? : string[];
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-分页参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpNucleusHaplotypePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-表格数据
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpNucleusHaplotypeTableData {
    list: OnpNucleusHaplotypeItem[];
    pagination: Partial < OnpNucleusHaplotypePagination > ;
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-查询参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 */
export interface OnpNucleusHaplotypeParams extends OnpNucleusHaplotypeItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
    nonhId ? : string;
    onhIdList ? : string[];
}