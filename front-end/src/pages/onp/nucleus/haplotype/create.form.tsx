import { OnpNucleusHaplotypeItem } from "@/pages/onp/nucleus/haplotype/data";
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_NUCLEUS_HAPLOTYPE-创建记录表单的输入参数
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
interface OnpNucleusHaplotypeCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpNucleusHaplotypeItem) => void;
}

/**
 * ONP_NUCLEUS_HAPLOTYPE-创建记录表单
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpNucleusHaplotypeCreateForm: React.FC<OnpNucleusHaplotypeCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:38 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { onhId,onhHaplotypeIndex,onhChr,onhOnpId,onhHaplotypeSequence,onhHaplotypeTagSequence,onhFrequency,onhColumn1,onhColumn2,onhColumn3,onhOrder,onhSpecies,onhRemark,onhCreateDate,onhUpdateDate }=fieldValues;
    onSubmit({ onhId,onhHaplotypeIndex,onhChr,onhOnpId,onhHaplotypeSequence,onhHaplotypeTagSequence,onhFrequency,onhColumn1,onhColumn2,onhColumn3,onhOrder,onhSpecies,onhRemark,onhCreateDate,onhUpdateDate });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/29/2021, 1:57:38 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.create.onp.nucleus.haplotype.form.title', defaultMessage: '创建' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.create.onp.nucleus.haplotype.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.onp.nucleus.haplotype.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="onhHaplotypeIndex"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeIndex.label',
                  defaultMessage: 'Haplotype Index',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeIndex.tip',
                        defaultMessage: 'Haplotype Index是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeIndex.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeIndex.placeholder',
                  defaultMessage: 'Haplotype Index',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="onhChr"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhChr.label',
                  defaultMessage: 'Chr',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhChr.tip',
                        defaultMessage: 'Chr是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhChr.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhChr.placeholder',
                  defaultMessage: 'Chr',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="onhOnpId"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhOnpId.label',
                  defaultMessage: 'ONP ID',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhOnpId.tip',
                        defaultMessage: 'ONP ID是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhOnpId.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhOnpId.placeholder',
                  defaultMessage: 'ONP ID',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="onhHaplotypeSequence"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeSequence.label',
                  defaultMessage: 'Haplotype Sequence',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeSequence.tip',
                        defaultMessage: 'Haplotype Sequence是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeSequence.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeSequence.placeholder',
                  defaultMessage: 'Haplotype Sequence',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="onhHaplotypeTagSequence"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeTagSequence.label',
                  defaultMessage: 'Haplotype Tag',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeTagSequence.tip',
                        defaultMessage: 'Haplotype Tag是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeTagSequence.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhHaplotypeTagSequence.placeholder',
                  defaultMessage: 'Haplotype Tag',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="onhFrequency"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhFrequency.label',
                  defaultMessage: 'Frequency',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhFrequency.tip',
                        defaultMessage: 'Frequency是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhFrequency.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhFrequency.placeholder',
                  defaultMessage: 'Frequency',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="onhColumn1"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhColumn1.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhColumn1.tip',
                        defaultMessage: 'Column1是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhColumn1.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhColumn1.placeholder',
                  defaultMessage: 'Column1',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="onhColumn2"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhColumn2.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhColumn2.tip',
                        defaultMessage: 'Column2是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhColumn2.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhColumn2.placeholder',
                  defaultMessage: 'Column2',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="onhColumn3"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhColumn3.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhColumn3.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhColumn3.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhColumn3.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="onhOrder"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhOrder.label',
                  defaultMessage: 'ID',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhOrder.tip',
                        defaultMessage: 'ID是必填项!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhOrder.placeholder',
                  defaultMessage: 'ID',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="onhSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhSpecies.label',
                  defaultMessage: 'Species',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhSpecies.tip',
                        defaultMessage: 'Species是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhSpecies.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhSpecies.placeholder',
                  defaultMessage: 'Species',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="onhRemark"
                label={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhRemark.label',
                  defaultMessage: 'Remark',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.nucleus.haplotype.onhRemark.tip',
                        defaultMessage: 'Remark是必填项!',
                      })),
                    },
                    {
                      max:128,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.nucleus.haplotype.onhRemark.length.tip',
                         defaultMessage: '最多可输入128个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.nucleus.haplotype.onhRemark.placeholder',
                  defaultMessage: 'Remark',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpNucleusHaplotypeCreateForm;
