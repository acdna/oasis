import {DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm} from 'antd';
import React, {useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import {OnpNucleusHaplotypeService} from '@/services/onp/onp.nucleus.haplotype.service';
import {OnpNucleusHaplotypeItem} from '@/pages/onp/nucleus/haplotype/data';
import {OnpNucleusHaplotypeHandles} from '@/pages/onp/nucleus/haplotype/handles';
import OnpNucleusHaplotypeCreateForm from '@/pages/onp/nucleus/haplotype/create.form';
import OnpNucleusHaplotypeUpdateForm from '@/pages/onp/nucleus/haplotype/update.form';
import {MLOCALES} from '@/common/locales/locale';
import * as moment from 'moment';

interface OnpNucleusHaplotypeTableListProps {
  // dispatch: Dispatch;
}

/**
 * ONP_NUCLEUS_HAPLOTYPE-列表查询
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpNucleusHaplotypeTableList: React.FC<OnpNucleusHaplotypeTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<OnpNucleusHaplotypeItem>();
  const [selectedRowsState, setSelectedRows] = useState<OnpNucleusHaplotypeItem[]>([]);
  const handles = new OnpNucleusHaplotypeHandles();
  //const { dispatch } = props;
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<OnpNucleusHaplotypeItem>[] = [
    {
      dataIndex: 'onhId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhHaplotypeIndex.label"
                               defaultMessage="Haplotype Index"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhHaplotypeIndex.tip',
        defaultMessage: 'Haplotype Index查询',
      })),
      dataIndex: 'onhHaplotypeIndex',
      valueType: 'text',
      sorter: (source, target) => (source.onhHaplotypeIndex || '').localeCompare(target.onhHaplotypeIndex || ''),
      fixed: 'left',

      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhHaplotypeIndex.tip"
                                defaultMessage="Haplotype Index"/>
            ),
          },
        ],
      },

      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhChr.label" defaultMessage="Chr"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhChr.tip',
        defaultMessage: 'Chr查询',
      })),
      dataIndex: 'onhChr',
      valueType: 'text',
      sorter: (source, target) => (source.onhChr || '').localeCompare(target.onhChr || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhChr.tip" defaultMessage="Chr"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhOnpId.label" defaultMessage="ONP ID"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhOnpId.tip',
        defaultMessage: 'ONP ID查询',
      })),
      dataIndex: 'onhOnpId',
      valueType: 'text',
      sorter: (source, target) => (source.onhOnpId || '').localeCompare(target.onhOnpId || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhOnpId.tip" defaultMessage="ONP ID"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhHaplotypeSequence.label"
                               defaultMessage="Haplotype Sequence"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhHaplotypeSequence.tip',
        defaultMessage: 'Haplotype Sequence查询',
      })),
      dataIndex: 'onhHaplotypeSequence',
      valueType: 'text',
      sorter: (source, target) => (source.onhHaplotypeSequence || '').localeCompare(target.onhHaplotypeSequence || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhHaplotypeSequence.tip"
                                defaultMessage="Haplotype Sequence"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhHaplotypeTagSequence.label"
                               defaultMessage="Haplotype Tag"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhHaplotypeTagSequence.tip',
        defaultMessage: 'Haplotype Tag查询',
      })),
      dataIndex: 'onhHaplotypeTagSequence',
      valueType: 'text',
      sorter: (source, target) => (source.onhHaplotypeTagSequence || '').localeCompare(target.onhHaplotypeTagSequence || ''),


      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhHaplotypeTagSequence.tip"
                                defaultMessage="Haplotype Tag"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhFrequency.label" defaultMessage="Frequency"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhFrequency.tip',
        defaultMessage: 'Frequency查询',
      })),
      dataIndex: 'onhFrequency',
      valueType: 'text',
      sorter: (source, target) => (source.onhFrequency || '').localeCompare(target.onhFrequency || ''),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhFrequency.tip" defaultMessage="Frequency"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhColumn1.label" defaultMessage="Column1"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhColumn1.tip',
        defaultMessage: 'Column1查询',
      })),
      dataIndex: 'onhColumn1',
      valueType: 'text',
      sorter: (source, target) => (source.onhColumn1 || '').localeCompare(target.onhColumn1 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhColumn1.tip" defaultMessage="Column1"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhColumn2.label" defaultMessage="Column2"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhColumn2.tip',
        defaultMessage: 'Column2查询',
      })),
      dataIndex: 'onhColumn2',
      valueType: 'text',
      sorter: (source, target) => (source.onhColumn2 || '').localeCompare(target.onhColumn2 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhColumn2.tip" defaultMessage="Column2"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhColumn3.label" defaultMessage="Column3"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhColumn3.tip',
        defaultMessage: 'Column3查询',
      })),
      dataIndex: 'onhColumn3',
      valueType: 'text',
      sorter: (source, target) => (source.onhColumn3 || '').localeCompare(target.onhColumn3 || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhColumn3.tip" defaultMessage="Column3"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhOrder.label" defaultMessage="ID"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhOrder.tip',
        defaultMessage: 'ID查询',
      })),
      dataIndex: 'onhOrder',
      valueType: 'digit',
      sorter: (source, target) => ((source.onhOrder) - (target.onhOrder)),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhOrder.tip" defaultMessage="ID"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhSpecies.label" defaultMessage="Species"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhSpecies.tip',
        defaultMessage: 'Species查询',
      })),
      dataIndex: 'onhSpecies',
      valueType: 'text',
      sorter: (source, target) => (source.onhSpecies || '').localeCompare(target.onhSpecies || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhSpecies.tip" defaultMessage="Species"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhRemark.label" defaultMessage="Remark"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhRemark.tip',
        defaultMessage: 'Remark查询',
      })),
      dataIndex: 'onhRemark',
      valueType: 'text',
      sorter: (source, target) => (source.onhRemark || '').localeCompare(target.onhRemark || ''),
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhRemark.tip" defaultMessage="Remark"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhCreateDate.label" defaultMessage="创建日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'onhCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhCreateDate.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhUpdateDate.label" defaultMessage="更新日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.nucleus.haplotype.onhUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'onhUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.onhUpdateDate.tip" defaultMessage="更新日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.nucleus.haplotype.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      align: 'center',
      width: 140,
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.onp.nucleus.haplotype.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.onp.nucleus.haplotype.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<OnpNucleusHaplotypeItem>
        headerTitle={<FormattedMessage id='pages.list.onp.nucleus.haplotype.form.title' defaultMessage='查询'/>}
        actionRef={actionRef}
        rowKey="onhId"
        scroll={{x: 1300}}
        search={
          {labelWidth: 120,}
        }
        toolBarRender={() => [
          // <Button type="primary" onClick={() => handleCreateVisible(true)}>
          //   <PlusOutlined/> <FormattedMessage id="pages.list.onp.nucleus.haplotype.new" defaultMessage="新建"/>
          // </Button>,
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
            <PlusOutlined/> <FormattedMessage id="pages.list.onp.nucleus.haplotype.upload" defaultMessage="上传"/>
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await OnpNucleusHaplotypeService.findPage({...params, sorter, filter});
          console.log('data===>',data);
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 10,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.chosen" defaultMessage="已选择"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.onp.nucleus.haplotype.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined/> <FormattedMessage id="pages.list.onp.nucleus.haplotype.batch.delete"
                                                defaultMessage="批量删除"/>
          </Button>
        </FooterToolbar>
      )}

      <OnpNucleusHaplotypeCreateForm onCancel={() => handleCreateVisible(false)}
                                     onSubmit={async (value) => {
                                       const success = await handles.add(value);
                                       if (success) {
                                         handleCreateVisible(false);
                                         if (actionRef.current) {
                                           actionRef.current.reload();
                                         }
                                       }
                                     }
                                     }
                                     visible={createVisible}>
      </OnpNucleusHaplotypeCreateForm>

      {formValues && Object.keys(formValues).length > 0 ? (
        <OnpNucleusHaplotypeUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.onhId && (
          <ProDescriptions<OnpNucleusHaplotypeItem>
            column={2}
            title={row?.onhId}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                onhId: row?.onhId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default OnpNucleusHaplotypeTableList;
