import { OnpNucleusHaplotypeService } from '@/services/onp/onp.nucleus.haplotype.service';
import { RuleObject, StoreValue } from "rc-field-form/lib/interface";
import { OnpNucleusHaplotypeParams } from "@/pages/onp/nucleus/haplotype/data";
/**
 * ONP_NUCLEUS_HAPLOTYPE记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export const ONP_NUCLEUS_HAPLOTYPE_FINDER = {
    /**
     * Haplotype Index-字段唯一性验证
     * @param onhHaplotypeIndex 输入框输入的Haplotype Index
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_HAPLOTYPE_INDEX: async (onhHaplotypeIndex: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhHaplotypeIndex,
        });
        return (res && res.onhHaplotypeIndex == onhHaplotypeIndex);
    },
    /**
     * Chr-字段唯一性验证
     * @param onhChr 输入框输入的Chr
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_CHR: async (onhChr: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhChr,
        });
        return (res && res.onhChr == onhChr);
    },
    /**
     * ONP ID-字段唯一性验证
     * @param onhOnpId 输入框输入的ONP ID
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_ONP_ID: async (onhOnpId: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhOnpId,
        });
        return (res && res.onhOnpId == onhOnpId);
    },
    /**
     * Haplotype Sequence-字段唯一性验证
     * @param onhHaplotypeSequence 输入框输入的Haplotype Sequence
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_HAPLOTYPE_SEQUENCE: async (onhHaplotypeSequence: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhHaplotypeSequence,
        });
        return (res && res.onhHaplotypeSequence == onhHaplotypeSequence);
    },
    /**
     * Haplotype Tag-字段唯一性验证
     * @param onhHaplotypeTagSequence 输入框输入的Haplotype Tag
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_HAPLOTYPE_TAG_SEQUENCE: async (onhHaplotypeTagSequence: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhHaplotypeTagSequence,
        });
        return (res && res.onhHaplotypeTagSequence == onhHaplotypeTagSequence);
    },
    /**
     * Frequency-字段唯一性验证
     * @param onhFrequency 输入框输入的Frequency
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_FREQUENCY: async (onhFrequency: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhFrequency,
        });
        return (res && res.onhFrequency == onhFrequency);
    },
    /**
     * Column1-字段唯一性验证
     * @param onhColumn1 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_COLUMN1: async (onhColumn1: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhColumn1,
        });
        return (res && res.onhColumn1 == onhColumn1);
    },
    /**
     * Column2-字段唯一性验证
     * @param onhColumn2 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_COLUMN2: async (onhColumn2: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhColumn2,
        });
        return (res && res.onhColumn2 == onhColumn2);
    },
    /**
     * Column3-字段唯一性验证
     * @param onhColumn3 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_COLUMN3: async (onhColumn3: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhColumn3,
        });
        return (res && res.onhColumn3 == onhColumn3);
    },
    /**
     * Species-字段唯一性验证
     * @param onhSpecies 输入框输入的Species
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_SPECIES: async (onhSpecies: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhSpecies,
        });
        return (res && res.onhSpecies == onhSpecies);
    },
    /**
     * Remark-字段唯一性验证
     * @param onhRemark 输入框输入的Remark
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_REMARK: async (onhRemark: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        const res = await OnpNucleusHaplotypeService.findOne({
            ...params,
            onhRemark,
        });
        return (res && res.onhRemark == onhRemark);
    },
};
/**
 * ONP_NUCLEUS_HAPLOTYPE验证器
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export const ONP_NUCLEUS_HAPLOTYPE_VALIDATOR = {
    /**
     * Haplotype Index-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_HAPLOTYPE_INDEX: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_HAPLOTYPE_INDEX(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Chr-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_CHR: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_CHR(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * ONP ID-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_ONP_ID: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_ONP_ID(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Haplotype Sequence-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_HAPLOTYPE_SEQUENCE: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_HAPLOTYPE_SEQUENCE(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Haplotype Tag-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_HAPLOTYPE_TAG_SEQUENCE: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_HAPLOTYPE_TAG_SEQUENCE(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Frequency-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_FREQUENCY: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_FREQUENCY(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_COLUMN1: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_COLUMN1(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_COLUMN2: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_COLUMN2(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_COLUMN3: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_COLUMN3(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Species-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_SPECIES: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_SPECIES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Remark-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/29/2021, 1:57:38 PM
     **/
    ONH_REMARK: (error: string, params: Partial < OnpNucleusHaplotypeParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_NUCLEUS_HAPLOTYPE_FINDER.ONH_REMARK(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
};