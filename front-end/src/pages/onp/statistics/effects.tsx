import { useEffect } from 'react';
import { Dispatch } from 'umi';
/**
 * 剥离出ONP_STATISTICS表相关的数据加载等逻辑，包装useEffect为方法，主要是为了提高复用性
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_STATISTICS_EFFECTS = {
    /**
     * 加载ONP_STATISTICS-ONP统计数据表表的所有数据信息
     * @author jiangbin
     * @date 4/6/2021, 10:21:58 AM
     **/
    LOAD_ALL: (dispatch: Dispatch, deps ? : any[]) => {
        useEffect(() => {
            dispatch({
                type: 'onp_statistics/fetchAll',
            });
        }, deps || []);
    },
};