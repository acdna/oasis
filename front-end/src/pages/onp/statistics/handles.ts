import {OnpStatisticsItem} from '@/pages/onp/statistics/data';
import {message} from 'antd';
import {OnpStatisticsService} from '@/services/onp/onp.statistics.service';
import {LocaleContext} from '@/common/locales/locale';
import {FileHandles} from "@/pages/common/file/file.handles";

/**
 * ONP_STATISTICS-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export class OnpStatisticsHandles {
  /**
   * 添加记录
   * @param fields
   */
  add = async (fields: OnpStatisticsItem) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics.add.loading',
      defaultMessage: '正在添加',
    }));
    try {
      await OnpStatisticsService.add({...fields});
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.add.success',
        defaultMessage: '添加成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.add.error',
        defaultMessage: '添加失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 更新记录
   * @param fields 更新信息
   */
  update = async (fields: OnpStatisticsItem) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics.update.loading',
      defaultMessage: '正在更新',
    }));
    try {
      await OnpStatisticsService.update(fields);
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.update.success',
        defaultMessage: '更新成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.update.error',
        defaultMessage: '更新失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param selectedRows 选中行记录列表
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteByIds = async (selectedRows: OnpStatisticsItem[]) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!selectedRows) return true;
    try {
      await OnpStatisticsService.deleteByIds(selectedRows.map(row => row.stId));
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param record 被删除的记录
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteById = async (record: OnpStatisticsItem) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!record) return true;
    try {
      await OnpStatisticsService.deleteById(record.stId);
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * @Description: 上传信息
   * @author zhengenze
   * @date 2021/4/8 3:20 下午
   **/
  upload = async (fields: Partial<OnpStatisticsItem>) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics..upload.loading',
      defaultMessage: '正在上传',
    }));
    try {
      await OnpStatisticsService.upload({...fields});
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.upload.success',
        defaultMessage: '上传成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.upload.error',
        defaultMessage: '上传失败，请重试!',
      }));
      return false;
    }
  };
  /**
   * @Description: 条件下载
   * @author zhengenze
   * @date 2021/4/9 10:20 上午
   **/
  downloadByBins = async (fields: Partial<OnpStatisticsItem>) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics.loading',
      defaultMessage: '正在导出',
    }));
    try {
      let pathResult = await OnpStatisticsService.downloadByBins({...fields});
      if(!pathResult?.path){
        hide();
        message.error(LocaleContext.message({
          id: 'page.onp.statistics.error',
          defaultMessage: '导出失败，请重试!',
        }));
        return false;
      }
      let fileHandles = new FileHandles();
      await fileHandles.downloadByPath({
        path: pathResult['relativePath'],
        fileName: LocaleContext.getLocaleMessage({cn: 'statistics', en: 'statistics'}),
        fileExt: pathResult['fileExt'],
      })
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.success',
        defaultMessage: '导出成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.error',
        defaultMessage: '导出失败，请重试!',
      }));
      return false;
    }
  };

  downloadAll = async () => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics.loading',
      defaultMessage: '正在导出',
    }));
    try {
      let pathResult = await OnpStatisticsService.downloadAll();
      if(!pathResult?.path){
        hide();
        message.error(LocaleContext.message({
          id: 'page.onp.statistics.error',
          defaultMessage: '导出失败，请重试!',
        }));
        return false;
      }
      let fileHandles = new FileHandles();
      await fileHandles.downloadByPath({
        path: pathResult['relativePath'],
        fileName: LocaleContext.getLocaleMessage({cn: 'statistics', en: 'statistics'}),
        fileExt: pathResult['fileExt'],
      })
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.success',
        defaultMessage: '导出成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.error',
        defaultMessage: '导出失败，请重试!',
      }));
      return false;
    }
  };

};
