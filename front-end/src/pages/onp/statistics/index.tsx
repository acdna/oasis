import {CloudDownloadOutlined, PlusOutlined, SearchOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm, Tabs} from 'antd';
import React, {useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import {OnpStatisticsService} from '@/services/onp/onp.statistics.service';
import {OnpStatisticsItem} from '@/pages/onp/statistics/data';
import {OnpStatisticsHandles} from '@/pages/onp/statistics/handles';
import OnpStatisticsCreateForm from '@/pages/onp/statistics/create.form';
import {MLOCALES} from '@/common/locales/locale';
import * as moment from 'moment';
import OnpStatisticsUploadForm from "@/pages/onp/statistics/upload.from";
import OnpStatisticsDownloadForm from "@/pages/onp/statistics/download.form";
import OnpStatisticsUpdateForm from "@/pages/onp/statistics/update.form";
import ProDescriptions from '@ant-design/pro-descriptions';
import OnpStatisticsQueryForm from "@/pages/onp/statistics/query.from";
import {PmsContext} from "@/common/pms/pms.context";
import {ONP_STATISTICS_PMS} from "@/permission/onp/onp.statistics.pms";
import OnpNucleusHaplotypeTableList from "@/pages/onp/nucleus/haplotype";
import OnpChloroplastOverallTableList from "@/pages/onp/chloroplast/overall";
import OnpChloroplastHaplotypeTableList from "@/pages/onp/chloroplast/haplotype";


interface OnpStatisticsTableListProps {

}

/**
 * ONP_STATISTICS-列表查询
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpStatisticsTableList: React.FC<OnpStatisticsTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [uploadVisible, handleUploadVisible] = useState<boolean>(false);
  const [queryVisible, handleQueryVisible] = useState<boolean>(false);
  const [downloadVisible, handleDownloadVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<OnpStatisticsItem>();
  const [selectedRowsState, setSelectedRows] = useState<OnpStatisticsItem[]>([]);
  const handles = new OnpStatisticsHandles();
  const intl = useIntl();
  const {TabPane} = Tabs;

  //定义列信息
  const columns: ProColumns<OnpStatisticsItem>[] = [
    {
      dataIndex: 'stId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stBin.label" defaultMessage="Bin"/>,
      dataIndex: 'stBin',
      valueType: 'text',
      colSize: 1,
      sorter: (source, target) => (source.stBin || '').localeCompare(target.stBin || ''),
      fixed: 'left',
      width: 40,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stBin.tip" defaultMessage="Bin"/>
            ),
          },
        ],
      },

      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stChr.label" defaultMessage="Chr"/>,
      dataIndex: 'stChr',
      valueType: 'text',
      colSize: 1,
      sorter: (source, target) => (source.stChr || '').localeCompare(target.stChr || ''),
      width: 40,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stChr.tip" defaultMessage="Chr"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stStart.label" defaultMessage="Start"/>,
      dataIndex: 'stStart',
      valueType: 'text',
      sorter: (source, target) => (source.stStart || '').localeCompare(target.stStart || ''),
      width: 40,
      colSize: 1,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stStart.tip" defaultMessage="Start"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stStop.label" defaultMessage="Stop"/>,
      dataIndex: 'stStop',
      valueType: 'text',
      sorter: (source, target) => (source.stStop || '').localeCompare(target.stStop || ''),
      width: 40,
      colSize: 1,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stStop.tip" defaultMessage="Stop"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stBinSize.label" defaultMessage="Bin_Size(Mb)"/>,
      dataIndex: 'stBinSize',
      valueType: 'text',
      sorter: (source, target) => (source.stBinSize || '').localeCompare(target.stBinSize || ''),
      width: 40,
      colSize: 1,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stBinSize.tip" defaultMessage="Bin"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stDesc.label" defaultMessage="Desc"/>,
      dataIndex: 'stDesc',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stDesc || '').localeCompare(target.stDesc || ''),
      width: 40,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stDesc.tip" defaultMessage="Desc"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stAllMarkers.label" defaultMessage="ALL"/>,
      dataIndex: 'stAllMarkers',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stAllMarkers || '').localeCompare(target.stAllMarkers || ''),
      width: 80,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stAllMarkers.tip" defaultMessage="ALL"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stSnpMarkers.label" defaultMessage="SNP"/>,
      dataIndex: 'stSnpMarkers',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stSnpMarkers || '').localeCompare(target.stSnpMarkers || ''),
      width: 80,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stSnpMarkers.tip" defaultMessage="SNP"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stIndelMarkers.label" defaultMessage="INDEL"/>,
      dataIndex: 'stIndelMarkers',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stIndelMarkers || '').localeCompare(target.stIndelMarkers || ''),
      width: 80,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stIndelMarkers.tip" defaultMessage="INDEL"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stBlockMarkers.label" defaultMessage="Block"/>,
      dataIndex: 'stBlockMarkers',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stBlockMarkers || '').localeCompare(target.stBlockMarkers || ''),
      width: 80,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stBlockMarkers.tip" defaultMessage="Block"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stTags.label" defaultMessage="Tags"/>,
      dataIndex: 'stTags',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stTags || '').localeCompare(target.stTags || ''),
      width: 40,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stTags.tip" defaultMessage="Tags"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stGenotypes.label" defaultMessage="Genotypes"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stGenotypes.tip',
        defaultMessage: 'Genotypes查询',
      })),
      dataIndex: 'stGenotypes',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stGenotypes || '').localeCompare(target.stGenotypes || ''),
      width: 60,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stGenotypes.tip" defaultMessage="Genotypes"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stMaxGenotypesFreq.label" defaultMessage="Max"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stMaxGenotypesFreq.tip',
        defaultMessage: 'Max查询',
      })),
      dataIndex: 'stMaxGenotypesFreq',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stMaxGenotypesFreq || '').localeCompare(target.stMaxGenotypesFreq || ''),
      width: 80,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stMaxGenotypesFreq.tip" defaultMessage="Max"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stMinGenotypesFreq.label" defaultMessage="Min"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stMinGenotypesFreq.tip',
        defaultMessage: 'Min查询',
      })),
      dataIndex: 'stMinGenotypesFreq',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stMinGenotypesFreq || '').localeCompare(target.stMinGenotypesFreq || ''),
      width: 80,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stMinGenotypesFreq.tip" defaultMessage="Min"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stPic.label" defaultMessage="PIC"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stPic.tip',
        defaultMessage: 'PIC查询',
      })),
      dataIndex: 'stPic',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stPic || '').localeCompare(target.stPic || ''),
      width: 40,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stPic.tip" defaultMessage="PIC"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stGeneticMap.label" defaultMessage="Genetic"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stGeneticMap.tip',
        defaultMessage: 'Genetic查询',
      })),
      dataIndex: 'stGeneticMap',
      valueType: 'text', search: false,
      sorter: (source, target) => (source.stGeneticMap || '').localeCompare(target.stGeneticMap || ''),
      width: 120,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stGeneticMap.tip" defaultMessage="Genetic"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stOrder.label" defaultMessage="ORDER"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stOrder.tip',
        defaultMessage: 'ORDER查询',
      })),
      dataIndex: 'stOrder',
      valueType: 'digit',
      hideInForm: true,
      search: false,
      hideInTable: true,
      sorter: (source, target) => ((source.stOrder) - (target.stOrder)),
      width: 120,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stOrder.tip" defaultMessage="ORDER"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stColumn1.label" defaultMessage="Column1"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stColumn1.tip',
        defaultMessage: 'Column1查询',
      })),
      dataIndex: 'stColumn1',
      valueType: 'text', search: false, hideInForm: true, hideInTable: true,
      sorter: (source, target) => (source.stColumn1 || '').localeCompare(target.stColumn1 || ''),
      width: 120,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stColumn1.tip" defaultMessage="Column1"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stColumn2.label" defaultMessage="Column2"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stColumn2.tip',
        defaultMessage: 'Column2查询',
      })),
      dataIndex: 'stColumn2',
      valueType: 'text', search: false, hideInForm: true, hideInTable: true,
      sorter: (source, target) => (source.stColumn2 || '').localeCompare(target.stColumn2 || ''),
      width: 120,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stColumn2.tip" defaultMessage="Column2"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stColumn3.label" defaultMessage="Column3"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stColumn3.tip',
        defaultMessage: 'Column3查询',
      })),
      dataIndex: 'stColumn3',
      valueType: 'text', search: false, hideInForm: true, hideInTable: true,
      sorter: (source, target) => (source.stColumn3 || '').localeCompare(target.stColumn3 || ''),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stColumn3.tip" defaultMessage="Column3"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stSpecies.label" defaultMessage="Species"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stSpecies.tip',
        defaultMessage: 'Species查询',
      })),
      dataIndex: 'stSpecies',
      valueType: 'text', search: false, hideInForm: true, hideInTable: true,
      sorter: (source, target) => (source.stSpecies || '').localeCompare(target.stSpecies || ''),
      width: 150,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stSpecies.tip" defaultMessage="Species"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stRemark.label" defaultMessage="Remark"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stRemark.tip',
        defaultMessage: 'Remark查询',
      })),
      dataIndex: 'stRemark',
      valueType: 'text', search: false, hideInForm: true, hideInTable: true,
      sorter: (source, target) => (source.stRemark || '').localeCompare(target.stRemark || ''),
      width: 120,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stRemark.tip" defaultMessage="Remark"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stCreateDate.label" defaultMessage="创建日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'stCreateDate',
      valueType: 'date', search: false, hideInForm: true, hideInTable: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      colSize: 0.9,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stCreateDate.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.statistics.stUpdateDate.label" defaultMessage="更新日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.onp.statistics.stUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'stUpdateDate',
      valueType: 'date', search: false, hideInForm: true, hideInTable: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.onp.statistics.stUpdateDate.tip" defaultMessage="更新日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.onp.forecast.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      align: 'center',
      width: 140,
      search: false, hideInForm: true, hideInTable: true,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.onp.forecast.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.onp.forecast.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.onp.forecast.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },

  ];

  return (
    <PageContainer>
      <Tabs defaultActiveKey="1" style={{marginBottom: 50, marginTop: 70}}>
        <TabPane tab="Nucleus_ONP_Overall Statistics" key="1">

          <ProTable<OnpStatisticsItem>
            headerTitle={<FormattedMessage id='pages.list.onp.statistics.form.title' defaultMessage='查询'/>}
            actionRef={actionRef}
            rowKey="stId"
            scroll={{x: 3000}}
            search={{
              layout: 'vertical',
              defaultCollapsed: false,
              // span: {
              //   xs: 4,
              //   sm: 4,
              //   md: 4,
              //   lg: 4,
              //   xl: 4,
              //   xxl: 4,
              // },
              collapsed: false,
              labelWidth: 60,
            }}
            toolBarRender={() => [
              // <Button type="primary" onClick={() => handleCreateVisible(true)}>
              //   <PlusOutlined/> <FormattedMessage id="pages.list.onp.statistics.new" defaultMessage="新建"/>
              // </Button>,
              <>
                <Button type="primary" onClick={() => handleDownloadVisible(true)}>
                  <CloudDownloadOutlined/> <FormattedMessage id="pages.download.onp.statistics.new"
                                                             defaultMessage="下载"/>
                </Button>
                {(PmsContext.has(ONP_STATISTICS_PMS.UPLOAD) ? (
                  <Button type="primary" onClick={() => handleUploadVisible(true)}>
                    <PlusOutlined/> <FormattedMessage id="pages.upload.onp.statistics.new" defaultMessage="上传"/>
                  </Button>) : null)}
                <Button type="primary" onClick={() => handleQueryVisible(true)}>
                  <SearchOutlined/><FormattedMessage id="pages.list.onp.statistics.form.title1" defaultMessage="高级查询"/>
                </Button>
              </>
            ]}
            request={async (params, sorter, filter) => {
              const data = await OnpStatisticsService.findPage({...params, sorter, filter});
              console.log('data', data)
              return {
                data: data.data,
                success: true,
                total: data.page.count,
                current: data.page.offset,
                pageSize: data.page.pageSize,
              };
            }
            }
            columns={columns}
            pagination={
              {
                defaultCurrent: 1,
                defaultPageSize: 10,
                showSizeChanger: true,
                showQuickJumper: true,
              }
            }
            rowSelection={
              {
                onChange: (_, selectedRows) => setSelectedRows(selectedRows),
              }
            }

          />

          {selectedRowsState?.length > 0}

          <OnpStatisticsCreateForm onCancel={() => handleCreateVisible(false)}
                                   onSubmit={async (value) => {
                                     const success = await handles.add(value);
                                     if (success) {
                                       handleCreateVisible(false);
                                       if (actionRef.current) {
                                         actionRef.current.reload();
                                       }
                                     }
                                   }
                                   }
                                   visible={createVisible}>
          </OnpStatisticsCreateForm>

          <OnpStatisticsQueryForm onCancel={() => handleQueryVisible(false)}
                                  visible={queryVisible}>
          </OnpStatisticsQueryForm>

          <OnpStatisticsUploadForm onCancel={() => handleUploadVisible(false)}
                                   onSubmit={async (value: Partial<OnpStatisticsItem>) => {
                                     const success = await handles.upload(value);
                                     if (success) {
                                       handleUploadVisible(false);
                                       if (actionRef.current) {
                                         actionRef.current.reload();
                                       }
                                     }
                                   }
                                   }
                                   visible={uploadVisible}>
          </OnpStatisticsUploadForm>


          <OnpStatisticsDownloadForm onCancel={() => handleDownloadVisible(false)}
                                     visible={downloadVisible}>
          </OnpStatisticsDownloadForm>

          {formValues && Object.keys(formValues).length > 0 ? (
            <OnpStatisticsUpdateForm
              onSubmit={async (value: any) => {
                const success = await handles.update(value);
                if (success) {
                  handleUpdateVisible(false);
                  setFormValues({});
                  if (actionRef.current) {
                    actionRef.current.reload();
                  }
                }
              }
              }
              onCancel={() => {
                handleUpdateVisible(false);
                setFormValues({});
              }
              }
              visible={updateVisible}
              values={formValues}
            />
          ) : null}


          <Drawer
            width={600}
            visible={!!row}
            onClose={() => {
              setRow(undefined);
            }
            }
            closable={false}
          >
            {row?.stId && (
              <ProDescriptions<OnpStatisticsItem>
                column={2}
                title={row?.stId}
                request={async () => ({
                  data: row || {},
                })}
                params={
                  {
                    stId: row?.stId,
                  }
                }
                columns={columns}
              />
            )}
          </Drawer>
        </TabPane>

        <TabPane tab="Nucleus_ONP_Haplotype Database" key="2">
          < OnpNucleusHaplotypeTableList/>

        </TabPane>

        <TabPane tab="Chloroplast_ONP_Overall Statistics" key="3">
          < OnpChloroplastOverallTableList/>
        </TabPane>

        <TabPane tab="Chloroplast_ONP_Haplotype Database" key="4">
          < OnpChloroplastHaplotypeTableList/>
        </TabPane>

      </Tabs>
    </PageContainer>
  );
};

export default OnpStatisticsTableList;
