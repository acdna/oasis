import React, {useRef} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {Button, Col, Form, Input, Modal, Row} from 'antd';
import ProForm from '@ant-design/pro-form';
import {AntdIconsEnum} from "@/common/icons/antd.icons.enum";
import {OnpStatisticsService} from "@/services/onp/onp.statistics.service";
import {ActionType} from "@ant-design/pro-table";

/**
 * SYS_CMS_FILES-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface OnpStatisticsQueryProps {
  visible: boolean;
  onCancel: () => void;
}

/**
 * SYS_CMS_FILES-创建记录表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const OnpStatisticsQueryForm: React.FC<OnpStatisticsQueryProps> = (props) => {
  const {visible, onCancel,} = props;
  const intl = useIntl();
  const [form] = Form.useForm();
  const actionRef = useRef<ActionType>();


  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleOk = async () => {
    console.log(form.getFieldsValue())
    let data = form.getFieldsValue();
    const success = await OnpStatisticsService.findPage(data);
    if (success && actionRef.current) {
      actionRef.current.reload();
    }
    return ;

  };


  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Modal
        destroyOnClose
        title={
          <>{AntdIconsEnum.UploadOutlined} <FormattedMessage id='pages.upload.onp.statistics.title'
                                                             defaultMessage='上传信息'/></>
        }

        onCancel={() => handleCancel()}
        onOk={() => handleOk()}

        getContainer={false}
        width={500}
        visible={visible}
        bodyStyle={{paddingBottom: 70}}
        closable={true}
        footer={
          <div
            style={{textAlign: 'right'}}
          >
            <Button type='default' onClick={() => handleCancel()} style={{marginRight: 8}}>
              {AntdIconsEnum.CloseOutlined}
              {intl.formatMessage({id: 'pages.create.onp.statistics.cancel', defaultMessage: '取消'})}
            </Button>
            <Button type="primary" onClick={() => handleOk()}>
              {AntdIconsEnum.UploadOutlined}
              {intl.formatMessage({id: 'pages.list.onp.statistics.form.title', defaultMessage: '查询'})}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form} >

          <Row gutter={8}>

            <Col span={12}>
              <Form.Item
                name="stBin"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBin.label',
                  defaultMessage: 'Bin',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.statistics.stBin.tip',
                      defaultMessage: 'Bin是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.statistics.stBin.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBin.placeholder',
                  defaultMessage: 'Bin',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stChr"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stChr.label',
                  defaultMessage: 'Chr',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.statistics.stChr.tip',
                      defaultMessage: 'Chr是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.statistics.stChr.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stChr.placeholder',
                  defaultMessage: 'Chr',
                })}/>
              </Form.Item>
            </Col>

          </Row>
        </ProForm>
      </Modal>
    </>
  );

};

export default OnpStatisticsQueryForm;
