import { OnpStatisticsItem } from "@/pages/onp/statistics/data";
import React, { useState,useEffect } from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_STATISTICS-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
interface OnpStatisticsUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpStatisticsItem) => void;
  values: any;
}

/**
 * ONP_STATISTICS-更新字典项信息表单
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpStatisticsUpdateForm: React.FC<OnpStatisticsUpdateFormProps> = (props) => {
  const { values, visible, onCancel, onSubmit } = props;
  const [record,setRecord]=useState(values);
  const intl = useIntl();
  const [form] = Form.useForm();

  //设置各控件的初始值
  useEffect(() => {
      form.setFieldsValue({
          stId: record.stId,
          stBin: record.stBin,
          stChr: record.stChr,
          stStart: record.stStart,
          stStop: record.stStop,
          stBinSize: record.stBinSize,
          stDesc: record.stDesc,
          stAllMarkers: record.stAllMarkers,
          stSnpMarkers: record.stSnpMarkers,
          stIndelMarkers: record.stIndelMarkers,
          stBlockMarkers: record.stBlockMarkers,
          stTags: record.stTags,
          stGenotypes: record.stGenotypes,
          stMaxGenotypesFreq: record.stMaxGenotypesFreq,
          stMinGenotypesFreq: record.stMinGenotypesFreq,
          stPic: record.stPic,
          stGeneticMap: record.stGeneticMap,
          stOrder: record.stOrder,
          stColumn1: record.stColumn1,
          stColumn2: record.stColumn2,
          stColumn3: record.stColumn3,
          stSpecies: record.stSpecies,
          stRemark: record.stRemark,
          stCreateDate: record.stCreateDate,
          stUpdateDate: record.stUpdateDate,
          
      });
  });

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...record, ...fieldValues };//合并数据
    setRecord(values);
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.update.onp.statistics.form.title', defaultMessage: '修改' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.update.onp.statistics.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.update.onp.statistics.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stBin"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stBin.label',
                        defaultMessage: 'Bin',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stBin.tip',
                          defaultMessage: 'Bin是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stBin.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stBin.placeholder',
                             defaultMessage: '请输入Bin',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stChr"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stChr.label',
                        defaultMessage: 'Chr',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stChr.tip',
                          defaultMessage: 'Chr是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stChr.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stChr.placeholder',
                             defaultMessage: '请输入Chr',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stStart"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stStart.label',
                        defaultMessage: 'Start',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stStart.tip',
                          defaultMessage: 'Start是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stStart.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stStart.placeholder',
                             defaultMessage: '请输入Start',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stStop"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stStop.label',
                        defaultMessage: 'Stop',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stStop.tip',
                          defaultMessage: 'Stop是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stStop.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stStop.placeholder',
                             defaultMessage: '请输入Stop',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stBinSize"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stBinSize.label',
                        defaultMessage: 'Bin',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stBinSize.tip',
                          defaultMessage: 'Bin是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stBinSize.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stBinSize.placeholder',
                             defaultMessage: '请输入Bin',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stDesc"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stDesc.label',
                        defaultMessage: 'Desc',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stDesc.tip',
                          defaultMessage: 'Desc是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stDesc.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stDesc.placeholder',
                             defaultMessage: '请输入Desc',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stAllMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stAllMarkers.label',
                        defaultMessage: 'ALL',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stAllMarkers.tip',
                          defaultMessage: 'ALL是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stAllMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stAllMarkers.placeholder',
                             defaultMessage: '请输入ALL',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stSnpMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stSnpMarkers.label',
                        defaultMessage: 'SNP',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stSnpMarkers.tip',
                          defaultMessage: 'SNP是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stSnpMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stSnpMarkers.placeholder',
                             defaultMessage: '请输入SNP',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stIndelMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stIndelMarkers.label',
                        defaultMessage: 'INDEL',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stIndelMarkers.tip',
                          defaultMessage: 'INDEL是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stIndelMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stIndelMarkers.placeholder',
                             defaultMessage: '请输入INDEL',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stBlockMarkers"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stBlockMarkers.label',
                        defaultMessage: 'Block',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stBlockMarkers.tip',
                          defaultMessage: 'Block是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stBlockMarkers.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stBlockMarkers.placeholder',
                             defaultMessage: '请输入Block',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stTags"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stTags.label',
                        defaultMessage: 'Tags',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stTags.tip',
                          defaultMessage: 'Tags是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stTags.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stTags.placeholder',
                             defaultMessage: '请输入Tags',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stGenotypes"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stGenotypes.label',
                        defaultMessage: 'Genotypes',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stGenotypes.tip',
                          defaultMessage: 'Genotypes是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stGenotypes.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stGenotypes.placeholder',
                             defaultMessage: '请输入Genotypes',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stMaxGenotypesFreq"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stMaxGenotypesFreq.label',
                        defaultMessage: 'Max',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stMaxGenotypesFreq.tip',
                          defaultMessage: 'Max是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stMaxGenotypesFreq.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stMaxGenotypesFreq.placeholder',
                             defaultMessage: '请输入Max',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stMinGenotypesFreq"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stMinGenotypesFreq.label',
                        defaultMessage: 'Min',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stMinGenotypesFreq.tip',
                          defaultMessage: 'Min是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stMinGenotypesFreq.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stMinGenotypesFreq.placeholder',
                             defaultMessage: '请输入Min',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stPic"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stPic.label',
                        defaultMessage: 'PIC',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stPic.tip',
                          defaultMessage: 'PIC是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stPic.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stPic.placeholder',
                             defaultMessage: '请输入PIC',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stGeneticMap"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stGeneticMap.label',
                        defaultMessage: 'Genetic',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stGeneticMap.tip',
                          defaultMessage: 'Genetic是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stGeneticMap.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stGeneticMap.placeholder',
                             defaultMessage: '请输入Genetic',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stOrder"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stOrder.label',
                        defaultMessage: 'ORDER',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stOrder.tip',
                          defaultMessage: 'ORDER是必填项!',
                        })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stOrder.placeholder',
                             defaultMessage: '请输入ORDER',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stColumn1"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stColumn1.label',
                        defaultMessage: 'Column1',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stColumn1.tip',
                          defaultMessage: 'Column1是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stColumn1.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stColumn1.placeholder',
                             defaultMessage: '请输入Column1',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stColumn2"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stColumn2.label',
                        defaultMessage: 'Column2',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stColumn2.tip',
                          defaultMessage: 'Column2是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stColumn2.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stColumn2.placeholder',
                             defaultMessage: '请输入Column2',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stColumn3"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stColumn3.label',
                        defaultMessage: 'Column3',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stColumn3.tip',
                          defaultMessage: 'Column3是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stColumn3.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stColumn3.placeholder',
                             defaultMessage: '请输入Column3',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="stSpecies"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stSpecies.label',
                        defaultMessage: 'Species',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stSpecies.tip',
                          defaultMessage: 'Species是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stSpecies.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stSpecies.placeholder',
                             defaultMessage: '请输入Species',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="stRemark"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.statistics.stRemark.label',
                        defaultMessage: 'Remark',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.statistics.stRemark.tip',
                          defaultMessage: 'Remark是必填项!',
                        })),
                       },
                       {
                          max:128,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.statistics.stRemark.length.tip',
                              defaultMessage: '最多可输入128个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.statistics.stRemark.placeholder',
                             defaultMessage: '请输入Remark',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpStatisticsUpdateForm;
