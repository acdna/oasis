import { OnpStatisticsService } from '@/services/onp/onp.statistics.service';
import { RuleObject, StoreValue } from "rc-field-form/lib/interface";
import { OnpStatisticsParams } from "@/pages/onp/statistics/data";
/**
 * ONP_STATISTICS记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_STATISTICS_FINDER = {
    /**
     * Bin-字段唯一性验证
     * @param stBin 输入框输入的Bin
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_BIN: async (stBin: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stBin,
        });
        return (res && res.stBin == stBin);
    },
    /**
     * Chr-字段唯一性验证
     * @param stChr 输入框输入的Chr
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_CHR: async (stChr: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stChr,
        });
        return (res && res.stChr == stChr);
    },
    /**
     * Start-字段唯一性验证
     * @param stStart 输入框输入的Start
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_START: async (stStart: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stStart,
        });
        return (res && res.stStart == stStart);
    },
    /**
     * Stop-字段唯一性验证
     * @param stStop 输入框输入的Stop
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_STOP: async (stStop: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stStop,
        });
        return (res && res.stStop == stStop);
    },
    /**
     * Bin-字段唯一性验证
     * @param stBinSize 输入框输入的Bin
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_BIN_SIZE: async (stBinSize: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stBinSize,
        });
        return (res && res.stBinSize == stBinSize);
    },
    /**
     * Desc-字段唯一性验证
     * @param stDesc 输入框输入的Desc
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_DESC: async (stDesc: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stDesc,
        });
        return (res && res.stDesc == stDesc);
    },
    /**
     * ALL-字段唯一性验证
     * @param stAllMarkers 输入框输入的ALL
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_ALL_MARKERS: async (stAllMarkers: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stAllMarkers,
        });
        return (res && res.stAllMarkers == stAllMarkers);
    },
    /**
     * SNP-字段唯一性验证
     * @param stSnpMarkers 输入框输入的SNP
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_SNP_MARKERS: async (stSnpMarkers: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stSnpMarkers,
        });
        return (res && res.stSnpMarkers == stSnpMarkers);
    },
    /**
     * INDEL-字段唯一性验证
     * @param stIndelMarkers 输入框输入的INDEL
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_INDEL_MARKERS: async (stIndelMarkers: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stIndelMarkers,
        });
        return (res && res.stIndelMarkers == stIndelMarkers);
    },
    /**
     * Block-字段唯一性验证
     * @param stBlockMarkers 输入框输入的Block
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_BLOCK_MARKERS: async (stBlockMarkers: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stBlockMarkers,
        });
        return (res && res.stBlockMarkers == stBlockMarkers);
    },
    /**
     * Tags-字段唯一性验证
     * @param stTags 输入框输入的Tags
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_TAGS: async (stTags: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stTags,
        });
        return (res && res.stTags == stTags);
    },
    /**
     * Genotypes-字段唯一性验证
     * @param stGenotypes 输入框输入的Genotypes
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_GENOTYPES: async (stGenotypes: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stGenotypes,
        });
        return (res && res.stGenotypes == stGenotypes);
    },
    /**
     * Max-字段唯一性验证
     * @param stMaxGenotypesFreq 输入框输入的Max
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_MAX_GENOTYPES_FREQ: async (stMaxGenotypesFreq: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stMaxGenotypesFreq,
        });
        return (res && res.stMaxGenotypesFreq == stMaxGenotypesFreq);
    },
    /**
     * Min-字段唯一性验证
     * @param stMinGenotypesFreq 输入框输入的Min
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_MIN_GENOTYPES_FREQ: async (stMinGenotypesFreq: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stMinGenotypesFreq,
        });
        return (res && res.stMinGenotypesFreq == stMinGenotypesFreq);
    },
    /**
     * PIC-字段唯一性验证
     * @param stPic 输入框输入的PIC
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_PIC: async (stPic: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stPic,
        });
        return (res && res.stPic == stPic);
    },
    /**
     * Genetic-字段唯一性验证
     * @param stGeneticMap 输入框输入的Genetic
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_GENETIC_MAP: async (stGeneticMap: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stGeneticMap,
        });
        return (res && res.stGeneticMap == stGeneticMap);
    },
    /**
     * Column1-字段唯一性验证
     * @param stColumn1 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_COLUMN1: async (stColumn1: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stColumn1,
        });
        return (res && res.stColumn1 == stColumn1);
    },
    /**
     * Column2-字段唯一性验证
     * @param stColumn2 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_COLUMN2: async (stColumn2: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stColumn2,
        });
        return (res && res.stColumn2 == stColumn2);
    },
    /**
     * Column3-字段唯一性验证
     * @param stColumn3 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_COLUMN3: async (stColumn3: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stColumn3,
        });
        return (res && res.stColumn3 == stColumn3);
    },
    /**
     * Species-字段唯一性验证
     * @param stSpecies 输入框输入的Species
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_SPECIES: async (stSpecies: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stSpecies,
        });
        return (res && res.stSpecies == stSpecies);
    },
    /**
     * Remark-字段唯一性验证
     * @param stRemark 输入框输入的Remark
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_REMARK: async (stRemark: string, params: Partial < OnpStatisticsParams >= {}) => {
        const res = await OnpStatisticsService.findOne({
            ...params,
            stRemark,
        });
        return (res && res.stRemark == stRemark);
    },
};
/**
 * ONP_STATISTICS验证器
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_STATISTICS_VALIDATOR = {
    /**
     * Bin-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_BIN: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_BIN(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Chr-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_CHR: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_CHR(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Start-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_START: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_START(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Stop-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_STOP: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_STOP(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Bin-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_BIN_SIZE: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_BIN_SIZE(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Desc-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_DESC: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_DESC(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * ALL-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_ALL_MARKERS: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_ALL_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * SNP-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_SNP_MARKERS: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_SNP_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * INDEL-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_INDEL_MARKERS: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_INDEL_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Block-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_BLOCK_MARKERS: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_BLOCK_MARKERS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Tags-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_TAGS: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_TAGS(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Genotypes-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_GENOTYPES: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_GENOTYPES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Max-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_MAX_GENOTYPES_FREQ: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_MAX_GENOTYPES_FREQ(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Min-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_MIN_GENOTYPES_FREQ: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_MIN_GENOTYPES_FREQ(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * PIC-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_PIC: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_PIC(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Genetic-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_GENETIC_MAP: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_GENETIC_MAP(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_COLUMN1: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_COLUMN1(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_COLUMN2: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_COLUMN2(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_COLUMN3: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_COLUMN3(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Species-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_SPECIES: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_SPECIES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Remark-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    ST_REMARK: (error: string, params: Partial < OnpStatisticsParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_STATISTICS_FINDER.ST_REMARK(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
};