import { OnpStatisticsItem } from "@/pages/onp/statistics/data";
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_STATISTICS-创建记录表单的输入参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
interface OnpStatisticsCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpStatisticsItem) => void;
}

/**
 * ONP_STATISTICS-创建记录表单
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpStatisticsCreateForm: React.FC<OnpStatisticsCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { stId,stBin,stChr,stStart,stStop,stBinSize,stDesc,stAllMarkers,stSnpMarkers,stIndelMarkers,stBlockMarkers,stTags,stGenotypes,stMaxGenotypesFreq,stMinGenotypesFreq,stPic,stGeneticMap,stOrder,stColumn1,stColumn2,stColumn3,stSpecies,stRemark,stCreateDate,stUpdateDate }=fieldValues;
    onSubmit({ stId,stBin,stChr,stStart,stStop,stBinSize,stDesc,stAllMarkers,stSnpMarkers,stIndelMarkers,stBlockMarkers,stTags,stGenotypes,stMaxGenotypesFreq,stMinGenotypesFreq,stPic,stGeneticMap,stOrder,stColumn1,stColumn2,stColumn3,stSpecies,stRemark,stCreateDate,stUpdateDate });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.create.onp.statistics.form.title', defaultMessage: '创建' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.create.onp.statistics.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.onp.statistics.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="stBin"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBin.label',
                  defaultMessage: 'Bin',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stBin.tip',
                        defaultMessage: 'Bin是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stBin.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBin.placeholder',
                  defaultMessage: 'Bin',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stChr"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stChr.label',
                  defaultMessage: 'Chr',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stChr.tip',
                        defaultMessage: 'Chr是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stChr.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stChr.placeholder',
                  defaultMessage: 'Chr',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stStart"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stStart.label',
                  defaultMessage: 'Start',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stStart.tip',
                        defaultMessage: 'Start是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stStart.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stStart.placeholder',
                  defaultMessage: 'Start',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stStop"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stStop.label',
                  defaultMessage: 'Stop',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stStop.tip',
                        defaultMessage: 'Stop是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stStop.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stStop.placeholder',
                  defaultMessage: 'Stop',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stBinSize"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBinSize.label',
                  defaultMessage: 'Bin',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stBinSize.tip',
                        defaultMessage: 'Bin是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stBinSize.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBinSize.placeholder',
                  defaultMessage: 'Bin',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stDesc"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stDesc.label',
                  defaultMessage: 'Desc',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stDesc.tip',
                        defaultMessage: 'Desc是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stDesc.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stDesc.placeholder',
                  defaultMessage: 'Desc',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stAllMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stAllMarkers.label',
                  defaultMessage: 'ALL',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stAllMarkers.tip',
                        defaultMessage: 'ALL是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stAllMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stAllMarkers.placeholder',
                  defaultMessage: 'ALL',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stSnpMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stSnpMarkers.label',
                  defaultMessage: 'SNP',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stSnpMarkers.tip',
                        defaultMessage: 'SNP是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stSnpMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stSnpMarkers.placeholder',
                  defaultMessage: 'SNP',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stIndelMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stIndelMarkers.label',
                  defaultMessage: 'INDEL',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stIndelMarkers.tip',
                        defaultMessage: 'INDEL是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stIndelMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stIndelMarkers.placeholder',
                  defaultMessage: 'INDEL',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stBlockMarkers"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBlockMarkers.label',
                  defaultMessage: 'Block',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stBlockMarkers.tip',
                        defaultMessage: 'Block是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stBlockMarkers.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stBlockMarkers.placeholder',
                  defaultMessage: 'Block',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stTags"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stTags.label',
                  defaultMessage: 'Tags',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stTags.tip',
                        defaultMessage: 'Tags是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stTags.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stTags.placeholder',
                  defaultMessage: 'Tags',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stGenotypes"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stGenotypes.label',
                  defaultMessage: 'Genotypes',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stGenotypes.tip',
                        defaultMessage: 'Genotypes是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stGenotypes.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stGenotypes.placeholder',
                  defaultMessage: 'Genotypes',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stMaxGenotypesFreq"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stMaxGenotypesFreq.label',
                  defaultMessage: 'Max',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stMaxGenotypesFreq.tip',
                        defaultMessage: 'Max是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stMaxGenotypesFreq.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stMaxGenotypesFreq.placeholder',
                  defaultMessage: 'Max',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stMinGenotypesFreq"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stMinGenotypesFreq.label',
                  defaultMessage: 'Min',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stMinGenotypesFreq.tip',
                        defaultMessage: 'Min是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stMinGenotypesFreq.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stMinGenotypesFreq.placeholder',
                  defaultMessage: 'Min',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stPic"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stPic.label',
                  defaultMessage: 'PIC',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stPic.tip',
                        defaultMessage: 'PIC是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stPic.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stPic.placeholder',
                  defaultMessage: 'PIC',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stGeneticMap"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stGeneticMap.label',
                  defaultMessage: 'Genetic',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stGeneticMap.tip',
                        defaultMessage: 'Genetic是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stGeneticMap.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stGeneticMap.placeholder',
                  defaultMessage: 'Genetic',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stOrder"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stOrder.label',
                  defaultMessage: 'ORDER',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stOrder.tip',
                        defaultMessage: 'ORDER是必填项!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stOrder.placeholder',
                  defaultMessage: 'ORDER',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stColumn1"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stColumn1.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stColumn1.tip',
                        defaultMessage: 'Column1是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stColumn1.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stColumn1.placeholder',
                  defaultMessage: 'Column1',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stColumn2"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stColumn2.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stColumn2.tip',
                        defaultMessage: 'Column2是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stColumn2.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stColumn2.placeholder',
                  defaultMessage: 'Column2',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stColumn3"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stColumn3.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stColumn3.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stColumn3.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stColumn3.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="stSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stSpecies.label',
                  defaultMessage: 'Species',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stSpecies.tip',
                        defaultMessage: 'Species是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stSpecies.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stSpecies.placeholder',
                  defaultMessage: 'Species',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="stRemark"
                label={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stRemark.label',
                  defaultMessage: 'Remark',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.statistics.stRemark.tip',
                        defaultMessage: 'Remark是必填项!',
                      })),
                    },
                    {
                      max:128,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.statistics.stRemark.length.tip',
                         defaultMessage: '最多可输入128个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.statistics.stRemark.placeholder',
                  defaultMessage: 'Remark',
                })} />
              </Form.Item>
            </Col>

          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpStatisticsCreateForm;
