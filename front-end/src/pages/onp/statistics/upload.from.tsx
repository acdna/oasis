import React, {useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {Button, Col, Form, message, Modal, Row} from 'antd';
import ProForm from '@ant-design/pro-form';
import FileUpload from '@/pages/common/file/file.upload';
import {LocaleContext} from "@/common/locales/locale";
import {ExcelHandles} from "@/pages/common/file/excel.handles";
import {EXCEL_TEMPLATES} from "@/pages/common/file/excel.template.ids";
import {AntdIconsEnum} from "@/common/icons/antd.icons.enum";

/**
 * SYS_CMS_FILES-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface OnpStatisticsUploadFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: { stFilePath: string }) => void;
}

/**
 * SYS_CMS_FILES-创建记录表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const OnpStatisticsUploadForm: React.FC<OnpStatisticsUploadFormProps> = (props) => {
  const {visible, onCancel, onSubmit,} = props;
  const intl = useIntl();
  const [filePath, setFilePath] = useState<string>('');
  const [fileName, setFileName] = useState<string>('');
  const [form] = Form.useForm();
  const excelHandles = new ExcelHandles();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleOk = async () => {
    if (!filePath || filePath.length == 0) {
      message.error(LocaleContext.message({
        id: 'pages.upload.onp.statistics.error',
        defaultMessage: '上传失败，请重新上传！',
      }));
      return;
    }
    onSubmit({
      stFilePath: filePath
    });
    setFilePath("");
  };


  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Modal
        destroyOnClose
        title={
          <>{AntdIconsEnum.UploadOutlined} <FormattedMessage id='pages.upload.onp.statistics.title' defaultMessage='上传信息'/></>
        }

        onCancel={() => handleCancel()}
        onOk={() => handleOk()}

        getContainer={false}
        width={500}
        visible={visible}
        bodyStyle={{paddingBottom: 70}}
        closable={true}
        footer={
          <div
            style={{textAlign: 'right'}}
          >
            <Button type='default' onClick={() => handleCancel()} style={{marginRight: 8}}>
              {AntdIconsEnum.CloseOutlined}
              {intl.formatMessage({id: 'pages.create.onp.statistics.cancel', defaultMessage: '取消'})}
            </Button>
            <Button type='default' onClick={async () => {
              await excelHandles.downloadTemplate(EXCEL_TEMPLATES.ssr_sample);
            }}>
              {AntdIconsEnum.DownloadOutlined}
              {intl.formatMessage({id:"pages.upload.onp.statistics.template",defaultMessage:"下载模板"})}
            </Button>
            <Button type="primary" onClick={() => handleOk()}>
              {AntdIconsEnum.UploadOutlined}
              {intl.formatMessage({id: 'pages.create.onp.statistics.submit', defaultMessage: '提交'})}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="stFilePath"
                label={intl.formatMessage({
                  id: 'pages.upload.onp.statistics.label',
                  defaultMessage: '信息Excel',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.upload.onp.statistics.tip',
                    defaultMessage: '信息文件是必填项!',
                  })),
                }]}
              >
                <FileUpload filePath={filePath} setFilePath={setFilePath} fileName={fileName}
                            setFileName={setFileName} uploadProps={{accept: '.xlsx'}}/>
              </Form.Item>
            </Col>
          </Row>
        </ProForm>
      </Modal>
    </>
  );

};

export default OnpStatisticsUploadForm;
