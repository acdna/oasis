/**
 * ONP_STATISTICS-字段信息
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpStatisticsItem {
  stId: string;
  stBin: string;
  stChr: string;
  stStart: string;
  stStop: string;
  stBinSize: string;
  stDesc: string;
  stAllMarkers: string;
  stSnpMarkers: string;
  stIndelMarkers: string;
  stBlockMarkers: string;
  stTags: string;
  stGenotypes: string;
  stMaxGenotypesFreq: string;
  stMinGenotypesFreq: string;
  stPic: string;
  stGeneticMap: string;
  stOrder: number;
  stColumn1: string;
  stColumn2: string;
  stColumn3: string;
  stSpecies: string;
  stRemark: string;
  stCreateDate: Date;
  stUpdateDate: Date;
  cols?: string[];

  stFilePath?: string;
}

/**
 * ONP_STATISTICS-分页参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpStatisticsPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * ONP_STATISTICS-表格数据
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpStatisticsTableData {
  list: OnpStatisticsItem[];
  pagination: Partial<OnpStatisticsPagination>;
}

/**
 * ONP_STATISTICS-查询参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpStatisticsParams extends OnpStatisticsItem {
  pageSize?: number;
  current?: number;
  pageNum?: number;
  offset?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
  nstId?: string;
  stIdList?: string[];
}
