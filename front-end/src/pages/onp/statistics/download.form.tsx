import React, {useEffect, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {Button, Col, Form, message, Modal, Row} from 'antd';
import ProForm from '@ant-design/pro-form';
import FileUpload from '@/pages/common/file/file.upload';
import {LocaleContext} from "@/common/locales/locale";
import {ExcelHandles} from "@/pages/common/file/excel.handles";
import {EXCEL_TEMPLATES} from "@/pages/common/file/excel.template.ids";
import {AntdIconsEnum} from "@/common/icons/antd.icons.enum";
import {OnpStatisticsHandles} from "@/pages/onp/statistics/handles";

/**
 * SYS_CMS_FILES-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface OnpStatisticsDownloadFormProps {
  visible: boolean;
  onCancel: () => void;
}

/**
 * SYS_CMS_FILES-创建记录表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const OnpStatisticsDownloadForm: React.FC<OnpStatisticsDownloadFormProps> = (props) => {
  const {visible, onCancel} = props;
  const intl = useIntl();
  const [filePath, setFilePath] = useState<string>('');
  const [fileName, setFileName] = useState<string>('');
  const [form] = Form.useForm();
  const excelHandles = new ExcelHandles();
  const handles = new OnpStatisticsHandles();

  useEffect(() => {
    form.setFieldsValue({
      stFilePath: filePath,
    });
  });

  /**
   * 条件 下载按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleDownload = async () => {
    if (!filePath || filePath.length == 0) {
      message.error(LocaleContext.message({
        id: 'pages.upload.onp.statistics.error',
        defaultMessage: '上传失败，请重新上传！',
      }));
      return;
    }
    await handles.downloadByBins({stFilePath: filePath});
    setFilePath("");

  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Modal
        destroyOnClose
        title={
          <>{AntdIconsEnum.UploadOutlined} <FormattedMessage id='pages.upload.onp.statistics.title'
                                                             defaultMessage='上传信息'/></>
        }

        onCancel={() => handleCancel()}
        getContainer={false}
        width={500}
        visible={visible}
        bodyStyle={{paddingBottom: 70}}
        closable={true}
        footer={
          <div
            style={{textAlign: 'right'}}
          >
            <Button type='default' onClick={() => handleCancel()} style={{marginRight: 8}}>
              {AntdIconsEnum.CloseOutlined}
              {intl.formatMessage({id: 'pages.create.onp.statistics.cancel', defaultMessage: '取消'})}
            </Button>
            <Button type='default' onClick={async () => {
              await excelHandles.downloadTemplate(EXCEL_TEMPLATES.onp_statistics);
            }}>
              {AntdIconsEnum.DownloadOutlined}
              {intl.formatMessage({id: "pages.upload.onp.statistics.template", defaultMessage: "下载模板"})}
            </Button>
            <Button type="primary" onClick={() => handleDownload()}>
              {AntdIconsEnum.CloudDownloadOutlined}
              {intl.formatMessage({id: 'pages.download.onp.statistics', defaultMessage: '下载'})}
            </Button>
            <Button type="primary" onClick={async () => {
              await handles.downloadAll();
            }}>
              {AntdIconsEnum.CloudDownloadOutlined}
              {intl.formatMessage({id: 'pages.download.onp.statistics.all', defaultMessage: '下载所有'})}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="stFilePath"
                label={intl.formatMessage({
                  id: 'pages.upload.onp.statistics.label',
                  defaultMessage: '信息Excel',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.upload.onp.statistics.tip',
                    defaultMessage: '信息文件是必填项!',
                  })),
                }]}
              >
                <FileUpload filePath={filePath} setFilePath={setFilePath} fileName={fileName}
                            setFileName={setFileName} uploadProps={{accept: '.xlsx'}}/>
              </Form.Item>
            </Col>
          </Row>
        </ProForm>
      </Modal>
    </>
  );

};

export default OnpStatisticsDownloadForm;
