import { OnpForecastItem } from "@/pages/onp/forecast/data";
import React, { useState,useEffect } from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_FORECAST-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
interface OnpForecastUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpForecastItem) => void;
  values: any;
}

/**
 * ONP_FORECAST-更新字典项信息表单
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpForecastUpdateForm: React.FC<OnpForecastUpdateFormProps> = (props) => {
  const { values, visible, onCancel, onSubmit } = props;
  const [record,setRecord]=useState(values);
  const intl = useIntl();
  const [form] = Form.useForm();

  //设置各控件的初始值
  useEffect(() => {
      form.setFieldsValue({
          foId: record.foId,
          foOrder: record.foOrder,
          foColumn1: record.foColumn1,
          foColumn2: record.foColumn2,
          foColumn3: record.foColumn3,
          foColumn4: record.foColumn4,
          foColumn5: record.foColumn5,
          foColumn6: record.foColumn6,
          foSpecies: record.foSpecies,
          foRemark: record.foRemark,
          foCreateDate: record.foCreateDate,
          foUpdateDate: record.foUpdateDate,
          
      });
  });

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...record, ...fieldValues };//合并数据
    setRecord(values);
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.update.onp.forecast.form.title', defaultMessage: '修改' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.update.onp.forecast.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.update.onp.forecast.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="foOrder"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foOrder.label',
                        defaultMessage: 'ID',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foOrder.tip',
                          defaultMessage: 'ID是必填项!',
                        })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foOrder.placeholder',
                             defaultMessage: '请输入ID',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="foColumn1"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foColumn1.label',
                        defaultMessage: 'Column1',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foColumn1.tip',
                          defaultMessage: 'Column1是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foColumn1.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foColumn1.placeholder',
                             defaultMessage: '请输入Column1',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="foColumn2"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foColumn2.label',
                        defaultMessage: 'Column2',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foColumn2.tip',
                          defaultMessage: 'Column2是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foColumn2.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foColumn2.placeholder',
                             defaultMessage: '请输入Column2',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="foColumn3"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foColumn3.label',
                        defaultMessage: 'Column3',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foColumn3.tip',
                          defaultMessage: 'Column3是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foColumn3.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foColumn3.placeholder',
                             defaultMessage: '请输入Column3',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="foColumn4"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foColumn4.label',
                        defaultMessage: 'Column1',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foColumn4.tip',
                          defaultMessage: 'Column1是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foColumn4.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foColumn4.placeholder',
                             defaultMessage: '请输入Column1',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="foColumn5"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foColumn5.label',
                        defaultMessage: 'Column2',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foColumn5.tip',
                          defaultMessage: 'Column2是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foColumn5.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foColumn5.placeholder',
                             defaultMessage: '请输入Column2',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="foColumn6"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foColumn6.label',
                        defaultMessage: 'Column3',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foColumn6.tip',
                          defaultMessage: 'Column3是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foColumn6.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foColumn6.placeholder',
                             defaultMessage: '请输入Column3',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="foSpecies"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foSpecies.label',
                        defaultMessage: 'Species',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foSpecies.tip',
                          defaultMessage: 'Species是必填项!',
                        })),
                       },
                       {
                          max:255,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foSpecies.length.tip',
                              defaultMessage: '最多可输入255个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foSpecies.placeholder',
                             defaultMessage: '请输入Species',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="foRemark"
                      label={intl.formatMessage({
                        id: 'pages.update.onp.forecast.foRemark.label',
                        defaultMessage: 'Remark',
                      })}
                      rules={[
                       {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.update.onp.forecast.foRemark.tip',
                          defaultMessage: 'Remark是必填项!',
                        })),
                       },
                       {
                          max:128,
                          message:(intl.formatMessage({
                              id: 'pages.update.onp.forecast.foRemark.length.tip',
                              defaultMessage: '最多可输入128个字符!',
                          })),
                       },
                      ]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.update.onp.forecast.foRemark.placeholder',
                             defaultMessage: '请输入Remark',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpForecastUpdateForm;
