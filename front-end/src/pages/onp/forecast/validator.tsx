import { OnpForecastService } from '@/services/onp/onp.forecast.service';
import { RuleObject, StoreValue } from "rc-field-form/lib/interface";
import { OnpForecastParams } from "@/pages/onp/forecast/data";
/**
 * ONP_FORECAST记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_FORECAST_FINDER = {
    /**
     * Column1-字段唯一性验证
     * @param foColumn1 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN1: async (foColumn1: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foColumn1,
        });
        return (res && res.foColumn1 == foColumn1);
    },
    /**
     * Column2-字段唯一性验证
     * @param foColumn2 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN2: async (foColumn2: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foColumn2,
        });
        return (res && res.foColumn2 == foColumn2);
    },
    /**
     * Column3-字段唯一性验证
     * @param foColumn3 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN3: async (foColumn3: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foColumn3,
        });
        return (res && res.foColumn3 == foColumn3);
    },
    /**
     * Column1-字段唯一性验证
     * @param foColumn4 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN4: async (foColumn4: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foColumn4,
        });
        return (res && res.foColumn4 == foColumn4);
    },
    /**
     * Column2-字段唯一性验证
     * @param foColumn5 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN5: async (foColumn5: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foColumn5,
        });
        return (res && res.foColumn5 == foColumn5);
    },
    /**
     * Column3-字段唯一性验证
     * @param foColumn6 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN6: async (foColumn6: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foColumn6,
        });
        return (res && res.foColumn6 == foColumn6);
    },
    /**
     * Species-字段唯一性验证
     * @param foSpecies 输入框输入的Species
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_SPECIES: async (foSpecies: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foSpecies,
        });
        return (res && res.foSpecies == foSpecies);
    },
    /**
     * Remark-字段唯一性验证
     * @param foRemark 输入框输入的Remark
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_REMARK: async (foRemark: string, params: Partial < OnpForecastParams >= {}) => {
        const res = await OnpForecastService.findOne({
            ...params,
            foRemark,
        });
        return (res && res.foRemark == foRemark);
    },
};
/**
 * ONP_FORECAST验证器
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_FORECAST_VALIDATOR = {
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN1: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_COLUMN1(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN2: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_COLUMN2(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN3: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_COLUMN3(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN4: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_COLUMN4(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN5: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_COLUMN5(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_COLUMN6: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_COLUMN6(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Species-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_SPECIES: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_SPECIES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Remark-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    FO_REMARK: (error: string, params: Partial < OnpForecastParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_FORECAST_FINDER.FO_REMARK(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
};