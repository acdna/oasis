import {OnpForecastItem} from "@/pages/onp/forecast/data";
import React from 'react';
import {useIntl} from 'umi';
import {Button, Col, Drawer, Form, Input, Row} from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_FORECAST-创建记录表单的输入参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
interface OnpForecastCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpForecastItem) => void;
}

/**
 * ONP_FORECAST-创建记录表单
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpForecastCreateForm: React.FC<OnpForecastCreateFormProps> = (props) => {
  const {visible, onCancel, onSubmit} = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {
      foId,
      foOrder,
      foColumn1,
      foColumn2,
      foColumn3,
      foColumn4,
      foColumn5,
      foColumn6,
      foSpecies,
      foRemark,
      foCreateDate,
      foUpdateDate
    } = fieldValues;
    onSubmit({
      foId,
      foOrder,
      foColumn1,
      foColumn2,
      foColumn3,
      foColumn4,
      foColumn5,
      foColumn6,
      foSpecies,
      foRemark,
      foCreateDate,
      foUpdateDate
    });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({id: 'pages.create.onp.forecast.form.title', defaultMessage: '创建'})}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{paddingBottom: 80}}
        closable={true}
        footer={
          <div
            style={{textAlign: 'right',}}
          >
            <Button onClick={() => handleCancel()} style={{marginRight: 8}}>
              {intl.formatMessage({id: 'pages.create.onp.forecast.cancel', defaultMessage: '取消'})}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({id: 'pages.create.onp.forecast.submit', defaultMessage: '提交'})}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="foOrder"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foOrder.label',
                  defaultMessage: 'ID',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foOrder.tip',
                      defaultMessage: 'ID是必填项!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foOrder.placeholder',
                  defaultMessage: 'ID',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="foColumn1"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn1.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn1.tip',
                      defaultMessage: 'Column1是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn1.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn1.placeholder',
                  defaultMessage: 'Column1',
                })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="foColumn2"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn2.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn2.tip',
                      defaultMessage: 'Column2是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn2.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn2.placeholder',
                  defaultMessage: 'Column2',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="foColumn3"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn3.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn3.tip',
                      defaultMessage: 'Column3是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn3.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn3.placeholder',
                  defaultMessage: 'Column3',
                })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="foColumn4"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn4.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn4.tip',
                      defaultMessage: 'Column1是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn4.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn4.placeholder',
                  defaultMessage: 'Column1',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="foColumn5"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn5.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn5.tip',
                      defaultMessage: 'Column2是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn5.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn5.placeholder',
                  defaultMessage: 'Column2',
                })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="foColumn6"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn6.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn6.tip',
                      defaultMessage: 'Column3是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn6.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foColumn6.placeholder',
                  defaultMessage: 'Column3',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="foSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foSpecies.label',
                  defaultMessage: 'Species',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foSpecies.tip',
                      defaultMessage: 'Species是必填项!',
                    })),
                  },
                  {
                    max: 255,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foSpecies.length.tip',
                      defaultMessage: '最多可输入255个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foSpecies.placeholder',
                  defaultMessage: 'Species',
                })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="foRemark"
                label={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foRemark.label',
                  defaultMessage: 'Remark',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foRemark.tip',
                      defaultMessage: 'Remark是必填项!',
                    })),
                  },
                  {
                    max: 128,
                    message: (intl.formatMessage({
                      id: 'pages.create.onp.forecast.foRemark.length.tip',
                      defaultMessage: '最多可输入128个字符!',
                    })),
                  },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.forecast.foRemark.placeholder',
                  defaultMessage: 'Remark',
                })}/>
              </Form.Item>
            </Col>

          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpForecastCreateForm;
