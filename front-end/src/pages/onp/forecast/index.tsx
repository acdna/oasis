import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { Dispatch,FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { OnpForecastService } from '@/services/onp/onp.forecast.service';
import { OnpForecastItem } from '@/pages/onp/forecast/data';
import { OnpForecastHandles } from '@/pages/onp/forecast/handles';
import OnpForecastCreateForm from '@/pages/onp/forecast/create.form';
import OnpForecastUpdateForm from '@/pages/onp/forecast/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

interface OnpForecastTableListProps {
    dispatch: Dispatch;
}

/**
 * ONP_FORECAST-列表查询
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpForecastTableList: React.FC<OnpForecastTableListProps> = (props) => {
    const [createVisible, handleCreateVisible] = useState<boolean>(false);
    const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<OnpForecastItem>();
    const [selectedRowsState, setSelectedRows] = useState<OnpForecastItem[]>([]);
    const handles = new OnpForecastHandles();
    //const { dispatch } = props;
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<OnpForecastItem>[] = [
        {
            dataIndex: 'foId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foOrder.label" defaultMessage="ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foOrder.tip',
                defaultMessage: 'ID查询',
            })),
            dataIndex: 'foOrder',
            valueType:'digit',hideInForm: true,
             sorter: (source, target) => ((source.foOrder)-(target.foOrder)), 
            fixed: 'left', 
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foOrder.tip" defaultMessage="ID" />
                        ),
                    },
                ],
            },
            
            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foColumn1.label" defaultMessage="Column1" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foColumn1.tip',
                defaultMessage: 'Column1查询',
            })),
            dataIndex: 'foColumn1',
            valueType:'text',
             sorter: (source, target) => (source.foColumn1||'').localeCompare(target.foColumn1||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foColumn1.tip" defaultMessage="Column1" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foColumn2.label" defaultMessage="Column2" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foColumn2.tip',
                defaultMessage: 'Column2查询',
            })),
            dataIndex: 'foColumn2',
            valueType:'text',
             sorter: (source, target) => (source.foColumn2||'').localeCompare(target.foColumn2||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foColumn2.tip" defaultMessage="Column2" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foColumn3.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foColumn3.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'foColumn3',
            valueType:'text',
             sorter: (source, target) => (source.foColumn3||'').localeCompare(target.foColumn3||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foColumn3.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foColumn4.label" defaultMessage="Column1" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foColumn4.tip',
                defaultMessage: 'Column1查询',
            })),
            dataIndex: 'foColumn4',
            valueType:'text',
             sorter: (source, target) => (source.foColumn4||'').localeCompare(target.foColumn4||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foColumn4.tip" defaultMessage="Column1" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foColumn5.label" defaultMessage="Column2" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foColumn5.tip',
                defaultMessage: 'Column2查询',
            })),
            dataIndex: 'foColumn5',
            valueType:'text',
             sorter: (source, target) => (source.foColumn5||'').localeCompare(target.foColumn5||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foColumn5.tip" defaultMessage="Column2" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foColumn6.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foColumn6.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'foColumn6',
            valueType:'text',
             sorter: (source, target) => (source.foColumn6||'').localeCompare(target.foColumn6||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foColumn6.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foSpecies.label" defaultMessage="Species" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foSpecies.tip',
                defaultMessage: 'Species查询',
            })),
            dataIndex: 'foSpecies',
            valueType:'text',
             sorter: (source, target) => (source.foSpecies||'').localeCompare(target.foSpecies||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foSpecies.tip" defaultMessage="Species" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foRemark.label" defaultMessage="Remark" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foRemark.tip',
                defaultMessage: 'Remark查询',
            })),
            dataIndex: 'foRemark',
            valueType:'text',
             sorter: (source, target) => (source.foRemark||'').localeCompare(target.foRemark||''), 
            
            
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foRemark.tip" defaultMessage="Remark" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'foCreateDate',
            valueType:'date',search : false,hideInForm: true,
            
            
            render: (text:any) => moment(text).format(MLOCALES.pattern),
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.foUpdateDate.label" defaultMessage="更新日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.forecast.foUpdateDate.tip',
                defaultMessage: '更新日期查询',
            })),
            dataIndex: 'foUpdateDate',
            valueType:'date',search : false,hideInForm: true,
            
            
            render: (text:any) => moment(text).format(MLOCALES.pattern),
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.forecast.foUpdateDate.tip" defaultMessage="更新日期" />
                        ),
                    },
                ],
            },
            
        },
        
        {
            title: <FormattedMessage id="pages.list.onp.forecast.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            align:'center',
            width: 140,
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.onp.forecast.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.onp.forecast.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.onp.forecast.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<OnpForecastItem>
                headerTitle={<FormattedMessage id='pages.list.onp.forecast.form.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="foId"
                scroll={ { x: 1300 } }
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.onp.forecast.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await OnpForecastService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.onp.forecast.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.onp.forecast.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.onp.forecast.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <OnpForecastCreateForm onCancel={() => handleCreateVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                    visible={createVisible}>
            </OnpForecastCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <OnpForecastUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateVisible(false);
                        setFormValues({});
                      }
                    }
                    visible={updateVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.foId && (
                    <ProDescriptions<OnpForecastItem>
                        column={2}
                        title={row?.foId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            foId: row?.foId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default OnpForecastTableList;