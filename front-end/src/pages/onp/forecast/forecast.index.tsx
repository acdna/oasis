import React, {useEffect, useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {Button, Card, Col, Form, Input, message, Modal, Progress, Row} from 'antd';
import ProForm from '@ant-design/pro-form';
import FileUpload from '@/pages/common/file/file.upload';
import {LocaleContext} from "@/common/locales/locale";
import {OnpForecastItem} from "@/pages/onp/forecast/data";
import {PageContainer} from "@ant-design/pro-layout";
import {uuid} from "@/common/utils/uuid.utils";
import {OnpForecastHandles} from "@/pages/onp/forecast/handles";
import {ActionType} from "@ant-design/pro-table";


/**
 * SYS_CMS_FILES-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface ForecastUploadFormProps {

}

/**
 * SYS_CMS_FILES-创建记录表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const ForecastUploadForm: React.FC<ForecastUploadFormProps> = (props) => {
  const intl = useIntl();
  const [filePath, setFilePath] = useState<string>('');
  const [recordId, setRecordId] = useState<string>('');
  const [fileName, setFileName] = useState<string>('');
  const [isModalVisible, setIsModalVisible] = useState(true);
  const [buttonName, setButtonName] = useState<string>('COMPARISON');
  const [percent, setState] = useState<number>(0);
  const [loadings, setStateLoading] = useState<boolean>(false);
  const [disabled, setdisabled] = useState<boolean>(true);
  const [form] = Form.useForm();
  const handles = new OnpForecastHandles();
  const actionRef = useRef<ActionType>();

  //关联设置文件名和文件路径
  useEffect(() => {
    form.setFieldsValue({
      foFilePath: filePath,
    });
  }, [filePath]);

  //
  const onSubmit = async (fields: OnpForecastItem) => {
    const success = await handles.add(fields);
    if (success) {
      if (actionRef.current) {
        actionRef.current.reload();
      }
    }
    let percenta = percent + 50;
    if (percenta > 100) {
      percenta = 100;
    }
    setState(percenta);
    setdisabled(false);
  };

  /**
   * 点击提交按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    if (!filePath || filePath.length == 0) {
      message.error(LocaleContext.message({
        id: 'pages.upload.onp.forecast.title',
        defaultMessage: '上传失败，请重新上传！',
      }));
      return;
    }
    let rid = uuid();
    setRecordId(rid);
    const {
      foColumn1,
      foColumn2,
      foColumn3,
      foColumn4,
      foColumn5,
      foColumn6,
      foSpecies,
      foCreateDate,
      foUpdateDate,
      foOrder,
      foRemark,
    } = fieldValues;
    await onSubmit({
      foId: rid,
      foColumn1,
      foColumn2,
      foColumn3,
      foColumn4,
      foColumn5,
      foColumn6,
      foSpecies,
      foCreateDate,
      foUpdateDate,
      foOrder,
      foRemark,
      foFilePath: filePath,
    });
  };

  //协议对话框
  const handleModaOk = () => {
    setIsModalVisible(false);
  };
  //协议对话框
  const handleModaCancel = () => {
    setIsModalVisible(false);
    window.location.href = '/oasis';
  };

  //按钮加载中
  const enterLoading = async () => {
    setStateLoading(true);
    setButtonName('processing...');
    setTimeout(() => {
      setStateLoading(false);
      setdisabled(true);
      setButtonName('OVER');

    }, 3000);

    // setButtonName('processing...');
    await handles.comparison(recordId, filePath);
    let percenta = percent + 50;
    if (percenta > 100) {
      percenta = 100;
    }
    setState(percenta);

  };

  return (
    <>

      <Row justify="space-around" align="middle">
        <Col span={18}>
          <PageContainer
            style={{marginTop: 60}}
            title={
              <FormattedMessage id='pages.information.onp.forecast.title' defaultMessage='信息'/>
            }
          >
            <ProForm layout="vertical" submitter={false} form={form}>
              <Row>
                <Col span={10} >
                  <Form.Item
                    name="foColumn1"
                    label={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn1.label',
                      defaultMessage: '姓名',
                    })}
                    rules={[
                      {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.onp.forecast.foColumn1.tip',
                          defaultMessage: '姓名是必填项!',
                        })),
                      },
                    ]}
                  >
                    <Input
                      style={{ width: '90%' }}
                      placeholder={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn1.placeholder',
                      defaultMessage: '姓名',
                    })}/>
                  </Form.Item>
                </Col>
                <Col span={10} >
                  <Form.Item
                    name="foColumn2"
                    label={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn2.label',
                      defaultMessage: '单位',
                    })}
                    rules={[
                      {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.onp.forecast.foColumn2.tip',
                          defaultMessage: '单位是必填项!',
                        })),
                      },
                    ]}
                  >
                    <Input
                      style={{ width: '90%' }}
                      placeholder={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn2.placeholder',
                      defaultMessage: '单位',
                    })}/>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={10} >
                  <Form.Item
                    name="foColumn3"
                    label={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn3.label',
                      defaultMessage: '电话',
                    })}
                    rules={[
                      {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.onp.forecast.foColumn3.tip',
                          defaultMessage: '电话是必填项!',
                        })),
                      },
                    ]}
                  >
                    <Input
                      style={{width: '90%'}}
                      placeholder={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn3.placeholder',
                      defaultMessage: '电话',
                    })}/>
                  </Form.Item>
                </Col>
                <Col span={10} >
                  <Form.Item
                    name="foColumn4"
                    label={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn4.label',
                      defaultMessage: '邮箱',
                    })}
                    rules={[
                      {
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.onp.forecast.foColumn4.tip',
                          defaultMessage: '邮箱是必填项!',
                        })),
                      },
                      //^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$
                      {
                        pattern: new RegExp(/^[a-z0-9A-Z-_]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/, "g"),
                        message: '邮箱格式不正确！'
                      },
                    ]}
                  >
                    <Input
                      style={{ width: '90%' }}
                      placeholder={intl.formatMessage({
                      id: 'pages.create.onp.forecast.foColumn4.placeholder',
                      defaultMessage: '邮箱',
                    })}/>
                  </Form.Item>
                </Col>
              </Row>
              <Row >
                <Col span={8}>
                  <Form.Item
                    name="foFilePath"
                    label={intl.formatMessage({
                      id: 'pages.upload.onp.forecast.label',
                      defaultMessage: 'STEP -Upload file',
                    })}
                    rules={[{
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.upload.onp.forecast.tip',
                        defaultMessage: '文件是必填项!',
                      })),
                    }]}
                  >
                    <FileUpload filePath={filePath}
                                setFilePath={setFilePath}
                                fileName={fileName}
                                setFileName={setFileName}
                    />
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item
                    name="SubmitFile"
                    label={intl.formatMessage({
                      id: 'pages.submit.onp.forecast.title',
                      defaultMessage: '提交',
                    })}
                  >
                    <Button onClick={() => handleOk()} type="primary">
                      {intl.formatMessage({id: 'pages.submit.onp.forecast.title', defaultMessage: '提交'})}
                    </Button>
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item
                    name="Convert"
                    label={intl.formatMessage({
                      id: 'pages.comparison.onp.forecast.title',
                      defaultMessage: 'STEP - Comparison',
                    })}
                  >
                    <Button
                      type="primary"
                      loading={loadings}
                      onClick={() => enterLoading()}
                      disabled={disabled}
                    >
                      {buttonName}
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={20}>
                  <div>
                    <Progress percent={percent}
                              strokeColor={{'30%': '#108ee9', '100%': '#87d068',}}
                    />
                  </div>
                </Col>
              </Row>
              <br/>
              <br/>
            </ProForm>
          </PageContainer>
        </Col>

        <Col span={6}>
          <Card style={{right: 70}}>
            <h1 style={{color:"red"}}>结果:</h1>
            {/*<p>123</p>*/}
            {/*<p>123</p>*/}
          </Card>
        </Col>
      </Row>
      <Modal title={<FormattedMessage id='pages.agreement.onp.forecast.title' defaultMessage='协议'/>}
             visible={isModalVisible}
             onOk={() => handleModaOk()}
             onCancel={() => handleModaCancel()}
             okText={<FormattedMessage id='pages.agree.onp.forecast.title' defaultMessage='同意'/>}
             cancelText={<FormattedMessage id='pages.disagree.onp.forecast.title' defaultMessage='不同意'/>}
             closable={false}
             maskClosable={false}

      >
        <p>Some contents...</p>
      </Modal>
    </>
  );
};

export default ForecastUploadForm;
