import { OnpForecastItem } from '@/pages/onp/forecast/data';
import { message } from 'antd';
import { OnpForecastService } from '@/services/onp/onp.forecast.service';
import { LocaleContext } from '@/common/locales/locale';
/**
 * ONP_FORECAST-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export class OnpForecastHandles {
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: OnpForecastItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.forecast.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await OnpForecastService.add({ ...fields });
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.forecast.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.forecast.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: OnpForecastItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.forecast.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await OnpForecastService.update(fields);
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.forecast.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.forecast.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: OnpForecastItem[]) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.forecast.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await OnpForecastService.deleteByIds(selectedRows.map(row => row.foId));
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.forecast.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.forecast.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: OnpForecastItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.forecast.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await OnpForecastService.deleteById(record.foId);
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.forecast.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.forecast.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };

  /**
   * @Description: 比对
   * @author zhengenze
   * @date 2021/4/9 4:18 下午
   **/
  comparison = async (id:string,filePath:string) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.onp.statistics..upload.loading',
      defaultMessage: '正在上传',
    }));
    try {
      await OnpForecastService.comparison(id,filePath);
      hide();
      message.success(LocaleContext.message({
        id: 'page.onp.statistics.upload.success',
        defaultMessage: '上传成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.onp.statistics.upload.error',
        defaultMessage: '上传失败，请重试!',
      }));
      return false;
    }
  };

};
