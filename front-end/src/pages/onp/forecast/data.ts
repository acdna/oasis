/**
 * ONP_FORECAST-字段信息
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpForecastItem {
    foId: string;
    foOrder: number;
    foColumn1: string;
    foColumn2: string;
    foColumn3: string;
    foColumn4: string;
    foColumn5: string;
    foColumn6: string;
    foSpecies: string;
    foRemark: string;
    foCreateDate: Date;
    foUpdateDate: Date;
    cols? : string[];

    foFilePath? : string;
}
/**
 * ONP_FORECAST-分页参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpForecastPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * ONP_FORECAST-表格数据
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpForecastTableData {
    list: OnpForecastItem[];
    pagination: Partial < OnpForecastPagination > ;
}
/**
 * ONP_FORECAST-查询参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpForecastParams extends OnpForecastItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
    nfoId ? : string;
    foIdList ? : string[];
}
