import { OnpVerificationService } from '@/services/onp/onp.verification.service';
import { RuleObject, StoreValue } from "rc-field-form/lib/interface";
import { OnpVerificationParams } from "@/pages/onp/verification/data";
/**
 * ONP_VERIFICATION记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_VERIFICATION_FINDER = {
    /**
     * Column1-字段唯一性验证
     * @param veColumn1 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN1: async (veColumn1: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn1,
        });
        return (res && res.veColumn1 == veColumn1);
    },
    /**
     * Column2-字段唯一性验证
     * @param veColumn2 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN2: async (veColumn2: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn2,
        });
        return (res && res.veColumn2 == veColumn2);
    },
    /**
     * Column3-字段唯一性验证
     * @param veColumn3 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN3: async (veColumn3: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn3,
        });
        return (res && res.veColumn3 == veColumn3);
    },
    /**
     * Column1-字段唯一性验证
     * @param veColumn4 输入框输入的Column1
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN4: async (veColumn4: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn4,
        });
        return (res && res.veColumn4 == veColumn4);
    },
    /**
     * Column2-字段唯一性验证
     * @param veColumn5 输入框输入的Column2
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN5: async (veColumn5: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn5,
        });
        return (res && res.veColumn5 == veColumn5);
    },
    /**
     * Column3-字段唯一性验证
     * @param veColumn6 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN6: async (veColumn6: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn6,
        });
        return (res && res.veColumn6 == veColumn6);
    },
    /**
     * Column3-字段唯一性验证
     * @param veColumn7 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN7: async (veColumn7: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn7,
        });
        return (res && res.veColumn7 == veColumn7);
    },
    /**
     * Column3-字段唯一性验证
     * @param veColumn8 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN8: async (veColumn8: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn8,
        });
        return (res && res.veColumn8 == veColumn8);
    },
    /**
     * Column3-字段唯一性验证
     * @param veColumn9 输入框输入的Column3
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN9: async (veColumn9: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veColumn9,
        });
        return (res && res.veColumn9 == veColumn9);
    },
    /**
     * Species-字段唯一性验证
     * @param veSpecies 输入框输入的Species
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_SPECIES: async (veSpecies: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veSpecies,
        });
        return (res && res.veSpecies == veSpecies);
    },
    /**
     * Remark-字段唯一性验证
     * @param veRemark 输入框输入的Remark
     * @param params 其它额外参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_REMARK: async (veRemark: string, params: Partial < OnpVerificationParams >= {}) => {
        const res = await OnpVerificationService.findOne({
            ...params,
            veRemark,
        });
        return (res && res.veRemark == veRemark);
    },
};
/**
 * ONP_VERIFICATION验证器
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const ONP_VERIFICATION_VALIDATOR = {
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN1: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN1(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN2: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN2(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN3: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN3(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column1-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN4: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN4(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column2-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN5: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN5(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN6: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN6(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN7: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN7(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN8: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN8(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Column3-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_COLUMN9: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_COLUMN9(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Species-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_SPECIES: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_SPECIES(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
    /**
     * Remark-字段唯一性验证
     * @param error 若数据存在重复时显示的错误提示消息
     * @param params 其它参数
     * @author: jiangbin
     * @date: 4/6/2021, 10:21:58 AM
     **/
    VE_REMARK: (error: string, params: Partial < OnpVerificationParams >= {}) => {
        return {
            validator: async (rule: RuleObject, value: StoreValue, callback: (error ? : string) => void) => {
                const res = await ONP_VERIFICATION_FINDER.VE_REMARK(value, params);
                return res ? Promise.reject(error) : Promise.resolve();
            },
        }
    },
};