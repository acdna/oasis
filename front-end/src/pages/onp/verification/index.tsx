import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { OnpVerificationService } from '@/services/onp/onp.verification.service';
import { OnpVerificationItem } from '@/pages/onp/verification/data';
import { OnpVerificationHandles } from '@/pages/onp/verification/handles';
import OnpVerificationCreateForm from '@/pages/onp/verification/create.form';
import OnpVerificationUpdateForm from '@/pages/onp/verification/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

interface OnpVerificationTableListProps {

}

/**
 * ONP_VERIFICATION-列表查询
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpVerificationTableList: React.FC<OnpVerificationTableListProps> = (props) => {
    const [createVisible, handleCreateVisible] = useState<boolean>(false);
    const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<OnpVerificationItem>();
    const [selectedRowsState, setSelectedRows] = useState<OnpVerificationItem[]>([]);
    const handles = new OnpVerificationHandles();
    //const { dispatch } = props;
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<OnpVerificationItem>[] = [
        {
            dataIndex: 'veId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veOrder.label" defaultMessage="ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veOrder.tip',
                defaultMessage: 'ID查询',
            })),
            dataIndex: 'veOrder',
            valueType:'digit',hideInForm: true,
             sorter: (source, target) => ((source.veOrder)-(target.veOrder)),
            fixed: 'left',

            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veOrder.tip" defaultMessage="ID" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn1.label" defaultMessage="Column1" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn1.tip',
                defaultMessage: 'Column1查询',
            })),
            dataIndex: 'veColumn1',
            valueType:'text',
             sorter: (source, target) => (source.veColumn1||'').localeCompare(target.veColumn1||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn1.tip" defaultMessage="Column1" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn2.label" defaultMessage="Column2" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn2.tip',
                defaultMessage: 'Column2查询',
            })),
            dataIndex: 'veColumn2',
            valueType:'text',
             sorter: (source, target) => (source.veColumn2||'').localeCompare(target.veColumn2||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn2.tip" defaultMessage="Column2" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn3.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn3.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'veColumn3',
            valueType:'text',
             sorter: (source, target) => (source.veColumn3||'').localeCompare(target.veColumn3||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn3.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn4.label" defaultMessage="Column1" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn4.tip',
                defaultMessage: 'Column1查询',
            })),
            dataIndex: 'veColumn4',
            valueType:'text',
             sorter: (source, target) => (source.veColumn4||'').localeCompare(target.veColumn4||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn4.tip" defaultMessage="Column1" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn5.label" defaultMessage="Column2" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn5.tip',
                defaultMessage: 'Column2查询',
            })),
            dataIndex: 'veColumn5',
            valueType:'text',
             sorter: (source, target) => (source.veColumn5||'').localeCompare(target.veColumn5||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn5.tip" defaultMessage="Column2" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn6.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn6.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'veColumn6',
            valueType:'text',
             sorter: (source, target) => (source.veColumn6||'').localeCompare(target.veColumn6||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn6.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn7.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn7.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'veColumn7',
            valueType:'text',
             sorter: (source, target) => (source.veColumn7||'').localeCompare(target.veColumn7||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn7.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn8.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn8.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'veColumn8',
            valueType:'text',
             sorter: (source, target) => (source.veColumn8||'').localeCompare(target.veColumn8||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn8.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veColumn9.label" defaultMessage="Column3" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veColumn9.tip',
                defaultMessage: 'Column3查询',
            })),
            dataIndex: 'veColumn9',
            valueType:'text',
             sorter: (source, target) => (source.veColumn9||'').localeCompare(target.veColumn9||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veColumn9.tip" defaultMessage="Column3" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veSpecies.label" defaultMessage="Species" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veSpecies.tip',
                defaultMessage: 'Species查询',
            })),
            dataIndex: 'veSpecies',
            valueType:'text',
             sorter: (source, target) => (source.veSpecies||'').localeCompare(target.veSpecies||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veSpecies.tip" defaultMessage="Species" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veRemark.label" defaultMessage="Remark" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veRemark.tip',
                defaultMessage: 'Remark查询',
            })),
            dataIndex: 'veRemark',
            valueType:'text',
             sorter: (source, target) => (source.veRemark||'').localeCompare(target.veRemark||''),


            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veRemark.tip" defaultMessage="Remark" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'veCreateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.veUpdateDate.label" defaultMessage="更新日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.onp.verification.veUpdateDate.tip',
                defaultMessage: '更新日期查询',
            })),
            dataIndex: 'veUpdateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            width: 120,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.onp.verification.veUpdateDate.tip" defaultMessage="更新日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.onp.verification.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            align:'center',
            width: 140,
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.onp.verification.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.onp.verification.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.onp.verification.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<OnpVerificationItem>
                headerTitle={<FormattedMessage id='pages.list.onp.verification.form.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="veId"
                scroll={ { x: 1300 } }
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.onp.verification.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await OnpVerificationService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.onp.verification.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.onp.verification.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.onp.verification.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <OnpVerificationCreateForm onCancel={() => handleCreateVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                    visible={createVisible}>
            </OnpVerificationCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <OnpVerificationUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateVisible(false);
                        setFormValues({});
                      }
                    }
                    visible={updateVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.veId && (
                    <ProDescriptions<OnpVerificationItem>
                        column={2}
                        title={row?.veId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            veId: row?.veId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default OnpVerificationTableList;
