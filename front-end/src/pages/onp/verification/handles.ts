import { OnpVerificationItem } from '@/pages/onp/verification/data';
import { message } from 'antd';
import { OnpVerificationService } from '@/services/onp/onp.verification.service';
import { LocaleContext } from '@/common/locales/locale';
/**
 * ONP_VERIFICATION-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export class OnpVerificationHandles {
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: OnpVerificationItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.verification.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await OnpVerificationService.add({ ...fields });
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.verification.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.verification.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: OnpVerificationItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.verification.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await OnpVerificationService.update(fields);
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.verification.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.verification.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: OnpVerificationItem[]) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.verification.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await OnpVerificationService.deleteByIds(selectedRows.map(row => row.veId));
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.verification.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.verification.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: OnpVerificationItem) => {
        const hide = message.loading(LocaleContext.message({
            id: 'page.onp.verification.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await OnpVerificationService.deleteById(record.veId);
            hide();
            message.success(LocaleContext.message({
                id: 'page.onp.verification.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(LocaleContext.message({
                id: 'page.onp.verification.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};