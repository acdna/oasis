import { OnpVerificationItem } from "@/pages/onp/verification/data";
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * ONP_VERIFICATION-创建记录表单的输入参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
interface OnpVerificationCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: OnpVerificationItem) => void;
}

/**
 * ONP_VERIFICATION-创建记录表单
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpVerificationCreateForm: React.FC<OnpVerificationCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { veId,veOrder,veColumn1,veColumn2,veColumn3,veColumn4,veColumn5,veColumn6,veColumn7,veColumn8,veColumn9,veSpecies,veRemark,veCreateDate,veUpdateDate }=fieldValues;
    onSubmit({ veId,veOrder,veColumn1,veColumn2,veColumn3,veColumn4,veColumn5,veColumn6,veColumn7,veColumn8,veColumn9,veSpecies,veRemark,veCreateDate,veUpdateDate });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 4/6/2021, 10:21:58 AM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        getContainer={false}
        title={intl.formatMessage({ id: 'pages.create.onp.verification.form.title', defaultMessage: '创建' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.create.onp.verification.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.onp.verification.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="veOrder"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veOrder.label',
                  defaultMessage: 'ID',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veOrder.tip',
                        defaultMessage: 'ID是必填项!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veOrder.placeholder',
                  defaultMessage: 'ID',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="veColumn1"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn1.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn1.tip',
                        defaultMessage: 'Column1是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn1.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn1.placeholder',
                  defaultMessage: 'Column1',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="veColumn2"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn2.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn2.tip',
                        defaultMessage: 'Column2是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn2.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn2.placeholder',
                  defaultMessage: 'Column2',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="veColumn3"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn3.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn3.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn3.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn3.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="veColumn4"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn4.label',
                  defaultMessage: 'Column1',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn4.tip',
                        defaultMessage: 'Column1是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn4.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn4.placeholder',
                  defaultMessage: 'Column1',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="veColumn5"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn5.label',
                  defaultMessage: 'Column2',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn5.tip',
                        defaultMessage: 'Column2是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn5.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn5.placeholder',
                  defaultMessage: 'Column2',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="veColumn6"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn6.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn6.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn6.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn6.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="veColumn7"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn7.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn7.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn7.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn7.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="veColumn8"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn8.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn8.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn8.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn8.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="veColumn9"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn9.label',
                  defaultMessage: 'Column3',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veColumn9.tip',
                        defaultMessage: 'Column3是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veColumn9.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veColumn9.placeholder',
                  defaultMessage: 'Column3',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                name="veSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veSpecies.label',
                  defaultMessage: 'Species',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veSpecies.tip',
                        defaultMessage: 'Species是必填项!',
                      })),
                    },
                    {
                      max:255,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veSpecies.length.tip',
                         defaultMessage: '最多可输入255个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veSpecies.placeholder',
                  defaultMessage: 'Species',
                })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                name="veRemark"
                label={intl.formatMessage({
                  id: 'pages.create.onp.verification.veRemark.label',
                  defaultMessage: 'Remark',
                })}
                rules={[
                    {
                      required: true,
                      message: (intl.formatMessage({
                        id: 'pages.create.onp.verification.veRemark.tip',
                        defaultMessage: 'Remark是必填项!',
                      })),
                    },
                    {
                      max:128,
                      message:(intl.formatMessage({
                         id: 'pages.create.onp.verification.veRemark.length.tip',
                         defaultMessage: '最多可输入128个字符!',
                      })),
                    },
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.onp.verification.veRemark.placeholder',
                  defaultMessage: 'Remark',
                })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default OnpVerificationCreateForm;
