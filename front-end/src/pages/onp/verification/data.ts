/**
 * ONP_VERIFICATION-字段信息
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpVerificationItem {
    veId: string;
    veOrder: number;
    veColumn1: string;
    veColumn2: string;
    veColumn3: string;
    veColumn4: string;
    veColumn5: string;
    veColumn6: string;
    veColumn7: string;
    veColumn8: string;
    veColumn9: string;
    veSpecies: string;
    veRemark: string;
    veCreateDate: Date;
    veUpdateDate: Date;
    cols? : string[];

    veFilePath? : string;
}
/**
 * ONP_VERIFICATION-分页参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpVerificationPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * ONP_VERIFICATION-表格数据
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpVerificationTableData {
    list: OnpVerificationItem[];
    pagination: Partial < OnpVerificationPagination > ;
}
/**
 * ONP_VERIFICATION-查询参数
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 */
export interface OnpVerificationParams extends OnpVerificationItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
    nveId ? : string;
    veIdList ? : string[];
}
