import React from 'react';
import {Button, Card, Col, Row} from 'antd';
import { DownOutlined } from '@ant-design/icons';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';

class Banner extends React.PureComponent {
  render() {
    const { ...currentProps } = this.props;
    const { dataSource } = currentProps;
    delete currentProps.dataSource;
    delete currentProps.isMobile;
    return (
      <div {...currentProps} {...dataSource.wrapper}>
        <QueueAnim
          key="QueueAnim"
          type={['bottom', 'top']}
          delay={200}
          {...dataSource.textWrapper}
        >
          {/*<div>*/}
          {/*  <Row gutter={16}>*/}

          {/*    <Col span={12}>*/}
          {/*<Card title="Default size card" extra={<a href="#">More</a>} style={{ width: 300,background:'#ffffff26',}}>*/}
          {/*  <p>Card content</p>*/}
          {/*  <p>Card content</p>*/}
          {/*  <p>Card content</p>*/}
          {/*</Card>*/}
          {/*    </Col>*/}
          {/*    <Col span={12}>*/}
          {/*<Card title="Default size card" extra={<a href="#">More</a>} style={{ width: 300 }}>*/}
          {/*  <p>Card content</p>*/}
          {/*  <p>Card content</p>*/}
          {/*  <p>Card content</p>*/}
          {/*</Card>*/}

          {/*    </Col>*/}
          {/*  </Row>*/}
          {/*</div>*/}

          <div key="content" {...dataSource.content}>
            {dataSource.content.children}
          </div>
          <Button ghost key="button" {...dataSource.button}>
            {dataSource.button.children}
          </Button>






        </QueueAnim>
        <TweenOne
          animation={{
            y: '-=20',
            yoyo: true,
            repeat: 10,
            duration: 1000,
          }}
          className="banner0-icon"
          key="icon"
        >
          <DownOutlined style={{width:'100%'}}/>
        </TweenOne>
      </div>
    );
  }
}
export default Banner;
