/* eslint no-undef: 0 */
/* eslint arrow-parens: 0 */
import React from 'react';
import {enquireScreen} from 'enquire-js';
import Banner0 from './Banner0';
import Content1 from './Content1';

import {
  Banner00DataSource,
  Content10DataSource,
  Feature21DataSource,
  Footer11DataSource,
} from './data.source';
import './less/antMotionStyle.less';
import Footer1 from "@/pages/Home/Footer1";
import Feature2 from "@/pages/Home/Feature2";

let isMobile;
enquireScreen((b) => {
  isMobile = b;
});

const {location = {}} = typeof window !== 'undefined' ? window : {};

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile,
      show: !location.port, // 如果不是 dva 2.0 请删除
    };
  }

  componentDidMount() {
    // 适配手机屏幕;
    enquireScreen((b) => {
      this.setState({isMobile: !!b});
    });
    // dva 2.0 样式在组件渲染之后动态加载，导致滚动组件不生效；线上不影响；
    /* 如果不是 dva 2.0 请删除 start */
    if (location.port) {
      // 样式 build 时间在 200-300ms 之间;
      setTimeout(() => {
        this.setState({
          show: true,
        });
      }, 500);
    }
    /* 如果不是 dva 2.0 请删除 end */
  }

  render() {
    const children = [

      <Banner0
        id="Banner0_0"
        key="Banner0_0"
        dataSource={Banner00DataSource}

      />,

      <Content1
        id="Content1_0"
        key="Content1_0"
        dataSource={Content10DataSource}

      />,
      <Feature2
        id="Feature2_1"
        key="Feature2_1"
        dataSource={Feature21DataSource}

      />,
      <Footer1
        id="Footer1_1"
        key="Footer1_1"
        dataSource={Footer11DataSource}

      />,
    ];
    return (
      <div
        className="templates-wrapper"
        ref={(d) => {
          this.dom = d;
        }}
      >
        {/* 如果不是 dva 2.0 替换成 {children} start */}
        {this.state.show && children}
        {/* 如果不是 dva 2.0 替换成 {children} end */}
      </div>
    );
  }
}
