import { SysHelpItem } from '@/pages/sys/help/data';
import { message } from 'antd';
import { SysHelpService } from '@/services/sys/sys.help.service';
import { useIntl } from 'umi';
/**
 * SYS_HELP-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export class SysHelpHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysHelpItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.help.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysHelpService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.help.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.help.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysHelpItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.help.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysHelpService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.help.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.help.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysHelpItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.help.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysHelpService.deleteByIds(selectedRows.map(row => row.helpId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.help.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.help.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysHelpItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.help.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysHelpService.deleteById(record.helpId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.help.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.help.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};