/**
 * SYS_HELP-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysHelpItem {
    helpId: string;
    helpQuestion: string;
    helpAnswer: string;
    helpParentId: string;
    helpType: string;
    helpPath: string;
    helpSort: string;
    helpCreateDate: Date;
    helpUpdateDate: Date;
}
/**
 * SYS_HELP-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysHelpPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_HELP-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysHelpTableData {
    list: SysHelpItem[];
    pagination: Partial < SysHelpPagination > ;
}
/**
 * SYS_HELP-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysHelpParams extends SysHelpItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}