import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysHelpItem } from "@/pages/sys/help/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_HELP-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysHelpCreateFormProps {
    onSubmit: (fields: SysHelpItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_HELP-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysHelpCreateForm: React.FC<SysHelpCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:27 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { helpId,helpQuestion,helpAnswer,helpParentId,helpType,helpPath,helpSort,helpCreateDate,helpUpdateDate }=fieldValues;
        onSubmit({ helpId,helpQuestion,helpAnswer,helpParentId,helpType,helpPath,helpSort,helpCreateDate,helpUpdateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.help.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="helpId"
                        label={<FormattedMessage id="pages.create.sys.help.helpId.label" defaultMessage="帮助编号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpId.tip" defaultMessage="帮助编号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpId.placeholder",defaultMessage:"帮助编号"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpQuestion"
                        label={<FormattedMessage id="pages.create.sys.help.helpQuestion.label" defaultMessage="帮助问题"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpQuestion.tip" defaultMessage="帮助问题为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpQuestion.placeholder",defaultMessage:"帮助问题"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpAnswer"
                        label={<FormattedMessage id="pages.create.sys.help.helpAnswer.label" defaultMessage="帮助回答"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpAnswer.tip" defaultMessage="帮助回答为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpAnswer.placeholder",defaultMessage:"帮助回答"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpParentId"
                        label={<FormattedMessage id="pages.create.sys.help.helpParentId.label" defaultMessage="问题编号父级编号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpParentId.tip" defaultMessage="问题编号父级编号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpParentId.placeholder",defaultMessage:"问题编号父级编号"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpType"
                        label={<FormattedMessage id="pages.create.sys.help.helpType.label" defaultMessage="帮助类别"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpType.tip" defaultMessage="帮助类别为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpType.placeholder",defaultMessage:"帮助类别"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpPath"
                        label={<FormattedMessage id="pages.create.sys.help.helpPath.label" defaultMessage="帮助匹配路径"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpPath.tip" defaultMessage="帮助匹配路径为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpPath.placeholder",defaultMessage:"帮助匹配路径"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpSort"
                        label={<FormattedMessage id="pages.create.sys.help.helpSort.label" defaultMessage="排序"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpSort.tip" defaultMessage="排序为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpSort.placeholder",defaultMessage:"排序"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpCreateDate"
                        label={<FormattedMessage id="pages.create.sys.help.helpCreateDate.label" defaultMessage="创建日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpCreateDate.tip" defaultMessage="创建日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpCreateDate.placeholder",defaultMessage:"创建日期"})}/>
                </Form.Item>
                
                <Form.Item
                        name="helpUpdateDate"
                        label={<FormattedMessage id="pages.create.sys.help.helpUpdateDate.label" defaultMessage="更新日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.help.helpUpdateDate.tip" defaultMessage="更新日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.help.helpUpdateDate.placeholder",defaultMessage:"更新日期"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysHelpCreateForm;
