import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysHelpService } from '@/services/sys/sys.help.service';
import { SysHelpItem } from '@/pages/sys/help/data';
import { SysHelpHandles } from '@/pages/sys/help/handles';
import SysHelpCreateForm from '@/pages/sys/help/create.form';
import SysHelpUpdateForm from '@/pages/sys/help/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_HELP-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysHelpTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysHelpItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysHelpItem[]>([]);
    const handles = new SysHelpHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysHelpItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.help.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'helpId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpId.label" defaultMessage="帮助编号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpId.tip',
                defaultMessage: '帮助编号查询',
            })),
            dataIndex: 'helpId',
            valueType:'text',
             sorter: (source, target) => (source.helpId||'').localeCompare(target.helpId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpId.tip" defaultMessage="帮助编号" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpQuestion.label" defaultMessage="帮助问题" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpQuestion.tip',
                defaultMessage: '帮助问题查询',
            })),
            dataIndex: 'helpQuestion',
            valueType:'text',
             sorter: (source, target) => (source.helpQuestion||'').localeCompare(target.helpQuestion||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpQuestion.tip" defaultMessage="帮助问题" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpAnswer.label" defaultMessage="帮助回答" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpAnswer.tip',
                defaultMessage: '帮助回答查询',
            })),
            dataIndex: 'helpAnswer',
            valueType:'text',
             sorter: (source, target) => (source.helpAnswer||'').localeCompare(target.helpAnswer||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpAnswer.tip" defaultMessage="帮助回答" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpParentId.label" defaultMessage="问题编号父级编号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpParentId.tip',
                defaultMessage: '问题编号父级编号查询',
            })),
            dataIndex: 'helpParentId',
            valueType:'text',
             sorter: (source, target) => (source.helpParentId||'').localeCompare(target.helpParentId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpParentId.tip" defaultMessage="问题编号父级编号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpType.label" defaultMessage="帮助类别" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpType.tip',
                defaultMessage: '帮助类别查询',
            })),
            dataIndex: 'helpType',
            valueType:'text',
             sorter: (source, target) => (source.helpType||'').localeCompare(target.helpType||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpType.tip" defaultMessage="帮助类别" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpPath.label" defaultMessage="帮助匹配路径" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpPath.tip',
                defaultMessage: '帮助匹配路径查询',
            })),
            dataIndex: 'helpPath',
            valueType:'text',
             sorter: (source, target) => (source.helpPath||'').localeCompare(target.helpPath||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpPath.tip" defaultMessage="帮助匹配路径" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpSort.label" defaultMessage="排序" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpSort.tip',
                defaultMessage: '排序查询',
            })),
            dataIndex: 'helpSort',
            valueType:'text',
             sorter: (source, target) => (source.helpSort||'').localeCompare(target.helpSort||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpSort.tip" defaultMessage="排序" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'helpCreateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.helpUpdateDate.label" defaultMessage="更新日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.help.helpUpdateDate.tip',
                defaultMessage: '更新日期查询',
            })),
            dataIndex: 'helpUpdateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.help.helpUpdateDate.tip" defaultMessage="更新日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.help.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.help.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.help.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.help.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysHelpItem>
                headerTitle={<FormattedMessage id='pages.list.sys.help.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="helpId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.help.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysHelpService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.help.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.help.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.help.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysHelpCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysHelpCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysHelpUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.helpId && (
                    <ProDescriptions<SysHelpItem>
                        column={2}
                        title={row?.helpId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            helpId: row?.helpId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysHelpTableList;
