import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysHelpItem } from "@/pages/sys/help/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_HELP-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysHelpUpdateFormProps {
    onSubmit: (fields: SysHelpItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_HELP-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysHelpUpdateForm: React.FC<SysHelpUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        helpId: values.helpId,
        helpQuestion: values.helpQuestion,
        helpAnswer: values.helpAnswer,
        helpParentId: values.helpParentId,
        helpType: values.helpType,
        helpPath: values.helpPath,
        helpSort: values.helpSort,
        helpCreateDate: values.helpCreateDate,
        helpUpdateDate: values.helpUpdateDate,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const renderContent = () => (
        <>
            <Input name='helpId' value={formVals.helpId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="helpId"
                label={<FormattedMessage id='pages.update.sys.help.helpId.label' defaultMessage='帮助编号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpId.tip' defaultMessage='请输入帮助编号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpId.tip',
                    defaultMessage: '请输入帮助编号！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpQuestion"
                label={<FormattedMessage id='pages.update.sys.help.helpQuestion.label' defaultMessage='帮助问题'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpQuestion.tip' defaultMessage='请输入帮助问题！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpQuestion.tip',
                    defaultMessage: '请输入帮助问题！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpAnswer"
                label={<FormattedMessage id='pages.update.sys.help.helpAnswer.label' defaultMessage='帮助回答'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpAnswer.tip' defaultMessage='请输入帮助回答！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpAnswer.tip',
                    defaultMessage: '请输入帮助回答！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpParentId"
                label={<FormattedMessage id='pages.update.sys.help.helpParentId.label' defaultMessage='问题编号父级编号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpParentId.tip' defaultMessage='请输入问题编号父级编号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpParentId.tip',
                    defaultMessage: '请输入问题编号父级编号！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpType"
                label={<FormattedMessage id='pages.update.sys.help.helpType.label' defaultMessage='帮助类别'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpType.tip' defaultMessage='请输入帮助类别！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpType.tip',
                    defaultMessage: '请输入帮助类别！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpPath"
                label={<FormattedMessage id='pages.update.sys.help.helpPath.label' defaultMessage='帮助匹配路径'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpPath.tip' defaultMessage='请输入帮助匹配路径！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpPath.tip',
                    defaultMessage: '请输入帮助匹配路径！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpSort"
                label={<FormattedMessage id='pages.update.sys.help.helpSort.label' defaultMessage='排序'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpSort.tip' defaultMessage='请输入排序！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpSort.tip',
                    defaultMessage: '请输入排序！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpCreateDate"
                label={<FormattedMessage id='pages.update.sys.help.helpCreateDate.label' defaultMessage='创建日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpCreateDate.tip' defaultMessage='请输入创建日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpCreateDate.tip',
                    defaultMessage: '请输入创建日期！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="helpUpdateDate"
                label={<FormattedMessage id='pages.update.sys.help.helpUpdateDate.label' defaultMessage='更新日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.help.helpUpdateDate.tip' defaultMessage='请输入更新日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.help.update.helpUpdateDate.tip',
                    defaultMessage: '请输入更新日期！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.help.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     helpId: formVals.helpId,
                     helpQuestion: formVals.helpQuestion,
                     helpAnswer: formVals.helpAnswer,
                     helpParentId: formVals.helpParentId,
                     helpType: formVals.helpType,
                     helpPath: formVals.helpPath,
                     helpSort: formVals.helpSort,
                     helpCreateDate: formVals.helpCreateDate,
                     helpUpdateDate: formVals.helpUpdateDate,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysHelpUpdateForm;