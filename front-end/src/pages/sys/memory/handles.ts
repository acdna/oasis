import { SysMemoryItem } from '@/pages/sys/memory/data';
import { message } from 'antd';
import { SysMemoryService } from '@/services/sys/sys.memory.service';
import { useIntl } from 'umi';
/**
 * SYS_MEMORY-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
export class SysMemoryHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysMemoryItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.memory.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysMemoryService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.memory.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.memory.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysMemoryItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.memory.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysMemoryService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.memory.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.memory.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysMemoryItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.memory.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysMemoryService.deleteByIds(selectedRows.map(row => row.memId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.memory.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.memory.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysMemoryItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.memory.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysMemoryService.deleteById(record.memId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.memory.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.memory.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};