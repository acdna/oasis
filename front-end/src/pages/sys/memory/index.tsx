import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysMemoryService } from '@/services/sys/sys.memory.service';
import { SysMemoryItem } from '@/pages/sys/memory/data';
import { SysMemoryHandles } from '@/pages/sys/memory/handles';
import SysMemoryCreateForm from '@/pages/sys/memory/create.form';
import SysMemoryUpdateForm from '@/pages/sys/memory/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_MEMORY-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysMemoryTableList: React.FC<{}> = () => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysMemoryItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysMemoryItem[]>([]);
  const handles = new SysMemoryHandles();
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<SysMemoryItem>[] = [
    {
      dataIndex: 'memId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memTotal.label" defaultMessage="总内存" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memTotal.tip',
        defaultMessage: '总内存查询',
      })),
      dataIndex: 'memTotal',
      valueType: 'digit', hideInForm: true,
      sorter: (source, target) => ((source.memTotal) - (target.memTotal)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memTotal.tip" defaultMessage="总内存" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memFree.label" defaultMessage="可用内存" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memFree.tip',
        defaultMessage: '可用内存查询',
      })),
      dataIndex: 'memFree',
      valueType: 'digit', hideInForm: true,
      sorter: (source, target) => ((source.memFree) - (target.memFree)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memFree.tip" defaultMessage="可用内存" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memUsed.label" defaultMessage="已使用内存" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memUsed.tip',
        defaultMessage: '已使用内存查询',
      })),
      dataIndex: 'memUsed',
      valueType: 'digit', hideInForm: true,
      sorter: (source, target) => ((source.memUsed) - (target.memUsed)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memUsed.tip" defaultMessage="已使用内存" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memIsWarning.label" defaultMessage="内存用量是否预警" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memIsWarning.tip',
        defaultMessage: '内存用量是否预警查询',
      })),
      dataIndex: 'memIsWarning',
      valueType: 'text',
      sorter: (source, target) => (source.memIsWarning || '').localeCompare(target.memIsWarning || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memIsWarning.tip" defaultMessage="内存用量是否预警" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memMax.label" defaultMessage="最大内存" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memMax.tip',
        defaultMessage: '最大内存查询',
      })),
      dataIndex: 'memMax',
      valueType: 'digit',
      hideInForm: true,
      sorter: (source, target) => ((source.memMax) - (target.memMax)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memMax.tip" defaultMessage="最大内存" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memCreateDate.label" defaultMessage="创建日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'memCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memCreateDate.tip" defaultMessage="创建日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.memUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.memory.memUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'memUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.memory.memUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.memory.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.memory.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.memory.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.memory.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysMemoryItem>
        headerTitle={<FormattedMessage id='pages.list.sys.memory.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="memId"
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.memory.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysMemoryService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.memory.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.memory.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.memory.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysMemoryCreateForm onCancel={() => handleCreateModalVisible(false)}
                           onSubmit={async (value) => {
                             const success = await handles.add(value);
                             if (success) {
                               handleCreateModalVisible(false);
                               if (actionRef.current) {
                                 actionRef.current.reload();
                               }
                             }
                           }
                           }
                           modalVisible={createModalVisible}>
      </SysMemoryCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysMemoryUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.memId && (
          <ProDescriptions<SysMemoryItem>
            column={2}
            title={row?.memId}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                memId: row?.memId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default SysMemoryTableList;
