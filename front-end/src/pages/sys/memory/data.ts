/**
 * SYS_MEMORY-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMemoryItem {
    memId: string;
    memTotal: number;
    memFree: number;
    memUsed: number;
    memIsWarning: string;
    memMax: number;
    memCreateDate: Date;
    memUpdateDate: Date;
}
/**
 * SYS_MEMORY-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMemoryPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_MEMORY-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMemoryTableData {
    list: SysMemoryItem[];
    pagination: Partial < SysMemoryPagination > ;
}
/**
 * SYS_MEMORY-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMemoryParams extends SysMemoryItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}