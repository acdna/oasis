import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysMemoryItem } from "@/pages/sys/memory/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_MEMORY-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysMemoryUpdateFormProps {
    onSubmit: (fields: SysMemoryItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_MEMORY-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysMemoryUpdateForm: React.FC<SysMemoryUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        memId: values.memId,
        memTotal: values.memTotal,
        memFree: values.memFree,
        memUsed: values.memUsed,
        memIsWarning: values.memIsWarning,
        memMax: values.memMax,
        memCreateDate: values.memCreateDate,
        memUpdateDate: values.memUpdateDate,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>

            <Form.Item
                name="memTotal"
                label={<FormattedMessage id='pages.update.sys.memory.memTotal.label' defaultMessage='总内存'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.memory.memTotal.tip' defaultMessage='请输入总内存！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.memory.update.memTotal.tip',
                    defaultMessage: '请输入总内存！'
                })}/>
            </Form.Item>

            <Form.Item
                name="memFree"
                label={<FormattedMessage id='pages.update.sys.memory.memFree.label' defaultMessage='可用内存'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.memory.memFree.tip' defaultMessage='请输入可用内存！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.memory.update.memFree.tip',
                    defaultMessage: '请输入可用内存！'
                })}/>
            </Form.Item>

            <Form.Item
                name="memUsed"
                label={<FormattedMessage id='pages.update.sys.memory.memUsed.label' defaultMessage='已使用内存'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.memory.memUsed.tip' defaultMessage='请输入已使用内存！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.memory.update.memUsed.tip',
                    defaultMessage: '请输入已使用内存！'
                })}/>
            </Form.Item>

            <Form.Item
                name="memIsWarning"
                label={<FormattedMessage id='pages.update.sys.memory.memIsWarning.label' defaultMessage='内存用量是否预警'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.memory.memIsWarning.tip' defaultMessage='请输入内存用量是否预警！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.memory.update.memIsWarning.tip',
                    defaultMessage: '请输入内存用量是否预警！'
                })}/>
            </Form.Item>

            <Form.Item
                name="memMax"
                label={<FormattedMessage id='pages.update.sys.memory.memMax.label' defaultMessage='最大内存'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.memory.memMax.tip' defaultMessage='请输入最大内存！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.memory.update.memMax.tip',
                    defaultMessage: '请输入最大内存！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.memory.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     memId: formVals.memId,
                     memTotal: formVals.memTotal,
                     memFree: formVals.memFree,
                     memUsed: formVals.memUsed,
                     memIsWarning: formVals.memIsWarning,
                     memMax: formVals.memMax,
                     memCreateDate: formVals.memCreateDate,
                     memUpdateDate: formVals.memUpdateDate,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysMemoryUpdateForm;
