import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysMemoryItem } from "@/pages/sys/memory/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_MEMORY-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysMemoryCreateFormProps {
    onSubmit: (fields: SysMemoryItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_MEMORY-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysMemoryCreateForm: React.FC<SysMemoryCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { memId,memTotal,memFree,memUsed,memIsWarning,memMax,memCreateDate,memUpdateDate }=fieldValues;
        onSubmit({ memId,memTotal,memFree,memUsed,memIsWarning,memMax,memCreateDate,memUpdateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.memory.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="memTotal"
                        label={<FormattedMessage id="pages.create.sys.memory.memTotal.label" defaultMessage="总内存"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.memory.memTotal.tip" defaultMessage="总内存为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.memory.memTotal.placeholder",defaultMessage:"总内存"})}/>
                </Form.Item>

                <Form.Item
                        name="memFree"
                        label={<FormattedMessage id="pages.create.sys.memory.memFree.label" defaultMessage="可用内存"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.memory.memFree.tip" defaultMessage="可用内存为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.memory.memFree.placeholder",defaultMessage:"可用内存"})}/>
                </Form.Item>

                <Form.Item
                        name="memUsed"
                        label={<FormattedMessage id="pages.create.sys.memory.memUsed.label" defaultMessage="已使用内存"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.memory.memUsed.tip" defaultMessage="已使用内存为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.memory.memUsed.placeholder",defaultMessage:"已使用内存"})}/>
                </Form.Item>

                <Form.Item
                        name="memIsWarning"
                        label={<FormattedMessage id="pages.create.sys.memory.memIsWarning.label" defaultMessage="内存用量是否预警"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.memory.memIsWarning.tip" defaultMessage="内存用量是否预警为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.memory.memIsWarning.placeholder",defaultMessage:"内存用量是否预警"})}/>
                </Form.Item>

                <Form.Item
                        name="memMax"
                        label={<FormattedMessage id="pages.create.sys.memory.memMax.label" defaultMessage="最大内存"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.memory.memMax.tip" defaultMessage="最大内存为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.memory.memMax.placeholder",defaultMessage:"最大内存"})}/>
                </Form.Item>
            </ProForm>
        </Modal>
    );
};

export default SysMemoryCreateForm;
