import { SysPermissionItem } from '@/pages/sys/permission/data';
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, InputNumber, Row, Select } from 'antd';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options } from '@/components/common/select.options';
import { SYS_ENABLE_STATE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_PERMISSION-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/1/2021, 7:17:26 PM
 **/
interface SysPermissionCreateItemFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysPermissionItem) => void;
  dictMap: DictMapper;
  parent: any;
}

/**
 * SYS_PERMISSION-创建记录表单
 * @author jiangbin
 * @date 1/1/2021, 7:17:26 PM
 **/
const SysPermissionCreateItemForm: React.FC<SysPermissionCreateItemFormProps> = (props) => {
  const { visible, onCancel, onSubmit, dictMap, parent } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 1/1/2021, 7:17:26 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { perId, perName, perNameEn, perUrl, perMethod, perOrder, perRemark, perState, perSystem, perModule } = fieldValues;
    onSubmit({
      perId,
      perName,
      perNameEn,
      perType: 'DATA',
      perUrl,
      perMethod,
      perParentId: parent.perId,
      perOrder,
      perRemark,
      perState,
      perSystem,
      perModule,
    });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/1/2021, 7:17:26 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({ id: 'pages.create.sys.permission.item.title', defaultMessage: '创建权限项' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        closable={true}
        footer={
          <div
            style={{ textAlign: 'right' }}
          >
            <Button onClick={() => handleCancel()} style={{ marginRight: 8 }}>
              {intl.formatMessage({ id: 'pages.create.sys.permission.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.sys.permission.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}
                 initialValues={{ perSystem: parent.perSystem, perModule: parent.perModule }}>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="perSystem"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perSystem.label',
                  defaultMessage: '所属子系统',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perSystem.tip',
                    defaultMessage: '所属子系统是必填项!',
                  })),
                }]}
              >
                <Input readOnly={true} placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perSystem.placeholder',
                  defaultMessage: '所属子系统',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="perModule"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perModule.label',
                  defaultMessage: '模块',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perModule.tip',
                    defaultMessage: '模块是必填项!',
                  })),
                }]}
              >
                <Input readOnly={true} placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perModule.placeholder',
                  defaultMessage: '权限分组',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="perName"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perName.label',
                  defaultMessage: '权限名称',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perName.tip',
                    defaultMessage: '权限名称是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perName.placeholder',
                  defaultMessage: '权限名称',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="perNameEn"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perNameEn.label',
                  defaultMessage: '权限英文名',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perNameEn.tip',
                    defaultMessage: '权限英文名是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perNameEn.placeholder',
                  defaultMessage: '权限英文名',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={24}>
              <Form.Item
                name="perUrl"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perUrl.label',
                  defaultMessage: '权限URL',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perUrl.tip',
                    defaultMessage: '权限URL是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perUrl.placeholder',
                  defaultMessage: '权限URL',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="perOrder"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perOrder.label',
                  defaultMessage: '排序',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perOrder.tip',
                    defaultMessage: '排序是必填项!',
                  })),
                }]}
              >
                <InputNumber placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perOrder.placeholder',
                  defaultMessage: '排序',
                })} style={{width:'100%'}}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="perState"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perState.label',
                  defaultMessage: '状态信息',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.permission.perState.tip',
                    defaultMessage: '状态信息是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.permission.perState.placeholder',
                    defaultMessage: '状态信息',
                  })
                  }
                >
                  {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="perRemark"
                label={intl.formatMessage({
                  id: 'pages.create.sys.permission.perRemark.label',
                  defaultMessage: '备注信息',
                })}
              >
                <TextArea placeholder={intl.formatMessage({
                  id: 'pages.create.sys.permission.perRemark.placeholder',
                  defaultMessage: '备注信息',
                })} />
              </Form.Item>
            </Col>

          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default SysPermissionCreateItemForm;
