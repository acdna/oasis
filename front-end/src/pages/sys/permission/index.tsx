import { CaretRightOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysPermissionService } from '@/services/sys/sys.permission.service';
import { SysPermissionItem } from '@/pages/sys/permission/data';
import { SysPermissionHandles } from '@/pages/sys/permission/handles';
import DictMapper from '@/common/dictionary/dict.mapper';
import { Dispatch } from '@@/plugin-dva/connect';
import { ConnectState } from '@/models/connect';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import { dictValueEnum } from '@/components/common/select.options';
import SysPermissionUpdateForm from '@/pages/sys/permission/update.form';
import SysPermissionCreateForm from '@/pages/sys/permission/create.form';
import SysPermissionUpdateItemForm from '@/pages/sys/permission/items/update.form';
import SysPermissionCreateItemForm from '@/pages/sys/permission/items/create.form';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_SUB_SYSTEM_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_PERMISSION-属性对象
 * @author: jiangbin
 * @date: 2020-12-24 15:37:17
 **/
interface SysPermissionTableListProps {
  dictMap: DictMapper;
  dispatch: Dispatch;
}

/**
 * SYS_PERMISSION-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysPermissionTableList: React.FC<SysPermissionTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [createItemVisible, handleCreateItemVisible] = useState<boolean>(false);
  const [updateItemVisible, handleUpdateItemVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysPermissionItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysPermissionItem[]>([]);
  const handles = new SysPermissionHandles();
  const { dictMap, dispatch } = props;
  const intl = useIntl();

  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysPermissionItem>[] = [
    {
      dataIndex: 'perId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.permission.perSystem.label" defaultMessage="子系统" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perSystem.tip',
        defaultMessage: '所属子系统查询',
      })),
      dataIndex: 'perSystem',
      valueType: 'text',
      fixed: 'left',
      width: 100,
      renderText: (text) => dictMap.getItemName(SYS_SUB_SYSTEM_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_SUB_SYSTEM_DICT_GROUP.group))),
      sorter: (source, target) => (source.perSystem || '').localeCompare(target.perSystem || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.permission.perSystem.tip" defaultMessage="所属子系统" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perModule.label" defaultMessage="权限分组" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perModule.tip',
        defaultMessage: '权限分组查询',
      })),
      dataIndex: 'perModule',
      valueType: 'text',
      search: false,
      width: 100,
      sorter: (source, target) => (source.perModule || '').localeCompare(target.perModule || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.permission.perModule.tip" defaultMessage="权限分组" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perModuleLike.label" defaultMessage="权限分组" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perModuleLike.tip',
        defaultMessage: '权限分组模糊查询',
      })),
      dataIndex: 'perModuleLike',
      valueType: 'text',
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perName.label" defaultMessage="权限名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perName.tip',
        defaultMessage: '权限名称查询',
      })),
      dataIndex: 'perName',
      valueType: 'text',
      width: 180,
      sorter: (source, target) => (source.perName || '').localeCompare(target.perName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.permission.perName.tip" defaultMessage="权限名称" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perNameEn.label" defaultMessage="英文名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perNameEn.tip',
        defaultMessage: '权限英文名查询',
      })),
      dataIndex: 'perNameEn',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perType.label" defaultMessage="权限类型" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perType.tip',
        defaultMessage: '权限类型查询',
      })),
      dataIndex: 'perType',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perUrl.label" defaultMessage="权限URL" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perUrl.tip',
        defaultMessage: '权限URL查询',
      })),
      dataIndex: 'perUrl',
      valueType: 'text',
      width: 200,
      sorter: (source, target) => (source.perUrl || '').localeCompare(target.perUrl || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.permission.perUrl.tip" defaultMessage="权限URL" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perMethod.label" defaultMessage="访问方法" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perMethod.tip',
        defaultMessage: '权限访问方法查询',
      })),
      dataIndex: 'perMethod',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perParentId.label" defaultMessage="父节点ID" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perParentId.tip',
        defaultMessage: '父节点ID查询',
      })),
      dataIndex: 'perParentId',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perOrder.label" defaultMessage="排序" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perOrder.tip',
        defaultMessage: '排序查询',
      })),
      dataIndex: 'perOrder',
      valueType: 'digit',
      hideInForm: true,
      width: 50,
      sorter: (source, target) => ((source.perOrder) - (target.perOrder)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.permission.perOrder.tip" defaultMessage="排序" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perRemark.label" defaultMessage="备注信息" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perRemark.tip',
        defaultMessage: '备注信息查询',
      })),
      dataIndex: 'perRemark',
      valueType: 'textarea',
      search: false,
      hideInForm: true,
      hideInTable: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.perState.label" defaultMessage="状态" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.permission.perState.tip',
        defaultMessage: '状态信息查询',
      })),
      dataIndex: 'perState',
      valueType: 'text',
      width: 40,
      renderText: (text) => dictMap.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_ENABLE_STATE_DICT_GROUP.group))),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.permission.perState.tip" defaultMessage="状态" />
            ),
          },
        ],
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.permission.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      align:'center',
      fixed: 'right',
      width: 120,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              if (record.perType === 'DIR') {
                handleUpdateVisible(true);
              } else {
                handleUpdateItemVisible(true);
              }
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.permission.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.permission.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.permission.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
          {record.perType === 'DIR' && (<Divider type="vertical" />)}
          {record.perType === 'DIR' && (
            <a
              onClick={() => {
                handleCreateItemVisible(true);
                setFormValues(record);
              }
              }
            >
              <FormattedMessage id="pages.list.sys.permission.option.create" defaultMessage="新建" />
            </a>
          )}

        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysPermissionItem>
        headerTitle={<FormattedMessage id='pages.list.sys.permission.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="perId"
        scroll={{ x: 1300 }}
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.permission.new" defaultMessage="新建" />
          </Button>,
          <Popconfirm title={<FormattedMessage id='pages.list.sys.permission.option.init.confirm.front'
                                               defaultMessage='将先删除已有前端权限再进行初始化操作,请确定是否继续?' />}
                      onConfirm={async () => {
                        const success = await handles.initFront();
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <Button type="dashed">
              <CaretRightOutlined /><FormattedMessage id="pages.list.sys.permission.init.front" defaultMessage="初始化前端权限" />
            </Button>
          </Popconfirm>,
          <Popconfirm title={<FormattedMessage id='pages.list.sys.permission.option.init.confirm.back'
                                               defaultMessage='将先删除已有后端权限再进行初始化操作,请确定是否继续?' />}
                      onConfirm={async () => {
                        const success = await handles.initBack();
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <Button type="dashed">
              <CaretRightOutlined /><FormattedMessage id="pages.list.sys.permission.init.back" defaultMessage="初始化后端权限" />
            </Button>
          </Popconfirm>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysPermissionService.findPageDetail({
            ...params,
            sorter,
            filter,
            perType: 'DIR',
            nperId: '1',
            order: 'PER_ORDER ASC',
          });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.permission.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.permission.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.permission.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysPermissionCreateForm onCancel={() => handleCreateVisible(false)}
                               onSubmit={async (value) => {
                                 const success = await handles.add(value);
                                 setFormValues({});
                                 if (success) {
                                   handleCreateVisible(false);
                                   if (actionRef.current) {
                                     actionRef.current.reload();
                                   }
                                 }
                               }
                               }
                               dictMap={dictMap}
                               visible={createVisible}>
      </SysPermissionCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysPermissionUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            setFormValues({});
            if (success) {
              handleUpdateVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}

      {formValues && Object.keys(formValues).length>0 ? (
        <SysPermissionCreateItemForm onCancel={() => handleCreateItemVisible(false)}
                                     onSubmit={async (value) => {
                                       const success = await handles.add(value);
                                       setFormValues({});
                                       if (success) {
                                         handleCreateItemVisible(false);
                                         if (actionRef.current) {
                                           actionRef.current.reload();
                                         }
                                       }
                                     }
                                     }
                                     parent={formValues}
                                     dictMap={dictMap}
                                     visible={createItemVisible}>
        </SysPermissionCreateItemForm>) : null}

      {formValues && Object.keys(formValues).length>0 ? (
        <SysPermissionUpdateItemForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            setFormValues({});
            if (success) {
              handleUpdateItemVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateItemVisible(false);
            setFormValues({});
          }
          }
          visible={updateItemVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}

      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.perId && (
          <ProDescriptions<SysPermissionItem>
            column={2}
            title={row?.perName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                perId: row?.perId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_dictionary }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap };
};

export default connect(mapStateToProps)(SysPermissionTableList);
