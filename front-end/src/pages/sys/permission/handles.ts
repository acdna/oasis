import { SysPermissionItem } from '@/pages/sys/permission/data';
import { message } from 'antd';
import { SysPermissionService } from '@/services/sys/sys.permission.service';
import { useIntl } from 'umi';

/**
 * SYS_PERMISSION-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
export class SysPermissionHandles {
  constructor(private readonly intl = useIntl()) {
  }

  /**
   * 添加记录
   * @param fields
   */
  add = async (fields: SysPermissionItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.permission.add.loading',
      defaultMessage: '正在添加',
    }));
    try {
      await SysPermissionService.add({ ...fields });
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.permission.add.success',
        defaultMessage: '添加成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.permission.add.error',
        defaultMessage: '添加失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 更新记录
   * @param fields 更新信息
   */
  update = async (fields: SysPermissionItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.permission.update.loading',
      defaultMessage: '正在更新',
    }));
    try {
      await SysPermissionService.update(fields);
      if (fields.perType === 'DIR') {
        await SysPermissionService.update({
          cols: ['perState'],
          perParentId: fields.perId,
          perState: fields.perState,
        });
      }
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.permission.update.success',
        defaultMessage: '更新成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.permission.update.error',
        defaultMessage: '更新失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param selectedRows 选中行记录列表
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteByIds = async (selectedRows: SysPermissionItem[]) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.permission.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!selectedRows) return true;
    try {
      //删除子节点列表
      let perParentIdList = selectedRows.filter(row => (row.perType === 'DIR')).map(row => row.perId);
      if (perParentIdList?.length > 0) {
        await SysPermissionService.delete({ perParentIdList });
      }
      //删除权限分组列表
      await SysPermissionService.deleteByIds(selectedRows.map(row => row.perId));
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.permission.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.permission.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param record 被删除的记录
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteById = async (record: SysPermissionItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.permission.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!record) return true;
    try {
      //删除权限子节点
      if (record.perType === 'DIR') {
        await SysPermissionService.delete({ perParentId: record.perParentId });
      }
      //删除权限分组
      await SysPermissionService.deleteById(record.perId);
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.permission.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.permission.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 初始化前端权限
   * @author: jiangbin
   * @date: 2021-01-02 12:45:46
   **/
  initFront = async () => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.permission.init.loading',
      defaultMessage: '正在初始化前端权限',
    }));
    try {
      await SysPermissionService.initFont();
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.permission.init.success',
        defaultMessage: '初始化成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.permission.init.error',
        defaultMessage: '初始化失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 初始化后端权限
   * @author: jiangbin
   * @date: 2021-01-02 12:45:46
   **/
  initBack = async () => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.permission.init.loading',
      defaultMessage: '正在初始化后端权限',
    }));
    try {
      await SysPermissionService.initBack();
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.permission.init.success',
        defaultMessage: '初始化成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.permission.init.error',
        defaultMessage: '初始化失败，请重试!',
      }));
      return false;
    }
  };
};
