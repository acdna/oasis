/**
 * SYS_PERMISSION-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysPermissionItem {
  perId: string;
  perName: string;
  perNameEn: string;
  perType: string;
  perUrl: string;
  perMethod: string;
  perParentId: string;
  perOrder: number;
  perRemark: string;
  perState: string;
  perSystem: string;
  perModule: string;

  cols?: string[];
}

/**
 * SYS_PERMISSION-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysPermissionPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_PERMISSION-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysPermissionTableData {
  list: SysPermissionItem[];
  pagination: Partial<SysPermissionPagination>;
}

/**
 * SYS_PERMISSION-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysPermissionParams extends SysPermissionItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };

  nperId?: string;
  order: string;
  perParentIdList?: string[];
  perIdList?: string[];
}
