/**
 * SYS_CONTACT-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysContactItem {
    contactId: string;
    contactAuthor: string;
    contactEmail: string;
    contactSubject: string;
    contactText: string;
    contactCreateDate: Date;
}
/**
 * SYS_CONTACT-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysContactPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_CONTACT-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysContactTableData {
    list: SysContactItem[];
    pagination: Partial < SysContactPagination > ;
}
/**
 * SYS_CONTACT-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysContactParams extends SysContactItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}