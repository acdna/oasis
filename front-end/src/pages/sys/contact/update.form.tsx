import React, {useState} from 'react';
import {Form, Input, Modal} from 'antd';
import {FormattedMessage, useIntl} from 'umi';
import {SysContactItem} from "@/pages/sys/contact/data";
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import {AuthContext} from "@/common/auth/auth.context";

/**
 * SYS_CONTACT-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysContactUpdateFormProps {
  onSubmit: (fields: SysContactItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
}

/**
 * SYS_CONTACT-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysContactUpdateForm: React.FC<SysContactUpdateFormProps> = (props) => {
  const {onSubmit, onCancel, updateModalVisible, values,} = props;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    contactId: values.contactId,
    contactAuthor: values.contactAuthor,
    contactEmail: values.contactEmail,
    contactSubject: values.contactSubject,
    contactText: values.contactText,
    contactCreateDate: values.contactCreateDate,
  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = {...formVals, ...fieldValues, contactAuthor: AuthContext.loginName};//合并数据
    onSubmit({...values});
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const renderContent = () => (
    <>

      <Form.Item
        name="contactAuthor"
        label={<FormattedMessage id='pages.update.sys.contact.contactAuthor.label' defaultMessage='反馈用户'/>}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.contact.contactAuthor.tip' defaultMessage='请输入反馈用户！'/>),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.contact.update.contactAuthor.tip',
          defaultMessage: '请输入反馈用户！'
        })}/>
      </Form.Item>

      <Form.Item
        name="contactEmail"
        label={<FormattedMessage id='pages.update.sys.contact.contactEmail.label' defaultMessage='反馈邮箱'/>}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.contact.contactEmail.tip' defaultMessage='请输入反馈邮箱！'/>),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.contact.update.contactEmail.tip',
          defaultMessage: '请输入反馈邮箱！'
        })}/>
      </Form.Item>

      <Form.Item
        name="contactSubject"
        label={<FormattedMessage id='pages.update.sys.contact.contactSubject.label' defaultMessage='反馈主题'/>}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.contact.contactSubject.tip' defaultMessage='请输入反馈主题！'/>),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.contact.update.contactSubject.tip',
          defaultMessage: '请输入反馈主题！'
        })}/>
      </Form.Item>

      <Form.Item
        name="contactText"
        label={<FormattedMessage id='pages.update.sys.contact.contactText.label' defaultMessage='反馈内容'/>}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.contact.contactText.tip' defaultMessage='请输入反馈内容！'/>),
          },
        ]}
      >
        <TextArea placeholder={intl.formatMessage({
          id: 'pages.sys.contact.update.contactText.tip',
          defaultMessage: '请输入反馈内容！'
        })}/>
      </Form.Item>

    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.contact.ruleConfig' defaultMessage='修改'/>}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            contactId: formVals.contactId,
            contactAuthor: formVals.contactAuthor,
            contactEmail: formVals.contactEmail,
            contactSubject: formVals.contactSubject,
            contactText: formVals.contactText,
            contactCreateDate: formVals.contactCreateDate,

          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysContactUpdateForm;
