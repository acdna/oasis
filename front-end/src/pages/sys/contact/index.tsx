import {DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm} from 'antd';
import React, {useRef, useState} from 'react';
import {connect, FormattedMessage, useIntl} from 'umi';
import {FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import {SysContactService} from '@/services/sys/sys.contact.service';
import {SysContactItem} from '@/pages/sys/contact/data';
import {SysContactHandles} from '@/pages/sys/contact/handles';
import SysContactCreateForm from '@/pages/sys/contact/create.form';
import SysContactUpdateForm from '@/pages/sys/contact/update.form';
import * as moment from 'moment';
import {MLOCALES} from '@/common/locales/locale';
import {SYS_USER_EFFECTS} from "@/pages/sys/user/effects";
import UserMapper from "@/common/users/user.mapper";
import {Dispatch} from "@@/plugin-dva/connect";
import {mapValueEnum} from "@/components/common/select.options";
import {ConnectState} from "@/models/connect";

interface SysContactTableListProps {
  userMap: UserMapper;
  dispatch: Dispatch;
}

/**
 * SYS_CONTACT-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysContactTableList: React.FC<SysContactTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysContactItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysContactItem[]>([]);
  const handles = new SysContactHandles();
  const intl = useIntl();
  const {userMap, dispatch} = props;

  SYS_USER_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysContactItem>[] = [
    {
      dataIndex: 'contactId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.contact.contactAuthor.label" defaultMessage="用户"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.contact.contactAuthor.tip',
        defaultMessage: '反馈用户查询',
      })),
      dataIndex: 'contactAuthor',
      valueType: 'text',
      sorter: (source, target) => (source.contactAuthor || '').localeCompare(target.contactAuthor || ''),
      renderText: (text) => userMap.getUserName(text),
      valueEnum: mapValueEnum({
        map: userMap.getAllUsers(),
        cnTextName: 'userName',
        enTextName: 'userNameEn',
        valueName: 'userLoginName',
      }),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.contact.contactAuthor.tip" defaultMessage="反馈用户"/>
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.contact.contactEmail.label" defaultMessage="邮箱"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.contact.contactEmail.tip',
        defaultMessage: '反馈邮箱查询',
      })),
      dataIndex: 'contactEmail',
      valueType: 'text',
      sorter: (source, target) => (source.contactEmail || '').localeCompare(target.contactEmail || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.contact.contactEmail.tip" defaultMessage="反馈邮箱"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.contact.contactSubject.label" defaultMessage="主题"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.contact.contactSubject.tip',
        defaultMessage: '反馈主题查询',
      })),
      dataIndex: 'contactSubject',
      valueType: 'text',
      sorter: (source, target) => (source.contactSubject || '').localeCompare(target.contactSubject || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.contact.contactSubject.tip" defaultMessage="反馈主题"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.contact.contactText.label" defaultMessage="内容"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.contact.contactText.tip',
        defaultMessage: '反馈内容查询',
      })),
      dataIndex: 'contactText',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.contact.contactCreateDate.label" defaultMessage="日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.contact.contactCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'contactCreateDate',
      valueType: 'date', search: false, hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.contact.contactCreateDate.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.contact.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.contact.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.sys.contact.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.contact.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysContactItem>
        headerTitle={<FormattedMessage id='pages.list.sys.contact.title' defaultMessage='查询'/>}
        actionRef={actionRef}
        rowKey="contactId"
        search={
          {labelWidth: 120}
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined/> <FormattedMessage id="pages.list.sys.contact.new" defaultMessage="新建"/>
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysContactService.findPage({...params, sorter, filter});
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.contact.chosen" defaultMessage="已选择"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.contact.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined/> <FormattedMessage id="pages.list.sys.contact.batch.delete" defaultMessage="批量删除"/>
          </Button>
        </FooterToolbar>
      )}

      <SysContactCreateForm onCancel={() => handleCreateModalVisible(false)}
                            onSubmit={async (value) => {
                              const success = await handles.add(value);
                              if (success) {
                                handleCreateModalVisible(false);
                                if (actionRef.current) {
                                  actionRef.current.reload();
                                }
                              }
                            }
                            }
                            modalVisible={createModalVisible}>
      </SysContactCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysContactUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.contactId && (
          <ProDescriptions<SysContactItem>
            column={2}
            title={row?.contactSubject}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                contactId: row?.contactId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};
const mapStateToProps = ({sys_user}: ConnectState) => {
  return {userMap: sys_user.userMap};
};

export default connect(mapStateToProps)(SysContactTableList);
