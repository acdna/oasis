import React from 'react';
import {Form, Input, Modal} from 'antd';
import {FormattedMessage, useIntl} from 'umi';
import {SysContactItem} from "@/pages/sys/contact/data";
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import {AuthContext} from "@/common/auth/auth.context";

/**
 * SYS_CONTACT-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysContactCreateFormProps {
  onSubmit: (fields: SysContactItem) => void,
  onCancel: () => void;
  modalVisible: boolean;
}

/**
 * SYS_CONTACT-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysContactCreateForm: React.FC<SysContactCreateFormProps> = (props) => {
  const {modalVisible, onCancel, onSubmit} = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {contactId, contactEmail, contactSubject, contactText, contactCreateDate} = fieldValues;
    onSubmit({
      contactId,
      contactAuthor: AuthContext.loginName,
      contactEmail,
      contactSubject,
      contactText,
      contactCreateDate
    });
  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.create.sys.contact.form.title' defaultMessage='新建'/>
      }
      visible={modalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false}>

        <Form.Item
          name="contactEmail"
          label={<FormattedMessage id="pages.create.sys.contact.contactEmail.label" defaultMessage="邮箱"/>}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.contact.contactEmail.tip" defaultMessage="反馈邮箱为必填项"/>),
          },]}
        >
          <Input placeholder={intl.formatMessage({
            id: "pages.create.sys.contact.contactEmail.placeholder",
            defaultMessage: "反馈邮箱"
          })}/>
        </Form.Item>

        <Form.Item
          name="contactSubject"
          label={<FormattedMessage id="pages.create.sys.contact.contactSubject.label" defaultMessage="主题"/>}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.contact.contactSubject.tip" defaultMessage="反馈主题为必填项"/>),
          },]}
        >
          <Input placeholder={intl.formatMessage({
            id: "pages.create.sys.contact.contactSubject.placeholder",
            defaultMessage: "反馈主题"
          })}/>
        </Form.Item>

        <Form.Item
          name="contactText"
          label={<FormattedMessage id="pages.create.sys.contact.contactText.label" defaultMessage="内容"/>}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.contact.contactText.tip" defaultMessage="反馈内容为必填项"/>),
          },]}
        >
          <TextArea placeholder={intl.formatMessage({
            id: "pages.create.sys.contact.contactText.placeholder",
            defaultMessage: "反馈内容"
          })}/>
        </Form.Item>

      </ProForm>
    </Modal>
  );
};

export default SysContactCreateForm;
