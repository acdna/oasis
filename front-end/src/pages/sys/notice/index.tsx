import {DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm} from 'antd';
import React, {useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import {FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import {SysNoticeService} from '@/services/sys/sys.notice.service';
import {SysNoticeItem} from '@/pages/sys/notice/data';
import {SysNoticeHandles} from '@/pages/sys/notice/handles';
import SysNoticeCreateForm from '@/pages/sys/notice/create.form';
import SysNoticeUpdateForm from '@/pages/sys/notice/update.form';
import {MLOCALES} from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_NOTICE-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysNoticeTableList: React.FC<{}> = () => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysNoticeItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysNoticeItem[]>([]);
  const handles = new SysNoticeHandles();
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<SysNoticeItem>[] = [
    {
      dataIndex: 'noticeId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeTitle.label" defaultMessage="消息标题"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeTitle.tip',
        defaultMessage: '消息标题查询',
      })),
      dataIndex: 'noticeTitle',
      valueType: 'text',
      sorter: (source, target) => (source.noticeTitle || '').localeCompare(target.noticeTitle || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeTitle.tip" defaultMessage="消息标题"/>
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeContent.label" defaultMessage="消息内容"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeContent.tip',
        defaultMessage: '消息内容查询',
      })),
      dataIndex: 'noticeContent',
      valueType: 'text',
      sorter: (source, target) => (source.noticeContent || '').localeCompare(target.noticeContent || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeContent.tip" defaultMessage="消息内容"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeManager.label" defaultMessage="消息发布者"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeManager.tip',
        defaultMessage: '消息发布者查询',
      })),
      dataIndex: 'noticeManager',
      valueType: 'text',
      sorter: (source, target) => (source.noticeManager || '').localeCompare(target.noticeManager || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeManager.tip" defaultMessage="消息发布者"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeIsNew.label" defaultMessage="是否是新消息"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeIsNew.tip',
        defaultMessage: '是否是新消息查询',
      })),
      dataIndex: 'noticeIsNew',
      valueType: 'text',
      sorter: (source, target) => (source.noticeIsNew || '').localeCompare(target.noticeIsNew || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeIsNew.tip" defaultMessage="是否是新消息"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeCreateDate.label" defaultMessage="创建日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'noticeCreateDate',
      valueType: 'date', search: false, hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeCreateDate.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeUpdateDate.label" defaultMessage="更新日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'noticeUpdateDate',
      valueType: 'date', search: false, hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeUpdateDate.tip" defaultMessage="更新日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.noticeReceiver.label" defaultMessage="消息接收者"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.notice.noticeReceiver.tip',
        defaultMessage: '消息接收者查询',
      })),
      dataIndex: 'noticeReceiver',
      valueType: 'text',
      sorter: (source, target) => (source.noticeReceiver || '').localeCompare(target.noticeReceiver || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.notice.noticeReceiver.tip" defaultMessage="消息接收者"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.notice.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.notice.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.sys.notice.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.notice.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysNoticeItem>
        headerTitle={<FormattedMessage id='pages.list.sys.notice.title' defaultMessage='查询'/>}
        actionRef={actionRef}
        rowKey="noticeId"
        search={
          {labelWidth: 120,}
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined/> <FormattedMessage id="pages.list.sys.notice.new" defaultMessage="新建"/>
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysNoticeService.findPage({...params, sorter, filter});
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.notice.chosen" defaultMessage="已选择"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.notice.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined/> <FormattedMessage id="pages.list.sys.notice.batch.delete" defaultMessage="批量删除"/>
          </Button>
        </FooterToolbar>
      )}

      <SysNoticeCreateForm onCancel={() => handleCreateModalVisible(false)}
                           onSubmit={async (value) => {
                             const success = await handles.add(value);
                             if (success) {
                               handleCreateModalVisible(false);
                               if (actionRef.current) {
                                 actionRef.current.reload();
                               }
                             }
                           }
                           }
                           modalVisible={createModalVisible}>
      </SysNoticeCreateForm>

      {formValues && Object.keys(formValues).length > 0 ? (
        <SysNoticeUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.noticeId && (
          <ProDescriptions<SysNoticeItem>
            column={2}
            title={row?.noticeTitle}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                noticeId: row?.noticeId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default SysNoticeTableList;
