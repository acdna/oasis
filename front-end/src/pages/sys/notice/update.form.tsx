import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysNoticeItem } from "@/pages/sys/notice/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_NOTICE-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysNoticeUpdateFormProps {
    onSubmit: (fields: SysNoticeItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_NOTICE-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysNoticeUpdateForm: React.FC<SysNoticeUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        noticeId: values.noticeId,
        noticeTitle: values.noticeTitle,
        noticeContent: values.noticeContent,
        noticeManager: values.noticeManager,
        noticeIsNew: values.noticeIsNew,
        noticeCreateDate: values.noticeCreateDate,
        noticeUpdateDate: values.noticeUpdateDate,
        noticeReceiver: values.noticeReceiver,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>

            <Form.Item
                name="noticeTitle"
                label={<FormattedMessage id='pages.update.sys.notice.noticeTitle.label' defaultMessage='消息标题'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeTitle.tip' defaultMessage='请输入消息标题！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeTitle.tip',
                    defaultMessage: '请输入消息标题！'
                })}/>
            </Form.Item>

            <Form.Item
                name="noticeContent"
                label={<FormattedMessage id='pages.update.sys.notice.noticeContent.label' defaultMessage='消息内容'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeContent.tip' defaultMessage='请输入消息内容！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeContent.tip',
                    defaultMessage: '请输入消息内容！'
                })}/>
            </Form.Item>

            <Form.Item
                name="noticeManager"
                label={<FormattedMessage id='pages.update.sys.notice.noticeManager.label' defaultMessage='消息发布者'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeManager.tip' defaultMessage='请输入消息发布者！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeManager.tip',
                    defaultMessage: '请输入消息发布者！'
                })}/>
            </Form.Item>

            <Form.Item
                name="noticeIsNew"
                label={<FormattedMessage id='pages.update.sys.notice.noticeIsNew.label' defaultMessage='是否是新消息'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeIsNew.tip' defaultMessage='请输入是否是新消息！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeIsNew.tip',
                    defaultMessage: '请输入是否是新消息！'
                })}/>
            </Form.Item>

            <Form.Item
                name="noticeCreateDate"
                label={<FormattedMessage id='pages.update.sys.notice.noticeCreateDate.label' defaultMessage='创建日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeCreateDate.tip' defaultMessage='请输入创建日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeCreateDate.tip',
                    defaultMessage: '请输入创建日期！'
                })}/>
            </Form.Item>

            <Form.Item
                name="noticeUpdateDate"
                label={<FormattedMessage id='pages.update.sys.notice.noticeUpdateDate.label' defaultMessage='更新日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeUpdateDate.tip' defaultMessage='请输入更新日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeUpdateDate.tip',
                    defaultMessage: '请输入更新日期！'
                })}/>
            </Form.Item>

            <Form.Item
                name="noticeReceiver"
                label={<FormattedMessage id='pages.update.sys.notice.noticeReceiver.label' defaultMessage='消息接收者'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.notice.noticeReceiver.tip' defaultMessage='请输入消息接收者！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.notice.update.noticeReceiver.tip',
                    defaultMessage: '请输入消息接收者！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.notice.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     noticeId: formVals.noticeId,
                     noticeTitle: formVals.noticeTitle,
                     noticeContent: formVals.noticeContent,
                     noticeManager: formVals.noticeManager,
                     noticeIsNew: formVals.noticeIsNew,
                     noticeCreateDate: formVals.noticeCreateDate,
                     noticeUpdateDate: formVals.noticeUpdateDate,
                     noticeReceiver: formVals.noticeReceiver,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysNoticeUpdateForm;
