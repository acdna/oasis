/**
 * SYS_NOTICE-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysNoticeItem {
    noticeId: string;
    noticeTitle: string;
    noticeContent: string;
    noticeManager: string;
    noticeIsNew: string;
    noticeCreateDate: Date;
    noticeUpdateDate: Date;
    noticeReceiver: string;
}
/**
 * SYS_NOTICE-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysNoticePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_NOTICE-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysNoticeTableData {
    list: SysNoticeItem[];
    pagination: Partial < SysNoticePagination > ;
}
/**
 * SYS_NOTICE-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysNoticeParams extends SysNoticeItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}