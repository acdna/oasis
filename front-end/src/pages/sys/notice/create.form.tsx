import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysNoticeItem } from "@/pages/sys/notice/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_NOTICE-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysNoticeCreateFormProps {
    onSubmit: (fields: SysNoticeItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_NOTICE-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysNoticeCreateForm: React.FC<SysNoticeCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { noticeId,noticeTitle,noticeContent,noticeManager,noticeIsNew,noticeCreateDate,noticeUpdateDate,noticeReceiver }=fieldValues;
        onSubmit({ noticeId,noticeTitle,noticeContent,noticeManager,noticeIsNew,noticeCreateDate,noticeUpdateDate,noticeReceiver });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.notice.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="noticeTitle"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeTitle.label" defaultMessage="消息标题"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeTitle.tip" defaultMessage="消息标题为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeTitle.placeholder",defaultMessage:"消息标题"})}/>
                </Form.Item>

                <Form.Item
                        name="noticeContent"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeContent.label" defaultMessage="消息内容"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeContent.tip" defaultMessage="消息内容为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeContent.placeholder",defaultMessage:"消息内容"})}/>
                </Form.Item>

                <Form.Item
                        name="noticeManager"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeManager.label" defaultMessage="消息发布者"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeManager.tip" defaultMessage="消息发布者为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeManager.placeholder",defaultMessage:"消息发布者"})}/>
                </Form.Item>

                <Form.Item
                        name="noticeIsNew"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeIsNew.label" defaultMessage="是否是新消息"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeIsNew.tip" defaultMessage="是否是新消息为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeIsNew.placeholder",defaultMessage:"是否是新消息"})}/>
                </Form.Item>

                <Form.Item
                        name="noticeCreateDate"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeCreateDate.label" defaultMessage="创建日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeCreateDate.tip" defaultMessage="创建日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeCreateDate.placeholder",defaultMessage:"创建日期"})}/>
                </Form.Item>

                <Form.Item
                        name="noticeUpdateDate"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeUpdateDate.label" defaultMessage="更新日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeUpdateDate.tip" defaultMessage="更新日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeUpdateDate.placeholder",defaultMessage:"更新日期"})}/>
                </Form.Item>

                <Form.Item
                        name="noticeReceiver"
                        label={<FormattedMessage id="pages.create.sys.notice.noticeReceiver.label" defaultMessage="消息接收者"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.notice.noticeReceiver.tip" defaultMessage="消息接收者为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.notice.noticeReceiver.placeholder",defaultMessage:"消息接收者"})}/>
                </Form.Item>

            </ProForm>
        </Modal>
    );
};

export default SysNoticeCreateForm;
