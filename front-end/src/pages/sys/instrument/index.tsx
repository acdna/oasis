import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysInstrumentService } from '@/services/sys/sys.instrument.service';
import { SysInstrumentItem } from '@/pages/sys/instrument/data';
import { SysInstrumentHandles } from '@/pages/sys/instrument/handles';
import SysInstrumentCreateForm from '@/pages/sys/instrument/create.form';
import SysInstrumentUpdateForm from '@/pages/sys/instrument/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_INSTRUMENT-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysInstrumentTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysInstrumentItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysInstrumentItem[]>([]);
    const handles = new SysInstrumentHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysInstrumentItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.instrument.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'insId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insId.label" defaultMessage="仪器编号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insId.tip',
                defaultMessage: '仪器编号查询',
            })),
            dataIndex: 'insId',
            valueType:'text',
             sorter: (source, target) => (source.insId||'').localeCompare(target.insId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insId.tip" defaultMessage="仪器编号" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insBarcode.label" defaultMessage="仪器条码号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insBarcode.tip',
                defaultMessage: '仪器条码号查询',
            })),
            dataIndex: 'insBarcode',
            valueType:'text',
             sorter: (source, target) => (source.insBarcode||'').localeCompare(target.insBarcode||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insBarcode.tip" defaultMessage="仪器条码号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insName.label" defaultMessage="仪器名称" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insName.tip',
                defaultMessage: '仪器名称查询',
            })),
            dataIndex: 'insName',
            valueType:'text',
             sorter: (source, target) => (source.insName||'').localeCompare(target.insName||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insName.tip" defaultMessage="仪器名称" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insModel.label" defaultMessage="仪器型号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insModel.tip',
                defaultMessage: '仪器型号查询',
            })),
            dataIndex: 'insModel',
            valueType:'text',
             sorter: (source, target) => (source.insModel||'').localeCompare(target.insModel||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insModel.tip" defaultMessage="仪器型号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insManager.label" defaultMessage="仪器负责人" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insManager.tip',
                defaultMessage: '仪器负责人查询',
            })),
            dataIndex: 'insManager',
            valueType:'text',
             sorter: (source, target) => (source.insManager||'').localeCompare(target.insManager||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insManager.tip" defaultMessage="仪器负责人" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insType.label" defaultMessage="仪器类型" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insType.tip',
                defaultMessage: '仪器类型查询',
            })),
            dataIndex: 'insType',
            valueType:'text',
             sorter: (source, target) => (source.insType||'').localeCompare(target.insType||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insType.tip" defaultMessage="仪器类型" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insRemark.label" defaultMessage="仪器描述" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insRemark.tip',
                defaultMessage: '仪器描述查询',
            })),
            dataIndex: 'insRemark',
            valueType:'text',
             sorter: (source, target) => (source.insRemark||'').localeCompare(target.insRemark||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insRemark.tip" defaultMessage="仪器描述" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insBuyDate.label" defaultMessage="仪器购买日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insBuyDate.tip',
                defaultMessage: '仪器购买日期查询',
            })),
            dataIndex: 'insBuyDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insBuyDate.tip" defaultMessage="仪器购买日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insMaintainPeriod.label" defaultMessage="仪器维护周期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insMaintainPeriod.tip',
                defaultMessage: '仪器维护周期查询',
            })),
            dataIndex: 'insMaintainPeriod',
            valueType:'text',
             sorter: (source, target) => (source.insMaintainPeriod||'').localeCompare(target.insMaintainPeriod||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insMaintainPeriod.tip" defaultMessage="仪器维护周期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insMaker.label" defaultMessage="仪器厂商" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insMaker.tip',
                defaultMessage: '仪器厂商查询',
            })),
            dataIndex: 'insMaker',
            valueType:'text',
             sorter: (source, target) => (source.insMaker||'').localeCompare(target.insMaker||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insMaker.tip" defaultMessage="仪器厂商" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insPrice.label" defaultMessage="仪器价格" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insPrice.tip',
                defaultMessage: '仪器价格查询',
            })),
            dataIndex: 'insPrice',
            valueType:'text',
             sorter: (source, target) => (source.insPrice||'').localeCompare(target.insPrice||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insPrice.tip" defaultMessage="仪器价格" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insExtraProvide.label" defaultMessage="insExtraProvide" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insExtraProvide.tip',
                defaultMessage: 'insExtraProvide查询',
            })),
            dataIndex: 'insExtraProvide',
            valueType:'text',
             sorter: (source, target) => (source.insExtraProvide||'').localeCompare(target.insExtraProvide||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insExtraProvide.tip" defaultMessage="insExtraProvide" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'insCreateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.insUpdateDate.label" defaultMessage="更新日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.instrument.insUpdateDate.tip',
                defaultMessage: '更新日期查询',
            })),
            dataIndex: 'insUpdateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.instrument.insUpdateDate.tip" defaultMessage="更新日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.instrument.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.instrument.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.instrument.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.instrument.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysInstrumentItem>
                headerTitle={<FormattedMessage id='pages.list.sys.instrument.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="insId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.instrument.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysInstrumentService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.instrument.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.instrument.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.instrument.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysInstrumentCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysInstrumentCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysInstrumentUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.insId && (
                    <ProDescriptions<SysInstrumentItem>
                        column={2}
                        title={row?.insId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            insId: row?.insId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysInstrumentTableList;
