/**
 * SYS_INSTRUMENT-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysInstrumentItem {
    insId: string;
    insBarcode: string;
    insName: string;
    insModel: string;
    insManager: string;
    insType: string;
    insRemark: string;
    insBuyDate: Date;
    insMaintainPeriod: string;
    insMaker: string;
    insPrice: string;
    insExtraProvide: string;
    insCreateDate: Date;
    insUpdateDate: Date;
}
/**
 * SYS_INSTRUMENT-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysInstrumentPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_INSTRUMENT-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysInstrumentTableData {
    list: SysInstrumentItem[];
    pagination: Partial < SysInstrumentPagination > ;
}
/**
 * SYS_INSTRUMENT-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysInstrumentParams extends SysInstrumentItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}