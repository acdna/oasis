import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysInstrumentItem } from "@/pages/sys/instrument/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_INSTRUMENT-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysInstrumentCreateFormProps {
    onSubmit: (fields: SysInstrumentItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_INSTRUMENT-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysInstrumentCreateForm: React.FC<SysInstrumentCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:27 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { insId,insBarcode,insName,insModel,insManager,insType,insRemark,insBuyDate,insMaintainPeriod,insMaker,insPrice,insExtraProvide,insCreateDate,insUpdateDate }=fieldValues;
        onSubmit({ insId,insBarcode,insName,insModel,insManager,insType,insRemark,insBuyDate,insMaintainPeriod,insMaker,insPrice,insExtraProvide,insCreateDate,insUpdateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.instrument.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="insBarcode"
                        label={<FormattedMessage id="pages.create.sys.instrument.insBarcode.label" defaultMessage="仪器条码号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insBarcode.tip" defaultMessage="仪器条码号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insBarcode.placeholder",defaultMessage:"仪器条码号"})}/>
                </Form.Item>

                <Form.Item
                        name="insName"
                        label={<FormattedMessage id="pages.create.sys.instrument.insName.label" defaultMessage="仪器名称"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insName.tip" defaultMessage="仪器名称为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insName.placeholder",defaultMessage:"仪器名称"})}/>
                </Form.Item>

                <Form.Item
                        name="insModel"
                        label={<FormattedMessage id="pages.create.sys.instrument.insModel.label" defaultMessage="仪器型号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insModel.tip" defaultMessage="仪器型号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insModel.placeholder",defaultMessage:"仪器型号"})}/>
                </Form.Item>

                <Form.Item
                        name="insManager"
                        label={<FormattedMessage id="pages.create.sys.instrument.insManager.label" defaultMessage="仪器负责人"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insManager.tip" defaultMessage="仪器负责人为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insManager.placeholder",defaultMessage:"仪器负责人"})}/>
                </Form.Item>

                <Form.Item
                        name="insType"
                        label={<FormattedMessage id="pages.create.sys.instrument.insType.label" defaultMessage="仪器类型"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insType.tip" defaultMessage="仪器类型为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insType.placeholder",defaultMessage:"仪器类型"})}/>
                </Form.Item>

                <Form.Item
                        name="insRemark"
                        label={<FormattedMessage id="pages.create.sys.instrument.insRemark.label" defaultMessage="仪器描述"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insRemark.tip" defaultMessage="仪器描述为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insRemark.placeholder",defaultMessage:"仪器描述"})}/>
                </Form.Item>

                <Form.Item
                        name="insBuyDate"
                        label={<FormattedMessage id="pages.create.sys.instrument.insBuyDate.label" defaultMessage="仪器购买日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insBuyDate.tip" defaultMessage="仪器购买日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insBuyDate.placeholder",defaultMessage:"仪器购买日期"})}/>
                </Form.Item>

                <Form.Item
                        name="insMaintainPeriod"
                        label={<FormattedMessage id="pages.create.sys.instrument.insMaintainPeriod.label" defaultMessage="仪器维护周期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insMaintainPeriod.tip" defaultMessage="仪器维护周期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insMaintainPeriod.placeholder",defaultMessage:"仪器维护周期"})}/>
                </Form.Item>

                <Form.Item
                        name="insMaker"
                        label={<FormattedMessage id="pages.create.sys.instrument.insMaker.label" defaultMessage="仪器厂商"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insMaker.tip" defaultMessage="仪器厂商为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insMaker.placeholder",defaultMessage:"仪器厂商"})}/>
                </Form.Item>

                <Form.Item
                        name="insPrice"
                        label={<FormattedMessage id="pages.create.sys.instrument.insPrice.label" defaultMessage="仪器价格"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insPrice.tip" defaultMessage="仪器价格为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insPrice.placeholder",defaultMessage:"仪器价格"})}/>
                </Form.Item>

                <Form.Item
                        name="insExtraProvide"
                        label={<FormattedMessage id="pages.create.sys.instrument.insExtraProvide.label" defaultMessage="insExtraProvide"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.instrument.insExtraProvide.tip" defaultMessage="insExtraProvide为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.instrument.insExtraProvide.placeholder",defaultMessage:"insExtraProvide"})}/>
                </Form.Item>

            </ProForm>
        </Modal>
    );
};

export default SysInstrumentCreateForm;
