import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysInstrumentItem } from "@/pages/sys/instrument/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_INSTRUMENT-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysInstrumentUpdateFormProps {
    onSubmit: (fields: SysInstrumentItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_INSTRUMENT-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysInstrumentUpdateForm: React.FC<SysInstrumentUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        insId: values.insId,
        insBarcode: values.insBarcode,
        insName: values.insName,
        insModel: values.insModel,
        insManager: values.insManager,
        insType: values.insType,
        insRemark: values.insRemark,
        insBuyDate: values.insBuyDate,
        insMaintainPeriod: values.insMaintainPeriod,
        insMaker: values.insMaker,
        insPrice: values.insPrice,
        insExtraProvide: values.insExtraProvide,
        insCreateDate: values.insCreateDate,
        insUpdateDate: values.insUpdateDate,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const renderContent = () => (
        <>

            <Form.Item
                name="insBarcode"
                label={<FormattedMessage id='pages.update.sys.instrument.insBarcode.label' defaultMessage='仪器条码号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insBarcode.tip' defaultMessage='请输入仪器条码号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insBarcode.tip',
                    defaultMessage: '请输入仪器条码号！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insName"
                label={<FormattedMessage id='pages.update.sys.instrument.insName.label' defaultMessage='仪器名称'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insName.tip' defaultMessage='请输入仪器名称！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insName.tip',
                    defaultMessage: '请输入仪器名称！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insModel"
                label={<FormattedMessage id='pages.update.sys.instrument.insModel.label' defaultMessage='仪器型号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insModel.tip' defaultMessage='请输入仪器型号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insModel.tip',
                    defaultMessage: '请输入仪器型号！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insManager"
                label={<FormattedMessage id='pages.update.sys.instrument.insManager.label' defaultMessage='仪器负责人'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insManager.tip' defaultMessage='请输入仪器负责人！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insManager.tip',
                    defaultMessage: '请输入仪器负责人！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insType"
                label={<FormattedMessage id='pages.update.sys.instrument.insType.label' defaultMessage='仪器类型'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insType.tip' defaultMessage='请输入仪器类型！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insType.tip',
                    defaultMessage: '请输入仪器类型！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insRemark"
                label={<FormattedMessage id='pages.update.sys.instrument.insRemark.label' defaultMessage='仪器描述'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insRemark.tip' defaultMessage='请输入仪器描述！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insRemark.tip',
                    defaultMessage: '请输入仪器描述！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insBuyDate"
                label={<FormattedMessage id='pages.update.sys.instrument.insBuyDate.label' defaultMessage='仪器购买日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insBuyDate.tip' defaultMessage='请输入仪器购买日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insBuyDate.tip',
                    defaultMessage: '请输入仪器购买日期！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insMaintainPeriod"
                label={<FormattedMessage id='pages.update.sys.instrument.insMaintainPeriod.label' defaultMessage='仪器维护周期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insMaintainPeriod.tip' defaultMessage='请输入仪器维护周期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insMaintainPeriod.tip',
                    defaultMessage: '请输入仪器维护周期！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insMaker"
                label={<FormattedMessage id='pages.update.sys.instrument.insMaker.label' defaultMessage='仪器厂商'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insMaker.tip' defaultMessage='请输入仪器厂商！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insMaker.tip',
                    defaultMessage: '请输入仪器厂商！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insPrice"
                label={<FormattedMessage id='pages.update.sys.instrument.insPrice.label' defaultMessage='仪器价格'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insPrice.tip' defaultMessage='请输入仪器价格！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insPrice.tip',
                    defaultMessage: '请输入仪器价格！'
                })}/>
            </Form.Item>

            <Form.Item
                name="insExtraProvide"
                label={<FormattedMessage id='pages.update.sys.instrument.insExtraProvide.label' defaultMessage='insExtraProvide'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.instrument.insExtraProvide.tip' defaultMessage='请输入insExtraProvide！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.instrument.update.insExtraProvide.tip',
                    defaultMessage: '请输入insExtraProvide！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.instrument.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     insId: formVals.insId,
                     insBarcode: formVals.insBarcode,
                     insName: formVals.insName,
                     insModel: formVals.insModel,
                     insManager: formVals.insManager,
                     insType: formVals.insType,
                     insRemark: formVals.insRemark,
                     insBuyDate: formVals.insBuyDate,
                     insMaintainPeriod: formVals.insMaintainPeriod,
                     insMaker: formVals.insMaker,
                     insPrice: formVals.insPrice,
                     insExtraProvide: formVals.insExtraProvide,
                     insCreateDate: formVals.insCreateDate,
                     insUpdateDate: formVals.insUpdateDate,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysInstrumentUpdateForm;
