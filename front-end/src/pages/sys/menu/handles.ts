import { SysMenuItem } from '@/pages/sys/menu/data';
import { message } from 'antd';
import { SysMenuService } from '@/services/sys/sys.menu.service';
import { useIntl } from 'umi';
/**
 * SYS_MENU-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
export class SysMenuHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysMenuItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.menu.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysMenuService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.menu.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.menu.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysMenuItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.menu.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysMenuService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.menu.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.menu.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysMenuItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.menu.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysMenuService.deleteByIds(selectedRows.map(row => row.menuId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.menu.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.menu.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
  /**
   * 删除选中行数据
   * @param ids ID列表
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteByIdList = async (ids: string[]) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.menu.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!ids) return true;
    try {
      await SysMenuService.deleteByIds(ids);
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.menu.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.menu.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysMenuItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.menu.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysMenuService.deleteById(record.menuId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.menu.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.menu.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};
