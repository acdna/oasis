import { DeleteOutlined, MinusCircleOutlined, PlusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Input, InputNumber, message, Popconfirm, Select, Space, Tree } from 'antd';
import React, { Key, useEffect, useState } from 'react';
import { connect, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import { SysMenuService } from '@/services/sys/sys.menu.service';
import { SysMenuItem } from '@/pages/sys/menu/data';
import { SysMenuHandles } from '@/pages/sys/menu/handles';
import SysMenuCreateForm from '@/pages/sys/menu/create.form';
import { ConnectState } from '@/models/connect';
import DictMapper from '@/common/dictionary/dict.mapper';
import { Dispatch } from '@@/plugin-dva/connect';
import { options } from '@/components/common/select.options';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import { TreeCreator } from '@/common/tree/tree.creator';
import { getChildrenIds } from '@/common/tree/tree.utils';
import SysMenuItemCreateForm from '@/pages/sys/menu/items/create.form';
import { SYS_ENABLE_STATE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_MENU-属性对象
 * @author: jiangbin
 * @date: 2020-12-24 15:37:17
 **/
interface SysMenuTableListProps {
  dictMap: DictMapper;
  dispatch: Dispatch;
}

/**
 * SYS_MENU-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysMenuTableList: React.FC<SysMenuTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState(false);
  const [createItemVisible, handleCreateItemVisible] = useState(false);
  const [dataSource, setDataSource] = useState([] as any[]);
  const [formValues, setFormValues] = useState({});
  const [menus, setMenus] = useState([] as SysMenuItem[]);
  const [nodes, setNodes] = useState([] as any[]);
  const [selectedRowsState, setSelectedRows] = useState([] as SysMenuItem[]);
  const handles = new SysMenuHandles();
  const { dictMap, dispatch } = props;
  const intl = useIntl();
  const treeCreator = new TreeCreator();

  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //加载数据到Tree组件上
  useEffect(() => {
    (async () => {
      await loadAll();
    })();
  }, []);

  /**
   * 菜单标题内容
   * @author: jiangbin
   * @date: 2021-01-05 13:07:27
   **/
  const titleRender = (item: SysMenuItem | Partial<SysMenuItem>) => {
    return (
      <>
        <Space>
          <Input defaultValue={item.menuName} style={{ width: 120 }}
                 placeholder={intl.formatMessage({
                   id: 'pages.tree.sys.menu.menuUrl.success',
                   defaultMessage: '菜单名称',
                 })}
                 onBlur={async (e) => {
                   if (item.menuName === e.target.value) {
                     return;
                   }
                   item.menuName = e.target.value;
                   if (!e.target.value) {
                     return;
                   }
                   await SysMenuService.update({
                     cols: ['menuUrl'],
                     menuId: item.menuId,
                     menuName: e.target.value,
                   });
                   message.success(intl.formatMessage({
                     id: 'pages.tree.sys.menu.menuName.success',
                     defaultMessage: '更新菜单名称成功!',
                   }));
                 }} />
          <Input defaultValue={item.menuNameEn} style={{ width: 120 }}
                 placeholder={intl.formatMessage({
                   id: 'pages.tree.sys.menu.menuNameEn.success',
                   defaultMessage: '英文菜单名称',
                 })}
                 onBlur={async (e) => {
                   if (item.menuNameEn === e.target.value) {
                     return;
                   }
                   item.menuNameEn = e.target.value;
                   if (!e.target.value) {
                     return;
                   }
                   await SysMenuService.update({
                     cols: ['menuNameEn'],
                     menuId: item.menuId,
                     menuNameEn: e.target.value,
                   });
                   message.success(intl.formatMessage({
                     id: 'pages.tree.sys.menu.menuName.success',
                     defaultMessage: '更新英文菜单名称成功!',
                   }));
                 }} />
        </Space>
        <Space style={{ paddingLeft: 10 }}>
          <Input name='menuUrl' defaultValue={item.menuUrl} style={{ width: 250 }}
                 placeholder='URL'
                 onBlur={async (e) => {
                   if (item.menuUrl === e.target.value) {
                     return;
                   }
                   item.menuUrl = e.target.value;
                   if (!e.target.value) {
                     await SysMenuService.update({
                       cols: ['menuUrl'],
                       menuId: item.menuId,
                       nmenuUrl: e.target.value,
                     });
                   } else {
                     await SysMenuService.update({
                       cols: ['menuUrl'],
                       menuId: item.menuId,
                       menuUrl: e.target.value,
                     });
                   }
                   message.success(intl.formatMessage({
                     id: 'pages.tree.sys.menu.menuUrl.success',
                     defaultMessage: '更新菜单URL成功!',
                   }));
                 }} />
          <Input name='menuIconClass' defaultValue={item.menuIconClass} style={{ width: 140 }}
                 placeholder={intl.formatMessage({
                   id: 'pages.tree.sys.menu.menuIconClass.success',
                   defaultMessage: '菜单图标',
                 })}
                 onBlur={async (e) => {
                   if (item.menuIconClass === e.target.value) {
                     return;
                   }
                   item.menuIconClass = e.target.value;
                   if (!e.target.value) {
                     await SysMenuService.update({
                       cols: ['menuIconClass'],
                       menuId: item.menuId,
                       nmenuIconClass: e.target.value,
                     });
                   } else {
                     await SysMenuService.update({
                       cols: ['menuIconClass'],
                       menuId: item.menuId,
                       menuIconClass: e.target.value,
                     });
                   }
                   message.success(intl.formatMessage({
                     id: 'pages.tree.sys.menu.menuIconClass.success',
                     defaultMessage: '更新菜单图标成功!',
                   }));
                 }} />
          <InputNumber name='menuOrder' required defaultValue={item.menuOrder} style={{ width: 60 }}
                       placeholder={intl.formatMessage({
                         id: 'pages.tree.sys.menu.menuOrder.success',
                         defaultMessage: '顺序',
                       })}
                       onBlur={async (e) => {
                         if (item.menuOrder == parseInt(e.target.value || '')) {
                           return;
                         }
                         item.menuOrder = parseInt(e.target.value || '');
                         if (!e.target.value) {
                           message.warn(intl.formatMessage({
                             id: 'pages.tree.sys.menu.menuOrder.warn',
                             defaultMessage: '请设置顺序!',
                           }));
                           return;
                         }
                         await SysMenuService.update({
                           cols: ['menuOrder'],
                           menuId: item.menuId,
                           menuOrder: parseInt(e.target.value),
                         });
                         message.success(intl.formatMessage({
                           id: 'pages.tree.sys.menu.menuOrder.success',
                           defaultMessage: '更新菜单顺序成功!',
                         }));
                       }} />
          <Select style={{ width: 110 }} defaultValue={item.menuState}
                  placeholder={intl.formatMessage({
                    id: 'pages.tree.sys.menu.menuState.success',
                    defaultMessage: '状态',
                  })}
                  onChange={async (value) => {
                    if (!value || item.menuState === value) {
                      return;
                    }
                    item.menuState = value;
                    await SysMenuService.update({
                      cols: ['menuState'],
                      menuId: item.menuId,
                      menuState: value,
                    });
                    message.success(intl.formatMessage({
                      id: 'pages.tree.sys.menu.menuState.success',
                      defaultMessage: '更新菜单状态成功!',
                    }));
                  }}>
            {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
          </Select>
        </Space>
        <div style={{ float: 'right' }}>
          <Space>
            <Popconfirm title={<FormattedMessage id='pages.list.sys.menu.tree.btn.delete.confirm'
                                                 defaultMessage='删除不可恢复，你确定要删除吗?' />}
                        onConfirm={async () => {
                          //获取节点和子节点ID列表
                          let ids = [item.menuId || ''];
                          let subIds = getChildrenIds(nodes, ids);
                          (subIds && subIds.length > 0) && (ids = ids.concat(subIds));

                          //删除ID列表记录
                          const success = await handles.deleteByIdList(ids);

                          //清除记录值
                          setFormValues({});

                          if (success) {
                            await loadAll();
                          }
                        }
                        }>
              <Button type='default'>
                <MinusCircleOutlined /> <FormattedMessage id='pages.list.sys.menu.tree.delete' defaultMessage='删除' />
              </Button>
            </Popconfirm>
            <Button key="3" type='primary'
                    onClick={() => {
                      setFormValues(item);
                      handleCreateItemVisible(true);
                    }}>
              <PlusCircleOutlined /> <FormattedMessage id='pages.list.sys.menu.tree.add' defaultMessage='添加' />
            </Button>
          </Space>
        </div>
      </>);
  };

  /**
   * 删除节点数据
   * @author: jiangbin
   * @date: 2021-01-06 10:13:37
   **/
  const loadAll = async () => {
    //过滤掉节点列表
    let newMenus = await SysMenuService.findAll();
    setMenus(newMenus);
    let tree = toTree(newMenus);
    setDataSource(tree);
  };

  //转换成树节点列表
  const toTree = (menus: SysMenuItem[]) => {
    let tnodes = new Array<SysMenuItem>();
    //构建菜单树
    menus && (tnodes = menus.map((item: SysMenuItem) => {
      return {
        ...item,
        key: item.menuId || '',
        title: titleRender(item),
        id: item.menuId,
        order: item.menuOrder,
        parentId: item.menuParentId,
      };
    }));

    //设置节点列表
    setNodes(tnodes);

    //构建成树结构
    return treeCreator.tree(tnodes);
  };

  /**
   * 选中节点时触发
   * @author: jiangbin
   * @date: 2021-01-06 09:33:41
   **/
  const onCheck = (checkedKeys: Key[], info: any) => {
    if (!checkedKeys || checkedKeys.length == 0) {
      setSelectedRows([]);
    } else {//过滤出菜单项列表
      let records = menus.filter(menu => checkedKeys.includes(menu.menuId));
      setSelectedRows(records);
    }
  };

  return (
    <>
      <PageContainer
        extra={[
          <Button key={1} type='primary' onClick={() => {
            handleCreateVisible(true);
          }}>
            <PlusOutlined />{intl.formatMessage({ id: 'pages.list.sys.menu.tree.add', defaultMessage: '添加' })}
          </Button>,
        ]}
        tabList={[
          { tab: (intl.formatMessage({ id: 'pages.list.sys.menu.tree.tab', defaultMessage: '菜单树' })), key: 'menuTree' },
        ]}
      >
        <Tree
          style={{ padding: 20 }}
          checkable
          showIcon
          blockNode={true}
          treeData={dataSource}
          // @ts-ignore
          onCheck={onCheck}
        />

        {selectedRowsState?.length > 0 && (
          <FooterToolbar
            extra={
              <div>
                <FormattedMessage id="pages.list.sys.menu.tree.chosen" defaultMessage="已选择" />{' '}
                <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
                <FormattedMessage id="pages.list.sys.menu.tree.item" defaultMessage="项" />
              </div>
            }
          >
            <Button danger
                    onClick={async () => {
                      await handles.deleteByIds(selectedRowsState);
                      await loadAll();
                      setSelectedRows([]);
                    }
                    }
            >
              <DeleteOutlined /> <FormattedMessage id="pages.list.sys.menu.tree.batch.delete" defaultMessage="批量删除" />
            </Button>
          </FooterToolbar>
        )}

        <SysMenuCreateForm
          onSubmit={async (value) => {
            const success = await handles.add(value);
            setFormValues({});
            if (success) {
              handleCreateVisible(false);
              //重新加载菜单项
              await loadAll();
            }
          }
          }
          onCancel={() => {
            handleCreateVisible(false);
            setFormValues({});
          }}
          visible={createVisible}
          dictMap={dictMap}>
        </SysMenuCreateForm>


        <SysMenuItemCreateForm
          onSubmit={async (value: any) => {
            const success = await handles.add(value);
            setFormValues({});
            if (success) {
              handleCreateItemVisible(false);
              //重新加载菜单项
              await loadAll();
            }
          }
          }
          onCancel={() => {
            handleCreateItemVisible(false);
            setFormValues({});
          }
          }
          visible={createItemVisible}
          parent={formValues as SysMenuItem}
          dictMap={dictMap}
        />
      </PageContainer>
    </>
  );
};

const mapStateToProps = ({ sys_dictionary }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap };
};

export default connect(mapStateToProps)(SysMenuTableList);
