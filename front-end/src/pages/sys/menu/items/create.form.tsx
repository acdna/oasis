import { SysMenuItem } from '@/pages/sys/menu/data';
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, InputNumber, Row, Select } from 'antd';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options } from '@/components/common/select.options';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_NODE_TYPE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_MENU-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/6/2021, 8:37:48 AM
 **/
interface SysMenuItemCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysMenuItem) => void;
  parent: SysMenuItem;
  dictMap: DictMapper;
}

/**
 * SYS_MENU-创建记录表单
 * @author jiangbin
 * @date 1/6/2021, 8:37:48 AM
 **/
const SysMenuItemCreateForm: React.FC<SysMenuItemCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit, parent, dictMap } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 8:37:48 AM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { menuId, menuState } = parent;
    const { menuName, menuNameEn, menuUrl, menuOrder, menuRemark, menuType, menuIconClass } = fieldValues;
    onSubmit({
      menuId:'',
      menuName,
      menuNameEn,
      menuUrl,
      menuParentId: menuId,
      menuOrder,
      menuRemark,
      menuState,
      menuType,
      menuIconClass,
    });
    await form.resetFields();
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 8:37:48 AM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({ id: 'pages.create.sys.menu.form.item.title', defaultMessage: '创建菜单项' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        closable={true}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => handleCancel()} style={{ marginRight: 8 }}>
              {intl.formatMessage({ id: 'pages.create.sys.menu.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.sys.menu.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}
                 initialValues={{ menuState: 'ON', menuType: 'DIR', menuOrder: 1 }}>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="menuName"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuName.label',
                  defaultMessage: '菜单名称',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.menu.menuName.tip',
                    defaultMessage: '菜单名称是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuName.placeholder',
                  defaultMessage: '菜单名称',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="menuNameEn"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuNameEn.label',
                  defaultMessage: '英文菜单名',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.menu.menuNameEn.tip',
                    defaultMessage: '英文菜单名是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuNameEn.placeholder',
                  defaultMessage: '英文菜单名',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="menuUrl"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuUrl.label',
                  defaultMessage: '菜单连接地址',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuUrl.placeholder',
                  defaultMessage: '菜单连接地址',
                })} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="menuIconClass"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuIconClass.label',
                  defaultMessage: '菜单图标CSS名称',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuIconClass.placeholder',
                  defaultMessage: '菜单图标CSS名称',
                })} />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="menuOrder"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuOrder.label',
                  defaultMessage: '菜单顺序',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.menu.menuOrder.tip',
                    defaultMessage: '菜单顺序是必填项!',
                  })),
                }]}
              >
                <InputNumber placeholder={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuOrder.placeholder',
                  defaultMessage: '菜单顺序',
                })} style={{width:'100%'}}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="menuType"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuType.label',
                  defaultMessage: '菜单类型',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.menu.menuType.tip',
                    defaultMessage: '菜单类型是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.menu.menuType.placeholder',
                    defaultMessage: '菜单类型',
                  })
                  }
                  defaultActiveFirstOption={true}
                >
                  {options(SYS_NODE_TYPE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="menuState"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuState.label',
                  defaultMessage: '菜单状态',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.menu.menuState.tip',
                    defaultMessage: '菜单状态是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.menu.menuState.placeholder',
                    defaultMessage: '启用?',
                  })
                  }
                  defaultActiveFirstOption={true}
                >
                  {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="menuRemark"
                label={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuRemark.label',
                  defaultMessage: '菜单备注',
                })}
              >
                <TextArea placeholder={intl.formatMessage({
                  id: 'pages.create.sys.menu.menuRemark.placeholder',
                  defaultMessage: '菜单备注',
                })} />
              </Form.Item>
            </Col>
          </Row>
        </ProForm>
      </Drawer>
    </>
  );

};

export default SysMenuItemCreateForm;
