/**
 * SYS_MENU-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMenuItem {
  menuId: string;
  menuName: string;
  menuNameEn: string;
  menuUrl: string;
  menuParentId: string;
  menuOrder: number;
  menuRemark: string;
  menuState: string;
  menuType: string;
  menuIconClass: string;
}

/**
 * SYS_MENU-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMenuPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_MENU-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMenuTableData {
  list: SysMenuItem[];
  pagination: Partial<SysMenuPagination>;
}

/**
 * SYS_MENU-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysMenuParams extends SysMenuItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };
  menuIdList?: string[];
  cols?: string[];
  nmenuUrl: string;
  nmenuRemark: string;
  nmenuIconClass: string;
}
