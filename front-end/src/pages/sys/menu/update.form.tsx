import React, { useState } from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysMenuItem } from '@/pages/sys/menu/data';
import ProForm from '@ant-design/pro-form';
import { options } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import TextArea from 'antd/es/input/TextArea';
import { SYS_ENABLE_STATE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_MENU-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysMenuUpdateFormProps {
  onSubmit: (fields: SysMenuItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_MENU-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysMenuUpdateForm: React.FC<SysMenuUpdateFormProps> = (props) => {
  const { onSubmit, onCancel, updateModalVisible, values, dictMap } = props;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    menuId: values.menuId,
    menuName: values.menuName,
    menuNameEn: values.menuNameEn,
    menuUrl: values.menuUrl,
    menuParentId: values.menuParentId,
    menuOrder: values.menuOrder,
    menuRemark: values.menuRemark,
    menuState: values.menuState,
    menuType: values.menuType,
    menuIconClass: values.menuIconClass,

  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const renderContent = () => (
    <>
      <Form.Item
        name="menuName"
        label={<FormattedMessage id='pages.update.sys.menu.menuName.label' defaultMessage='菜单名称' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuName.tip' defaultMessage='请输入菜单名称！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.menu.update.menuName.tip',
          defaultMessage: '请输入菜单名称！',
        })} />
      </Form.Item>

      <Form.Item
        name="menuNameEn"
        label={<FormattedMessage id='pages.update.sys.menu.menuNameEn.label' defaultMessage='英文菜单名' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuNameEn.tip' defaultMessage='请输入英文菜单名！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.menu.update.menuNameEn.tip',
          defaultMessage: '请输入英文菜单名！',
        })} />
      </Form.Item>

      <Form.Item
        name="menuUrl"
        label={<FormattedMessage id='pages.update.sys.menu.menuUrl.label' defaultMessage='URL' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuUrl.tip' defaultMessage='请输入URL！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.menu.update.menuUrl.tip',
          defaultMessage: '请输入URL！',
        })} />
      </Form.Item>

      <Form.Item
        name="menuOrder"
        label={<FormattedMessage id='pages.update.sys.menu.menuOrder.label' defaultMessage='顺序' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuOrder.tip' defaultMessage='请输入菜单顺序！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.menu.update.menuOrder.tip',
          defaultMessage: '请输入菜单顺序！',
        })} />
      </Form.Item>

      <Form.Item
        name="menuState"
        label={<FormattedMessage id='pages.update.sys.menu.menuState.label' defaultMessage='状态' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuState.tip' defaultMessage='请输入菜单状态！' />),
          },
        ]}
      >
        <Select
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.update.sys.menu.menuState.placeholder',
            defaultMessage: '启用?',
          })
          }
          defaultActiveFirstOption={true}
        >
          {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>

      <Form.Item
        name="menuIconClass"
        label={<FormattedMessage id='pages.update.sys.menu.menuIconClass.label' defaultMessage='图标' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuIconClass.tip' defaultMessage='请输入菜单图标！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.menu.update.menuIconClass.tip',
          defaultMessage: '请输入菜单图标！',
        })} />
      </Form.Item>

      <Form.Item
        name="menuRemark"
        label={<FormattedMessage id='pages.update.sys.menu.menuRemark.label' defaultMessage='备注' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.menu.menuRemark.tip' defaultMessage='请输入菜单备注！' />),
          },
        ]}
      >
        <TextArea placeholder={intl.formatMessage({
          id: 'pages.sys.menu.update.menuRemark.tip',
          defaultMessage: '请输入菜单备注！',
        })} />
      </Form.Item>
    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{ padding: '32px 40px 48px' }}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.menu.form.title' defaultMessage='修改' />}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            menuId: formVals.menuId,
            menuName: formVals.menuName,
            menuNameEn: formVals.menuNameEn,
            menuUrl: formVals.menuUrl,
            menuParentId: formVals.menuParentId,
            menuOrder: formVals.menuOrder,
            menuRemark: formVals.menuRemark,
            menuState: formVals.menuState,
            menuType: formVals.menuType,
            menuIconClass: formVals.menuIconClass,

          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysMenuUpdateForm;
