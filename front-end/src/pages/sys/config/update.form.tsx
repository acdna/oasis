import {SysConfigItem} from '@/pages/sys/config/data';
import React, {useState} from 'react';
import {useIntl} from 'umi';
import {Button, Col, Drawer, Form, Input, InputNumber, Row, Select} from 'antd';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import {options} from '@/components/common/select.options';
import TextArea from 'antd/es/input/TextArea';
import {SYS_CONFIG_TYPE_DICT_GROUP, SYS_ENABLE_STATE_DICT_GROUP} from '@/common/dictionary/sys.dict.defined';
import {SYS_CONFIG_VALIDATOR} from '@/pages/sys/config/validator';

/**
 * SYS_CONFIG-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface SysConfigUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysConfigItem) => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_CONFIG-更新字典项信息表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const SysConfigUpdateForm: React.FC<SysConfigUpdateFormProps> = (props) => {
  const {values, visible, onCancel, onSubmit, dictMap} = props;
  const intl = useIntl();

  const [formVals] = useState({
    conId: values.conId,
    conName: values.conName,
    conNameEn: values.conNameEn,
    conValue: values.conValue,
    conGroup: values.conGroup,
    conParentId: values.conParentId,
    conParamType: values.conParamType,
    conType: 'DIR',
    conCreateDate: values.conCreateDate,
    conUpdateDate: values.conUpdateDate,
    conManager: values.conManager,
    conSpecies: values.conSpecies,
    conOrder: values.conOrder,
    conState: values.conState,
    conRemark: values.conRemark,
  });
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = {...formVals, ...fieldValues};//合并数据
    onSubmit({...values});
    await form.resetFields();
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({id: 'pages.update.sys.config.form.title', defaultMessage: '修改'})}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{paddingBottom: 80}}
        closable={true}
        footer={
          <div
            style={{textAlign: 'right'}}
          >
            <Button onClick={() => handleCancel()} style={{marginRight: 8}}>
              {intl.formatMessage({id: 'pages.update.sys.config.form.cancel', defaultMessage: '取消'})}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({id: 'pages.update.sys.config.form.submit', defaultMessage: '提交'})}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form} initialValues={formVals}>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="conName"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conName.label',
                  defaultMessage: '参数名称',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.update.sys.config.conName.tip',
                      defaultMessage: '参数名称是必填项!',
                    })),
                  }, SYS_CONFIG_VALIDATOR.CON_NAME(intl.formatMessage({
                    id: 'pages.update.sys.config.conName.valid',
                    defaultMessage: '参数名称已存在!',
                  }), {nconId: values.conId}),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.config.conName.placeholder',
                  defaultMessage: '参数名称',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="conNameEn"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conNameEn.label',
                  defaultMessage: '英文参数名称',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.update.sys.config.conNameEn.tip',
                      defaultMessage: '英文参数名称是必填项!',
                    })),
                  }, SYS_CONFIG_VALIDATOR.CON_NAME_EN(intl.formatMessage({
                    id: 'pages.update.sys.config.conNameEn.valid',
                    defaultMessage: '英文参数名称已存在!',
                  }), {nconId: values.conId, conType: 'DIR'}),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.config.conNameEn.placeholder',
                  defaultMessage: '英文参数名称',
                })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="conGroup"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conGroup.label',
                  defaultMessage: '分组',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.update.sys.config.conGroup.tip',
                      defaultMessage: '分组是必填项!',
                    })),
                  }, SYS_CONFIG_VALIDATOR.CON_GROUP(intl.formatMessage({
                    id: 'pages.update.sys.config.conGroup.valid',
                    defaultMessage: '分组名称已存在!',
                  }), {nconId: values.conId, conType: 'DIR'}),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.config.conGroup.placeholder',
                  defaultMessage: '分组',
                })}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="conParamType"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conParamType.label',
                  defaultMessage: '参数类型',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.config.conParamType.tip',
                    defaultMessage: '参数类型是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{width: '100%'}}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.config.conParamType.placeholder',
                    defaultMessage: '参数类型',
                  })}
                >
                  {options(SYS_CONFIG_TYPE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="conOrder"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conOrder.label',
                  defaultMessage: '排序',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.config.conOrder.tip',
                    defaultMessage: '排序是必填项!',
                  })),
                }]}
              >
                <InputNumber placeholder={intl.formatMessage({
                  id: 'pages.update.sys.config.conOrder.placeholder',
                  defaultMessage: '排序',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="conState"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conState.label',
                  defaultMessage: '状态',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.config.conState.tip',
                    defaultMessage: '状态是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{width: '100%'}}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.config.conState.placeholder',
                    defaultMessage: '启用?',
                  })}
                >
                  {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={24}>
              <Form.Item
                name="conRemark"
                label={intl.formatMessage({
                  id: 'pages.update.sys.config.conRemark.label',
                  defaultMessage: '描述',
                })}
              >
                <TextArea placeholder={intl.formatMessage({
                  id: 'pages.update.sys.config.conRemark.placeholder',
                  defaultMessage: '描述',
                })}/>
              </Form.Item>
            </Col>

          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default SysConfigUpdateForm;
