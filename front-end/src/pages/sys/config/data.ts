/**
 * SYS_CONFIG-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysConfigItem {
  conId: string;
  conName: string;
  conNameEn: string;
  conValue: string;
  conGroup: string;
  conParentId: string;
  conType: string;
  conCreateDate: Date;
  conUpdateDate: Date;
  conManager: string;
  conSpecies: string;
  conOrder: number;
  conState: string;
  conRemark: string;
  conParamType: string;
}

/**
 * SYS_CONFIG-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysConfigPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_CONFIG-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysConfigTableData {
  list: SysConfigItem[];
  pagination: Partial<SysConfigPagination>;
}

/**
 * SYS_CONFIG-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysConfigParams extends SysConfigItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };
  nconId?: string;
  cols?: string[];
  conParentIdList?: string[];
  conIdList?: string[];
}
