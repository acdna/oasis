import { CaretRightOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysConfigService } from '@/services/sys/sys.config.service';
import { SysConfigItem } from '@/pages/sys/config/data';
import { SysConfigHandles } from '@/pages/sys/config/handles';
import SysConfigCreateForm from '@/pages/sys/config/create.form';
import SysConfigUpdateForm from '@/pages/sys/config/update.form';
import { ConnectState } from '@/models/connect';
import DictMapper from '@/common/dictionary/dict.mapper';
import { Dispatch } from '@@/plugin-dva/connect';
import SysConfigItemCreateForm from '@/pages/sys/config/items/create.form';
import SysConfigItemUpdateForm from '@/pages/sys/config/items/update.form';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import {
  SYS_CONFIG_TYPE_DICT_GROUP,
  SYS_ENABLE_STATE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';
import UserMapper from '@/common/users/user.mapper';
import { SYS_USER_EFFECTS } from '@/pages/sys/user/effects';
import * as moment from "moment";
import {MLOCALES} from "@/common/locales/locale";

/**
 * SYS_CONFIG-属性对象
 * @author: jiangbin
 * @date: 2020-12-24 15:37:17
 **/
interface SysConfigTableListProps {
  dictMap: DictMapper;
  userMap: UserMapper;
  dispatch: Dispatch;
}

/**
 * SYS_CONFIG-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysConfigTableList: React.FC<SysConfigTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [createItemVisible, handleItemCreateVisible] = useState<boolean>(false);
  const [updateItemVisible, handleItemUpdateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysConfigItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysConfigItem[]>([]);
  const handles = new SysConfigHandles();
  const { userMap, dictMap, dispatch } = props;
  const intl = useIntl();

  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);
  SYS_USER_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysConfigItem>[] = [
    {
      dataIndex: 'conId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conName.label" defaultMessage="名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conName.tip',
        defaultMessage: '参数名称查询',
      })),
      dataIndex: 'conName',
      valueType: 'text',
      fixed: 'left',
      search: false,
      width: 230,
      sorter: (source, target) => (source.conName || '').localeCompare(target.conName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conName.tip" defaultMessage="参数名称" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conNameLike.label" defaultMessage="名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conNameLike.tip',
        defaultMessage: '参数名称模糊查询',
      })),
      dataIndex: 'conNameLike',
      valueType: 'text',
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conNameEn.label" defaultMessage="英文参数名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conNameEn.tip',
        defaultMessage: '英文参数名称查询',
      })),
      dataIndex: 'conNameEn',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conValue.label" defaultMessage="参数值" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conValue.tip',
        defaultMessage: '参数值查询',
      })),
      dataIndex: 'conValue',
      valueType: 'text',
      width: 200,
      ellipsis: true,
      sorter: (source, target) => (source.conValue || '').localeCompare(target.conValue || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conValue.tip" defaultMessage="参数值" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conGroup.label" defaultMessage="所属组" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conGroup.tip',
        defaultMessage: '所属组查询',
      })),
      dataIndex: 'conGroup',
      valueType: 'text',
      width: 150,
      sorter: (source, target) => (source.conGroup || '').localeCompare(target.conGroup || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conGroup.tip" defaultMessage="所属组" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.config.conParentId.label" defaultMessage="父级ID" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conParentId.tip',
        defaultMessage: '父级ID查询',
      })),
      dataIndex: 'conParentId',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conType.label" defaultMessage="节点类型" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conType.tip',
        defaultMessage: '参数节点类型查询',
      })),
      dataIndex: 'conType',
      valueType: 'select',
      search: false,
      hideInTable: true,
      hideInForm: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.config.conParamType.label" defaultMessage="参数类型" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conParamType.tip',
        defaultMessage: '参数类型查询',
      })),
      dataIndex: 'conParamType',
      valueType: 'select',
      sorter: (source, target) => (source.conParamType || '').localeCompare(target.conParamType || ''),
      renderText: (text) => dictMap.getItemName(SYS_CONFIG_TYPE_DICT_GROUP.group, text?.split(',')),
      valueEnum: dictMap.valueEnum(SYS_CONFIG_TYPE_DICT_GROUP.group),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conParamType.tip" defaultMessage="参数类型" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.config.conManager.label" defaultMessage="用户" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conManager.tip',
        defaultMessage: '用户查询',
      })),
      dataIndex: 'conManager',
      valueType: 'text',
      sorter: (source, target) => (source.conManager || '').localeCompare(target.conManager || ''),
      renderText: (text) => userMap.getUserName(text),
      valueEnum: userMap.valueEnum(),
      width: 120,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conManager.tip" defaultMessage="用户" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conSpecies.label" defaultMessage="种属" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conSpecies.tip',
        defaultMessage: '种属查询',
      })),
      dataIndex: 'conSpecies',
      valueType: 'text',
      width: 80,
      ellipsis: true,
      renderText: (text) => dictMap.getItemName(SYS_SAM_SPECIES_DICT_GROUP.group, text),
      valueEnum: dictMap.valueEnum(SYS_SAM_SPECIES_DICT_GROUP.group),
      sorter: (source, target) => (source.conSpecies || '').localeCompare(target.conSpecies || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conSpecies.tip" defaultMessage="种属" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conOrder.label" defaultMessage="排序" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conOrder.tip',
        defaultMessage: '排序查询',
      })),
      dataIndex: 'conOrder',
      valueType: 'digit',
      hideInForm: true,
      search: false,
      width: 80,
      sorter: (source, target) => ((source.conOrder) - (target.conOrder)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conOrder.tip" defaultMessage="排序" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conState.label" defaultMessage="状态" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conState.tip',
        defaultMessage: '状态查询',
      })),
      dataIndex: 'conState',
      valueType: 'text',
      renderText: (text) => dictMap.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, text),
      valueEnum: dictMap.valueEnum(SYS_ENABLE_STATE_DICT_GROUP.group),
      width: 80,
      sorter: (source, target) => (source.conState || '').localeCompare(target.conState || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.config.conState.tip" defaultMessage="状态" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.config.conRemark.label" defaultMessage="描述" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conRemark.tip',
        defaultMessage: '描述查询',
      })),
      dataIndex: 'conRemark',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,

    },
    {
      title: <FormattedMessage id="pages.list.sys.config.conCreateDate.label" defaultMessage="创建日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'conCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,

    },
    {
      title: <FormattedMessage id="pages.list.sys.config.conUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.config.conUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'conUpdateDate',
      valueType: 'date',
      width:150,
      search: false,
      hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
    },
    {
      title: <FormattedMessage id="pages.list.sys.config.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      align: 'center',
      width: 130,
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              if (record.conType === 'DIR') {
                handleUpdateVisible(true);
              } else {
                handleItemUpdateVisible(true);
              }
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.config.option.edit" defaultMessage="修改" />
          </a>

          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.config.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.config.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
          {(record.conType === 'DIR') && (<Divider type={'vertical'} />)}
          {(record.conType === 'DIR') && (
            <a
              onClick={() => {
                handleItemCreateVisible(true);
                setFormValues(record);
              }
              }
            >
              <FormattedMessage id="pages.list.sys.config.option.create.item" defaultMessage="新建" />
            </a>
          )}
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysConfigItem>
        headerTitle={<FormattedMessage id='pages.list.sys.config.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="conId"
        scroll={{ x: 1300 }}
        search={{ labelWidth: 120 }}
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.config.new" defaultMessage="新建" />
          </Button>,
          <Popconfirm title={<FormattedMessage id='pages.list.sys.config.option.init.front.confirm'
                                               defaultMessage='将先删除已有参数再进行初始化操作,请确定是否继续?' />}
                      onConfirm={async () => {
                        const success = await handles.initConfig();
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <Button type="dashed">
              <CaretRightOutlined /><FormattedMessage id="pages.list.sys.config.init" defaultMessage="初始化参数" />
            </Button>
          </Popconfirm>,
        ]}
        request={async (params, sorter, filter) => {

          const data = await SysConfigService.findPageDetail({
            ...params,
            sorter,
            filter,
            nconId: '1',
            conType: 'DIR',
          });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.config.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.config.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.config.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysConfigCreateForm onCancel={() => handleCreateVisible(false)}
                           onSubmit={async (value) => {
                             const success = await handles.add(value);
                             if (success) {
                               handleCreateVisible(false);
                               if (actionRef.current) {
                                 actionRef.current.reload();
                               }
                             }
                           }
                           }
                           visible={createVisible}
                           dictMap={dictMap}>
      </SysConfigCreateForm>

      {formValues && Object.keys(formValues).length > 0 ? (
        <SysConfigUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}

      <SysConfigItemCreateForm onCancel={() => handleItemCreateVisible(false)}
                               onSubmit={async (value) => {
                                 value.conType = 'DATA';
                                 if (!value.conState) value.conState = 'ON';
                                 const success = await handles.add(value);
                                 setFormValues({});
                                 if (success) {
                                   handleItemCreateVisible(false);
                                   if (actionRef.current) {
                                     actionRef.current.reload();
                                   }
                                 }
                               }
                               }
                               visible={createItemVisible}
                               dictMap={dictMap}
                               parent={formValues}
      >
      </SysConfigItemCreateForm>

      {(formValues && Object.keys(formValues).length > 0 && formValues['conType'] === 'DATA') ? (
        <SysConfigItemUpdateForm
          onSubmit={async (value: any) => {
            value.conType = 'DATA';
            if (!value.conState) value.conState = 'ON';
            const success = await handles.update(value);
            setFormValues({});
            if (success) {
              handleItemUpdateVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleItemUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateItemVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}

      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.conId && (
          <ProDescriptions<SysConfigItem>
            column={2}
            title={row?.conName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                conId: row?.conId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_dictionary, sys_user }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap, userMap: sys_user.userMap };
};

export default connect(mapStateToProps)(SysConfigTableList);
