import {SysConfigService} from '@/services/sys/sys.config.service';
import type {RuleObject, StoreValue} from 'rc-field-form/lib/interface';
import type {SysConfigParams} from '@/pages/sys/config/data';

/**
 * SYS_CONFIG记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 2021/1/23 下午11:47:35
 * */
export const SYS_CONFIG_FINDER = {
  /**
   * 参数名称-字段唯一性验证
   * @param conName 输入框输入的参数名称
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_NAME: async (conName: string, params: Partial<SysConfigParams> = {}) => {
    const res = await SysConfigService.findOne({
      ...params,
      conName,
    });
    return !res || res.conName !== conName;
  },
  /**
   * 参数名称-字段唯一性验证
   * @param conNameEn 输入框输入的参数名称
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_NAME_EN: async (conNameEn: string, params: Partial<SysConfigParams> = {}) => {
    const res = await SysConfigService.findOne({
      ...params,
      conNameEn,
    });
    return (!res || res.conNameEn !== conNameEn);
  },
  /**
   * 参数值-字段唯一性验证
   * @param conValue 输入框输入的参数值
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_VALUE: async (conValue: string, params: Partial<SysConfigParams> = {}) => {
    const res = await SysConfigService.findOne({
      ...params,
      conValue,
    });
    return (!res || res.conValue !== conValue);
  },
  /**
   * 所属组-字段唯一性验证
   * @param conGroup 输入框输入的所属组
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_GROUP: async (conGroup: string, params: Partial<SysConfigParams> = {}) => {
    const res = await SysConfigService.findOne({
      ...params,
      conGroup,
    });
    return (!res || res.conGroup !== conGroup);
  },
};
/**
 * SYS_CONFIG验证器
 * @author jiangbin
 * @date 2021/1/23 下午11:47:35
 * */
export const SYS_CONFIG_VALIDATOR = {
  /**
   * 参数名称-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_NAME: (error: string, params: Partial<SysConfigParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error?: string) => void) => {
        const res = await SYS_CONFIG_FINDER.CON_NAME(value, params);
        return res ? Promise.resolve() : Promise.reject(error);
      },
    };
  },
  /**
   * 参数名称-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_NAME_EN: async (error: string, params: Partial<SysConfigParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error?: string) => void) => {
        const res = await SYS_CONFIG_FINDER.CON_NAME_EN(value, params);
        return res ? Promise.resolve() : Promise.reject(error);
      },
    };
  },
  /**
   * 参数值-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_VALUE: (error: string, params: Partial<SysConfigParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error?: string) => void) => {
        const res = await SYS_CONFIG_FINDER.CON_VALUE(value, params);
        return res ? Promise.resolve() : Promise.reject(error);
      },
    };
  },
  /**
   * 所属组-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 2021/1/23 下午11:47:35
   * */
  CON_GROUP: (error: string, params: Partial<SysConfigParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error?: string) => void) => {
        const res = await SYS_CONFIG_FINDER.CON_GROUP(value, params);
        return res ? Promise.resolve() : Promise.reject(error);
      },
    };
  },

};
