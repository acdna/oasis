import React, {useState} from 'react';
import {Button, Col, Drawer, Form, Input, InputNumber, Row, Select} from 'antd';
import {FormattedMessage, useIntl} from 'umi';
import {SysConfigItem} from '@/pages/sys/config/data';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import DictMapper from '@/common/dictionary/dict.mapper';
import {options} from '@/components/common/select.options';
import {AuthContext} from '@/common/auth/auth.context';
import {
  SYS_CONFIG_TYPE_DICT_GROUP,
  SYS_ENABLE_STATE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';
import {SYS_CONFIG_VALIDATOR} from '@/pages/sys/config/validator';

/**
 * SYS_CONFIG-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysConfigItemUpdateFormProps {
  onSubmit: (fields: SysConfigItem) => void,
  visible: boolean;
  onCancel: () => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_CONFIG-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysConfigItemUpdateForm: React.FC<SysConfigItemUpdateFormProps> = (props) => {
  const {onSubmit, onCancel, visible, values, dictMap} = props;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    conId: values.conId,
    conName: values.conName,
    conNameEn: values.conNameEn,
    conValue: values.conValue,
    conGroup: values.conGroup,
    conParentId: values.conParentId,
    conParamType: values.conParamType,
    conType: 'DATA',
    conCreateDate: values.conCreateDate,
    conUpdateDate: values.conUpdateDate,
    conManager: AuthContext.loginName,
    conSpecies: values.conSpecies ? values.conSpecies.split(',') : undefined,
    conOrder: values.conOrder,
    conState: values.conState,
    conRemark: values.conRemark,

  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = {...formVals, ...fieldValues};//合并数据
    onSubmit({...values});
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const renderContent = () => (
    <>
      <Row gutter={16}>

        <Col span={12}>
          <Form.Item
            name="conName"
            label={<FormattedMessage id='pages.update.sys.config.conName.label' defaultMessage='参数名称'/>}
            rules={[
              {
                required: true,
                message: (<FormattedMessage id='pages.update.sys.config.conName.tip' defaultMessage='请输入参数名称！'/>),
              },
              SYS_CONFIG_VALIDATOR.CON_NAME(
                intl.formatMessage({
                  id: 'pages.update.sys.config.conName.valid',
                  defaultMessage: '参数名称已存在!',
                }), {conParentId: values.conParentId, nconId: values.conId, conType: 'DATA'}),
            ]}
          >
            <Input placeholder={intl.formatMessage({
              id: 'pages.sys.config.update.conName.tip',
              defaultMessage: '请输入参数名称！',
            })}/>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="conNameEn"
            label={<FormattedMessage id='pages.update.sys.config.conNameEn.label' defaultMessage='英文参数名称'/>}
            rules={[
              {
                required: true,
                message: (<FormattedMessage id='pages.update.sys.config.conNameEn.tip' defaultMessage='请输入英文参数名称！'/>),
              },
              SYS_CONFIG_VALIDATOR.CON_NAME(
                intl.formatMessage({
                  id: 'pages.update.sys.config.conNameEn.valid',
                  defaultMessage: '英文参数名称已存在!',
                }), {conParentId: values.conParentId, nconId: values.conId, conType: 'DATA'}),
            ]}
          >
            <Input placeholder={intl.formatMessage({
              id: 'pages.sys.config.update.conNameEn.tip',
              defaultMessage: '请输入英文参数名称！',
            })}/>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>

        <Col span={12}>
          <Form.Item
            name="conValue"
            label={<FormattedMessage id='pages.update.sys.config.conValue.label' defaultMessage='参数值'/>}
            rules={[
              {
                required: true,
                message: (<FormattedMessage id='pages.update.sys.config.conValue.tip' defaultMessage='请输入参数值！'/>),
              },
              SYS_CONFIG_VALIDATOR.CON_VALUE(
                intl.formatMessage({
                  id: 'pages.update.sys.config.conValue.valid',
                  defaultMessage: '参数值已存在!',
                }), {conParentId: values.conParentId, nconId: values.conId, conType: 'DATA'}),
            ]}
          >
            <Input placeholder={intl.formatMessage({
              id: 'pages.sys.config.update.conValue.tip',
              defaultMessage: '请输入参数值！',
            })}/>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="conGroup"
            label={<FormattedMessage id='pages.update.sys.config.conGroup.label' defaultMessage='所属组'/>}
          >
            <Input readOnly={true} placeholder={intl.formatMessage({
              id: 'pages.sys.config.update.conGroup.tip',
              defaultMessage: '请输入所属组！',
            })}/>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>

        <Col span={12}>
          <Form.Item
            name="conSpecies"
            label={<FormattedMessage id='pages.update.sys.config.conSpecies.label' defaultMessage='种属'/>}
          >
            <Select
              mode='multiple'
              style={{width: '100%'}}
              placeholder={intl.formatMessage({
                id: 'pages.update.sys.config.conSpecies.placeholder',
                defaultMessage: '种属',
              })}
            >
              {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
            </Select>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="conParamType"
            label={intl.formatMessage({
              id: 'pages.create.sys.config.conParamType.label',
              defaultMessage: '参数类型',
            })}
            rules={[{
              required: true,
              message: (intl.formatMessage({
                id: 'pages.create.sys.config.conParamType.tip',
                defaultMessage: '参数类型是必填项!',
              })),
            }]}
          >
            <Select disabled
                    style={{width: '100%'}}
                    placeholder={intl.formatMessage({
                      id: 'pages.update.sys.config.conParamType.placeholder',
                      defaultMessage: '参数类型',
                    })}
            >
              {options(SYS_CONFIG_TYPE_DICT_GROUP.group, dictMap)}
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="conOrder"
            label={<FormattedMessage id='pages.update.sys.config.conOrder.label' defaultMessage='排序'/>}
            rules={[
              {
                required: true,
                message: (<FormattedMessage id='pages.update.sys.config.conOrder.tip' defaultMessage='请输入排序！'/>),
              },
            ]}
          >
            <InputNumber placeholder={intl.formatMessage({
              id: 'pages.sys.config.update.conOrder.tip',
              defaultMessage: '请输入排序！',
            })} style={{width: '100%'}}/>
          </Form.Item>
        </Col>

        <Col span={12}>
          <Form.Item
            name="conState"
            label={<FormattedMessage id='pages.update.sys.config.conState.label' defaultMessage='状态'/>}
            rules={[
              {
                required: true,
                message: (<FormattedMessage id='pages.update.sys.config.conState.tip' defaultMessage='请输入状态！'/>),
              },
            ]}
          >
            <Select
              style={{width: '100%'}}
              placeholder={intl.formatMessage({
                id: 'pages.update.sys.config.conState.placeholder',
                defaultMessage: '启用?',
              })}
            >
              {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>

        <Col span={24}>
          <Form.Item
            name="conRemark"
            label={<FormattedMessage id='pages.update.sys.config.conRemark.label' defaultMessage='描述'/>}
          >
            <TextArea placeholder={intl.formatMessage({
              id: 'pages.sys.config.update.conRemark.tip',
              defaultMessage: '请输入描述！',
            })}/>
          </Form.Item>
        </Col>
      </Row>
    </>
  );

  return (
    <Drawer
      destroyOnClose
      title={intl.formatMessage({id: 'pages.update.sys.config.form.item.title', defaultMessage: '修改参数项'})}
      width={720}
      onClose={onCancel}
      visible={visible}
      bodyStyle={{paddingBottom: 80}}
      closable={true}
      footer={
        <div
          style={{textAlign: 'right'}}
        >
          <Button onClick={() => handleCancel()} style={{marginRight: 8}}>
            {intl.formatMessage({id: 'pages.update.sys.config.cancel', defaultMessage: '取消'})}
          </Button>
          <Button onClick={() => handleOk()} type="primary">
            {intl.formatMessage({id: 'pages.update.sys.config.submit', defaultMessage: '提交'})}
          </Button>
        </div>
      }
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            conId: formVals.conId,
            conName: formVals.conName,
            conNameEn: formVals.conNameEn,
            conValue: formVals.conValue,
            conGroup: formVals.conGroup,
            conParentId: formVals.conParentId,
            conType: formVals.conType,
            conCreateDate: formVals.conCreateDate,
            conUpdateDate: formVals.conUpdateDate,
            conManager: formVals.conManager,
            conSpecies: formVals.conSpecies,
            conOrder: formVals.conOrder,
            conState: formVals.conState,
            conRemark: formVals.conRemark,
            conParamType: formVals.conParamType,
          }
        }
      >
        {renderContent()}
      </ProForm>

    </Drawer>
  );
};

export default SysConfigItemUpdateForm;
