import {SysConfigItem} from '@/pages/sys/config/data';
import React from 'react';
import {useIntl} from 'umi';
import {Button, Col, Drawer, Form, Input, InputNumber, Row, Select} from 'antd';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import {AuthContext} from '@/common/auth/auth.context';
import {options} from '@/components/common/select.options';
import TextArea from 'antd/es/input/TextArea';
import {
  SYS_CONFIG_TYPE_DICT_GROUP,
  SYS_ENABLE_STATE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';
import {SYS_CONFIG_VALIDATOR} from '@/pages/sys/config/validator';

/**
 * SYS_CONFIG-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 * */
interface SysConfigItemCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysConfigItem) => void;
  dictMap: DictMapper;
  parent: any;
}

/**
 * SYS_CONFIG-创建记录表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const SysConfigItemCreateForm: React.FC<SysConfigItemCreateFormProps> = (props) => {
  const {visible, onCancel, onSubmit, dictMap, parent} = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {
      conId,
      conName,
      conNameEn,
      conValue,
      conCreateDate,
      conUpdateDate,
      conSpecies,
      conOrder,
      conState,
      conRemark,
    } = fieldValues;
    onSubmit({
      conId,
      conName,
      conNameEn,
      conValue,
      conParamType: parent.conParamType,
      conGroup: parent.conGroup,
      conParentId: parent.conId,
      conType: 'DATA',
      conCreateDate,
      conUpdateDate,
      conManager: AuthContext.loginName,
      conSpecies,
      conOrder,
      conState,
      conRemark,
    });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/6/2021, 2:23:40 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  // @ts-ignore
  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({id: 'pages.create.sys.config.form.item.title', defaultMessage: '新建参数项'})}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{paddingBottom: 80}}
        closable={true}
        footer={
          <div
            style={{textAlign: 'right'}}
          >
            <Button onClick={() => handleCancel()} style={{marginRight: 8}}>
              {intl.formatMessage({id: 'pages.create.sys.config.form.item.cancel', defaultMessage: '取消'})}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({id: 'pages.create.sys.config.form.item.submit', defaultMessage: '提交'})}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}
                 initialValues={{
                   conState: 'ON',
                   conOrder: 1,
                   conGroup: parent.conGroup,
                   conParamType: parent.conParamType,
                 }}>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="conName"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conName.label',
                  defaultMessage: '参数名称',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.config.conName.tip',
                      defaultMessage: '参数名称是必填项!',
                    })),
                  },
                  SYS_CONFIG_VALIDATOR.CON_NAME(
                    intl.formatMessage({
                      id: 'pages.update.sys.config.conName.valid',
                      defaultMessage: '参数名称已存在!',
                    }), {conParentId: parent.conId, conType: 'DATA'}),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.config.conName.placeholder',
                  defaultMessage: '参数名称',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="conNameEn"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conNameEn.label',
                  defaultMessage: '英文参数名称',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.config.conNameEn.tip',
                      defaultMessage: '英文参数名称是必填项!',
                    })),
                  },
                  SYS_CONFIG_VALIDATOR.CON_NAME_EN(
                    intl.formatMessage({
                      id: 'pages.update.sys.config.conNameEn.valid',
                      defaultMessage: '英文参数名称已存在!',
                    }), {conParentId: parent.conId, conType: 'DATA'}),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.config.conNameEn.placeholder',
                  defaultMessage: '英文参数名称',
                })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="conValue"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conValue.label',
                  defaultMessage: '参数值',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.config.conValue.tip',
                      defaultMessage: '参数值是必填项!',
                    })),
                  },
                  SYS_CONFIG_VALIDATOR.CON_VALUE(
                    intl.formatMessage({
                      id: 'pages.update.sys.config.conValue.valid',
                      defaultMessage: '参数值已存在!',
                    }), {conParentId: parent.conId, conType: 'DATA'}),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.config.conValue.placeholder',
                  defaultMessage: '参数值',
                })}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="conGroup"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conGroup.label',
                  defaultMessage: '所属组',
                })}
              >
                <Input readOnly={true}
                       placeholder={intl.formatMessage({
                         id: 'pages.create.sys.config.conGroup.placeholder',
                         defaultMessage: '所属组',
                       })}/>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="conSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conSpecies.label',
                  defaultMessage: '种属',
                })}
              >
                <Select
                  mode='multiple'
                  style={{width: '100%'}}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.config.conSpecies.placeholder',
                    defaultMessage: '种属',
                  })}
                >
                  {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="conParamType"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conParamType.label',
                  defaultMessage: '参数类型',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.config.conParamType.tip',
                    defaultMessage: '参数类型是必填项!',
                  })),
                }]}
              >
                <Select disabled
                        style={{width: '100%'}}
                        placeholder={intl.formatMessage({
                          id: 'pages.create.sys.config.conParamType.placeholder',
                          defaultMessage: '参数类型',
                        })}
                >
                  {options(SYS_CONFIG_TYPE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="conOrder"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conOrder.label',
                  defaultMessage: '排序',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.config.conOrder.tip',
                    defaultMessage: '排序是必填项!',
                  })),
                }]}
              >
                <InputNumber placeholder={intl.formatMessage({
                  id: 'pages.create.sys.config.conOrder.placeholder',
                  defaultMessage: '排序',
                })} style={{width: '100%'}}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="conState"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conState.label',
                  defaultMessage: '状态',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.config.conState.tip',
                    defaultMessage: '状态是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{width: '100%'}}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.config.conState.placeholder',
                    defaultMessage: '状态',
                  })}
                >
                  {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={24}>
              <Form.Item
                name="conRemark"
                label={intl.formatMessage({
                  id: 'pages.create.sys.config.conRemark.label',
                  defaultMessage: '描述',
                })}
              >
                <TextArea placeholder={intl.formatMessage({
                  id: 'pages.create.sys.config.conRemark.placeholder',
                  defaultMessage: '描述',
                })}/>
              </Form.Item>
            </Col>

          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default SysConfigItemCreateForm;
