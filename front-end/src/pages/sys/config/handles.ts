import {SysConfigItem} from '@/pages/sys/config/data';
import {message} from 'antd';
import {SysConfigService} from '@/services/sys/sys.config.service';
import {LocaleContext} from "@/common/locales/locale";

/**
 * SYS_CONFIG-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export class SysConfigHandles {

  /**
   * 添加记录
   * @param fields
   */
  add = async (fields: SysConfigItem) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.sys.config.add.loading',
      defaultMessage: '正在添加',
    }));
    try {
      await SysConfigService.add({...fields});
      hide();
      message.success(LocaleContext.message({
        id: 'page.sys.config.add.success',
        defaultMessage: '添加成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.sys.config.add.error',
        defaultMessage: '添加失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 更新记录
   * @param fields 更新信息
   */
  update = async (fields: SysConfigItem) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.sys.config.update.loading',
      defaultMessage: '正在更新',
    }));
    try {
      await SysConfigService.update(fields);
      if (fields.conType === 'DIR') {
        //查找子结点列表
        let items: [] = await SysConfigService.findList({
          cols: ['conId'],
          conParentId: fields.conId,
          conType: 'DATA',
        });
        if (items?.length >= 0) {//更新子结点启用状态
          let childrenIds = items.map(item => item['conId']);
          await SysConfigService.updateByIds({
            cols: ['conState', 'conParamType', 'conGroup'],
            conState: fields.conState,
            conParamType: fields.conParamType,
            conGroup: fields.conGroup,
            conIdList: childrenIds,
          });
        }
      }
      hide();
      message.success(LocaleContext.message({
        id: 'page.sys.config.update.success',
        defaultMessage: '更新成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.sys.config.update.error',
        defaultMessage: '更新失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param selectedRows 选中行记录列表
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteByIds = async (selectedRows: SysConfigItem[]) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.sys.config.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!selectedRows) return true;
    try {
      let dirIds = selectedRows.filter(row => row.conType === 'DIR').map(row => row.conId);
      dirIds?.length > 0 && await SysConfigService.delete({conParentIdList: dirIds});
      await SysConfigService.deleteByIds(selectedRows.map(row => row.conId));
      hide();
      message.success(LocaleContext.message({
        id: 'page.sys.config.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.sys.config.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param record 被删除的记录
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteById = async (record: SysConfigItem) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.sys.config.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!record) return true;
    try {
      if (record.conType === 'DIR') {
        await SysConfigService.delete({conParentId: record.conId});
      }
      await SysConfigService.deleteById(record.conId);
      hide();
      message.success(LocaleContext.message({
        id: 'page.sys.config.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.sys.config.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 初始化参数
   * @author: jiangbin
   * @date: 2020-12-31 21:21:39
   **/
  initConfig = async () => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.sys.config.init.loading',
      defaultMessage: '正在初始化参数',
    }));
    try {
      await SysConfigService.init();
      hide();
      message.success(LocaleContext.message({
        id: 'page.sys.config.init.success',
        defaultMessage: '初始化参数成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.sys.config.init.error',
        defaultMessage: '初始化参数失败，请重试!',
      }));
      return false;
    }
  };
};
