import { SysUserItem } from '@/pages/sys/user/data';
import React, { useState } from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, InputNumber, Row, Select } from 'antd';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options, recordOptions } from '@/components/common/select.options';
import TextArea from 'antd/es/input/TextArea';
import { SysRoleData } from '@/models/sys/sys.role.model';
import {
  SYS_ENABLE_STATE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
  SYS_USER_SEX_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';
import { SYS_USER_VALIDATOR } from '@/pages/sys/user/validator';

/**
 * SYS_USER-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 1/1/2021, 7:17:26 PM
 **/
interface SysUserUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysUserItem) => void;
  values: any;
  dictMap: DictMapper;
  roles: SysRoleData[];
}

/**
 * SYS_USER-更新字典项信息表单
 * @author jiangbin
 * @date 1/1/2021, 7:17:26 PM
 **/
const SysUserUpdateForm: React.FC<SysUserUpdateFormProps> = (props) => {
  const { values, visible, onCancel, onSubmit, dictMap, roles } = props;
  const intl = useIntl();

  const [formVals] = useState({
    userId: values.userId,
    userLoginName: values.userLoginName,
    userPassword: values.userPassword,
    userPasswordPrivate: values.userPasswordPrivate,
    userNameAbbr: values.userNameAbbr,
    userName: values.userName,
    userNameEn: values.userNameEn,
    userSex: values.userSex,
    userPhone: values.userPhone,
    userEmail: values.userEmail,
    userImageUrl: values.userImageUrl,
    userOrder: values.userOrder,
    userState: values.userState,
    userCode: values.userCode,
    userCreateDate: values.userCreateDate,
    userUpdateDate: values.userUpdateDate,
    userLastLoginTime: values.userLastLoginTime,
    userLastLoginIp: values.userLastLoginIp,
    userUnit: values.userUnit,
    userJobTitle: values.userJobTitle,
    userAddr: values.userAddr,
    userPostcode: values.userPostcode,
    userSamSpecies: values.userSamSpecies ? values.userSamSpecies.split(',') : undefined,
    userCurrSpecies: values.userCurrSpecies || undefined,
    userRoles: values.userRoles,
  });
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({ id: 'pages.update.sys.user.title', defaultMessage: '修改' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        closable={true}
        footer={
          <div
            style={{ textAlign: 'right' }}
          >
            <Button onClick={() => handleCancel()} style={{ marginRight: 8 }}>
              {intl.formatMessage({ id: 'pages.update.sys.user.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.update.sys.user.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form} initialValues={formVals}>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userLoginName"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userLoginName.label',
                  defaultMessage: '用户帐号',
                })}
                rules={[
                  {
                    min: 6,
                    max: 24,
                    message: (intl.formatMessage({
                      id: 'pages.update.sys.user.userLoginName.length.tip',
                      defaultMessage: '用户帐号长度必须为6~24个字符!',
                    })),
                  },
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.update.sys.user.userLoginName.tip',
                      defaultMessage: '用户帐号是必填项!',
                    })),
                  },
                  SYS_USER_VALIDATOR.USER_LOGIN_NAME(
                    intl.formatMessage({
                      id: 'pages.update.sys.user.userLoginName.valid.tip',
                      defaultMessage: '用户帐号已存在!',
                    }), { nuserId: values.userId },
                  ),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userLoginName.placeholder',
                  defaultMessage: '用户帐号',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userNameAbbr"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userNameAbbr.label',
                  defaultMessage: '姓名缩写',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userNameAbbr.placeholder',
                  defaultMessage: '姓名缩写',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userName"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userName.label',
                  defaultMessage: '用户姓名',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.user.userName.tip',
                    defaultMessage: '用户姓名是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userName.placeholder',
                  defaultMessage: '用户姓名',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userNameEn"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userNameEn.label',
                  defaultMessage: '英文名',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.user.userNameEn.tip',
                    defaultMessage: '英文名是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userNameEn.placeholder',
                  defaultMessage: '英文名',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userOrder"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userOrder.label',
                  defaultMessage: '用户排序',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.user.userOrder.tip',
                    defaultMessage: '用户排序是必填项!',
                  })),
                }]}
              >
                <InputNumber placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userOrder.placeholder',
                  defaultMessage: '用户排序',
                })} style={{width:'100%'}}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="userState"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userState.label',
                  defaultMessage: '状态',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.user.userState.tip',
                    defaultMessage: '状态是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.user.userSex.placeholder',
                    defaultMessage: '状态',
                  })
                  }
                >
                  {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="userSamSpecies"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userSamSpecies.label',
                  defaultMessage: '用户绑定种属',
                })}
              >
                <Select
                  mode={'multiple'}
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.user.userSamSpecies.placeholder',
                    defaultMessage: '用户绑定种属',
                  })
                  }
                >
                  {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="userRoles"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userRoles.label',
                  defaultMessage: '角色',
                })}
              >
                <Select
                  mode={'multiple'}
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.user.userRoles.placeholder',
                    defaultMessage: '角色',
                  })
                  }
                >
                  {recordOptions({
                    records: roles,
                    cnLabelKey: 'roleName',
                    enLabelKey: 'roleNameEn',
                    valueKey: 'roleId',
                  })}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="userCurrSpecies"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userCurrSpecies.label',
                  defaultMessage: '当前种属',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.user.userCurrSpecies.tip',
                    defaultMessage: '当前种属是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.user.userCurrSpecies.placeholder',
                    defaultMessage: '当前种属',
                  })
                  }
                >
                  {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userSex"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userSex.label',
                  defaultMessage: '性别',
                })}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.user.userSex.placeholder',
                    defaultMessage: '性别',
                  })
                  }
                >
                  {options(SYS_USER_SEX_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userPhone"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userPhone.label',
                  defaultMessage: '用户手机号',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userPhone.placeholder',
                  defaultMessage: '用户手机号',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userEmail"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userEmail.label',
                  defaultMessage: '用户的邮箱',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userEmail.placeholder',
                  defaultMessage: '用户的邮箱',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userImageUrl"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userImageUrl.label',
                  defaultMessage: '用户头像路径',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userImageUrl.placeholder',
                  defaultMessage: '用户头像路径',
                })} />
              </Form.Item>
            </Col>

          </Row>
          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userCode"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userCode.label',
                  defaultMessage: '用户代码',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userCode.placeholder',
                  defaultMessage: '用户代码',
                })} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="userPostcode"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userPostcode.label',
                  defaultMessage: '邮编',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userPostcode.placeholder',
                  defaultMessage: '邮编',
                })} />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userUnit"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userUnit.label',
                  defaultMessage: '用户所属单位',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userUnit.placeholder',
                  defaultMessage: '用户所属单位',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userJobTitle"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userJobTitle.label',
                  defaultMessage: '用户职务/职称',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userJobTitle.placeholder',
                  defaultMessage: '用户职务/职称',
                })} />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="userAddr"
                label={intl.formatMessage({
                  id: 'pages.update.sys.user.userAddr.label',
                  defaultMessage: '用户通讯地址',
                })}
              >
                <TextArea placeholder={intl.formatMessage({
                  id: 'pages.update.sys.user.userAddr.placeholder',
                  defaultMessage: '用户通讯地址',
                })} />
              </Form.Item>
            </Col>
          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default SysUserUpdateForm;
