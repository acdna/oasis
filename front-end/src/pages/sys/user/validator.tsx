import { SysUserService } from '@/services/sys/sys.user.service';
import { RuleObject, StoreValue } from 'rc-field-form/lib/interface';
import { SysUserParams } from '@/pages/sys/user/data';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * SYS_USER记录创建和更新相关后端验证功能
 * @author jiangbin
 * @date 1/25/2021, 8:03:05 PM
 **/
export const SYS_USER_FINDER = {
  /**
   * 用户登录名称-字段唯一性验证
   * @param userLoginName 输入框输入的用户登录名称
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_LOGIN_NAME: async (userLoginName: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userLoginName,
    });
    return (res && res.userLoginName == userLoginName);
  },
  /**
   * 用户密码-字段唯一性验证
   * @param userPassword 输入框输入的用户密码
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_PASSWORD: async (userPassword: string, params: Partial<SysUserParams> = {}) => {
    const newPassword = AuthContext.md5(userPassword);
    let res = await SysUserService.findOne({
      ...params,
      userPassword: newPassword,
    });
    return res && res.userPassword == newPassword;
  },
  /**
   * -字段唯一性验证
   * @param userPasswordPrivate 输入框输入的
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_PASSWORD_PRIVATE: async (userPasswordPrivate: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userPasswordPrivate,
    });
    return (res && res.userPasswordPrivate == userPasswordPrivate);
  },
  /**
   * 姓名缩写-字段唯一性验证
   * @param userNameAbbr 输入框输入的姓名缩写
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_NAME_ABBR: async (userNameAbbr: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userNameAbbr,
    });
    return (res && res.userNameAbbr == userNameAbbr);
  },
  /**
   * 用户名称-字段唯一性验证
   * @param userName 输入框输入的用户名称
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_NAME: async (userName: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userName,
    });
    return (res && res.userName == userName);
  },
  /**
   * 英文名-字段唯一性验证
   * @param userNameEn 输入框输入的英文名
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_NAME_EN: async (userNameEn: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userNameEn,
    });
    return (res && res.userNameEn == userNameEn);
  },
  /**
   * 用户的性别-字段唯一性验证
   * @param userSex 输入框输入的用户的性别
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_SEX: async (userSex: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userSex,
    });
    return (res && res.userSex == userSex);
  },
  /**
   * 用户手机号-字段唯一性验证
   * @param userPhone 输入框输入的用户手机号
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_PHONE: async (userPhone: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userPhone,
    });
    return (res && res.userPhone == userPhone);
  },
  /**
   * 用户的邮箱-字段唯一性验证
   * @param userEmail 输入框输入的用户的邮箱
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_EMAIL: async (userEmail: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userEmail,
    });
    return (res && res.userEmail == userEmail);
  },
  /**
   * 用户头像路径-字段唯一性验证
   * @param userImageUrl 输入框输入的用户头像路径
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_IMAGE_URL: async (userImageUrl: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userImageUrl,
    });
    return (res && res.userImageUrl == userImageUrl);
  },
  /**
   * 状态-字段唯一性验证
   * @param userState 输入框输入的状态
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_STATE: async (userState: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userState,
    });
    return (res && res.userState == userState);
  },
  /**
   * 用户代码-字段唯一性验证
   * @param userCode 输入框输入的用户代码
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_CODE: async (userCode: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userCode,
    });
    return (res && res.userCode == userCode);
  },
  /**
   * 用户最后登录Ip-字段唯一性验证
   * @param userLastLoginIp 输入框输入的用户最后登录Ip
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_LAST_LOGIN_IP: async (userLastLoginIp: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userLastLoginIp,
    });
    return (res && res.userLastLoginIp == userLastLoginIp);
  },
  /**
   * 用户所属单位-字段唯一性验证
   * @param userUnit 输入框输入的用户所属单位
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_UNIT: async (userUnit: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userUnit,
    });
    return (res && res.userUnit == userUnit);
  },
  /**
   * 用户职务/职称-字段唯一性验证
   * @param userJobTitle 输入框输入的用户职务/职称
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_JOB_TITLE: async (userJobTitle: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userJobTitle,
    });
    return (res && res.userJobTitle == userJobTitle);
  },
  /**
   * 用户通讯地址-字段唯一性验证
   * @param userAddr 输入框输入的用户通讯地址
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_ADDR: async (userAddr: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userAddr,
    });
    return (res && res.userAddr == userAddr);
  },
  /**
   * 邮编-字段唯一性验证
   * @param userPostcode 输入框输入的邮编
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_POSTCODE: async (userPostcode: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userPostcode,
    });
    return (res && res.userPostcode == userPostcode);
  },
  /**
   * 用户绑定种属-字段唯一性验证
   * @param userSamSpecies 输入框输入的用户绑定种属
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_SAM_SPECIES: async (userSamSpecies: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userSamSpecies,
    });
    return (res && res.userSamSpecies == userSamSpecies);
  },
  /**
   * 当前种属-字段唯一性验证
   * @param userCurrSpecies 输入框输入的当前种属
   * @param params 其它额外参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_CURR_SPECIES: async (userCurrSpecies: string, params: Partial<SysUserParams> = {}) => {
    let res = await SysUserService.findOne({
      ...params,
      userCurrSpecies,
    });
    return (res && res.userCurrSpecies == userCurrSpecies);
  },
};
/**
 * SYS_USER验证器
 * @author jiangbin
 * @date 1/25/2021, 8:03:05 PM
 **/
export const SYS_USER_VALIDATOR = {
  /**
   * 用户登录名称-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_LOGIN_NAME: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_LOGIN_NAME(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户密码-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_PASSWORD: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_PASSWORD(value, params);
        return res ? Promise.resolve() : Promise.reject(error);
      },
    };
  },
  /**
   * -字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_PASSWORD_PRIVATE: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_PASSWORD_PRIVATE(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 姓名缩写-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_NAME_ABBR: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_NAME_ABBR(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户名称-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_NAME: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_NAME(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 英文名-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_NAME_EN: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_NAME_EN(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户的性别-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_SEX: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_SEX(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户手机号-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_PHONE: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_PHONE(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户的邮箱-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_EMAIL: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_EMAIL(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户头像路径-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_IMAGE_URL: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_IMAGE_URL(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 状态-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_STATE: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_STATE(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户代码-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_CODE: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_CODE(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户最后登录Ip-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_LAST_LOGIN_IP: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_LAST_LOGIN_IP(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户所属单位-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_UNIT: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_UNIT(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户职务/职称-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_JOB_TITLE: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_JOB_TITLE(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户通讯地址-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_ADDR: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_ADDR(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 邮编-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_POSTCODE: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_POSTCODE(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 用户绑定种属-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_SAM_SPECIES: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_SAM_SPECIES(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
  /**
   * 当前种属-字段唯一性验证
   * @param error 若数据存在重复时显示的错误提示消息
   * @param params 其它参数
   * @author: jiangbin
   * @date: 1/25/2021, 8:03:05 PM
   **/
  USER_CURR_SPECIES: (error: string, params: Partial<SysUserParams> = {}) => {
    return {
      validator: async (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
        let res = await SYS_USER_FINDER.USER_CURR_SPECIES(value, params);
        return res ? Promise.reject(error) : Promise.resolve();
      },
    };
  },
};
