import { SysUserItem } from '@/pages/sys/user/data';
import { message } from 'antd';
import { SysUserService } from '@/services/sys/sys.user.service';
import { useIntl } from 'umi';
import { SysUserRoleService } from '@/services/sys/sys.user.role.service';
import { md5 } from '@/common/utils/system';

/**
 * SYS_USER-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
export class SysUserHandles {
  constructor(private readonly intl = useIntl()) {
  }

  /**
   * 添加记录
   * @param fields
   */
  add = async (fields: SysUserItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.user.add.loading',
      defaultMessage: '正在添加',
    }));
    try {
      //密码MD5加密
      let userPassword = md5(fields.userPassword);
      let userPasswordPrivate = md5(fields.userPasswordPrivate);
      await SysUserService.add({ ...fields, userPassword, userPasswordPrivate });
      //关联角色
      await this.relateRoles(fields.userLoginName, fields.userRoles);
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.user.add.success',
        defaultMessage: '添加成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.user.add.error',
        defaultMessage: '添加失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 更新记录
   * @param fields 更新信息
   */
  update = async (fields: SysUserItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.user.update.loading',
      defaultMessage: '正在更新',
    }));
    try {
      await SysUserService.update(fields);
      //保存角色关联信息
      await this.relateRoles(fields.userLoginName, fields.userRoles);
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.user.update.success',
        defaultMessage: '更新成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.user.update.error',
        defaultMessage: '更新失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param selectedRows 选中行记录列表
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteByIds = async (selectedRows: SysUserItem[]) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.user.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!selectedRows) return true;
    try {
      await SysUserService.deleteByIds(selectedRows.map(row => row.userId));

      //删除角色关联记录
      let userLoginNames = selectedRows.map(row => row.userLoginName);
      if (userLoginNames?.length > 0) {
        await SysUserRoleService.delete({ urUserLoginNameList: userLoginNames });
      }

      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.user.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.user.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param record 被删除的记录
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteById = async (record: SysUserItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.user.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!record) return true;
    try {
      await SysUserService.deleteById(record.userId);
      //删除角色关联信息
      if (record.userLoginName) {
        await SysUserRoleService.delete({ urUserLoginName: record.userLoginName });
      }
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.user.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.user.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 关联用户和角色
   * @param userLoginName 用户登录帐号
   * @param userRoleIds 角色ID列表
   * @author: jiangbin
   * @date: 2021-01-02 09:57:01
   **/
  relateRoles = async (userLoginName: string = '', userRoleIds: string[] = []) => {
    if (userLoginName.length == 0 || userRoleIds.length == 0) return;
    await SysUserRoleService.delete({ urUserLoginName: userLoginName });
    let records = userRoleIds.map(roleId => {
      return { urId: '', urUserLoginName: userLoginName, urRoleId: roleId };
    });
    await SysUserRoleService.addList(records);
  };
};
