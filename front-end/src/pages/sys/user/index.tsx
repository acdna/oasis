import { UserAddOutlined, UsergroupDeleteOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, Dispatch, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysUserService } from '@/services/sys/sys.user.service';
import { SysUserItem } from '@/pages/sys/user/data';
import { SysUserHandles } from '@/pages/sys/user/handles';
import SysUserCreateForm from '@/pages/sys/user/create.form';
import SysUserUpdateForm from '@/pages/sys/user/update.form';
import * as moment from 'moment';
import { dictValueEnum, mapValue2LabelStr } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import { ConnectState } from '@/models/connect';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import { SYS_ROLE_EFFECTS } from '@/pages/sys/role/effects';
import { SysRoleData } from '@/models/sys/sys.role.model';
import { MLOCALES } from '@/common/locales/locale';
import {
  SYS_ENABLE_STATE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
  SYS_USER_SEX_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_DICTIONARY-属性对象
 * @author: jiangbin
 * @date: 2020-12-24 15:37:17
 **/
interface SysUserTableListProps {
  dictMap: DictMapper;
  dispatch: Dispatch;
  roles: SysRoleData[];
}

/**
 * SYS_USER-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserTableList: React.FC<SysUserTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysUserItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysUserItem[]>([]);
  const handles = new SysUserHandles();
  const { dictMap, dispatch, roles } = props;
  const intl = useIntl();

  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);
  SYS_ROLE_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysUserItem>[] = [
    {
      dataIndex: 'userId',
      valueType: 'text',
      sorter: false,
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userUnit.label" defaultMessage="单位" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userUnit.tip',
        defaultMessage: '用户所属单位查询',
      })),
      dataIndex: 'userUnit',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userLoginName.label" defaultMessage="登录帐号" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userLoginName.tip',
        defaultMessage: '登录帐号查询',
      })),
      dataIndex: 'userLoginName',
      valueType: 'text',
      search: false,
      width: 150,
      fixed: 'left',
      sorter: (source, target) => (source.userLoginName || '').localeCompare(target.userLoginName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.user.userLoginName.tip" defaultMessage="登录帐号" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userLoginNameLike.label" defaultMessage="登录帐号" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userLoginNameLike.tip',
        defaultMessage: '登录名称模糊查询',
      })),
      dataIndex: 'userLoginNameLike',
      valueType: 'text',
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userName.label" defaultMessage="姓名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userName.tip',
        defaultMessage: '姓名查询',
      })),
      dataIndex: 'userName',
      valueType: 'text',
      search: false,
      width: 120,
      sorter: (source, target) => (source.userName || '').localeCompare(target.userName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.user.userName.tip" defaultMessage="姓名" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userNameLike.label" defaultMessage="姓名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userNameLike.tip',
        defaultMessage: '姓名模糊查询',
      })),
      dataIndex: 'userNameLike',
      valueType: 'text',
      hideInDescriptions: true,
      hideInTable: true,
      hideInForm: false,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userNameEn.label" defaultMessage="英文名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userNameEn.tip',
        defaultMessage: '英文名查询',
      })),
      dataIndex: 'userNameEn',
      valueType: 'text',
      hideInForm: true,
      hideInTable: true,
      sorter: (source, target) => (source.userNameEn || '').localeCompare(target.userNameEn || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.user.userNameEn.tip" defaultMessage="英文名" />
            ),
          },
        ],
      },
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userNameAbbr.label" defaultMessage="姓名缩写" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userNameAbbr.tip',
        defaultMessage: '姓名缩写查询',
      })),
      dataIndex: 'userNameAbbr',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.userNameAbbr || '').localeCompare(target.userNameAbbr || ''),
      search: false,
      hideInForm: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userSex.label" defaultMessage="性别" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userSex.tip',
        defaultMessage: '性别查询',
      })),
      dataIndex: 'userSex',
      valueType: 'text',
      width: 150,
      sorter: (source, target) => (source.userSex || '').localeCompare(target.userSex || ''),
      renderText: (text) => dictMap.getItemName(SYS_USER_SEX_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_USER_SEX_DICT_GROUP.group))),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.user.userSex.tip" defaultMessage="性别" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userPhone.label" defaultMessage="手机号" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userPhone.tip',
        defaultMessage: '用户手机号查询',
      })),
      dataIndex: 'userPhone',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userEmail.label" defaultMessage="邮箱" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userEmail.tip',
        defaultMessage: '用户的邮箱查询',
      })),
      dataIndex: 'userEmail',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userImageUrl.label" defaultMessage="用户头像路径" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userImageUrl.tip',
        defaultMessage: '用户头像路径查询',
      })),
      dataIndex: 'userImageUrl',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userOrder.label" defaultMessage="排序" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userOrder.tip',
        defaultMessage: '排序查询',
      })),
      dataIndex: 'userOrder',
      valueType: 'digit',
      hideInForm: true,
      search: false,
      width: 150,
      sorter: (source, target) => ((source.userOrder) - (target.userOrder)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.user.userOrder.tip" defaultMessage="排序" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userSamSpecies.label" defaultMessage="绑定种属" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userSamSpecies.tip',
        defaultMessage: '用户绑定种属查询',
      })),
      dataIndex: 'userSamSpecies',
      valueType: 'text',
      ellipsis: true,
      renderText: (text) => dictMap.getItemNamesStr(SYS_SAM_SPECIES_DICT_GROUP.group, text?.split(',')),
      search: false,
      hideInForm: true,
      width: 200,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userCurrSpecies.label" defaultMessage="当前种属" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userCurrSpecies.tip',
        defaultMessage: '当前种属查询',
      })),
      dataIndex: 'userCurrSpecies',
      valueType: 'text',
      renderText: (text) => dictMap.getItemName(SYS_SAM_SPECIES_DICT_GROUP.group, text),
      search: false,
      hideInForm: true,
      hideInTable: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userRoles.label" defaultMessage="绑定角色" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userRoles.tip',
        defaultMessage: '绑定角色',
      })),
      dataIndex: 'userRoles',
      valueType: 'text',
      width: 200,
      ellipsis: true,
      renderText: (text) => mapValue2LabelStr({
        records: roles,
        values: text,
        valueKey: 'roleId',
        cnLabelKey: 'roleName',
        enLabelKey: 'roleNameEn',
      }),
      search: false,
      hideInForm: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userState.label" defaultMessage="状态" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userState.tip',
        defaultMessage: '状态查询',
      })),
      dataIndex: 'userState',
      valueType: 'text',
      width: 100,
      sorter: (source, target) => (source.userState || '').localeCompare(target.userState || ''),
      renderText: (text) => dictMap.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_ENABLE_STATE_DICT_GROUP.group))),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.user.userState.tip" defaultMessage="状态" />
            ),
          },
        ],
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userCode.label" defaultMessage="用户代码" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userCode.tip',
        defaultMessage: '用户代码查询',
      })),
      dataIndex: 'userCode',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userCreateDate.label" defaultMessage="创建日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'userCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'userUpdateDate',
      valueType: 'date',
      search: false,
      width: 150,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: false,
            message: (
              <FormattedMessage id="pages.list.sys.user.userUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userLastLoginTime.label" defaultMessage="用户最后登录时间" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userLastLoginTime.tip',
        defaultMessage: '用户最后登录时间查询',
      })),
      dataIndex: 'userLastLoginTime',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userLastLoginIp.label" defaultMessage="用户最后登录Ip" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userLastLoginIp.tip',
        defaultMessage: '用户最后登录Ip查询',
      })),
      dataIndex: 'userLastLoginIp',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userJobTitle.label" defaultMessage="用户职务/职称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userJobTitle.tip',
        defaultMessage: '用户职务/职称查询',
      })),
      dataIndex: 'userJobTitle',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.user.userPostcode.label" defaultMessage="邮编" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userPostcode.tip',
        defaultMessage: '邮编查询',
      })),
      dataIndex: 'userPostcode',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.userAddr.label" defaultMessage="用户通讯地址" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.user.userAddr.tip',
        defaultMessage: '用户通讯地址查询',
      })),
      dataIndex: 'userAddr',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.user.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      align:'center',
      width: 100,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.user.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.user.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.user.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysUserItem>
        headerTitle={<FormattedMessage id='pages.list.sys.user.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="userId"
        search={
          { labelWidth: 120 }
        }
        scroll={{ x: 1300 }}
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <UserAddOutlined /> <FormattedMessage id="pages.list.sys.user.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysUserService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      { selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.user.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.user.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <UsergroupDeleteOutlined /> <FormattedMessage id="pages.list.sys.user.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysUserCreateForm onCancel={() => handleCreateModalVisible(false)}
                         onSubmit={async (value) => {
                           const success = await handles.add(value);
                           if (success) {
                             handleCreateModalVisible(false);
                             if (actionRef.current) {
                               actionRef.current.reload();
                             }
                           }
                         }
                         }
                         roles={roles}
                         visible={createModalVisible}
                         dictMap={dictMap}>
      </SysUserCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysUserUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          roles={roles}
          visible={updateModalVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.userId && (
          <ProDescriptions<SysUserItem>
            column={2}
            title={row?.userName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                userId: row?.userId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_dictionary, sys_role }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap, roles: sys_role.roles };
};

export default connect(mapStateToProps)(SysUserTableList);
