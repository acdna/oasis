/**
 * SYS_USER-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserItem {
  userId: string;
  userLoginName: string;
  userPassword: string;
  userPasswordPrivate: string;
  userNameAbbr: string;
  userName: string;
  userNameEn: string;
  userSex: string;
  userPhone: string;
  userEmail: string;
  userImageUrl: string;
  userOrder: number;
  userState: string;
  userCode: string;
  userCreateDate: Date;
  userUpdateDate: Date;
  userLastLoginTime: Date;
  userLastLoginIp: string;
  userUnit: string;
  userJobTitle: string;
  userAddr: string;
  userPostcode: string;
  userSamSpecies: string;
  userCurrSpecies: string;

  userRoles?: string[];
  cols?: string[];

  userPasswordNew?: string;
  userPasswordRepeat?: string;
}

/**
 * SYS_USER-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_USER-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserTableData {
  list: SysUserItem[];
  pagination: Partial<SysUserPagination>;
}

/**
 * SYS_USER-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserParams extends SysUserItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };

  nuserId?: string;
}
