import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysUserTopUpService } from '@/services/sys/sys.user.top.up.service';
import { SysUserTopUpItem } from '@/pages/sys/user/top/up/data';
import { SysUserTopUpHandles } from '@/pages/sys/user/top/up/handles';
import SysUserTopUpCreateForm from '@/pages/sys/user/top/up/create.form';
import SysUserTopUpUpdateForm from '@/pages/sys/user/top/up/update.form';
import * as moment from 'moment';
import { MLOCALES } from '@/common/locales/locale';

/**
 * SYS_USER_TOP_UP-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserTopUpTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysUserTopUpItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysUserTopUpItem[]>([]);
    const handles = new SysUserTopUpHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysUserTopUpItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'utuId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuId.label" defaultMessage="id" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuId.tip',
                defaultMessage: 'id查询',
            })),
            dataIndex: 'utuId',
            valueType:'text',
             sorter: (source, target) => (source.utuId||'').localeCompare(target.utuId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuId.tip" defaultMessage="id" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuLoginName.label" defaultMessage="用户登录名" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuLoginName.tip',
                defaultMessage: '用户登录名查询',
            })),
            dataIndex: 'utuLoginName',
            valueType:'text',
             sorter: (source, target) => (source.utuLoginName||'').localeCompare(target.utuLoginName||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuLoginName.tip" defaultMessage="用户登录名" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuSerialNumber.label" defaultMessage="序列号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuSerialNumber.tip',
                defaultMessage: '序列号查询',
            })),
            dataIndex: 'utuSerialNumber',
            valueType:'text',
             sorter: (source, target) => (source.utuSerialNumber||'').localeCompare(target.utuSerialNumber||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuSerialNumber.tip" defaultMessage="序列号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuBillingStartDate.label" defaultMessage="计费开始时间" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuBillingStartDate.tip',
                defaultMessage: '计费开始时间查询',
            })),
            dataIndex: 'utuBillingStartDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuBillingStartDate.tip" defaultMessage="计费开始时间" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuBillingEndDate.label" defaultMessage="计费结束时间" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuBillingEndDate.tip',
                defaultMessage: '计费结束时间查询',
            })),
            dataIndex: 'utuBillingEndDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuBillingEndDate.tip" defaultMessage="计费结束时间" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuState.label" defaultMessage="状态" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuState.tip',
                defaultMessage: '状态查询',
            })),
            dataIndex: 'utuState',
            valueType:'text',
             sorter: (source, target) => (source.utuState||'').localeCompare(target.utuState||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuState.tip" defaultMessage="状态" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.utuCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.top.up.utuCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'utuCreateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.top.up.utuCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.top.up.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.user.top.up.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.user.top.up.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.user.top.up.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysUserTopUpItem>
                headerTitle={<FormattedMessage id='pages.list.sys.user.top.up.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="utuId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.user.top.up.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysUserTopUpService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.user.top.up.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.user.top.up.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.user.top.up.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysUserTopUpCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysUserTopUpCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysUserTopUpUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.utuId && (
                    <ProDescriptions<SysUserTopUpItem>
                        column={2}
                        title={row?.utuId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            utuId: row?.utuId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysUserTopUpTableList;
