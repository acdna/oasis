import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysUserTopUpItem } from "@/pages/sys/user/top/up/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_USER_TOP_UP-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysUserTopUpCreateFormProps {
    onSubmit: (fields: SysUserTopUpItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_USER_TOP_UP-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserTopUpCreateForm: React.FC<SysUserTopUpCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { utuId,utuLoginName,utuSerialNumber,utuBillingStartDate,utuBillingEndDate,utuState,utuCreateDate }=fieldValues;
        onSubmit({ utuId,utuLoginName,utuSerialNumber,utuBillingStartDate,utuBillingEndDate,utuState,utuCreateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.user.top.up.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="utuId"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuId.label" defaultMessage="id"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuId.tip" defaultMessage="id为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuId.placeholder",defaultMessage:"id"})}/>
                </Form.Item>
                
                <Form.Item
                        name="utuLoginName"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuLoginName.label" defaultMessage="用户登录名"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuLoginName.tip" defaultMessage="用户登录名为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuLoginName.placeholder",defaultMessage:"用户登录名"})}/>
                </Form.Item>
                
                <Form.Item
                        name="utuSerialNumber"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuSerialNumber.label" defaultMessage="序列号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuSerialNumber.tip" defaultMessage="序列号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuSerialNumber.placeholder",defaultMessage:"序列号"})}/>
                </Form.Item>
                
                <Form.Item
                        name="utuBillingStartDate"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuBillingStartDate.label" defaultMessage="计费开始时间"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuBillingStartDate.tip" defaultMessage="计费开始时间为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuBillingStartDate.placeholder",defaultMessage:"计费开始时间"})}/>
                </Form.Item>
                
                <Form.Item
                        name="utuBillingEndDate"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuBillingEndDate.label" defaultMessage="计费结束时间"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuBillingEndDate.tip" defaultMessage="计费结束时间为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuBillingEndDate.placeholder",defaultMessage:"计费结束时间"})}/>
                </Form.Item>
                
                <Form.Item
                        name="utuState"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuState.label" defaultMessage="状态"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuState.tip" defaultMessage="状态为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuState.placeholder",defaultMessage:"状态"})}/>
                </Form.Item>
                
                <Form.Item
                        name="utuCreateDate"
                        label={<FormattedMessage id="pages.create.sys.user.top.up.utuCreateDate.label" defaultMessage="创建日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.top.up.utuCreateDate.tip" defaultMessage="创建日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.top.up.utuCreateDate.placeholder",defaultMessage:"创建日期"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysUserTopUpCreateForm;
