/**
 * SYS_USER_TOP_UP-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserTopUpItem {
    utuId: string;
    utuLoginName: string;
    utuSerialNumber: string;
    utuBillingStartDate: Date;
    utuBillingEndDate: Date;
    utuState: string;
    utuCreateDate: Date;
}
/**
 * SYS_USER_TOP_UP-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserTopUpPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_USER_TOP_UP-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserTopUpTableData {
    list: SysUserTopUpItem[];
    pagination: Partial < SysUserTopUpPagination > ;
}
/**
 * SYS_USER_TOP_UP-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserTopUpParams extends SysUserTopUpItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}