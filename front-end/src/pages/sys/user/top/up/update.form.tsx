import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysUserTopUpItem } from "@/pages/sys/user/top/up/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_USER_TOP_UP-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysUserTopUpUpdateFormProps {
    onSubmit: (fields: SysUserTopUpItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_USER_TOP_UP-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserTopUpUpdateForm: React.FC<SysUserTopUpUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        utuId: values.utuId,
        utuLoginName: values.utuLoginName,
        utuSerialNumber: values.utuSerialNumber,
        utuBillingStartDate: values.utuBillingStartDate,
        utuBillingEndDate: values.utuBillingEndDate,
        utuState: values.utuState,
        utuCreateDate: values.utuCreateDate,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>
            <Input name='utuId' value={formVals.utuId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="utuId"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuId.label' defaultMessage='id'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuId.tip' defaultMessage='请输入id！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuId.tip',
                    defaultMessage: '请输入id！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="utuLoginName"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuLoginName.label' defaultMessage='用户登录名'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuLoginName.tip' defaultMessage='请输入用户登录名！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuLoginName.tip',
                    defaultMessage: '请输入用户登录名！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="utuSerialNumber"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuSerialNumber.label' defaultMessage='序列号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuSerialNumber.tip' defaultMessage='请输入序列号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuSerialNumber.tip',
                    defaultMessage: '请输入序列号！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="utuBillingStartDate"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuBillingStartDate.label' defaultMessage='计费开始时间'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuBillingStartDate.tip' defaultMessage='请输入计费开始时间！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuBillingStartDate.tip',
                    defaultMessage: '请输入计费开始时间！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="utuBillingEndDate"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuBillingEndDate.label' defaultMessage='计费结束时间'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuBillingEndDate.tip' defaultMessage='请输入计费结束时间！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuBillingEndDate.tip',
                    defaultMessage: '请输入计费结束时间！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="utuState"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuState.label' defaultMessage='状态'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuState.tip' defaultMessage='请输入状态！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuState.tip',
                    defaultMessage: '请输入状态！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="utuCreateDate"
                label={<FormattedMessage id='pages.update.sys.user.top.up.utuCreateDate.label' defaultMessage='创建日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.top.up.utuCreateDate.tip' defaultMessage='请输入创建日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.top.up.update.utuCreateDate.tip',
                    defaultMessage: '请输入创建日期！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.user.top.up.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     utuId: formVals.utuId,
                     utuLoginName: formVals.utuLoginName,
                     utuSerialNumber: formVals.utuSerialNumber,
                     utuBillingStartDate: formVals.utuBillingStartDate,
                     utuBillingEndDate: formVals.utuBillingEndDate,
                     utuState: formVals.utuState,
                     utuCreateDate: formVals.utuCreateDate,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysUserTopUpUpdateForm;