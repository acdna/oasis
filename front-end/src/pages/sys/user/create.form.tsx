import { SysUserItem } from '@/pages/sys/user/data';
import React from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, InputNumber, Row, Select } from 'antd';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options, recordOptions } from '@/components/common/select.options';
import TextArea from 'antd/es/input/TextArea';
import LockOutlined from '@ant-design/icons';
import { SysRoleData } from '@/models/sys/sys.role.model';
import {
  SYS_ENABLE_STATE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
  SYS_USER_SEX_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';
import { SYS_USER_VALIDATOR } from '@/pages/sys/user/validator';

/**
 * SYS_USER-创建记录表单的输入参数
 * @author jiangbin
 * @date 1/1/2021, 7:17:26 PM
 **/
interface SysUserCreateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysUserItem) => void;
  dictMap: DictMapper;
  roles: SysRoleData[];
}

/**
 * SYS_USER-创建记录表单
 * @author jiangbin
 * @date 1/1/2021, 7:17:26 PM
 **/
const SysUserCreateForm: React.FC<SysUserCreateFormProps> = (props) => {
  const { visible, onCancel, onSubmit, dictMap, roles } = props;
  const intl = useIntl();
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 1/1/2021, 7:17:26 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {
      userId,
      userLoginName,
      userPassword,
      userPasswordPrivate,
      userNameAbbr,
      userName,
      userNameEn,
      userSex,
      userPhone,
      userEmail,
      userImageUrl,
      userOrder,
      userState,
      userCode,
      userCreateDate,
      userUpdateDate,
      userLastLoginTime,
      userLastLoginIp,
      userUnit,
      userJobTitle,
      userAddr,
      userPostcode,
      userSamSpecies,
      userCurrSpecies,
    } = fieldValues;
    onSubmit({
      userId,
      userLoginName,
      userPassword,
      userPasswordPrivate,
      userNameAbbr,
      userName,
      userNameEn,
      userSex,
      userPhone,
      userEmail,
      userImageUrl,
      userOrder,
      userState,
      userCode,
      userCreateDate,
      userUpdateDate,
      userLastLoginTime,
      userLastLoginIp,
      userUnit,
      userJobTitle,
      userAddr,
      userPostcode,
      userSamSpecies,
      userCurrSpecies,
    });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 1/1/2021, 7:17:26 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({ id: 'pages.create.sys.user.form.title', defaultMessage: '创建' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        closable={true}
        footer={
          <div
            style={{ textAlign: 'right' }}
          >
            <Button onClick={() => handleCancel()} style={{ marginRight: 8 }}>
              {intl.formatMessage({ id: 'pages.create.sys.user.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.create.sys.user.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userLoginName"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userLoginName.label',
                  defaultMessage: '用户帐号',
                })}
                rules={[
                  {
                    min: 6,
                    max: 24,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.user.userLoginName.length.tip',
                      defaultMessage: '用户帐号长度为6~24个字符!',
                    })),
                  },
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.user.userLoginName.tip',
                      defaultMessage: '用户帐号是必填项!',
                    })),
                  },
                  SYS_USER_VALIDATOR.USER_LOGIN_NAME(
                    intl.formatMessage({
                      id: 'pages.create.sys.user.userLoginName.valid.tip',
                      defaultMessage: '用户帐号已存在!',
                    }),
                  ),
                ]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userLoginName.placeholder',
                  defaultMessage: '用户帐号',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userPassword"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userPassword.label',
                  defaultMessage: '用户密码',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.user.userPassword.tip',
                      defaultMessage: '用户密码是必填项!',
                    })),
                  }, {
                    min: 6,
                    max: 24,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.user.userPassword.length.tip',
                      defaultMessage: '密码长度必须为6~24个字符!',
                    })),
                  },
                ]}
              >
                <Input.Password prefix={<LockOutlined />} placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userPassword.placeholder',
                  defaultMessage: '用户密码',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userPasswordPrivate"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userPasswordPrivate.label',
                  defaultMessage: 'userPasswordPrivate',
                })}
                rules={[
                  {
                    required: true,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.user.userPasswordPrivate.tip',
                      defaultMessage: 'userPasswordPrivate是必填项!',
                    })),
                  }, {
                    min: 6,
                    max: 24,
                    message: (intl.formatMessage({
                      id: 'pages.create.sys.user.userPasswordPrivate.length.tip',
                      defaultMessage: '密码长度必须为6~24个字符!',
                    })),
                  },
                ]}
              >
                <Input.Password prefix={<LockOutlined />} placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userPasswordPrivate.placeholder',
                  defaultMessage: 'userPasswordPrivate',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userNameAbbr"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userNameAbbr.label',
                  defaultMessage: '姓名缩写',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.user.userNameAbbr.tip',
                    defaultMessage: '姓名缩写是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userNameAbbr.placeholder',
                  defaultMessage: '姓名缩写',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userName"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userName.label',
                  defaultMessage: '用户名称',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.user.userName.tip',
                    defaultMessage: '用户名称是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userName.placeholder',
                  defaultMessage: '用户名称',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userNameEn"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userNameEn.label',
                  defaultMessage: '英文名',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.user.userNameEn.tip',
                    defaultMessage: '英文名是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userNameEn.placeholder',
                  defaultMessage: '英文名',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userOrder"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userOrder.label',
                  defaultMessage: '用户排序',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.user.userOrder.tip',
                    defaultMessage: '用户排序是必填项!',
                  })),
                }]}
              >
                <InputNumber placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userOrder.placeholder',
                  defaultMessage: '用户排序',
                })} style={{width:'100%'}}/>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userState"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userState.label',
                  defaultMessage: '状态',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.user.userState.tip',
                    defaultMessage: '状态是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.user.userState.placeholder',
                    defaultMessage: '状态',
                  })
                  }
                  defaultActiveFirstOption={true}
                >
                  {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="userSamSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userSamSpecies.label',
                  defaultMessage: '用户绑定种属',
                })}
              >
                <Select
                  mode={'multiple'}
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.user.userSamSpecies.placeholder',
                    defaultMessage: '用户绑定种属',
                  })
                  }
                  defaultActiveFirstOption={true}
                >
                  {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="userRoles"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userRoles.label',
                  defaultMessage: '角色',
                })}
              >
                <Select
                  mode={'multiple'}
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.user.userRoles.placeholder',
                    defaultMessage: '角色',
                  })
                  }
                >
                  {recordOptions({
                    records: roles,
                    cnLabelKey: 'roleName',
                    enLabelKey: 'roleNameEn',
                    valueKey: 'roleId',
                  })}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="userCurrSpecies"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userCurrSpecies.label',
                  defaultMessage: '当前种属',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.create.sys.user.userCurrSpecies.tip',
                    defaultMessage: '当前种属是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.user.userCurrSpecies.placeholder',
                    defaultMessage: '当前种属',
                  })
                  }
                  defaultActiveFirstOption={true}
                >
                  {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="userSex"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userSex.label',
                  defaultMessage: '性别',
                })}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.create.sys.user.userSex.placeholder',
                    defaultMessage: '性别',
                  })
                  }
                  defaultActiveFirstOption={true}
                >
                  {options(SYS_USER_SEX_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userPhone"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userPhone.label',
                  defaultMessage: '用户手机号',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userPhone.placeholder',
                  defaultMessage: '用户手机号',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userEmail"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userEmail.label',
                  defaultMessage: '用户的邮箱',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userEmail.placeholder',
                  defaultMessage: '用户的邮箱',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userImageUrl"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userImageUrl.label',
                  defaultMessage: '用户头像路径',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userImageUrl.placeholder',
                  defaultMessage: '用户头像路径',
                })} />
              </Form.Item>
            </Col>

          </Row>
          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userCode"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userCode.label',
                  defaultMessage: '用户代码',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userCode.placeholder',
                  defaultMessage: '用户代码',
                })} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="userPostcode"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userPostcode.label',
                  defaultMessage: '邮编',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userPostcode.placeholder',
                  defaultMessage: '邮编',
                })} />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="userUnit"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userUnit.label',
                  defaultMessage: '用户所属单位',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userUnit.placeholder',
                  defaultMessage: '用户所属单位',
                })} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="userJobTitle"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userJobTitle.label',
                  defaultMessage: '用户职务/职称',
                })}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userJobTitle.placeholder',
                  defaultMessage: '用户职务/职称',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={24}>
              <Form.Item
                name="userAddr"
                label={intl.formatMessage({
                  id: 'pages.create.sys.user.userAddr.label',
                  defaultMessage: '用户通讯地址',
                })}
              >
                <TextArea placeholder={intl.formatMessage({
                  id: 'pages.create.sys.user.userAddr.placeholder',
                  defaultMessage: '用户通讯地址',
                })} />
              </Form.Item>
            </Col>

          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default SysUserCreateForm;
