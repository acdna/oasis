/**
 * SYS_USER_ROLE-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserRoleItem {
  urId: string;
  urRoleId?: string;
  urUserLoginName?: string;
}

/**
 * SYS_USER_ROLE-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserRolePagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_USER_ROLE-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserRoleTableData {
  list: SysUserRoleItem[];
  pagination: Partial<SysUserRolePagination>;
}

/**
 * SYS_USER_ROLE-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUserRoleParams extends SysUserRoleItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };

  urUserLoginNameList?: string[];
}
