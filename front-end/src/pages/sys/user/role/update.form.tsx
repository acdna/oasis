import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysUserRoleItem } from "@/pages/sys/user/role/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_USER_ROLE-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysUserRoleUpdateFormProps {
    onSubmit: (fields: SysUserRoleItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_USER_ROLE-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserRoleUpdateForm: React.FC<SysUserRoleUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        urId: values.urId,
        urRoleId: values.urRoleId,
        urUserLoginName: values.urUserLoginName,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>
            <Input name='urId' value={formVals.urId} hidden={true} readOnly={true} disabled={true}/>

            <Form.Item
                name="urId"
                label={<FormattedMessage id='pages.update.sys.user.role.urId.label' defaultMessage='用户角色ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.role.urId.tip' defaultMessage='请输入用户角色ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.role.update.urId.tip',
                    defaultMessage: '请输入用户角色ID！'
                })}/>
            </Form.Item>

            <Form.Item
                name="urRoleId"
                label={<FormattedMessage id='pages.update.sys.user.role.urRoleId.label' defaultMessage='角色ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.role.urRoleId.tip' defaultMessage='请输入角色ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.role.update.urRoleId.tip',
                    defaultMessage: '请输入角色ID！'
                })}/>
            </Form.Item>

            <Form.Item
                name="urUserLoginName"
                label={<FormattedMessage id='pages.update.sys.user.role.urUserLoginName.label' defaultMessage='用户帐号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.user.role.urUserLoginName.tip' defaultMessage='请输入用户帐号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.user.role.update.urUserLoginName.tip',
                    defaultMessage: '请输入用户帐号！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.user.role.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     urId: formVals.urId,
                     urRoleId: formVals.urRoleId,
                     urUserLoginName: formVals.urUserLoginName,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysUserRoleUpdateForm;
