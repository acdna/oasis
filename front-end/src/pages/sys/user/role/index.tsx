import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysUserRoleService } from '@/services/sys/sys.user.role.service';
import { SysUserRoleItem } from '@/pages/sys/user/role/data';
import { SysUserRoleHandles } from '@/pages/sys/user/role/handles';
import SysUserRoleCreateForm from '@/pages/sys/user/role/create.form';
import SysUserRoleUpdateForm from '@/pages/sys/user/role/update.form';


/**
 * SYS_USER_ROLE-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserRoleTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysUserRoleItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysUserRoleItem[]>([]);
    const handles = new SysUserRoleHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysUserRoleItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.user.role.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'urId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.user.role.urId.label" defaultMessage="用户角色ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.role.urId.tip',
                defaultMessage: '用户角色ID查询',
            })),
            dataIndex: 'urId',
            valueType:'text',
             sorter: (source, target) => (source.urId||'').localeCompare(target.urId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.role.urId.tip" defaultMessage="用户角色ID" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.role.urRoleId.label" defaultMessage="角色ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.role.urRoleId.tip',
                defaultMessage: '角色ID查询',
            })),
            dataIndex: 'urRoleId',
            valueType:'text',
             sorter: (source, target) => (source.urRoleId||'').localeCompare(target.urRoleId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.role.urRoleId.tip" defaultMessage="角色ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.role.urUserLoginName.label" defaultMessage="用户帐号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.user.role.urUserLoginName.tip',
                defaultMessage: '用户帐号查询',
            })),
            dataIndex: 'urUserLoginName',
            valueType:'text',
             sorter: (source, target) => (source.urUserLoginName||'').localeCompare(target.urUserLoginName||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.user.role.urUserLoginName.tip" defaultMessage="用户帐号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.user.role.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.user.role.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.user.role.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.user.role.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysUserRoleItem>
                headerTitle={<FormattedMessage id='pages.list.sys.user.role.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="urId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.user.role.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysUserRoleService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.user.role.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.user.role.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.user.role.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysUserRoleCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysUserRoleCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysUserRoleUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.urId && (
                    <ProDescriptions<SysUserRoleItem>
                        column={2}
                        title={row?.urId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            urId: row?.urId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysUserRoleTableList;
