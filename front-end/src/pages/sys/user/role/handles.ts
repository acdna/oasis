import { SysUserRoleItem } from '@/pages/sys/user/role/data';
import { message } from 'antd';
import { SysUserRoleService } from '@/services/sys/sys.user.role.service';
import { useIntl } from 'umi';
/**
 * SYS_USER_ROLE-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
export class SysUserRoleHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysUserRoleItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.user.role.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysUserRoleService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.user.role.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.user.role.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysUserRoleItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.user.role.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysUserRoleService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.user.role.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.user.role.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysUserRoleItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.user.role.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysUserRoleService.deleteByIds(selectedRows.map(row => row.urId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.user.role.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.user.role.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysUserRoleItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.user.role.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysUserRoleService.deleteById(record.urId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.user.role.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.user.role.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};