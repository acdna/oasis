import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysUserRoleItem } from "@/pages/sys/user/role/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_USER_ROLE-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysUserRoleCreateFormProps {
    onSubmit: (fields: SysUserRoleItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_USER_ROLE-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUserRoleCreateForm: React.FC<SysUserRoleCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { urId,urRoleId,urUserLoginName }=fieldValues;
        onSubmit({ urId,urRoleId,urUserLoginName });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.user.role.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="urId"
                        label={<FormattedMessage id="pages.create.sys.user.role.urId.label" defaultMessage="用户角色ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.role.urId.tip" defaultMessage="用户角色ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.role.urId.placeholder",defaultMessage:"用户角色ID"})}/>
                </Form.Item>

                <Form.Item
                        name="urRoleId"
                        label={<FormattedMessage id="pages.create.sys.user.role.urRoleId.label" defaultMessage="角色ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.role.urRoleId.tip" defaultMessage="角色ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.role.urRoleId.placeholder",defaultMessage:"角色ID"})}/>
                </Form.Item>

                <Form.Item
                        name="urUserLoginName"
                        label={<FormattedMessage id="pages.create.sys.user.role.urUserLoginName.label" defaultMessage="用户帐号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.user.role.urUserLoginName.tip" defaultMessage="用户帐号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.user.role.urUserLoginName.placeholder",defaultMessage:"用户帐号"})}/>
                </Form.Item>

            </ProForm>
        </Modal>
    );
};

export default SysUserRoleCreateForm;
