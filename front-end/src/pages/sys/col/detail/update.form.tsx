import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysColDetailItem } from "@/pages/sys/col/detail/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_COL_DETAIL-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysColDetailUpdateFormProps {
    onSubmit: (fields: SysColDetailItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_COL_DETAIL-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysColDetailUpdateForm: React.FC<SysColDetailUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        cdId: values.cdId,
        cdTableName: values.cdTableName,
        cdColName: values.cdColName,
        cdDescName: values.cdDescName,
        cdDictName: values.cdDictName,
        cdMode: values.cdMode,
        cdCreateDate: values.cdCreateDate,
        cdUpdateDate: values.cdUpdateDate,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const renderContent = () => (
        <>

            <Form.Item
                name="cdTableName"
                label={<FormattedMessage id='pages.update.sys.col.detail.cdTableName.label' defaultMessage='表名'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.col.detail.cdTableName.tip' defaultMessage='请输入表名！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.col.detail.update.cdTableName.tip',
                    defaultMessage: '请输入表名！'
                })}/>
            </Form.Item>

            <Form.Item
                name="cdColName"
                label={<FormattedMessage id='pages.update.sys.col.detail.cdColName.label' defaultMessage='列名'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.col.detail.cdColName.tip' defaultMessage='请输入列名！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.col.detail.update.cdColName.tip',
                    defaultMessage: '请输入列名！'
                })}/>
            </Form.Item>

            <Form.Item
                name="cdDescName"
                label={<FormattedMessage id='pages.update.sys.col.detail.cdDescName.label' defaultMessage='列中文名'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.col.detail.cdDescName.tip' defaultMessage='请输入列中文名！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.col.detail.update.cdDescName.tip',
                    defaultMessage: '请输入列中文名！'
                })}/>
            </Form.Item>

            <Form.Item
                name="cdDictName"
                label={<FormattedMessage id='pages.update.sys.col.detail.cdDictName.label' defaultMessage='字典名'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.col.detail.cdDictName.tip' defaultMessage='请输入字典名！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.col.detail.update.cdDictName.tip',
                    defaultMessage: '请输入字典名！'
                })}/>
            </Form.Item>

            <Form.Item
                name="cdMode"
                label={<FormattedMessage id='pages.update.sys.col.detail.cdMode.label' defaultMessage='模式'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.col.detail.cdMode.tip' defaultMessage='请输入模式！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.col.detail.update.cdMode.tip',
                    defaultMessage: '请输入模式！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.col.detail.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     cdId: formVals.cdId,
                     cdTableName: formVals.cdTableName,
                     cdColName: formVals.cdColName,
                     cdDescName: formVals.cdDescName,
                     cdDictName: formVals.cdDictName,
                     cdMode: formVals.cdMode,
                     cdCreateDate: formVals.cdCreateDate,
                     cdUpdateDate: formVals.cdUpdateDate,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysColDetailUpdateForm;
