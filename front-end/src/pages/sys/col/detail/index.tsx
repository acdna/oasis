import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysColDetailService } from '@/services/sys/sys.col.detail.service';
import { SysColDetailItem } from '@/pages/sys/col/detail/data';
import { SysColDetailHandles } from '@/pages/sys/col/detail/handles';
import SysColDetailCreateForm from '@/pages/sys/col/detail/create.form';
import SysColDetailUpdateForm from '@/pages/sys/col/detail/update.form';
import * as moment from 'moment';
import { MLOCALES } from '@/common/locales/locale';

/**
 * SYS_COL_DETAIL-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysColDetailTableList: React.FC<{}> = () => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysColDetailItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysColDetailItem[]>([]);
  const handles = new SysColDetailHandles();
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<SysColDetailItem>[] = [

    {
      dataIndex: 'cdId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.cdTableName.label" defaultMessage="表名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.col.detail.cdTableName.tip',
        defaultMessage: '表名查询',
      })),
      dataIndex: 'cdTableName',
      valueType: 'text',
      sorter: (source, target) => (source.cdTableName || '').localeCompare(target.cdTableName || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.col.detail.cdTableName.tip" defaultMessage="表名" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.cdColName.label" defaultMessage="列名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.col.detail.cdColName.tip',
        defaultMessage: '列名查询',
      })),
      dataIndex: 'cdColName',
      valueType: 'text',
      sorter: (source, target) => (source.cdColName || '').localeCompare(target.cdColName || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.col.detail.cdColName.tip" defaultMessage="列名" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.cdDescName.label" defaultMessage="列中文名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.col.detail.cdDescName.tip',
        defaultMessage: '列中文名查询',
      })),
      dataIndex: 'cdDescName',
      valueType: 'text',
      sorter: (source, target) => (source.cdDescName || '').localeCompare(target.cdDescName || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.col.detail.cdDescName.tip" defaultMessage="列中文名" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.cdDictName.label" defaultMessage="字典名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.col.detail.cdDictName.tip',
        defaultMessage: '字典名查询',
      })),
      dataIndex: 'cdDictName',
      valueType: 'text',
      sorter: (source, target) => (source.cdDictName || '').localeCompare(target.cdDictName || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.col.detail.cdDictName.tip" defaultMessage="字典名" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.cdMode.label" defaultMessage="模式" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.col.detail.cdMode.tip',
        defaultMessage: '模式查询',
      })),
      dataIndex: 'cdMode',
      valueType: 'text',
      sorter: (source, target) => (source.cdMode || '').localeCompare(target.cdMode || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.col.detail.cdMode.tip" defaultMessage="模式" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.cdUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.col.detail.cdUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'cdUpdateDate',
      valueType: 'date', search: false, hideInForm: true,


      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.col.detail.cdUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.col.detail.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.col.detail.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.col.detail.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.col.detail.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysColDetailItem>
        headerTitle={<FormattedMessage id='pages.list.sys.col.detail.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="cdId"
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.col.detail.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysColDetailService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.col.detail.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.col.detail.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.col.detail.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysColDetailCreateForm onCancel={() => handleCreateModalVisible(false)}
                              onSubmit={async (value) => {
                                const success = await handles.add(value);
                                if (success) {
                                  handleCreateModalVisible(false);
                                  if (actionRef.current) {
                                    actionRef.current.reload();
                                  }
                                }
                              }
                              }
                              modalVisible={createModalVisible}>
      </SysColDetailCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysColDetailUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.cdId && (
          <ProDescriptions<SysColDetailItem>
            column={2}
            title={row?.cdId}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                cdId: row?.cdId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default SysColDetailTableList;
