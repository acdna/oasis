/**
 * SYS_COL_DETAIL-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysColDetailItem {
    cdId: string;
    cdTableName: string;
    cdColName: string;
    cdDescName: string;
    cdDictName: string;
    cdMode: string;
    cdCreateDate: Date;
    cdUpdateDate: Date;
}
/**
 * SYS_COL_DETAIL-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysColDetailPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_COL_DETAIL-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysColDetailTableData {
    list: SysColDetailItem[];
    pagination: Partial < SysColDetailPagination > ;
}
/**
 * SYS_COL_DETAIL-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysColDetailParams extends SysColDetailItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}