import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysColDetailItem } from "@/pages/sys/col/detail/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_COL_DETAIL-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysColDetailCreateFormProps {
    onSubmit: (fields: SysColDetailItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_COL_DETAIL-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysColDetailCreateForm: React.FC<SysColDetailCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:27 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { cdId,cdTableName,cdColName,cdDescName,cdDictName,cdMode,cdCreateDate,cdUpdateDate }=fieldValues;
        onSubmit({ cdId,cdTableName,cdColName,cdDescName,cdDictName,cdMode,cdCreateDate,cdUpdateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.col.detail.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="cdTableName"
                        label={<FormattedMessage id="pages.create.sys.col.detail.cdTableName.label" defaultMessage="表名"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.col.detail.cdTableName.tip" defaultMessage="表名为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.col.detail.cdTableName.placeholder",defaultMessage:"表名"})}/>
                </Form.Item>

                <Form.Item
                        name="cdColName"
                        label={<FormattedMessage id="pages.create.sys.col.detail.cdColName.label" defaultMessage="列名"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.col.detail.cdColName.tip" defaultMessage="列名为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.col.detail.cdColName.placeholder",defaultMessage:"列名"})}/>
                </Form.Item>

                <Form.Item
                        name="cdDescName"
                        label={<FormattedMessage id="pages.create.sys.col.detail.cdDescName.label" defaultMessage="列中文名"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.col.detail.cdDescName.tip" defaultMessage="列中文名为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.col.detail.cdDescName.placeholder",defaultMessage:"列中文名"})}/>
                </Form.Item>

                <Form.Item
                        name="cdDictName"
                        label={<FormattedMessage id="pages.create.sys.col.detail.cdDictName.label" defaultMessage="字典名"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.col.detail.cdDictName.tip" defaultMessage="字典名为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.col.detail.cdDictName.placeholder",defaultMessage:"字典名"})}/>
                </Form.Item>

                <Form.Item
                        name="cdMode"
                        label={<FormattedMessage id="pages.create.sys.col.detail.cdMode.label" defaultMessage="模式"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.col.detail.cdMode.tip" defaultMessage="模式为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.col.detail.cdMode.placeholder",defaultMessage:"模式"})}/>
                </Form.Item>

            </ProForm>
        </Modal>
    );
};

export default SysColDetailCreateForm;
