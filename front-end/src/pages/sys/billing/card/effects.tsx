import { useEffect } from 'react';
import { Dispatch } from 'umi';
/**
 * 剥离出SYS_BILLING_CARD表相关的数据加载等逻辑，包装useEffect为方法，主要是为了提高复用性
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export const SYS_BILLING_CARD_EFFECTS = {
    /**
     * 加载SYS_BILLING_CARD-计费卡信息表表的所有数据信息
     * @author jiangbin
     * @date 1/11/2021, 1:33:32 PM
     **/
    LOAD_ALL: (dispatch: Dispatch, deps ? : any[]) => {
        useEffect(() => {
            dispatch({
                type: 'sys_billing_card/fetchAll',
            });
        }, deps || []);
    },
};