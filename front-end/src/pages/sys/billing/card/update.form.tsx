import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysBillingCardItem } from "@/pages/sys/billing/card/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_BILLING_CARD-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysBillingCardUpdateFormProps {
    onSubmit: (fields: SysBillingCardItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_BILLING_CARD-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysBillingCardUpdateForm: React.FC<SysBillingCardUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        bcId: values.bcId,
        bcSerialNumber: values.bcSerialNumber,
        bcAmount: values.bcAmount,
        bcTotalTime: values.bcTotalTime,
        bcType: values.bcType,
        bcAccount: values.bcAccount,
        bcPassword: values.bcPassword,
        bcState: values.bcState,
        bcBiggestGeneCount: values.bcBiggestGeneCount,
        bcCreateDate: values.bcCreateDate,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const renderContent = () => (
        <>
            <Input name='bcId' value={formVals.bcId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="bcId"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcId.label' defaultMessage='id'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcId.tip' defaultMessage='请输入id！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcId.tip',
                    defaultMessage: '请输入id！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcSerialNumber"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcSerialNumber.label' defaultMessage='序列号信息'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcSerialNumber.tip' defaultMessage='请输入序列号信息！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcSerialNumber.tip',
                    defaultMessage: '请输入序列号信息！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcAmount"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcAmount.label' defaultMessage='总额'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcAmount.tip' defaultMessage='请输入总额！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcAmount.tip',
                    defaultMessage: '请输入总额！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcTotalTime"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcTotalTime.label' defaultMessage='总时间'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcTotalTime.tip' defaultMessage='请输入总时间！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcTotalTime.tip',
                    defaultMessage: '请输入总时间！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcType"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcType.label' defaultMessage='类型'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcType.tip' defaultMessage='请输入类型！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcType.tip',
                    defaultMessage: '请输入类型！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcAccount"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcAccount.label' defaultMessage='绑定帐号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcAccount.tip' defaultMessage='请输入绑定帐号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcAccount.tip',
                    defaultMessage: '请输入绑定帐号！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcPassword"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcPassword.label' defaultMessage='绑定密码'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcPassword.tip' defaultMessage='请输入绑定密码！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcPassword.tip',
                    defaultMessage: '请输入绑定密码！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcState"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcState.label' defaultMessage='状态'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcState.tip' defaultMessage='请输入状态！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcState.tip',
                    defaultMessage: '请输入状态！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcBiggestGeneCount"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcBiggestGeneCount.label' defaultMessage='最大指纹数'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcBiggestGeneCount.tip' defaultMessage='请输入最大指纹数！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcBiggestGeneCount.tip',
                    defaultMessage: '请输入最大指纹数！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="bcCreateDate"
                label={<FormattedMessage id='pages.update.sys.billing.card.bcCreateDate.label' defaultMessage='创建日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.billing.card.bcCreateDate.tip' defaultMessage='请输入创建日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.billing.card.update.bcCreateDate.tip',
                    defaultMessage: '请输入创建日期！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.billing.card.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     bcId: formVals.bcId,
                     bcSerialNumber: formVals.bcSerialNumber,
                     bcAmount: formVals.bcAmount,
                     bcTotalTime: formVals.bcTotalTime,
                     bcType: formVals.bcType,
                     bcAccount: formVals.bcAccount,
                     bcPassword: formVals.bcPassword,
                     bcState: formVals.bcState,
                     bcBiggestGeneCount: formVals.bcBiggestGeneCount,
                     bcCreateDate: formVals.bcCreateDate,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysBillingCardUpdateForm;