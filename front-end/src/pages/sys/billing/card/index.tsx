import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysBillingCardService } from '@/services/sys/sys.billing.card.service';
import { SysBillingCardItem } from '@/pages/sys/billing/card/data';
import { SysBillingCardHandles } from '@/pages/sys/billing/card/handles';
import SysBillingCardCreateForm from '@/pages/sys/billing/card/create.form';
import SysBillingCardUpdateForm from '@/pages/sys/billing/card/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_BILLING_CARD-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysBillingCardTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysBillingCardItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysBillingCardItem[]>([]);
    const handles = new SysBillingCardHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysBillingCardItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.billing.card.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'bcId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcId.label" defaultMessage="id" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcId.tip',
                defaultMessage: 'id查询',
            })),
            dataIndex: 'bcId',
            valueType:'text',
             sorter: (source, target) => (source.bcId||'').localeCompare(target.bcId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcId.tip" defaultMessage="id" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcSerialNumber.label" defaultMessage="序列号信息" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcSerialNumber.tip',
                defaultMessage: '序列号信息查询',
            })),
            dataIndex: 'bcSerialNumber',
            valueType:'text',
             sorter: (source, target) => (source.bcSerialNumber||'').localeCompare(target.bcSerialNumber||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcSerialNumber.tip" defaultMessage="序列号信息" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcAmount.label" defaultMessage="总额" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcAmount.tip',
                defaultMessage: '总额查询',
            })),
            dataIndex: 'bcAmount',
            valueType:'text',
             sorter: (source, target) => (source.bcAmount||'').localeCompare(target.bcAmount||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcAmount.tip" defaultMessage="总额" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcTotalTime.label" defaultMessage="总时间" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcTotalTime.tip',
                defaultMessage: '总时间查询',
            })),
            dataIndex: 'bcTotalTime',
            valueType:'digit',hideInForm: true,
             sorter: (source, target) => ((source.bcTotalTime)-(target.bcTotalTime)),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcTotalTime.tip" defaultMessage="总时间" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcType.label" defaultMessage="类型" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcType.tip',
                defaultMessage: '类型查询',
            })),
            dataIndex: 'bcType',
            valueType:'text',
             sorter: (source, target) => (source.bcType||'').localeCompare(target.bcType||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcType.tip" defaultMessage="类型" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcAccount.label" defaultMessage="绑定帐号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcAccount.tip',
                defaultMessage: '绑定帐号查询',
            })),
            dataIndex: 'bcAccount',
            valueType:'text',
             sorter: (source, target) => (source.bcAccount||'').localeCompare(target.bcAccount||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcAccount.tip" defaultMessage="绑定帐号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcPassword.label" defaultMessage="绑定密码" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcPassword.tip',
                defaultMessage: '绑定密码查询',
            })),
            dataIndex: 'bcPassword',
            valueType:'text',
             sorter: (source, target) => (source.bcPassword||'').localeCompare(target.bcPassword||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcPassword.tip" defaultMessage="绑定密码" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcState.label" defaultMessage="状态" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcState.tip',
                defaultMessage: '状态查询',
            })),
            dataIndex: 'bcState',
            valueType:'text',
             sorter: (source, target) => (source.bcState||'').localeCompare(target.bcState||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcState.tip" defaultMessage="状态" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcBiggestGeneCount.label" defaultMessage="最大指纹数" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcBiggestGeneCount.tip',
                defaultMessage: '最大指纹数查询',
            })),
            dataIndex: 'bcBiggestGeneCount',
            valueType:'text',
             sorter: (source, target) => (source.bcBiggestGeneCount||'').localeCompare(target.bcBiggestGeneCount||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcBiggestGeneCount.tip" defaultMessage="最大指纹数" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.bcCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.billing.card.bcCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'bcCreateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.billing.card.bcCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.billing.card.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.billing.card.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.billing.card.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.billing.card.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysBillingCardItem>
                headerTitle={<FormattedMessage id='pages.list.sys.billing.card.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="bcId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.billing.card.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysBillingCardService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.billing.card.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.billing.card.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.billing.card.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysBillingCardCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysBillingCardCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysBillingCardUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.bcId && (
                    <ProDescriptions<SysBillingCardItem>
                        column={2}
                        title={row?.bcId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            bcId: row?.bcId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysBillingCardTableList;
