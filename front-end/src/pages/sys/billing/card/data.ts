/**
 * SYS_BILLING_CARD-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysBillingCardItem {
    bcId: string;
    bcSerialNumber: string;
    bcAmount: string;
    bcTotalTime: number;
    bcType: string;
    bcAccount: string;
    bcPassword: string;
    bcState: string;
    bcBiggestGeneCount: string;
    bcCreateDate: Date;
}
/**
 * SYS_BILLING_CARD-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysBillingCardPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_BILLING_CARD-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysBillingCardTableData {
    list: SysBillingCardItem[];
    pagination: Partial < SysBillingCardPagination > ;
}
/**
 * SYS_BILLING_CARD-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysBillingCardParams extends SysBillingCardItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}