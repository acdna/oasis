import { SysBillingCardItem } from '@/pages/sys/billing/card/data';
import { message } from 'antd';
import { SysBillingCardService } from '@/services/sys/sys.billing.card.service';
import { useIntl } from 'umi';
/**
 * SYS_BILLING_CARD-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export class SysBillingCardHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysBillingCardItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.billing.card.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysBillingCardService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.billing.card.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.billing.card.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysBillingCardItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.billing.card.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysBillingCardService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.billing.card.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.billing.card.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysBillingCardItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.billing.card.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysBillingCardService.deleteByIds(selectedRows.map(row => row.bcId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.billing.card.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.billing.card.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysBillingCardItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.billing.card.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysBillingCardService.deleteById(record.bcId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.billing.card.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.billing.card.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};