import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysBillingCardItem } from "@/pages/sys/billing/card/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_BILLING_CARD-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysBillingCardCreateFormProps {
    onSubmit: (fields: SysBillingCardItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_BILLING_CARD-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysBillingCardCreateForm: React.FC<SysBillingCardCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:27 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { bcId,bcSerialNumber,bcAmount,bcTotalTime,bcType,bcAccount,bcPassword,bcState,bcBiggestGeneCount,bcCreateDate }=fieldValues;
        onSubmit({ bcId,bcSerialNumber,bcAmount,bcTotalTime,bcType,bcAccount,bcPassword,bcState,bcBiggestGeneCount,bcCreateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.billing.card.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="bcId"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcId.label" defaultMessage="id"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcId.tip" defaultMessage="id为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcId.placeholder",defaultMessage:"id"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcSerialNumber"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcSerialNumber.label" defaultMessage="序列号信息"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcSerialNumber.tip" defaultMessage="序列号信息为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcSerialNumber.placeholder",defaultMessage:"序列号信息"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcAmount"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcAmount.label" defaultMessage="总额"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcAmount.tip" defaultMessage="总额为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcAmount.placeholder",defaultMessage:"总额"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcTotalTime"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcTotalTime.label" defaultMessage="总时间"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcTotalTime.tip" defaultMessage="总时间为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcTotalTime.placeholder",defaultMessage:"总时间"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcType"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcType.label" defaultMessage="类型"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcType.tip" defaultMessage="类型为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcType.placeholder",defaultMessage:"类型"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcAccount"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcAccount.label" defaultMessage="绑定帐号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcAccount.tip" defaultMessage="绑定帐号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcAccount.placeholder",defaultMessage:"绑定帐号"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcPassword"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcPassword.label" defaultMessage="绑定密码"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcPassword.tip" defaultMessage="绑定密码为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcPassword.placeholder",defaultMessage:"绑定密码"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcState"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcState.label" defaultMessage="状态"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcState.tip" defaultMessage="状态为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcState.placeholder",defaultMessage:"状态"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcBiggestGeneCount"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcBiggestGeneCount.label" defaultMessage="最大指纹数"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcBiggestGeneCount.tip" defaultMessage="最大指纹数为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcBiggestGeneCount.placeholder",defaultMessage:"最大指纹数"})}/>
                </Form.Item>
                
                <Form.Item
                        name="bcCreateDate"
                        label={<FormattedMessage id="pages.create.sys.billing.card.bcCreateDate.label" defaultMessage="创建日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.billing.card.bcCreateDate.tip" defaultMessage="创建日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.billing.card.bcCreateDate.placeholder",defaultMessage:"创建日期"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysBillingCardCreateForm;
