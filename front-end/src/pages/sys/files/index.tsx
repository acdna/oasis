import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysFilesService } from '@/services/sys/sys.files.service';
import { SysFilesItem } from '@/pages/sys/files/data';
import { SysFilesHandles } from '@/pages/sys/files/handles';
import SysFilesCreateForm from '@/pages/sys/files/create.form';
import SysFilesUpdateForm from '@/pages/sys/files/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_FILES-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysFilesTableList: React.FC<{}> = () => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysFilesItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysFilesItem[]>([]);
  const handles = new SysFilesHandles();
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<SysFilesItem>[] = [
    {
      dataIndex: 'fileId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileBarcode.label" defaultMessage="条码号" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileBarcode.tip',
        defaultMessage: '文件条码号查询',
      })),
      dataIndex: 'fileBarcode',
      valueType: 'text',
      width: 220,
      sorter: (source, target) => (source.fileBarcode || '').localeCompare(target.fileBarcode || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileBarcode.tip" defaultMessage="条码号" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileRelateId.label" defaultMessage="关联的其它记录信息ID号" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileRelateId.tip',
        defaultMessage: '关联的其它记录信息ID号查询',
      })),
      dataIndex: 'fileRelateId',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileName.label" defaultMessage="名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileName.tip',
        defaultMessage: '文件名称查询',
      })),
      dataIndex: 'fileName',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileName || '').localeCompare(target.fileName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileName.tip" defaultMessage="名称" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileNameEn.label" defaultMessage="文件英文名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileNameEn.tip',
        defaultMessage: '文件名查询',
      })),
      dataIndex: 'fileNameEn',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.files.filePath.label" defaultMessage="存储路径" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.filePath.tip',
        defaultMessage: '文件存储路径查询',
      })),
      dataIndex: 'filePath',
      valueType: 'text',
      width: 270,
      sorter: (source, target) => (source.filePath || '').localeCompare(target.filePath || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.filePath.tip" defaultMessage="存储路径" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileCreateDate.label" defaultMessage="创建日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'fileCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'fileUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      width: 220,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileIsUsed.label" defaultMessage="启用?" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileIsUsed.tip',
        defaultMessage: '是否被启用查询',
      })),
      dataIndex: 'fileIsUsed',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileIsUsed || '').localeCompare(target.fileIsUsed || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileIsUsed.tip" defaultMessage="启用?" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileOwner.label" defaultMessage="用户" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileOwner.tip',
        defaultMessage: '所属用户查询',
      })),
      dataIndex: 'fileOwner',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileOwner || '').localeCompare(target.fileOwner || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileOwner.tip" defaultMessage="用户" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileSize.label" defaultMessage="大小" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileSize.tip',
        defaultMessage: '文件大小查询',
      })),
      dataIndex: 'fileSize',
      valueType: 'digit',
      hideInForm: true,
      width: 120,
      sorter: (source, target) => ((source.fileSize) - (target.fileSize)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileSize.tip" defaultMessage="大小" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileGrants.label" defaultMessage="用户权限表" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileGrants.tip',
        defaultMessage: '用户权限表查询',
      })),
      dataIndex: 'fileGrants',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileClasses.label" defaultMessage="分类" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileClasses.tip',
        defaultMessage: '文件分类查询',
      })),
      dataIndex: 'fileClasses',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileClasses || '').localeCompare(target.fileClasses || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileClasses.tip" defaultMessage="文件分类" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileState.label" defaultMessage="状态" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileState.tip',
        defaultMessage: '文件状态信息查询',
      })),
      dataIndex: 'fileState',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileState || '').localeCompare(target.fileState || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileState.tip" defaultMessage="文件状态信息" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileType.label" defaultMessage="类型" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileType.tip',
        defaultMessage: '文件类型查询',
      })),
      dataIndex: 'fileType',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileType || '').localeCompare(target.fileType || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileType.tip" defaultMessage="文件类型" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileComments.label" defaultMessage="备注" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileComments.tip',
        defaultMessage: '文件备注信息查询',
      })),
      dataIndex: 'fileComments',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileComments || '').localeCompare(target.fileComments || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileComments.tip" defaultMessage="文件备注信息" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.fileSpecies.label" defaultMessage="种属" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.files.fileSpecies.tip',
        defaultMessage: '种属查询',
      })),
      dataIndex: 'fileSpecies',
      valueType: 'text',
      width: 120,
      sorter: (source, target) => (source.fileSpecies || '').localeCompare(target.fileSpecies || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.files.fileSpecies.tip" defaultMessage="种属" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.files.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      width: 120,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.files.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.files.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.files.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysFilesItem>
        headerTitle={<FormattedMessage id='pages.list.sys.files.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="fileId"
        scroll={{ x: 1300 }}
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.files.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysFilesService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.files.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.files.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.files.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysFilesCreateForm onCancel={() => handleCreateVisible(false)}
                          onSubmit={async (value) => {
                            const success = await handles.add(value);
                            if (success) {
                              handleCreateVisible(false);
                              if (actionRef.current) {
                                actionRef.current.reload();
                              }
                            }
                          }
                          }
                          visible={createVisible}>
      </SysFilesCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysFilesUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          visible={updateVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.fileId && (
          <ProDescriptions<SysFilesItem>
            column={2}
            title={row?.fileName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                fileId: row?.fileId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default SysFilesTableList;
