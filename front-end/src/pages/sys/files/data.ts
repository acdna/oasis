/**
 * SYS_FILES-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysFilesItem {
    fileId: string;
    fileBarcode: string;
    fileRelateId: string;
    fileName: string;
    fileNameEn: string;
    filePath: string;
    fileCreateDate: Date;
    fileUpdateDate: Date;
    fileIsUsed: string;
    fileOwner: string;
    fileSize: number;
    fileGrants: string;
    fileClasses: string;
    fileState: string;
    fileType: string;
    fileComments: string;
    fileSpecies: string;
}
/**
 * SYS_FILES-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysFilesPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_FILES-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysFilesTableData {
    list: SysFilesItem[];
    pagination: Partial < SysFilesPagination > ;
}
/**
 * SYS_FILES-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysFilesParams extends SysFilesItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}