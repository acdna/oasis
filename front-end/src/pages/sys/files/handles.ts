import { SysFilesItem } from '@/pages/sys/files/data';
import { message } from 'antd';
import { SysFilesService } from '@/services/sys/sys.files.service';
import { useIntl } from 'umi';
/**
 * SYS_FILES-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export class SysFilesHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysFilesItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.files.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysFilesService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.files.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.files.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysFilesItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.files.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysFilesService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.files.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.files.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysFilesItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.files.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysFilesService.deleteByIds(selectedRows.map(row => row.fileId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.files.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.files.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysFilesItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.files.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysFilesService.deleteById(record.fileId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.files.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.files.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};