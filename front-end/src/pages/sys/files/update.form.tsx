import { SysFilesItem } from "@/pages/sys/files/data";
import React, { useState } from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row } from 'antd';
import ProForm from '@ant-design/pro-form';

/**
 * SYS_FILES-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface SysFilesUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysFilesItem) => void;
  values: any;
}

/**
 * SYS_FILES-更新字典项信息表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const SysFilesUpdateForm: React.FC<SysFilesUpdateFormProps> = (props) => {
  const { values, visible, onCancel, onSubmit } = props;
  const intl = useIntl();

  const [formVals] = useState({
    fileId: values.fileId,
    fileBarcode: values.fileBarcode,
    fileRelateId: values.fileRelateId,
    fileName: values.fileName,
    fileNameEn: values.fileNameEn,
    filePath: values.filePath,
    fileCreateDate: values.fileCreateDate,
    fileUpdateDate: values.fileUpdateDate,
    fileIsUsed: values.fileIsUsed,
    fileOwner: values.fileOwner,
    fileSize: values.fileSize,
    fileGrants: values.fileGrants,
    fileClasses: values.fileClasses,
    fileState: values.fileState,
    fileType: values.fileType,
    fileComments: values.fileComments,
    fileSpecies: values.fileSpecies,
    
  });
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        destroyOnClose
        title={intl.formatMessage({ id: 'pages.update.sys.files.title', defaultMessage: '修改' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={ { paddingBottom: 80 } }
        closable={true}
        footer={
          <div
            style={ { textAlign: 'right',} }
          >
            <Button onClick={() => handleCancel()} style={ { marginRight: 8 } }>
              {intl.formatMessage({ id: 'pages.update.sys.files.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.update.sys.files.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form} initialValues={formVals}>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="fileBarcode"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileBarcode.label',
                        defaultMessage: '文件条码号',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileBarcode.tip',
                          defaultMessage: '文件条码号是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileBarcode.placeholder',
                             defaultMessage: '文件条码号',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileRelateId"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileRelateId.label',
                        defaultMessage: '关联的其它记录信息ID号',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileRelateId.tip',
                          defaultMessage: '关联的其它记录信息ID号是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileRelateId.placeholder',
                             defaultMessage: '关联的其它记录信息ID号',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="fileName"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileName.label',
                        defaultMessage: '文件名称',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileName.tip',
                          defaultMessage: '文件名称是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileName.placeholder',
                             defaultMessage: '文件名称',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileNameEn"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileNameEn.label',
                        defaultMessage: '文件名',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileNameEn.tip',
                          defaultMessage: '文件名是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileNameEn.placeholder',
                             defaultMessage: '文件名',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="filePath"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.filePath.label',
                        defaultMessage: '文件存储路径',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.filePath.tip',
                          defaultMessage: '文件存储路径是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.filePath.placeholder',
                             defaultMessage: '文件存储路径',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileIsUsed"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileIsUsed.label',
                        defaultMessage: '是否被启用',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileIsUsed.tip',
                          defaultMessage: '是否被启用是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileIsUsed.placeholder',
                             defaultMessage: '是否被启用',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="fileOwner"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileOwner.label',
                        defaultMessage: '所属用户',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileOwner.tip',
                          defaultMessage: '所属用户是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileOwner.placeholder',
                             defaultMessage: '所属用户',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileSize"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileSize.label',
                        defaultMessage: '文件大小',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileSize.tip',
                          defaultMessage: '文件大小是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileSize.placeholder',
                             defaultMessage: '文件大小',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="fileGrants"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileGrants.label',
                        defaultMessage: '用户权限表',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileGrants.tip',
                          defaultMessage: '用户权限表是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileGrants.placeholder',
                             defaultMessage: '用户权限表',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileClasses"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileClasses.label',
                        defaultMessage: '文件分类',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileClasses.tip',
                          defaultMessage: '文件分类是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileClasses.placeholder',
                             defaultMessage: '文件分类',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="fileState"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileState.label',
                        defaultMessage: '文件状态信息',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileState.tip',
                          defaultMessage: '文件状态信息是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileState.placeholder',
                             defaultMessage: '文件状态信息',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileType"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileType.label',
                        defaultMessage: '文件类型',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileType.tip',
                          defaultMessage: '文件类型是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileType.placeholder',
                             defaultMessage: '文件类型',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
          <Row gutter={16}>
            
            <Col span={12}>
              <Form.Item
                      name="fileComments"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileComments.label',
                        defaultMessage: '文件备注信息',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileComments.tip',
                          defaultMessage: '文件备注信息是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileComments.placeholder',
                             defaultMessage: '文件备注信息',
                      })} />
              </Form.Item>
            </Col>
            
            <Col span={12}>
              <Form.Item
                      name="fileSpecies"
                      label={intl.formatMessage({
                        id: 'pages.create.sys.files.fileSpecies.label',
                        defaultMessage: '种属',
                      })}
                      rules={[{
                        required: true,
                        message: (intl.formatMessage({
                          id: 'pages.create.sys.files.fileSpecies.tip',
                          defaultMessage: '种属是必填项!',
                        })),
                      }]}
                      >
                      <Input placeholder={intl.formatMessage({
                             id: 'pages.create.sys.files.fileSpecies.placeholder',
                             defaultMessage: '种属',
                      })} />
              </Form.Item>
            </Col>
            
          </Row>
          
        </ProForm>
      </Drawer>
    </>
  );

};

export default SysFilesUpdateForm;
