import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysCacheTraceItem } from "@/pages/sys/cache/trace/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_CACHE_TRACE-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysCacheTraceCreateFormProps {
    onSubmit: (fields: SysCacheTraceItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_CACHE_TRACE-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCacheTraceCreateForm: React.FC<SysCacheTraceCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:27 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { ctId,ctCode,ctOperate,ctTargetId,ctCreateDate,ctComment }=fieldValues;
        onSubmit({ ctId,ctCode,ctOperate,ctTargetId,ctCreateDate,ctComment });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.cache.trace.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="ctId"
                        label={<FormattedMessage id="pages.create.sys.cache.trace.ctId.label" defaultMessage="缓存记录ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.cache.trace.ctId.tip" defaultMessage="缓存记录ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.cache.trace.ctId.placeholder",defaultMessage:"缓存记录ID"})}/>
                </Form.Item>
                
                <Form.Item
                        name="ctCode"
                        label={<FormattedMessage id="pages.create.sys.cache.trace.ctCode.label" defaultMessage="缓存类型代码"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.cache.trace.ctCode.tip" defaultMessage="缓存类型代码为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.cache.trace.ctCode.placeholder",defaultMessage:"缓存类型代码"})}/>
                </Form.Item>
                
                <Form.Item
                        name="ctOperate"
                        label={<FormattedMessage id="pages.create.sys.cache.trace.ctOperate.label" defaultMessage="缓存操作类型代码"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.cache.trace.ctOperate.tip" defaultMessage="缓存操作类型代码为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.cache.trace.ctOperate.placeholder",defaultMessage:"缓存操作类型代码"})}/>
                </Form.Item>
                
                <Form.Item
                        name="ctTargetId"
                        label={<FormattedMessage id="pages.create.sys.cache.trace.ctTargetId.label" defaultMessage="缓存需要操作的目标记录ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.cache.trace.ctTargetId.tip" defaultMessage="缓存需要操作的目标记录ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.cache.trace.ctTargetId.placeholder",defaultMessage:"缓存需要操作的目标记录ID"})}/>
                </Form.Item>
                
                <Form.Item
                        name="ctCreateDate"
                        label={<FormattedMessage id="pages.create.sys.cache.trace.ctCreateDate.label" defaultMessage="创建日期"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.cache.trace.ctCreateDate.tip" defaultMessage="创建日期为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.cache.trace.ctCreateDate.placeholder",defaultMessage:"创建日期"})}/>
                </Form.Item>
                
                <Form.Item
                        name="ctComment"
                        label={<FormattedMessage id="pages.create.sys.cache.trace.ctComment.label" defaultMessage="缓存备注信息"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.cache.trace.ctComment.tip" defaultMessage="缓存备注信息为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.cache.trace.ctComment.placeholder",defaultMessage:"缓存备注信息"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysCacheTraceCreateForm;
