import { useEffect } from 'react';
import { Dispatch } from 'umi';
/**
 * 剥离出SYS_CACHE_TRACE表相关的数据加载等逻辑，包装useEffect为方法，主要是为了提高复用性
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export const SYS_CACHE_TRACE_EFFECTS = {
    /**
     * 加载SYS_CACHE_TRACE-缓存信息表表的所有数据信息
     * @author jiangbin
     * @date 1/11/2021, 1:33:32 PM
     **/
    LOAD_ALL: (dispatch: Dispatch, deps ? : any[]) => {
        useEffect(() => {
            dispatch({
                type: 'sys_cache_trace/fetchAll',
            });
        }, deps || []);
    },
};