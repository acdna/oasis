/**
 * SYS_CACHE_TRACE-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCacheTraceItem {
    ctId: string;
    ctCode: string;
    ctOperate: string;
    ctTargetId: string;
    ctCreateDate: Date;
    ctComment: string;
}
/**
 * SYS_CACHE_TRACE-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCacheTracePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_CACHE_TRACE-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCacheTraceTableData {
    list: SysCacheTraceItem[];
    pagination: Partial < SysCacheTracePagination > ;
}
/**
 * SYS_CACHE_TRACE-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCacheTraceParams extends SysCacheTraceItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}