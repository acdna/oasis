import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysCacheTraceService } from '@/services/sys/sys.cache.trace.service';
import { SysCacheTraceItem } from '@/pages/sys/cache/trace/data';
import { SysCacheTraceHandles } from '@/pages/sys/cache/trace/handles';
import SysCacheTraceCreateForm from '@/pages/sys/cache/trace/create.form';
import SysCacheTraceUpdateForm from '@/pages/sys/cache/trace/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_CACHE_TRACE-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCacheTraceTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysCacheTraceItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysCacheTraceItem[]>([]);
    const handles = new SysCacheTraceHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysCacheTraceItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'ctId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.ctId.label" defaultMessage="缓存记录ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.cache.trace.ctId.tip',
                defaultMessage: '缓存记录ID查询',
            })),
            dataIndex: 'ctId',
            valueType:'text',
             sorter: (source, target) => (source.ctId||'').localeCompare(target.ctId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.cache.trace.ctId.tip" defaultMessage="缓存记录ID" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.ctCode.label" defaultMessage="缓存类型代码" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.cache.trace.ctCode.tip',
                defaultMessage: '缓存类型代码查询',
            })),
            dataIndex: 'ctCode',
            valueType:'text',
             sorter: (source, target) => (source.ctCode||'').localeCompare(target.ctCode||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.cache.trace.ctCode.tip" defaultMessage="缓存类型代码" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.ctOperate.label" defaultMessage="缓存操作类型代码" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.cache.trace.ctOperate.tip',
                defaultMessage: '缓存操作类型代码查询',
            })),
            dataIndex: 'ctOperate',
            valueType:'text',
             sorter: (source, target) => (source.ctOperate||'').localeCompare(target.ctOperate||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.cache.trace.ctOperate.tip" defaultMessage="缓存操作类型代码" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.ctTargetId.label" defaultMessage="缓存需要操作的目标记录ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.cache.trace.ctTargetId.tip',
                defaultMessage: '缓存需要操作的目标记录ID查询',
            })),
            dataIndex: 'ctTargetId',
            valueType:'text',
             sorter: (source, target) => (source.ctTargetId||'').localeCompare(target.ctTargetId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.cache.trace.ctTargetId.tip" defaultMessage="缓存需要操作的目标记录ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.ctCreateDate.label" defaultMessage="创建日期" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.cache.trace.ctCreateDate.tip',
                defaultMessage: '创建日期查询',
            })),
            dataIndex: 'ctCreateDate',
            valueType:'date',search : false,hideInForm: true,


            render: (text:any) => moment(text).format(MLOCALES.pattern),
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.cache.trace.ctCreateDate.tip" defaultMessage="创建日期" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.ctComment.label" defaultMessage="缓存备注信息" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.cache.trace.ctComment.tip',
                defaultMessage: '缓存备注信息查询',
            })),
            dataIndex: 'ctComment',
            valueType:'text',
             sorter: (source, target) => (source.ctComment||'').localeCompare(target.ctComment||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.cache.trace.ctComment.tip" defaultMessage="缓存备注信息" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.cache.trace.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.cache.trace.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.cache.trace.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.cache.trace.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysCacheTraceItem>
                headerTitle={<FormattedMessage id='pages.list.sys.cache.trace.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="ctId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.cache.trace.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysCacheTraceService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.cache.trace.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.cache.trace.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.cache.trace.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysCacheTraceCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysCacheTraceCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysCacheTraceUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.ctId && (
                    <ProDescriptions<SysCacheTraceItem>
                        column={2}
                        title={row?.ctId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            ctId: row?.ctId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysCacheTraceTableList;
