import { SysCacheTraceItem } from '@/pages/sys/cache/trace/data';
import { message } from 'antd';
import { SysCacheTraceService } from '@/services/sys/sys.cache.trace.service';
import { useIntl } from 'umi';
/**
 * SYS_CACHE_TRACE-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export class SysCacheTraceHandles {
    constructor(private readonly intl = useIntl()) {}
    /**
     * 添加记录
     * @param fields
     */
    add = async (fields: SysCacheTraceItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.cache.trace.add.loading',
            defaultMessage: '正在添加',
        }));
        try {
            await SysCacheTraceService.add({ ...fields });
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.cache.trace.add.success',
                defaultMessage: '添加成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.cache.trace.add.error',
                defaultMessage: '添加失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 更新记录
     * @param fields 更新信息
     */
    update = async (fields: SysCacheTraceItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.cache.trace.update.loading',
            defaultMessage: '正在更新',
        }));
        try {
            await SysCacheTraceService.update(fields);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.cache.trace.update.success',
                defaultMessage: '更新成功',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.cache.trace.update.error',
                defaultMessage: '更新失败请重试！',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param selectedRows 选中行记录列表
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteByIds = async (selectedRows: SysCacheTraceItem[]) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.cache.trace.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!selectedRows) return true;
        try {
            await SysCacheTraceService.deleteByIds(selectedRows.map(row => row.ctId));
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.cache.trace.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.cache.trace.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
    /**
     * 删除选中行数据
     * @param record 被删除的记录
     * @return
     * @author jiang
     * @date 2020-12-18 23:00:13
     **/
    deleteById = async (record: SysCacheTraceItem) => {
        const hide = message.loading(this.intl.formatMessage({
            id: 'page.sys.cache.trace.delete.loading',
            defaultMessage: '正在删除',
        }));
        if (!record) return true;
        try {
            await SysCacheTraceService.deleteById(record.ctId);
            hide();
            message.success(this.intl.formatMessage({
                id: 'page.sys.cache.trace.delete.success',
                defaultMessage: '删除成功，即将刷新!',
            }));
            return true;
        } catch (error) {
            hide();
            message.error(this.intl.formatMessage({
                id: 'page.sys.cache.trace.delete.error',
                defaultMessage: '删除失败，请重试!',
            }));
            return false;
        }
    };
};