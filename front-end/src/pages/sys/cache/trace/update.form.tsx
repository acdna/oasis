import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysCacheTraceItem } from "@/pages/sys/cache/trace/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_CACHE_TRACE-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysCacheTraceUpdateFormProps {
    onSubmit: (fields: SysCacheTraceItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_CACHE_TRACE-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCacheTraceUpdateForm: React.FC<SysCacheTraceUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        ctId: values.ctId,
        ctCode: values.ctCode,
        ctOperate: values.ctOperate,
        ctTargetId: values.ctTargetId,
        ctCreateDate: values.ctCreateDate,
        ctComment: values.ctComment,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const renderContent = () => (
        <>
            <Input name='ctId' value={formVals.ctId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="ctId"
                label={<FormattedMessage id='pages.update.sys.cache.trace.ctId.label' defaultMessage='缓存记录ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.cache.trace.ctId.tip' defaultMessage='请输入缓存记录ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.cache.trace.update.ctId.tip',
                    defaultMessage: '请输入缓存记录ID！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="ctCode"
                label={<FormattedMessage id='pages.update.sys.cache.trace.ctCode.label' defaultMessage='缓存类型代码'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.cache.trace.ctCode.tip' defaultMessage='请输入缓存类型代码！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.cache.trace.update.ctCode.tip',
                    defaultMessage: '请输入缓存类型代码！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="ctOperate"
                label={<FormattedMessage id='pages.update.sys.cache.trace.ctOperate.label' defaultMessage='缓存操作类型代码'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.cache.trace.ctOperate.tip' defaultMessage='请输入缓存操作类型代码！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.cache.trace.update.ctOperate.tip',
                    defaultMessage: '请输入缓存操作类型代码！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="ctTargetId"
                label={<FormattedMessage id='pages.update.sys.cache.trace.ctTargetId.label' defaultMessage='缓存需要操作的目标记录ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.cache.trace.ctTargetId.tip' defaultMessage='请输入缓存需要操作的目标记录ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.cache.trace.update.ctTargetId.tip',
                    defaultMessage: '请输入缓存需要操作的目标记录ID！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="ctCreateDate"
                label={<FormattedMessage id='pages.update.sys.cache.trace.ctCreateDate.label' defaultMessage='创建日期'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.cache.trace.ctCreateDate.tip' defaultMessage='请输入创建日期！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.cache.trace.update.ctCreateDate.tip',
                    defaultMessage: '请输入创建日期！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="ctComment"
                label={<FormattedMessage id='pages.update.sys.cache.trace.ctComment.label' defaultMessage='缓存备注信息'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.cache.trace.ctComment.tip' defaultMessage='请输入缓存备注信息！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.cache.trace.update.ctComment.tip',
                    defaultMessage: '请输入缓存备注信息！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.cache.trace.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     ctId: formVals.ctId,
                     ctCode: formVals.ctCode,
                     ctOperate: formVals.ctOperate,
                     ctTargetId: formVals.ctTargetId,
                     ctCreateDate: formVals.ctCreateDate,
                     ctComment: formVals.ctComment,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysCacheTraceUpdateForm;