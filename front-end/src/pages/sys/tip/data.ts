/**
 * SYS_TIP-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysTipItem {
    tipId: string;
    tipCount: number;
    tipContents: string;
    tipManager: string;
    tipAudience: string;
    tipCreateDate: Date;
    tipUpdateDate: Date;
}
/**
 * SYS_TIP-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysTipPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_TIP-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysTipTableData {
    list: SysTipItem[];
    pagination: Partial < SysTipPagination > ;
}
/**
 * SYS_TIP-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysTipParams extends SysTipItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}