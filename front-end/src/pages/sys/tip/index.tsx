import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysTipService } from '@/services/sys/sys.tip.service';
import { SysTipItem } from '@/pages/sys/tip/data';
import { SysTipHandles } from '@/pages/sys/tip/handles';
import SysTipCreateForm from '@/pages/sys/tip/create.form';
import SysTipUpdateForm from '@/pages/sys/tip/update.form';
import { MLOCALES } from '@/common/locales/locale';
import * as moment from 'moment';

/**
 * SYS_TIP-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysTipTableList: React.FC<{}> = () => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysTipItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysTipItem[]>([]);
  const handles = new SysTipHandles();
  const intl = useIntl();

  //定义列信息
  const columns: ProColumns<SysTipItem>[] = [
    {
      dataIndex: 'tipId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.tipCount.label" defaultMessage="提示次数" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.tip.tipCount.tip',
        defaultMessage: '提示次数查询',
      })),
      dataIndex: 'tipCount',
      valueType: 'digit', hideInForm: true,
      sorter: (source, target) => ((source.tipCount) - (target.tipCount)),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.tip.tipCount.tip" defaultMessage="提示次数" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.tipContents.label" defaultMessage="内容" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.tip.tipContents.tip',
        defaultMessage: '提示信息内容查询',
      })),
      dataIndex: 'tipContents',
      valueType: 'text',
      sorter: (source, target) => (source.tipContents || '').localeCompare(target.tipContents || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.tip.tipContents.tip" defaultMessage="提示信息内容" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.tipManager.label" defaultMessage="所属者" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.tip.tipManager.tip',
        defaultMessage: '所属者查询',
      })),
      dataIndex: 'tipManager',
      valueType: 'text',
      sorter: (source, target) => (source.tipManager || '').localeCompare(target.tipManager || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.tip.tipManager.tip" defaultMessage="所属者" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.tipAudience.label" defaultMessage="消息受众" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.tip.tipAudience.tip',
        defaultMessage: '消息受众查询',
      })),
      dataIndex: 'tipAudience',
      valueType: 'text',
      sorter: (source, target) => (source.tipAudience || '').localeCompare(target.tipAudience || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.tip.tipAudience.tip" defaultMessage="消息受众" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.tipCreateDate.label" defaultMessage="创建日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.tip.tipCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'tipCreateDate',
      valueType: 'date', search: false, hideInForm: true,


      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.tip.tipCreateDate.tip" defaultMessage="创建日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.tipUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.tip.tipUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'tipUpdateDate',
      valueType: 'date', search: false, hideInForm: true,


      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.tip.tipUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.tip.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.tip.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.tip.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.tip.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysTipItem>
        headerTitle={<FormattedMessage id='pages.list.sys.tip.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="tipId"
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.tip.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysTipService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.tip.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.tip.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.tip.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysTipCreateForm onCancel={() => handleCreateModalVisible(false)}
                        onSubmit={async (value) => {
                          const success = await handles.add(value);
                          if (success) {
                            handleCreateModalVisible(false);
                            if (actionRef.current) {
                              actionRef.current.reload();
                            }
                          }
                        }
                        }
                        modalVisible={createModalVisible}>
      </SysTipCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysTipUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.tipId && (
          <ProDescriptions<SysTipItem>
            column={2}
            title={row?.tipManager}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                tipId: row?.tipId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default SysTipTableList;
