import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysTipItem } from "@/pages/sys/tip/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_TIP-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysTipCreateFormProps {
    onSubmit: (fields: SysTipItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_TIP-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysTipCreateForm: React.FC<SysTipCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { tipId,tipCount,tipContents,tipManager,tipAudience,tipCreateDate,tipUpdateDate }=fieldValues;
        onSubmit({ tipId,tipCount,tipContents,tipManager,tipAudience,tipCreateDate,tipUpdateDate });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.tip.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="tipCount"
                        label={<FormattedMessage id="pages.create.sys.tip.tipCount.label" defaultMessage="提示次数"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.tip.tipCount.tip" defaultMessage="提示次数为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.tip.tipCount.placeholder",defaultMessage:"提示次数"})}/>
                </Form.Item>

                <Form.Item
                        name="tipContents"
                        label={<FormattedMessage id="pages.create.sys.tip.tipContents.label" defaultMessage="提示信息内容"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.tip.tipContents.tip" defaultMessage="提示信息内容为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.tip.tipContents.placeholder",defaultMessage:"提示信息内容"})}/>
                </Form.Item>

                <Form.Item
                        name="tipManager"
                        label={<FormattedMessage id="pages.create.sys.tip.tipManager.label" defaultMessage="所属者"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.tip.tipManager.tip" defaultMessage="所属者为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.tip.tipManager.placeholder",defaultMessage:"所属者"})}/>
                </Form.Item>

                <Form.Item
                        name="tipAudience"
                        label={<FormattedMessage id="pages.create.sys.tip.tipAudience.label" defaultMessage="消息受众"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.tip.tipAudience.tip" defaultMessage="消息受众为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.tip.tipAudience.placeholder",defaultMessage:"消息受众"})}/>
                </Form.Item>

            </ProForm>
        </Modal>
    );
};

export default SysTipCreateForm;
