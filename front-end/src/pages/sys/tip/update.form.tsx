import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysTipItem } from "@/pages/sys/tip/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_TIP-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysTipUpdateFormProps {
    onSubmit: (fields: SysTipItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_TIP-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysTipUpdateForm: React.FC<SysTipUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        tipId: values.tipId,
        tipCount: values.tipCount,
        tipContents: values.tipContents,
        tipManager: values.tipManager,
        tipAudience: values.tipAudience,
        tipCreateDate: values.tipCreateDate,
        tipUpdateDate: values.tipUpdateDate,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>

            <Form.Item
                name="tipCount"
                label={<FormattedMessage id='pages.update.sys.tip.tipCount.label' defaultMessage='提示次数'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.tip.tipCount.tip' defaultMessage='请输入提示次数！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.tip.update.tipCount.tip',
                    defaultMessage: '请输入提示次数！'
                })}/>
            </Form.Item>

            <Form.Item
                name="tipContents"
                label={<FormattedMessage id='pages.update.sys.tip.tipContents.label' defaultMessage='提示信息内容'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.tip.tipContents.tip' defaultMessage='请输入提示信息内容！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.tip.update.tipContents.tip',
                    defaultMessage: '请输入提示信息内容！'
                })}/>
            </Form.Item>

            <Form.Item
                name="tipManager"
                label={<FormattedMessage id='pages.update.sys.tip.tipManager.label' defaultMessage='所属者'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.tip.tipManager.tip' defaultMessage='请输入所属者！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.tip.update.tipManager.tip',
                    defaultMessage: '请输入所属者！'
                })}/>
            </Form.Item>

            <Form.Item
                name="tipAudience"
                label={<FormattedMessage id='pages.update.sys.tip.tipAudience.label' defaultMessage='消息受众'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.tip.tipAudience.tip' defaultMessage='请输入消息受众！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.tip.update.tipAudience.tip',
                    defaultMessage: '请输入消息受众！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.tip.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     tipId: formVals.tipId,
                     tipCount: formVals.tipCount,
                     tipContents: formVals.tipContents,
                     tipManager: formVals.tipManager,
                     tipAudience: formVals.tipAudience,
                     tipCreateDate: formVals.tipCreateDate,
                     tipUpdateDate: formVals.tipUpdateDate,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysTipUpdateForm;
