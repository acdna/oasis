import React, { useState } from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import ProForm from '@ant-design/pro-form';
import { options } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SYS_DICTIONARY_VALIDATOR } from '@/pages/sys/dictionary/validator';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_MODULES_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_DICTIONARY-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
interface SysDictionaryUpdateFormProps {
  onSubmit: (fields: SysDictionaryItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_DICTIONARY-更新字典项信息表单
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
const SysDictionaryUpdateForm: React.FC<SysDictionaryUpdateFormProps> = (props) => {
  const { onSubmit, onCancel, updateModalVisible, values, dictMap } = props;
  const { dicId } = values;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    dicId: values.dicId,
    dicName: values.dicName,
    dicNameEn: values.dicNameEn,
    dicValue: values.dicValue,
    dicGroup: values.dicGroup,
    dicModule: values.dicModule,
    dicParentId: '1',
    dicType: 'DIR',
    dicOrder: values.dicOrder,
    dicState: values.dicState,
    dicParams: values.dicParams,
    dicSpecies: values.dicSpecies,
    dicCreateDate: values.dicCreateDate,
    dicUpdateDate: values.dicUpdateDate,
  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const renderContent = () => (
    <>
      <Form.Item
        name="dicModule"
        label={<FormattedMessage id="pages.create.sys.dictionary.dicModule.label" defaultMessage="模块名" />}
        rules={[{
          required: true,
          message: (<FormattedMessage id="pages.create.sys.dictionary.dicModule.tip" defaultMessage="模块名为必填项" />),
        },
        ]}
      >
        <Select
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicModule.placeholder',
            defaultMessage: '模块名',
          })
          }
        >
          {options(SYS_MODULES_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>

      <Form.Item
        name="dicGroup"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicGroup.label' defaultMessage='分组' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.dicGroup.tip' defaultMessage='请输入分组！' />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.DIC_GROUP(value, dicId);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dicGroup.valid',
                defaultMessage: '字典分组已经存在!',
              }));
            },
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.dicGroup.tip',
          defaultMessage: '请输入分组！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicName"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicName.label' defaultMessage='字典名称' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.dicName.tip' defaultMessage='请输入字典名称！' />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.DIC_NAME(value, dicId);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dicName.valid',
                defaultMessage: '字典名已经存在!',
              }));
            },
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.dicName.tip',
          defaultMessage: '请输入字典名称！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicNameEn"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicNameEn.label' defaultMessage='字典英文名' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.dicNameEn.tip' defaultMessage='请输入字典英文名！' />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.DIC_NAME_EN(value, dicId);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dicNameEn.valid',
                defaultMessage: '字典英文名已经存在!',
              }));
            },
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.dicNameEn.tip',
          defaultMessage: '请输入字典英文名！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicOrder"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicOrder.label' defaultMessage='排序' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.dicState.tip' defaultMessage='请输入排序！' />),
          },
        ]}
      >
        <Input type={'number'} placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.dicOrder.tip',
          defaultMessage: '请输入排序！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicState"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicState.label' defaultMessage='启用?' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.dicState.tip' defaultMessage='请选择是否启用！' />),
          },
        ]}
      >
        <Select
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.update.sys.dictionary.dicState.placeholder',
            defaultMessage: '启用?',
          })}
        >
          {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>

    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{ padding: '32px 40px 48px' }}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.dictionary.dict.form.title' defaultMessage='修改字典' />}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            dicId: formVals.dicId,
            dicName: formVals.dicName,
            dicNameEn: formVals.dicNameEn,
            dicValue: formVals.dicValue,
            dicGroup: formVals.dicGroup,
            dicModule: formVals.dicModule,
            dicParentId: '1',
            dicType: 'DIR',
            dicOrder: formVals.dicOrder,
            dicState: formVals.dicState,
            dicParams: formVals.dicParams,
            dicSpecies: formVals.dicSpecies,
            dicCreateDate: formVals.dicCreateDate,
            dicUpdateDate: formVals.dicUpdateDate,

          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysDictionaryUpdateForm;
