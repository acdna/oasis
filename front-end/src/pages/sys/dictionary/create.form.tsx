import React from 'react';
import { Form, Input, InputNumber, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import ProForm from '@ant-design/pro-form';
import { options } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SYS_DICTIONARY_VALIDATOR } from '@/pages/sys/dictionary/validator';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_MODULES_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_DICTIONARY-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
interface SysDictionaryCreateFormProps {
  onSubmit: (fields: SysDictionaryItem) => void,
  onCancel: () => void;
  modalVisible: boolean;
  dictMap: DictMapper;
}

/**
 * SYS_DICTIONARY-创建记录表单
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
const SysDictionaryCreateForm: React.FC<SysDictionaryCreateFormProps> = (props) => {
  const { modalVisible, onCancel, onSubmit, dictMap } = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {
      dicId,
      dicName,
      dicNameEn,
      dicValue,
      dicGroup,
      dicOrder,
      dicState,
      dicParams,
      dicSpecies,
      dicModule,
      dicCreateDate,
      dicUpdateDate,
    } = fieldValues;
    onSubmit({
      dicId,
      dicName,
      dicNameEn,
      dicValue,
      dicGroup,
      dicModule,
      dicParentId: '1',
      dicType: 'DIR',
      dicOrder,
      dicState,
      dicParams,
      dicSpecies,
      dicCreateDate,
      dicUpdateDate,
    });
    await form.resetFields();
  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.create.sys.dictionary.dict.form.title' defaultMessage='新建字典' />
      }
      visible={modalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false} initialValues={{ dicOrder: 50, dicType: 'DIR', dicState: 'ON' }}>

        <Form.Item
          name="dicModule"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicModule.label" defaultMessage="模块名" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicModule.tip" defaultMessage="模块名为必填项" />),
          },
          ]}
        >
          <Select
            style={{ width: '100%' }}
            placeholder={intl.formatMessage({
              id: 'pages.update.sys.dictionary.dicModule.placeholder',
              defaultMessage: '模块名',
            })
            }
            defaultActiveFirstOption={true}
          >
            {options(SYS_MODULES_DICT_GROUP.group, dictMap)}
          </Select>
        </Form.Item>

        <Form.Item
          name="dicGroup"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicGroup.label" defaultMessage="分组" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicGroup.tip" defaultMessage="分组为必填项" />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.DIC_GROUP(value);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dicGroup.valid',
                defaultMessage: '字典分组已经存在!',
              }));
            },
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicGroup.placeholder',
            defaultMessage: '分组',
          })} />
        </Form.Item>

        <Form.Item
          name="dicName"
          label={<FormattedMessage id="pages.create.sys.dictionary.dict.dicName.label" defaultMessage="字典名" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dict.dicName.tip" defaultMessage="字典名为必填项" />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.DIC_NAME(value);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dict.dicName.valid',
                defaultMessage: '字典名已经存在!',
              }));
            },
          },
          ]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dict.dicName.placeholder',
            defaultMessage: '字典名',
          })} />
        </Form.Item>

        <Form.Item
          name="dicNameEn"
          label={<FormattedMessage id="pages.create.sys.dictionary.dict.dicNameEn.label" defaultMessage="字典英文名" />}
          rules={[{
            required: true,
            message: (
              <FormattedMessage id="pages.create.sys.dictionary.dict.dicNameEn.tip" defaultMessage="字典英文名为必填项" />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.DIC_NAME_EN(value);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dict.dicNameEn.valid',
                defaultMessage: '字典英文名已经存在!',
              }));
            },
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicNameEn.placeholder',
            defaultMessage: '字典项英文名',
          })} />
        </Form.Item>

        <Form.Item
          name="dicOrder"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicOrder.label" defaultMessage="排序" />}
          rules={[{
            required: false,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicOrder.tip" defaultMessage="排序为必选项" />),
          }]}
        >
          <InputNumber placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicOrder.placeholder',
            defaultMessage: '排序',
          })} style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item
          name="dicState"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicState.label" defaultMessage="启用?" />}
          rules={[{
            required: false,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicState.tip" defaultMessage="启用为必选项" />),
          }]}
        >
          <Select
            style={{ width: '100%' }}
            placeholder={intl.formatMessage({
              id: 'pages.create.sys.dictionary.dicState.placeholder',
              defaultMessage: '启用?',
            })
            }
            defaultActiveFirstOption={true}
          >
            {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
          </Select>
        </Form.Item>

      </ProForm>
    </Modal>
  );
};

export default SysDictionaryCreateForm;
