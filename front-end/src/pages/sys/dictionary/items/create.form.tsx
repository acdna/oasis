import React from 'react';
import { Form, Input, InputNumber, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import ProForm from '@ant-design/pro-form';
import { options } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SYS_DICTIONARY_VALIDATOR } from '@/pages/sys/dictionary/validator';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_SAM_SPECIES_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_DICTIONARY-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
interface SysDictionaryItemCreateFormProps {
  onSubmit: (fields: SysDictionaryItem) => void,
  onCancel: () => void;
  modalVisible: boolean;
  dictMap: DictMapper;
  dict: any;//字典节点的数据
}

/**
 * SYS_DICTIONARY-创建字典项记录表单
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
const SysDictionaryItemCreateForm: React.FC<SysDictionaryItemCreateFormProps> = (props) => {
  const { modalVisible, onCancel, onSubmit, dictMap, dict } = props;
  const { dicGroup, dicParentId, dicModule } = dict;
  const [form] = Form.useForm();
  const intl = useIntl();

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {
      dicId,
      dicName,
      dicNameEn,
      dicValue,
      dicOrder,
      dicState,
      dicModule,
      dicParams,
      dicSpecies,
      dicCreateDate,
      dicUpdateDate,
    } = fieldValues;
    onSubmit({
      dicId,
      dicName,
      dicNameEn,
      dicValue,
      dicGroup,
      dicParentId,
      dicType: 'DATA',
      dicModule,
      dicOrder,
      dicState,
      dicParams,
      dicSpecies,
      dicCreateDate,
      dicUpdateDate,
    });
    await form.resetFields();
  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.create.sys.dictionary.item.form.title' defaultMessage='新建字典项' />
      }
      visible={modalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false} initialValues={{
        dicType: 'DATA',
        dicState: 'ON',
        dicOrder: 50,
        dicModule,
        dicGroup,
        dicParentId,
      }}>

        <Form.Item
          name="dicModule"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicModule.label" defaultMessage="所属模块" />}
        >
          <Input readOnly={true} placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicModule.placeholder',
            defaultMessage: '所属模块',
          })} />
        </Form.Item>
        <Form.Item
          name="dicGroup"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicGroup.label" defaultMessage="所属模块" />}
        >
          <Input readOnly={true} placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicGroup.placeholder',
            defaultMessage: '所属组',
          })} />
        </Form.Item>

        <Form.Item
          name="dicName"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicName.label" defaultMessage="字典项名" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicName.tip" defaultMessage="字典项名为必填项" />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.ITEM_DIC_NAME(value, dicGroup);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dicName.valid',
                defaultMessage: '字典项名已经存在!',
              }));
            },
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicName.placeholder',
            defaultMessage: '字典项名',
          })} />
        </Form.Item>

        <Form.Item
          name="dicNameEn"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicNameEn.label" defaultMessage="字典项英文名" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicNameEn.tip" defaultMessage="字典项英文名为必填项" />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.ITEM_DIC_NAME_EN(value, dicGroup);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.dicNameEn.valid',
                defaultMessage: '字典项英文名已经存在!',
              }));
            },
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicNameEn.placeholder',
            defaultMessage: '字典项英文名',
          })} />
        </Form.Item>

        <Form.Item
          name="dicValue"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicValue.label" defaultMessage="字典项值" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicValue.tip" defaultMessage="字典项值为必填项" />),
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicValue.placeholder',
            defaultMessage: '字典项值',
          })} />
        </Form.Item>

        <Form.Item
          name="dicOrder"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicOrder.label" defaultMessage="排序" />}
        >
          <InputNumber placeholder={intl.formatMessage({
            id: 'pages.create.sys.dictionary.dicOrder.placeholder',
            defaultMessage: '排序',
          })} style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item
          name="dicSpecies"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicSpecies.label" defaultMessage="种属" />}
        >
          <Select
            mode="multiple"
            tokenSeparators={[',']}
            style={{ width: '100%' }}
            placeholder={intl.formatMessage({
              id: 'pages.create.sys.dictionary.dicSpecies.placeholder',
              defaultMessage: '种属',
            })}
          >
            {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
          </Select>
        </Form.Item>

        <Form.Item
          name="dicState"
          label={<FormattedMessage id="pages.create.sys.dictionary.dicState.label" defaultMessage="启用?" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.dictionary.dicState.tip" defaultMessage="启用?为必选项" />),
          }]}
        >
          <Select
            style={{ width: '100%' }}
            placeholder={intl.formatMessage({
              id: 'pages.create.sys.dictionary.dicState.placeholder',
              defaultMessage: '启用?',
            })}
          >
            {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
          </Select>
        </Form.Item>

      </ProForm>
    </Modal>
  );
};

export default SysDictionaryItemCreateForm;
