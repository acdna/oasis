import React, { useState } from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options } from '@/components/common/select.options';
import { SYS_DICTIONARY_VALIDATOR } from '@/pages/sys/dictionary/validator';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_SAM_SPECIES_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_DICTIONARY-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
interface SysDictionaryItemUpdateFormProps {
  onSubmit: (fields: SysDictionaryItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_DICTIONARY-更新字典项信息表单
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
const SysDictionaryItemUpdateForm: React.FC<SysDictionaryItemUpdateFormProps> = (props) => {
  const { onSubmit, onCancel, updateModalVisible, values, dictMap } = props;
  const { dicGroup, dicId } = values;
  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    dicId: values.dicId,
    dicName: values.dicName,
    dicNameEn: values.dicNameEn,
    dicValue: values.dicValue,
    dicGroup: values.dicValue,
    dicParentId: values.dicValue,
    dicType: 'DATA',
    dicModule: values.dicModule,
    dicOrder: values.dicOrder,
    dicState: values.dicState,
    dicParams: values.dicParams,
    dicSpecies: values.dicSpecies,
    dicCreateDate: values.dicCreateDate,
    dicUpdateDate: values.dicUpdateDate,
  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues, dicType: 'DATA' };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/22/2020, 2:34:07 PM
   **/
  const renderContent = () => (
    <>
      <Form.Item
        name="dicGroup"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicGroup.label' defaultMessage='所属组' />}
      >
        <Input readOnly={true} placeholder={intl.formatMessage({
          id: 'pages.sys.dictionary.update.dicGroup.tip',
          defaultMessage: '请输入所属组！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicName"
        label={<FormattedMessage id='pages.update.sys.dictionary.item.dicName.label' defaultMessage='字典项名' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.item.dicName.tip' defaultMessage='请输入字典项名！' />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.ITEM_DIC_NAME(value, dicGroup, dicId);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.create.sys.dictionary.item.dicName.valid',
                defaultMessage: '字典项名已经存在!',
              }));
            },
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.item.dicName.tip',
          defaultMessage: '请输入字典项名！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicNameEn"
        label={<FormattedMessage id='pages.update.sys.dictionary.item.dicNameEn.label' defaultMessage='字典项英文名' />}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage id='pages.update.sys.dictionary.item.dicNameEn.tip' defaultMessage='请输入字典项英文名！' />),
          }, {
            validator: async (rule, value, callback) => {
              let res = await SYS_DICTIONARY_VALIDATOR.ITEM_DIC_NAME_EN(value, dicGroup, dicId);
              return res ? Promise.resolve() : Promise.reject(intl.formatMessage({
                id: 'pages.update.sys.dictionary.item.dicNameEn.valid',
                defaultMessage: '字典项英文名已经存在!',
              }));
            },
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.dictionary.item.update.dicNameEn.tip',
          defaultMessage: '请输入字典项英文名！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicValue"
        label={<FormattedMessage id='pages.update.sys.dictionary.item.dicValue.label' defaultMessage='字典值' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.dictionary.item.dicValue.tip' defaultMessage='请输入字典值！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.item.dicValue.tip',
          defaultMessage: '请输入字典值！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicParentId"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicParentId.label' defaultMessage='父级ID' />}
        hidden={true}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.dictionary.dicParentId.tip',
          defaultMessage: '请输入父级ID！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicType"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicType.label' defaultMessage='字典类型' />}
        hidden={true}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.dictionary.update.dicType.tip',
          defaultMessage: '请输入字典类型！',
        })} />
      </Form.Item>

      <Form.Item
        name="dicOrder"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicOrder.label' defaultMessage='排序' />}
      >
        <Input type={'number'} placeholder={intl.formatMessage({
          id: 'pages.sys.dictionary.update.dicOrder.tip',
          defaultMessage: '请输入排序！',
        })} defaultValue={50} />
      </Form.Item>

      <Form.Item
        name="dicSpecies"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicSpecies.label' defaultMessage='种属' />}
      >
        <Select
          mode="multiple"
          tokenSeparators={[',']}
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.update.sys.dictionary.dicSpecies.placeholder',
            defaultMessage: '种属',
          })}
        >
          {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>

      <Form.Item
        name="dicState"
        label={<FormattedMessage id='pages.update.sys.dictionary.dicState.label' defaultMessage='启用?' />}
        rules={[
          {
            required: false,
            message: (<FormattedMessage id='pages.update.sys.dictionary.dicState.tip' defaultMessage='请选择是否启用！' />),
          },
        ]}
      >
        <Select
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.update.sys.dictionary.dicState.placeholder',
            defaultMessage: '启用?',
          })}
        >
          {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>
    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{ padding: '32px 40px 48px' }}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.dictionary.item.form.title' defaultMessage='修改字典项' />}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            dicId: formVals.dicId,
            dicName: formVals.dicName,
            dicNameEn: formVals.dicNameEn,
            dicValue: formVals.dicValue,
            dicGroup: formVals.dicGroup,
            dicParentId: formVals.dicParentId,
            dicType: 'DATA',
            dicModule: formVals.dicModule,
            dicOrder: formVals.dicOrder,
            dicState: formVals.dicState,
            dicParams: formVals.dicParams,
            dicSpecies: formVals.dicSpecies ? formVals.dicSpecies.split(',') : undefined,
            dicCreateDate: formVals.dicCreateDate,
            dicUpdateDate: formVals.dicUpdateDate,

          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysDictionaryItemUpdateForm;
