/**
 * SYS_DICTIONARY-字段信息
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 */
export interface SysDictionaryItem {
  dicId: string;
  dicName: string;
  dicNameEn: string;
  dicValue: string;
  dicGroup: string;
  dicParentId: string;
  dicModule: string;
  dicType: string;
  dicOrder: number;
  dicState: string;
  dicParams: string;
  dicSpecies: string;
  dicCreateDate: Date;
  dicUpdateDate: Date;
}

/**
 * SYS_DICTIONARY-分页参数
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 */
export interface SysDictionaryPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_DICTIONARY-表格数据
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 */
export interface SysDictionaryTableData {
  list: SysDictionaryItem[];
  pagination: Partial<SysDictionaryPagination>;
}

/**
 * SYS_DICTIONARY-查询参数
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 */
export interface SysDictionaryParams extends SysDictionaryItem {
  pageSize?: number;
  current?: number;
  pageNum?: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };

  ndicId?: string;
  dicParentIdList?: string[];
  distinct?: boolean;
  cols?: string[];
  dicIdList?: string[];
  order?:string;
}
