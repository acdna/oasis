import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import { message } from 'antd';
import { SysDictionaryService } from '@/services/sys/sys.dictionary.service';
import { useIntl } from 'umi';

/**
 * SYS_DICTIONARY-增、删、改表单数据操作句柄
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
export class SysDictionaryHandles {
  constructor(private readonly intl = useIntl()) {
  }

  /**
   * 添加记录
   * @param fields
   */
  add = async (fields: SysDictionaryItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.dictionary.add.loading',
      defaultMessage: '正在添加',
    }));
    try {
      await SysDictionaryService.add({ ...fields });
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.dictionary.add.success',
        defaultMessage: '添加成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.dictionary.add.error',
        defaultMessage: '添加失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 更新记录
   * @param fields 更新信息
   */
  update = async (fields: SysDictionaryItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.dictionary.update.loading',
      defaultMessage: '正在更新',
    }));
    try {
      //更新记录值
      await SysDictionaryService.update(fields);
      if (fields.dicType === 'DIR') {
        //查找子结点列表
        let items: [] = await SysDictionaryService.findList({
          cols: ['dicId'],
          dicParentId: fields.dicId,
          dicType: 'DATA',
        });
        if (items?.length >= 0) {//更新子结点启用状态
          let childrenIds = items.map(item => item['dicId']);
          await SysDictionaryService.updateByIds({
            cols: ['dicState', 'dicModule','dicGroup'],
            dicState: fields.dicState,
            dicModule: fields.dicModule,
            dicGroup: fields.dicGroup,
            dicIdList: childrenIds,
          });
        }
      }
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.dictionary.update.success',
        defaultMessage: '更新成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.dictionary.update.error',
        defaultMessage: '更新失败请重试！',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param selectedRows 选中行记录列表
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  delete = async (selectedRows: SysDictionaryItem[]) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.dictionary.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!selectedRows) return true;
    try {
      //删除字典包含的字典项节点列表
      let dirIds = selectedRows.filter(row => row.dicType === 'DIR').map(row => row.dicId);
      dirIds?.length > 0 && await SysDictionaryService.delete({ dicParentIdList: dirIds });
      //删除节点记录
      await SysDictionaryService.deleteByIds(selectedRows.map(row => row.dicId));
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.dictionary.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.dictionary.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };
  /**
   * 删除选中行数据
   * @param record 被删除的记录
   * @return
   * @author jiang
   * @date 2020-12-18 23:00:13
   **/
  deleteById = async (record: SysDictionaryItem) => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.dictionary.delete.loading',
      defaultMessage: '正在删除',
    }));
    if (!record) return true;
    try {
      if (record.dicType === 'DIR') {//先删除字典项列表
        await SysDictionaryService.delete({ dicParentId: record.dicId });
      }
      await SysDictionaryService.deleteById(record.dicId);
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.dictionary.delete.success',
        defaultMessage: '删除成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.dictionary.delete.error',
        defaultMessage: '删除失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 初始化字典和字典项列表
   *
   * @return
   * @author jiang
   * @date 2020-12-26 13:31:27
   **/
  init = async () => {
    const hide = message.loading(this.intl.formatMessage({
      id: 'page.sys.dictionary.init.loading',
      defaultMessage: '正在初始化字典',
    }));
    try {
      await SysDictionaryService.init();
      hide();
      message.success(this.intl.formatMessage({
        id: 'page.sys.dictionary.init.success',
        defaultMessage: '初始化成功，即将刷新!',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(this.intl.formatMessage({
        id: 'page.sys.dictionary.init.error',
        defaultMessage: '初始化失败，请重试!',
      }));
      return false;
    }
  };
};
