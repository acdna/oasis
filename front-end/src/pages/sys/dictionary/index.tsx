import { CaretRightOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, Dispatch, FormattedMessage, useIntl } from 'umi';
import {DefaultFooter, FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysDictionaryService } from '@/services/sys/sys.dictionary.service';
import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import { SysDictionaryHandles } from '@/pages/sys/dictionary/handles';
import SysDictionaryCreateForm from '@/pages/sys/dictionary/create.form';
import SysDictionaryUpdateForm from '@/pages/sys/dictionary/update.form';
import * as moment from 'moment';
import { ConnectState } from '@/models/connect';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import DictMapper from '@/common/dictionary/dict.mapper';
import SysDictionaryItemUpdateForm from '@/pages/sys/dictionary/items/update.form';
import SysDictionaryItemCreateForm from '@/pages/sys/dictionary/items/create.form';
import { dictValueEnum } from '@/components/common/select.options';
import { MLOCALES } from '@/common/locales/locale';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_MODULES_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';
import Nav0 from "@/pages/Home/Nav0";
import {Nav00DataSource} from "@/pages/Home/data.source";
import {AuthContext} from "@/common/auth/auth.context";

/**
 * SYS_DICTIONARY-属性对象
 * @author: jiangbin
 * @date: 2020-12-24 15:37:17
 **/
interface SysDictionaryTableListProps {
  dictMap: DictMapper;
  dispatch: Dispatch;
}

/**
 * SYS_DICTIONARY-列表查询
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
const SysDictionaryTableList: React.FC<SysDictionaryTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [createItemModalVisible, handleItemCreateModalVisible] = useState<boolean>(false);
  const [updateItemModalVisible, handleItemUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysDictionaryItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysDictionaryItem[]>([]);
  const handles = new SysDictionaryHandles();
  const { dictMap, dispatch } = props;
  const intl = useIntl();

  //加载字典和权限
  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysDictionaryItem>[] = [
    {
      dataIndex: 'dicId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicModule.label" defaultMessage="模块名" />,
      dataIndex: 'dicModule',
      valueType: 'select',
      sorter: (source, target) => (source.dicModule || '').localeCompare(target.dicModule || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicModule.tip" defaultMessage="模块名" />
            ),
          },
        ],
      },
      renderText: (text) => dictMap.getItemName(SYS_MODULES_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_MODULES_DICT_GROUP.group))),
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },
    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicName.label" defaultMessage="字典名称" />,
      dataIndex: 'dicName',
      valueType: 'text',
      search: false,
      sorter: (source, target) => (source.dicName || '').localeCompare(target.dicName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicName.tip" defaultMessage="字典名称" />
            ),
          },
        ],
      },
    },
    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicNameLike.label" defaultMessage="字典名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.dictionary.dicNameLike.placeholder',
        defaultMessage: '字典名称模糊查询',
      })),
      dataIndex: 'dicNameLike',
      valueType: 'text',
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicNameEn.label" defaultMessage="英文名" />,
      dataIndex: 'dicNameEn',
      valueType: 'text',
      sorter: (source, target) => (source.dicNameEn || '').localeCompare(target.dicNameEn || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicNameEn.tip" defaultMessage="英文名" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicValue.label" defaultMessage="字典值" />,
      dataIndex: 'dicValue',
      valueType: 'text',
      search: false,
      sorter: (source, target) => (source.dicValue || '').localeCompare(target.dicValue || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicValue.tip" defaultMessage="字典值" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicGroup.label" defaultMessage="分组号" />,
      dataIndex: 'dicGroup',
      valueType: 'text',
      sorter: (source, target) => (source.dicGroup || '').localeCompare(target.dicGroup || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicGroup.tip" defaultMessage="分组号" />
            ),
          },
        ],
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicParentId.label" defaultMessage="父级ID" />,
      dataIndex: 'dicParentId',
      valueType: 'text',
      sorter: false,
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicType.label" defaultMessage="字典类型" />,
      dataIndex: 'dicType',
      valueType: 'text',
      hideInForm: true,
      sorter: false,
      search: false,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicOrder.label" defaultMessage="排序" />,
      dataIndex: 'dicOrder',
      valueType: 'digit',
      hideInForm: true,
      search: false,
      sorter: (source, target) => (source.dicOrder - target.dicOrder),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicOrder.tip" defaultMessage="排序" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicState.label" defaultMessage="状态" />,
      dataIndex: 'dicState',
      valueType: 'text',
      sorter: (source, target) => (source.dicState || '').localeCompare(target.dicState || ''),
      renderText: (text) => dictMap.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_ENABLE_STATE_DICT_GROUP.group))),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicState.tip" defaultMessage="状态" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicSpecies.label" defaultMessage="关联种属" />,
      dataIndex: 'dicSpecies',
      valueType: 'text',
      hideInForm: true,
      sorter: false,
      search: false,
      hideInTable: true,
      hideInDescriptions: true,
    },
    {
      title: <FormattedMessage id="pages.list.sys.dictionary.dicUpdateDate.label" defaultMessage="日期" />,
      dataIndex: 'dicUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      sorter: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.dictionary.dicUpdateDate.tip" defaultMessage="日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.dictionary.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      width: 180,
      align:'center',
      fixed: 'right',
      render: (_, record) => (
        <>

          <a
            onClick={() => {
              if (record.dicType === 'DIR') {
                handleUpdateModalVisible(true);
              } else {
                handleItemUpdateModalVisible(true);
              }
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.dictionary.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.dictionary.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除字典吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.dictionary.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
          {(record.dicType === 'DIR') && (<Divider type={'vertical'} />)}
          {(record.dicType === 'DIR') && (
            <a
              onClick={() => {
                handleItemCreateModalVisible(true);
                setFormValues(record);
              }
              }
            >
              <FormattedMessage id="pages.list.sys.dictionary.option.create.item" defaultMessage="新建" />
            </a>
          )}
        </>
      ),
    },
  ];

  return (
    <>
    <PageContainer>
      <ProTable<SysDictionaryItem>
        headerTitle={<FormattedMessage id='pages.list.sys.dictionary.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="dicId"
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.dictionary.new.dict" defaultMessage="新建字典" />
          </Button>,
          <Popconfirm title={<FormattedMessage id='pages.list.sys.dictionary.option.init.confirm'
                                               defaultMessage='将先删除已有字典再进行初始化操作,请确定是否继续?' />}
                      onConfirm={async () => {
                        const success = await handles.init();
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <Button type="dashed">
              <CaretRightOutlined /><FormattedMessage id="pages.list.sys.dictionary.init.dict" defaultMessage="初始化字典" />
            </Button>
          </Popconfirm>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysDictionaryService.findPageDetail({
            ...params,
            sorter,
            filter,
            dicType: 'DIR',
            ndicId: '1',
          });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 10,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.dictionary.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.dictionary.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger={true}
                  onClick={async () => {
                    await handles.delete(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.dictionary.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysDictionaryCreateForm onCancel={() => handleCreateModalVisible(false)}
                               onSubmit={async (value) => {
                                 value.dicType = 'DIR';
                                 if (!value.dicState) value.dicState = 'ON';
                                 const success = await handles.add(value);
                                 setFormValues({});
                                 if (success) {
                                   handleCreateModalVisible(false);
                                   if (actionRef.current) {
                                     actionRef.current.reload();
                                   }
                                 }
                               }
                               }
                               modalVisible={createModalVisible}
                               dictMap={dictMap}
      >
      </SysDictionaryCreateForm>

      {formValues && Object.keys(formValues).length && formValues['dicType'] === 'DIR' ? (
        <SysDictionaryUpdateForm
          onSubmit={async (value: any) => {
            value.dicType = 'DIR';
            if (!value.dicState) value.dicState = 'ON';
            const success = await handles.update(value);
            setFormValues({});
            if (success) {
              handleUpdateModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}

      <SysDictionaryItemCreateForm onCancel={() => handleItemCreateModalVisible(false)}
                                   onSubmit={async (value) => {
                                     value.dicType = 'DATA';
                                     value.dicParentId = formValues['dicId'];
                                     if (!value.dicState) value.dicState = 'ON';
                                     const success = await handles.add(value);
                                     setFormValues({});
                                     if (success) {
                                       handleItemCreateModalVisible(false);
                                       if (actionRef.current) {
                                         actionRef.current.reload();
                                       }
                                     }
                                   }
                                   }
                                   modalVisible={createItemModalVisible}
                                   dictMap={dictMap}
                                   dict={formValues}
      >
      </SysDictionaryItemCreateForm>

      {(formValues && Object.keys(formValues).length > 0 && formValues['dicType'] === 'DATA') ? (
        <SysDictionaryItemUpdateForm
          onSubmit={async (value: any) => {
            value.dicType = 'DATA';
            if (!value.dicState) value.dicState = 'ON';
            const success = await handles.update(value);
            setFormValues({});
            if (success) {
              handleItemUpdateModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleItemUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateItemModalVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.dicId && (
          <ProDescriptions<SysDictionaryItem>
            column={2}
            title={row?.dicName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                dicId: row?.dicId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
      <DefaultFooter copyright={AuthContext.copyright}/>
    </>
  );
};

const mapStateToProps = ({ sys_dictionary }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap };
};

export default connect(mapStateToProps)(SysDictionaryTableList);


