import { SysDictionaryService } from '@/services/sys/sys.dictionary.service';

/**
 * 字典和字典项记录创建和更新相关后端验证功能
 * @author: jiangbin
 * @date: 2020-12-28 15:09:56
 **/
export const SYS_DICTIONARY_VALIDATOR = {

  /**
   * 字典分组验证功能
   * @param dicGroup 输入框输入的字典分组
   * @param dicId 字典ID，给定值时用于更新字典验证，不给定值用于创建字典验证
   * @author: jiangbin
   * @date: 2020-12-28 15:10:36
   **/
  DIC_GROUP: async (dicGroup: string, dicId = '') => {
    let res = await SysDictionaryService.findOne({
      dicType: 'DIR',
      dicGroup,
      ndicId: dicId,
    });
    return (!res || res.dicGroup != dicGroup);
  },
  /**
   * 字典名验证功能
   * @param dicName 输入框输入的字典名
   * @param dicId 字典ID，给定值时用于更新字典验证，不给定值用于创建字典验证
   * @author: jiangbin
   * @date: 2020-12-28 15:10:36
   **/
  DIC_NAME: async (dicName: string, dicId = '') => {
    let res = await SysDictionaryService.findOne({
      dicName,
      ndicId: dicId,
      dicType: 'DIR',
    });
    return (!res || res.dicName != dicName);
  },
  /**
   * 字典英文名验证功能
   * @param dicNameEn 输入框输入的字典英文名
   * @param dicId 字典ID，给定值时用于更新字典验证，不给定值用于创建字典验证
   * @author: jiangbin
   * @date: 2020-12-28 15:10:36
   **/
  DIC_NAME_EN: async (dicNameEn: string, dicId = '') => {
    let res = await SysDictionaryService.findOne({
      dicNameEn,
      ndicId: dicId,
      dicType: 'DIR',
    });
    return (!res || res.dicNameEn != dicNameEn);
  },
  /**
   * 字典项名验证功能
   * @param dicName 输入框输入的字典名
   * @param dicGroup 字典分组
   * @param dicId 字典ID，给定值时用于更新字典项验证，不给定值用于创建字典项验证
   * @author: jiangbin
   * @date: 2020-12-28 15:10:36
   **/
  ITEM_DIC_NAME: async (dicName: string, dicGroup: string, dicId = '') => {
    let res = await SysDictionaryService.findOne({
      ndicId: dicId,
      dicName,
      dicType: 'DATA',
      dicGroup,
    });
    return (!res || res.dicName != dicName);
  },
  /**
   * 字典项英文名验证功能
   * @param dicNameEn 输入框输入的字典名英文名
   * @param dicGroup 字典分组
   * @param dicId 字典ID，给定值时用于更新字典项验证，不给定值用于创建字典项验证
   * @author: jiangbin
   * @date: 2020-12-28 15:10:36
   **/
  ITEM_DIC_NAME_EN: async (dicNameEn: string, dicGroup: string, dicId = '') => {
    let res = await SysDictionaryService.findOne({
      dicNameEn,
      dicType: 'DATA',
      dicGroup,
      ndicId: dicId,
    });
    return (!res || res.dicNameEn != dicNameEn);
  },
};
