import { useEffect } from 'react';
import { Dispatch } from 'umi';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SysMenuService } from '@/services/sys/sys.menu.service';
import { SysMenuItem } from '@/pages/sys/menu/data';
import { TreeCreator } from '@/common/tree/tree.creator';
import { SysRoleMenuService } from '@/services/sys/sys.role.menu.service';
import { SysRoleMenuItem } from '@/pages/sys/role/menu/data';
import { SysDictionaryService } from '@/services/sys/sys.dictionary.service';
import { SysDictionaryItem } from '@/pages/sys/dictionary/data';
import { SysRoleDictionaryService } from '@/services/sys/sys.role.dictionary.service';
import { SysRoleDictionaryItem } from '@/pages/sys/role/dictionary/data';
import { SysPermissionService } from '@/services/sys/sys.permission.service';
import { SysPermissionItem } from '@/pages/sys/permission/data';
import { SysRolePermissionService } from '@/services/sys/sys.role.permission.service';
import { SysRolePermissionItem } from '@/pages/sys/role/permission/data';
import { Space, Tag } from 'antd';
import { SYS_ENABLE_STATE_DICT_GROUP, SYS_SUB_SYSTEM_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';
import { getLocaleMessage } from '@/common/locales/locale';

/**
 * 树数据加载器
 * @author: jiangbin
 * @date: 2021-01-04 09:26:23
 **/
interface SysTreeDataLoaderParams {
  setDataSource?: any;
  dictMap?: DictMapper;
  setExpandedKeys?: any;
  setCheckedKeys?: any;
  roleId?: string;
  setNodes: any;
}

/**
 * 通过useEffect加载数据到Model中，主要是为了提高复用性
 * @author: jiangbin
 * @date: 2020-12-24 15:57:40
 **/
export const SYS_ROLE_EFFECTS = {

  /**
   * 加载当前模块的权限列表
   * @author: jiangbin
   * @date: 2020-12-25 14:23:19
   **/
  LOAD_ALL: (dispatch: Dispatch, deps = []) => {
    useEffect(() => {
      dispatch({
        type: 'sys_role/fetchAll',
      });
    }, deps);
  },
  /**
   * 加载全部菜单树数据
   * @param params 参数对象
   * @author: jiangbin
   * @date: 2021-01-03 01:22:46
   **/
  menu: (params: SysTreeDataLoaderParams) => {
    useEffect(() => {
      (async () => {
        const { setDataSource, dictMap, setNodes } = params;
        let res: SysMenuItem[] = await SysMenuService.findAll();
        if (!res || res.length == 0) return;
        const nodes = res.map((item) => {
          let menuState = dictMap?.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, item.menuState);
          let node = {
            id: item.menuId,
            order: item.menuOrder,
            parentId: item.menuParentId,
            title: (
              <>
                <span>{getLocaleMessage({ en: item.menuNameEn, cn: item.menuName })}</span>
                <Space style={{ float: 'right' }}>
                  {(item.menuType != 'DIR' && item.menuUrl?.length > 0) && (
                    <Tag color='processing'>{item.menuUrl}</Tag>)}
                  <Tag color={item.menuState == 'ON' ? 'success' : 'error'}>{menuState}</Tag>
                </Space>
              </>
            ),
            key: item.menuId,
          };
          return node;
        });
        //保存全部节点列表
        setNodes(nodes);
        const pmsTree = new TreeCreator().tree(nodes);
        setDataSource(pmsTree);
        await SYS_ROLE_EFFECTS.loadRoleMenus(params);
      })();
    }, []);
  },
  /**
   * 将用户已授权菜单项设置成选中状态
   * @param params 参数对象
   * @author: jiangbin
   * @date: 2021-01-03 12:06:18
   **/
  loadRoleMenus: async (params: SysTreeDataLoaderParams) => {
    const { setCheckedKeys, roleId } = params;

    //查询已授权菜单信息
    let rmRes: SysRoleMenuItem[] = await SysRoleMenuService.findList({ rmRoleId: roleId });
    if (!rmRes || rmRes.length == 0) return;
    let menuIdList = rmRes.map((item) => item.rmMenuId);

    //只查询叶子节点的菜单
    let mRes: SysMenuItem[] = await SysMenuService.findList({ menuIdList, menuType: 'DATA' });
    if (!mRes || mRes.length == 0) return;
    let idList = mRes.map((item) => item.menuId);
    setCheckedKeys(idList);
  },
  /**
   * 加载全部字典树数据
   * @param params.hasLoad 是否已加载过菜单数据的标志StateHook
   * @param params.setHasLoad 设置是否已加载过菜单数据的标志StateHook方法
   * @param params.setDataSource 设置Tree插件数据源的StateHook
   * @param params.dictMap 字典映射表
   * @author: jiangbin
   * @date: 2021-01-03 01:22:46
   **/
  dict: (params: SysTreeDataLoaderParams) => {
    useEffect(() => {
      (async () => {
        const { setDataSource, dictMap, setNodes } = params;
        //查询全部数据
        let res: SysDictionaryItem[] = await SysDictionaryService.findAll();
        if (!res || res.length == 0) return;
        //转换成树节点列表
        const nodes = res.map((item) => {
          let dictState = dictMap?.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, item.dicState);
          let node = {
            id: item.dicId,
            order: item.dicOrder,
            parentId: item.dicParentId,
            title: (
              <>
                <span>{getLocaleMessage({ cn: item.dicName, en: item.dicNameEn })}</span>
                <Space style={{ float: 'right' }}>
                  {(item.dicValue?.length > 0) && (<Tag color='purple'>{item.dicValue}</Tag>)}
                  <Tag color='pink'>{item.dicModule}</Tag>
                  <Tag color={item.dicState == 'ON' ? 'success' : 'error'}>{dictState}</Tag>
                </Space>
              </>
            ),
            key: item.dicId,
          };
          return node;
        });
        //保存全部节点列表
        setNodes(nodes);
        //构建成树型结构
        const pmsTree = new TreeCreator().tree(nodes);
        setDataSource(pmsTree);
        //加载角色绑定的字典项
        await SYS_ROLE_EFFECTS.loadRoleDicts(params);
      })();
    }, []);
  },
  /**
   * 将用户已授权字典项设置成选中状态
   * @param params 参数对象
   * @author: jiangbin
   * @date: 2021-01-03 12:06:18
   **/
  loadRoleDicts: async (params: SysTreeDataLoaderParams) => {
    //查询已授权字典
    const { setCheckedKeys, roleId } = params;
    let rdRes: SysRoleDictionaryItem[] = await SysRoleDictionaryService.findList({ rdRoleId: roleId });
    if (!rdRes || rdRes.length == 0) return;
    let dicIdList = rdRes.map((item) => item.rdDictionaryId);

    //只查询叶子节点的菜单
    let dRes: SysDictionaryItem[] = await SysDictionaryService.findList({ dicIdList, dicType: 'DATA' });
    if (!dRes || dRes.length == 0) return;
    let idList = dRes.map((item) => item.dicId);
    setCheckedKeys(idList);
  },
  /**
   * 加载全部权限树数据
   * @param params 参数对象
   * @author: jiangbin
   * @date: 2021-01-03 01:22:46
   **/
  pms: (params: SysTreeDataLoaderParams) => {
    useEffect(() => {
      (async () => {
        const { setDataSource, dictMap, setNodes } = params;
        //查询全部权限数据
        let res: SysPermissionItem[] = await SysPermissionService.findAll();
        if (!res || res.length == 0) return;
        //转换成树节点列表
        const nodes = res.map((item) => {
          let pmsState = dictMap?.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, item.perState);
          let pmsSystem = dictMap?.getItemName(SYS_SUB_SYSTEM_DICT_GROUP.group, item.perSystem);
          let node = {
            id: item.perId,
            order: item.perOrder,
            parentId: item.perParentId,
            title: (
              <>
                <span>{getLocaleMessage({ cn: item.perName, en: item.perNameEn })}</span>
                <Space style={{ float: 'right' }}>
                  {(item.perUrl?.length > 0) && (<Tag color='purple'>{item.perUrl}</Tag>)}
                  <Tag color='orange'>{pmsSystem}</Tag>
                  <Tag color={item.perState == 'ON' ? 'success' : 'error'}>{pmsState}</Tag>
                </Space>
              </>
            ),
            key: item.perId,
          };
          return node;
        });
        //保存全部节点列表
        setNodes(nodes);
        //构建成树形结构
        const pmsTree = new TreeCreator().tree(nodes);
        setDataSource(pmsTree);
        //加载角色绑定的权限项
        await SYS_ROLE_EFFECTS.loadRolePms(params);
      })();
    }, []);
  },
  /**
   * 将用户已授权权限项设置成选中状态
   * @param params 参数对象
   * @author: jiangbin
   * @date: 2021-01-03 12:06:18
   **/
  loadRolePms: async (params: SysTreeDataLoaderParams) => {
    const { setCheckedKeys, roleId } = params;

    //查询已授权权限
    let rpRes: SysRolePermissionItem[] = await SysRolePermissionService.findList({ rpRoleId: roleId });
    if (!rpRes || rpRes.length == 0) return;
    let perIdList = rpRes.map((item) => item.rpPermissionId);

    //只查询叶子节点的菜单
    let pRes: SysPermissionItem[] = await SysPermissionService.findList({
      cols: ['perId'],
      perIdList,
      perType: 'DATA',
    });
    if (!pRes || pRes.length == 0) return;
    let idList = pRes.map((item) => item.perId);

    //更新树节点选择状态
    setCheckedKeys(idList);
  },
};


