import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysRoleMenuItem } from "@/pages/sys/role/menu/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_ROLE_MENU-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRoleMenuCreateFormProps {
    onSubmit: (fields: SysRoleMenuItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_ROLE_MENU-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleMenuCreateForm: React.FC<SysRoleMenuCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { rmId,rmRoleId,rmMenuId }=fieldValues;
        onSubmit({ rmId,rmRoleId,rmMenuId });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.role.menu.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="rmId"
                        label={<FormattedMessage id="pages.create.sys.role.menu.rmId.label" defaultMessage="角色菜单ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.menu.rmId.tip" defaultMessage="角色菜单ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.menu.rmId.placeholder",defaultMessage:"角色菜单ID"})}/>
                </Form.Item>
                
                <Form.Item
                        name="rmRoleId"
                        label={<FormattedMessage id="pages.create.sys.role.menu.rmRoleId.label" defaultMessage="角色ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.menu.rmRoleId.tip" defaultMessage="角色ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.menu.rmRoleId.placeholder",defaultMessage:"角色ID"})}/>
                </Form.Item>
                
                <Form.Item
                        name="rmMenuId"
                        label={<FormattedMessage id="pages.create.sys.role.menu.rmMenuId.label" defaultMessage="菜单ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.menu.rmMenuId.tip" defaultMessage="菜单ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.menu.rmMenuId.placeholder",defaultMessage:"菜单ID"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysRoleMenuCreateForm;
