/**
 * SYS_ROLE_MENU-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleMenuItem {
  rmId: string;
  rmRoleId: string;
  rmMenuId: string;
}

/**
 * SYS_ROLE_MENU-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleMenuPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_ROLE_MENU-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleMenuTableData {
  list: SysRoleMenuItem[];
  pagination: Partial<SysRoleMenuPagination>;
}

/**
 * SYS_ROLE_MENU-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleMenuParams extends SysRoleMenuItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };

  rmMenuIdList?: string[];
}
