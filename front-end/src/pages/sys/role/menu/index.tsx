import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysRoleMenuService } from '@/services/sys/sys.role.menu.service';
import { SysRoleMenuItem } from '@/pages/sys/role/menu/data';
import { SysRoleMenuHandles } from '@/pages/sys/role/menu/handles';
import SysRoleMenuCreateForm from '@/pages/sys/role/menu/create.form';
import SysRoleMenuUpdateForm from '@/pages/sys/role/menu/update.form';


/**
 * SYS_ROLE_MENU-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleMenuTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysRoleMenuItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysRoleMenuItem[]>([]);
    const handles = new SysRoleMenuHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysRoleMenuItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.role.menu.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'rmId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.role.menu.rmId.label" defaultMessage="角色菜单ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.menu.rmId.tip',
                defaultMessage: '角色菜单ID查询',
            })),
            dataIndex: 'rmId',
            valueType:'text',
             sorter: (source, target) => (source.rmId||'').localeCompare(target.rmId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.menu.rmId.tip" defaultMessage="角色菜单ID" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.menu.rmRoleId.label" defaultMessage="角色ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.menu.rmRoleId.tip',
                defaultMessage: '角色ID查询',
            })),
            dataIndex: 'rmRoleId',
            valueType:'text',
             sorter: (source, target) => (source.rmRoleId||'').localeCompare(target.rmRoleId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.menu.rmRoleId.tip" defaultMessage="角色ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.menu.rmMenuId.label" defaultMessage="菜单ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.menu.rmMenuId.tip',
                defaultMessage: '菜单ID查询',
            })),
            dataIndex: 'rmMenuId',
            valueType:'text',
             sorter: (source, target) => (source.rmMenuId||'').localeCompare(target.rmMenuId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.menu.rmMenuId.tip" defaultMessage="菜单ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.menu.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.role.menu.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.role.menu.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.role.menu.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysRoleMenuItem>
                headerTitle={<FormattedMessage id='pages.list.sys.role.menu.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="rmId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.role.menu.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysRoleMenuService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.role.menu.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.role.menu.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.role.menu.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysRoleMenuCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysRoleMenuCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysRoleMenuUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.rmId && (
                    <ProDescriptions<SysRoleMenuItem>
                        column={2}
                        title={row?.rmId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            rmId: row?.rmId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysRoleMenuTableList;
