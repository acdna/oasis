import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysRoleMenuItem } from "@/pages/sys/role/menu/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_ROLE_MENU-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRoleMenuUpdateFormProps {
    onSubmit: (fields: SysRoleMenuItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_ROLE_MENU-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleMenuUpdateForm: React.FC<SysRoleMenuUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        rmId: values.rmId,
        rmRoleId: values.rmRoleId,
        rmMenuId: values.rmMenuId,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>
            <Input name='rmId' value={formVals.rmId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="rmId"
                label={<FormattedMessage id='pages.update.sys.role.menu.rmId.label' defaultMessage='角色菜单ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.menu.rmId.tip' defaultMessage='请输入角色菜单ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.menu.update.rmId.tip',
                    defaultMessage: '请输入角色菜单ID！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="rmRoleId"
                label={<FormattedMessage id='pages.update.sys.role.menu.rmRoleId.label' defaultMessage='角色ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.menu.rmRoleId.tip' defaultMessage='请输入角色ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.menu.update.rmRoleId.tip',
                    defaultMessage: '请输入角色ID！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="rmMenuId"
                label={<FormattedMessage id='pages.update.sys.role.menu.rmMenuId.label' defaultMessage='菜单ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.menu.rmMenuId.tip' defaultMessage='请输入菜单ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.menu.update.rmMenuId.tip',
                    defaultMessage: '请输入菜单ID！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.role.menu.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     rmId: formVals.rmId,
                     rmRoleId: formVals.rmRoleId,
                     rmMenuId: formVals.rmMenuId,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysRoleMenuUpdateForm;