import React, { useState } from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysRoleItem } from '@/pages/sys/role/data';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options } from '@/components/common/select.options';
import { SYS_ENABLE_STATE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_ROLE-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRoleUpdateFormProps {
  onSubmit: (fields: SysRoleItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_ROLE-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleUpdateForm: React.FC<SysRoleUpdateFormProps> = (props) => {
  const { onSubmit, onCancel, updateModalVisible, values,dictMap } = props;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    roleId: values.roleId,
    roleName: values.roleName,
    roleNameEn: values.roleNameEn,
    roleState: values.roleState,
    roleSys: values.roleSys,

  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const renderContent = () => (
    <>
      <Form.Item
        name="roleName"
        label={<FormattedMessage id='pages.update.sys.role.roleName.label' defaultMessage='角色名称' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.role.roleName.tip' defaultMessage='请输入角色名称！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.role.update.roleName.tip',
          defaultMessage: '请输入角色名称！',
        })} />
      </Form.Item>

      <Form.Item
        name="roleNameEn"
        label={<FormattedMessage id='pages.update.sys.role.roleNameEn.label' defaultMessage='角色英文名' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.role.roleNameEn.tip' defaultMessage='请输入角色英文名！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.update.sys.role.roleNameEn.placeholder',
          defaultMessage: '请输入角色英文名！',
        })} />
      </Form.Item>

      <Form.Item
        name="roleSys"
        label={<FormattedMessage id='pages.update.sys.role.roleSys.label' defaultMessage='角色所属系统' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.role.roleSys.tip' defaultMessage='请输入角色所属系统！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.role.update.roleSys.tip',
          defaultMessage: '请输入角色所属系统！',
        })} />
      </Form.Item>

      <Form.Item
        name="roleState"
        label={<FormattedMessage id='pages.update.sys.role.roleState.label' defaultMessage='状态' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.role.roleState.tip' defaultMessage='请输入状态！' />),
          },
        ]}
      >
        <Select
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.update.sys.role.roleState.placeholder',
            defaultMessage: '请输入状态',
          })
          }
        >
          {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>
    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{ padding: '32px 40px 48px' }}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.role.form.title' defaultMessage='修改' />}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            roleId: formVals.roleId,
            roleName: formVals.roleName,
            roleNameEn: formVals.roleNameEn,
            roleState: formVals.roleState,
            roleSys: formVals.roleSys,
          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysRoleUpdateForm;
