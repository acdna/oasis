import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysRoleService } from '@/services/sys/sys.role.service';
import { SysRoleItem } from '@/pages/sys/role/data';
import { SysRoleHandles } from '@/pages/sys/role/handles';
import SysRoleCreateForm from '@/pages/sys/role/create.form';
import SysRoleUpdateForm from '@/pages/sys/role/update.form';
import { ConnectState } from '@/models/connect';
import DictMapper from '@/common/dictionary/dict.mapper';
import { Dispatch } from '@@/plugin-dva/connect';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import SysRoleDictionaryRelateForm from '@/pages/sys/role/dictionary/relate.form';
import SysRoleMenuRelateForm from '@/pages/sys/role/menu/relate.form';
import SysRolePermissionRelateForm from '@/pages/sys/role/permission/relate.form';
import { SYS_ENABLE_STATE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_ROLE-属性对象
 * @author: jiangbin
 * @date: 2020-12-24 15:37:17
 **/
interface SysRoleTableListProps {
  dictMap: DictMapper;
  dispatch: Dispatch;
}

/**
 * SYS_ROLE-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleTableList: React.FC<SysRoleTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [menuRelateVisible, handleMenuRelateVisible] = useState<boolean>(false);
  const [pmsRelateVisible, handlePmsRelateVisible] = useState<boolean>(false);
  const [dictRelateVisible, handleDictRelateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysRoleItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysRoleItem[]>([]);
  const handles = new SysRoleHandles();
  const { dictMap, dispatch } = props;
  const intl = useIntl();

  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysRoleItem>[] = [
    {
      title: <FormattedMessage id="pages.list.sys.role.index.label" defaultMessage="序号" />,
      dataIndex: 'index',
      valueType: 'indexBorder',
      fixed: true,
      width: 50,
    },
    {
      dataIndex: 'roleId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.role.roleName.label" defaultMessage="角色名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.role.roleName.tip',
        defaultMessage: '角色名称查询',
      })),
      dataIndex: 'roleName',
      valueType: 'text',
      width: 180,
      sorter: (source, target) => (source.roleName || '').localeCompare(target.roleName || ''),
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.role.roleName.tip" defaultMessage="角色名称" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.role.roleNameEn.label" defaultMessage="角色英文名" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.role.roleNameEn.tip',
        defaultMessage: '角色英文名查询',
      })),
      dataIndex: 'roleNameEn',
      valueType: 'text',
      width: 180,
      sorter: (source, target) => (source.roleNameEn || '').localeCompare(target.roleNameEn || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.role.roleNameEn.tip" defaultMessage="角色英文名" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.role.roleSys.label" defaultMessage="角色所属系统" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.role.roleSys.tip',
        defaultMessage: '角色所属系统查询',
      })),
      dataIndex: 'roleSys',
      valueType: 'text',
      width: 150,
      sorter: (source, target) => (source.roleSys || '').localeCompare(target.roleSys || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.role.roleSys.tip" defaultMessage="角色所属系统" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.role.roleState.label" defaultMessage="状态" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.role.roleState.tip',
        defaultMessage: '状态查询',
      })),
      dataIndex: 'roleState',
      valueType: 'text',
      width: 100,
      sorter: (source, target) => (source.roleState || '').localeCompare(target.roleState || ''),
      renderText: (text) => dictMap.getItemName(SYS_ENABLE_STATE_DICT_GROUP.group, text),
      valueEnum: dictMap.valueEnum(SYS_ENABLE_STATE_DICT_GROUP.group),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.role.roleState.tip" defaultMessage="状态" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.role.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      width: 200,
      align:'center',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.role.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.role.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.role.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
          <Divider type="vertical" />
          <a
            onClick={() => {
              handleMenuRelateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.role.option.relate.menu" defaultMessage="菜单" />
          </a>
          <Divider type="vertical" />
          <a
            onClick={() => {
              handlePmsRelateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.role.option.relate.pms" defaultMessage="权限" />
          </a>
          <Divider type="vertical" />
          <a
            onClick={() => {
              handleDictRelateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.role.option.relate.dict" defaultMessage="字典" />
          </a>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysRoleItem>
        headerTitle={<FormattedMessage id='pages.list.sys.role.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="roleId"
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.role.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysRoleService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.role.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.role.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.role.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysRoleCreateForm onCancel={() => handleCreateModalVisible(false)}
                         onSubmit={async (value) => {
                           const success = await handles.add(value);
                           setFormValues({});
                           if (success) {
                             handleCreateModalVisible(false);
                             if (actionRef.current) {
                               actionRef.current.reload();
                             }
                           }
                         }
                         }
                         modalVisible={createModalVisible}
                         dictMap={dictMap}>
      </SysRoleCreateForm>

      {formValues && Object.keys(formValues).length > 0 ? (
        <SysRoleUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            setFormValues({});
            if (success) {
              handleUpdateModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}

      {formValues && Object.keys(formValues).length > 0 && dictRelateVisible ? (
        <SysRoleDictionaryRelateForm
          onCancel={() => {
            handleDictRelateVisible(false);
            setFormValues({});
          }
          }
          visible={dictRelateVisible}
          role={formValues}
          dictMap={dictMap}
        />
      ) : null}

      {formValues && Object.keys(formValues).length > 0 && menuRelateVisible ? (
        <SysRoleMenuRelateForm
          onCancel={() => {
            handleMenuRelateVisible(false);
            setFormValues({});
          }
          }
          visible={menuRelateVisible}
          role={formValues}
          dictMap={dictMap}
        />
      ) : null}

      {formValues && Object.keys(formValues).length > 0 && pmsRelateVisible ? (
        <SysRolePermissionRelateForm
          onCancel={() => {
            handlePmsRelateVisible(false);
            setFormValues({});
          }
          }
          visible={pmsRelateVisible}
          role={formValues}
          dictMap={dictMap}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.roleId && (
          <ProDescriptions<SysRoleItem>
            column={2}
            title={row?.roleName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                roleId: row?.roleId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_dictionary }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap };
};

export default connect(mapStateToProps)(SysRoleTableList);
