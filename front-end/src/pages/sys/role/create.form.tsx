import React from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysRoleItem } from '@/pages/sys/role/data';
import ProForm from '@ant-design/pro-form';
import { options } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SYS_ENABLE_STATE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';

/**
 * SYS_ROLE-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRoleCreateFormProps {
  onSubmit: (fields: SysRoleItem) => void,
  onCancel: () => void;
  modalVisible: boolean;
  dictMap: DictMapper;
}

/**
 * SYS_ROLE-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleCreateForm: React.FC<SysRoleCreateFormProps> = (props) => {
  const { modalVisible, onCancel, onSubmit, dictMap } = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { roleId, roleName, roleNameEn, roleState, roleSys } = fieldValues;
    onSubmit({ roleId, roleName, roleNameEn, roleState, roleSys });
  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.create.sys.role.form.title' defaultMessage='新建' />
      }
      visible={modalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false}>

        <Form.Item
          name="roleName"
          label={<FormattedMessage id="pages.create.sys.role.roleName.label" defaultMessage="角色名称" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.role.roleName.tip" defaultMessage="角色名称为必填项" />),
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.role.roleName.placeholder',
            defaultMessage: '角色名称',
          })} />
        </Form.Item>

        <Form.Item
          name="roleNameEn"
          label={<FormattedMessage id="pages.create.sys.role.roleNameEn.label" defaultMessage="角色英文名" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.role.roleNameEn.tip" defaultMessage="角色英文名为必填项" />),
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.role.roleNameEn.placeholder',
            defaultMessage: '角色英文名',
          })} />
        </Form.Item>

        <Form.Item
          name="roleSys"
          label={<FormattedMessage id="pages.create.sys.role.roleSys.label" defaultMessage="角色所属系统" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.role.roleSys.tip" defaultMessage="角色所属系统为必填项" />),
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.role.roleSys.placeholder',
            defaultMessage: '角色所属系统',
          })} />
        </Form.Item>

        <Form.Item
          name="roleState"
          label={<FormattedMessage id="pages.create.sys.role.roleState.label" defaultMessage="状态" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.role.roleState.tip" defaultMessage="状态为必填项" />),
          }]}
        >
          <Select
            style={{ width: '100%' }}
            placeholder={intl.formatMessage({
              id: 'pages.create.sys.role.roleState.placeholder',
              defaultMessage: '状态',
            })
            }
            defaultActiveFirstOption={true}
          >
            {options(SYS_ENABLE_STATE_DICT_GROUP.group, dictMap)}
          </Select>
        </Form.Item>
      </ProForm>
    </Modal>
  );
};

export default SysRoleCreateForm;
