import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysRolePermissionItem } from "@/pages/sys/role/permission/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_ROLE_PERMISSION-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRolePermissionUpdateFormProps {
    onSubmit: (fields: SysRolePermissionItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_ROLE_PERMISSION-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRolePermissionUpdateForm: React.FC<SysRolePermissionUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        rpId: values.rpId,
        rpRoleId: values.rpRoleId,
        rpPermissionId: values.rpPermissionId,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>
            <Input name='rpId' value={formVals.rpId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="rpId"
                label={<FormattedMessage id='pages.update.sys.role.permission.rpId.label' defaultMessage='角色权限表Id'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.permission.rpId.tip' defaultMessage='请输入角色权限表Id！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.permission.update.rpId.tip',
                    defaultMessage: '请输入角色权限表Id！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="rpRoleId"
                label={<FormattedMessage id='pages.update.sys.role.permission.rpRoleId.label' defaultMessage='角色ID号'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.permission.rpRoleId.tip' defaultMessage='请输入角色ID号！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.permission.update.rpRoleId.tip',
                    defaultMessage: '请输入角色ID号！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="rpPermissionId"
                label={<FormattedMessage id='pages.update.sys.role.permission.rpPermissionId.label' defaultMessage='权限ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.permission.rpPermissionId.tip' defaultMessage='请输入权限ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.permission.update.rpPermissionId.tip',
                    defaultMessage: '请输入权限ID！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.role.permission.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     rpId: formVals.rpId,
                     rpRoleId: formVals.rpRoleId,
                     rpPermissionId: formVals.rpPermissionId,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysRolePermissionUpdateForm;