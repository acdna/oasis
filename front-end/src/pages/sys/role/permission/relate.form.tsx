import React, { useState } from 'react';
import { Button, Drawer, message, Tree } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysRolePermissionItem } from '@/pages/sys/role/permission/data';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SysRolePermissionService } from '@/services/sys/sys.role.permission.service';
import { SysRoleData } from '@/models/sys/sys.role.model';
import { DeleteOutlined } from '@ant-design/icons';
import { getPrentIds } from '@/common/tree/tree.utils';
import { SYS_ROLE_EFFECTS } from '@/pages/sys/role/effects';
import { getLocaleMessage } from '@/common/locales/locale';

/**
 * SYS_ROLE_PERMISSION-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRolePermissionRelateFormProps {
  onCancel: () => void;
  visible: boolean;
  role: SysRoleData;
  dictMap: DictMapper;
}

/**
 * SYS_ROLE_PERMISSION-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRolePermissionRelateForm: React.FC<SysRolePermissionRelateFormProps> = (props) => {
  const { visible, onCancel, role, dictMap } = props;
  const intl = useIntl();
  const [expandedKeys, setExpandedKeys] = useState<string[]>([]);
  const [checkedKeys, setCheckedKeys] = useState<string[]>([]);
  const [dataSource, setDataSource] = useState<any[]>([]);
  const [nodes, setNodes] = useState<[]>([]);

  /**
   * 加载权限树和角色绑定的权限信息
   * @author: jiangbin
   * @date: 2021-01-03 18:47:18
   **/
  SYS_ROLE_EFFECTS.pms({
    setDataSource,
    dictMap,
    roleId: role.roleId,
    setExpandedKeys,
    setCheckedKeys,
    setNodes,
  });

  const onExpand = (expandedKeys: any) => {
    setExpandedKeys(expandedKeys);
  };

  const onCheck = (checkedKeys: any) => {
    setCheckedKeys(checkedKeys);
  };

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const hide = message.loading(intl.formatMessage({
      id: 'page.sys.role.permission.add.loading',
      defaultMessage: '正在授权',
    }));
    try {
      //先删除已授权权限
      await SysRolePermissionService.delete({ rpRoleId: role.roleId });

      //过滤出父级结点ID列表
      let pIds = getPrentIds(nodes, checkedKeys);
      pIds = pIds.concat(checkedKeys);

      //构建授权信息
      let relates: SysRolePermissionItem[] = pIds.map(pmsId => {
        return {
          rpId: '',
          rpRoleId: role.roleId || '',
          rpPermissionId: pmsId,
        };
      });

      //添加授权信息
      await SysRolePermissionService.addList(relates);
      hide();
      message.success(intl.formatMessage({
        id: 'page.sys.role.permission.add.success',
        defaultMessage: '授权成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(intl.formatMessage({
        id: 'page.sys.role.permission.add.error',
        defaultMessage: '授权失败请重试！',
      }));
      return false;
    }

  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    onCancel();
  };

  return (
    <Drawer
      destroyOnClose
      title={intl.formatMessage({
        id: 'pages.sys.role.permission.relate.title',
        defaultMessage: '权限授权',
      }) + `(${getLocaleMessage({en:role.roleNameEn,cn:role.roleName})||role.roleName})`}
      width={720}
      onClose={onCancel}
      visible={visible}
      bodyStyle={{ paddingBottom: 80 }}
      closable={true}
      footer={
        <>
          <div style={{ textAlign: 'left', float: 'left', paddingTop: 5 }}>
            <FormattedMessage id="pages.sys.role.permission.relate.chosen" defaultMessage="已选择" />{' '}
            <a style={{ fontWeight: 600 }}>{checkedKeys.length}</a>{' '}
            <FormattedMessage id="pages.sys.role.permission.relate.item" defaultMessage="项" />
          </div>
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setCheckedKeys([])} style={{ marginRight: 8 }}>
              <DeleteOutlined />{intl.formatMessage({
              id: 'pages.sys.role.permission.relate.cancel.all',
              defaultMessage: '取消选择',
            })}
            </Button>
            <Button onClick={() => handleCancel()} style={{ marginRight: 8 }}>
              {intl.formatMessage({ id: 'pages.sys.role.permission.relate.cancel', defaultMessage: '关闭' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.sys.role.permission.relate.submit', defaultMessage: '授权' })}
            </Button>
          </div>
        </>
      }
    >
      <Tree
        checkable
        showIcon
        blockNode={true}
        onExpand={onExpand}
        onCheck={onCheck}
        treeData={dataSource}
        expandedKeys={expandedKeys}
        checkedKeys={checkedKeys}
      />
    </Drawer>
  );
};

export default SysRolePermissionRelateForm;
