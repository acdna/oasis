/**
 * SYS_ROLE_PERMISSION-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRolePermissionItem {
  rpId: string;
  rpRoleId: string;
  rpPermissionId: string;
}

/**
 * SYS_ROLE_PERMISSION-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRolePermissionPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_ROLE_PERMISSION-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRolePermissionTableData {
  list: SysRolePermissionItem[];
  pagination: Partial<SysRolePermissionPagination>;
}

/**
 * SYS_ROLE_PERMISSION-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRolePermissionParams extends SysRolePermissionItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };
  rpRoleIdList?: string[];
}
