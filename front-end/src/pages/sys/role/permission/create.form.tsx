import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysRolePermissionItem } from "@/pages/sys/role/permission/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_ROLE_PERMISSION-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRolePermissionCreateFormProps {
    onSubmit: (fields: SysRolePermissionItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_ROLE_PERMISSION-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRolePermissionCreateForm: React.FC<SysRolePermissionCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { rpId,rpRoleId,rpPermissionId }=fieldValues;
        onSubmit({ rpId,rpRoleId,rpPermissionId });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.role.permission.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="rpId"
                        label={<FormattedMessage id="pages.create.sys.role.permission.rpId.label" defaultMessage="角色权限表Id"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.permission.rpId.tip" defaultMessage="角色权限表Id为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.permission.rpId.placeholder",defaultMessage:"角色权限表Id"})}/>
                </Form.Item>
                
                <Form.Item
                        name="rpRoleId"
                        label={<FormattedMessage id="pages.create.sys.role.permission.rpRoleId.label" defaultMessage="角色ID号"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.permission.rpRoleId.tip" defaultMessage="角色ID号为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.permission.rpRoleId.placeholder",defaultMessage:"角色ID号"})}/>
                </Form.Item>
                
                <Form.Item
                        name="rpPermissionId"
                        label={<FormattedMessage id="pages.create.sys.role.permission.rpPermissionId.label" defaultMessage="权限ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.permission.rpPermissionId.tip" defaultMessage="权限ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.permission.rpPermissionId.placeholder",defaultMessage:"权限ID"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysRolePermissionCreateForm;
