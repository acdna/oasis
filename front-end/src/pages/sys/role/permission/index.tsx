import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysRolePermissionService } from '@/services/sys/sys.role.permission.service';
import { SysRolePermissionItem } from '@/pages/sys/role/permission/data';
import { SysRolePermissionHandles } from '@/pages/sys/role/permission/handles';
import SysRolePermissionCreateForm from '@/pages/sys/role/permission/create.form';
import SysRolePermissionUpdateForm from '@/pages/sys/role/permission/update.form';


/**
 * SYS_ROLE_PERMISSION-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRolePermissionTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysRolePermissionItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysRolePermissionItem[]>([]);
    const handles = new SysRolePermissionHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysRolePermissionItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.role.permission.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'rpId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.role.permission.rpId.label" defaultMessage="角色权限表Id" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.permission.rpId.tip',
                defaultMessage: '角色权限表Id查询',
            })),
            dataIndex: 'rpId',
            valueType:'text',
             sorter: (source, target) => (source.rpId||'').localeCompare(target.rpId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.permission.rpId.tip" defaultMessage="角色权限表Id" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.permission.rpRoleId.label" defaultMessage="角色ID号" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.permission.rpRoleId.tip',
                defaultMessage: '角色ID号查询',
            })),
            dataIndex: 'rpRoleId',
            valueType:'text',
             sorter: (source, target) => (source.rpRoleId||'').localeCompare(target.rpRoleId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.permission.rpRoleId.tip" defaultMessage="角色ID号" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.permission.rpPermissionId.label" defaultMessage="权限ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.permission.rpPermissionId.tip',
                defaultMessage: '权限ID查询',
            })),
            dataIndex: 'rpPermissionId',
            valueType:'text',
             sorter: (source, target) => (source.rpPermissionId||'').localeCompare(target.rpPermissionId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.permission.rpPermissionId.tip" defaultMessage="权限ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.permission.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.role.permission.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.role.permission.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.role.permission.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysRolePermissionItem>
                headerTitle={<FormattedMessage id='pages.list.sys.role.permission.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="rpId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.role.permission.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysRolePermissionService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.role.permission.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.role.permission.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.role.permission.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysRolePermissionCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysRolePermissionCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysRolePermissionUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.rpId && (
                    <ProDescriptions<SysRolePermissionItem>
                        column={2}
                        title={row?.rpId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            rpId: row?.rpId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysRolePermissionTableList;
