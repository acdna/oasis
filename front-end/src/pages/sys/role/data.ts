/**
 * SYS_ROLE-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleItem {
    roleId: string;
    roleName: string;
    roleNameEn: string;
    roleState: string;
    roleSys: string;
}
/**
 * SYS_ROLE-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRolePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_ROLE-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleTableData {
    list: SysRoleItem[];
    pagination: Partial < SysRolePagination > ;
}
/**
 * SYS_ROLE-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleParams extends SysRoleItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}