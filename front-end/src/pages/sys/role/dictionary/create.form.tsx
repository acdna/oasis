import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysRoleDictionaryItem } from "@/pages/sys/role/dictionary/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_ROLE_DICTIONARY-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRoleDictionaryCreateFormProps {
    onSubmit: (fields: SysRoleDictionaryItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_ROLE_DICTIONARY-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleDictionaryCreateForm: React.FC<SysRoleDictionaryCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:28 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { rdId,rdRoleId,rdDictionaryId }=fieldValues;
        onSubmit({ rdId,rdRoleId,rdDictionaryId });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.role.dictionary.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>
                
                <Form.Item
                        name="rdId"
                        label={<FormattedMessage id="pages.create.sys.role.dictionary.rdId.label" defaultMessage="角色字典ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.dictionary.rdId.tip" defaultMessage="角色字典ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.dictionary.rdId.placeholder",defaultMessage:"角色字典ID"})}/>
                </Form.Item>
                
                <Form.Item
                        name="rdRoleId"
                        label={<FormattedMessage id="pages.create.sys.role.dictionary.rdRoleId.label" defaultMessage="角色ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.dictionary.rdRoleId.tip" defaultMessage="角色ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.dictionary.rdRoleId.placeholder",defaultMessage:"角色ID"})}/>
                </Form.Item>
                
                <Form.Item
                        name="rdDictionaryId"
                        label={<FormattedMessage id="pages.create.sys.role.dictionary.rdDictionaryId.label" defaultMessage="字典ID"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.role.dictionary.rdDictionaryId.tip" defaultMessage="字典ID为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.role.dictionary.rdDictionaryId.placeholder",defaultMessage:"字典ID"})}/>
                </Form.Item>
                
            </ProForm>
        </Modal>
    );
};

export default SysRoleDictionaryCreateForm;
