import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysRoleDictionaryItem } from "@/pages/sys/role/dictionary/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_ROLE_DICTIONARY-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysRoleDictionaryUpdateFormProps {
    onSubmit: (fields: SysRoleDictionaryItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_ROLE_DICTIONARY-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleDictionaryUpdateForm: React.FC<SysRoleDictionaryUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        rdId: values.rdId,
        rdRoleId: values.rdRoleId,
        rdDictionaryId: values.rdDictionaryId,
        
    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:28 PM
     **/
    const renderContent = () => (
        <>
            <Input name='rdId' value={formVals.rdId} hidden={true} readOnly={true} disabled={true}/>
            
            <Form.Item
                name="rdId"
                label={<FormattedMessage id='pages.update.sys.role.dictionary.rdId.label' defaultMessage='角色字典ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.dictionary.rdId.tip' defaultMessage='请输入角色字典ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.dictionary.update.rdId.tip',
                    defaultMessage: '请输入角色字典ID！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="rdRoleId"
                label={<FormattedMessage id='pages.update.sys.role.dictionary.rdRoleId.label' defaultMessage='角色ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.dictionary.rdRoleId.tip' defaultMessage='请输入角色ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.dictionary.update.rdRoleId.tip',
                    defaultMessage: '请输入角色ID！'
                })}/>
            </Form.Item>
            
            <Form.Item
                name="rdDictionaryId"
                label={<FormattedMessage id='pages.update.sys.role.dictionary.rdDictionaryId.label' defaultMessage='字典ID'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.role.dictionary.rdDictionaryId.tip' defaultMessage='请输入字典ID！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.role.dictionary.update.rdDictionaryId.tip',
                    defaultMessage: '请输入字典ID！'
                })}/>
            </Form.Item>
            
        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.role.dictionary.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     rdId: formVals.rdId,
                     rdRoleId: formVals.rdRoleId,
                     rdDictionaryId: formVals.rdDictionaryId,
                    
                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysRoleDictionaryUpdateForm;