/**
 * SYS_ROLE_DICTIONARY-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleDictionaryItem {
  rdId: string;
  rdRoleId: string;
  rdDictionaryId: string;
}

/**
 * SYS_ROLE_DICTIONARY-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleDictionaryPagination {
  total: number;
  pageSize: number;
  current: number;
  pageNum: number;
}

/**
 * SYS_ROLE_DICTIONARY-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleDictionaryTableData {
  list: SysRoleDictionaryItem[];
  pagination: Partial<SysRoleDictionaryPagination>;
}

/**
 * SYS_ROLE_DICTIONARY-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysRoleDictionaryParams extends SysRoleDictionaryItem {
  pageSize?: number;
  current: number;
  pageNum: number;
  filter?: {
    [key: string]: any[]
  };
  sorter?: {
    [key: string]: any
  };

  rdRoleIdList?: string[];
}
