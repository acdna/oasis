import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage,useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysRoleDictionaryService } from '@/services/sys/sys.role.dictionary.service';
import { SysRoleDictionaryItem } from '@/pages/sys/role/dictionary/data';
import { SysRoleDictionaryHandles } from '@/pages/sys/role/dictionary/handles';
import SysRoleDictionaryCreateForm from '@/pages/sys/role/dictionary/create.form';
import SysRoleDictionaryUpdateForm from '@/pages/sys/role/dictionary/update.form';


/**
 * SYS_ROLE_DICTIONARY-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysRoleDictionaryTableList: React.FC<{}> = () => {
    const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
    const [formValues, setFormValues] = useState({});
    const actionRef = useRef<ActionType>();
    const [row, setRow] = useState<SysRoleDictionaryItem>();
    const [selectedRowsState, setSelectedRows] = useState<SysRoleDictionaryItem[]>([]);
    const handles = new SysRoleDictionaryHandles();
    const intl=useIntl();

    //定义列信息
    const columns: ProColumns<SysRoleDictionaryItem>[] = [
        {
            title: <FormattedMessage id="pages.list.sys.role.dictionary.index.label" defaultMessage="序号" />,
            dataIndex: 'index',
            valueType: 'indexBorder',
            fixed:true,
            width:50,
        },
        {
            dataIndex: 'rdId',
            sorter: false,
            valueType: 'text',
            search: false,
            hideInForm: true,
            hideInTable: true,
            hideInDescriptions: true,
        },

        {
            title: <FormattedMessage id="pages.list.sys.role.dictionary.rdId.label" defaultMessage="角色字典ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.dictionary.rdId.tip',
                defaultMessage: '角色字典ID查询',
            })),
            dataIndex: 'rdId',
            valueType:'text',
             sorter: (source, target) => (source.rdId||'').localeCompare(target.rdId||''),
            fixed: 'left',

            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.dictionary.rdId.tip" defaultMessage="角色字典ID" />
                        ),
                    },
                ],
            },

            render: (dom, entity) => {
                return <a onClick={() => setRow(entity)}>{dom}</a>;
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.dictionary.rdRoleId.label" defaultMessage="角色ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.dictionary.rdRoleId.tip',
                defaultMessage: '角色ID查询',
            })),
            dataIndex: 'rdRoleId',
            valueType:'text',
             sorter: (source, target) => (source.rdRoleId||'').localeCompare(target.rdRoleId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.dictionary.rdRoleId.tip" defaultMessage="角色ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.dictionary.rdDictionaryId.label" defaultMessage="字典ID" />,
            tooltip: (intl.formatMessage({
                id: 'pages.list.sys.role.dictionary.rdDictionaryId.tip',
                defaultMessage: '字典ID查询',
            })),
            dataIndex: 'rdDictionaryId',
            valueType:'text',
             sorter: (source, target) => (source.rdDictionaryId||'').localeCompare(target.rdDictionaryId||''),


            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="pages.list.sys.role.dictionary.rdDictionaryId.tip" defaultMessage="字典ID" />
                        ),
                    },
                ],
            },

        },

        {
            title: <FormattedMessage id="pages.list.sys.role.dictionary.option.title" defaultMessage="操作" />,
            dataIndex: 'option',
            valueType: 'option',
            fixed: 'right',
            render: (_, record) => (
                <>
                    <a
                        onClick={() => {
                            handleUpdateModalVisible(true);
                            setFormValues(record);
                          }
                        }
                    >
                        <FormattedMessage id="pages.list.sys.role.dictionary.option.edit" defaultMessage="修改" />
                    </a>
                    <Divider type="vertical" />
                    <Popconfirm title={<FormattedMessage id='pages.list.sys.role.dictionary.option.delete.confirm'
                                                         defaultMessage='删除不可恢复，你确定要删除吗?' />}
                                onConfirm={async () => {
                                    const success = await handles.deleteById(record);
                                    if (success && actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                  }
                                }>
                        <a>
                            <FormattedMessage id="pages.list.sys.role.dictionary.option.delete" defaultMessage="删除" />
                        </a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    return (
        <PageContainer>
            <ProTable<SysRoleDictionaryItem>
                headerTitle={<FormattedMessage id='pages.list.sys.role.dictionary.title' defaultMessage='查询' />}
                actionRef={actionRef}
                rowKey="rdId"
                search={
                    {labelWidth: 120,}
                }
                toolBarRender={() => [
                    <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
                        <PlusOutlined /> <FormattedMessage id="pages.list.sys.role.dictionary.new" defaultMessage="新建" />
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    const data = await SysRoleDictionaryService.findPage({ ...params, sorter, filter });
                    return {
                        data: data.data,
                        success: true,
                        total: data.page.count,
                        current: data.page.offset,
                        pageSize: data.page.pageSize,
                    };
                  }
                }
                columns={columns}
                pagination={
                  {
                    defaultCurrent: 1,
                    defaultPageSize: 20,
                    showSizeChanger: true,
                    showQuickJumper: true,
                  }
                }
                rowSelection={
                  {
                    onChange: (_, selectedRows) => setSelectedRows(selectedRows),
                  }
                }

            />

            {selectedRowsState?.length > 0 && (
                <FooterToolbar
                    extra={
                        <div>
                            <FormattedMessage id="pages.list.sys.role.dictionary.chosen" defaultMessage="已选择" />{' '}
                            <a style={  {fontWeight: 600} }>{selectedRowsState.length}</a>{' '}
                            <FormattedMessage id="pages.list.sys.role.dictionary.item" defaultMessage="项" />
                        </div>
                    }
                >
                    <Button danger
                            onClick={async () => {
                                await handles.deleteByIds(selectedRowsState);
                                setSelectedRows([]);
                                actionRef.current?.reloadAndRest?.();
                              }
                            }
                    >
                        <DeleteOutlined /> <FormattedMessage id="pages.list.sys.role.dictionary.batch.delete" defaultMessage="批量删除" />
                    </Button>
                </FooterToolbar>
            )}

            <SysRoleDictionaryCreateForm onCancel={() => handleCreateModalVisible(false)}
                                     onSubmit={async (value) => {
                                         const success = await handles.add(value);
                                         if (success) {
                                             handleCreateModalVisible(false);
                                             if (actionRef.current) {
                                                 actionRef.current.reload();
                                             }
                                         }
                                       }
                                     }
                                     modalVisible={createModalVisible}>
            </SysRoleDictionaryCreateForm>

            {formValues && Object.keys(formValues).length>0 ? (
                <SysRoleDictionaryUpdateForm
                    onSubmit={async (value: any) => {
                        const success = await handles.update(value);
                        if (success) {
                            handleUpdateModalVisible(false);
                            setFormValues({});
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                      }
                    }
                    onCancel={() => {
                        handleUpdateModalVisible(false);
                        setFormValues({});
                      }
                    }
                    updateModalVisible={updateModalVisible}
                    values={formValues}
                />
            ) : null}


            <Drawer
                width={600}
                visible={!!row}
                onClose={() => {
                    setRow(undefined);
                  }
                }
                closable={false}
            >
                {row?.rdId && (
                    <ProDescriptions<SysRoleDictionaryItem>
                        column={2}
                        title={row?.rdId}
                        request={async () => ({
                            data: row || {},
                        })}
                        params={
                          {
                            rdId: row?.rdId,
                          }
                        }
                        columns={columns}
                    />
                )}
            </Drawer>
        </PageContainer>
    );
};


export default SysRoleDictionaryTableList;
