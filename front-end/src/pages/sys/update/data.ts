/**
 * SYS_UPDATE-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUpdateItem {
    updateId: string;
    updateVersion: string;
    updateContent: string;
    updateManager: string;
    updateCreateTime: Date;
}
/**
 * SYS_UPDATE-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUpdatePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_UPDATE-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUpdateTableData {
    list: SysUpdateItem[];
    pagination: Partial < SysUpdatePagination > ;
}
/**
 * SYS_UPDATE-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 */
export interface SysUpdateParams extends SysUpdateItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}