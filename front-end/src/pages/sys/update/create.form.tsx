import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysUpdateItem } from '@/pages/sys/update/data';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * SYS_UPDATE-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysUpdateCreateFormProps {
  onSubmit: (fields: SysUpdateItem) => void,
  onCancel: () => void;
  modalVisible: boolean;
}

/**
 * SYS_UPDATE-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUpdateCreateForm: React.FC<SysUpdateCreateFormProps> = (props) => {
  const { modalVisible, onCancel, onSubmit } = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const { updateId, updateVersion, updateContent, updateCreateTime } = fieldValues;
    onSubmit({ updateId, updateVersion, updateContent, updateManager: AuthContext.loginName, updateCreateTime });
  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.create.sys.update.form.title' defaultMessage='新建' />
      }
      visible={modalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false}>

        <Form.Item
          name="updateVersion"
          label={<FormattedMessage id="pages.create.sys.update.updateVersion.label" defaultMessage="升级版本号" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.update.updateVersion.tip" defaultMessage="升级版本号为必填项" />),
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.update.updateVersion.placeholder',
            defaultMessage: '升级版本号',
          })} />
        </Form.Item>

        <Form.Item
          name="updateContent"
          label={<FormattedMessage id="pages.create.sys.update.updateContent.label" defaultMessage="升级内容" />}
          rules={[{
            required: true,
            message: (<FormattedMessage id="pages.create.sys.update.updateContent.tip" defaultMessage="升级内容为必填项" />),
          }]}
        >
          <TextArea placeholder={intl.formatMessage({
            id: 'pages.create.sys.update.updateContent.placeholder',
            defaultMessage: '升级内容',
          })} />
        </Form.Item>

      </ProForm>
    </Modal>
  );
};

export default SysUpdateCreateForm;
