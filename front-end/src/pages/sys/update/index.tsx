import {DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Divider, Drawer, Popconfirm} from 'antd';
import React, {useRef, useState} from 'react';
import {connect, FormattedMessage, useIntl} from 'umi';
import {FooterToolbar, PageContainer} from '@ant-design/pro-layout';
import ProTable, {ActionType, ProColumns} from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import {SysUpdateService} from '@/services/sys/sys.update.service';
import {SysUpdateItem} from '@/pages/sys/update/data';
import {SysUpdateHandles} from '@/pages/sys/update/handles';
import SysUpdateCreateForm from '@/pages/sys/update/create.form';
import SysUpdateUpdateForm from '@/pages/sys/update/update.form';
import {MLOCALES} from '@/common/locales/locale';
import * as moment from 'moment';
import UserMapper from '@/common/users/user.mapper';
import {Dispatch} from '@@/plugin-dva/connect';
import {mapValueEnum} from '@/components/common/select.options';
import {ConnectState} from '@/models/connect';
import {SYS_USER_EFFECTS} from "@/pages/sys/user/effects";

interface SysUpdateTableListProps {
  userMap: UserMapper;
  dispatch: Dispatch;
}

/**
 * SYS_UPDATE-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUpdateTableList: React.FC<SysUpdateTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysUpdateItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysUpdateItem[]>([]);
  const handles = new SysUpdateHandles();
  const intl = useIntl();
  const {userMap, dispatch} = props;

  SYS_USER_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysUpdateItem>[] = [

    {
      dataIndex: 'updateId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.update.updateVersion.label" defaultMessage="升级版本号"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.update.updateVersion.tip',
        defaultMessage: '升级版本号查询',
      })),
      dataIndex: 'updateVersion',
      valueType: 'text',
      sorter: (source, target) => (source.updateVersion || '').localeCompare(target.updateVersion || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.update.updateVersion.tip" defaultMessage="升级版本号"/>
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.update.updateContent.label" defaultMessage="升级内容"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.update.updateContent.tip',
        defaultMessage: '升级内容查询',
      })),
      dataIndex: 'updateContent',
      valueType: 'text',
      sorter: (source, target) => (source.updateContent || '').localeCompare(target.updateContent || ''),

      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.update.updateContent.tip" defaultMessage="升级内容"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.update.updateManager.label" defaultMessage="负责人"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.update.updateManager.tip',
        defaultMessage: '升级负责人查询',
      })),
      dataIndex: 'updateManager',
      valueType: 'text',
      renderText: (text) => userMap.getUserName(text),
      valueEnum: mapValueEnum({
        map: userMap.getAllUsers(),
        cnTextName: 'userName',
        enTextName: 'userNameEn',
        valueName: 'userLoginName',
      }),
      sorter: (source, target) => (source.updateManager || '').localeCompare(target.updateManager || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.update.updateManager.tip" defaultMessage="升级负责人"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.update.updateCreateTime.label" defaultMessage="创建日期"/>,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.update.updateCreateTime.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'updateCreateTime',
      valueType: 'date',
      search: false,
      hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: false,
            message: (
              <FormattedMessage id="pages.list.sys.update.updateCreateTime.tip" defaultMessage="创建日期"/>
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.update.option.title" defaultMessage="操作"/>,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.update.option.edit" defaultMessage="修改"/>
          </a>
          <Divider type="vertical"/>
          <Popconfirm title={<FormattedMessage id='pages.list.sys.update.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?'/>}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.update.option.delete" defaultMessage="删除"/>
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysUpdateItem>
        headerTitle={<FormattedMessage id='pages.list.sys.update.title' defaultMessage='查询'/>}
        actionRef={actionRef}
        rowKey="updateId"
        search={
          {labelWidth: 120}
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined/> <FormattedMessage id="pages.list.sys.update.new" defaultMessage="新建"/>
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysUpdateService.findPage({...params, sorter, filter});
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.update.chosen" defaultMessage="已选择"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.update.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined/> <FormattedMessage id="pages.list.sys.update.batch.delete" defaultMessage="批量删除"/>
          </Button>
        </FooterToolbar>
      )}

      <SysUpdateCreateForm onCancel={() => handleCreateModalVisible(false)}
                           onSubmit={async (value) => {
                             const success = await handles.add(value);
                             if (success) {
                               handleCreateModalVisible(false);
                               if (actionRef.current) {
                                 actionRef.current.reload();
                               }
                             }
                           }
                           }
                           modalVisible={createModalVisible}>
      </SysUpdateCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysUpdateUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.updateId && (
          <ProDescriptions<SysUpdateItem>
            column={2}
            title={row?.updateVersion}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                updateId: row?.updateId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({sys_user}: ConnectState) => {
  return {userMap: sys_user.userMap};
};

export default connect(mapStateToProps)(SysUpdateTableList);
