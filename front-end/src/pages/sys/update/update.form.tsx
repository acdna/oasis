import React, { useState } from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysUpdateItem } from '@/pages/sys/update/data';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * SYS_UPDATE-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
interface SysUpdateUpdateFormProps {
  onSubmit: (fields: SysUpdateItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
}

/**
 * SYS_UPDATE-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const SysUpdateUpdateForm: React.FC<SysUpdateUpdateFormProps> = (props) => {
  const { onSubmit, onCancel, updateModalVisible, values } = props;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    updateId: values.updateId,
    updateVersion: values.updateVersion,
    updateContent: values.updateContent,
    updateManager: values.updateManager,
    updateCreateTime: values.updateCreateTime,

  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues, updateManager: AuthContext.loginName };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const renderContent = () => (
    <>

      <Form.Item
        name="updateVersion"
        label={<FormattedMessage id='pages.update.sys.update.updateVersion.label' defaultMessage='升级版本号' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.update.updateVersion.tip' defaultMessage='请输入升级版本号！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.update.update.updateVersion.tip',
          defaultMessage: '请输入升级版本号！',
        })} />
      </Form.Item>

      <Form.Item
        name="updateContent"
        label={<FormattedMessage id='pages.update.sys.update.updateContent.label' defaultMessage='升级内容' />}
        rules={[
          {
            required: true,
            message: (<FormattedMessage id='pages.update.sys.update.updateContent.tip' defaultMessage='请输入升级内容！' />),
          },
        ]}
      >
        <TextArea placeholder={intl.formatMessage({
          id: 'pages.sys.update.update.updateContent.tip',
          defaultMessage: '请输入升级内容！',
        })} />
      </Form.Item>

    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{ padding: '32px 40px 48px' }}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.update.form.title' defaultMessage='修改' />}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            updateId: formVals.updateId,
            updateVersion: formVals.updateVersion,
            updateContent: formVals.updateContent,
            updateManager: formVals.updateManager,
            updateCreateTime: formVals.updateCreateTime,
          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysUpdateUpdateForm;
