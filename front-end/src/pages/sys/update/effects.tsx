import { useEffect } from 'react';
import { Dispatch } from 'umi';
/**
 * 剥离出SYS_UPDATE表相关的数据加载等逻辑，包装useEffect为方法，主要是为了提高复用性
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export const SYS_UPDATE_EFFECTS = {
    /**
     * 加载SYS_UPDATE-表的所有数据信息
     * @author jiangbin
     * @date 1/11/2021, 1:33:33 PM
     **/
    LOAD_ALL: (dispatch: Dispatch, deps ? : any[]) => {
        useEffect(() => {
            dispatch({
                type: 'sys_update/fetchAll',
            });
        }, deps || []);
    },
};