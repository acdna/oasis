import React, {useState} from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysLogItem } from "@/pages/sys/log/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_LOG-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysLogUpdateFormProps {
    onSubmit: (fields: SysLogItem) => void,
    updateModalVisible: boolean;
    onCancel: () => void;
    values: any;
}

/**
 * SYS_LOG-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysLogUpdateForm: React.FC<SysLogUpdateFormProps> = (props) => {
    const {onSubmit, onCancel, updateModalVisible, values,} = props;

    const intl = useIntl();

    //表单的初始数据信息
    const [formVals] = useState({
        logId: values.logId,
        logUser: values.logUser,
        logTime: values.logTime,
        logIp: values.logIp,
        logUrl: values.logUrl,
        logTitle: values.logTitle,
        logContent: values.logContent,
        logType: values.logType,

    });

    //表单信息对象
    const [form] = Form.useForm();

    /**
     * 点击更新按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const values={...formVals,...fieldValues};//合并数据
        onSubmit({...values});
    };

    /**
     * 点击取消按钮的操作
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    /**
     * 渲染表单内容
     * @author jiangbin
     * @date 12/30/2020, 5:14:27 PM
     **/
    const renderContent = () => (
        <>

            <Form.Item
                name="logUser"
                label={<FormattedMessage id='pages.update.sys.log.logUser.label' defaultMessage='操作人员名称'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logUser.tip' defaultMessage='请输入操作人员名称！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logUser.tip',
                    defaultMessage: '请输入操作人员名称！'
                })}/>
            </Form.Item>

            <Form.Item
                name="logTime"
                label={<FormattedMessage id='pages.update.sys.log.logTime.label' defaultMessage='操作时间'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logTime.tip' defaultMessage='请输入操作时间！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logTime.tip',
                    defaultMessage: '请输入操作时间！'
                })}/>
            </Form.Item>

            <Form.Item
                name="logIp"
                label={<FormattedMessage id='pages.update.sys.log.logIp.label' defaultMessage='IP地址'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logIp.tip' defaultMessage='请输入IP地址！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logIp.tip',
                    defaultMessage: '请输入IP地址！'
                })}/>
            </Form.Item>

            <Form.Item
                name="logUrl"
                label={<FormattedMessage id='pages.update.sys.log.logUrl.label' defaultMessage='操作URL'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logUrl.tip' defaultMessage='请输入操作URL！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logUrl.tip',
                    defaultMessage: '请输入操作URL！'
                })}/>
            </Form.Item>

            <Form.Item
                name="logTitle"
                label={<FormattedMessage id='pages.update.sys.log.logTitle.label' defaultMessage='模块'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logTitle.tip' defaultMessage='请输入模块！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logTitle.tip',
                    defaultMessage: '请输入模块！'
                })}/>
            </Form.Item>

            <Form.Item
                name="logContent"
                label={<FormattedMessage id='pages.update.sys.log.logContent.label' defaultMessage='内容'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logContent.tip' defaultMessage='请输入内容！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logContent.tip',
                    defaultMessage: '请输入内容！'
                })}/>
            </Form.Item>

            <Form.Item
                name="logType"
                label={<FormattedMessage id='pages.update.sys.log.logType.label' defaultMessage='操作类型'/>}
                rules={[
                    {
                        required: true,
                        message: (<FormattedMessage id='pages.update.sys.log.logType.tip' defaultMessage='请输入操作类型！'/>),
                    },
                ]}
            >
                <Input placeholder={intl.formatMessage({
                    id: 'pages.sys.log.update.logType.tip',
                    defaultMessage: '请输入操作类型！'
                })}/>
            </Form.Item>

        </>
    );

    return (
        <Modal
            width={640}
            bodyStyle={  {padding: '32px 40px 48px'}  }
            destroyOnClose
            title={<FormattedMessage id='pages.update.sys.log.ruleConfig' defaultMessage='修改'/>}
            visible={updateModalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm
                submitter={false}
                form={form}
                initialValues={
                  {
                     logId: formVals.logId,
                     logUser: formVals.logUser,
                     logTime: formVals.logTime,
                     logIp: formVals.logIp,
                     logUrl: formVals.logUrl,
                     logTitle: formVals.logTitle,
                     logContent: formVals.logContent,
                     logType: formVals.logType,

                  }
                }
            >
                {renderContent()}
            </ProForm>

        </Modal>
    );
};

export default SysLogUpdateForm;
