import { DeleteOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysLogService } from '@/services/sys/sys.log.service';
import { SysLogItem } from '@/pages/sys/log/data';
import { SysLogHandles } from '@/pages/sys/log/handles';
import SysLogCreateForm from '@/pages/sys/log/create.form';
import SysLogUpdateForm from '@/pages/sys/log/update.form';
import { ConnectState } from '@/models/connect';
import UserMapper from '@/common/users/user.mapper';
import { Dispatch } from '@@/plugin-dva/connect';
import { dictValueEnum, mapValueEnum } from '@/components/common/select.options';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SYS_LOG_TYPE_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';
import * as moment from 'moment';
import { MLOCALES } from '@/common/locales/locale';
import { SYS_USER_EFFECTS } from '@/pages/sys/user/effects';
import {SYS_DICTIONARY_EFFECTS} from "@/pages/sys/dictionary/effects";

interface SysLogTableListProps {
  userMap: UserMapper;
  dictMap: DictMapper;
  dispatch: Dispatch;
}

/**
 * SYS_LOG-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysLogTableList: React.FC<SysLogTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysLogItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysLogItem[]>([]);
  const handles = new SysLogHandles();
  const intl = useIntl();
  const { userMap, dictMap, dispatch } = props;

  SYS_USER_EFFECTS.LOAD_ALL(dispatch);
  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysLogItem>[] = [
    {
      dataIndex: 'logId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.log.logIp.label" defaultMessage="IP" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logIp.tip',
        defaultMessage: 'IP地址查询',
      })),
      dataIndex: 'logIp',
      valueType: 'text',
      fixed: 'left',
      width: 220,
      sorter: (source, target) => (source.logIp || '').localeCompare(target.logIp || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.log.logIp.tip" defaultMessage="IP" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.log.logUrl.label" defaultMessage="URL" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logUrl.tip',
        defaultMessage: '操作URL查询',
      })),
      dataIndex: 'logUrl',
      valueType: 'text',
      width: 300,
      sorter: (source, target) => (source.logUrl || '').localeCompare(target.logUrl || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.log.logUrl.tip" defaultMessage="URL" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.log.logTitle.label" defaultMessage="操作结果" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logTitle.tip',
        defaultMessage: '操作结果查询',
      })),
      dataIndex: 'logTitle',
      valueType: 'text',
      width: 250,
      sorter: (source, target) => (source.logTitle || '').localeCompare(target.logTitle || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.log.logTitle.tip" defaultMessage="操作结果" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.log.logContent.label" defaultMessage="内容" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logContent.tip',
        defaultMessage: '内容查询',
      })),
      dataIndex: 'logContent',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.log.logType.label" defaultMessage="操作类型" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logType.tip',
        defaultMessage: '操作类型查询',
      })),
      dataIndex: 'logType',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      width: 220,
      renderText: (text) => dictMap.getItemName(SYS_LOG_TYPE_DICT_GROUP.group, text) || '',
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_LOG_TYPE_DICT_GROUP.group))),
      sorter: (source, target) => ((source.logType) - (target.logType)),
    },
    {
      title: <FormattedMessage id="pages.list.sys.log.logUser.label" defaultMessage="操作人" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logUser.tip',
        defaultMessage: '操作人查询',
      })),
      dataIndex: 'logUser',
      valueType: 'text',
      width: 200,
      renderText: (text) => userMap.getUserName(text),
      valueEnum: mapValueEnum({
        map: userMap.getAllUsers(),
        cnTextName: 'userName',
        enTextName: 'userNameEn',
        valueName: 'userLoginName',
      }),
      sorter: (source, target) => (source.logUser || '').localeCompare(target.logUser || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.log.logUser.tip" defaultMessage="操作人" />
            ),
          },
        ],
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.log.logTime.label" defaultMessage="时间" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.log.logTime.tip',
        defaultMessage: '操作时间查询',
      })),
      dataIndex: 'logTime',
      valueType: 'date',
      search: false,
      hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      width: 250,
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.log.logTime.tip" defaultMessage="操作时间" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.log.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      width: 160,
      render: (_, record) => (
        <>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.log.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.log.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysLogItem>
        headerTitle={<FormattedMessage id='pages.list.sys.log.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="logId"
        scroll={{ x: 1300 }}
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => []}
        request={async (params, sorter, filter) => {
          const data = await SysLogService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.log.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.log.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.log.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysLogCreateForm onCancel={() => handleCreateModalVisible(false)}
                        onSubmit={async (value) => {
                          const success = await handles.add(value);
                          if (success) {
                            handleCreateModalVisible(false);
                            if (actionRef.current) {
                              actionRef.current.reload();
                            }
                          }
                        }
                        }
                        modalVisible={createModalVisible}>
      </SysLogCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysLogUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.logId && (
          <ProDescriptions<SysLogItem>
            column={2}
            title={row?.logTitle}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                logId: row?.logId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_user, sys_dictionary }: ConnectState) => {
  return { userMap: sys_user.userMap, dictMap: sys_dictionary.dictMap };
};

export default connect(mapStateToProps)(SysLogTableList);
