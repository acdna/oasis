import React from 'react';
import { Form, Input, Modal } from 'antd';
import { FormattedMessage,useIntl } from 'umi';
import { SysLogItem } from "@/pages/sys/log/data";
import ProForm from '@ant-design/pro-form';

/**
 * SYS_LOG-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysLogCreateFormProps {
    onSubmit: (fields: SysLogItem) =>void,
    onCancel: () => void;
    modalVisible: boolean;
}

/**
 * SYS_LOG-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysLogCreateForm: React.FC<SysLogCreateFormProps> = (props) => {
    const {modalVisible, onCancel, onSubmit} = props;
    const [form] = Form.useForm();
    const intl=useIntl();

    /**
    * 点击确定按钮的操作方法
    * @author jiangbin
    * @date 12/30/2020, 5:14:27 PM
    **/
    const handleOk = async () => {
        const fieldValues = await form.validateFields();
        const { logId,logUser,logTime,logIp,logUrl,logTitle,logContent,logType }=fieldValues;
        onSubmit({ logId,logUser,logTime,logIp,logUrl,logTitle,logContent,logType });
    };

    /**
    * 点击取消按钮的操作方法
    * @author jiang
    * @date 2020-12-19 14:05:15
    **/
    const handleCancel = async () => {
        await form.resetFields();
        onCancel();
    };

    return (
        <Modal
            destroyOnClose
            title={
                <FormattedMessage id='pages.create.sys.log.form.title' defaultMessage='新建'/>
            }
            visible={modalVisible}
            onCancel={() => handleCancel()}
            onOk={() => handleOk()}
        >
            <ProForm form={form} submitter={false}>

                <Form.Item
                        name="logUser"
                        label={<FormattedMessage id="pages.create.sys.log.logUser.label" defaultMessage="操作人员名称"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logUser.tip" defaultMessage="操作人员名称为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logUser.placeholder",defaultMessage:"操作人员名称"})}/>
                </Form.Item>

                <Form.Item
                        name="logTime"
                        label={<FormattedMessage id="pages.create.sys.log.logTime.label" defaultMessage="操作时间"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logTime.tip" defaultMessage="操作时间为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logTime.placeholder",defaultMessage:"操作时间"})}/>
                </Form.Item>

                <Form.Item
                        name="logIp"
                        label={<FormattedMessage id="pages.create.sys.log.logIp.label" defaultMessage="IP地址"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logIp.tip" defaultMessage="IP地址为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logIp.placeholder",defaultMessage:"IP地址"})}/>
                </Form.Item>

                <Form.Item
                        name="logUrl"
                        label={<FormattedMessage id="pages.create.sys.log.logUrl.label" defaultMessage="操作URL"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logUrl.tip" defaultMessage="操作URL为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logUrl.placeholder",defaultMessage:"操作URL"})}/>
                </Form.Item>

                <Form.Item
                        name="logTitle"
                        label={<FormattedMessage id="pages.create.sys.log.logTitle.label" defaultMessage="模块"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logTitle.tip" defaultMessage="模块为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logTitle.placeholder",defaultMessage:"模块"})}/>
                </Form.Item>

                <Form.Item
                        name="logContent"
                        label={<FormattedMessage id="pages.create.sys.log.logContent.label" defaultMessage="内容"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logContent.tip" defaultMessage="内容为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logContent.placeholder",defaultMessage:"内容"})}/>
                </Form.Item>

                <Form.Item
                        name="logType"
                        label={<FormattedMessage id="pages.create.sys.log.logType.label" defaultMessage="操作类型"/>}
                        rules={[{
                            required: true,
                            message: (<FormattedMessage id="pages.create.sys.log.logType.tip" defaultMessage="操作类型为必填项"/>),
                        },]}
                >
                    <Input placeholder={intl.formatMessage({id:"pages.create.sys.log.logType.placeholder",defaultMessage:"操作类型"})}/>
                </Form.Item>

            </ProForm>
        </Modal>
    );
};

export default SysLogCreateForm;
