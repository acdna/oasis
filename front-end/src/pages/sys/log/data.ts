/**
 * SYS_LOG-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysLogItem {
    logId: string;
    logUser: string;
    logTime: Date;
    logIp: string;
    logUrl: string;
    logTitle: string;
    logContent: string;
    logType: number;
}
/**
 * SYS_LOG-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysLogPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_LOG-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysLogTableData {
    list: SysLogItem[];
    pagination: Partial < SysLogPagination > ;
}
/**
 * SYS_LOG-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysLogParams extends SysLogItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}