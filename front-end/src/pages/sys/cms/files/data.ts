/**
 * SYS_CMS_FILES-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsFilesItem {
    scFileId: string;
    scFileName: string;
    scFileNameCn: string;
    scFileType: string;
    scFilePath: string;
    scFileIsNew: string;
    scFileManager: string;
    scFileCreateDate: Date;
    scFielUpdateDate: Date;
    scFileSpecies: string;
}
/**
 * SYS_CMS_FILES-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsFilesPagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_CMS_FILES-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsFilesTableData {
    list: SysCmsFilesItem[];
    pagination: Partial < SysCmsFilesPagination > ;
}
/**
 * SYS_CMS_FILES-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsFilesParams extends SysCmsFilesItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}