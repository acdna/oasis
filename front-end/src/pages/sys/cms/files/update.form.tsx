import { SysCmsFilesItem } from '@/pages/sys/cms/files/data';
import React, { useState } from 'react';
import { useIntl } from 'umi';
import { Button, Col, Drawer, Form, Input, Row, Select } from 'antd';
import ProForm from '@ant-design/pro-form';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options } from '@/components/common/select.options';
import FileUpload from '@/pages/common/file/file.upload';
import {
  SYS_FILE_TYPE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
  SYS_SWITCH_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * SYS_CMS_FILES-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
interface SysCmsFilesUpdateFormProps {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (fields: SysCmsFilesItem) => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_CMS_FILES-更新字典项信息表单
 * @author jiangbin
 * @date 1/6/2021, 2:23:40 PM
 **/
const SysCmsFilesUpdateForm: React.FC<SysCmsFilesUpdateFormProps> = (props) => {
  const { values, visible, onCancel, onSubmit, dictMap } = props;
  const intl = useIntl();
  const [record, setRecord] = useState(values);
  const [filePath, setFilePath] = useState(record.scFilePath);
  const [fileName, setFileName] = useState(record.scFileName);
  const [form] = Form.useForm();

  //关联设置文件名和文件路径
  form.setFieldsValue({
    scFileId: record.scFileId,
    scFileName: fileName,
    scFileNameCn: record.scFileNameCn,
    scFileType: record.scFileType,
    scFilePath: filePath,
    scFileIsNew: record.scFileIsNew,
    scFileManager: record.scFileManager,
    scFileCreateDate: record.scFileCreateDate,
    scFielUpdateDate: record.scFielUpdateDate,
    scFileSpecies: record.scFileSpecies ? record.scFileSpecies.split(',') : undefined,
  });

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...record, ...fieldValues, scFileManager: AuthContext.loginName };//合并数据
    setRecord(values);
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:28 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <>
      <Drawer
        getContainer={false}
        destroyOnClose
        title={intl.formatMessage({ id: 'pages.update.sys.cms.files.title', defaultMessage: '修改' })}
        width={720}
        onClose={onCancel}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        closable={true}
        footer={
          <div
            style={{ textAlign: 'right' }}
          >
            <Button onClick={() => handleCancel()} style={{ marginRight: 8 }}>
              {intl.formatMessage({ id: 'pages.update.sys.cms.files.cancel', defaultMessage: '取消' })}
            </Button>
            <Button onClick={() => handleOk()} type="primary">
              {intl.formatMessage({ id: 'pages.update.sys.cms.files.submit', defaultMessage: '提交' })}
            </Button>
          </div>
        }
      >
        <ProForm layout="vertical" submitter={false} form={form}>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="scFileName"
                label={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileName.label',
                  defaultMessage: '文件名称',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileName.tip',
                    defaultMessage: '文件名称是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileName.placeholder',
                  defaultMessage: '文件名称',
                })} onChange={(event) => {
                  setFileName(event.target.value || '');
                }} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="scFileNameCn"
                label={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileNameCn.label',
                  defaultMessage: '英文文件名',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileNameCn.tip',
                    defaultMessage: '英文文件名是必填项!',
                  })),
                }]}
              >
                <Input placeholder={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileNameCn.placeholder',
                  defaultMessage: '英文文件名',
                })} />
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>

            <Col span={12}>
              <Form.Item
                name="scFileType"
                label={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileType.label',
                  defaultMessage: '文件类型',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileType.tip',
                    defaultMessage: '文件类型是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileType.placeholder',
                    defaultMessage: '文件类型',
                  })}
                >
                  {options(SYS_FILE_TYPE_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="scFileIsNew"
                label={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileIsNew.label',
                  defaultMessage: '是否是最新',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileIsNew.tip',
                    defaultMessage: '是否是最新是必填项!',
                  })),
                }]}
              >
                <Select
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileIsNew.placeholder',
                    defaultMessage: '是否是最新',
                  })}
                >
                  {options(SYS_SWITCH_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="scFileSpecies"
                label={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFileSpecies.label',
                  defaultMessage: '种属',
                })}
              >
                <Select
                  allowClear={true}
                  mode={'multiple'}
                  style={{ width: '100%' }}
                  placeholder={intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFileSpecies.placeholder',
                    defaultMessage: '种属',
                  })}
                >
                  {options(SYS_SAM_SPECIES_DICT_GROUP.group, dictMap)}
                </Select>
              </Form.Item>
            </Col>

          </Row>

          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="scFilePath"
                label={intl.formatMessage({
                  id: 'pages.update.sys.cms.files.scFilePath.label',
                  defaultMessage: '文件路径',
                })}
                rules={[{
                  required: true,
                  message: (intl.formatMessage({
                    id: 'pages.update.sys.cms.files.scFilePath.tip',
                    defaultMessage: '文件路径是必填项!',
                  })),
                }]}
              >
                <FileUpload setFilePath={setFilePath} filePath={filePath} setFileName={setFileName}
                            fileName={fileName} />
              </Form.Item>
            </Col>
          </Row>

        </ProForm>
      </Drawer>
    </>
  );

};

export default SysCmsFilesUpdateForm;
