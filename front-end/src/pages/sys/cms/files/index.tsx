import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, Dispatch, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysCmsFilesService } from '@/services/sys/sys.cms.files.service';
import { SysCmsFilesItem } from '@/pages/sys/cms/files/data';
import { SysCmsFilesHandles } from '@/pages/sys/cms/files/handles';
import SysCmsFilesCreateForm from '@/pages/sys/cms/files/create.form';
import SysCmsFilesUpdateForm from '@/pages/sys/cms/files/update.form';
import * as moment from 'moment';
import { ConnectState } from '@/models/connect';
import DictMapper from '@/common/dictionary/dict.mapper';
import { dictValueEnum, mapValueEnum } from '@/components/common/select.options';
import UserMapper from '@/common/users/user.mapper';
import { SYS_USER_EFFECTS } from '@/pages/sys/user/effects';
import { SYS_DICTIONARY_EFFECTS } from '@/pages/sys/dictionary/effects';
import { FileHandles } from '@/pages/common/file/file.handles';
import { MLOCALES } from '@/common/locales/locale';
import {
  SYS_FILE_TYPE_DICT_GROUP,
  SYS_SAM_SPECIES_DICT_GROUP,
  SYS_SWITCH_DICT_GROUP,
} from '@/common/dictionary/sys.dict.defined';

interface SysCmsFilesTableListProps {
  dictMap: DictMapper;
  userMap: UserMapper;
  dispatch: Dispatch;
}

/**
 * SYS_CMS_FILES-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCmsFilesTableList: React.FC<SysCmsFilesTableListProps> = (props) => {
  const [createVisible, handleCreateVisible] = useState<boolean>(false);
  const [updateVisible, handleUpdateVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const { dictMap, userMap, dispatch } = props;
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysCmsFilesItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysCmsFilesItem[]>([]);
  const handles = new SysCmsFilesHandles();
  const fileHandles = new FileHandles();
  const intl = useIntl();

  SYS_USER_EFFECTS.LOAD_ALL(dispatch);
  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysCmsFilesItem>[] = [
    {
      dataIndex: 'scFileId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileName.label" defaultMessage="文件名称" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileName.tip',
        defaultMessage: '文件名称查询',
      })),
      dataIndex: 'scFileName',
      valueType: 'text',
      fixed: 'left',
      width: 180,
      sorter: (source, target) => (source.scFileName || '').localeCompare(target.scFileName || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.files.scFileName.tip" defaultMessage="文件名称" />
            ),
          },
        ],
      },
      render: (dom, entity) => {
        return <a onClick={() => setRow(entity)}>{dom}</a>;
      },
    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileNameCn.label" defaultMessage="scFileNameCn" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileNameCn.tip',
        defaultMessage: 'scFileNameCn查询',
      })),
      dataIndex: 'scFileNameCn',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileType.label" defaultMessage="文件类型" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileType.tip',
        defaultMessage: '文件类型查询',
      })),
      dataIndex: 'scFileType',
      valueType: 'text',
      width: 120,
      renderText: (text) => dictMap.getItemName(SYS_FILE_TYPE_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_FILE_TYPE_DICT_GROUP.group))),
      sorter: (source, target) => (source.scFileType || '').localeCompare(target.scFileType || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.files.scFileType.tip" defaultMessage="文件类型" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFilePath.label" defaultMessage="文件路径" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFilePath.tip',
        defaultMessage: '文件路径查询',
      })),
      dataIndex: 'scFilePath',
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileIsNew.label" defaultMessage="最新?" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileIsNew.tip',
        defaultMessage: '是否是最新查询',
      })),
      dataIndex: 'scFileIsNew',
      valueType: 'select',
      width: 60,
      renderText: (text) => dictMap.getItemName(SYS_SWITCH_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_SWITCH_DICT_GROUP.group))),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.files.scFileIsNew.tip" defaultMessage="是否是最新" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileManager.label" defaultMessage="上传者" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileManager.tip',
        defaultMessage: '文件上传者查询',
      })),
      dataIndex: 'scFileManager',
      valueType: 'select',
      width: 120,
      renderText: (text) => userMap.getUserName(text),
      valueEnum: mapValueEnum({
        map: userMap.getAllUsers(),
        cnTextName: 'userName',
        enTextName: 'userNameEn',
        valueName: 'userLoginName',
      }),
      sorter: (source, target) => (source.scFileManager || '').localeCompare(target.scFileManager || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.files.scFileManager.tip" defaultMessage="文件上传者" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileCreateDate.label" defaultMessage="创建日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileCreateDate.tip',
        defaultMessage: '创建日期查询',
      })),
      dataIndex: 'scFileCreateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,

    },
    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFileSpecies.label" defaultMessage="种属" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFileSpecies.tip',
        defaultMessage: '种属查询',
      })),
      dataIndex: 'scFileSpecies',
      valueType: 'select',
      width: 60,
      renderText: (text) => dictMap.getItemName(SYS_SAM_SPECIES_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_SAM_SPECIES_DICT_GROUP.group))),
      sorter: (source, target) => (source.scFileSpecies || '').localeCompare(target.scFileSpecies || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.files.scFileSpecies.tip" defaultMessage="种属" />
            ),
          },
        ],
      },

    },
    {
      title: <FormattedMessage id="pages.list.sys.cms.files.scFielUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.files.scFielUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'scFielUpdateDate',
      valueType: 'date',
      search: false,
      hideInForm: true,
      width: 120,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.files.scFielUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.files.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      align: 'center',
      fixed: 'right',
      width: 140,
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.cms.files.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.cms.files.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.cms.files.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
          <Divider type="vertical" />
          <a
            onClick={async () => {
              await fileHandles.downloadByPath({ path: record.scFilePath, fileName: record.scFileName });
            }
            }
          >
            <FormattedMessage id="pages.list.files.download" defaultMessage="下载" />
          </a>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysCmsFilesItem>
        headerTitle={<FormattedMessage id='pages.list.sys.cms.files.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="scFileId"
        scroll={{ x: 1300 }}
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.cms.files.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysCmsFilesService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.cms.files.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.cms.files.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.cms.files.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysCmsFilesCreateForm onCancel={() => handleCreateVisible(false)}
                             onSubmit={async (value) => {
                               const success = await handles.add(value);
                               if (success) {
                                 handleCreateVisible(false);
                                 if (actionRef.current) {
                                   actionRef.current.reload();
                                 }
                               }
                             }
                             }
                             dictMap={dictMap}
                             visible={createVisible}>
      </SysCmsFilesCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysCmsFilesUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateVisible(false);
            setFormValues({});
          }
          }
          dictMap={dictMap}
          visible={updateVisible}
          values={formValues}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.scFileId && (
          <ProDescriptions<SysCmsFilesItem>
            column={2}
            title={row?.scFileName}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                scFileId: row?.scFileId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_dictionary, sys_user }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap, userMap: sys_user.userMap };
};

export default connect(mapStateToProps)(SysCmsFilesTableList);
