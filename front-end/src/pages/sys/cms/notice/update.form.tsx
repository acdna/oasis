import React, { useState } from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysCmsNoticeItem } from '@/pages/sys/cms/notice/data';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import { options } from '@/components/common/select.options';
import { SYS_SWITCH_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';
import DictMapper from '@/common/dictionary/dict.mapper';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * SYS_CMS_NOTICE-更新记录表单的输入参数定义
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysCmsNoticeUpdateFormProps {
  onSubmit: (fields: SysCmsNoticeItem) => void,
  updateModalVisible: boolean;
  onCancel: () => void;
  values: any;
  dictMap: DictMapper;
}

/**
 * SYS_CMS_NOTICE-更新字典项信息表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCmsNoticeUpdateForm: React.FC<SysCmsNoticeUpdateFormProps> = (props) => {
  const { onSubmit, onCancel, updateModalVisible, values, dictMap } = props;

  const intl = useIntl();

  //表单的初始数据信息
  const [formVals] = useState({
    scNoticeId: values.scNoticeId,
    scNoticeTitle: values.scNoticeTitle,
    scNoticeContent: values.scNoticeContent,
    scNoticeIsNew: values.scNoticeIsNew,
    scNoticeManager: values.scNoticeManager,
    scNoticeCreateDate: values.scNoticeCreateDate,
    scNoticeUpdateDate: values.scNoticeUpdateDate,

  });

  //表单信息对象
  const [form] = Form.useForm();

  /**
   * 点击更新按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const values = { ...formVals, ...fieldValues, scNoticeManager: AuthContext.loginName };//合并数据
    onSubmit({ ...values });
  };

  /**
   * 点击取消按钮的操作
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  /**
   * 渲染表单内容
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const renderContent = () => (
    <>
      <Form.Item
        name="scNoticeTitle"
        label={<FormattedMessage id='pages.update.sys.cms.notice.scNoticeTitle.label' defaultMessage='公告标题' />}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage id='pages.update.sys.cms.notice.scNoticeTitle.tip' defaultMessage='请输入公告标题！' />),
          },
        ]}
      >
        <Input placeholder={intl.formatMessage({
          id: 'pages.sys.cms.notice.update.scNoticeTitle.tip',
          defaultMessage: '请输入公告标题！',
        })} />
      </Form.Item>

      <Form.Item
        name="scNoticeContent"
        label={<FormattedMessage id='pages.update.sys.cms.notice.scNoticeContent.label' defaultMessage='公告内容' />}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage id='pages.update.sys.cms.notice.scNoticeContent.tip' defaultMessage='请输入公告内容！' />),
          },
        ]}
      >
        <TextArea placeholder={intl.formatMessage({
          id: 'pages.sys.cms.notice.update.scNoticeContent.tip',
          defaultMessage: '请输入公告内容！',
        })} />
      </Form.Item>

      <Form.Item
        name="scNoticeIsNew"
        label={<FormattedMessage id='pages.update.sys.cms.notice.scNoticeIsNew.label' defaultMessage='是否是新文件' />}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage id='pages.update.sys.cms.notice.scNoticeIsNew.tip' defaultMessage='请输入是否是新文件！' />),
          },
        ]}
      >
        <Select
          style={{ width: '100%' }}
          placeholder={intl.formatMessage({
            id: 'pages.update.sys.cms.notice.scNoticeIsNew.placeholder',
            defaultMessage: '是否是新文件',
          })}
        >
          {options(SYS_SWITCH_DICT_GROUP.group, dictMap)}
        </Select>
      </Form.Item>

    </>
  );

  return (
    <Modal
      width={640}
      bodyStyle={{ padding: '32px 40px 48px' }}
      destroyOnClose
      title={<FormattedMessage id='pages.update.sys.cms.notice.form.title' defaultMessage='修改' />}
      visible={updateModalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm
        submitter={false}
        form={form}
        initialValues={
          {
            scNoticeId: formVals.scNoticeId,
            scNoticeTitle: formVals.scNoticeTitle,
            scNoticeContent: formVals.scNoticeContent,
            scNoticeIsNew: formVals.scNoticeIsNew,
            scNoticeManager: formVals.scNoticeManager,
            scNoticeCreateDate: formVals.scNoticeCreateDate,
            scNoticeUpdateDate: formVals.scNoticeUpdateDate,

          }
        }
      >
        {renderContent()}
      </ProForm>

    </Modal>
  );
};

export default SysCmsNoticeUpdateForm;
