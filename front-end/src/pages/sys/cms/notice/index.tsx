import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm } from 'antd';
import React, { useRef, useState } from 'react';
import { connect, FormattedMessage, useIntl } from 'umi';
import { FooterToolbar, PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import { SysCmsNoticeService } from '@/services/sys/sys.cms.notice.service';
import { SysCmsNoticeItem } from '@/pages/sys/cms/notice/data';
import { SysCmsNoticeHandles } from '@/pages/sys/cms/notice/handles';
import SysCmsNoticeCreateForm from '@/pages/sys/cms/notice/create.form';
import SysCmsNoticeUpdateForm from '@/pages/sys/cms/notice/update.form';
import * as moment from 'moment';
import { MLOCALES } from '@/common/locales/locale';
import { dictValueEnum, mapValueEnum } from '@/components/common/select.options';
import { ConnectState } from '@/models/connect';
import UserMapper from '@/common/users/user.mapper';
import { Dispatch } from '@@/plugin-dva/connect';
import DictMapper from '@/common/dictionary/dict.mapper';
import { SYS_SWITCH_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';
import {SYS_DICTIONARY_EFFECTS} from "@/pages/sys/dictionary/effects";
import {SYS_USER_EFFECTS} from "@/pages/sys/user/effects";

interface SysCmsNoticeTableListProps {
  dictMap: DictMapper;
  userMap: UserMapper;
  dispatch: Dispatch;
}

/**
 * SYS_CMS_NOTICE-列表查询
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCmsNoticeTableList: React.FC<SysCmsNoticeTableListProps> = (props) => {
  const [createModalVisible, handleCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [formValues, setFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<SysCmsNoticeItem>();
  const [selectedRowsState, setSelectedRows] = useState<SysCmsNoticeItem[]>([]);
  const handles = new SysCmsNoticeHandles();
  const intl = useIntl();
  const { dictMap, userMap ,dispatch} = props;

  SYS_USER_EFFECTS.LOAD_ALL(dispatch);
  SYS_DICTIONARY_EFFECTS.LOAD_ALL(dispatch);

  //定义列信息
  const columns: ProColumns<SysCmsNoticeItem>[] = [
    {
      dataIndex: 'scNoticeId',
      sorter: false,
      valueType: 'text',
      search: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.notice.scNoticeTitle.label" defaultMessage="公告标题" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.notice.scNoticeTitle.tip',
        defaultMessage: '公告标题查询',
      })),
      dataIndex: 'scNoticeTitle',
      valueType: 'text',
      sorter: (source, target) => (source.scNoticeTitle || '').localeCompare(target.scNoticeTitle || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.notice.scNoticeTitle.tip" defaultMessage="公告标题" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.notice.scNoticeContent.label" defaultMessage="公告内容" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.notice.scNoticeContent.tip',
        defaultMessage: '公告内容查询',
      })),
      dataIndex: 'scNoticeContent',
      valueType: 'text',
      sorter: (source, target) => (source.scNoticeContent || '').localeCompare(target.scNoticeContent || ''),


      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.notice.scNoticeContent.tip" defaultMessage="公告内容" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.notice.scNoticeIsNew.label" defaultMessage="是否是新文件" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.notice.scNoticeIsNew.tip',
        defaultMessage: '是否是新文件查询',
      })),
      dataIndex: 'scNoticeIsNew',
      valueType: 'text',
      renderText: (text) => dictMap.getItemName(SYS_SWITCH_DICT_GROUP.group, text),
      valueEnum: (dictValueEnum(dictMap.getDictItems(SYS_SWITCH_DICT_GROUP.group))),
      sorter: (source, target) => (source.scNoticeIsNew || '').localeCompare(target.scNoticeIsNew || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.notice.scNoticeIsNew.tip" defaultMessage="是否是新文件" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.notice.scNoticeManager.label" defaultMessage="公告发布者" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.notice.scNoticeManager.tip',
        defaultMessage: '公告发布者查询',
      })),
      dataIndex: 'scNoticeManager',
      valueType: 'text',
      renderText: (text) => userMap.getUserName(text),
      valueEnum: mapValueEnum({
        map: userMap.getAllUsers(),
        cnTextName: 'userName',
        enTextName: 'userNameEn',
        valueName: 'userLoginName',
      }),
      sorter: (source, target) => (source.scNoticeManager || '').localeCompare(target.scNoticeManager || ''),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.notice.scNoticeManager.tip" defaultMessage="公告发布者" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.notice.scNoticeUpdateDate.label" defaultMessage="更新日期" />,
      tooltip: (intl.formatMessage({
        id: 'pages.list.sys.cms.notice.scNoticeUpdateDate.tip',
        defaultMessage: '更新日期查询',
      })),
      dataIndex: 'scNoticeUpdateDate',
      valueType: 'date', search: false, hideInForm: true,
      render: (text: any) => moment(text).format(MLOCALES.pattern),
      formItemProps: {
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="pages.list.sys.cms.notice.scNoticeUpdateDate.tip" defaultMessage="更新日期" />
            ),
          },
        ],
      },

    },

    {
      title: <FormattedMessage id="pages.list.sys.cms.notice.option.title" defaultMessage="操作" />,
      dataIndex: 'option',
      valueType: 'option',
      fixed: 'right',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setFormValues(record);
            }
            }
          >
            <FormattedMessage id="pages.list.sys.cms.notice.option.edit" defaultMessage="修改" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title={<FormattedMessage id='pages.list.sys.cms.notice.option.delete.confirm'
                                               defaultMessage='删除不可恢复，你确定要删除吗?' />}
                      onConfirm={async () => {
                        const success = await handles.deleteById(record);
                        if (success && actionRef.current) {
                          actionRef.current.reload();
                        }
                      }
                      }>
            <a>
              <FormattedMessage id="pages.list.sys.cms.notice.option.delete" defaultMessage="删除" />
            </a>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable<SysCmsNoticeItem>
        headerTitle={<FormattedMessage id='pages.list.sys.cms.notice.title' defaultMessage='查询' />}
        actionRef={actionRef}
        rowKey="scNoticeId"
        search={
          { labelWidth: 120 }
        }
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleCreateModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="pages.list.sys.cms.notice.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const data = await SysCmsNoticeService.findPage({ ...params, sorter, filter });
          return {
            data: data.data,
            success: true,
            total: data.page.count,
            current: data.page.offset,
            pageSize: data.page.pageSize,
          };
        }
        }
        columns={columns}
        pagination={
          {
            defaultCurrent: 1,
            defaultPageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
          }
        }
        rowSelection={
          {
            onChange: (_, selectedRows) => setSelectedRows(selectedRows),
          }
        }

      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.list.sys.cms.notice.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.list.sys.cms.notice.item" defaultMessage="项" />
            </div>
          }
        >
          <Button danger
                  onClick={async () => {
                    await handles.deleteByIds(selectedRowsState);
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                  }
          >
            <DeleteOutlined /> <FormattedMessage id="pages.list.sys.cms.notice.batch.delete" defaultMessage="批量删除" />
          </Button>
        </FooterToolbar>
      )}

      <SysCmsNoticeCreateForm onCancel={() => handleCreateModalVisible(false)}
                              onSubmit={async (value) => {
                                const success = await handles.add(value);
                                if (success) {
                                  handleCreateModalVisible(false);
                                  if (actionRef.current) {
                                    actionRef.current.reload();
                                  }
                                }
                              }
                              }
                              dictMap={dictMap}
                              modalVisible={createModalVisible}>
      </SysCmsNoticeCreateForm>

      {formValues && Object.keys(formValues).length>0 ? (
        <SysCmsNoticeUpdateForm
          onSubmit={async (value: any) => {
            const success = await handles.update(value);
            if (success) {
              handleUpdateModalVisible(false);
              setFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }
          }
          onCancel={() => {
            handleUpdateModalVisible(false);
            setFormValues({});
          }
          }
          updateModalVisible={updateModalVisible}
          values={formValues}
          dictMap={dictMap}
        />
      ) : null}


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }
        }
        closable={false}
      >
        {row?.scNoticeId && (
          <ProDescriptions<SysCmsNoticeItem>
            column={2}
            title={row?.scNoticeId}
            request={async () => ({
              data: row || {},
            })}
            params={
              {
                scNoticeId: row?.scNoticeId,
              }
            }
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

const mapStateToProps = ({ sys_dictionary, sys_user }: ConnectState) => {
  return { dictMap: sys_dictionary.dictMap, userMap: sys_user.userMap };
};

export default connect(mapStateToProps)(SysCmsNoticeTableList);
