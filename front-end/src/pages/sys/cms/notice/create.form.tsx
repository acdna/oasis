import React from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { SysCmsNoticeItem } from '@/pages/sys/cms/notice/data';
import ProForm from '@ant-design/pro-form';
import TextArea from 'antd/es/input/TextArea';
import DictMapper from '@/common/dictionary/dict.mapper';
import { options } from '@/components/common/select.options';
import { SYS_SWITCH_DICT_GROUP } from '@/common/dictionary/sys.dict.defined';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * SYS_CMS_NOTICE-创建记录表单的输入参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
interface SysCmsNoticeCreateFormProps {
  onSubmit: (fields: SysCmsNoticeItem) => void,
  onCancel: () => void;
  modalVisible: boolean;
  dictMap: DictMapper;
}

/**
 * SYS_CMS_NOTICE-创建记录表单
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const SysCmsNoticeCreateForm: React.FC<SysCmsNoticeCreateFormProps> = (props) => {
  const { modalVisible, onCancel, onSubmit, dictMap } = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  /**
   * 点击确定按钮的操作方法
   * @author jiangbin
   * @date 12/30/2020, 5:14:27 PM
   **/
  const handleOk = async () => {
    const fieldValues = await form.validateFields();
    const {
      scNoticeId,
      scNoticeTitle,
      scNoticeContent,
      scNoticeIsNew,
      scNoticeCreateDate,
      scNoticeUpdateDate,
    } = fieldValues;
    onSubmit({
      scNoticeId,
      scNoticeTitle,
      scNoticeContent,
      scNoticeIsNew,
      scNoticeManager:AuthContext.loginName,
      scNoticeCreateDate,
      scNoticeUpdateDate,
    });
  };

  /**
   * 点击取消按钮的操作方法
   * @author jiang
   * @date 2020-12-19 14:05:15
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.create.sys.cms.notice.form.title' defaultMessage='新建' />
      }
      visible={modalVisible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false}>

        <Form.Item
          name="scNoticeTitle"
          label={<FormattedMessage id="pages.create.sys.cms.notice.scNoticeTitle.label" defaultMessage="公告标题" />}
          rules={[{
            required: true,
            message: (
              <FormattedMessage id="pages.create.sys.cms.notice.scNoticeTitle.tip" defaultMessage="公告标题为必填项" />),
          }]}
        >
          <Input placeholder={intl.formatMessage({
            id: 'pages.create.sys.cms.notice.scNoticeTitle.placeholder',
            defaultMessage: '公告标题',
          })} />
        </Form.Item>

        <Form.Item
          name="scNoticeContent"
          label={<FormattedMessage id="pages.create.sys.cms.notice.scNoticeContent.label" defaultMessage="公告内容" />}
          rules={[{
            required: true,
            message: (
              <FormattedMessage id="pages.create.sys.cms.notice.scNoticeContent.tip" defaultMessage="公告内容为必填项" />),
          }]}
        >
          <TextArea placeholder={intl.formatMessage({
            id: 'pages.create.sys.cms.notice.scNoticeContent.placeholder',
            defaultMessage: '公告内容',
          })} />
        </Form.Item>

        <Form.Item
          name="scNoticeIsNew"
          label={<FormattedMessage id="pages.create.sys.cms.notice.scNoticeIsNew.label" defaultMessage="是否是新文件" />}
          rules={[{
            required: true,
            message: (
              <FormattedMessage id="pages.create.sys.cms.notice.scNoticeIsNew.tip" defaultMessage="是否是新文件为必填项" />),
          }]}
        >
          <Select
            style={{ width: '100%' }}
            placeholder={intl.formatMessage({
              id: 'pages.create.sys.cms.notice.scNoticeIsNew.placeholder',
              defaultMessage: '是否是新文件',
            })}
          >
            {options(SYS_SWITCH_DICT_GROUP.group, dictMap)}
          </Select>
        </Form.Item>

      </ProForm>
    </Modal>
  );
};

export default SysCmsNoticeCreateForm;
