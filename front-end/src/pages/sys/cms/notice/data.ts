/**
 * SYS_CMS_NOTICE-字段信息
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsNoticeItem {
    scNoticeId: string;
    scNoticeTitle: string;
    scNoticeContent: string;
    scNoticeIsNew: string;
    scNoticeManager: string;
    scNoticeCreateDate: Date;
    scNoticeUpdateDate: Date;
}
/**
 * SYS_CMS_NOTICE-分页参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsNoticePagination {
    total: number;
    pageSize: number;
    current: number;
    pageNum: number;
}
/**
 * SYS_CMS_NOTICE-表格数据
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsNoticeTableData {
    list: SysCmsNoticeItem[];
    pagination: Partial < SysCmsNoticePagination > ;
}
/**
 * SYS_CMS_NOTICE-查询参数
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 */
export interface SysCmsNoticeParams extends SysCmsNoticeItem {
    pageSize ? : number;
    current: number;
    pageNum: number;
    filter ? : {
        [key: string]: any[] };
    sorter ? : {
        [key: string]: any };
}