export interface ExcelTemplate {
  templateId: string;
  fileName: string;
  ext: string;
}

/**
 * 后台的文件模板定义
 * @author: jiangbin
 * @date: 2021-02-03 12:21:40
 **/
export const EXCEL_TEMPLATES = {
  onp_statistics:{
    templateId: 'onp_statistics',
    fileName: { en: 'statistics-template', cn: '信息模板' },
    ext: 'xlsx',
  }
};

