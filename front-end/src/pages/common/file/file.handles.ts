import { message } from 'antd';
import { downloadFile } from '@/common/utils/system';
import { FileService } from '@/services/common/file/file.service';
import { FileUtils } from '@/common/utils/file.utils';
import * as moment from 'moment';
import { SysFilesService } from '@/services/sys/sys.files.service';
import {getLocaleMessage, LocaleContext} from '@/common/locales/locale';

/**
 * 文件操作句柄类
 * @author: jiangbin
 * @date: 2021-02-05 18:53:59
 **/
export class FileHandles {
  /**
   * 文件下载，该功能不提供国际化支持，使用指定文件名下载文件，所以需要提前对文件名进行国际化处理
   *
   * @param record.path 相对路径
   * @param record.fileName 文件名，不带扩展名
   * @param record.fileExt 文件扩展名，若未指定则从文件路径中获取
   * <br/><br/>
   * Create by WuHaotian on 2021-01-14
   */
  downloadByPath = async (record: { path: string, fileName: string, fileExt?: string }) => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.file.download.loading',
      defaultMessage: '正在下载',
    }));
    if (!record) return true;
    let {path, fileName, fileExt} = record;
    try {
      let ext = fileExt || FileUtils.getFileExt(path);
      ext = ext.startsWith('.')?ext:`.${ext}`;
      fileName = FileUtils.getSimpleFileName(fileName);
      const date = moment(new Date()).format('YYYYMMDDHHmmss');
      fileName = `${fileName}-${date}${ext}`;
      record.fileName = fileName;
      const fileData = await FileService.downloadByPath(path);
      downloadFile({ buf: fileData, fileName });
      hide();
      message.success(LocaleContext.message({
        id: 'pages.file.download.success',
        defaultMessage: '下载成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.file.download.error',
        defaultMessage: '下载失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 下载指定ID的文件，本方法的国际化是基于数据库文件表中字义的中英文文件名来实现的
   *
   * @param record.fileId sys_files记录的ID号
   * @param record.fileName 文件名，不带扩展名
   * @param record.fileExt 文件扩展名，若未指定则从文件路径中获取
   * <br/><br/>
   * Create by WuHaotian on 2021-01-14
   */
  downloadById = async (record: { fileId: string, fileName?: string,fileExt?: string }) => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.file.download.loading',
      defaultMessage: '正在下载',
    }));
    if (!record) return true;
    let {fileId, fileName, fileExt} = record;
    try {
      let file = await SysFilesService.findById(fileId);
      if (!file) {
        throw new Error('文件记录不存在!');
      }

      const ext = fileExt || FileUtils.getFileExt(file.filePath);
      record.fileExt = ext;

      fileName = fileName || getLocaleMessage({ cn: file.fileName, en: file.fileNameEn });
      fileName = FileUtils.getSimpleFileName(fileName||'');

      const date = moment(new Date()).format('YYYYMMDDHHmmss');
      fileName = `${fileName}-${date}${ext}`;
      const fileData = await FileService.downloadById(fileId);
      downloadFile({ buf: fileData, fileName });

      hide();
      message.success(LocaleContext.message({
        id: 'pages.file.download.success',
        defaultMessage: '下载成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.file.download.error',
        defaultMessage: '下载失败，请重试!',
      }));
      return false;
    }
  };
};
