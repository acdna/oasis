/**
 * 文件模板列表
 * @author: jiangbin
 * @date: 2021-02-03 12:21:40
 **/
export const DOCX_TEMPLATES = {
  test_result: {
    templateId: 'test_result',
    fileName: {
      cn: '检验结果单.docx',
      en: 'TestResults.docx',
    },
    ext: 'docx',
  },
};
