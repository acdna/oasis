import {message} from 'antd';
import {downloadFile} from '@/common/utils/system';
import {FileUtils} from '@/common/utils/file.utils';
import * as moment from 'moment';
import {getLocaleMessage, LocaleContext} from '@/common/locales/locale';
import {ExcelService} from '@/services/common/file/excel.service';

/**
 * Excel操作句柄类
 * @author: jiangbin
 * @date: 2021-02-05 18:46:16
 * */
export class ExcelHandles {

  /**
   * 下载指定ID的模板文件
   *
   * @param record.path 相对路径
   * @param record.fileName 支持国际化的文件名，不带扩展名
   * @param record.ext 文件扩展名
   * <br/><br/>
   * Create by WuHaotian on 2021-01-14
   */
  downloadTemplate = async (record: { templateId: string, fileName: { cn: string, en: string }, ext: string }) => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.file.download.loading',
      defaultMessage: '正在下载',
    }));
    if (!record) return true;
    try {
      const ext = record.ext.startsWith('.') ? record.ext : `.${record.ext}`;
      let fileName = getLocaleMessage(record.fileName);
      fileName = FileUtils.getSimpleFileName(fileName);
      const date = moment(new Date()).format('YYYYMMDDHHmmss');
      fileName = `${fileName}-${date}${ext}`;
      const fileData = await ExcelService.downloadTemplate(record.templateId);
      downloadFile({buf: fileData, fileName});

      hide();
      message.success(LocaleContext.message({
        id: 'pages.file.download.success',
        defaultMessage: '下载成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.file.download.error',
        defaultMessage: '下载失败，请重试!',
      }));
      return false;
    }
  };
};
