import { getServerUrl, SERVER_URLS } from '@/common/utils/system';
import { AuthContext } from '@/common/auth/auth.context';
import { Button, Divider, message, Upload } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import React from 'react';
import { DownloadOutlined, UploadOutlined } from '@ant-design/icons';
import { UploadProps } from 'antd/lib/upload/interface';
import { FileHandles } from '@/pages/common/file/file.handles';
import { FileUtils } from '@/common/utils/file.utils';

/**
 * 文件上传属性对象
 * @author: jiangbin
 * @date: 2021-01-17 14:51:36
 **/
export interface FileUploadProps {
  /**
   * 文件名称，用于下载文件
   * @author: jiangbin
   * @date: 2021-01-17 15:39:17
   **/
  fileName?: string;
  /**
   * 文件名称，用于下载文件
   * @author: jiangbin
   * @date: 2021-01-17 15:39:17
   **/
  setFileName?: any;
  /**
   * 文件相对路径，用于下载文件
   * @author: jiangbin
   * @date: 2021-01-17 15:39:14
   **/
  filePath: string;
  /**
   * 设置文件相对路径
   * @author: jiangbin
   * @date: 2021-01-17 15:39:12
   **/
  setFilePath: any;
  /**
   * 设置文件扩展名
   * @author: jiangbin
   * @date: 2021-01-17 15:39:12
   **/
  setFileExt?: any;
  /**
   * 设置文件大小，用于设置上传后的文件大小
   * @author: jiangbin
   * @date: 2021-01-17 15:39:12
   **/
  setFileSize?: any;
  /**
   * 下载按钮的样式，支持按钮和链接两种样式，默认使用按钮样式:"button"
   * @author: jiangbin
   * @date: 2021-01-17 15:39:09
   **/
  downloadRender?: 'button' | 'a';
  /**
   * 新的自定义上传组件Upload的配置参数，优先使用这此参数
   * @author: jiangbin
   * @date: 2021-01-17 16:06:29
   **/
  uploadProps?: UploadProps;
}

/**
 * 文件上传组件的二次封装
 * @author: jiangbin
 * @date: 2021-01-17 14:51:33
 **/
const FileUpload: React.FC<FileUploadProps> = (props) => {
  const {
    filePath,
    setFilePath,
    fileName,
    setFileName,
    setFileExt,
    downloadRender = 'button',
    uploadProps = {},
    setFileSize,
  } = props;
  const intl = useIntl();
  const fileHandles = new FileHandles();

  /**
   * 上传文件参数
   * @author: jiangbin
   * @date: 2021-01-06 17:43:29
   **/
  const sUploadProps = {
    name: 'file',
    multiple: false,
    action: getServerUrl(SERVER_URLS.UPLOAD_FILE),
    headers: AuthContext.headers,
    onChange(info: any) {
      if (info.file.status !== 'uploading') {
        //console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        let response = info.file.response;
        if (response.status === 'OK') {
          //回填文件相对路径参数
          setFilePath && setFilePath(response.relativePath);
          setFileSize && setFileSize(info.file.size || 0);
          setFileName && setFileName(FileUtils.getSimpleFileName(response.originalName));
          setFileExt && setFileExt(response.originalName);
        }
        message.success(`${info.file.name} ${intl.formatMessage({
          id: 'pages.file.upload.success',
          defaultMessage: '文件上传成功.',
        })}`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} ${intl.formatMessage({
          id: 'pages.file.upload.fail',
          defaultMessage: '文件上传失败!',
        })}`);
      }
    },
  };

  //合并参数
  const targetProps = { ...sUploadProps, ...uploadProps };

  return (
    <>
      {(downloadRender === 'a' && filePath && filePath.length > 0) && (
        <a
          onClick={async () => {
            await fileHandles.downloadByPath({ path: filePath || '', fileName: fileName || '文件' });
          }
          }
        >
          <FormattedMessage id="pages.file.download.tip" defaultMessage="下载文件" />
        </a>
      )}
      {(downloadRender === 'button' && filePath && filePath.length > 0) && (
        <Button icon={<DownloadOutlined />}
                onClick={async () => {
                  await fileHandles.downloadByPath({ path: (filePath || ''), fileName: (fileName || '文件') });
                }
                }
        >
          <FormattedMessage id="pages.file.download.tip" defaultMessage="下载文件" />
        </Button>
      )}
      {(filePath && filePath.length > 0) && (<Divider type="vertical" />)}
      <Upload {...targetProps}>
        <Button icon={<UploadOutlined />}>
          <FormattedMessage id="pages.file.upload.tip" defaultMessage="上传文件" />
        </Button>
      </Upload>
    </>
  );
};
export default FileUpload;
