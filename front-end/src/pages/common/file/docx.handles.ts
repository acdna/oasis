import {message} from 'antd';
import {downloadFile} from '@/common/utils/system';
import {FileService} from '@/services/common/file/file.service';
import {FileUtils} from '@/common/utils/file.utils';
import * as moment from 'moment';
import {getLocaleMessage, LocaleContext} from '@/common/locales/locale';
import {DocxService} from '@/services/common/file/docx.service';

/**
 * Word模板文件合并操作句柄类
 * @author: jiangbin
 * @date: 2021-02-04 18:33:55
 **/
export class DocxHandles {

  /**
   * 基于ID的Word模板创建Docx文件并下载文件
   * @param params.templateId 模板ID
   * @param params.fileName 文件名称
   * @param params.ext 文件扩展名
   * @param params.templateId 基本参数信息
   * @param data 合并Word模板时使用的JSON参数数据
   * @author: jiangbin
   * @date: 2021-02-04 18:34:15
   **/
  createByTemplateId = async (params: { templateId: string, fileName: { cn: string, en: string }, ext: string }, data: any) => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.word.download.loading',
      defaultMessage: '正在生成Word文件',
    }));
    if (!params) return true;
    try {
      // 文件扩展名
      const ext = params.ext.startsWith('.') ? params.ext : `.${params.ext}`;

      // 创建文件名
      let fileName = getLocaleMessage(params.fileName);
      fileName = FileUtils.getSimpleFileName(fileName);
      const date = moment(new Date()).format('YYYYMMDDHHmmss');
      fileName = `${fileName}-${date}${ext}`;

      // 创建word并下载文件
      const pathResult = await DocxService.createByTemplateId(params.templateId, data);
      if (pathResult.code !== 200) {
        throw new Error('创建Word文件失败!');
      }
      let fileData = await FileService.downloadByPath(pathResult.relativePath);
      downloadFile({buf: fileData, fileName});

      hide();
      message.success(LocaleContext.message({
        id: 'pages.word.download.success',
        defaultMessage: '生成Word文件成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.word.download.error',
        defaultMessage: '生成Word文件失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 基于相对路径的Word模板创建Docx文件并下载文件
   * @param params.templatePath 模板文件的相对路径
   * @param params.fileName 文件名称
   * @param params.ext 文件扩展名
   * @param params.templateId 基本参数信息
   * @param data 合并Word模板时使用的JSON参数数据
   * @author: jiangbin
   * @date: 2021-02-04 18:34:15
   **/
  createByRelativePath = async (params: { templatePath: string, fileName: { cn: string, en: string }, ext: string }, data: any) => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.word.download.loading',
      defaultMessage: '正在生成Word文件',
    }));
    if (!params) return true;
    try {
      //文件扩展名
      params.ext = params.ext.startsWith('.') ? params.ext : `.${params.ext}`;

      //创建文件名
      let fileName = getLocaleMessage(params.fileName);
      fileName = FileUtils.getSimpleFileName(fileName);
      const date = moment(new Date()).format('YYYYMMDDHHmmss');
      fileName = `${fileName}-${date}${params.ext}`;

      //创建word并下载文件
      const pathResult = await DocxService.createByRelativePath(params.templatePath, data);
      if (pathResult.code !== 200) {
        throw new Error('创建Word文件失败!');
      }
      let fileData = await FileService.downloadByPath(pathResult.relativePath);
      downloadFile({buf: fileData, fileName});

      hide();
      message.success(LocaleContext.message({
        id: 'pages.word.download.success',
        defaultMessage: '生成Word文件成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.word.download.error',
        defaultMessage: '生成Word文件失败，请重试!',
      }));
      return false;
    }
  };

  /**
   * 基于全路径的Word模板创建Docx文件并下载文件
   * @param params.templatePath 模板文件的相对路径
   * @param params.fileName 文件名称
   * @param params.ext 文件扩展名
   * @param params.templateId 基本参数信息
   * @param data 合并Word模板时使用的JSON参数数据
   * @author: jiangbin
   * @date: 2021-02-04 18:34:15
   **/
  createByFullPath = async (params: { templatePath: string, fileName: { cn: string, en: string }, ext: string }, data: any) => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.word.download.loading',
      defaultMessage: '正在生成Word文件',
    }));
    if (!params) return true;
    try {
      //文件扩展名
      params.ext = params.ext.startsWith('.') ? params.ext : `.${params.ext}`;

      //创建文件名
      let fileName = getLocaleMessage(params.fileName);
      fileName = FileUtils.getSimpleFileName(fileName);
      const date = moment(new Date()).format('YYYYMMDDHHmmss');
      fileName = `${fileName}-${date}${params.ext}`;

      //创建word并下载文件
      const pathResult = await DocxService.createByFullPath(params.templatePath, data);
      if (pathResult.code !== 200) {
        throw new Error('创建Word文件失败!');
      }
      let fileData = await FileService.downloadByPath(pathResult.relativePath);
      downloadFile({buf: fileData, fileName});

      hide();
      message.success(LocaleContext.message({
        id: 'pages.word.download.success',
        defaultMessage: '生成Word文件成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.word.download.error',
        defaultMessage: '生成Word文件失败，请重试!',
      }));
      return false;
    }
  };
};
