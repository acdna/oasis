import {message} from 'antd';
import {SystemService} from "@/services/common/system/system.service";
import {LocaleContext} from "@/common/locales/locale";

/**
 * 系统功能操作句柄类
 * @author: jiangbin
 * @date: 2021-02-04 18:33:55
 **/
export class SystemHandles {

  /**
   * 刷新缓存数据
   * @author: jiangbin
   * @date: 2021-02-09 15:52:40
   **/
  refreshCache = async () => {
    const hide = message.loading(LocaleContext.message({
      id: 'pages.system.refresh.cache.loading',
      defaultMessage: '正在刷新系统缓存数据',
    }));
    try {
      await SystemService.refreshCache();
      hide();
      message.success(LocaleContext.message({
        id: 'pages.system.refresh.cache.success',
        defaultMessage: '正在刷新系统缓存数据成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'pages.system.refresh.cache.error',
        defaultMessage: '正在刷新系统缓存数据失败，请重试!',
      }));
      return false;
    }
  };
};
