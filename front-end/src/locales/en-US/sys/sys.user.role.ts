export default {
  'pages.create.sys.user.role.form.title': 'Create',

  'pages.create.sys.user.role.urId.label': '用户角色ID',
  'pages.create.sys.user.role.urId.tip': '用户角色ID',
  'pages.create.sys.user.role.urId.placeholder': '用户角色ID',

  'pages.create.sys.user.role.urRoleId.label': '角色ID',
  'pages.create.sys.user.role.urRoleId.tip': '角色ID',
  'pages.create.sys.user.role.urRoleId.placeholder': '角色ID',

  'pages.create.sys.user.role.urUserLoginName.label': '用户帐号',
  'pages.create.sys.user.role.urUserLoginName.tip': '用户帐号',
  'pages.create.sys.user.role.urUserLoginName.placeholder': '用户帐号',


  'pages.update.sys.user.role.form.title': 'Update',

  'pages.update.sys.user.role.urId.label': '用户角色ID',
  'pages.update.sys.user.role.urId.tip': '用户角色ID',
  'pages.update.sys.user.role.urId.placeholder': '用户角色ID',

  'pages.update.sys.user.role.urRoleId.label': '角色ID',
  'pages.update.sys.user.role.urRoleId.tip': '角色ID',
  'pages.update.sys.user.role.urRoleId.placeholder': '角色ID',

  'pages.update.sys.user.role.urUserLoginName.label': '用户帐号',
  'pages.update.sys.user.role.urUserLoginName.tip': '用户帐号',
  'pages.update.sys.user.role.urUserLoginName.placeholder': '用户帐号',


  'pages.list.sys.user.role.form.title': 'Query',

  'pages.list.sys.user.role.urId.label': '用户角色ID',
  'pages.list.sys.user.role.urId.tip': '用户角色ID',
  'pages.list.sys.user.role.urId.placeholder': '用户角色ID',

  'pages.list.sys.user.role.urRoleId.label': '角色ID',
  'pages.list.sys.user.role.urRoleId.tip': '角色ID',
  'pages.list.sys.user.role.urRoleId.placeholder': '角色ID',

  'pages.list.sys.user.role.urUserLoginName.label': '用户帐号',
  'pages.list.sys.user.role.urUserLoginName.tip': '用户帐号',
  'pages.list.sys.user.role.urUserLoginName.placeholder': '用户帐号',


  'pages.list.sys.user.role.option.delete':'Delete',
  'pages.list.sys.user.role.batch.delete':'Batch delete',
  'pages.list.sys.user.role.item':'item',
  'pages.list.sys.user.role.chosen':'Has chosen',
  'pages.list.sys.user.role.new':'Create',
  'pages.list.sys.user.role.title':'Query',
  'pages.list.sys.user.role.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.user.role.option.edit':'Edit',
  'pages.list.sys.user.role.option.title':'Option',
  'pages.list.sys.user.role.index.label':'No.',
};
