export default {
  'pages.create.sys.billing.card.form.title': 'Create',
  
  'pages.create.sys.billing.card.bcId.label': 'id',
  'pages.create.sys.billing.card.bcId.tip': 'id',
  'pages.create.sys.billing.card.bcId.placeholder': 'id',
  
  'pages.create.sys.billing.card.bcSerialNumber.label': '序列号信息',
  'pages.create.sys.billing.card.bcSerialNumber.tip': '序列号信息',
  'pages.create.sys.billing.card.bcSerialNumber.placeholder': '序列号信息',
  
  'pages.create.sys.billing.card.bcAmount.label': '总额',
  'pages.create.sys.billing.card.bcAmount.tip': '总额',
  'pages.create.sys.billing.card.bcAmount.placeholder': '总额',
  
  'pages.create.sys.billing.card.bcTotalTime.label': '总时间',
  'pages.create.sys.billing.card.bcTotalTime.tip': '总时间',
  'pages.create.sys.billing.card.bcTotalTime.placeholder': '总时间',
  
  'pages.create.sys.billing.card.bcType.label': '类型',
  'pages.create.sys.billing.card.bcType.tip': '类型',
  'pages.create.sys.billing.card.bcType.placeholder': '类型',
  
  'pages.create.sys.billing.card.bcAccount.label': '绑定帐号',
  'pages.create.sys.billing.card.bcAccount.tip': '绑定帐号',
  'pages.create.sys.billing.card.bcAccount.placeholder': '绑定帐号',
  
  'pages.create.sys.billing.card.bcPassword.label': '绑定密码',
  'pages.create.sys.billing.card.bcPassword.tip': '绑定密码',
  'pages.create.sys.billing.card.bcPassword.placeholder': '绑定密码',
  
  'pages.create.sys.billing.card.bcState.label': '状态',
  'pages.create.sys.billing.card.bcState.tip': '状态',
  'pages.create.sys.billing.card.bcState.placeholder': '状态',
  
  'pages.create.sys.billing.card.bcBiggestGeneCount.label': '最大指纹数',
  'pages.create.sys.billing.card.bcBiggestGeneCount.tip': '最大指纹数',
  'pages.create.sys.billing.card.bcBiggestGeneCount.placeholder': '最大指纹数',
  
  'pages.create.sys.billing.card.bcCreateDate.label': '创建日期',
  'pages.create.sys.billing.card.bcCreateDate.tip': '创建日期',
  'pages.create.sys.billing.card.bcCreateDate.placeholder': '创建日期',
  

  'pages.update.sys.billing.card.form.title': 'Update',
  
  'pages.update.sys.billing.card.bcId.label': 'id',
  'pages.update.sys.billing.card.bcId.tip': 'id',
  'pages.update.sys.billing.card.bcId.placeholder': 'id',
  
  'pages.update.sys.billing.card.bcSerialNumber.label': '序列号信息',
  'pages.update.sys.billing.card.bcSerialNumber.tip': '序列号信息',
  'pages.update.sys.billing.card.bcSerialNumber.placeholder': '序列号信息',
  
  'pages.update.sys.billing.card.bcAmount.label': '总额',
  'pages.update.sys.billing.card.bcAmount.tip': '总额',
  'pages.update.sys.billing.card.bcAmount.placeholder': '总额',
  
  'pages.update.sys.billing.card.bcTotalTime.label': '总时间',
  'pages.update.sys.billing.card.bcTotalTime.tip': '总时间',
  'pages.update.sys.billing.card.bcTotalTime.placeholder': '总时间',
  
  'pages.update.sys.billing.card.bcType.label': '类型',
  'pages.update.sys.billing.card.bcType.tip': '类型',
  'pages.update.sys.billing.card.bcType.placeholder': '类型',
  
  'pages.update.sys.billing.card.bcAccount.label': '绑定帐号',
  'pages.update.sys.billing.card.bcAccount.tip': '绑定帐号',
  'pages.update.sys.billing.card.bcAccount.placeholder': '绑定帐号',
  
  'pages.update.sys.billing.card.bcPassword.label': '绑定密码',
  'pages.update.sys.billing.card.bcPassword.tip': '绑定密码',
  'pages.update.sys.billing.card.bcPassword.placeholder': '绑定密码',
  
  'pages.update.sys.billing.card.bcState.label': '状态',
  'pages.update.sys.billing.card.bcState.tip': '状态',
  'pages.update.sys.billing.card.bcState.placeholder': '状态',
  
  'pages.update.sys.billing.card.bcBiggestGeneCount.label': '最大指纹数',
  'pages.update.sys.billing.card.bcBiggestGeneCount.tip': '最大指纹数',
  'pages.update.sys.billing.card.bcBiggestGeneCount.placeholder': '最大指纹数',
  
  'pages.update.sys.billing.card.bcCreateDate.label': '创建日期',
  'pages.update.sys.billing.card.bcCreateDate.tip': '创建日期',
  'pages.update.sys.billing.card.bcCreateDate.placeholder': '创建日期',
  

  'pages.list.sys.billing.card.form.title': 'Query',
  
  'pages.list.sys.billing.card.bcId.label': 'id',
  'pages.list.sys.billing.card.bcId.tip': 'id',
  'pages.list.sys.billing.card.bcId.placeholder': 'id',
  
  'pages.list.sys.billing.card.bcSerialNumber.label': '序列号信息',
  'pages.list.sys.billing.card.bcSerialNumber.tip': '序列号信息',
  'pages.list.sys.billing.card.bcSerialNumber.placeholder': '序列号信息',
  
  'pages.list.sys.billing.card.bcAmount.label': '总额',
  'pages.list.sys.billing.card.bcAmount.tip': '总额',
  'pages.list.sys.billing.card.bcAmount.placeholder': '总额',
  
  'pages.list.sys.billing.card.bcTotalTime.label': '总时间',
  'pages.list.sys.billing.card.bcTotalTime.tip': '总时间',
  'pages.list.sys.billing.card.bcTotalTime.placeholder': '总时间',
  
  'pages.list.sys.billing.card.bcType.label': '类型',
  'pages.list.sys.billing.card.bcType.tip': '类型',
  'pages.list.sys.billing.card.bcType.placeholder': '类型',
  
  'pages.list.sys.billing.card.bcAccount.label': '绑定帐号',
  'pages.list.sys.billing.card.bcAccount.tip': '绑定帐号',
  'pages.list.sys.billing.card.bcAccount.placeholder': '绑定帐号',
  
  'pages.list.sys.billing.card.bcPassword.label': '绑定密码',
  'pages.list.sys.billing.card.bcPassword.tip': '绑定密码',
  'pages.list.sys.billing.card.bcPassword.placeholder': '绑定密码',
  
  'pages.list.sys.billing.card.bcState.label': '状态',
  'pages.list.sys.billing.card.bcState.tip': '状态',
  'pages.list.sys.billing.card.bcState.placeholder': '状态',
  
  'pages.list.sys.billing.card.bcBiggestGeneCount.label': '最大指纹数',
  'pages.list.sys.billing.card.bcBiggestGeneCount.tip': '最大指纹数',
  'pages.list.sys.billing.card.bcBiggestGeneCount.placeholder': '最大指纹数',
  
  'pages.list.sys.billing.card.bcCreateDate.label': '创建日期',
  'pages.list.sys.billing.card.bcCreateDate.tip': '创建日期',
  'pages.list.sys.billing.card.bcCreateDate.placeholder': '创建日期',
  

  'pages.list.sys.billing.card.option.delete':'Delete',
  'pages.list.sys.billing.card.batch.delete':'Batch delete',
  'pages.list.sys.billing.card.item':'item',
  'pages.list.sys.billing.card.chosen':'Has chosen',
  'pages.list.sys.billing.card.new':'Create',
  'pages.list.sys.billing.card.title':'Query',
  'pages.list.sys.billing.card.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.billing.card.option.edit':'Edit',
  'pages.list.sys.billing.card.option.title':'Option',
  'pages.list.sys.billing.card.index.label':'No.',
};