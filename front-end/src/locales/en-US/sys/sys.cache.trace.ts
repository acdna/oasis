export default {
  'pages.create.sys.cache.trace.form.title': 'Create',
  
  'pages.create.sys.cache.trace.ctId.label': '缓存记录ID',
  'pages.create.sys.cache.trace.ctId.tip': '缓存记录ID',
  'pages.create.sys.cache.trace.ctId.placeholder': '缓存记录ID',
  
  'pages.create.sys.cache.trace.ctCode.label': '缓存类型代码',
  'pages.create.sys.cache.trace.ctCode.tip': '缓存类型代码',
  'pages.create.sys.cache.trace.ctCode.placeholder': '缓存类型代码',
  
  'pages.create.sys.cache.trace.ctOperate.label': '缓存操作类型代码',
  'pages.create.sys.cache.trace.ctOperate.tip': '缓存操作类型代码',
  'pages.create.sys.cache.trace.ctOperate.placeholder': '缓存操作类型代码',
  
  'pages.create.sys.cache.trace.ctTargetId.label': '缓存需要操作的目标记录ID',
  'pages.create.sys.cache.trace.ctTargetId.tip': '缓存需要操作的目标记录ID',
  'pages.create.sys.cache.trace.ctTargetId.placeholder': '缓存需要操作的目标记录ID',
  
  'pages.create.sys.cache.trace.ctCreateDate.label': '创建日期',
  'pages.create.sys.cache.trace.ctCreateDate.tip': '创建日期',
  'pages.create.sys.cache.trace.ctCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.cache.trace.ctComment.label': '缓存备注信息',
  'pages.create.sys.cache.trace.ctComment.tip': '缓存备注信息',
  'pages.create.sys.cache.trace.ctComment.placeholder': '缓存备注信息',
  

  'pages.update.sys.cache.trace.form.title': 'Update',
  
  'pages.update.sys.cache.trace.ctId.label': '缓存记录ID',
  'pages.update.sys.cache.trace.ctId.tip': '缓存记录ID',
  'pages.update.sys.cache.trace.ctId.placeholder': '缓存记录ID',
  
  'pages.update.sys.cache.trace.ctCode.label': '缓存类型代码',
  'pages.update.sys.cache.trace.ctCode.tip': '缓存类型代码',
  'pages.update.sys.cache.trace.ctCode.placeholder': '缓存类型代码',
  
  'pages.update.sys.cache.trace.ctOperate.label': '缓存操作类型代码',
  'pages.update.sys.cache.trace.ctOperate.tip': '缓存操作类型代码',
  'pages.update.sys.cache.trace.ctOperate.placeholder': '缓存操作类型代码',
  
  'pages.update.sys.cache.trace.ctTargetId.label': '缓存需要操作的目标记录ID',
  'pages.update.sys.cache.trace.ctTargetId.tip': '缓存需要操作的目标记录ID',
  'pages.update.sys.cache.trace.ctTargetId.placeholder': '缓存需要操作的目标记录ID',
  
  'pages.update.sys.cache.trace.ctCreateDate.label': '创建日期',
  'pages.update.sys.cache.trace.ctCreateDate.tip': '创建日期',
  'pages.update.sys.cache.trace.ctCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.cache.trace.ctComment.label': '缓存备注信息',
  'pages.update.sys.cache.trace.ctComment.tip': '缓存备注信息',
  'pages.update.sys.cache.trace.ctComment.placeholder': '缓存备注信息',
  

  'pages.list.sys.cache.trace.form.title': 'Query',
  
  'pages.list.sys.cache.trace.ctId.label': '缓存记录ID',
  'pages.list.sys.cache.trace.ctId.tip': '缓存记录ID',
  'pages.list.sys.cache.trace.ctId.placeholder': '缓存记录ID',
  
  'pages.list.sys.cache.trace.ctCode.label': '缓存类型代码',
  'pages.list.sys.cache.trace.ctCode.tip': '缓存类型代码',
  'pages.list.sys.cache.trace.ctCode.placeholder': '缓存类型代码',
  
  'pages.list.sys.cache.trace.ctOperate.label': '缓存操作类型代码',
  'pages.list.sys.cache.trace.ctOperate.tip': '缓存操作类型代码',
  'pages.list.sys.cache.trace.ctOperate.placeholder': '缓存操作类型代码',
  
  'pages.list.sys.cache.trace.ctTargetId.label': '缓存需要操作的目标记录ID',
  'pages.list.sys.cache.trace.ctTargetId.tip': '缓存需要操作的目标记录ID',
  'pages.list.sys.cache.trace.ctTargetId.placeholder': '缓存需要操作的目标记录ID',
  
  'pages.list.sys.cache.trace.ctCreateDate.label': '创建日期',
  'pages.list.sys.cache.trace.ctCreateDate.tip': '创建日期',
  'pages.list.sys.cache.trace.ctCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.cache.trace.ctComment.label': '缓存备注信息',
  'pages.list.sys.cache.trace.ctComment.tip': '缓存备注信息',
  'pages.list.sys.cache.trace.ctComment.placeholder': '缓存备注信息',
  

  'pages.list.sys.cache.trace.option.delete':'Delete',
  'pages.list.sys.cache.trace.batch.delete':'Batch delete',
  'pages.list.sys.cache.trace.item':'item',
  'pages.list.sys.cache.trace.chosen':'Has chosen',
  'pages.list.sys.cache.trace.new':'Create',
  'pages.list.sys.cache.trace.title':'Query',
  'pages.list.sys.cache.trace.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.cache.trace.option.edit':'Edit',
  'pages.list.sys.cache.trace.option.title':'Option',
  'pages.list.sys.cache.trace.index.label':'No.',
};