export default {
  'pages.create.sys.col.detail.form.title': 'Create',
  
  'pages.create.sys.col.detail.cdId.label': '主键ID',
  'pages.create.sys.col.detail.cdId.tip': '主键ID',
  'pages.create.sys.col.detail.cdId.placeholder': '主键ID',
  
  'pages.create.sys.col.detail.cdTableName.label': '表名',
  'pages.create.sys.col.detail.cdTableName.tip': '表名',
  'pages.create.sys.col.detail.cdTableName.placeholder': '表名',
  
  'pages.create.sys.col.detail.cdColName.label': '列名',
  'pages.create.sys.col.detail.cdColName.tip': '列名',
  'pages.create.sys.col.detail.cdColName.placeholder': '列名',
  
  'pages.create.sys.col.detail.cdDescName.label': '列中文名',
  'pages.create.sys.col.detail.cdDescName.tip': '列中文名',
  'pages.create.sys.col.detail.cdDescName.placeholder': '列中文名',
  
  'pages.create.sys.col.detail.cdDictName.label': '字典名',
  'pages.create.sys.col.detail.cdDictName.tip': '字典名',
  'pages.create.sys.col.detail.cdDictName.placeholder': '字典名',
  
  'pages.create.sys.col.detail.cdMode.label': '模式',
  'pages.create.sys.col.detail.cdMode.tip': '模式',
  'pages.create.sys.col.detail.cdMode.placeholder': '模式',
  
  'pages.create.sys.col.detail.cdCreateDate.label': '创建日期',
  'pages.create.sys.col.detail.cdCreateDate.tip': '创建日期',
  'pages.create.sys.col.detail.cdCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.col.detail.cdUpdateDate.label': '更新日期',
  'pages.create.sys.col.detail.cdUpdateDate.tip': '更新日期',
  'pages.create.sys.col.detail.cdUpdateDate.placeholder': '更新日期',
  

  'pages.update.sys.col.detail.form.title': 'Update',
  
  'pages.update.sys.col.detail.cdId.label': '主键ID',
  'pages.update.sys.col.detail.cdId.tip': '主键ID',
  'pages.update.sys.col.detail.cdId.placeholder': '主键ID',
  
  'pages.update.sys.col.detail.cdTableName.label': '表名',
  'pages.update.sys.col.detail.cdTableName.tip': '表名',
  'pages.update.sys.col.detail.cdTableName.placeholder': '表名',
  
  'pages.update.sys.col.detail.cdColName.label': '列名',
  'pages.update.sys.col.detail.cdColName.tip': '列名',
  'pages.update.sys.col.detail.cdColName.placeholder': '列名',
  
  'pages.update.sys.col.detail.cdDescName.label': '列中文名',
  'pages.update.sys.col.detail.cdDescName.tip': '列中文名',
  'pages.update.sys.col.detail.cdDescName.placeholder': '列中文名',
  
  'pages.update.sys.col.detail.cdDictName.label': '字典名',
  'pages.update.sys.col.detail.cdDictName.tip': '字典名',
  'pages.update.sys.col.detail.cdDictName.placeholder': '字典名',
  
  'pages.update.sys.col.detail.cdMode.label': '模式',
  'pages.update.sys.col.detail.cdMode.tip': '模式',
  'pages.update.sys.col.detail.cdMode.placeholder': '模式',
  
  'pages.update.sys.col.detail.cdCreateDate.label': '创建日期',
  'pages.update.sys.col.detail.cdCreateDate.tip': '创建日期',
  'pages.update.sys.col.detail.cdCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.col.detail.cdUpdateDate.label': '更新日期',
  'pages.update.sys.col.detail.cdUpdateDate.tip': '更新日期',
  'pages.update.sys.col.detail.cdUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.col.detail.form.title': 'Query',
  
  'pages.list.sys.col.detail.cdId.label': '主键ID',
  'pages.list.sys.col.detail.cdId.tip': '主键ID',
  'pages.list.sys.col.detail.cdId.placeholder': '主键ID',
  
  'pages.list.sys.col.detail.cdTableName.label': '表名',
  'pages.list.sys.col.detail.cdTableName.tip': '表名',
  'pages.list.sys.col.detail.cdTableName.placeholder': '表名',
  
  'pages.list.sys.col.detail.cdColName.label': '列名',
  'pages.list.sys.col.detail.cdColName.tip': '列名',
  'pages.list.sys.col.detail.cdColName.placeholder': '列名',
  
  'pages.list.sys.col.detail.cdDescName.label': '列中文名',
  'pages.list.sys.col.detail.cdDescName.tip': '列中文名',
  'pages.list.sys.col.detail.cdDescName.placeholder': '列中文名',
  
  'pages.list.sys.col.detail.cdDictName.label': '字典名',
  'pages.list.sys.col.detail.cdDictName.tip': '字典名',
  'pages.list.sys.col.detail.cdDictName.placeholder': '字典名',
  
  'pages.list.sys.col.detail.cdMode.label': '模式',
  'pages.list.sys.col.detail.cdMode.tip': '模式',
  'pages.list.sys.col.detail.cdMode.placeholder': '模式',
  
  'pages.list.sys.col.detail.cdCreateDate.label': '创建日期',
  'pages.list.sys.col.detail.cdCreateDate.tip': '创建日期',
  'pages.list.sys.col.detail.cdCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.col.detail.cdUpdateDate.label': '更新日期',
  'pages.list.sys.col.detail.cdUpdateDate.tip': '更新日期',
  'pages.list.sys.col.detail.cdUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.col.detail.option.delete':'Delete',
  'pages.list.sys.col.detail.batch.delete':'Batch delete',
  'pages.list.sys.col.detail.item':'item',
  'pages.list.sys.col.detail.chosen':'Has chosen',
  'pages.list.sys.col.detail.new':'Create',
  'pages.list.sys.col.detail.title':'Query',
  'pages.list.sys.col.detail.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.col.detail.option.edit':'Edit',
  'pages.list.sys.col.detail.option.title':'Option',
  'pages.list.sys.col.detail.index.label':'No.',
};