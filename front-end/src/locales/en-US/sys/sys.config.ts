export default {
  'pages.create.sys.config.form.title': 'Create',
  'pages.create.sys.config.form.item.title': 'Create Item',
  'pages.update.sys.config.form.item.title': 'Update Item',
  'pages.create.sys.config.form.cancel': 'Cancel',
  'pages.create.sys.config.form.submit': 'Submit',
  'pages.create.sys.config.form.item.cancel': 'Cancel',
  'pages.create.sys.config.form.item.submit': 'Submit',
  'pages.update.sys.config.form.cancel': 'Cancel',
  'pages.update.sys.config.form.submit': 'Submit',
  'pages.update.sys.config.form.item.cancel': 'Cancel',
  'pages.update.sys.config.form.item.submit': 'Submit',

  'pages.create.sys.config.conId.label': 'ID',
  'pages.create.sys.config.conId.tip': 'ID',
  'pages.create.sys.config.conId.placeholder': 'ID',

  'pages.create.sys.config.conName.label': 'Name',
  'pages.create.sys.config.conName.tip': 'Name',
  'pages.create.sys.config.conName.placeholder': 'Name',

  'pages.create.sys.config.conNameEn.label': 'NameEn',
  'pages.create.sys.config.conNameEn.tip': 'NameEn',
  'pages.create.sys.config.conNameEn.placeholder': 'NameEn',

  'pages.create.sys.config.conValue.label': 'Value',
  'pages.create.sys.config.conValue.tip': 'Value',
  'pages.create.sys.config.conValue.placeholder': 'Value',

  'pages.create.sys.config.conGroup.label': 'Group',
  'pages.create.sys.config.conGroup.tip': 'Group',
  'pages.create.sys.config.conGroup.placeholder': 'Group',

  'pages.create.sys.config.conParentId.label': '父级ID',
  'pages.create.sys.config.conParentId.tip': '父级ID',
  'pages.create.sys.config.conParentId.placeholder': '父级ID',

  'pages.create.sys.config.conType.label': 'Type',
  'pages.create.sys.config.conType.tip': 'Type',
  'pages.create.sys.config.conType.placeholder': 'Type',

  'pages.create.sys.config.conParamType.label': 'ParamType',
  'pages.create.sys.config.conParamType.tip': 'ParamType',
  'pages.create.sys.config.conParamType.placeholder': 'ParamType',

  'pages.create.sys.config.conCreateDate.label': 'CreateDate',
  'pages.create.sys.config.conCreateDate.tip': 'CreateDate',
  'pages.create.sys.config.conCreateDate.placeholder': 'CreateDate',

  'pages.create.sys.config.conUpdateDate.label': 'UpdateDate',
  'pages.create.sys.config.conUpdateDate.tip': 'UpdateDate',
  'pages.create.sys.config.conUpdateDate.placeholder': 'UpdateDate',

  'pages.create.sys.config.conManager.label': 'Manager',
  'pages.create.sys.config.conManager.tip': 'Manager',
  'pages.create.sys.config.conManager.placeholder': 'Manager',

  'pages.create.sys.config.conSpecies.label': 'Species',
  'pages.create.sys.config.conSpecies.tip': 'Species',
  'pages.create.sys.config.conSpecies.placeholder': 'Species',

  'pages.create.sys.config.conOrder.label': 'Order',
  'pages.create.sys.config.conOrder.tip': 'Order',
  'pages.create.sys.config.conOrder.placeholder': 'Order',

  'pages.create.sys.config.conState.label': 'State',
  'pages.create.sys.config.conState.tip': 'State',
  'pages.create.sys.config.conState.placeholder': 'State',

  'pages.create.sys.config.conRemark.label': 'Remark',
  'pages.create.sys.config.conRemark.tip': 'Remark',
  'pages.create.sys.config.conRemark.placeholder': 'Remark',


  'pages.update.sys.config.form.title': 'Update',

  'pages.update.sys.config.conId.label': 'ID',
  'pages.update.sys.config.conId.tip': 'ID',
  'pages.update.sys.config.conId.placeholder': 'ID',

  'pages.update.sys.config.conName.label': 'Name',
  'pages.update.sys.config.conName.tip': 'Name',
  'pages.update.sys.config.conName.placeholder': 'Name',

  'pages.update.sys.config.conNameEn.label': 'NameEn',
  'pages.update.sys.config.conNameEn.tip': 'NameEn',
  'pages.update.sys.config.conNameEn.placeholder': 'NameEn',

  'pages.update.sys.config.conValue.label': 'Value',
  'pages.update.sys.config.conValue.tip': 'Value',
  'pages.update.sys.config.conValue.placeholder': 'Value',

  'pages.update.sys.config.conGroup.label': 'Group',
  'pages.update.sys.config.conGroup.tip': 'Group',
  'pages.update.sys.config.conGroup.placeholder': 'Group',

  'pages.update.sys.config.conParentId.label': '父级ID',
  'pages.update.sys.config.conParentId.tip': '父级ID',
  'pages.update.sys.config.conParentId.placeholder': '父级ID',

  'pages.update.sys.config.conType.label': 'Type',
  'pages.update.sys.config.conType.tip': 'Type',
  'pages.update.sys.config.conType.placeholder': 'Type',

  'pages.update.sys.config.conParamType.label': 'ParamType',
  'pages.update.sys.config.conParamType.tip': 'ParamType',
  'pages.update.sys.config.conParamType.placeholder': 'ParamType',

  'pages.update.sys.config.conCreateDate.label': 'CreateDate',
  'pages.update.sys.config.conCreateDate.tip': 'CreateDate',
  'pages.update.sys.config.conCreateDate.placeholder': 'CreateDate',

  'pages.update.sys.config.conUpdateDate.label': 'UpdateDate',
  'pages.update.sys.config.conUpdateDate.tip': 'UpdateDate',
  'pages.update.sys.config.conUpdateDate.placeholder': 'UpdateDate',

  'pages.update.sys.config.conManager.label': 'Manager',
  'pages.update.sys.config.conManager.tip': 'Manager',
  'pages.update.sys.config.conManager.placeholder': 'Manager',

  'pages.update.sys.config.conSpecies.label': 'Species',
  'pages.update.sys.config.conSpecies.tip': 'Species',
  'pages.update.sys.config.conSpecies.placeholder': 'Species',

  'pages.update.sys.config.conOrder.label': 'Order',
  'pages.update.sys.config.conOrder.tip': 'Order',
  'pages.update.sys.config.conOrder.placeholder': 'Order',

  'pages.update.sys.config.conState.label': 'State',
  'pages.update.sys.config.conState.tip': 'State',
  'pages.update.sys.config.conState.placeholder': 'State',

  'pages.update.sys.config.conRemark.label': 'Remark',
  'pages.update.sys.config.conRemark.tip': 'Remark',
  'pages.update.sys.config.conRemark.placeholder': 'Remark',


  'pages.list.sys.config.form.title': 'Query',

  'pages.list.sys.config.conId.label': 'ID',
  'pages.list.sys.config.conId.tip': 'ID',
  'pages.list.sys.config.conId.placeholder': 'ID',

  'pages.list.sys.config.conName.label': 'Name',
  'pages.list.sys.config.conName.tip': 'Name',
  'pages.list.sys.config.conName.placeholder': 'Name',

  'pages.list.sys.config.conNameLike.label': 'Name',
  'pages.list.sys.config.conNameLike.tip': 'Name',
  'pages.list.sys.config.conNameLike.placeholder': 'Name',

  'pages.list.sys.config.conNameEn.label': 'NameEn',
  'pages.list.sys.config.conNameEn.tip': 'NameEn',
  'pages.list.sys.config.conNameEn.placeholder': 'NameEn',

  'pages.list.sys.config.conValue.label': 'Value',
  'pages.list.sys.config.conValue.tip': 'Value',
  'pages.list.sys.config.conValue.placeholder': 'Value',

  'pages.list.sys.config.conGroup.label': 'Group',
  'pages.list.sys.config.conGroup.tip': 'Group',
  'pages.list.sys.config.conGroup.placeholder': 'Group',

  'pages.list.sys.config.conParentId.label': '父级ID',
  'pages.list.sys.config.conParentId.tip': '父级ID',
  'pages.list.sys.config.conParentId.placeholder': '父级ID',

  'pages.list.sys.config.conType.label': 'Type',
  'pages.list.sys.config.conType.tip': 'Type',
  'pages.list.sys.config.conType.placeholder': 'Type',

  'pages.list.sys.config.conParamType.label': 'ParamType',
  'pages.list.sys.config.conParamType.tip': 'ParamType',
  'pages.list.sys.config.conParamType.placeholder': 'ParamType',

  'pages.list.sys.config.conCreateDate.label': 'CreateDate',
  'pages.list.sys.config.conCreateDate.tip': 'CreateDate',
  'pages.list.sys.config.conCreateDate.placeholder': 'CreateDate',

  'pages.list.sys.config.conUpdateDate.label': 'UpdateDate',
  'pages.list.sys.config.conUpdateDate.tip': 'UpdateDate',
  'pages.list.sys.config.conUpdateDate.placeholder': 'UpdateDate',

  'pages.list.sys.config.conManager.label': 'Manager',
  'pages.list.sys.config.conManager.tip': 'Manager',
  'pages.list.sys.config.conManager.placeholder': 'Manager',

  'pages.list.sys.config.conSpecies.label': 'Species',
  'pages.list.sys.config.conSpecies.tip': 'Species',
  'pages.list.sys.config.conSpecies.placeholder': 'Species',

  'pages.list.sys.config.conOrder.label': 'Order',
  'pages.list.sys.config.conOrder.tip': 'Order',
  'pages.list.sys.config.conOrder.placeholder': 'Order',

  'pages.list.sys.config.conState.label': 'State',
  'pages.list.sys.config.conState.tip': 'State',
  'pages.list.sys.config.conState.placeholder': 'State',

  'pages.list.sys.config.conRemark.label': 'Remark',
  'pages.list.sys.config.conRemark.tip': 'Remark',
  'pages.list.sys.config.conRemark.placeholder': 'Remark',


  'pages.list.sys.config.option.delete':'Delete',
  'pages.list.sys.config.batch.delete':'Batch delete',
  'pages.list.sys.config.item':'item',
  'pages.list.sys.config.chosen':'Has chosen',
  'pages.list.sys.config.new':'Create',
  'pages.list.sys.config.title':'Query',
  'pages.list.sys.config.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.config.option.edit':'Edit',
  'pages.list.sys.config.option.title':'Option',
  'pages.list.sys.config.index.label':'No.',
  'pages.list.sys.config.option.init.front.confirm':'The existing parameters will be deleted before initialization. Are you sure you want to continue?',
  'pages.list.sys.config.init':'Init Configs',
};
