export default {

  'pages.list.sys.permission.option.init.confirm.front': 'The existing front-end permissions will be deleted before initialization. Are you sure you want to continue?',
  'pages.list.sys.permission.option.init.confirm.back': 'The existing back-end permissions will be deleted before initialization. Are you sure you want to continue?',
  'pages.create.sys.permission.item.title': 'Create Item',
  'pages.update.sys.permission.item.title': 'Update Item',
  'pages.create.sys.permission.title': 'Create',
  'pages.update.sys.permission.title': 'Update',
  'pages.create.sys.permission.form.title': 'Create',
  'pages.create.sys.permission.cancel': 'Cancel',
  'pages.update.sys.permission.cancel': 'Cancel',
  'pages.create.sys.permission.submit': 'Submit',
  'pages.update.sys.permission.submit': 'Submit',

  'pages.create.sys.permission.perId.label': 'ID',
  'pages.create.sys.permission.perId.tip': 'ID',
  'pages.create.sys.permission.perId.placeholder': 'ID',

  'pages.create.sys.permission.perName.label': 'Name',
  'pages.create.sys.permission.perName.tip': 'Name',
  'pages.create.sys.permission.perName.placeholder': 'Name',

  'pages.create.sys.permission.perNameEn.label': 'NameEn',
  'pages.create.sys.permission.perNameEn.tip': 'NameEn',
  'pages.create.sys.permission.perNameEn.placeholder': 'NameEn',

  'pages.create.sys.permission.perType.label': 'Type',
  'pages.create.sys.permission.perType.tip': 'Type',
  'pages.create.sys.permission.perType.placeholder': 'Type',

  'pages.create.sys.permission.perUrl.label': 'URL',
  'pages.create.sys.permission.perUrl.tip': 'URL',
  'pages.create.sys.permission.perUrl.placeholder': 'URL',

  'pages.create.sys.permission.perMethod.label': 'Method',
  'pages.create.sys.permission.perMethod.tip': 'Method',
  'pages.create.sys.permission.perMethod.placeholder': 'Method',

  'pages.create.sys.permission.perParentId.label': 'ParentId',
  'pages.create.sys.permission.perParentId.tip': 'ParentId',
  'pages.create.sys.permission.perParentId.placeholder': 'ParentId',

  'pages.create.sys.permission.perOrder.label': 'Order',
  'pages.create.sys.permission.perOrder.tip': 'Order',
  'pages.create.sys.permission.perOrder.placeholder': 'Order',

  'pages.create.sys.permission.perRemark.label': 'Remark',
  'pages.create.sys.permission.perRemark.tip': 'Remark',
  'pages.create.sys.permission.perRemark.placeholder': 'Remark',

  'pages.create.sys.permission.perState.label': 'State',
  'pages.create.sys.permission.perState.tip': 'State',
  'pages.create.sys.permission.perState.placeholder': 'State',

  'pages.create.sys.permission.perSystem.label': 'System',
  'pages.create.sys.permission.perSystem.tip': 'System',
  'pages.create.sys.permission.perSystem.placeholder': 'System',

  'pages.create.sys.permission.perModule.label': 'Module',
  'pages.create.sys.permission.perModule.tip': 'Module',
  'pages.create.sys.permission.perModule.placeholder': 'Module',


  'pages.update.sys.permission.form.title': 'Update',

  'pages.update.sys.permission.perId.label': 'ID',
  'pages.update.sys.permission.perId.tip': 'ID',
  'pages.update.sys.permission.perId.placeholder': 'ID',

  'pages.update.sys.permission.perName.label': 'Name',
  'pages.update.sys.permission.perName.tip': 'Name',
  'pages.update.sys.permission.perName.placeholder': 'Name',

  'pages.update.sys.permission.perNameEn.label': 'NameEn',
  'pages.update.sys.permission.perNameEn.tip': 'NameEn',
  'pages.update.sys.permission.perNameEn.placeholder': 'NameEn',

  'pages.update.sys.permission.perType.label': 'Type',
  'pages.update.sys.permission.perType.tip': 'Type',
  'pages.update.sys.permission.perType.placeholder': 'Type',

  'pages.update.sys.permission.perUrl.label': 'URL',
  'pages.update.sys.permission.perUrl.tip': 'URL',
  'pages.update.sys.permission.perUrl.placeholder': 'URL',

  'pages.update.sys.permission.perMethod.label': 'Method',
  'pages.update.sys.permission.perMethod.tip': 'Method',
  'pages.update.sys.permission.perMethod.placeholder': 'Method',

  'pages.update.sys.permission.perParentId.label': 'ParentId',
  'pages.update.sys.permission.perParentId.tip': 'ParentId',
  'pages.update.sys.permission.perParentId.placeholder': 'ParentId',

  'pages.update.sys.permission.perOrder.label': 'Order',
  'pages.update.sys.permission.perOrder.tip': 'Order',
  'pages.update.sys.permission.perOrder.placeholder': 'Order',

  'pages.update.sys.permission.perRemark.label': 'Remark',
  'pages.update.sys.permission.perRemark.tip': 'Remark',
  'pages.update.sys.permission.perRemark.placeholder': 'Remark',

  'pages.update.sys.permission.perState.label': 'State',
  'pages.update.sys.permission.perState.tip': 'State',
  'pages.update.sys.permission.perState.placeholder': 'State',

  'pages.update.sys.permission.perSystem.label': 'System',
  'pages.update.sys.permission.perSystem.tip': 'System',
  'pages.update.sys.permission.perSystem.placeholder': 'System',

  'pages.update.sys.permission.perModule.label': 'Module',
  'pages.update.sys.permission.perModule.tip': 'Module',
  'pages.update.sys.permission.perModule.placeholder': 'Module',


  'pages.list.sys.permission.form.title': 'Query',

  'pages.list.sys.permission.perId.label': 'ID',
  'pages.list.sys.permission.perId.tip': 'ID',
  'pages.list.sys.permission.perId.placeholder': 'ID',

  'pages.list.sys.permission.perName.label': 'Name',
  'pages.list.sys.permission.perName.tip': 'Name',
  'pages.list.sys.permission.perName.placeholder': 'Name',

  'pages.list.sys.permission.perNameEn.label': 'NameEn',
  'pages.list.sys.permission.perNameEn.tip': 'NameEn',
  'pages.list.sys.permission.perNameEn.placeholder': 'NameEn',

  'pages.list.sys.permission.perType.label': 'Type',
  'pages.list.sys.permission.perType.tip': 'Type',
  'pages.list.sys.permission.perType.placeholder': 'Type',

  'pages.list.sys.permission.perUrl.label': 'URL',
  'pages.list.sys.permission.perUrl.tip': 'URL',
  'pages.list.sys.permission.perUrl.placeholder': 'URL',

  'pages.list.sys.permission.perMethod.label': 'Method',
  'pages.list.sys.permission.perMethod.tip': 'Method',
  'pages.list.sys.permission.perMethod.placeholder': 'Method',

  'pages.list.sys.permission.perParentId.label': 'ParentId',
  'pages.list.sys.permission.perParentId.tip': 'ParentId',
  'pages.list.sys.permission.perParentId.placeholder': 'ParentId',

  'pages.list.sys.permission.perOrder.label': 'Order',
  'pages.list.sys.permission.perOrder.tip': 'Order',
  'pages.list.sys.permission.perOrder.placeholder': 'Order',

  'pages.list.sys.permission.perRemark.label': 'Remark',
  'pages.list.sys.permission.perRemark.tip': 'Remark',
  'pages.list.sys.permission.perRemark.placeholder': 'Remark',

  'pages.list.sys.permission.perState.label': 'State',
  'pages.list.sys.permission.perState.tip': 'State',
  'pages.list.sys.permission.perState.placeholder': 'State',

  'pages.list.sys.permission.perSystem.label': 'System',
  'pages.list.sys.permission.perSystem.tip': 'System',
  'pages.list.sys.permission.perSystem.placeholder': 'System',

  'pages.list.sys.permission.perSystemLike.label': 'System',
  'pages.list.sys.permission.perSystemLike.tip': 'System Fuzzy query',
  'pages.list.sys.permission.perSystemLike.placeholder': 'System Fuzzy query',

  'pages.list.sys.permission.perModule.label': 'Module',
  'pages.list.sys.permission.perModule.tip': 'Module',
  'pages.list.sys.permission.perModule.placeholder': 'Module',

  'pages.list.sys.permission.perModuleLike.label': 'Module',
  'pages.list.sys.permission.perModuleLike.tip': 'Fuzzy query Module',
  'pages.list.sys.permission.perModuleLike.placeholder': 'Fuzzy query Module',

  'pages.list.sys.permission.init.back':'Init Back-End Pms',
  'pages.list.sys.permission.init.front':'Init Front-End Pms',
  'pages.list.sys.permission.option.create':'Create',
  'pages.list.sys.permission.option.delete':'Delete',
  'pages.list.sys.permission.batch.delete':'Batch delete',
  'pages.list.sys.permission.item':'item',
  'pages.list.sys.permission.chosen':'Has chosen',
  'pages.list.sys.permission.new':'Create',
  'pages.list.sys.permission.title':'Query',
  'pages.list.sys.permission.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.permission.option.edit':'Edit',
  'pages.list.sys.permission.option.title':'Option',
  'pages.list.sys.permission.index.label':'No.',
};
