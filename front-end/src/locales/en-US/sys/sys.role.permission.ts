export default {
  'pages.sys.role.permission.relate.title':'Permission authorization',
  'pages.sys.role.permission.relate.chosen':'Selected',
  'pages.sys.role.permission.relate.item':'Item',
  'pages.sys.role.permission.relate.cancel.all':'Deselect',
  'pages.sys.role.permission.relate.cancel':'Cancel',
  'pages.sys.role.permission.relate.submit':'Submit',
  'pages.sys.role.permission.relate.loading':'Authorizing',
  'pages.sys.role.permission.relate.success':'Authorization success',
  'pages.sys.role.permission.relate.error':'Authorization failed, please try again',

  'pages.create.sys.role.permission.form.title': 'Create',

  'pages.create.sys.role.permission.rpId.label': '角色权限表Id',
  'pages.create.sys.role.permission.rpId.tip': '角色权限表Id',
  'pages.create.sys.role.permission.rpId.placeholder': '角色权限表Id',

  'pages.create.sys.role.permission.rpRoleId.label': '角色ID号',
  'pages.create.sys.role.permission.rpRoleId.tip': '角色ID号',
  'pages.create.sys.role.permission.rpRoleId.placeholder': '角色ID号',

  'pages.create.sys.role.permission.rpPermissionId.label': '权限ID',
  'pages.create.sys.role.permission.rpPermissionId.tip': '权限ID',
  'pages.create.sys.role.permission.rpPermissionId.placeholder': '权限ID',


  'pages.update.sys.role.permission.form.title': 'Update',

  'pages.update.sys.role.permission.rpId.label': '角色权限表Id',
  'pages.update.sys.role.permission.rpId.tip': '角色权限表Id',
  'pages.update.sys.role.permission.rpId.placeholder': '角色权限表Id',

  'pages.update.sys.role.permission.rpRoleId.label': '角色ID号',
  'pages.update.sys.role.permission.rpRoleId.tip': '角色ID号',
  'pages.update.sys.role.permission.rpRoleId.placeholder': '角色ID号',

  'pages.update.sys.role.permission.rpPermissionId.label': '权限ID',
  'pages.update.sys.role.permission.rpPermissionId.tip': '权限ID',
  'pages.update.sys.role.permission.rpPermissionId.placeholder': '权限ID',


  'pages.list.sys.role.permission.form.title': 'Query',

  'pages.list.sys.role.permission.rpId.label': '角色权限表Id',
  'pages.list.sys.role.permission.rpId.tip': '角色权限表Id',
  'pages.list.sys.role.permission.rpId.placeholder': '角色权限表Id',

  'pages.list.sys.role.permission.rpRoleId.label': '角色ID号',
  'pages.list.sys.role.permission.rpRoleId.tip': '角色ID号',
  'pages.list.sys.role.permission.rpRoleId.placeholder': '角色ID号',

  'pages.list.sys.role.permission.rpPermissionId.label': '权限ID',
  'pages.list.sys.role.permission.rpPermissionId.tip': '权限ID',
  'pages.list.sys.role.permission.rpPermissionId.placeholder': '权限ID',


  'pages.list.sys.role.permission.option.delete':'Delete',
  'pages.list.sys.role.permission.batch.delete':'Batch delete',
  'pages.list.sys.role.permission.item':'item',
  'pages.list.sys.role.permission.chosen':'Has chosen',
  'pages.list.sys.role.permission.new':'Create',
  'pages.list.sys.role.permission.title':'Query',
  'pages.list.sys.role.permission.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.role.permission.option.edit':'Edit',
  'pages.list.sys.role.permission.option.title':'Option',
  'pages.list.sys.role.permission.index.label':'No.',
};
