export default {
  'pages.create.sys.contact.form.title': 'Create',

  'pages.create.sys.contact.contactId.label': 'ID',
  'pages.create.sys.contact.contactId.tip': 'ID',
  'pages.create.sys.contact.contactId.placeholder': 'ID',

  'pages.create.sys.contact.contactAuthor.label': 'Author',
  'pages.create.sys.contact.contactAuthor.tip': 'Author',
  'pages.create.sys.contact.contactAuthor.placeholder': 'Author',

  'pages.create.sys.contact.contactEmail.label': 'Email',
  'pages.create.sys.contact.contactEmail.tip': 'Email',
  'pages.create.sys.contact.contactEmail.placeholder': 'Email',

  'pages.create.sys.contact.contactSubject.label': 'Subject',
  'pages.create.sys.contact.contactSubject.tip': 'Subject',
  'pages.create.sys.contact.contactSubject.placeholder': 'Subject',

  'pages.create.sys.contact.contactText.label': 'Content',
  'pages.create.sys.contact.contactText.tip': 'Content',
  'pages.create.sys.contact.contactText.placeholder': 'Content',

  'pages.create.sys.contact.contactCreateDate.label': 'CreateDate',
  'pages.create.sys.contact.contactCreateDate.tip': 'CreateDate',
  'pages.create.sys.contact.contactCreateDate.placeholder': 'CreateDate',


  'pages.update.sys.contact.form.title': 'Update',

  'pages.update.sys.contact.contactId.label': 'ID',
  'pages.update.sys.contact.contactId.tip': 'ID',
  'pages.update.sys.contact.contactId.placeholder': 'ID',

  'pages.update.sys.contact.contactAuthor.label': 'Author',
  'pages.update.sys.contact.contactAuthor.tip': 'Author',
  'pages.update.sys.contact.contactAuthor.placeholder': 'Author',

  'pages.update.sys.contact.contactEmail.label': 'Email',
  'pages.update.sys.contact.contactEmail.tip': 'Email',
  'pages.update.sys.contact.contactEmail.placeholder': 'Email',

  'pages.update.sys.contact.contactSubject.label': 'Subject',
  'pages.update.sys.contact.contactSubject.tip': 'Subject',
  'pages.update.sys.contact.contactSubject.placeholder': 'Subject',

  'pages.update.sys.contact.contactText.label': 'Content',
  'pages.update.sys.contact.contactText.tip': 'Content',
  'pages.update.sys.contact.contactText.placeholder': 'Content',

  'pages.update.sys.contact.contactCreateDate.label': 'CreateDate',
  'pages.update.sys.contact.contactCreateDate.tip': 'CreateDate',
  'pages.update.sys.contact.contactCreateDate.placeholder': 'CreateDate',


  'pages.list.sys.contact.form.title': 'Query',

  'pages.list.sys.contact.contactId.label': 'ID',
  'pages.list.sys.contact.contactId.tip': 'ID',
  'pages.list.sys.contact.contactId.placeholder': 'ID',

  'pages.list.sys.contact.contactAuthor.label': 'Author',
  'pages.list.sys.contact.contactAuthor.tip': 'Author',
  'pages.list.sys.contact.contactAuthor.placeholder': 'Author',

  'pages.list.sys.contact.contactEmail.label': 'Email',
  'pages.list.sys.contact.contactEmail.tip': 'Email',
  'pages.list.sys.contact.contactEmail.placeholder': 'Email',

  'pages.list.sys.contact.contactSubject.label': 'Subject',
  'pages.list.sys.contact.contactSubject.tip': 'Subject',
  'pages.list.sys.contact.contactSubject.placeholder': 'Subject',

  'pages.list.sys.contact.contactText.label': 'Content',
  'pages.list.sys.contact.contactText.tip': 'Content',
  'pages.list.sys.contact.contactText.placeholder': 'Content',

  'pages.list.sys.contact.contactCreateDate.label': 'CreateDate',
  'pages.list.sys.contact.contactCreateDate.tip': 'CreateDate',
  'pages.list.sys.contact.contactCreateDate.placeholder': 'CreateDate',


  'pages.list.sys.contact.option.delete':'Delete',
  'pages.list.sys.contact.batch.delete':'Batch delete',
  'pages.list.sys.contact.item':'item',
  'pages.list.sys.contact.chosen':'Has chosen',
  'pages.list.sys.contact.new':'Create',
  'pages.list.sys.contact.title':'Query',
  'pages.list.sys.contact.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.contact.option.edit':'Edit',
  'pages.list.sys.contact.option.title':'Option',
  'pages.list.sys.contact.index.label':'No.',
};
