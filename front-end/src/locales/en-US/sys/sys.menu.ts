export default {
  'pages.list.sys.menu.tree.tab': 'MENU TREE',
  'pages.list.sys.menu.tree.add': 'CREATE',
  'pages.list.sys.menu.tree.delete': 'DELETE',
  'pages.create.sys.menu.cancel': 'Cancel',
  'pages.create.sys.menu.submit': 'Submit',
  'pages.update.sys.menu.cancel': 'Cancel',
  'pages.update.sys.menu.submit': 'Submit',
  'pages.list.sys.menu.tree.btn.delete.confirm':'Deletion is not recoverable. Are you sure you want to delete it?',

  'pages.create.sys.menu.form.title': 'Create',
  'pages.create.sys.menu.form.item.title': 'Create Itme',

  'pages.create.sys.menu.menuId.label': '菜单ID号',
  'pages.create.sys.menu.menuId.tip': '菜单ID号',
  'pages.create.sys.menu.menuId.placeholder': '菜单ID号',

  'pages.create.sys.menu.menuName.label': 'Name',
  'pages.create.sys.menu.menuName.tip': 'Name',
  'pages.create.sys.menu.menuName.placeholder': 'Name',

  'pages.create.sys.menu.menuNameEn.label': 'NameEn',
  'pages.create.sys.menu.menuNameEn.tip': 'NameEn',
  'pages.create.sys.menu.menuNameEn.placeholder': 'NameEn',

  'pages.create.sys.menu.menuUrl.label': 'URL',
  'pages.create.sys.menu.menuUrl.tip': 'URL',
  'pages.create.sys.menu.menuUrl.placeholder': 'URL',

  'pages.create.sys.menu.menuParentId.label': '父菜单ID',
  'pages.create.sys.menu.menuParentId.tip': '父菜单ID',
  'pages.create.sys.menu.menuParentId.placeholder': '父菜单ID',

  'pages.create.sys.menu.menuOrder.label': 'Order',
  'pages.create.sys.menu.menuOrder.tip': 'Order',
  'pages.create.sys.menu.menuOrder.placeholder': 'Order',

  'pages.create.sys.menu.menuRemark.label': 'Remark',
  'pages.create.sys.menu.menuRemark.tip': 'Remark',
  'pages.create.sys.menu.menuRemark.placeholder': 'Remark',

  'pages.create.sys.menu.menuState.label': 'State',
  'pages.create.sys.menu.menuState.tip': 'State',
  'pages.create.sys.menu.menuState.placeholder': 'State',

  'pages.create.sys.menu.menuType.label': 'Type',
  'pages.create.sys.menu.menuType.tip': 'Type',
  'pages.create.sys.menu.menuType.placeholder': 'Type',

  'pages.create.sys.menu.menuIconClass.label': 'IconClass',
  'pages.create.sys.menu.menuIconClass.tip': 'IconClass',
  'pages.create.sys.menu.menuIconClass.placeholder': 'IconClass',


  'pages.update.sys.menu.form.title': 'Update',

  'pages.update.sys.menu.menuId.label': '菜单ID号',
  'pages.update.sys.menu.menuId.tip': '菜单ID号',
  'pages.update.sys.menu.menuId.placeholder': '菜单ID号',

  'pages.update.sys.menu.menuName.label': 'Name',
  'pages.update.sys.menu.menuName.tip': 'Name',
  'pages.update.sys.menu.menuName.placeholder': 'Name',

  'pages.update.sys.menu.menuNameEn.label': 'NameEn',
  'pages.update.sys.menu.menuNameEn.tip': 'NameEn',
  'pages.update.sys.menu.menuNameEn.placeholder': 'NameEn',

  'pages.update.sys.menu.menuUrl.label': 'URL',
  'pages.update.sys.menu.menuUrl.tip': 'URL',
  'pages.update.sys.menu.menuUrl.placeholder': 'URL',

  'pages.update.sys.menu.menuParentId.label': '父菜单ID',
  'pages.update.sys.menu.menuParentId.tip': '父菜单ID',
  'pages.update.sys.menu.menuParentId.placeholder': '父菜单ID',

  'pages.update.sys.menu.menuOrder.label': 'Order',
  'pages.update.sys.menu.menuOrder.tip': 'Order',
  'pages.update.sys.menu.menuOrder.placeholder': 'Order',

  'pages.update.sys.menu.menuRemark.label': 'Remark',
  'pages.update.sys.menu.menuRemark.tip': 'Remark',
  'pages.update.sys.menu.menuRemark.placeholder': 'Remark',

  'pages.update.sys.menu.menuState.label': 'State',
  'pages.update.sys.menu.menuState.tip': 'State',
  'pages.update.sys.menu.menuState.placeholder': 'State',

  'pages.update.sys.menu.menuType.label': 'Type',
  'pages.update.sys.menu.menuType.tip': 'Type',
  'pages.update.sys.menu.menuType.placeholder': 'Type',

  'pages.update.sys.menu.menuIconClass.label': 'IconClass',
  'pages.update.sys.menu.menuIconClass.tip': 'IconClass',
  'pages.update.sys.menu.menuIconClass.placeholder': 'IconClass',


  'pages.list.sys.menu.form.title': 'Query',

  'pages.list.sys.menu.menuId.label': '菜单ID号',
  'pages.list.sys.menu.menuId.tip': '菜单ID号',
  'pages.list.sys.menu.menuId.placeholder': '菜单ID号',

  'pages.list.sys.menu.menuName.label': 'Name',
  'pages.list.sys.menu.menuName.tip': 'Name',
  'pages.list.sys.menu.menuName.placeholder': 'Name',

  'pages.list.sys.menu.menuNameEn.label': 'NameEn',
  'pages.list.sys.menu.menuNameEn.tip': 'NameEn',
  'pages.list.sys.menu.menuNameEn.placeholder': 'NameEn',

  'pages.list.sys.menu.menuUrl.label': 'URL',
  'pages.list.sys.menu.menuUrl.tip': 'URL',
  'pages.list.sys.menu.menuUrl.placeholder': 'URL',

  'pages.list.sys.menu.menuParentId.label': '父菜单ID',
  'pages.list.sys.menu.menuParentId.tip': '父菜单ID',
  'pages.list.sys.menu.menuParentId.placeholder': '父菜单ID',

  'pages.list.sys.menu.menuOrder.label': 'Order',
  'pages.list.sys.menu.menuOrder.tip': 'Order',
  'pages.list.sys.menu.menuOrder.placeholder': 'Order',

  'pages.list.sys.menu.menuRemark.label': 'Remark',
  'pages.list.sys.menu.menuRemark.tip': 'Remark',
  'pages.list.sys.menu.menuRemark.placeholder': 'Remark',

  'pages.list.sys.menu.menuState.label': 'State',
  'pages.list.sys.menu.menuState.tip': 'State',
  'pages.list.sys.menu.menuState.placeholder': 'State',

  'pages.list.sys.menu.menuType.label': 'Type',
  'pages.list.sys.menu.menuType.tip': 'Type',
  'pages.list.sys.menu.menuType.placeholder': 'Type',

  'pages.list.sys.menu.menuIconClass.label': 'IconClass',
  'pages.list.sys.menu.menuIconClass.tip': 'IconClass',
  'pages.list.sys.menu.menuIconClass.placeholder': 'IconClass',


  'pages.list.sys.menu.option.delete':'Delete',
  'pages.list.sys.menu.batch.delete':'Batch delete',
  'pages.list.sys.menu.item':'item',
  'pages.list.sys.menu.chosen':'Has chosen',
  'pages.list.sys.menu.new':'Create',
  'pages.list.sys.menu.title':'Query',
  'pages.list.sys.menu.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.menu.option.edit':'Edit',
  'pages.list.sys.menu.option.title':'Option',
  'pages.list.sys.menu.index.label':'No.',
};
