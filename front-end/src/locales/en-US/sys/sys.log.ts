export default {
  'pages.create.sys.log.form.title': 'Create',

  'pages.create.sys.log.logId.label': '日志ID号',
  'pages.create.sys.log.logId.tip': '日志ID号',
  'pages.create.sys.log.logId.placeholder': '日志ID号',

  'pages.create.sys.log.logUser.label': 'Manager',
  'pages.create.sys.log.logUser.tip': 'Manager',
  'pages.create.sys.log.logUser.placeholder': 'Manager',

  'pages.create.sys.log.logTime.label': 'CreateDate',
  'pages.create.sys.log.logTime.tip': 'CreateDate',
  'pages.create.sys.log.logTime.placeholder': 'CreateDate',

  'pages.create.sys.log.logIp.label': 'IP',
  'pages.create.sys.log.logIp.tip': 'IP',
  'pages.create.sys.log.logIp.placeholder': 'IP',

  'pages.create.sys.log.logUrl.label': 'URL',
  'pages.create.sys.log.logUrl.tip': 'URL',
  'pages.create.sys.log.logUrl.placeholder': 'URL',

  'pages.create.sys.log.logTitle.label': 'Title',
  'pages.create.sys.log.logTitle.tip': 'Title',
  'pages.create.sys.log.logTitle.placeholder': 'Title',

  'pages.create.sys.log.logContent.label': 'Content',
  'pages.create.sys.log.logContent.tip': 'Content',
  'pages.create.sys.log.logContent.placeholder': 'Content',

  'pages.create.sys.log.logType.label': 'Type',
  'pages.create.sys.log.logType.tip': 'Type',
  'pages.create.sys.log.logType.placeholder': 'Type',


  'pages.update.sys.log.form.title': 'Update',

  'pages.update.sys.log.logId.label': '日志ID号',
  'pages.update.sys.log.logId.tip': '日志ID号',
  'pages.update.sys.log.logId.placeholder': '日志ID号',

  'pages.update.sys.log.logUser.label': 'Manager',
  'pages.update.sys.log.logUser.tip': 'Manager',
  'pages.update.sys.log.logUser.placeholder': 'Manager',

  'pages.update.sys.log.logTime.label': 'CreateDate',
  'pages.update.sys.log.logTime.tip': 'CreateDate',
  'pages.update.sys.log.logTime.placeholder': 'CreateDate',

  'pages.update.sys.log.logIp.label': 'IP',
  'pages.update.sys.log.logIp.tip': 'IP',
  'pages.update.sys.log.logIp.placeholder': 'IP',

  'pages.update.sys.log.logUrl.label': 'URL',
  'pages.update.sys.log.logUrl.tip': 'URL',
  'pages.update.sys.log.logUrl.placeholder': 'URL',

  'pages.update.sys.log.logTitle.label': 'Title',
  'pages.update.sys.log.logTitle.tip': 'Title',
  'pages.update.sys.log.logTitle.placeholder': 'Title',

  'pages.update.sys.log.logContent.label': 'Content',
  'pages.update.sys.log.logContent.tip': 'Content',
  'pages.update.sys.log.logContent.placeholder': 'Content',

  'pages.update.sys.log.logType.label': 'Type',
  'pages.update.sys.log.logType.tip': 'Type',
  'pages.update.sys.log.logType.placeholder': 'Type',


  'pages.list.sys.log.form.title': 'Query',

  'pages.list.sys.log.logId.label': '日志ID号',
  'pages.list.sys.log.logId.tip': '日志ID号',
  'pages.list.sys.log.logId.placeholder': '日志ID号',

  'pages.list.sys.log.logUser.label': 'Manager',
  'pages.list.sys.log.logUser.tip': 'Manager',
  'pages.list.sys.log.logUser.placeholder': 'Manager',

  'pages.list.sys.log.logTime.label': 'CreateDate',
  'pages.list.sys.log.logTime.tip': 'CreateDate',
  'pages.list.sys.log.logTime.placeholder': 'CreateDate',

  'pages.list.sys.log.logIp.label': 'IP',
  'pages.list.sys.log.logIp.tip': 'IP',
  'pages.list.sys.log.logIp.placeholder': 'IP',

  'pages.list.sys.log.logUrl.label': 'URL',
  'pages.list.sys.log.logUrl.tip': 'URL',
  'pages.list.sys.log.logUrl.placeholder': 'URL',

  'pages.list.sys.log.logTitle.label': 'Title',
  'pages.list.sys.log.logTitle.tip': 'Title',
  'pages.list.sys.log.logTitle.placeholder': 'Title',

  'pages.list.sys.log.logContent.label': 'Content',
  'pages.list.sys.log.logContent.tip': 'Content',
  'pages.list.sys.log.logContent.placeholder': 'Content',

  'pages.list.sys.log.logType.label': 'Type',
  'pages.list.sys.log.logType.tip': 'Type',
  'pages.list.sys.log.logType.placeholder': 'Type',


  'pages.list.sys.log.option.delete':'Delete',
  'pages.list.sys.log.batch.delete':'Batch delete',
  'pages.list.sys.log.item':'item',
  'pages.list.sys.log.chosen':'Has chosen',
  'pages.list.sys.log.new':'Create',
  'pages.list.sys.log.title':'Query',
  'pages.list.sys.log.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.log.option.edit':'Edit',
  'pages.list.sys.log.option.title':'Option',
  'pages.list.sys.log.index.label':'No.',
};
