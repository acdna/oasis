export default {
  'pages.list.sys.role.option.relate.menu': 'MENU',
  'pages.list.sys.role.option.relate.pms': 'PMS',
  'pages.list.sys.role.option.relate.dict': 'DICT',


  'pages.create.sys.role.form.title': 'Create',

  'pages.create.sys.role.roleId.label': '角色ID',
  'pages.create.sys.role.roleId.tip': '角色ID',
  'pages.create.sys.role.roleId.placeholder': '角色ID',

  'pages.create.sys.role.roleName.label': 'Role Name',
  'pages.create.sys.role.roleName.tip': 'Role Name',
  'pages.create.sys.role.roleName.placeholder': 'Role Name',

  'pages.create.sys.role.roleNameEn.label': 'Role Name En',
  'pages.create.sys.role.roleNameEn.tip': 'Role Name En',
  'pages.create.sys.role.roleNameEn.placeholder': 'Role Name En',

  'pages.create.sys.role.roleState.label': 'State',
  'pages.create.sys.role.roleState.tip': 'State',
  'pages.create.sys.role.roleState.placeholder': 'State',

  'pages.create.sys.role.roleSys.label': 'System',
  'pages.create.sys.role.roleSys.tip': 'System',
  'pages.create.sys.role.roleSys.placeholder': 'System',


  'pages.update.sys.role.form.title': 'Update',

  'pages.update.sys.role.roleId.label': '角色ID',
  'pages.update.sys.role.roleId.tip': '角色ID',
  'pages.update.sys.role.roleId.placeholder': '角色ID',

  'pages.update.sys.role.roleName.label': 'Role Name',
  'pages.update.sys.role.roleName.tip': 'Role Name',
  'pages.update.sys.role.roleName.placeholder': 'Role Name',

  'pages.update.sys.role.roleNameEn.label': 'Role Name En',
  'pages.update.sys.role.roleNameEn.tip': 'Role Name En',
  'pages.update.sys.role.roleNameEn.placeholder': 'Role Name En',

  'pages.update.sys.role.roleState.label': 'State',
  'pages.update.sys.role.roleState.tip': 'State',
  'pages.update.sys.role.roleState.placeholder': 'State',

  'pages.update.sys.role.roleSys.label': 'System',
  'pages.update.sys.role.roleSys.tip': 'System',
  'pages.update.sys.role.roleSys.placeholder': 'System',

  'pages.list.sys.role.form.title': 'Query',

  'pages.list.sys.role.roleId.label': '角色ID',
  'pages.list.sys.role.roleId.tip': '角色ID',
  'pages.list.sys.role.roleId.placeholder': '角色ID',

  'pages.list.sys.role.roleName.label': 'Role Name',
  'pages.list.sys.role.roleName.tip': 'Role Name',
  'pages.list.sys.role.roleName.placeholder': 'Role Name',

  'pages.list.sys.role.roleNameEn.label': 'Role Name En',
  'pages.list.sys.role.roleNameEn.tip': 'Role Name En',
  'pages.list.sys.role.roleNameEn.placeholder': 'Role Name En',

  'pages.list.sys.role.roleState.label': 'State',
  'pages.list.sys.role.roleState.tip': 'State',
  'pages.list.sys.role.roleState.placeholder': 'State',

  'pages.list.sys.role.roleSys.label': 'System',
  'pages.list.sys.role.roleSys.tip': 'System',
  'pages.list.sys.role.roleSys.placeholder': 'System',


  'pages.list.sys.role.option.delete':'Delete',
  'pages.list.sys.role.batch.delete':'Batch delete',
  'pages.list.sys.role.item':'item',
  'pages.list.sys.role.chosen':'Has chosen',
  'pages.list.sys.role.new':'Create',
  'pages.list.sys.role.title':'Query',
  'pages.list.sys.role.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.role.option.edit':'Edit',
  'pages.list.sys.role.option.title':'Option',
  'pages.list.sys.role.index.label':'No.',
};
