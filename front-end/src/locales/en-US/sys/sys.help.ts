export default {
  'pages.create.sys.help.form.title': 'Create',
  
  'pages.create.sys.help.helpId.label': '帮助编号',
  'pages.create.sys.help.helpId.tip': '帮助编号',
  'pages.create.sys.help.helpId.placeholder': '帮助编号',
  
  'pages.create.sys.help.helpQuestion.label': '帮助问题',
  'pages.create.sys.help.helpQuestion.tip': '帮助问题',
  'pages.create.sys.help.helpQuestion.placeholder': '帮助问题',
  
  'pages.create.sys.help.helpAnswer.label': '帮助回答',
  'pages.create.sys.help.helpAnswer.tip': '帮助回答',
  'pages.create.sys.help.helpAnswer.placeholder': '帮助回答',
  
  'pages.create.sys.help.helpParentId.label': '问题编号父级编号',
  'pages.create.sys.help.helpParentId.tip': '问题编号父级编号',
  'pages.create.sys.help.helpParentId.placeholder': '问题编号父级编号',
  
  'pages.create.sys.help.helpType.label': '帮助类别',
  'pages.create.sys.help.helpType.tip': '帮助类别',
  'pages.create.sys.help.helpType.placeholder': '帮助类别',
  
  'pages.create.sys.help.helpPath.label': '帮助匹配路径',
  'pages.create.sys.help.helpPath.tip': '帮助匹配路径',
  'pages.create.sys.help.helpPath.placeholder': '帮助匹配路径',
  
  'pages.create.sys.help.helpSort.label': '排序',
  'pages.create.sys.help.helpSort.tip': '排序',
  'pages.create.sys.help.helpSort.placeholder': '排序',
  
  'pages.create.sys.help.helpCreateDate.label': '创建日期',
  'pages.create.sys.help.helpCreateDate.tip': '创建日期',
  'pages.create.sys.help.helpCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.help.helpUpdateDate.label': '更新日期',
  'pages.create.sys.help.helpUpdateDate.tip': '更新日期',
  'pages.create.sys.help.helpUpdateDate.placeholder': '更新日期',
  

  'pages.update.sys.help.form.title': 'Update',
  
  'pages.update.sys.help.helpId.label': '帮助编号',
  'pages.update.sys.help.helpId.tip': '帮助编号',
  'pages.update.sys.help.helpId.placeholder': '帮助编号',
  
  'pages.update.sys.help.helpQuestion.label': '帮助问题',
  'pages.update.sys.help.helpQuestion.tip': '帮助问题',
  'pages.update.sys.help.helpQuestion.placeholder': '帮助问题',
  
  'pages.update.sys.help.helpAnswer.label': '帮助回答',
  'pages.update.sys.help.helpAnswer.tip': '帮助回答',
  'pages.update.sys.help.helpAnswer.placeholder': '帮助回答',
  
  'pages.update.sys.help.helpParentId.label': '问题编号父级编号',
  'pages.update.sys.help.helpParentId.tip': '问题编号父级编号',
  'pages.update.sys.help.helpParentId.placeholder': '问题编号父级编号',
  
  'pages.update.sys.help.helpType.label': '帮助类别',
  'pages.update.sys.help.helpType.tip': '帮助类别',
  'pages.update.sys.help.helpType.placeholder': '帮助类别',
  
  'pages.update.sys.help.helpPath.label': '帮助匹配路径',
  'pages.update.sys.help.helpPath.tip': '帮助匹配路径',
  'pages.update.sys.help.helpPath.placeholder': '帮助匹配路径',
  
  'pages.update.sys.help.helpSort.label': '排序',
  'pages.update.sys.help.helpSort.tip': '排序',
  'pages.update.sys.help.helpSort.placeholder': '排序',
  
  'pages.update.sys.help.helpCreateDate.label': '创建日期',
  'pages.update.sys.help.helpCreateDate.tip': '创建日期',
  'pages.update.sys.help.helpCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.help.helpUpdateDate.label': '更新日期',
  'pages.update.sys.help.helpUpdateDate.tip': '更新日期',
  'pages.update.sys.help.helpUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.help.form.title': 'Query',
  
  'pages.list.sys.help.helpId.label': '帮助编号',
  'pages.list.sys.help.helpId.tip': '帮助编号',
  'pages.list.sys.help.helpId.placeholder': '帮助编号',
  
  'pages.list.sys.help.helpQuestion.label': '帮助问题',
  'pages.list.sys.help.helpQuestion.tip': '帮助问题',
  'pages.list.sys.help.helpQuestion.placeholder': '帮助问题',
  
  'pages.list.sys.help.helpAnswer.label': '帮助回答',
  'pages.list.sys.help.helpAnswer.tip': '帮助回答',
  'pages.list.sys.help.helpAnswer.placeholder': '帮助回答',
  
  'pages.list.sys.help.helpParentId.label': '问题编号父级编号',
  'pages.list.sys.help.helpParentId.tip': '问题编号父级编号',
  'pages.list.sys.help.helpParentId.placeholder': '问题编号父级编号',
  
  'pages.list.sys.help.helpType.label': '帮助类别',
  'pages.list.sys.help.helpType.tip': '帮助类别',
  'pages.list.sys.help.helpType.placeholder': '帮助类别',
  
  'pages.list.sys.help.helpPath.label': '帮助匹配路径',
  'pages.list.sys.help.helpPath.tip': '帮助匹配路径',
  'pages.list.sys.help.helpPath.placeholder': '帮助匹配路径',
  
  'pages.list.sys.help.helpSort.label': '排序',
  'pages.list.sys.help.helpSort.tip': '排序',
  'pages.list.sys.help.helpSort.placeholder': '排序',
  
  'pages.list.sys.help.helpCreateDate.label': '创建日期',
  'pages.list.sys.help.helpCreateDate.tip': '创建日期',
  'pages.list.sys.help.helpCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.help.helpUpdateDate.label': '更新日期',
  'pages.list.sys.help.helpUpdateDate.tip': '更新日期',
  'pages.list.sys.help.helpUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.help.option.delete':'Delete',
  'pages.list.sys.help.batch.delete':'Batch delete',
  'pages.list.sys.help.item':'item',
  'pages.list.sys.help.chosen':'Has chosen',
  'pages.list.sys.help.new':'Create',
  'pages.list.sys.help.title':'Query',
  'pages.list.sys.help.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.help.option.edit':'Edit',
  'pages.list.sys.help.option.title':'Option',
  'pages.list.sys.help.index.label':'No.',
};