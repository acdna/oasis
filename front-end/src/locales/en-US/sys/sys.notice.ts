export default {
  'pages.create.sys.notice.form.title': 'Create',
  
  'pages.create.sys.notice.noticeId.label': '消息编号',
  'pages.create.sys.notice.noticeId.tip': '消息编号',
  'pages.create.sys.notice.noticeId.placeholder': '消息编号',
  
  'pages.create.sys.notice.noticeTitle.label': '消息标题',
  'pages.create.sys.notice.noticeTitle.tip': '消息标题',
  'pages.create.sys.notice.noticeTitle.placeholder': '消息标题',
  
  'pages.create.sys.notice.noticeContent.label': '消息内容',
  'pages.create.sys.notice.noticeContent.tip': '消息内容',
  'pages.create.sys.notice.noticeContent.placeholder': '消息内容',
  
  'pages.create.sys.notice.noticeManager.label': '消息发布者',
  'pages.create.sys.notice.noticeManager.tip': '消息发布者',
  'pages.create.sys.notice.noticeManager.placeholder': '消息发布者',
  
  'pages.create.sys.notice.noticeIsNew.label': '是否是新消息',
  'pages.create.sys.notice.noticeIsNew.tip': '是否是新消息',
  'pages.create.sys.notice.noticeIsNew.placeholder': '是否是新消息',
  
  'pages.create.sys.notice.noticeCreateDate.label': '创建日期',
  'pages.create.sys.notice.noticeCreateDate.tip': '创建日期',
  'pages.create.sys.notice.noticeCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.notice.noticeUpdateDate.label': '更新日期',
  'pages.create.sys.notice.noticeUpdateDate.tip': '更新日期',
  'pages.create.sys.notice.noticeUpdateDate.placeholder': '更新日期',
  
  'pages.create.sys.notice.noticeReceiver.label': '消息接收者',
  'pages.create.sys.notice.noticeReceiver.tip': '消息接收者',
  'pages.create.sys.notice.noticeReceiver.placeholder': '消息接收者',
  

  'pages.update.sys.notice.form.title': 'Update',
  
  'pages.update.sys.notice.noticeId.label': '消息编号',
  'pages.update.sys.notice.noticeId.tip': '消息编号',
  'pages.update.sys.notice.noticeId.placeholder': '消息编号',
  
  'pages.update.sys.notice.noticeTitle.label': '消息标题',
  'pages.update.sys.notice.noticeTitle.tip': '消息标题',
  'pages.update.sys.notice.noticeTitle.placeholder': '消息标题',
  
  'pages.update.sys.notice.noticeContent.label': '消息内容',
  'pages.update.sys.notice.noticeContent.tip': '消息内容',
  'pages.update.sys.notice.noticeContent.placeholder': '消息内容',
  
  'pages.update.sys.notice.noticeManager.label': '消息发布者',
  'pages.update.sys.notice.noticeManager.tip': '消息发布者',
  'pages.update.sys.notice.noticeManager.placeholder': '消息发布者',
  
  'pages.update.sys.notice.noticeIsNew.label': '是否是新消息',
  'pages.update.sys.notice.noticeIsNew.tip': '是否是新消息',
  'pages.update.sys.notice.noticeIsNew.placeholder': '是否是新消息',
  
  'pages.update.sys.notice.noticeCreateDate.label': '创建日期',
  'pages.update.sys.notice.noticeCreateDate.tip': '创建日期',
  'pages.update.sys.notice.noticeCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.notice.noticeUpdateDate.label': '更新日期',
  'pages.update.sys.notice.noticeUpdateDate.tip': '更新日期',
  'pages.update.sys.notice.noticeUpdateDate.placeholder': '更新日期',
  
  'pages.update.sys.notice.noticeReceiver.label': '消息接收者',
  'pages.update.sys.notice.noticeReceiver.tip': '消息接收者',
  'pages.update.sys.notice.noticeReceiver.placeholder': '消息接收者',
  

  'pages.list.sys.notice.form.title': 'Query',
  
  'pages.list.sys.notice.noticeId.label': '消息编号',
  'pages.list.sys.notice.noticeId.tip': '消息编号',
  'pages.list.sys.notice.noticeId.placeholder': '消息编号',
  
  'pages.list.sys.notice.noticeTitle.label': '消息标题',
  'pages.list.sys.notice.noticeTitle.tip': '消息标题',
  'pages.list.sys.notice.noticeTitle.placeholder': '消息标题',
  
  'pages.list.sys.notice.noticeContent.label': '消息内容',
  'pages.list.sys.notice.noticeContent.tip': '消息内容',
  'pages.list.sys.notice.noticeContent.placeholder': '消息内容',
  
  'pages.list.sys.notice.noticeManager.label': '消息发布者',
  'pages.list.sys.notice.noticeManager.tip': '消息发布者',
  'pages.list.sys.notice.noticeManager.placeholder': '消息发布者',
  
  'pages.list.sys.notice.noticeIsNew.label': '是否是新消息',
  'pages.list.sys.notice.noticeIsNew.tip': '是否是新消息',
  'pages.list.sys.notice.noticeIsNew.placeholder': '是否是新消息',
  
  'pages.list.sys.notice.noticeCreateDate.label': '创建日期',
  'pages.list.sys.notice.noticeCreateDate.tip': '创建日期',
  'pages.list.sys.notice.noticeCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.notice.noticeUpdateDate.label': '更新日期',
  'pages.list.sys.notice.noticeUpdateDate.tip': '更新日期',
  'pages.list.sys.notice.noticeUpdateDate.placeholder': '更新日期',
  
  'pages.list.sys.notice.noticeReceiver.label': '消息接收者',
  'pages.list.sys.notice.noticeReceiver.tip': '消息接收者',
  'pages.list.sys.notice.noticeReceiver.placeholder': '消息接收者',
  

  'pages.list.sys.notice.option.delete':'Delete',
  'pages.list.sys.notice.batch.delete':'Batch delete',
  'pages.list.sys.notice.item':'item',
  'pages.list.sys.notice.chosen':'Has chosen',
  'pages.list.sys.notice.new':'Create',
  'pages.list.sys.notice.title':'Query',
  'pages.list.sys.notice.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.notice.option.edit':'Edit',
  'pages.list.sys.notice.option.title':'Option',
  'pages.list.sys.notice.index.label':'No.',
};