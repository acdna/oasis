export default {
  'pages.create.sys.dictionary.dict.form.title': 'Create Dict',
  'pages.create.sys.dictionary.item.form.title': 'Create Item',
  'pages.create.sys.dictionary.form.title': 'Create',
  'pages.list.sys.dictionary.option.create.item':'Create',

  'pages.create.sys.dictionary.dicId.label': 'ID',
  'pages.create.sys.dictionary.dicId.tip': 'ID',
  'pages.create.sys.dictionary.dicId.placeholder': 'Please input ID',

  'pages.create.sys.dictionary.dicName.label': 'Name',
  'pages.create.sys.dictionary.dicName.tip': 'Name is required',
  'pages.create.sys.dictionary.dicName.placeholder': 'Please input Name',

  'pages.create.sys.dictionary.dict.dicName.label': 'Name',
  'pages.create.sys.dictionary.dict.dicName.tip': 'Name is required',
  'pages.create.sys.dictionary.dict.dicName.placeholder': 'Please input Name',

  'pages.create.sys.dictionary.item.dicName.label': 'Name',
  'pages.create.sys.dictionary.item.dicName.tip': 'Name is required',
  'pages.create.sys.dictionary.item.dicName.placeholder': 'Please input Name',

  'pages.create.sys.dictionary.dicNameLike.label': 'Name',
  'pages.create.sys.dictionary.dicNameLike.tip': 'Name is required',
  'pages.create.sys.dictionary.dicNameLike.placeholder': 'Please input Name',

  'pages.create.sys.dictionary.dicNameEn.label': 'NameEn',
  'pages.create.sys.dictionary.dicNameEn.tip': 'NameEn is required',
  'pages.create.sys.dictionary.dicNameEn.placeholder': 'Please input NameEn',

  'pages.create.sys.dictionary.item.dicNameEn.label': 'NameEn',
  'pages.create.sys.dictionary.item.dicNameEn.tip': 'NameEn is required',
  'pages.create.sys.dictionary.item.dicNameEn.placeholder': 'Please input NameEn',

  'pages.create.sys.dictionary.dict.dicNameEn.label': 'NameEn',
  'pages.create.sys.dictionary.dict.dicNameEn.tip': 'NameEn is required',
  'pages.create.sys.dictionary.dict.dicNameEn.placeholder': 'Please input NameEn',

  'pages.create.sys.dictionary.dicNameEnLike.label': 'NameEn',
  'pages.create.sys.dictionary.dicNameEnLike.tip': 'NameEn is required',
  'pages.create.sys.dictionary.dicNameEnLike.placeholder': 'Please input NameEn',

  'pages.create.sys.dictionary.dicValue.label': 'Value',
  'pages.create.sys.dictionary.dicValue.tip': 'Value is required',
  'pages.create.sys.dictionary.dicValue.placeholder': 'Please input Value',

  'pages.create.sys.dictionary.dicGroup.label': 'Group',
  'pages.create.sys.dictionary.dicGroup.tip': 'Group is required',
  'pages.create.sys.dictionary.dicGroup.placeholder': 'Please input Group',

  'pages.create.sys.dictionary.dicModule.label': 'Module',
  'pages.create.sys.dictionary.dicModule.tip': 'Module',
  'pages.create.sys.dictionary.dicModule.placeholder': 'Please input Module',

  'pages.create.sys.dictionary.dicParentId.label': '父级ID',
  'pages.create.sys.dictionary.dicParentId.tip': '父级ID',
  'pages.create.sys.dictionary.dicParentId.placeholder': 'Please input 父级ID',

  'pages.create.sys.dictionary.dicType.label': 'Type',
  'pages.create.sys.dictionary.dicType.tip': 'Type is required',
  'pages.create.sys.dictionary.dicType.placeholder': 'Please input Type',

  'pages.create.sys.dictionary.dicOrder.label': 'Order',
  'pages.create.sys.dictionary.dicOrder.tip': 'Order',
  'pages.create.sys.dictionary.dicOrder.placeholder': 'Please input Order',

  'pages.create.sys.dictionary.dicState.label': 'State',
  'pages.create.sys.dictionary.dicState.tip': 'State',
  'pages.create.sys.dictionary.dicState.placeholder': 'Please input State',

  'pages.create.sys.dictionary.dicParams.label': 'dicParams',
  'pages.create.sys.dictionary.dicParams.tip': 'dicParams',
  'pages.create.sys.dictionary.dicParams.placeholder': 'Please input dicParams',

  'pages.create.sys.dictionary.dicSpecies.label': 'Species',
  'pages.create.sys.dictionary.dicSpecies.tip': 'Species is required',
  'pages.create.sys.dictionary.dicSpecies.placeholder': 'Please input Species',

  'pages.create.sys.dictionary.dicCreateDate.label': 'CreateDate',
  'pages.create.sys.dictionary.dicCreateDate.tip': 'CreateDate',
  'pages.create.sys.dictionary.dicCreateDate.placeholder': 'Please input CreateDate',

  'pages.create.sys.dictionary.dicUpdateDate.label': 'UpdateDate',
  'pages.create.sys.dictionary.dicUpdateDate.tip': 'UpdateDate',
  'pages.create.sys.dictionary.dicUpdateDate.placeholder': 'Please input UpdateDate',


  'pages.update.sys.dictionary.dict.form.title': 'Update Dict',
  'pages.update.sys.dictionary.item.form.title': 'Update Item',
  'pages.update.sys.dictionary.form.title': 'Update',

  'pages.update.sys.dictionary.dicId.label': 'ID',
  'pages.update.sys.dictionary.dicId.tip': 'ID',
  'pages.update.sys.dictionary.dicId.placeholder': 'Please input ID',

  'pages.update.sys.dictionary.dicName.label': 'Name',
  'pages.update.sys.dictionary.dicName.tip': 'Name is required',
  'pages.update.sys.dictionary.dicName.placeholder': 'Please input Name',

  'pages.update.sys.dictionary.item.dicName.label': 'Name',
  'pages.update.sys.dictionary.item.dicName.tip': 'Name is required',
  'pages.update.sys.dictionary.item.dicName.placeholder': 'Please input Name',

  'pages.update.sys.dictionary.dicNameEn.label': 'NameEn',
  'pages.update.sys.dictionary.dicNameEn.tip': 'NameEn is required',
  'pages.update.sys.dictionary.dicNameEn.placeholder': 'Please input NameEn',

  'pages.update.sys.dictionary.item.dicNameEn.label': 'NameEn',
  'pages.update.sys.dictionary.item.dicNameEn.tip': 'NameEn is required',
  'pages.update.sys.dictionary.item.dicNameEn.placeholder': 'Please input NameEn',

  'pages.update.sys.dictionary.dict.dicName.label': 'Name',
  'pages.update.sys.dictionary.dict.dicName.tip': 'Name is required',
  'pages.update.sys.dictionary.dict.dicName.placeholder': 'Please input Name',

  'pages.update.sys.dictionary.dict.dicNameEn.label': 'NameEn',
  'pages.update.sys.dictionary.dict.dicNameEn.tip': 'NameEn is required',
  'pages.update.sys.dictionary.dict.dicNameEn.placeholder': 'Please input NameEn',

  'pages.update.sys.dictionary.dicValue.label': 'Value',
  'pages.update.sys.dictionary.dicValue.tip': 'Value is required',
  'pages.update.sys.dictionary.dicValue.placeholder': 'Please input Value',

  'pages.update.sys.dictionary.item.dicValue.label': 'Value',
  'pages.update.sys.dictionary.item.dicValue.tip': 'Value is required',
  'pages.update.sys.dictionary.item.dicValue.placeholder': 'Please input Value',

  'pages.update.sys.dictionary.dicGroup.label': 'Group',
  'pages.update.sys.dictionary.dicGroup.tip': 'Group is required',
  'pages.update.sys.dictionary.dicGroup.placeholder': 'Please input Group',

  'pages.update.sys.dictionary.dicModule.label': 'Module',
  'pages.update.sys.dictionary.dicModule.tip': 'Module is required',
  'pages.update.sys.dictionary.dicModule.placeholder': 'Please input Module',

  'pages.update.sys.dictionary.dicParentId.label': '父级ID',
  'pages.update.sys.dictionary.dicParentId.tip': '父级ID',
  'pages.update.sys.dictionary.dicParentId.placeholder': 'Please input 父级ID',

  'pages.update.sys.dictionary.dicType.label': 'Type',
  'pages.update.sys.dictionary.dicType.tip': 'Type',
  'pages.update.sys.dictionary.dicType.placeholder': 'Please input Type',

  'pages.update.sys.dictionary.dicOrder.label': 'Order',
  'pages.update.sys.dictionary.dicOrder.tip': 'Order is required',
  'pages.update.sys.dictionary.dicOrder.placeholder': 'Please input Order',

  'pages.update.sys.dictionary.dicState.label': 'State',
  'pages.update.sys.dictionary.dicState.tip': 'State is required',
  'pages.update.sys.dictionary.dicState.placeholder': 'Please input State',

  'pages.update.sys.dictionary.dicParams.label': 'dicParams',
  'pages.update.sys.dictionary.dicParams.tip': 'dicParams',
  'pages.update.sys.dictionary.dicParams.placeholder': 'Please input dicParams',

  'pages.update.sys.dictionary.dicSpecies.label': 'Species',
  'pages.update.sys.dictionary.dicSpecies.tip': 'Species is required',
  'pages.update.sys.dictionary.dicSpecies.placeholder': 'Please input Species',

  'pages.update.sys.dictionary.dicCreateDate.label': 'CreateDate',
  'pages.update.sys.dictionary.dicCreateDate.tip': 'CreateDate',
  'pages.update.sys.dictionary.dicCreateDate.placeholder': 'Please input CreateDate',

  'pages.update.sys.dictionary.dicUpdateDate.label': 'UpdateDate',
  'pages.update.sys.dictionary.dicUpdateDate.tip': 'UpdateDate',
  'pages.update.sys.dictionary.dicUpdateDate.placeholder': 'Please input UpdateDate',

  'pages.list.sys.dictionary.new.dict': 'Create Dict',
  'pages.list.sys.dictionary.init.dict': 'Init Dicts',

  'pages.list.sys.dictionary.form.title': 'Query',

  'pages.list.sys.dictionary.dicId.label': 'ID',
  'pages.list.sys.dictionary.dicId.tip': 'ID',
  'pages.list.sys.dictionary.dicId.placeholder': 'Please input ID',

  'pages.list.sys.dictionary.dicName.label': 'Name',
  'pages.list.sys.dictionary.dicName.tip': 'Name',
  'pages.list.sys.dictionary.dicName.placeholder': 'Please input Name',

  'pages.list.sys.dictionary.dicNameEn.label': 'NameEn',
  'pages.list.sys.dictionary.dicNameEn.tip': 'NameEn',
  'pages.list.sys.dictionary.dicNameEn.placeholder': 'Please input NameEn',

  'pages.list.sys.dictionary.dicNameLike.label': 'Name',
  'pages.list.sys.dictionary.dicNameLike.tip': 'Name',
  'pages.list.sys.dictionary.dicNameLike.placeholder': 'Please input Name',

  'pages.list.sys.dictionary.dict.dicNameEn.label': 'NameEn',
  'pages.list.sys.dictionary.dict.dicNameEn.tip': 'NameEn',
  'pages.list.sys.dictionary.dict.dicNameEn.placeholder': 'Please input NameEn',

  'pages.list.sys.dictionary.dict.dicNameLike.label': 'Name',
  'pages.list.sys.dictionary.dict.dicNameLike.tip': 'Name',
  'pages.list.sys.dictionary.dict.dicNameLike.placeholder': 'Please input Name',

  'pages.list.sys.dictionary.dicNameEnLike.label': 'NameEn',
  'pages.list.sys.dictionary.dicNameEnLike.tip': 'NameEn',
  'pages.list.sys.dictionary.dicNameEnLike.placeholder': 'Please input NameEn',

  'pages.list.sys.dictionary.dicName.item.label': 'Item Name',
  'pages.list.sys.dictionary.dicName.item.tip': 'Item Name',
  'pages.list.sys.dictionary.dicName.item.placeholder': 'Please input Item Name',

  'pages.list.sys.dictionary.dicNameEn.item.label': 'Item NameEn',
  'pages.list.sys.dictionary.dicNameEn.item.tip': 'Item NameEn',
  'pages.list.sys.dictionary.dicNameEn.item.placeholder': 'Please input Item NameEn',

  'pages.list.sys.dictionary.dicValue.label': 'Value',
  'pages.list.sys.dictionary.dicValue.tip': 'Value',
  'pages.list.sys.dictionary.dicValue.placeholder': 'Please input Value',

  'pages.list.sys.dictionary.dicGroup.label': 'Group',
  'pages.list.sys.dictionary.dicGroup.tip': 'Group',
  'pages.list.sys.dictionary.dicGroup.placeholder': 'Please input Group',

  'pages.list.sys.dictionary.dicModule.label': 'Module',
  'pages.list.sys.dictionary.dicModule.tip': 'Module',
  'pages.list.sys.dictionary.dicModule.placeholder': 'Please input Module',

  'pages.list.sys.dictionary.dicParentId.label': '父级ID',
  'pages.list.sys.dictionary.dicParentId.tip': '父级ID',
  'pages.list.sys.dictionary.dicParentId.placeholder': 'Please input 父级ID',

  'pages.list.sys.dictionary.dicType.label': 'Type',
  'pages.list.sys.dictionary.dicType.tip': 'Type',
  'pages.list.sys.dictionary.dicType.placeholder': 'Please input Type',

  'pages.list.sys.dictionary.dicOrder.label': 'Order',
  'pages.list.sys.dictionary.dicOrder.tip': 'Order',
  'pages.list.sys.dictionary.dicOrder.placeholder': 'Please input Order',

  'pages.list.sys.dictionary.dicState.label': 'State',
  'pages.list.sys.dictionary.dicState.tip': 'State',
  'pages.list.sys.dictionary.dicState.placeholder': 'Please input State',

  'pages.list.sys.dictionary.dicParams.label': 'dicParams',
  'pages.list.sys.dictionary.dicParams.tip': 'dicParams',
  'pages.list.sys.dictionary.dicParams.placeholder': 'Please input dicParams',

  'pages.list.sys.dictionary.dicSpecies.label': 'Species',
  'pages.list.sys.dictionary.dicSpecies.tip': 'Species',
  'pages.list.sys.dictionary.dicSpecies.placeholder': 'Please input Species',

  'pages.list.sys.dictionary.dicCreateDate.label': 'CreateDate',
  'pages.list.sys.dictionary.dicCreateDate.tip': 'CreateDate',
  'pages.list.sys.dictionary.dicCreateDate.placeholder': 'Please input CreateDate',

  'pages.list.sys.dictionary.dicUpdateDate.label': 'UpdateDate',
  'pages.list.sys.dictionary.dicUpdateDate.tip': 'UpdateDate',
  'pages.list.sys.dictionary.dicUpdateDate.placeholder': 'Please input UpdateDate',

  'pages.list.sys.dictionary.option.delete':'Delete',
  'pages.list.sys.dictionary.batch.delete':'Batch delete',
  'pages.list.sys.dictionary.item':'item',
  'pages.list.sys.dictionary.chosen':'Has chosen',
  'pages.list.sys.dictionary.new':'Create',
  'pages.list.sys.dictionary.title':'Query',
  'pages.list.sys.dictionary.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.dictionary.option.init.confirm':'The existing dictionary will be deleted before initialization. Are you sure you want to continue?',
  'pages.list.sys.dictionary.option.edit':'Edit',
  'pages.list.sys.dictionary.option.title':'Option',
  'pages.list.sys.dictionary.index.label':'No.',
};
