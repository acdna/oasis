export default {
  'pages.create.sys.memory.form.title': 'Create',
  
  'pages.create.sys.memory.memId.label': 'memId',
  'pages.create.sys.memory.memId.tip': 'memId',
  'pages.create.sys.memory.memId.placeholder': 'memId',
  
  'pages.create.sys.memory.memTotal.label': '总内存',
  'pages.create.sys.memory.memTotal.tip': '总内存',
  'pages.create.sys.memory.memTotal.placeholder': '总内存',
  
  'pages.create.sys.memory.memFree.label': '可用内存',
  'pages.create.sys.memory.memFree.tip': '可用内存',
  'pages.create.sys.memory.memFree.placeholder': '可用内存',
  
  'pages.create.sys.memory.memUsed.label': '已使用内存',
  'pages.create.sys.memory.memUsed.tip': '已使用内存',
  'pages.create.sys.memory.memUsed.placeholder': '已使用内存',
  
  'pages.create.sys.memory.memIsWarning.label': '内存用量是否预警',
  'pages.create.sys.memory.memIsWarning.tip': '内存用量是否预警',
  'pages.create.sys.memory.memIsWarning.placeholder': '内存用量是否预警',
  
  'pages.create.sys.memory.memMax.label': '最大内存',
  'pages.create.sys.memory.memMax.tip': '最大内存',
  'pages.create.sys.memory.memMax.placeholder': '最大内存',
  
  'pages.create.sys.memory.memCreateDate.label': '创建日期',
  'pages.create.sys.memory.memCreateDate.tip': '创建日期',
  'pages.create.sys.memory.memCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.memory.memUpdateDate.label': '更新日期',
  'pages.create.sys.memory.memUpdateDate.tip': '更新日期',
  'pages.create.sys.memory.memUpdateDate.placeholder': '更新日期',
  

  'pages.update.sys.memory.form.title': 'Update',
  
  'pages.update.sys.memory.memId.label': 'memId',
  'pages.update.sys.memory.memId.tip': 'memId',
  'pages.update.sys.memory.memId.placeholder': 'memId',
  
  'pages.update.sys.memory.memTotal.label': '总内存',
  'pages.update.sys.memory.memTotal.tip': '总内存',
  'pages.update.sys.memory.memTotal.placeholder': '总内存',
  
  'pages.update.sys.memory.memFree.label': '可用内存',
  'pages.update.sys.memory.memFree.tip': '可用内存',
  'pages.update.sys.memory.memFree.placeholder': '可用内存',
  
  'pages.update.sys.memory.memUsed.label': '已使用内存',
  'pages.update.sys.memory.memUsed.tip': '已使用内存',
  'pages.update.sys.memory.memUsed.placeholder': '已使用内存',
  
  'pages.update.sys.memory.memIsWarning.label': '内存用量是否预警',
  'pages.update.sys.memory.memIsWarning.tip': '内存用量是否预警',
  'pages.update.sys.memory.memIsWarning.placeholder': '内存用量是否预警',
  
  'pages.update.sys.memory.memMax.label': '最大内存',
  'pages.update.sys.memory.memMax.tip': '最大内存',
  'pages.update.sys.memory.memMax.placeholder': '最大内存',
  
  'pages.update.sys.memory.memCreateDate.label': '创建日期',
  'pages.update.sys.memory.memCreateDate.tip': '创建日期',
  'pages.update.sys.memory.memCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.memory.memUpdateDate.label': '更新日期',
  'pages.update.sys.memory.memUpdateDate.tip': '更新日期',
  'pages.update.sys.memory.memUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.memory.form.title': 'Query',
  
  'pages.list.sys.memory.memId.label': 'memId',
  'pages.list.sys.memory.memId.tip': 'memId',
  'pages.list.sys.memory.memId.placeholder': 'memId',
  
  'pages.list.sys.memory.memTotal.label': '总内存',
  'pages.list.sys.memory.memTotal.tip': '总内存',
  'pages.list.sys.memory.memTotal.placeholder': '总内存',
  
  'pages.list.sys.memory.memFree.label': '可用内存',
  'pages.list.sys.memory.memFree.tip': '可用内存',
  'pages.list.sys.memory.memFree.placeholder': '可用内存',
  
  'pages.list.sys.memory.memUsed.label': '已使用内存',
  'pages.list.sys.memory.memUsed.tip': '已使用内存',
  'pages.list.sys.memory.memUsed.placeholder': '已使用内存',
  
  'pages.list.sys.memory.memIsWarning.label': '内存用量是否预警',
  'pages.list.sys.memory.memIsWarning.tip': '内存用量是否预警',
  'pages.list.sys.memory.memIsWarning.placeholder': '内存用量是否预警',
  
  'pages.list.sys.memory.memMax.label': '最大内存',
  'pages.list.sys.memory.memMax.tip': '最大内存',
  'pages.list.sys.memory.memMax.placeholder': '最大内存',
  
  'pages.list.sys.memory.memCreateDate.label': '创建日期',
  'pages.list.sys.memory.memCreateDate.tip': '创建日期',
  'pages.list.sys.memory.memCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.memory.memUpdateDate.label': '更新日期',
  'pages.list.sys.memory.memUpdateDate.tip': '更新日期',
  'pages.list.sys.memory.memUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.memory.option.delete':'Delete',
  'pages.list.sys.memory.batch.delete':'Batch delete',
  'pages.list.sys.memory.item':'item',
  'pages.list.sys.memory.chosen':'Has chosen',
  'pages.list.sys.memory.new':'Create',
  'pages.list.sys.memory.title':'Query',
  'pages.list.sys.memory.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.memory.option.edit':'Edit',
  'pages.list.sys.memory.option.title':'Option',
  'pages.list.sys.memory.index.label':'No.',
};