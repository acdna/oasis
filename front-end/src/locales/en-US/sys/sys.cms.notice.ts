export default {
  'pages.create.sys.cms.notice.form.title': 'Create',

  'pages.create.sys.cms.notice.scNoticeId.label': 'ID',
  'pages.create.sys.cms.notice.scNoticeId.tip': 'ID',
  'pages.create.sys.cms.notice.scNoticeId.placeholder': 'ID',

  'pages.create.sys.cms.notice.scNoticeTitle.label': 'Title',
  'pages.create.sys.cms.notice.scNoticeTitle.tip': 'Title',
  'pages.create.sys.cms.notice.scNoticeTitle.placeholder': 'Title',

  'pages.create.sys.cms.notice.scNoticeContent.label': 'Content',
  'pages.create.sys.cms.notice.scNoticeContent.tip': 'Content',
  'pages.create.sys.cms.notice.scNoticeContent.placeholder': 'Content',

  'pages.create.sys.cms.notice.scNoticeIsNew.label': 'IsNew',
  'pages.create.sys.cms.notice.scNoticeIsNew.tip': 'IsNew',
  'pages.create.sys.cms.notice.scNoticeIsNew.placeholder': 'IsNew',

  'pages.create.sys.cms.notice.scNoticeManager.label': 'Manager',
  'pages.create.sys.cms.notice.scNoticeManager.tip': 'Manager',
  'pages.create.sys.cms.notice.scNoticeManager.placeholder': 'Manager',

  'pages.create.sys.cms.notice.scNoticeCreateDate.label': 'CreateDate',
  'pages.create.sys.cms.notice.scNoticeCreateDate.tip': 'CreateDate',
  'pages.create.sys.cms.notice.scNoticeCreateDate.placeholder': 'CreateDate',

  'pages.create.sys.cms.notice.scNoticeUpdateDate.label': 'UpdateDate',
  'pages.create.sys.cms.notice.scNoticeUpdateDate.tip': 'UpdateDate',
  'pages.create.sys.cms.notice.scNoticeUpdateDate.placeholder': 'UpdateDate',


  'pages.update.sys.cms.notice.form.title': 'Update',

  'pages.update.sys.cms.notice.scNoticeId.label': 'ID',
  'pages.update.sys.cms.notice.scNoticeId.tip': 'ID',
  'pages.update.sys.cms.notice.scNoticeId.placeholder': 'ID',

  'pages.update.sys.cms.notice.scNoticeTitle.label': 'Title',
  'pages.update.sys.cms.notice.scNoticeTitle.tip': 'Title',
  'pages.update.sys.cms.notice.scNoticeTitle.placeholder': 'Title',

  'pages.update.sys.cms.notice.scNoticeContent.label': 'Content',
  'pages.update.sys.cms.notice.scNoticeContent.tip': 'Content',
  'pages.update.sys.cms.notice.scNoticeContent.placeholder': 'Content',

  'pages.update.sys.cms.notice.scNoticeIsNew.label': 'IsNew',
  'pages.update.sys.cms.notice.scNoticeIsNew.tip': 'IsNew',
  'pages.update.sys.cms.notice.scNoticeIsNew.placeholder': 'IsNew',

  'pages.update.sys.cms.notice.scNoticeManager.label': 'Manager',
  'pages.update.sys.cms.notice.scNoticeManager.tip': 'Manager',
  'pages.update.sys.cms.notice.scNoticeManager.placeholder': 'Manager',

  'pages.update.sys.cms.notice.scNoticeCreateDate.label': 'CreateDate',
  'pages.update.sys.cms.notice.scNoticeCreateDate.tip': 'CreateDate',
  'pages.update.sys.cms.notice.scNoticeCreateDate.placeholder': 'CreateDate',

  'pages.update.sys.cms.notice.scNoticeUpdateDate.label': 'UpdateDate',
  'pages.update.sys.cms.notice.scNoticeUpdateDate.tip': 'UpdateDate',
  'pages.update.sys.cms.notice.scNoticeUpdateDate.placeholder': 'UpdateDate',


  'pages.list.sys.cms.notice.form.title': 'Query',

  'pages.list.sys.cms.notice.scNoticeId.label': 'ID',
  'pages.list.sys.cms.notice.scNoticeId.tip': 'ID',
  'pages.list.sys.cms.notice.scNoticeId.placeholder': 'ID',

  'pages.list.sys.cms.notice.scNoticeTitle.label': 'Title',
  'pages.list.sys.cms.notice.scNoticeTitle.tip': 'Title',
  'pages.list.sys.cms.notice.scNoticeTitle.placeholder': 'Title',

  'pages.list.sys.cms.notice.scNoticeContent.label': 'Content',
  'pages.list.sys.cms.notice.scNoticeContent.tip': 'Content',
  'pages.list.sys.cms.notice.scNoticeContent.placeholder': 'Content',

  'pages.list.sys.cms.notice.scNoticeIsNew.label': 'IsNew',
  'pages.list.sys.cms.notice.scNoticeIsNew.tip': 'IsNew',
  'pages.list.sys.cms.notice.scNoticeIsNew.placeholder': 'IsNew',

  'pages.list.sys.cms.notice.scNoticeManager.label': 'Manager',
  'pages.list.sys.cms.notice.scNoticeManager.tip': 'Manager',
  'pages.list.sys.cms.notice.scNoticeManager.placeholder': 'Manager',

  'pages.list.sys.cms.notice.scNoticeCreateDate.label': 'CreateDate',
  'pages.list.sys.cms.notice.scNoticeCreateDate.tip': 'CreateDate',
  'pages.list.sys.cms.notice.scNoticeCreateDate.placeholder': 'CreateDate',

  'pages.list.sys.cms.notice.scNoticeUpdateDate.label': 'UpdateDate',
  'pages.list.sys.cms.notice.scNoticeUpdateDate.tip': 'UpdateDate',
  'pages.list.sys.cms.notice.scNoticeUpdateDate.placeholder': 'UpdateDate',


  'pages.list.sys.cms.notice.option.delete':'Delete',
  'pages.list.sys.cms.notice.batch.delete':'Batch delete',
  'pages.list.sys.cms.notice.item':'item',
  'pages.list.sys.cms.notice.chosen':'Has chosen',
  'pages.list.sys.cms.notice.new':'Create',
  'pages.list.sys.cms.notice.title':'Query',
  'pages.list.sys.cms.notice.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.cms.notice.option.edit':'Edit',
  'pages.list.sys.cms.notice.option.title':'Option',
  'pages.list.sys.cms.notice.index.label':'No.',
};
