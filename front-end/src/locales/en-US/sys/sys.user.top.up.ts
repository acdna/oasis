export default {
  'pages.create.sys.user.top.up.form.title': 'Create',
  
  'pages.create.sys.user.top.up.utuId.label': 'id',
  'pages.create.sys.user.top.up.utuId.tip': 'id',
  'pages.create.sys.user.top.up.utuId.placeholder': 'id',
  
  'pages.create.sys.user.top.up.utuLoginName.label': '用户登录名',
  'pages.create.sys.user.top.up.utuLoginName.tip': '用户登录名',
  'pages.create.sys.user.top.up.utuLoginName.placeholder': '用户登录名',
  
  'pages.create.sys.user.top.up.utuSerialNumber.label': '序列号',
  'pages.create.sys.user.top.up.utuSerialNumber.tip': '序列号',
  'pages.create.sys.user.top.up.utuSerialNumber.placeholder': '序列号',
  
  'pages.create.sys.user.top.up.utuBillingStartDate.label': '计费开始时间',
  'pages.create.sys.user.top.up.utuBillingStartDate.tip': '计费开始时间',
  'pages.create.sys.user.top.up.utuBillingStartDate.placeholder': '计费开始时间',
  
  'pages.create.sys.user.top.up.utuBillingEndDate.label': '计费结束时间',
  'pages.create.sys.user.top.up.utuBillingEndDate.tip': '计费结束时间',
  'pages.create.sys.user.top.up.utuBillingEndDate.placeholder': '计费结束时间',
  
  'pages.create.sys.user.top.up.utuState.label': '状态',
  'pages.create.sys.user.top.up.utuState.tip': '状态',
  'pages.create.sys.user.top.up.utuState.placeholder': '状态',
  
  'pages.create.sys.user.top.up.utuCreateDate.label': '创建日期',
  'pages.create.sys.user.top.up.utuCreateDate.tip': '创建日期',
  'pages.create.sys.user.top.up.utuCreateDate.placeholder': '创建日期',
  

  'pages.update.sys.user.top.up.form.title': 'Update',
  
  'pages.update.sys.user.top.up.utuId.label': 'id',
  'pages.update.sys.user.top.up.utuId.tip': 'id',
  'pages.update.sys.user.top.up.utuId.placeholder': 'id',
  
  'pages.update.sys.user.top.up.utuLoginName.label': '用户登录名',
  'pages.update.sys.user.top.up.utuLoginName.tip': '用户登录名',
  'pages.update.sys.user.top.up.utuLoginName.placeholder': '用户登录名',
  
  'pages.update.sys.user.top.up.utuSerialNumber.label': '序列号',
  'pages.update.sys.user.top.up.utuSerialNumber.tip': '序列号',
  'pages.update.sys.user.top.up.utuSerialNumber.placeholder': '序列号',
  
  'pages.update.sys.user.top.up.utuBillingStartDate.label': '计费开始时间',
  'pages.update.sys.user.top.up.utuBillingStartDate.tip': '计费开始时间',
  'pages.update.sys.user.top.up.utuBillingStartDate.placeholder': '计费开始时间',
  
  'pages.update.sys.user.top.up.utuBillingEndDate.label': '计费结束时间',
  'pages.update.sys.user.top.up.utuBillingEndDate.tip': '计费结束时间',
  'pages.update.sys.user.top.up.utuBillingEndDate.placeholder': '计费结束时间',
  
  'pages.update.sys.user.top.up.utuState.label': '状态',
  'pages.update.sys.user.top.up.utuState.tip': '状态',
  'pages.update.sys.user.top.up.utuState.placeholder': '状态',
  
  'pages.update.sys.user.top.up.utuCreateDate.label': '创建日期',
  'pages.update.sys.user.top.up.utuCreateDate.tip': '创建日期',
  'pages.update.sys.user.top.up.utuCreateDate.placeholder': '创建日期',
  

  'pages.list.sys.user.top.up.form.title': 'Query',
  
  'pages.list.sys.user.top.up.utuId.label': 'id',
  'pages.list.sys.user.top.up.utuId.tip': 'id',
  'pages.list.sys.user.top.up.utuId.placeholder': 'id',
  
  'pages.list.sys.user.top.up.utuLoginName.label': '用户登录名',
  'pages.list.sys.user.top.up.utuLoginName.tip': '用户登录名',
  'pages.list.sys.user.top.up.utuLoginName.placeholder': '用户登录名',
  
  'pages.list.sys.user.top.up.utuSerialNumber.label': '序列号',
  'pages.list.sys.user.top.up.utuSerialNumber.tip': '序列号',
  'pages.list.sys.user.top.up.utuSerialNumber.placeholder': '序列号',
  
  'pages.list.sys.user.top.up.utuBillingStartDate.label': '计费开始时间',
  'pages.list.sys.user.top.up.utuBillingStartDate.tip': '计费开始时间',
  'pages.list.sys.user.top.up.utuBillingStartDate.placeholder': '计费开始时间',
  
  'pages.list.sys.user.top.up.utuBillingEndDate.label': '计费结束时间',
  'pages.list.sys.user.top.up.utuBillingEndDate.tip': '计费结束时间',
  'pages.list.sys.user.top.up.utuBillingEndDate.placeholder': '计费结束时间',
  
  'pages.list.sys.user.top.up.utuState.label': '状态',
  'pages.list.sys.user.top.up.utuState.tip': '状态',
  'pages.list.sys.user.top.up.utuState.placeholder': '状态',
  
  'pages.list.sys.user.top.up.utuCreateDate.label': '创建日期',
  'pages.list.sys.user.top.up.utuCreateDate.tip': '创建日期',
  'pages.list.sys.user.top.up.utuCreateDate.placeholder': '创建日期',
  

  'pages.list.sys.user.top.up.option.delete':'Delete',
  'pages.list.sys.user.top.up.batch.delete':'Batch delete',
  'pages.list.sys.user.top.up.item':'item',
  'pages.list.sys.user.top.up.chosen':'Has chosen',
  'pages.list.sys.user.top.up.new':'Create',
  'pages.list.sys.user.top.up.title':'Query',
  'pages.list.sys.user.top.up.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.user.top.up.option.edit':'Edit',
  'pages.list.sys.user.top.up.option.title':'Option',
  'pages.list.sys.user.top.up.index.label':'No.',
};