export default {
  'pages.create.sys.instrument.form.title': 'Create',
  
  'pages.create.sys.instrument.insId.label': '仪器编号',
  'pages.create.sys.instrument.insId.tip': '仪器编号',
  'pages.create.sys.instrument.insId.placeholder': '仪器编号',
  
  'pages.create.sys.instrument.insBarcode.label': '仪器条码号',
  'pages.create.sys.instrument.insBarcode.tip': '仪器条码号',
  'pages.create.sys.instrument.insBarcode.placeholder': '仪器条码号',
  
  'pages.create.sys.instrument.insName.label': '仪器名称',
  'pages.create.sys.instrument.insName.tip': '仪器名称',
  'pages.create.sys.instrument.insName.placeholder': '仪器名称',
  
  'pages.create.sys.instrument.insModel.label': '仪器型号',
  'pages.create.sys.instrument.insModel.tip': '仪器型号',
  'pages.create.sys.instrument.insModel.placeholder': '仪器型号',
  
  'pages.create.sys.instrument.insManager.label': '仪器负责人',
  'pages.create.sys.instrument.insManager.tip': '仪器负责人',
  'pages.create.sys.instrument.insManager.placeholder': '仪器负责人',
  
  'pages.create.sys.instrument.insType.label': '仪器类型',
  'pages.create.sys.instrument.insType.tip': '仪器类型',
  'pages.create.sys.instrument.insType.placeholder': '仪器类型',
  
  'pages.create.sys.instrument.insRemark.label': '仪器描述',
  'pages.create.sys.instrument.insRemark.tip': '仪器描述',
  'pages.create.sys.instrument.insRemark.placeholder': '仪器描述',
  
  'pages.create.sys.instrument.insBuyDate.label': '仪器购买日期',
  'pages.create.sys.instrument.insBuyDate.tip': '仪器购买日期',
  'pages.create.sys.instrument.insBuyDate.placeholder': '仪器购买日期',
  
  'pages.create.sys.instrument.insMaintainPeriod.label': '仪器维护周期',
  'pages.create.sys.instrument.insMaintainPeriod.tip': '仪器维护周期',
  'pages.create.sys.instrument.insMaintainPeriod.placeholder': '仪器维护周期',
  
  'pages.create.sys.instrument.insMaker.label': '仪器厂商',
  'pages.create.sys.instrument.insMaker.tip': '仪器厂商',
  'pages.create.sys.instrument.insMaker.placeholder': '仪器厂商',
  
  'pages.create.sys.instrument.insPrice.label': '仪器价格',
  'pages.create.sys.instrument.insPrice.tip': '仪器价格',
  'pages.create.sys.instrument.insPrice.placeholder': '仪器价格',
  
  'pages.create.sys.instrument.insExtraProvide.label': 'insExtraProvide',
  'pages.create.sys.instrument.insExtraProvide.tip': 'insExtraProvide',
  'pages.create.sys.instrument.insExtraProvide.placeholder': 'insExtraProvide',
  
  'pages.create.sys.instrument.insCreateDate.label': '创建日期',
  'pages.create.sys.instrument.insCreateDate.tip': '创建日期',
  'pages.create.sys.instrument.insCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.instrument.insUpdateDate.label': '更新日期',
  'pages.create.sys.instrument.insUpdateDate.tip': '更新日期',
  'pages.create.sys.instrument.insUpdateDate.placeholder': '更新日期',
  

  'pages.update.sys.instrument.form.title': 'Update',
  
  'pages.update.sys.instrument.insId.label': '仪器编号',
  'pages.update.sys.instrument.insId.tip': '仪器编号',
  'pages.update.sys.instrument.insId.placeholder': '仪器编号',
  
  'pages.update.sys.instrument.insBarcode.label': '仪器条码号',
  'pages.update.sys.instrument.insBarcode.tip': '仪器条码号',
  'pages.update.sys.instrument.insBarcode.placeholder': '仪器条码号',
  
  'pages.update.sys.instrument.insName.label': '仪器名称',
  'pages.update.sys.instrument.insName.tip': '仪器名称',
  'pages.update.sys.instrument.insName.placeholder': '仪器名称',
  
  'pages.update.sys.instrument.insModel.label': '仪器型号',
  'pages.update.sys.instrument.insModel.tip': '仪器型号',
  'pages.update.sys.instrument.insModel.placeholder': '仪器型号',
  
  'pages.update.sys.instrument.insManager.label': '仪器负责人',
  'pages.update.sys.instrument.insManager.tip': '仪器负责人',
  'pages.update.sys.instrument.insManager.placeholder': '仪器负责人',
  
  'pages.update.sys.instrument.insType.label': '仪器类型',
  'pages.update.sys.instrument.insType.tip': '仪器类型',
  'pages.update.sys.instrument.insType.placeholder': '仪器类型',
  
  'pages.update.sys.instrument.insRemark.label': '仪器描述',
  'pages.update.sys.instrument.insRemark.tip': '仪器描述',
  'pages.update.sys.instrument.insRemark.placeholder': '仪器描述',
  
  'pages.update.sys.instrument.insBuyDate.label': '仪器购买日期',
  'pages.update.sys.instrument.insBuyDate.tip': '仪器购买日期',
  'pages.update.sys.instrument.insBuyDate.placeholder': '仪器购买日期',
  
  'pages.update.sys.instrument.insMaintainPeriod.label': '仪器维护周期',
  'pages.update.sys.instrument.insMaintainPeriod.tip': '仪器维护周期',
  'pages.update.sys.instrument.insMaintainPeriod.placeholder': '仪器维护周期',
  
  'pages.update.sys.instrument.insMaker.label': '仪器厂商',
  'pages.update.sys.instrument.insMaker.tip': '仪器厂商',
  'pages.update.sys.instrument.insMaker.placeholder': '仪器厂商',
  
  'pages.update.sys.instrument.insPrice.label': '仪器价格',
  'pages.update.sys.instrument.insPrice.tip': '仪器价格',
  'pages.update.sys.instrument.insPrice.placeholder': '仪器价格',
  
  'pages.update.sys.instrument.insExtraProvide.label': 'insExtraProvide',
  'pages.update.sys.instrument.insExtraProvide.tip': 'insExtraProvide',
  'pages.update.sys.instrument.insExtraProvide.placeholder': 'insExtraProvide',
  
  'pages.update.sys.instrument.insCreateDate.label': '创建日期',
  'pages.update.sys.instrument.insCreateDate.tip': '创建日期',
  'pages.update.sys.instrument.insCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.instrument.insUpdateDate.label': '更新日期',
  'pages.update.sys.instrument.insUpdateDate.tip': '更新日期',
  'pages.update.sys.instrument.insUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.instrument.form.title': 'Query',
  
  'pages.list.sys.instrument.insId.label': '仪器编号',
  'pages.list.sys.instrument.insId.tip': '仪器编号',
  'pages.list.sys.instrument.insId.placeholder': '仪器编号',
  
  'pages.list.sys.instrument.insBarcode.label': '仪器条码号',
  'pages.list.sys.instrument.insBarcode.tip': '仪器条码号',
  'pages.list.sys.instrument.insBarcode.placeholder': '仪器条码号',
  
  'pages.list.sys.instrument.insName.label': '仪器名称',
  'pages.list.sys.instrument.insName.tip': '仪器名称',
  'pages.list.sys.instrument.insName.placeholder': '仪器名称',
  
  'pages.list.sys.instrument.insModel.label': '仪器型号',
  'pages.list.sys.instrument.insModel.tip': '仪器型号',
  'pages.list.sys.instrument.insModel.placeholder': '仪器型号',
  
  'pages.list.sys.instrument.insManager.label': '仪器负责人',
  'pages.list.sys.instrument.insManager.tip': '仪器负责人',
  'pages.list.sys.instrument.insManager.placeholder': '仪器负责人',
  
  'pages.list.sys.instrument.insType.label': '仪器类型',
  'pages.list.sys.instrument.insType.tip': '仪器类型',
  'pages.list.sys.instrument.insType.placeholder': '仪器类型',
  
  'pages.list.sys.instrument.insRemark.label': '仪器描述',
  'pages.list.sys.instrument.insRemark.tip': '仪器描述',
  'pages.list.sys.instrument.insRemark.placeholder': '仪器描述',
  
  'pages.list.sys.instrument.insBuyDate.label': '仪器购买日期',
  'pages.list.sys.instrument.insBuyDate.tip': '仪器购买日期',
  'pages.list.sys.instrument.insBuyDate.placeholder': '仪器购买日期',
  
  'pages.list.sys.instrument.insMaintainPeriod.label': '仪器维护周期',
  'pages.list.sys.instrument.insMaintainPeriod.tip': '仪器维护周期',
  'pages.list.sys.instrument.insMaintainPeriod.placeholder': '仪器维护周期',
  
  'pages.list.sys.instrument.insMaker.label': '仪器厂商',
  'pages.list.sys.instrument.insMaker.tip': '仪器厂商',
  'pages.list.sys.instrument.insMaker.placeholder': '仪器厂商',
  
  'pages.list.sys.instrument.insPrice.label': '仪器价格',
  'pages.list.sys.instrument.insPrice.tip': '仪器价格',
  'pages.list.sys.instrument.insPrice.placeholder': '仪器价格',
  
  'pages.list.sys.instrument.insExtraProvide.label': 'insExtraProvide',
  'pages.list.sys.instrument.insExtraProvide.tip': 'insExtraProvide',
  'pages.list.sys.instrument.insExtraProvide.placeholder': 'insExtraProvide',
  
  'pages.list.sys.instrument.insCreateDate.label': '创建日期',
  'pages.list.sys.instrument.insCreateDate.tip': '创建日期',
  'pages.list.sys.instrument.insCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.instrument.insUpdateDate.label': '更新日期',
  'pages.list.sys.instrument.insUpdateDate.tip': '更新日期',
  'pages.list.sys.instrument.insUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.instrument.option.delete':'Delete',
  'pages.list.sys.instrument.batch.delete':'Batch delete',
  'pages.list.sys.instrument.item':'item',
  'pages.list.sys.instrument.chosen':'Has chosen',
  'pages.list.sys.instrument.new':'Create',
  'pages.list.sys.instrument.title':'Query',
  'pages.list.sys.instrument.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.instrument.option.edit':'Edit',
  'pages.list.sys.instrument.option.title':'Option',
  'pages.list.sys.instrument.index.label':'No.',
};