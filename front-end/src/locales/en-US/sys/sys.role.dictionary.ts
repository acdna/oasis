export default {
  'pages.sys.role.dictionary.relate.title':'Dictionary authorization',
  'pages.sys.role.dictionary.relate.chosen':'Selected',
  'pages.sys.role.dictionary.relate.item':'Item',
  'pages.sys.role.dictionary.relate.cancel.all':'Deselect',
  'pages.sys.role.dictionary.relate.cancel':'Cancel',
  'pages.sys.role.dictionary.relate.submit':'Submit',
  'pages.sys.role.dictionary.relate.loading':'Authorizing',
  'pages.sys.role.dictionary.relate.success':'Authorization success',
  'pages.sys.role.dictionary.relate.error':'Authorization failed, please try again',

  'pages.create.sys.role.dictionary.form.title': 'Create',

  'pages.create.sys.role.dictionary.rdId.label': '角色字典ID',
  'pages.create.sys.role.dictionary.rdId.tip': '角色字典ID',
  'pages.create.sys.role.dictionary.rdId.placeholder': '角色字典ID',

  'pages.create.sys.role.dictionary.rdRoleId.label': '角色ID',
  'pages.create.sys.role.dictionary.rdRoleId.tip': '角色ID',
  'pages.create.sys.role.dictionary.rdRoleId.placeholder': '角色ID',

  'pages.create.sys.role.dictionary.rdDictionaryId.label': '字典ID',
  'pages.create.sys.role.dictionary.rdDictionaryId.tip': '字典ID',
  'pages.create.sys.role.dictionary.rdDictionaryId.placeholder': '字典ID',


  'pages.update.sys.role.dictionary.form.title': 'Update',

  'pages.update.sys.role.dictionary.rdId.label': '角色字典ID',
  'pages.update.sys.role.dictionary.rdId.tip': '角色字典ID',
  'pages.update.sys.role.dictionary.rdId.placeholder': '角色字典ID',

  'pages.update.sys.role.dictionary.rdRoleId.label': '角色ID',
  'pages.update.sys.role.dictionary.rdRoleId.tip': '角色ID',
  'pages.update.sys.role.dictionary.rdRoleId.placeholder': '角色ID',

  'pages.update.sys.role.dictionary.rdDictionaryId.label': '字典ID',
  'pages.update.sys.role.dictionary.rdDictionaryId.tip': '字典ID',
  'pages.update.sys.role.dictionary.rdDictionaryId.placeholder': '字典ID',


  'pages.list.sys.role.dictionary.form.title': 'Query',

  'pages.list.sys.role.dictionary.rdId.label': '角色字典ID',
  'pages.list.sys.role.dictionary.rdId.tip': '角色字典ID',
  'pages.list.sys.role.dictionary.rdId.placeholder': '角色字典ID',

  'pages.list.sys.role.dictionary.rdRoleId.label': '角色ID',
  'pages.list.sys.role.dictionary.rdRoleId.tip': '角色ID',
  'pages.list.sys.role.dictionary.rdRoleId.placeholder': '角色ID',

  'pages.list.sys.role.dictionary.rdDictionaryId.label': '字典ID',
  'pages.list.sys.role.dictionary.rdDictionaryId.tip': '字典ID',
  'pages.list.sys.role.dictionary.rdDictionaryId.placeholder': '字典ID',


  'pages.list.sys.role.dictionary.option.delete':'Delete',
  'pages.list.sys.role.dictionary.batch.delete':'Batch delete',
  'pages.list.sys.role.dictionary.item':'item',
  'pages.list.sys.role.dictionary.chosen':'Has chosen',
  'pages.list.sys.role.dictionary.new':'Create',
  'pages.list.sys.role.dictionary.title':'Query',
  'pages.list.sys.role.dictionary.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.role.dictionary.option.edit':'Edit',
  'pages.list.sys.role.dictionary.option.title':'Option',
  'pages.list.sys.role.dictionary.index.label':'No.',
};
