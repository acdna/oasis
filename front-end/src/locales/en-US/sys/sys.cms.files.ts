export default {
  'pages.create.sys.cms.files.form.title': 'Create',

  'pages.create.sys.cms.files.scFileId.label': '文件编号',
  'pages.create.sys.cms.files.scFileId.tip': '文件编号',
  'pages.create.sys.cms.files.scFileId.placeholder': '文件编号',

  'pages.create.sys.cms.files.scFileName.label': 'FileName',
  'pages.create.sys.cms.files.scFileName.tip': 'FileName',
  'pages.create.sys.cms.files.scFileName.placeholder': 'FileName',

  'pages.create.sys.cms.files.scFileNameCn.label': 'FileNameEn',
  'pages.create.sys.cms.files.scFileNameCn.tip': 'FileNameEn',
  'pages.create.sys.cms.files.scFileNameCn.placeholder': 'FileNameEn',

  'pages.create.sys.cms.files.scFileType.label': 'FileType',
  'pages.create.sys.cms.files.scFileType.tip': 'FileType',
  'pages.create.sys.cms.files.scFileType.placeholder': 'FileType',

  'pages.create.sys.cms.files.scFilePath.label': 'FilePath',
  'pages.create.sys.cms.files.scFilePath.tip': 'FilePath',
  'pages.create.sys.cms.files.scFilePath.placeholder': 'FilePath',

  'pages.create.sys.cms.files.scFileIsNew.label': 'IsNew',
  'pages.create.sys.cms.files.scFileIsNew.tip': 'IsNew',
  'pages.create.sys.cms.files.scFileIsNew.placeholder': 'IsNew',

  'pages.create.sys.cms.files.scFileManager.label': 'Manager',
  'pages.create.sys.cms.files.scFileManager.tip': 'Manager',
  'pages.create.sys.cms.files.scFileManager.placeholder': 'Manager',

  'pages.create.sys.cms.files.scFileCreateDate.label': 'CreateDate',
  'pages.create.sys.cms.files.scFileCreateDate.tip': 'CreateDate',
  'pages.create.sys.cms.files.scFileCreateDate.placeholder': 'CreateDate',

  'pages.create.sys.cms.files.scFielUpdateDate.label': 'UpdateDate',
  'pages.create.sys.cms.files.scFielUpdateDate.tip': 'UpdateDate',
  'pages.create.sys.cms.files.scFielUpdateDate.placeholder': 'UpdateDate',

  'pages.create.sys.cms.files.scFileSpecies.label': 'Species',
  'pages.create.sys.cms.files.scFileSpecies.tip': 'Species',
  'pages.create.sys.cms.files.scFileSpecies.placeholder': 'Species',


  'pages.update.sys.cms.files.form.title': 'Update',

  'pages.update.sys.cms.files.scFileId.label': '文件编号',
  'pages.update.sys.cms.files.scFileId.tip': '文件编号',
  'pages.update.sys.cms.files.scFileId.placeholder': '文件编号',

  'pages.update.sys.cms.files.scFileName.label': 'FileName',
  'pages.update.sys.cms.files.scFileName.tip': 'FileName',
  'pages.update.sys.cms.files.scFileName.placeholder': 'FileName',

  'pages.update.sys.cms.files.scFileNameCn.label': 'FileNameEn',
  'pages.update.sys.cms.files.scFileNameCn.tip': 'FileNameEn',
  'pages.update.sys.cms.files.scFileNameCn.placeholder': 'FileNameEn',

  'pages.update.sys.cms.files.scFileType.label': 'FileType',
  'pages.update.sys.cms.files.scFileType.tip': 'FileType',
  'pages.update.sys.cms.files.scFileType.placeholder': 'FileType',

  'pages.update.sys.cms.files.scFilePath.label': 'FilePath',
  'pages.update.sys.cms.files.scFilePath.tip': 'FilePath',
  'pages.update.sys.cms.files.scFilePath.placeholder': 'FilePath',

  'pages.update.sys.cms.files.scFileIsNew.label': 'IsNew',
  'pages.update.sys.cms.files.scFileIsNew.tip': 'IsNew',
  'pages.update.sys.cms.files.scFileIsNew.placeholder': 'IsNew',

  'pages.update.sys.cms.files.scFileManager.label': 'Manager',
  'pages.update.sys.cms.files.scFileManager.tip': 'Manager',
  'pages.update.sys.cms.files.scFileManager.placeholder': 'Manager',

  'pages.update.sys.cms.files.scFileCreateDate.label': 'CreateDate',
  'pages.update.sys.cms.files.scFileCreateDate.tip': 'CreateDate',
  'pages.update.sys.cms.files.scFileCreateDate.placeholder': 'CreateDate',

  'pages.update.sys.cms.files.scFielUpdateDate.label': 'UpdateDate',
  'pages.update.sys.cms.files.scFielUpdateDate.tip': 'UpdateDate',
  'pages.update.sys.cms.files.scFielUpdateDate.placeholder': 'UpdateDate',

  'pages.update.sys.cms.files.scFileSpecies.label': 'Species',
  'pages.update.sys.cms.files.scFileSpecies.tip': 'Species',
  'pages.update.sys.cms.files.scFileSpecies.placeholder': 'Species',


  'pages.list.sys.cms.files.form.title': 'Query',

  'pages.list.sys.cms.files.scFileId.label': '文件编号',
  'pages.list.sys.cms.files.scFileId.tip': '文件编号',
  'pages.list.sys.cms.files.scFileId.placeholder': '文件编号',

  'pages.list.sys.cms.files.scFileName.label': 'FileName',
  'pages.list.sys.cms.files.scFileName.tip': 'FileName',
  'pages.list.sys.cms.files.scFileName.placeholder': 'FileName',

  'pages.list.sys.cms.files.scFileNameCn.label': 'FileNameEn',
  'pages.list.sys.cms.files.scFileNameCn.tip': 'FileNameEn',
  'pages.list.sys.cms.files.scFileNameCn.placeholder': 'FileNameEn',

  'pages.list.sys.cms.files.scFileType.label': 'FileType',
  'pages.list.sys.cms.files.scFileType.tip': 'FileType',
  'pages.list.sys.cms.files.scFileType.placeholder': 'FileType',

  'pages.list.sys.cms.files.scFilePath.label': 'FilePath',
  'pages.list.sys.cms.files.scFilePath.tip': 'FilePath',
  'pages.list.sys.cms.files.scFilePath.placeholder': 'FilePath',

  'pages.list.sys.cms.files.scFileIsNew.label': 'IsNew',
  'pages.list.sys.cms.files.scFileIsNew.tip': 'IsNew',
  'pages.list.sys.cms.files.scFileIsNew.placeholder': 'IsNew',

  'pages.list.sys.cms.files.scFileManager.label': 'Manager',
  'pages.list.sys.cms.files.scFileManager.tip': 'Manager',
  'pages.list.sys.cms.files.scFileManager.placeholder': 'Manager',

  'pages.list.sys.cms.files.scFileCreateDate.label': 'CreateDate',
  'pages.list.sys.cms.files.scFileCreateDate.tip': 'CreateDate',
  'pages.list.sys.cms.files.scFileCreateDate.placeholder': 'CreateDate',

  'pages.list.sys.cms.files.scFielUpdateDate.label': 'UpdateDate',
  'pages.list.sys.cms.files.scFielUpdateDate.tip': 'UpdateDate',
  'pages.list.sys.cms.files.scFielUpdateDate.placeholder': 'UpdateDate',

  'pages.list.sys.cms.files.scFileSpecies.label': 'Species',
  'pages.list.sys.cms.files.scFileSpecies.tip': 'Species',
  'pages.list.sys.cms.files.scFileSpecies.placeholder': 'Species',


  'pages.list.sys.cms.files.option.delete':'Delete',
  'pages.list.sys.cms.files.batch.delete':'Batch delete',
  'pages.list.sys.cms.files.item':'item',
  'pages.list.sys.cms.files.chosen':'Has chosen',
  'pages.list.sys.cms.files.new':'Create',
  'pages.list.sys.cms.files.title':'Query',
  'pages.list.sys.cms.files.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.cms.files.option.edit':'Edit',
  'pages.list.sys.cms.files.option.title':'Option',
  'pages.list.sys.cms.files.index.label':'No.',
  'pages.list.sys.cms.files.index.download':'download',

  'pages.create.sys.cms.files.title': 'Create',
  'pages.update.sys.cms.files.title': 'Update',
  'pages.create.sys.cms.files.cancel': 'Cancel',
  'pages.update.sys.cms.files.cancel': 'Cancel',
  'pages.create.sys.cms.files.submit': 'Submit',
  'pages.update.sys.cms.files.submit': 'Submit',
};
