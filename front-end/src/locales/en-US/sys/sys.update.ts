export default {
  'pages.create.sys.update.form.title': 'Create',

  'pages.create.sys.update.updateId.label': '系统升级日志主键ID',
  'pages.create.sys.update.updateId.tip': '系统升级日志主键ID',
  'pages.create.sys.update.updateId.placeholder': '系统升级日志主键ID',

  'pages.create.sys.update.updateVersion.label': 'Version',
  'pages.create.sys.update.updateVersion.tip': 'Version',
  'pages.create.sys.update.updateVersion.placeholder': 'Version',

  'pages.create.sys.update.updateContent.label': 'Content',
  'pages.create.sys.update.updateContent.tip': 'Content',
  'pages.create.sys.update.updateContent.placeholder': 'Content',

  'pages.create.sys.update.updateManager.label': 'Manager',
  'pages.create.sys.update.updateManager.tip': 'Manager',
  'pages.create.sys.update.updateManager.placeholder': 'Manager',

  'pages.create.sys.update.updateCreateTime.label': 'CreateDate',
  'pages.create.sys.update.updateCreateTime.tip': 'CreateDate',
  'pages.create.sys.update.updateCreateTime.placeholder': 'CreateDate',


  'pages.update.sys.update.form.title': 'Update',

  'pages.update.sys.update.updateId.label': '系统升级日志主键ID',
  'pages.update.sys.update.updateId.tip': '系统升级日志主键ID',
  'pages.update.sys.update.updateId.placeholder': '系统升级日志主键ID',

  'pages.update.sys.update.updateVersion.label': 'Version',
  'pages.update.sys.update.updateVersion.tip': 'Version',
  'pages.update.sys.update.updateVersion.placeholder': 'Version',

  'pages.update.sys.update.updateContent.label': 'Content',
  'pages.update.sys.update.updateContent.tip': 'Content',
  'pages.update.sys.update.updateContent.placeholder': 'Content',

  'pages.update.sys.update.updateManager.label': 'Manager',
  'pages.update.sys.update.updateManager.tip': 'Manager',
  'pages.update.sys.update.updateManager.placeholder': 'Manager',

  'pages.update.sys.update.updateCreateTime.label': 'CreateDate',
  'pages.update.sys.update.updateCreateTime.tip': 'CreateDate',
  'pages.update.sys.update.updateCreateTime.placeholder': 'CreateDate',


  'pages.list.sys.update.form.title': 'Query',

  'pages.list.sys.update.updateId.label': '系统升级日志主键ID',
  'pages.list.sys.update.updateId.tip': '系统升级日志主键ID',
  'pages.list.sys.update.updateId.placeholder': '系统升级日志主键ID',

  'pages.list.sys.update.updateVersion.label': 'Version',
  'pages.list.sys.update.updateVersion.tip': 'Version',
  'pages.list.sys.update.updateVersion.placeholder': 'Version',

  'pages.list.sys.update.updateContent.label': 'Content',
  'pages.list.sys.update.updateContent.tip': 'Content',
  'pages.list.sys.update.updateContent.placeholder': 'Content',

  'pages.list.sys.update.updateManager.label': 'Manager',
  'pages.list.sys.update.updateManager.tip': 'Manager',
  'pages.list.sys.update.updateManager.placeholder': 'Manager',

  'pages.list.sys.update.updateCreateTime.label': 'CreateDate',
  'pages.list.sys.update.updateCreateTime.tip': 'CreateDate',
  'pages.list.sys.update.updateCreateTime.placeholder': 'CreateDate',


  'pages.list.sys.update.option.delete':'Delete',
  'pages.list.sys.update.batch.delete':'Batch delete',
  'pages.list.sys.update.item':'item',
  'pages.list.sys.update.chosen':'Has chosen',
  'pages.list.sys.update.new':'Create',
  'pages.list.sys.update.title':'Query',
  'pages.list.sys.update.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.update.option.edit':'Edit',
  'pages.list.sys.update.option.title':'Option',
  'pages.list.sys.update.index.label':'No.',
};
