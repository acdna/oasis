export default {
  'pages.create.sys.tip.form.title': 'Create',
  
  'pages.create.sys.tip.tipId.label': '提示信息ID',
  'pages.create.sys.tip.tipId.tip': '提示信息ID',
  'pages.create.sys.tip.tipId.placeholder': '提示信息ID',
  
  'pages.create.sys.tip.tipCount.label': '提示次数',
  'pages.create.sys.tip.tipCount.tip': '提示次数',
  'pages.create.sys.tip.tipCount.placeholder': '提示次数',
  
  'pages.create.sys.tip.tipContents.label': '提示信息内容',
  'pages.create.sys.tip.tipContents.tip': '提示信息内容',
  'pages.create.sys.tip.tipContents.placeholder': '提示信息内容',
  
  'pages.create.sys.tip.tipManager.label': '所属者',
  'pages.create.sys.tip.tipManager.tip': '所属者',
  'pages.create.sys.tip.tipManager.placeholder': '所属者',
  
  'pages.create.sys.tip.tipAudience.label': '消息受众',
  'pages.create.sys.tip.tipAudience.tip': '消息受众',
  'pages.create.sys.tip.tipAudience.placeholder': '消息受众',
  
  'pages.create.sys.tip.tipCreateDate.label': '创建日期',
  'pages.create.sys.tip.tipCreateDate.tip': '创建日期',
  'pages.create.sys.tip.tipCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.tip.tipUpdateDate.label': '更新日期',
  'pages.create.sys.tip.tipUpdateDate.tip': '更新日期',
  'pages.create.sys.tip.tipUpdateDate.placeholder': '更新日期',
  

  'pages.update.sys.tip.form.title': 'Update',
  
  'pages.update.sys.tip.tipId.label': '提示信息ID',
  'pages.update.sys.tip.tipId.tip': '提示信息ID',
  'pages.update.sys.tip.tipId.placeholder': '提示信息ID',
  
  'pages.update.sys.tip.tipCount.label': '提示次数',
  'pages.update.sys.tip.tipCount.tip': '提示次数',
  'pages.update.sys.tip.tipCount.placeholder': '提示次数',
  
  'pages.update.sys.tip.tipContents.label': '提示信息内容',
  'pages.update.sys.tip.tipContents.tip': '提示信息内容',
  'pages.update.sys.tip.tipContents.placeholder': '提示信息内容',
  
  'pages.update.sys.tip.tipManager.label': '所属者',
  'pages.update.sys.tip.tipManager.tip': '所属者',
  'pages.update.sys.tip.tipManager.placeholder': '所属者',
  
  'pages.update.sys.tip.tipAudience.label': '消息受众',
  'pages.update.sys.tip.tipAudience.tip': '消息受众',
  'pages.update.sys.tip.tipAudience.placeholder': '消息受众',
  
  'pages.update.sys.tip.tipCreateDate.label': '创建日期',
  'pages.update.sys.tip.tipCreateDate.tip': '创建日期',
  'pages.update.sys.tip.tipCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.tip.tipUpdateDate.label': '更新日期',
  'pages.update.sys.tip.tipUpdateDate.tip': '更新日期',
  'pages.update.sys.tip.tipUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.tip.form.title': 'Query',
  
  'pages.list.sys.tip.tipId.label': '提示信息ID',
  'pages.list.sys.tip.tipId.tip': '提示信息ID',
  'pages.list.sys.tip.tipId.placeholder': '提示信息ID',
  
  'pages.list.sys.tip.tipCount.label': '提示次数',
  'pages.list.sys.tip.tipCount.tip': '提示次数',
  'pages.list.sys.tip.tipCount.placeholder': '提示次数',
  
  'pages.list.sys.tip.tipContents.label': '提示信息内容',
  'pages.list.sys.tip.tipContents.tip': '提示信息内容',
  'pages.list.sys.tip.tipContents.placeholder': '提示信息内容',
  
  'pages.list.sys.tip.tipManager.label': '所属者',
  'pages.list.sys.tip.tipManager.tip': '所属者',
  'pages.list.sys.tip.tipManager.placeholder': '所属者',
  
  'pages.list.sys.tip.tipAudience.label': '消息受众',
  'pages.list.sys.tip.tipAudience.tip': '消息受众',
  'pages.list.sys.tip.tipAudience.placeholder': '消息受众',
  
  'pages.list.sys.tip.tipCreateDate.label': '创建日期',
  'pages.list.sys.tip.tipCreateDate.tip': '创建日期',
  'pages.list.sys.tip.tipCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.tip.tipUpdateDate.label': '更新日期',
  'pages.list.sys.tip.tipUpdateDate.tip': '更新日期',
  'pages.list.sys.tip.tipUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.tip.option.delete':'Delete',
  'pages.list.sys.tip.batch.delete':'Batch delete',
  'pages.list.sys.tip.item':'item',
  'pages.list.sys.tip.chosen':'Has chosen',
  'pages.list.sys.tip.new':'Create',
  'pages.list.sys.tip.title':'Query',
  'pages.list.sys.tip.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.tip.option.edit':'Edit',
  'pages.list.sys.tip.option.title':'Option',
  'pages.list.sys.tip.index.label':'No.',
};