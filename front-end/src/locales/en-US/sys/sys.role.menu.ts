export default {
  'pages.sys.role.menu.relate.title':'Menu authorization',
  'pages.sys.role.menu.relate.chosen':'Selected',
  'pages.sys.role.menu.relate.item':'Item',
  'pages.sys.role.menu.relate.cancel.all':'Deselect',
  'pages.sys.role.menu.relate.cancel':'Cancel',
  'pages.sys.role.menu.relate.submit':'Submit',
  'pages.sys.role.menu.relate.loading':'Authorizing',
  'pages.sys.role.menu.relate.success':'Authorization success',
  'pages.sys.role.menu.relate.error':'Authorization failed, please try again',

  'pages.create.sys.role.menu.form.title': 'Create',

  'pages.create.sys.role.menu.rmId.label': '角色菜单ID',
  'pages.create.sys.role.menu.rmId.tip': '角色菜单ID',
  'pages.create.sys.role.menu.rmId.placeholder': '角色菜单ID',

  'pages.create.sys.role.menu.rmRoleId.label': '角色ID',
  'pages.create.sys.role.menu.rmRoleId.tip': '角色ID',
  'pages.create.sys.role.menu.rmRoleId.placeholder': '角色ID',

  'pages.create.sys.role.menu.rmMenuId.label': '菜单ID',
  'pages.create.sys.role.menu.rmMenuId.tip': '菜单ID',
  'pages.create.sys.role.menu.rmMenuId.placeholder': '菜单ID',


  'pages.update.sys.role.menu.form.title': 'Update',

  'pages.update.sys.role.menu.rmId.label': '角色菜单ID',
  'pages.update.sys.role.menu.rmId.tip': '角色菜单ID',
  'pages.update.sys.role.menu.rmId.placeholder': '角色菜单ID',

  'pages.update.sys.role.menu.rmRoleId.label': '角色ID',
  'pages.update.sys.role.menu.rmRoleId.tip': '角色ID',
  'pages.update.sys.role.menu.rmRoleId.placeholder': '角色ID',

  'pages.update.sys.role.menu.rmMenuId.label': '菜单ID',
  'pages.update.sys.role.menu.rmMenuId.tip': '菜单ID',
  'pages.update.sys.role.menu.rmMenuId.placeholder': '菜单ID',


  'pages.list.sys.role.menu.form.title': 'Query',

  'pages.list.sys.role.menu.rmId.label': '角色菜单ID',
  'pages.list.sys.role.menu.rmId.tip': '角色菜单ID',
  'pages.list.sys.role.menu.rmId.placeholder': '角色菜单ID',

  'pages.list.sys.role.menu.rmRoleId.label': '角色ID',
  'pages.list.sys.role.menu.rmRoleId.tip': '角色ID',
  'pages.list.sys.role.menu.rmRoleId.placeholder': '角色ID',

  'pages.list.sys.role.menu.rmMenuId.label': '菜单ID',
  'pages.list.sys.role.menu.rmMenuId.tip': '菜单ID',
  'pages.list.sys.role.menu.rmMenuId.placeholder': '菜单ID',


  'pages.list.sys.role.menu.option.delete':'Delete',
  'pages.list.sys.role.menu.batch.delete':'Batch delete',
  'pages.list.sys.role.menu.item':'item',
  'pages.list.sys.role.menu.chosen':'Has chosen',
  'pages.list.sys.role.menu.new':'Create',
  'pages.list.sys.role.menu.title':'Query',
  'pages.list.sys.role.menu.option.delete.confirm':'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.role.menu.option.edit':'Edit',
  'pages.list.sys.role.menu.option.title':'Option',
  'pages.list.sys.role.menu.index.label':'No.',
};
