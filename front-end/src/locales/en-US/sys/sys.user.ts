export default {
  'pages.create.sys.user.title': 'Create',
  'pages.update.sys.user.title': 'Update',
  'pages.create.sys.user.cancel': 'Cancel',
  'pages.update.sys.user.cancel': 'Cancel',
  'pages.create.sys.user.submit': 'Submit',
  'pages.update.sys.user.submit': 'Submit',
  'pages.create.sys.user.form.title': 'Create',

  'pages.create.sys.user.userId.label': '用户ID',
  'pages.create.sys.user.userId.tip': '用户ID',
  'pages.create.sys.user.userId.placeholder': '用户ID',

  'pages.create.sys.user.userLoginName.label': 'Login Name',
  'pages.create.sys.user.userLoginName.tip': 'Login Name',
  'pages.create.sys.user.userLoginName.placeholder': 'Login Name',

  'pages.create.sys.user.userPassword.label': 'Password',
  'pages.create.sys.user.userPassword.tip': 'Password',
  'pages.create.sys.user.userPassword.placeholder': 'Password',

  'pages.create.sys.user.userPasswordPrivate.label': 'Private Password',
  'pages.create.sys.user.userPasswordPrivate.tip': 'Private Password',
  'pages.create.sys.user.userPasswordPrivate.placeholder': 'Private Password',

  'pages.create.sys.user.userNameAbbr.label': 'Name Abbr',
  'pages.create.sys.user.userNameAbbr.tip': 'Name Abbr',
  'pages.create.sys.user.userNameAbbr.placeholder': 'Name Abbr',

  'pages.create.sys.user.userName.label': 'Name',
  'pages.create.sys.user.userName.tip': 'Name',
  'pages.create.sys.user.userName.placeholder': 'Name',

  'pages.create.sys.user.userNameEn.label': 'NameEn',
  'pages.create.sys.user.userNameEn.tip': 'NameEn',
  'pages.create.sys.user.userNameEn.placeholder': 'NameEn',

  'pages.create.sys.user.userSex.label': 'Gender',
  'pages.create.sys.user.userSex.tip': 'Gender',
  'pages.create.sys.user.userSex.placeholder': 'Gender',

  'pages.create.sys.user.userPhone.label': 'Phone',
  'pages.create.sys.user.userPhone.tip': 'Phone',
  'pages.create.sys.user.userPhone.placeholder': 'Phone',

  'pages.create.sys.user.userEmail.label': 'Email',
  'pages.create.sys.user.userEmail.tip': 'Email',
  'pages.create.sys.user.userEmail.placeholder': 'Email',

  'pages.create.sys.user.userImageUrl.label': 'Portrait',
  'pages.create.sys.user.userImageUrl.tip': 'Portrait',
  'pages.create.sys.user.userImageUrl.placeholder': 'Portrait',

  'pages.create.sys.user.userSamSpecies.label': 'Species',
  'pages.create.sys.user.userSamSpecies.tip': 'Species',
  'pages.create.sys.user.userSamSpecies.placeholder': 'Species',

  'pages.create.sys.user.userRoles.label': 'Roles',
  'pages.create.sys.user.userRoles.tip': 'Roles',
  'pages.create.sys.user.userRoles.placeholder': 'Roles',

  'pages.create.sys.user.userOrder.label': 'Order',
  'pages.create.sys.user.userOrder.tip': 'Order',
  'pages.create.sys.user.userOrder.placeholder': 'Order',

  'pages.create.sys.user.userState.label': 'State',
  'pages.create.sys.user.userState.tip': 'State',
  'pages.create.sys.user.userState.placeholder': 'State',

  'pages.create.sys.user.userCode.label': 'User Code',
  'pages.create.sys.user.userCode.tip': 'User Code',
  'pages.create.sys.user.userCode.placeholder': 'User Code',

  'pages.create.sys.user.userCreateDate.label': 'CreateDate',
  'pages.create.sys.user.userCreateDate.tip': 'CreateDate',
  'pages.create.sys.user.userCreateDate.placeholder': 'CreateDate',

  'pages.create.sys.user.userUpdateDate.label': 'UpdateDate',
  'pages.create.sys.user.userUpdateDate.tip': 'UpdateDate',
  'pages.create.sys.user.userUpdateDate.placeholder': 'UpdateDate',

  'pages.create.sys.user.userLastLoginTime.label': 'LastLoginTime',
  'pages.create.sys.user.userLastLoginTime.tip': 'LastLoginTime',
  'pages.create.sys.user.userLastLoginTime.placeholder': 'LastLoginTime',

  'pages.create.sys.user.userLastLoginIp.label': 'LastLoginIp',
  'pages.create.sys.user.userLastLoginIp.tip': 'LastLoginIp',
  'pages.create.sys.user.userLastLoginIp.placeholder': 'LastLoginIp',

  'pages.create.sys.user.userUnit.label': 'Dept',
  'pages.create.sys.user.userUnit.tip': 'Dept',
  'pages.create.sys.user.userUnit.placeholder': 'Dept',

  'pages.create.sys.user.userJobTitle.label': 'Job',
  'pages.create.sys.user.userJobTitle.tip': 'Job',
  'pages.create.sys.user.userJobTitle.placeholder': 'Job',

  'pages.create.sys.user.userAddr.label': 'Address',
  'pages.create.sys.user.userAddr.tip': 'Address',
  'pages.create.sys.user.userAddr.placeholder': 'Address',

  'pages.create.sys.user.userPostcode.label': 'Postcode',
  'pages.create.sys.user.userPostcode.tip': 'Postcode',
  'pages.create.sys.user.userPostcode.placeholder': 'Postcode',

  'pages.create.sys.user.userCurrSpecies.label': 'CurrSpecies',
  'pages.create.sys.user.userCurrSpecies.tip': 'CurrSpecies',
  'pages.create.sys.user.userCurrSpecies.placeholder': 'CurrSpecies',

  'pages.update.sys.user.form.title': 'Update',

  'pages.update.sys.user.userId.label': '用户ID',
  'pages.update.sys.user.userId.tip': '用户ID',
  'pages.update.sys.user.userId.placeholder': '用户ID',

  'pages.update.sys.user.userLoginName.label': 'Login Name',
  'pages.update.sys.user.userLoginName.tip': 'Login Name',
  'pages.update.sys.user.userLoginName.placeholder': 'Login Name',

  'pages.update.sys.user.userPassword.label': 'Password',
  'pages.update.sys.user.userPassword.tip': 'Password',
  'pages.update.sys.user.userPassword.placeholder': 'Password',

  'pages.update.sys.user.userPasswordPrivate.label': 'Private Password',
  'pages.update.sys.user.userPasswordPrivate.tip': 'Private Password',
  'pages.update.sys.user.userPasswordPrivate.placeholder': 'Private Password',

  'pages.update.sys.user.userNameAbbr.label': 'Name Abbr',
  'pages.update.sys.user.userNameAbbr.tip': 'Name Abbr',
  'pages.update.sys.user.userNameAbbr.placeholder': 'Name Abbr',

  'pages.update.sys.user.userName.label': 'Name',
  'pages.update.sys.user.userName.tip': 'Name',
  'pages.update.sys.user.userName.placeholder': 'Name',

  'pages.update.sys.user.userNameEn.label': 'NameEn',
  'pages.update.sys.user.userNameEn.tip': 'NameEn',
  'pages.update.sys.user.userNameEn.placeholder': 'NameEn',

  'pages.update.sys.user.userSex.label': 'Gender',
  'pages.update.sys.user.userSex.tip': 'Gender',
  'pages.update.sys.user.userSex.placeholder': 'Gender',

  'pages.update.sys.user.userPhone.label': 'Phone',
  'pages.update.sys.user.userPhone.tip': 'Phone',
  'pages.update.sys.user.userPhone.placeholder': 'Phone',

  'pages.update.sys.user.userEmail.label': 'Email',
  'pages.update.sys.user.userEmail.tip': 'Email',
  'pages.update.sys.user.userEmail.placeholder': 'Email',

  'pages.update.sys.user.userImageUrl.label': 'Portrait',
  'pages.update.sys.user.userImageUrl.tip': 'Portrait',
  'pages.update.sys.user.userImageUrl.placeholder': 'Portrait',

  'pages.update.sys.user.userOrder.label': 'Order',
  'pages.update.sys.user.userOrder.tip': 'Order',
  'pages.update.sys.user.userOrder.placeholder': 'Order',

  'pages.update.sys.user.userState.label': 'State',
  'pages.update.sys.user.userState.tip': 'State',
  'pages.update.sys.user.userState.placeholder': 'State',

  'pages.update.sys.user.userCode.label': 'User Code',
  'pages.update.sys.user.userCode.tip': 'User Code',
  'pages.update.sys.user.userCode.placeholder': 'User Code',

  'pages.update.sys.user.userRoles.label': 'Roles',
  'pages.update.sys.user.userRoles.tip': 'Roles',
  'pages.update.sys.user.userRoles.placeholder': 'Roles',

  'pages.update.sys.user.userSamSpecies.label': 'Species',
  'pages.update.sys.user.userSamSpecies.tip': 'Species',
  'pages.update.sys.user.userSamSpecies.placeholder': 'Species',

  'pages.update.sys.user.userCurrSpecies.label': 'CurrSpecies',
  'pages.update.sys.user.userCurrSpecies.tip': 'CurrSpecies',
  'pages.update.sys.user.userCurrSpecies.placeholder': 'CurrSpecies',

  'pages.update.sys.user.userCreateDate.label': 'CreateDate',
  'pages.update.sys.user.userCreateDate.tip': 'CreateDate',
  'pages.update.sys.user.userCreateDate.placeholder': 'CreateDate',

  'pages.update.sys.user.userUpdateDate.label': 'UpdateDate',
  'pages.update.sys.user.userUpdateDate.tip': 'UpdateDate',
  'pages.update.sys.user.userUpdateDate.placeholder': 'UpdateDate',

  'pages.update.sys.user.userLastLoginTime.label': 'LastLoginTime',
  'pages.update.sys.user.userLastLoginTime.tip': 'LastLoginTime',
  'pages.update.sys.user.userLastLoginTime.placeholder': 'LastLoginTime',

  'pages.update.sys.user.userLastLoginIp.label': 'LastLoginIp',
  'pages.update.sys.user.userLastLoginIp.tip': 'LastLoginIp',
  'pages.update.sys.user.userLastLoginIp.placeholder': 'LastLoginIp',

  'pages.update.sys.user.userUnit.label': 'Dept',
  'pages.update.sys.user.userUnit.tip': 'Dept',
  'pages.update.sys.user.userUnit.placeholder': 'Dept',

  'pages.update.sys.user.userJobTitle.label': 'Job',
  'pages.update.sys.user.userJobTitle.tip': 'Job',
  'pages.update.sys.user.userJobTitle.placeholder': 'Job',

  'pages.update.sys.user.userAddr.label': 'Address',
  'pages.update.sys.user.userAddr.tip': 'Address',
  'pages.update.sys.user.userAddr.placeholder': 'Address',

  'pages.update.sys.user.userPostcode.label': 'Postcode',
  'pages.update.sys.user.userPostcode.tip': 'Postcode',
  'pages.update.sys.user.userPostcode.placeholder': 'Postcode',


  'pages.list.sys.user.form.title': 'Query',
  'pages.list.sys.config.option.create.item': 'Create',

  'pages.list.sys.user.userId.label': '用户ID',
  'pages.list.sys.user.userId.tip': '用户ID',
  'pages.list.sys.user.userId.placeholder': '用户ID',

  'pages.list.sys.user.userLoginName.label': 'Login Name',
  'pages.list.sys.user.userLoginName.tip': 'Login Name',
  'pages.list.sys.user.userLoginName.placeholder': 'Login Name',

  'pages.list.sys.user.userLoginNameLike.label': 'Login Name',
  'pages.list.sys.user.userLoginNameLike.tip': 'Login Name',
  'pages.list.sys.user.userLoginNameLike.placeholder': 'Login Name',

  'pages.list.sys.user.userRoles.label': 'Roles',
  'pages.list.sys.user.userRoles.tip': 'Roles',
  'pages.list.sys.user.userRoles.placeholder': 'Roles',

  'pages.list.sys.user.userPassword.label': 'Password',
  'pages.list.sys.user.userPassword.tip': 'Password',
  'pages.list.sys.user.userPassword.placeholder': 'Password',

  'pages.list.sys.user.userPasswordPrivate.label': 'Private Password',
  'pages.list.sys.user.userPasswordPrivate.tip': 'Private Password',
  'pages.list.sys.user.userPasswordPrivate.placeholder': 'Private Password',

  'pages.list.sys.user.userNameAbbr.label': 'Name Abbr',
  'pages.list.sys.user.userNameAbbr.tip': 'Name Abbr',
  'pages.list.sys.user.userNameAbbr.placeholder': 'Name Abbr',

  'pages.list.sys.user.userName.label': 'Name',
  'pages.list.sys.user.userName.tip': 'Name',
  'pages.list.sys.user.userName.placeholder': 'Name',

  'pages.list.sys.user.userNameLike.label': 'Name',
  'pages.list.sys.user.userNameLike.tip': 'Name',
  'pages.list.sys.user.userNameLike.placeholder': 'Name',

  'pages.list.sys.user.userNameEn.label': 'NameEn',
  'pages.list.sys.user.userNameEn.tip': 'NameEn',
  'pages.list.sys.user.userNameEn.placeholder': 'NameEn',

  'pages.list.sys.user.userSex.label': 'Gender',
  'pages.list.sys.user.userSex.tip': 'Gender',
  'pages.list.sys.user.userSex.placeholder': 'Gender',

  'pages.list.sys.user.userPhone.label': 'Phone',
  'pages.list.sys.user.userPhone.tip': 'Phone',
  'pages.list.sys.user.userPhone.placeholder': 'Phone',

  'pages.list.sys.user.userEmail.label': 'Email',
  'pages.list.sys.user.userEmail.tip': 'Email',
  'pages.list.sys.user.userEmail.placeholder': 'Email',

  'pages.list.sys.user.userImageUrl.label': 'Portrait',
  'pages.list.sys.user.userImageUrl.tip': 'Portrait',
  'pages.list.sys.user.userImageUrl.placeholder': 'Portrait',

  'pages.list.sys.user.userOrder.label': 'Order',
  'pages.list.sys.user.userOrder.tip': 'Order',
  'pages.list.sys.user.userOrder.placeholder': 'Order',

  'pages.list.sys.user.userState.label': 'State',
  'pages.list.sys.user.userState.tip': 'State',
  'pages.list.sys.user.userState.placeholder': 'State',

  'pages.list.sys.user.userCode.label': 'User Code',
  'pages.list.sys.user.userCode.tip': 'User Code',
  'pages.list.sys.user.userCode.placeholder': 'User Code',

  'pages.list.sys.user.userCreateDate.label': 'CreateDate',
  'pages.list.sys.user.userCreateDate.tip': 'CreateDate',
  'pages.list.sys.user.userCreateDate.placeholder': 'CreateDate',

  'pages.list.sys.user.userUpdateDate.label': 'UpdateDate',
  'pages.list.sys.user.userUpdateDate.tip': 'UpdateDate',
  'pages.list.sys.user.userUpdateDate.placeholder': 'UpdateDate',

  'pages.list.sys.user.userLastLoginTime.label': 'LastLoginTime',
  'pages.list.sys.user.userLastLoginTime.tip': 'LastLoginTime',
  'pages.list.sys.user.userLastLoginTime.placeholder': 'LastLoginTime',

  'pages.list.sys.user.userLastLoginIp.label': 'LastLoginIp',
  'pages.list.sys.user.userLastLoginIp.tip': 'LastLoginIp',
  'pages.list.sys.user.userLastLoginIp.placeholder': 'LastLoginIp',

  'pages.list.sys.user.userUnit.label': 'Dept',
  'pages.list.sys.user.userUnit.tip': 'Dept',
  'pages.list.sys.user.userUnit.placeholder': 'Dept',

  'pages.list.sys.user.userJobTitle.label': 'Job',
  'pages.list.sys.user.userJobTitle.tip': 'Job',
  'pages.list.sys.user.userJobTitle.placeholder': 'Job',

  'pages.list.sys.user.userAddr.label': 'Address',
  'pages.list.sys.user.userAddr.tip': 'Address',
  'pages.list.sys.user.userAddr.placeholder': 'Address',

  'pages.list.sys.user.userPostcode.label': 'Postcode',
  'pages.list.sys.user.userPostcode.tip': 'Postcode',
  'pages.list.sys.user.userPostcode.placeholder': 'Postcode',

  'pages.list.sys.user.userSamSpecies.label': 'Species',
  'pages.list.sys.user.userSamSpecies.tip': 'Species',
  'pages.list.sys.user.userSamSpecies.placeholder': 'Species',

  'pages.list.sys.user.userCurrSpecies.label': 'CurrSpecies',
  'pages.list.sys.user.userCurrSpecies.tip': 'CurrSpecies',
  'pages.list.sys.user.userCurrSpecies.placeholder': 'CurrSpecies',

  'pages.list.sys.user.option.delete': 'Delete',
  'pages.list.sys.user.batch.delete': 'Batch delete',
  'pages.list.sys.user.item': 'item',
  'pages.list.sys.user.chosen': 'Has chosen',
  'pages.list.sys.user.new': 'Create',
  'pages.list.sys.user.title': 'Query',
  'pages.list.sys.user.option.delete.confirm': 'Deletion is unrecoverable. Are you sure you want to delete it?',
  'pages.list.sys.user.option.edit': 'Edit',
  'pages.list.sys.user.option.title': 'Option',
  'pages.list.sys.user.index.label': 'No.',

  'pages.change.password.userPassword.label': 'Original password',
  'pages.change.password.userPassword.length.tip': 'The length must be 6 ~ 24 characters!',
  'pages.change.password.userPassword.tip': 'Original password is required',
  'pages.change.password.userPassword.placeholder': 'Original password',

  'pages.change.password.userPasswordNew.label': 'New password',
  'pages.change.password.userPasswordNew.length.tip': 'The length must be 6 ~ 24 characters!',
  'pages.change.password.userPasswordNew.tip': 'New password is required',
  'pages.change.password.userPasswordNew.placeholder': 'New password',

  'pages.change.password.userPasswordRepeat.label': 'Confirm new password',
  'pages.change.password.userPasswordRepeat.length.tip': 'The length must be 6 ~ 24 characters!',
  'pages.change.password.userPasswordRepeat.tip': 'Confirm new password is required',
  'pages.change.password.userPasswordRepeat.placeholder': 'Confirm new password',

  'pages.change.password.form.title': 'Change Password',
  'pages.change.password.userPassword.right.tip': 'Original password is incorrect',

  'pages.create.sys.user.userPasswordPrivate.length.tip': 'The length must be 6 ~ 24 characters!',
  'pages.create.sys.user.userLoginName.length.tip': 'The length must be 6 ~ 24 characters!',
  'pages.create.sys.user.userLoginName.valid.tip': 'User account already exists!',

  'pages.create.sys.user.userPassword.length.tip': 'The length must be 6 ~ 24 characters!',

  'pages.update.sys.user.userLoginName.length.tip': 'The length must be 6 ~ 24 characters!',
  'pages.update.sys.user.userLoginName.valid.tip': 'User account already exists!',
  'pages.change.password.userPasswordRepeat.same.tip':'The input value is inconsistent with the new password, please re-enter!',

};
