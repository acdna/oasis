export default {
  'pages.list.select.all': 'All',
  'pages.system.info.iphone.label': 'Contact telephone',
  'pages.system.info.dept.address': '9 Shuguang Huayuan Middle Road, Haidian District, Beijing',
  'pages.system.info.address.label': 'Address',
  'pages.system.info.copyright.label': 'Copyright',
  'pages.system.info.address.soft': '9 Shuguang Huayuan Middle Road, Haidian District, Beijing',
  'pages.system.info.technical.support.label': 'Technical support',
  'pages.system.info.technical.support.address': 'Software R & D Office of Beijing Maize Seed Testing Center',
  'pages.system.refresh.cache': 'Refresh Cache Data',
  'pages.system.refresh.cache.loading': 'Refreshing system cache data',
  'pages.system.refresh.cache.success': 'Refreshing system cache data successfully',
  'pages.system.refresh.cache.error': 'Refreshing system cache data failed, please try again!',
};
