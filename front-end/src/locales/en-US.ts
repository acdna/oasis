import component from './en-US/component';
import globalHeader from './en-US/globalHeader';
import menu from './en-US/menu';
import pwa from './en-US/pwa';
import settingDrawer from './en-US/settingDrawer';
import settings from './en-US/settings';
import pages from './en-US/pages';
import file from './en-US/common/file';
import system from './en-US/common/system';

import SysBillingCard from './en-US/sys/sys.billing.card';
import SysCacheTrace from './en-US/sys/sys.cache.trace';
import SysCmsFiles from './en-US/sys/sys.cms.files';
import SysCmsNotice from './en-US/sys/sys.cms.notice';
import SysColDetail from './en-US/sys/sys.col.detail';
import SysConfig from './en-US/sys/sys.config';
import SysContact from './en-US/sys/sys.contact';
import SysDictionary from './en-US/sys/sys.dictionary';
import SysFiles from './en-US/sys/sys.files';
import SysHelp from './en-US/sys/sys.help';
import SysInstrument from './en-US/sys/sys.instrument';
import SysLog from './en-US/sys/sys.log';
import SysMemory from './en-US/sys/sys.memory';
import SysMenu from './en-US/sys/sys.menu';
import SysNotice from './en-US/sys/sys.notice';
import SysPermission from './en-US/sys/sys.permission';
import SysRole from './en-US/sys/sys.role';
import SysRoleDictionary from './en-US/sys/sys.role.dictionary';
import SysRoleMenu from './en-US/sys/sys.role.menu';
import SysRolePermission from './en-US/sys/sys.role.permission';
import SysTip from './en-US/sys/sys.tip';
import SysUpdate from './en-US/sys/sys.update';
import SysUser from './en-US/sys/sys.user';
import SysUserRole from './en-US/sys/sys.user.role';
import SysUserTopUp from './en-US/sys/sys.user.top.up';
import OnpForecast from './en-US/onp/onp.forecast';
import OnpStatistics from './en-US/onp/onp.statistics';
import OnpVerification from './en-US/onp/onp.verification';
import OnpChloroplastHaplotype from "../../../dmt/dist/front-end/locales/en-US/onp/onp.chloroplast.haplotype";
import OnpChloroplastOverall from "../../../dmt/dist/front-end/locales/en-US/onp/onp.chloroplast.overall";
import OnpNucleusHaplotype from "../../../dmt/dist/front-end/locales/en-US/onp/onp.nucleus.haplotype";

export default {
  'navBar.lang': 'Languages',
  'layout.user.link.help': 'Help',
  'layout.user.link.privacy': 'Privacy',
  'layout.user.link.terms': 'Terms',
  'layout.basic.result.subTitle': 'Sorry, you are not authorized to access this page.',
  'layout.basic.result.redirect.login': 'Jump to the login page',
  'app.preview.down.block': 'Download this page to your local project',
  'app.welcome.link.fetch-blocks': 'Get all block',
  'app.welcome.link.block-list': 'Quickly build standard, pages based on `block` development',
  'global.header.avatar.user.center': 'User Center',
  'global.header.avatar.user.setting': 'User Settings',
  'global.header.avatar.change.password': 'Change password',
  'global.header.avatar.system.info': 'System Info',
  'global.header.avatar.logout': 'Logout',

  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...pages,
  ...file,
  ...system,

  ...SysBillingCard,
  ...SysCacheTrace,
  ...SysCmsFiles,
  ...SysCmsNotice,
  ...SysColDetail,
  ...SysConfig,
  ...SysContact,
  ...SysDictionary,
  ...SysFiles,
  ...SysHelp,
  ...SysInstrument,
  ...SysLog,
  ...SysMemory,
  ...SysMenu,
  ...SysNotice,
  ...SysPermission,
  ...SysRole,
  ...SysRoleDictionary,
  ...SysRoleMenu,
  ...SysRolePermission,
  ...SysTip,
  ...SysUpdate,
  ...SysUser,
  ...SysUserRole,
  ...SysUserTopUp,

  ...OnpForecast,
  ...OnpStatistics,
  ...OnpVerification,
  ...OnpChloroplastHaplotype,
  ...OnpChloroplastOverall,
  ...OnpNucleusHaplotype,
};
