export default {
  'pages.list.select.all': '全部',
  'pages.system.info.iphone.label': '联系电话',
  'pages.system.info.dept.address': '北京市海淀区曙光花园中路9号北京市农林科学院玉米研究中心',
  'pages.system.info.address.label': '地址',
  'pages.system.info.copyright.label': '版权',
  'pages.system.info.address.soft': '北京市海淀区曙光花园中路9号北京市农林科学院玉米研究中心种子楼111室',
  'pages.system.info.technical.support.label': '技术支持',
  'pages.system.info.technical.support.address': '北京玉米种子检测中心软件研发室',
  'pages.system.refresh.cache': '刷新缓存数据',
  'pages.system.refresh.cache.loading': '正在刷新系统缓存数据',
  'pages.system.refresh.cache.success': '正在刷新系统缓存数据成功',
  'pages.system.refresh.cache.error': '正在刷新系统缓存数据失败，请重试!',
};
