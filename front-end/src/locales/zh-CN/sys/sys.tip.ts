export default {
  'pages.sys.tip.create.form.title': '创建',
  
  'pages.create.sys.tip.tipId.label': '提示信息ID',
  'pages.create.sys.tip.tipId.tip': '提示信息ID',
  'pages.create.sys.tip.tipId.placeholder': '提示信息ID',
  
  'pages.create.sys.tip.tipCount.label': '提示次数',
  'pages.create.sys.tip.tipCount.tip': '提示次数',
  'pages.create.sys.tip.tipCount.placeholder': '提示次数',
  
  'pages.create.sys.tip.tipContents.label': '提示信息内容',
  'pages.create.sys.tip.tipContents.tip': '提示信息内容',
  'pages.create.sys.tip.tipContents.placeholder': '提示信息内容',
  
  'pages.create.sys.tip.tipManager.label': '所属者',
  'pages.create.sys.tip.tipManager.tip': '所属者',
  'pages.create.sys.tip.tipManager.placeholder': '所属者',
  
  'pages.create.sys.tip.tipAudience.label': '消息受众',
  'pages.create.sys.tip.tipAudience.tip': '消息受众',
  'pages.create.sys.tip.tipAudience.placeholder': '消息受众',
  
  'pages.create.sys.tip.tipCreateDate.label': '创建日期',
  'pages.create.sys.tip.tipCreateDate.tip': '创建日期',
  'pages.create.sys.tip.tipCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.tip.tipUpdateDate.label': '更新日期',
  'pages.create.sys.tip.tipUpdateDate.tip': '更新日期',
  'pages.create.sys.tip.tipUpdateDate.placeholder': '更新日期',
  

  'pages.sys.tip.update.form.title': '更新',
  
  'pages.update.sys.tip.tipId.label': '提示信息ID',
  'pages.update.sys.tip.tipId.tip': '提示信息ID',
  'pages.update.sys.tip.tipId.placeholder': '提示信息ID',
  
  'pages.update.sys.tip.tipCount.label': '提示次数',
  'pages.update.sys.tip.tipCount.tip': '提示次数',
  'pages.update.sys.tip.tipCount.placeholder': '提示次数',
  
  'pages.update.sys.tip.tipContents.label': '提示信息内容',
  'pages.update.sys.tip.tipContents.tip': '提示信息内容',
  'pages.update.sys.tip.tipContents.placeholder': '提示信息内容',
  
  'pages.update.sys.tip.tipManager.label': '所属者',
  'pages.update.sys.tip.tipManager.tip': '所属者',
  'pages.update.sys.tip.tipManager.placeholder': '所属者',
  
  'pages.update.sys.tip.tipAudience.label': '消息受众',
  'pages.update.sys.tip.tipAudience.tip': '消息受众',
  'pages.update.sys.tip.tipAudience.placeholder': '消息受众',
  
  'pages.update.sys.tip.tipCreateDate.label': '创建日期',
  'pages.update.sys.tip.tipCreateDate.tip': '创建日期',
  'pages.update.sys.tip.tipCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.tip.tipUpdateDate.label': '更新日期',
  'pages.update.sys.tip.tipUpdateDate.tip': '更新日期',
  'pages.update.sys.tip.tipUpdateDate.placeholder': '更新日期',
  

  'pages.sys.tip.list.form.title': '查询',
  
  'pages.list.sys.tip.tipId.label': '提示信息ID',
  'pages.list.sys.tip.tipId.tip': '提示信息ID',
  'pages.list.sys.tip.tipId.placeholder': '提示信息ID',
  
  'pages.list.sys.tip.tipCount.label': '提示次数',
  'pages.list.sys.tip.tipCount.tip': '提示次数',
  'pages.list.sys.tip.tipCount.placeholder': '提示次数',
  
  'pages.list.sys.tip.tipContents.label': '提示信息内容',
  'pages.list.sys.tip.tipContents.tip': '提示信息内容',
  'pages.list.sys.tip.tipContents.placeholder': '提示信息内容',
  
  'pages.list.sys.tip.tipManager.label': '所属者',
  'pages.list.sys.tip.tipManager.tip': '所属者',
  'pages.list.sys.tip.tipManager.placeholder': '所属者',
  
  'pages.list.sys.tip.tipAudience.label': '消息受众',
  'pages.list.sys.tip.tipAudience.tip': '消息受众',
  'pages.list.sys.tip.tipAudience.placeholder': '消息受众',
  
  'pages.list.sys.tip.tipCreateDate.label': '创建日期',
  'pages.list.sys.tip.tipCreateDate.tip': '创建日期',
  'pages.list.sys.tip.tipCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.tip.tipUpdateDate.label': '更新日期',
  'pages.list.sys.tip.tipUpdateDate.tip': '更新日期',
  'pages.list.sys.tip.tipUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.tip.option.delete':'删除',
  'pages.list.sys.tip.batch.delete':'批量删除',
  'pages.list.sys.tip.item':'项',
  'pages.list.sys.tip.chosen':'已选择',
  'pages.list.sys.tip.new':'新建',
  'pages.list.sys.tip.title':'查询',
  'pages.list.sys.tip.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.tip.option.edit':'修改',
  'pages.list.sys.tip.option.title':'操作',
  'pages.list.sys.tip.index.label':'序号',

};
