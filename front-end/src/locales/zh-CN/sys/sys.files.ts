export default {
  'pages.sys.files.create.form.title': '创建',
  
  'pages.create.sys.files.fileId.label': '记录ID',
  'pages.create.sys.files.fileId.tip': '记录ID',
  'pages.create.sys.files.fileId.placeholder': '记录ID',
  
  'pages.create.sys.files.fileBarcode.label': '文件条码号',
  'pages.create.sys.files.fileBarcode.tip': '文件条码号',
  'pages.create.sys.files.fileBarcode.placeholder': '文件条码号',
  
  'pages.create.sys.files.fileRelateId.label': '关联的其它记录信息ID号',
  'pages.create.sys.files.fileRelateId.tip': '关联的其它记录信息ID号',
  'pages.create.sys.files.fileRelateId.placeholder': '关联的其它记录信息ID号',
  
  'pages.create.sys.files.fileName.label': '文件名称',
  'pages.create.sys.files.fileName.tip': '文件名称',
  'pages.create.sys.files.fileName.placeholder': '文件名称',
  
  'pages.create.sys.files.fileNameEn.label': '文件名',
  'pages.create.sys.files.fileNameEn.tip': '文件名',
  'pages.create.sys.files.fileNameEn.placeholder': '文件名',
  
  'pages.create.sys.files.filePath.label': '文件存储路径',
  'pages.create.sys.files.filePath.tip': '文件存储路径',
  'pages.create.sys.files.filePath.placeholder': '文件存储路径',
  
  'pages.create.sys.files.fileCreateDate.label': '创建日期',
  'pages.create.sys.files.fileCreateDate.tip': '创建日期',
  'pages.create.sys.files.fileCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.files.fileUpdateDate.label': '更新日期',
  'pages.create.sys.files.fileUpdateDate.tip': '更新日期',
  'pages.create.sys.files.fileUpdateDate.placeholder': '更新日期',
  
  'pages.create.sys.files.fileIsUsed.label': '是否被启用',
  'pages.create.sys.files.fileIsUsed.tip': '是否被启用',
  'pages.create.sys.files.fileIsUsed.placeholder': '是否被启用',
  
  'pages.create.sys.files.fileOwner.label': '所属用户',
  'pages.create.sys.files.fileOwner.tip': '所属用户',
  'pages.create.sys.files.fileOwner.placeholder': '所属用户',
  
  'pages.create.sys.files.fileSize.label': '文件大小',
  'pages.create.sys.files.fileSize.tip': '文件大小',
  'pages.create.sys.files.fileSize.placeholder': '文件大小',
  
  'pages.create.sys.files.fileGrants.label': '用户权限表',
  'pages.create.sys.files.fileGrants.tip': '用户权限表',
  'pages.create.sys.files.fileGrants.placeholder': '用户权限表',
  
  'pages.create.sys.files.fileClasses.label': '文件分类',
  'pages.create.sys.files.fileClasses.tip': '文件分类',
  'pages.create.sys.files.fileClasses.placeholder': '文件分类',
  
  'pages.create.sys.files.fileState.label': '文件状态信息',
  'pages.create.sys.files.fileState.tip': '文件状态信息',
  'pages.create.sys.files.fileState.placeholder': '文件状态信息',
  
  'pages.create.sys.files.fileType.label': '文件类型',
  'pages.create.sys.files.fileType.tip': '文件类型',
  'pages.create.sys.files.fileType.placeholder': '文件类型',
  
  'pages.create.sys.files.fileComments.label': '文件备注信息',
  'pages.create.sys.files.fileComments.tip': '文件备注信息',
  'pages.create.sys.files.fileComments.placeholder': '文件备注信息',
  
  'pages.create.sys.files.fileSpecies.label': '种属',
  'pages.create.sys.files.fileSpecies.tip': '种属',
  'pages.create.sys.files.fileSpecies.placeholder': '种属',
  

  'pages.sys.files.update.form.title': '更新',
  
  'pages.update.sys.files.fileId.label': '记录ID',
  'pages.update.sys.files.fileId.tip': '记录ID',
  'pages.update.sys.files.fileId.placeholder': '记录ID',
  
  'pages.update.sys.files.fileBarcode.label': '文件条码号',
  'pages.update.sys.files.fileBarcode.tip': '文件条码号',
  'pages.update.sys.files.fileBarcode.placeholder': '文件条码号',
  
  'pages.update.sys.files.fileRelateId.label': '关联的其它记录信息ID号',
  'pages.update.sys.files.fileRelateId.tip': '关联的其它记录信息ID号',
  'pages.update.sys.files.fileRelateId.placeholder': '关联的其它记录信息ID号',
  
  'pages.update.sys.files.fileName.label': '文件名称',
  'pages.update.sys.files.fileName.tip': '文件名称',
  'pages.update.sys.files.fileName.placeholder': '文件名称',
  
  'pages.update.sys.files.fileNameEn.label': '文件名',
  'pages.update.sys.files.fileNameEn.tip': '文件名',
  'pages.update.sys.files.fileNameEn.placeholder': '文件名',
  
  'pages.update.sys.files.filePath.label': '文件存储路径',
  'pages.update.sys.files.filePath.tip': '文件存储路径',
  'pages.update.sys.files.filePath.placeholder': '文件存储路径',
  
  'pages.update.sys.files.fileCreateDate.label': '创建日期',
  'pages.update.sys.files.fileCreateDate.tip': '创建日期',
  'pages.update.sys.files.fileCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.files.fileUpdateDate.label': '更新日期',
  'pages.update.sys.files.fileUpdateDate.tip': '更新日期',
  'pages.update.sys.files.fileUpdateDate.placeholder': '更新日期',
  
  'pages.update.sys.files.fileIsUsed.label': '是否被启用',
  'pages.update.sys.files.fileIsUsed.tip': '是否被启用',
  'pages.update.sys.files.fileIsUsed.placeholder': '是否被启用',
  
  'pages.update.sys.files.fileOwner.label': '所属用户',
  'pages.update.sys.files.fileOwner.tip': '所属用户',
  'pages.update.sys.files.fileOwner.placeholder': '所属用户',
  
  'pages.update.sys.files.fileSize.label': '文件大小',
  'pages.update.sys.files.fileSize.tip': '文件大小',
  'pages.update.sys.files.fileSize.placeholder': '文件大小',
  
  'pages.update.sys.files.fileGrants.label': '用户权限表',
  'pages.update.sys.files.fileGrants.tip': '用户权限表',
  'pages.update.sys.files.fileGrants.placeholder': '用户权限表',
  
  'pages.update.sys.files.fileClasses.label': '文件分类',
  'pages.update.sys.files.fileClasses.tip': '文件分类',
  'pages.update.sys.files.fileClasses.placeholder': '文件分类',
  
  'pages.update.sys.files.fileState.label': '文件状态信息',
  'pages.update.sys.files.fileState.tip': '文件状态信息',
  'pages.update.sys.files.fileState.placeholder': '文件状态信息',
  
  'pages.update.sys.files.fileType.label': '文件类型',
  'pages.update.sys.files.fileType.tip': '文件类型',
  'pages.update.sys.files.fileType.placeholder': '文件类型',
  
  'pages.update.sys.files.fileComments.label': '文件备注信息',
  'pages.update.sys.files.fileComments.tip': '文件备注信息',
  'pages.update.sys.files.fileComments.placeholder': '文件备注信息',
  
  'pages.update.sys.files.fileSpecies.label': '种属',
  'pages.update.sys.files.fileSpecies.tip': '种属',
  'pages.update.sys.files.fileSpecies.placeholder': '种属',
  

  'pages.sys.files.list.form.title': '查询',
  
  'pages.list.sys.files.fileId.label': '记录ID',
  'pages.list.sys.files.fileId.tip': '记录ID',
  'pages.list.sys.files.fileId.placeholder': '记录ID',
  
  'pages.list.sys.files.fileBarcode.label': '文件条码号',
  'pages.list.sys.files.fileBarcode.tip': '文件条码号',
  'pages.list.sys.files.fileBarcode.placeholder': '文件条码号',
  
  'pages.list.sys.files.fileRelateId.label': '关联的其它记录信息ID号',
  'pages.list.sys.files.fileRelateId.tip': '关联的其它记录信息ID号',
  'pages.list.sys.files.fileRelateId.placeholder': '关联的其它记录信息ID号',
  
  'pages.list.sys.files.fileName.label': '文件名称',
  'pages.list.sys.files.fileName.tip': '文件名称',
  'pages.list.sys.files.fileName.placeholder': '文件名称',
  
  'pages.list.sys.files.fileNameEn.label': '文件名',
  'pages.list.sys.files.fileNameEn.tip': '文件名',
  'pages.list.sys.files.fileNameEn.placeholder': '文件名',
  
  'pages.list.sys.files.filePath.label': '文件存储路径',
  'pages.list.sys.files.filePath.tip': '文件存储路径',
  'pages.list.sys.files.filePath.placeholder': '文件存储路径',
  
  'pages.list.sys.files.fileCreateDate.label': '创建日期',
  'pages.list.sys.files.fileCreateDate.tip': '创建日期',
  'pages.list.sys.files.fileCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.files.fileUpdateDate.label': '更新日期',
  'pages.list.sys.files.fileUpdateDate.tip': '更新日期',
  'pages.list.sys.files.fileUpdateDate.placeholder': '更新日期',
  
  'pages.list.sys.files.fileIsUsed.label': '是否被启用',
  'pages.list.sys.files.fileIsUsed.tip': '是否被启用',
  'pages.list.sys.files.fileIsUsed.placeholder': '是否被启用',
  
  'pages.list.sys.files.fileOwner.label': '所属用户',
  'pages.list.sys.files.fileOwner.tip': '所属用户',
  'pages.list.sys.files.fileOwner.placeholder': '所属用户',
  
  'pages.list.sys.files.fileSize.label': '文件大小',
  'pages.list.sys.files.fileSize.tip': '文件大小',
  'pages.list.sys.files.fileSize.placeholder': '文件大小',
  
  'pages.list.sys.files.fileGrants.label': '用户权限表',
  'pages.list.sys.files.fileGrants.tip': '用户权限表',
  'pages.list.sys.files.fileGrants.placeholder': '用户权限表',
  
  'pages.list.sys.files.fileClasses.label': '文件分类',
  'pages.list.sys.files.fileClasses.tip': '文件分类',
  'pages.list.sys.files.fileClasses.placeholder': '文件分类',
  
  'pages.list.sys.files.fileState.label': '文件状态信息',
  'pages.list.sys.files.fileState.tip': '文件状态信息',
  'pages.list.sys.files.fileState.placeholder': '文件状态信息',
  
  'pages.list.sys.files.fileType.label': '文件类型',
  'pages.list.sys.files.fileType.tip': '文件类型',
  'pages.list.sys.files.fileType.placeholder': '文件类型',
  
  'pages.list.sys.files.fileComments.label': '文件备注信息',
  'pages.list.sys.files.fileComments.tip': '文件备注信息',
  'pages.list.sys.files.fileComments.placeholder': '文件备注信息',
  
  'pages.list.sys.files.fileSpecies.label': '种属',
  'pages.list.sys.files.fileSpecies.tip': '种属',
  'pages.list.sys.files.fileSpecies.placeholder': '种属',
  

  'pages.list.sys.files.option.delete':'删除',
  'pages.list.sys.files.batch.delete':'批量删除',
  'pages.list.sys.files.item':'项',
  'pages.list.sys.files.chosen':'已选择',
  'pages.list.sys.files.new':'新建',
  'pages.list.sys.files.title':'查询',
  'pages.list.sys.files.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.files.option.edit':'修改',
  'pages.list.sys.files.option.title':'操作',
  'pages.list.sys.files.index.label':'序号',

};
