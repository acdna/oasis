export default {
  'pages.sys.notice.create.form.title': '创建',
  
  'pages.create.sys.notice.noticeId.label': '消息编号',
  'pages.create.sys.notice.noticeId.tip': '消息编号',
  'pages.create.sys.notice.noticeId.placeholder': '消息编号',
  
  'pages.create.sys.notice.noticeTitle.label': '消息标题',
  'pages.create.sys.notice.noticeTitle.tip': '消息标题',
  'pages.create.sys.notice.noticeTitle.placeholder': '消息标题',
  
  'pages.create.sys.notice.noticeContent.label': '消息内容',
  'pages.create.sys.notice.noticeContent.tip': '消息内容',
  'pages.create.sys.notice.noticeContent.placeholder': '消息内容',
  
  'pages.create.sys.notice.noticeManager.label': '消息发布者',
  'pages.create.sys.notice.noticeManager.tip': '消息发布者',
  'pages.create.sys.notice.noticeManager.placeholder': '消息发布者',
  
  'pages.create.sys.notice.noticeIsNew.label': '是否是新消息',
  'pages.create.sys.notice.noticeIsNew.tip': '是否是新消息',
  'pages.create.sys.notice.noticeIsNew.placeholder': '是否是新消息',
  
  'pages.create.sys.notice.noticeCreateDate.label': '创建日期',
  'pages.create.sys.notice.noticeCreateDate.tip': '创建日期',
  'pages.create.sys.notice.noticeCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.notice.noticeUpdateDate.label': '更新日期',
  'pages.create.sys.notice.noticeUpdateDate.tip': '更新日期',
  'pages.create.sys.notice.noticeUpdateDate.placeholder': '更新日期',
  
  'pages.create.sys.notice.noticeReceiver.label': '消息接收者',
  'pages.create.sys.notice.noticeReceiver.tip': '消息接收者',
  'pages.create.sys.notice.noticeReceiver.placeholder': '消息接收者',
  

  'pages.sys.notice.update.form.title': '更新',
  
  'pages.update.sys.notice.noticeId.label': '消息编号',
  'pages.update.sys.notice.noticeId.tip': '消息编号',
  'pages.update.sys.notice.noticeId.placeholder': '消息编号',
  
  'pages.update.sys.notice.noticeTitle.label': '消息标题',
  'pages.update.sys.notice.noticeTitle.tip': '消息标题',
  'pages.update.sys.notice.noticeTitle.placeholder': '消息标题',
  
  'pages.update.sys.notice.noticeContent.label': '消息内容',
  'pages.update.sys.notice.noticeContent.tip': '消息内容',
  'pages.update.sys.notice.noticeContent.placeholder': '消息内容',
  
  'pages.update.sys.notice.noticeManager.label': '消息发布者',
  'pages.update.sys.notice.noticeManager.tip': '消息发布者',
  'pages.update.sys.notice.noticeManager.placeholder': '消息发布者',
  
  'pages.update.sys.notice.noticeIsNew.label': '是否是新消息',
  'pages.update.sys.notice.noticeIsNew.tip': '是否是新消息',
  'pages.update.sys.notice.noticeIsNew.placeholder': '是否是新消息',
  
  'pages.update.sys.notice.noticeCreateDate.label': '创建日期',
  'pages.update.sys.notice.noticeCreateDate.tip': '创建日期',
  'pages.update.sys.notice.noticeCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.notice.noticeUpdateDate.label': '更新日期',
  'pages.update.sys.notice.noticeUpdateDate.tip': '更新日期',
  'pages.update.sys.notice.noticeUpdateDate.placeholder': '更新日期',
  
  'pages.update.sys.notice.noticeReceiver.label': '消息接收者',
  'pages.update.sys.notice.noticeReceiver.tip': '消息接收者',
  'pages.update.sys.notice.noticeReceiver.placeholder': '消息接收者',
  

  'pages.sys.notice.list.form.title': '查询',
  
  'pages.list.sys.notice.noticeId.label': '消息编号',
  'pages.list.sys.notice.noticeId.tip': '消息编号',
  'pages.list.sys.notice.noticeId.placeholder': '消息编号',
  
  'pages.list.sys.notice.noticeTitle.label': '消息标题',
  'pages.list.sys.notice.noticeTitle.tip': '消息标题',
  'pages.list.sys.notice.noticeTitle.placeholder': '消息标题',
  
  'pages.list.sys.notice.noticeContent.label': '消息内容',
  'pages.list.sys.notice.noticeContent.tip': '消息内容',
  'pages.list.sys.notice.noticeContent.placeholder': '消息内容',
  
  'pages.list.sys.notice.noticeManager.label': '消息发布者',
  'pages.list.sys.notice.noticeManager.tip': '消息发布者',
  'pages.list.sys.notice.noticeManager.placeholder': '消息发布者',
  
  'pages.list.sys.notice.noticeIsNew.label': '是否是新消息',
  'pages.list.sys.notice.noticeIsNew.tip': '是否是新消息',
  'pages.list.sys.notice.noticeIsNew.placeholder': '是否是新消息',
  
  'pages.list.sys.notice.noticeCreateDate.label': '创建日期',
  'pages.list.sys.notice.noticeCreateDate.tip': '创建日期',
  'pages.list.sys.notice.noticeCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.notice.noticeUpdateDate.label': '更新日期',
  'pages.list.sys.notice.noticeUpdateDate.tip': '更新日期',
  'pages.list.sys.notice.noticeUpdateDate.placeholder': '更新日期',
  
  'pages.list.sys.notice.noticeReceiver.label': '消息接收者',
  'pages.list.sys.notice.noticeReceiver.tip': '消息接收者',
  'pages.list.sys.notice.noticeReceiver.placeholder': '消息接收者',
  

  'pages.list.sys.notice.option.delete':'删除',
  'pages.list.sys.notice.batch.delete':'批量删除',
  'pages.list.sys.notice.item':'项',
  'pages.list.sys.notice.chosen':'已选择',
  'pages.list.sys.notice.new':'新建',
  'pages.list.sys.notice.title':'查询',
  'pages.list.sys.notice.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.notice.option.edit':'修改',
  'pages.list.sys.notice.option.title':'操作',
  'pages.list.sys.notice.index.label':'序号',

};
