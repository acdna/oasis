export default {
  'pages.sys.cache.trace.create.form.title': '创建',
  
  'pages.create.sys.cache.trace.ctId.label': '缓存记录ID',
  'pages.create.sys.cache.trace.ctId.tip': '缓存记录ID',
  'pages.create.sys.cache.trace.ctId.placeholder': '缓存记录ID',
  
  'pages.create.sys.cache.trace.ctCode.label': '缓存类型代码',
  'pages.create.sys.cache.trace.ctCode.tip': '缓存类型代码',
  'pages.create.sys.cache.trace.ctCode.placeholder': '缓存类型代码',
  
  'pages.create.sys.cache.trace.ctOperate.label': '缓存操作类型代码',
  'pages.create.sys.cache.trace.ctOperate.tip': '缓存操作类型代码',
  'pages.create.sys.cache.trace.ctOperate.placeholder': '缓存操作类型代码',
  
  'pages.create.sys.cache.trace.ctTargetId.label': '缓存需要操作的目标记录ID',
  'pages.create.sys.cache.trace.ctTargetId.tip': '缓存需要操作的目标记录ID',
  'pages.create.sys.cache.trace.ctTargetId.placeholder': '缓存需要操作的目标记录ID',
  
  'pages.create.sys.cache.trace.ctCreateDate.label': '创建日期',
  'pages.create.sys.cache.trace.ctCreateDate.tip': '创建日期',
  'pages.create.sys.cache.trace.ctCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.cache.trace.ctComment.label': '缓存备注信息',
  'pages.create.sys.cache.trace.ctComment.tip': '缓存备注信息',
  'pages.create.sys.cache.trace.ctComment.placeholder': '缓存备注信息',
  

  'pages.sys.cache.trace.update.form.title': '更新',
  
  'pages.update.sys.cache.trace.ctId.label': '缓存记录ID',
  'pages.update.sys.cache.trace.ctId.tip': '缓存记录ID',
  'pages.update.sys.cache.trace.ctId.placeholder': '缓存记录ID',
  
  'pages.update.sys.cache.trace.ctCode.label': '缓存类型代码',
  'pages.update.sys.cache.trace.ctCode.tip': '缓存类型代码',
  'pages.update.sys.cache.trace.ctCode.placeholder': '缓存类型代码',
  
  'pages.update.sys.cache.trace.ctOperate.label': '缓存操作类型代码',
  'pages.update.sys.cache.trace.ctOperate.tip': '缓存操作类型代码',
  'pages.update.sys.cache.trace.ctOperate.placeholder': '缓存操作类型代码',
  
  'pages.update.sys.cache.trace.ctTargetId.label': '缓存需要操作的目标记录ID',
  'pages.update.sys.cache.trace.ctTargetId.tip': '缓存需要操作的目标记录ID',
  'pages.update.sys.cache.trace.ctTargetId.placeholder': '缓存需要操作的目标记录ID',
  
  'pages.update.sys.cache.trace.ctCreateDate.label': '创建日期',
  'pages.update.sys.cache.trace.ctCreateDate.tip': '创建日期',
  'pages.update.sys.cache.trace.ctCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.cache.trace.ctComment.label': '缓存备注信息',
  'pages.update.sys.cache.trace.ctComment.tip': '缓存备注信息',
  'pages.update.sys.cache.trace.ctComment.placeholder': '缓存备注信息',
  

  'pages.sys.cache.trace.list.form.title': '查询',
  
  'pages.list.sys.cache.trace.ctId.label': '缓存记录ID',
  'pages.list.sys.cache.trace.ctId.tip': '缓存记录ID',
  'pages.list.sys.cache.trace.ctId.placeholder': '缓存记录ID',
  
  'pages.list.sys.cache.trace.ctCode.label': '缓存类型代码',
  'pages.list.sys.cache.trace.ctCode.tip': '缓存类型代码',
  'pages.list.sys.cache.trace.ctCode.placeholder': '缓存类型代码',
  
  'pages.list.sys.cache.trace.ctOperate.label': '缓存操作类型代码',
  'pages.list.sys.cache.trace.ctOperate.tip': '缓存操作类型代码',
  'pages.list.sys.cache.trace.ctOperate.placeholder': '缓存操作类型代码',
  
  'pages.list.sys.cache.trace.ctTargetId.label': '缓存需要操作的目标记录ID',
  'pages.list.sys.cache.trace.ctTargetId.tip': '缓存需要操作的目标记录ID',
  'pages.list.sys.cache.trace.ctTargetId.placeholder': '缓存需要操作的目标记录ID',
  
  'pages.list.sys.cache.trace.ctCreateDate.label': '创建日期',
  'pages.list.sys.cache.trace.ctCreateDate.tip': '创建日期',
  'pages.list.sys.cache.trace.ctCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.cache.trace.ctComment.label': '缓存备注信息',
  'pages.list.sys.cache.trace.ctComment.tip': '缓存备注信息',
  'pages.list.sys.cache.trace.ctComment.placeholder': '缓存备注信息',
  

  'pages.list.sys.cache.trace.option.delete':'删除',
  'pages.list.sys.cache.trace.batch.delete':'批量删除',
  'pages.list.sys.cache.trace.item':'项',
  'pages.list.sys.cache.trace.chosen':'已选择',
  'pages.list.sys.cache.trace.new':'新建',
  'pages.list.sys.cache.trace.title':'查询',
  'pages.list.sys.cache.trace.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.cache.trace.option.edit':'修改',
  'pages.list.sys.cache.trace.option.title':'操作',
  'pages.list.sys.cache.trace.index.label':'序号',

};
