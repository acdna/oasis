export default {
  'pages.sys.user.top.up.create.form.title': '创建',
  
  'pages.create.sys.user.top.up.utuId.label': 'id',
  'pages.create.sys.user.top.up.utuId.tip': 'id',
  'pages.create.sys.user.top.up.utuId.placeholder': 'id',
  
  'pages.create.sys.user.top.up.utuLoginName.label': '用户登录名',
  'pages.create.sys.user.top.up.utuLoginName.tip': '用户登录名',
  'pages.create.sys.user.top.up.utuLoginName.placeholder': '用户登录名',
  
  'pages.create.sys.user.top.up.utuSerialNumber.label': '序列号',
  'pages.create.sys.user.top.up.utuSerialNumber.tip': '序列号',
  'pages.create.sys.user.top.up.utuSerialNumber.placeholder': '序列号',
  
  'pages.create.sys.user.top.up.utuBillingStartDate.label': '计费开始时间',
  'pages.create.sys.user.top.up.utuBillingStartDate.tip': '计费开始时间',
  'pages.create.sys.user.top.up.utuBillingStartDate.placeholder': '计费开始时间',
  
  'pages.create.sys.user.top.up.utuBillingEndDate.label': '计费结束时间',
  'pages.create.sys.user.top.up.utuBillingEndDate.tip': '计费结束时间',
  'pages.create.sys.user.top.up.utuBillingEndDate.placeholder': '计费结束时间',
  
  'pages.create.sys.user.top.up.utuState.label': '状态',
  'pages.create.sys.user.top.up.utuState.tip': '状态',
  'pages.create.sys.user.top.up.utuState.placeholder': '状态',
  
  'pages.create.sys.user.top.up.utuCreateDate.label': '创建日期',
  'pages.create.sys.user.top.up.utuCreateDate.tip': '创建日期',
  'pages.create.sys.user.top.up.utuCreateDate.placeholder': '创建日期',
  

  'pages.sys.user.top.up.update.form.title': '更新',
  
  'pages.update.sys.user.top.up.utuId.label': 'id',
  'pages.update.sys.user.top.up.utuId.tip': 'id',
  'pages.update.sys.user.top.up.utuId.placeholder': 'id',
  
  'pages.update.sys.user.top.up.utuLoginName.label': '用户登录名',
  'pages.update.sys.user.top.up.utuLoginName.tip': '用户登录名',
  'pages.update.sys.user.top.up.utuLoginName.placeholder': '用户登录名',
  
  'pages.update.sys.user.top.up.utuSerialNumber.label': '序列号',
  'pages.update.sys.user.top.up.utuSerialNumber.tip': '序列号',
  'pages.update.sys.user.top.up.utuSerialNumber.placeholder': '序列号',
  
  'pages.update.sys.user.top.up.utuBillingStartDate.label': '计费开始时间',
  'pages.update.sys.user.top.up.utuBillingStartDate.tip': '计费开始时间',
  'pages.update.sys.user.top.up.utuBillingStartDate.placeholder': '计费开始时间',
  
  'pages.update.sys.user.top.up.utuBillingEndDate.label': '计费结束时间',
  'pages.update.sys.user.top.up.utuBillingEndDate.tip': '计费结束时间',
  'pages.update.sys.user.top.up.utuBillingEndDate.placeholder': '计费结束时间',
  
  'pages.update.sys.user.top.up.utuState.label': '状态',
  'pages.update.sys.user.top.up.utuState.tip': '状态',
  'pages.update.sys.user.top.up.utuState.placeholder': '状态',
  
  'pages.update.sys.user.top.up.utuCreateDate.label': '创建日期',
  'pages.update.sys.user.top.up.utuCreateDate.tip': '创建日期',
  'pages.update.sys.user.top.up.utuCreateDate.placeholder': '创建日期',
  

  'pages.sys.user.top.up.list.form.title': '查询',
  
  'pages.list.sys.user.top.up.utuId.label': 'id',
  'pages.list.sys.user.top.up.utuId.tip': 'id',
  'pages.list.sys.user.top.up.utuId.placeholder': 'id',
  
  'pages.list.sys.user.top.up.utuLoginName.label': '用户登录名',
  'pages.list.sys.user.top.up.utuLoginName.tip': '用户登录名',
  'pages.list.sys.user.top.up.utuLoginName.placeholder': '用户登录名',
  
  'pages.list.sys.user.top.up.utuSerialNumber.label': '序列号',
  'pages.list.sys.user.top.up.utuSerialNumber.tip': '序列号',
  'pages.list.sys.user.top.up.utuSerialNumber.placeholder': '序列号',
  
  'pages.list.sys.user.top.up.utuBillingStartDate.label': '计费开始时间',
  'pages.list.sys.user.top.up.utuBillingStartDate.tip': '计费开始时间',
  'pages.list.sys.user.top.up.utuBillingStartDate.placeholder': '计费开始时间',
  
  'pages.list.sys.user.top.up.utuBillingEndDate.label': '计费结束时间',
  'pages.list.sys.user.top.up.utuBillingEndDate.tip': '计费结束时间',
  'pages.list.sys.user.top.up.utuBillingEndDate.placeholder': '计费结束时间',
  
  'pages.list.sys.user.top.up.utuState.label': '状态',
  'pages.list.sys.user.top.up.utuState.tip': '状态',
  'pages.list.sys.user.top.up.utuState.placeholder': '状态',
  
  'pages.list.sys.user.top.up.utuCreateDate.label': '创建日期',
  'pages.list.sys.user.top.up.utuCreateDate.tip': '创建日期',
  'pages.list.sys.user.top.up.utuCreateDate.placeholder': '创建日期',
  

  'pages.list.sys.user.top.up.option.delete':'删除',
  'pages.list.sys.user.top.up.batch.delete':'批量删除',
  'pages.list.sys.user.top.up.item':'项',
  'pages.list.sys.user.top.up.chosen':'已选择',
  'pages.list.sys.user.top.up.new':'新建',
  'pages.list.sys.user.top.up.title':'查询',
  'pages.list.sys.user.top.up.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.user.top.up.option.edit':'修改',
  'pages.list.sys.user.top.up.option.title':'操作',
  'pages.list.sys.user.top.up.index.label':'序号',

};
