export default {
  'pages.sys.role.menu.relate.title':'菜单授权',
  'pages.sys.role.menu.relate.chosen':'已选择',
  'pages.sys.role.menu.relate.item':'项',
  'pages.sys.role.menu.relate.cancel.all':'取消选择',
  'pages.sys.role.menu.relate.cancel':'关闭',
  'pages.sys.role.menu.relate.submit':'授权',
  'pages.sys.role.menu.relate.loading':'正在授权',
  'pages.sys.role.menu.relate.success':'授权成功',
  'pages.sys.role.menu.relate.error':'授权失败，请重试',

  'pages.sys.role.menu.create.form.title': '创建',

  'pages.create.sys.role.menu.rmId.label': '角色菜单ID',
  'pages.create.sys.role.menu.rmId.tip': '角色菜单ID',
  'pages.create.sys.role.menu.rmId.placeholder': '角色菜单ID',

  'pages.create.sys.role.menu.rmRoleId.label': '角色ID',
  'pages.create.sys.role.menu.rmRoleId.tip': '角色ID',
  'pages.create.sys.role.menu.rmRoleId.placeholder': '角色ID',

  'pages.create.sys.role.menu.rmMenuId.label': '菜单ID',
  'pages.create.sys.role.menu.rmMenuId.tip': '菜单ID',
  'pages.create.sys.role.menu.rmMenuId.placeholder': '菜单ID',


  'pages.sys.role.menu.update.form.title': '更新',

  'pages.update.sys.role.menu.rmId.label': '角色菜单ID',
  'pages.update.sys.role.menu.rmId.tip': '角色菜单ID',
  'pages.update.sys.role.menu.rmId.placeholder': '角色菜单ID',

  'pages.update.sys.role.menu.rmRoleId.label': '角色ID',
  'pages.update.sys.role.menu.rmRoleId.tip': '角色ID',
  'pages.update.sys.role.menu.rmRoleId.placeholder': '角色ID',

  'pages.update.sys.role.menu.rmMenuId.label': '菜单ID',
  'pages.update.sys.role.menu.rmMenuId.tip': '菜单ID',
  'pages.update.sys.role.menu.rmMenuId.placeholder': '菜单ID',


  'pages.sys.role.menu.list.form.title': '查询',

  'pages.list.sys.role.menu.rmId.label': '角色菜单ID',
  'pages.list.sys.role.menu.rmId.tip': '角色菜单ID',
  'pages.list.sys.role.menu.rmId.placeholder': '角色菜单ID',

  'pages.list.sys.role.menu.rmRoleId.label': '角色ID',
  'pages.list.sys.role.menu.rmRoleId.tip': '角色ID',
  'pages.list.sys.role.menu.rmRoleId.placeholder': '角色ID',

  'pages.list.sys.role.menu.rmMenuId.label': '菜单ID',
  'pages.list.sys.role.menu.rmMenuId.tip': '菜单ID',
  'pages.list.sys.role.menu.rmMenuId.placeholder': '菜单ID',


  'pages.list.sys.role.menu.option.delete':'删除',
  'pages.list.sys.role.menu.batch.delete':'批量删除',
  'pages.list.sys.role.menu.item':'项',
  'pages.list.sys.role.menu.chosen':'已选择',
  'pages.list.sys.role.menu.new':'新建',
  'pages.list.sys.role.menu.title':'查询',
  'pages.list.sys.role.menu.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.role.menu.option.edit':'修改',
  'pages.list.sys.role.menu.option.title':'操作',
  'pages.list.sys.role.menu.index.label':'序号',

};
