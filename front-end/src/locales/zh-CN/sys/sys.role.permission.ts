export default {
  'pages.sys.role.permission.relate.title':'权限授权',
  'pages.sys.role.permission.relate.chosen':'已选择',
  'pages.sys.role.permission.relate.item':'项',
  'pages.sys.role.permission.relate.cancel.all':'取消选择',
  'pages.sys.role.permission.relate.cancel':'关闭',
  'pages.sys.role.permission.relate.submit':'授权',
  'pages.sys.role.permission.relate.loading':'正在授权',
  'pages.sys.role.permission.relate.success':'授权成功',
  'pages.sys.role.permission.relate.error':'授权失败，请重试',

  'pages.sys.role.permission.create.form.title': '创建',

  'pages.create.sys.role.permission.rpId.label': '角色权限表Id',
  'pages.create.sys.role.permission.rpId.tip': '角色权限表Id',
  'pages.create.sys.role.permission.rpId.placeholder': '角色权限表Id',

  'pages.create.sys.role.permission.rpRoleId.label': '角色ID号',
  'pages.create.sys.role.permission.rpRoleId.tip': '角色ID号',
  'pages.create.sys.role.permission.rpRoleId.placeholder': '角色ID号',

  'pages.create.sys.role.permission.rpPermissionId.label': '权限ID',
  'pages.create.sys.role.permission.rpPermissionId.tip': '权限ID',
  'pages.create.sys.role.permission.rpPermissionId.placeholder': '权限ID',


  'pages.sys.role.permission.update.form.title': '更新',

  'pages.update.sys.role.permission.rpId.label': '角色权限表Id',
  'pages.update.sys.role.permission.rpId.tip': '角色权限表Id',
  'pages.update.sys.role.permission.rpId.placeholder': '角色权限表Id',

  'pages.update.sys.role.permission.rpRoleId.label': '角色ID号',
  'pages.update.sys.role.permission.rpRoleId.tip': '角色ID号',
  'pages.update.sys.role.permission.rpRoleId.placeholder': '角色ID号',

  'pages.update.sys.role.permission.rpPermissionId.label': '权限ID',
  'pages.update.sys.role.permission.rpPermissionId.tip': '权限ID',
  'pages.update.sys.role.permission.rpPermissionId.placeholder': '权限ID',


  'pages.sys.role.permission.list.form.title': '查询',

  'pages.list.sys.role.permission.rpId.label': '角色权限表Id',
  'pages.list.sys.role.permission.rpId.tip': '角色权限表Id',
  'pages.list.sys.role.permission.rpId.placeholder': '角色权限表Id',

  'pages.list.sys.role.permission.rpRoleId.label': '角色ID号',
  'pages.list.sys.role.permission.rpRoleId.tip': '角色ID号',
  'pages.list.sys.role.permission.rpRoleId.placeholder': '角色ID号',

  'pages.list.sys.role.permission.rpPermissionId.label': '权限ID',
  'pages.list.sys.role.permission.rpPermissionId.tip': '权限ID',
  'pages.list.sys.role.permission.rpPermissionId.placeholder': '权限ID',


  'pages.list.sys.role.permission.option.delete':'删除',
  'pages.list.sys.role.permission.batch.delete':'批量删除',
  'pages.list.sys.role.permission.item':'项',
  'pages.list.sys.role.permission.chosen':'已选择',
  'pages.list.sys.role.permission.new':'新建',
  'pages.list.sys.role.permission.title':'查询',
  'pages.list.sys.role.permission.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.role.permission.option.edit':'修改',
  'pages.list.sys.role.permission.option.title':'操作',
  'pages.list.sys.role.permission.index.label':'序号',

};
