export default {
  'pages.create.sys.cms.notice.form.title': '创建',

  'pages.create.sys.cms.notice.scNoticeId.label': '公告编号',
  'pages.create.sys.cms.notice.scNoticeId.tip': '公告编号',
  'pages.create.sys.cms.notice.scNoticeId.placeholder': '公告编号',

  'pages.create.sys.cms.notice.scNoticeTitle.label': '公告标题',
  'pages.create.sys.cms.notice.scNoticeTitle.tip': '公告标题',
  'pages.create.sys.cms.notice.scNoticeTitle.placeholder': '公告标题',

  'pages.create.sys.cms.notice.scNoticeContent.label': '公告内容',
  'pages.create.sys.cms.notice.scNoticeContent.tip': '公告内容',
  'pages.create.sys.cms.notice.scNoticeContent.placeholder': '公告内容',

  'pages.create.sys.cms.notice.scNoticeIsNew.label': '是否是新文件',
  'pages.create.sys.cms.notice.scNoticeIsNew.tip': '是否是新文件',
  'pages.create.sys.cms.notice.scNoticeIsNew.placeholder': '是否是新文件',

  'pages.create.sys.cms.notice.scNoticeManager.label': '公告发布者',
  'pages.create.sys.cms.notice.scNoticeManager.tip': '公告发布者',
  'pages.create.sys.cms.notice.scNoticeManager.placeholder': '公告发布者',

  'pages.create.sys.cms.notice.scNoticeCreateDate.label': '创建日期',
  'pages.create.sys.cms.notice.scNoticeCreateDate.tip': '创建日期',
  'pages.create.sys.cms.notice.scNoticeCreateDate.placeholder': '创建日期',

  'pages.create.sys.cms.notice.scNoticeUpdateDate.label': '更新日期',
  'pages.create.sys.cms.notice.scNoticeUpdateDate.tip': '更新日期',
  'pages.create.sys.cms.notice.scNoticeUpdateDate.placeholder': '更新日期',


  'pages.update.sys.cms.notice.form.title': '更新',

  'pages.update.sys.cms.notice.scNoticeId.label': '公告编号',
  'pages.update.sys.cms.notice.scNoticeId.tip': '公告编号',
  'pages.update.sys.cms.notice.scNoticeId.placeholder': '公告编号',

  'pages.update.sys.cms.notice.scNoticeTitle.label': '公告标题',
  'pages.update.sys.cms.notice.scNoticeTitle.tip': '公告标题',
  'pages.update.sys.cms.notice.scNoticeTitle.placeholder': '公告标题',

  'pages.update.sys.cms.notice.scNoticeContent.label': '公告内容',
  'pages.update.sys.cms.notice.scNoticeContent.tip': '公告内容',
  'pages.update.sys.cms.notice.scNoticeContent.placeholder': '公告内容',

  'pages.update.sys.cms.notice.scNoticeIsNew.label': '是否是新文件',
  'pages.update.sys.cms.notice.scNoticeIsNew.tip': '是否是新文件',
  'pages.update.sys.cms.notice.scNoticeIsNew.placeholder': '是否是新文件',

  'pages.update.sys.cms.notice.scNoticeManager.label': '公告发布者',
  'pages.update.sys.cms.notice.scNoticeManager.tip': '公告发布者',
  'pages.update.sys.cms.notice.scNoticeManager.placeholder': '公告发布者',

  'pages.update.sys.cms.notice.scNoticeCreateDate.label': '创建日期',
  'pages.update.sys.cms.notice.scNoticeCreateDate.tip': '创建日期',
  'pages.update.sys.cms.notice.scNoticeCreateDate.placeholder': '创建日期',

  'pages.update.sys.cms.notice.scNoticeUpdateDate.label': '更新日期',
  'pages.update.sys.cms.notice.scNoticeUpdateDate.tip': '更新日期',
  'pages.update.sys.cms.notice.scNoticeUpdateDate.placeholder': '更新日期',


  'pages.list.sys.cms.notice.form.title': '查询',

  'pages.list.sys.cms.notice.scNoticeId.label': '公告编号',
  'pages.list.sys.cms.notice.scNoticeId.tip': '公告编号',
  'pages.list.sys.cms.notice.scNoticeId.placeholder': '公告编号',

  'pages.list.sys.cms.notice.scNoticeTitle.label': '公告标题',
  'pages.list.sys.cms.notice.scNoticeTitle.tip': '公告标题',
  'pages.list.sys.cms.notice.scNoticeTitle.placeholder': '公告标题',

  'pages.list.sys.cms.notice.scNoticeContent.label': '公告内容',
  'pages.list.sys.cms.notice.scNoticeContent.tip': '公告内容',
  'pages.list.sys.cms.notice.scNoticeContent.placeholder': '公告内容',

  'pages.list.sys.cms.notice.scNoticeIsNew.label': '是否是新文件',
  'pages.list.sys.cms.notice.scNoticeIsNew.tip': '是否是新文件',
  'pages.list.sys.cms.notice.scNoticeIsNew.placeholder': '是否是新文件',

  'pages.list.sys.cms.notice.scNoticeManager.label': '公告发布者',
  'pages.list.sys.cms.notice.scNoticeManager.tip': '公告发布者',
  'pages.list.sys.cms.notice.scNoticeManager.placeholder': '公告发布者',

  'pages.list.sys.cms.notice.scNoticeCreateDate.label': '创建日期',
  'pages.list.sys.cms.notice.scNoticeCreateDate.tip': '创建日期',
  'pages.list.sys.cms.notice.scNoticeCreateDate.placeholder': '创建日期',

  'pages.list.sys.cms.notice.scNoticeUpdateDate.label': '更新日期',
  'pages.list.sys.cms.notice.scNoticeUpdateDate.tip': '更新日期',
  'pages.list.sys.cms.notice.scNoticeUpdateDate.placeholder': '更新日期',


  'pages.list.sys.cms.notice.option.delete':'删除',
  'pages.list.sys.cms.notice.batch.delete':'批量删除',
  'pages.list.sys.cms.notice.item':'项',
  'pages.list.sys.cms.notice.chosen':'已选择',
  'pages.list.sys.cms.notice.new':'新建',
  'pages.list.sys.cms.notice.title':'查询',
  'pages.list.sys.cms.notice.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.cms.notice.option.edit':'修改',
  'pages.list.sys.cms.notice.option.title':'操作',
  'pages.list.sys.cms.notice.index.label':'序号',

};
