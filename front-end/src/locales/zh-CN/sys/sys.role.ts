export default {
  'pages.list.sys.role.option.relate.menu': '菜单',
  'pages.list.sys.role.option.relate.pms': '权限',
  'pages.list.sys.role.option.relate.dict': '字典',

  'pages.sys.role.create.form.title': '创建',

  'pages.create.sys.role.roleId.label': '角色ID',
  'pages.create.sys.role.roleId.tip': '角色ID',
  'pages.create.sys.role.roleId.placeholder': '角色ID',

  'pages.create.sys.role.roleName.label': '角色名称',
  'pages.create.sys.role.roleName.tip': '角色名称',
  'pages.create.sys.role.roleName.placeholder': '角色名称',

  'pages.create.sys.role.roleNameEn.label': '角色英文名',
  'pages.create.sys.role.roleNameEn.tip': '角色英文名',
  'pages.create.sys.role.roleNameEn.placeholder': '角色英文名',

  'pages.create.sys.role.roleState.label': '状态',
  'pages.create.sys.role.roleState.tip': '状态',
  'pages.create.sys.role.roleState.placeholder': '状态',

  'pages.create.sys.role.roleSys.label': '角色所属系统',
  'pages.create.sys.role.roleSys.tip': '角色所属系统',
  'pages.create.sys.role.roleSys.placeholder': '角色所属系统',


  'pages.sys.role.update.form.title': '修改',

  'pages.update.sys.role.roleId.label': '角色ID',
  'pages.update.sys.role.roleId.tip': '角色ID',
  'pages.update.sys.role.roleId.placeholder': '角色ID',

  'pages.update.sys.role.roleName.label': '角色名称',
  'pages.update.sys.role.roleName.tip': '角色名称',
  'pages.update.sys.role.roleName.placeholder': '角色名称',

  'pages.update.sys.role.roleNameEn.label': '角色英文名',
  'pages.update.sys.role.roleNameEn.tip': '角色英文名',
  'pages.update.sys.role.roleNameEn.placeholder': '角色英文名',

  'pages.update.sys.role.roleState.label': '状态',
  'pages.update.sys.role.roleState.tip': '状态',
  'pages.update.sys.role.roleState.placeholder': '状态',

  'pages.update.sys.role.roleSys.label': '角色所属系统',
  'pages.update.sys.role.roleSys.tip': '角色所属系统',
  'pages.update.sys.role.roleSys.placeholder': '角色所属系统',


  'pages.sys.role.list.form.title': '查询',

  'pages.list.sys.role.roleId.label': '角色ID',
  'pages.list.sys.role.roleId.tip': '角色ID',
  'pages.list.sys.role.roleId.placeholder': '角色ID',

  'pages.list.sys.role.roleName.label': '角色名称',
  'pages.list.sys.role.roleName.tip': '角色名称',
  'pages.list.sys.role.roleName.placeholder': '角色名称',

  'pages.list.sys.role.roleNameEn.label': '角色英文名',
  'pages.list.sys.role.roleNameEn.tip': '角色英文名',
  'pages.list.sys.role.roleNameEn.placeholder': '角色英文名',

  'pages.list.sys.role.roleState.label': '状态',
  'pages.list.sys.role.roleState.tip': '状态',
  'pages.list.sys.role.roleState.placeholder': '状态',

  'pages.list.sys.role.roleSys.label': '角色所属系统',
  'pages.list.sys.role.roleSys.tip': '角色所属系统',
  'pages.list.sys.role.roleSys.placeholder': '角色所属系统',


  'pages.list.sys.role.option.delete':'删除',
  'pages.list.sys.role.batch.delete':'批量删除',
  'pages.list.sys.role.item':'项',
  'pages.list.sys.role.chosen':'已选择',
  'pages.list.sys.role.new':'新建',
  'pages.list.sys.role.title':'查询',
  'pages.list.sys.role.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.role.option.edit':'修改',
  'pages.list.sys.role.option.title':'操作',
  'pages.list.sys.role.index.label':'序号',

};
