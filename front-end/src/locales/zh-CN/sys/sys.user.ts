export default {
  'pages.create.sys.user.form.title': 'Create',

  'pages.create.sys.user.userId.label': '用户ID',
  'pages.create.sys.user.userId.tip': '用户ID',
  'pages.create.sys.user.userId.placeholder': '用户ID',

  'pages.create.sys.user.userLoginName.label': '登录帐号',
  'pages.create.sys.user.userLoginName.tip': '登录帐号',
  'pages.create.sys.user.userLoginName.placeholder': '登录帐号',

  'pages.create.sys.user.userPassword.label': '用户密码',
  'pages.create.sys.user.userPassword.tip': '用户密码',
  'pages.create.sys.user.userPassword.placeholder': '用户密码',

  'pages.create.sys.user.userPasswordPrivate.label': '私有密码',
  'pages.create.sys.user.userPasswordPrivate.tip': '私有密码',
  'pages.create.sys.user.userPasswordPrivate.placeholder': '私有密码',

  'pages.create.sys.user.userNameAbbr.label': '姓名缩写',
  'pages.create.sys.user.userNameAbbr.tip': '姓名缩写',
  'pages.create.sys.user.userNameAbbr.placeholder': '姓名缩写',

  'pages.create.sys.user.userName.label': '姓名',
  'pages.create.sys.user.userName.tip': '姓名',
  'pages.create.sys.user.userName.placeholder': '姓名',

  'pages.create.sys.user.userNameEn.label': '英文名',
  'pages.create.sys.user.userNameEn.tip': '英文名',
  'pages.create.sys.user.userNameEn.placeholder': '英文名',

  'pages.create.sys.user.userSex.label': '性别',
  'pages.create.sys.user.userSex.tip': '性别',
  'pages.create.sys.user.userSex.placeholder': '性别',

  'pages.create.sys.user.userPhone.label': '手机号',
  'pages.create.sys.user.userPhone.tip': '手机号',
  'pages.create.sys.user.userPhone.placeholder': '手机号',

  'pages.create.sys.user.userEmail.label': '邮箱',
  'pages.create.sys.user.userEmail.tip': '邮箱',
  'pages.create.sys.user.userEmail.placeholder': '邮箱',

  'pages.create.sys.user.userImageUrl.label': '用户头像路径',
  'pages.create.sys.user.userImageUrl.tip': '用户头像路径',
  'pages.create.sys.user.userImageUrl.placeholder': '用户头像路径',

  'pages.create.sys.user.userOrder.label': '顺序',
  'pages.create.sys.user.userOrder.tip': '顺序',
  'pages.create.sys.user.userOrder.placeholder': '顺序',

  'pages.create.sys.user.userState.label': '状态',
  'pages.create.sys.user.userState.tip': '状态',
  'pages.create.sys.user.userState.placeholder': '状态',

  'pages.create.sys.user.userCode.label': '用户代码',
  'pages.create.sys.user.userCode.tip': '用户代码',
  'pages.create.sys.user.userCode.placeholder': '用户代码',

  'pages.create.sys.user.userCreateDate.label': '创建日期',
  'pages.create.sys.user.userCreateDate.tip': '创建日期',
  'pages.create.sys.user.userCreateDate.placeholder': '创建日期',

  'pages.create.sys.user.userUpdateDate.label': '更新日期',
  'pages.create.sys.user.userUpdateDate.tip': '更新日期',
  'pages.create.sys.user.userUpdateDate.placeholder': '更新日期',

  'pages.create.sys.user.userLastLoginTime.label': '用户最后登录时间',
  'pages.create.sys.user.userLastLoginTime.tip': '用户最后登录时间',
  'pages.create.sys.user.userLastLoginTime.placeholder': '用户最后登录时间',

  'pages.create.sys.user.userLastLoginIp.label': '用户最后登录Ip',
  'pages.create.sys.user.userLastLoginIp.tip': '用户最后登录Ip',
  'pages.create.sys.user.userLastLoginIp.placeholder': '用户最后登录Ip',

  'pages.create.sys.user.userUnit.label': '单位',
  'pages.create.sys.user.userUnit.tip': '单位',
  'pages.create.sys.user.userUnit.placeholder': '单位',

  'pages.create.sys.user.userJobTitle.label': '职务',
  'pages.create.sys.user.userJobTitle.tip': '职务',
  'pages.create.sys.user.userJobTitle.placeholder': '职务',

  'pages.create.sys.user.userAddr.label': '地址',
  'pages.create.sys.user.userAddr.tip': '地址',
  'pages.create.sys.user.userAddr.placeholder': '地址',

  'pages.create.sys.user.userPostcode.label': '邮编',
  'pages.create.sys.user.userPostcode.tip': '邮编',
  'pages.create.sys.user.userPostcode.placeholder': '邮编',

  'pages.create.sys.user.userSamSpecies.label': '种属',
  'pages.create.sys.user.userSamSpecies.tip': '种属',
  'pages.create.sys.user.userSamSpecies.placeholder': '种属',

  'pages.create.sys.user.userRoles.label': '角色',
  'pages.create.sys.user.userRoles.tip': '角色',
  'pages.create.sys.user.userRoles.placeholder': '角色',

  'pages.create.sys.user.userCurrSpecies.label': '当前绑定种属',
  'pages.create.sys.user.userCurrSpecies.tip': '当前绑定种属',
  'pages.create.sys.user.userCurrSpecies.placeholder': '当前绑定种属',

  'pages.update.sys.user.form.title': 'Update',

  'pages.update.sys.user.userId.label': '用户ID',
  'pages.update.sys.user.userId.tip': '用户ID',
  'pages.update.sys.user.userId.placeholder': '用户ID',

  'pages.update.sys.user.userLoginName.label': '登录帐号',
  'pages.update.sys.user.userLoginName.tip': '登录帐号',
  'pages.update.sys.user.userLoginName.placeholder': '登录帐号',

  'pages.update.sys.user.userPassword.label': '用户密码',
  'pages.update.sys.user.userPassword.tip': '用户密码',
  'pages.update.sys.user.userPassword.placeholder': '用户密码',

  'pages.update.sys.user.userPasswordPrivate.label': '私有密码',
  'pages.update.sys.user.userPasswordPrivate.tip': '私有密码',
  'pages.update.sys.user.userPasswordPrivate.placeholder': '私有密码',

  'pages.update.sys.user.userNameAbbr.label': '姓名缩写',
  'pages.update.sys.user.userNameAbbr.tip': '姓名缩写',
  'pages.update.sys.user.userNameAbbr.placeholder': '姓名缩写',

  'pages.update.sys.user.userName.label': '姓名',
  'pages.update.sys.user.userName.tip': '姓名',
  'pages.update.sys.user.userName.placeholder': '姓名',

  'pages.update.sys.user.userNameEn.label': '英文名',
  'pages.update.sys.user.userNameEn.tip': '英文名',
  'pages.update.sys.user.userNameEn.placeholder': '英文名',

  'pages.update.sys.user.userSex.label': '性别',
  'pages.update.sys.user.userSex.tip': '性别',
  'pages.update.sys.user.userSex.placeholder': '性别',

  'pages.update.sys.user.userPhone.label': '手机号',
  'pages.update.sys.user.userPhone.tip': '手机号',
  'pages.update.sys.user.userPhone.placeholder': '手机号',

  'pages.update.sys.user.userEmail.label': '邮箱',
  'pages.update.sys.user.userEmail.tip': '邮箱',
  'pages.update.sys.user.userEmail.placeholder': '邮箱',

  'pages.update.sys.user.userImageUrl.label': '用户头像路径',
  'pages.update.sys.user.userImageUrl.tip': '用户头像路径',
  'pages.update.sys.user.userImageUrl.placeholder': '用户头像路径',

  'pages.update.sys.user.userOrder.label': '顺序',
  'pages.update.sys.user.userOrder.tip': '顺序',
  'pages.update.sys.user.userOrder.placeholder': '顺序',

  'pages.update.sys.user.userState.label': '状态',
  'pages.update.sys.user.userState.tip': '状态',
  'pages.update.sys.user.userState.placeholder': '状态',

  'pages.update.sys.user.userCode.label': '用户代码',
  'pages.update.sys.user.userCode.tip': '用户代码',
  'pages.update.sys.user.userCode.placeholder': '用户代码',

  'pages.update.sys.user.userCreateDate.label': '创建日期',
  'pages.update.sys.user.userCreateDate.tip': '创建日期',
  'pages.update.sys.user.userCreateDate.placeholder': '创建日期',

  'pages.update.sys.user.userUpdateDate.label': '更新日期',
  'pages.update.sys.user.userUpdateDate.tip': '更新日期',
  'pages.update.sys.user.userUpdateDate.placeholder': '更新日期',

  'pages.update.sys.user.userLastLoginTime.label': '用户最后登录时间',
  'pages.update.sys.user.userLastLoginTime.tip': '用户最后登录时间',
  'pages.update.sys.user.userLastLoginTime.placeholder': '用户最后登录时间',

  'pages.update.sys.user.userLastLoginIp.label': '用户最后登录Ip',
  'pages.update.sys.user.userLastLoginIp.tip': '用户最后登录Ip',
  'pages.update.sys.user.userLastLoginIp.placeholder': '用户最后登录Ip',

  'pages.update.sys.user.userUnit.label': '单位',
  'pages.update.sys.user.userUnit.tip': '单位',
  'pages.update.sys.user.userUnit.placeholder': '单位',

  'pages.update.sys.user.userJobTitle.label': '职务',
  'pages.update.sys.user.userJobTitle.tip': '职务',
  'pages.update.sys.user.userJobTitle.placeholder': '职务',

  'pages.update.sys.user.userAddr.label': '地址',
  'pages.update.sys.user.userAddr.tip': '地址',
  'pages.update.sys.user.userAddr.placeholder': '地址',

  'pages.update.sys.user.userPostcode.label': '邮编',
  'pages.update.sys.user.userPostcode.tip': '邮编',
  'pages.update.sys.user.userPostcode.placeholder': '邮编',

  'pages.update.sys.user.userRoles.label': '角色',
  'pages.update.sys.user.userRoles.tip': '角色',
  'pages.update.sys.user.userRoles.placeholder': '角色',

  'pages.update.sys.user.userSamSpecies.label': '种属',
  'pages.update.sys.user.userSamSpecies.tip': '种属',
  'pages.update.sys.user.userSamSpecies.placeholder': '种属',

  'pages.update.sys.user.userCurrSpecies.label': '当前绑定种属',
  'pages.update.sys.user.userCurrSpecies.tip': '当前绑定种属',
  'pages.update.sys.user.userCurrSpecies.placeholder': '当前绑定种属',

  'pages.list.sys.user.form.title': 'Query',

  'pages.list.sys.user.userId.label': '用户ID',
  'pages.list.sys.user.userId.tip': '用户ID',
  'pages.list.sys.user.userId.placeholder': '用户ID',

  'pages.list.sys.user.userLoginName.label': '登录帐号',
  'pages.list.sys.user.userLoginName.tip': '登录帐号',
  'pages.list.sys.user.userLoginName.placeholder': '登录帐号',

  'pages.list.sys.user.userLoginNameLike.label': '登录帐号',
  'pages.list.sys.user.userLoginNameLike.tip': '登录帐号',
  'pages.list.sys.user.userLoginNameLike.placeholder': '登录帐号',

  'pages.list.sys.user.userPassword.label': '用户密码',
  'pages.list.sys.user.userPassword.tip': '用户密码',
  'pages.list.sys.user.userPassword.placeholder': '用户密码',

  'pages.list.sys.user.userPasswordPrivate.label': '私有密码',
  'pages.list.sys.user.userPasswordPrivate.tip': '私有密码',
  'pages.list.sys.user.userPasswordPrivate.placeholder': '私有密码',

  'pages.list.sys.user.userNameAbbr.label': '姓名缩写',
  'pages.list.sys.user.userNameAbbr.tip': '姓名缩写',
  'pages.list.sys.user.userNameAbbr.placeholder': '姓名缩写',

  'pages.list.sys.user.userRoles.label': '绑定角色',
  'pages.list.sys.user.userRoles.tip': '绑定角色',
  'pages.list.sys.user.userRoles.placeholder': '绑定角色',

  'pages.list.sys.user.userName.label': '姓名',
  'pages.list.sys.user.userName.tip': '姓名',
  'pages.list.sys.user.userName.placeholder': '姓名',

  'pages.list.sys.user.userNameLike.label': '姓名',
  'pages.list.sys.user.userNameLike.tip': '姓名',
  'pages.list.sys.user.userNameLike.placeholder': '姓名',

  'pages.list.sys.user.userNameEn.label': '英文名',
  'pages.list.sys.user.userNameEn.tip': '英文名',
  'pages.list.sys.user.userNameEn.placeholder': '英文名',

  'pages.list.sys.user.userSex.label': '性别',
  'pages.list.sys.user.userSex.tip': '性别',
  'pages.list.sys.user.userSex.placeholder': '性别',

  'pages.list.sys.user.userPhone.label': '手机号',
  'pages.list.sys.user.userPhone.tip': '手机号',
  'pages.list.sys.user.userPhone.placeholder': '手机号',

  'pages.list.sys.user.userEmail.label': '邮箱',
  'pages.list.sys.user.userEmail.tip': '邮箱',
  'pages.list.sys.user.userEmail.placeholder': '邮箱',

  'pages.list.sys.user.userImageUrl.label': '用户头像路径',
  'pages.list.sys.user.userImageUrl.tip': '用户头像路径',
  'pages.list.sys.user.userImageUrl.placeholder': '用户头像路径',

  'pages.list.sys.user.userOrder.label': '顺序',
  'pages.list.sys.user.userOrder.tip': '顺序',
  'pages.list.sys.user.userOrder.placeholder': '顺序',

  'pages.list.sys.user.userState.label': '状态',
  'pages.list.sys.user.userState.tip': '状态',
  'pages.list.sys.user.userState.placeholder': '状态',

  'pages.list.sys.user.userCode.label': '用户代码',
  'pages.list.sys.user.userCode.tip': '用户代码',
  'pages.list.sys.user.userCode.placeholder': '用户代码',

  'pages.list.sys.user.userCreateDate.label': '创建日期',
  'pages.list.sys.user.userCreateDate.tip': '创建日期',
  'pages.list.sys.user.userCreateDate.placeholder': '创建日期',

  'pages.list.sys.user.userUpdateDate.label': '更新日期',
  'pages.list.sys.user.userUpdateDate.tip': '更新日期',
  'pages.list.sys.user.userUpdateDate.placeholder': '更新日期',

  'pages.list.sys.user.userLastLoginTime.label': '用户最后登录时间',
  'pages.list.sys.user.userLastLoginTime.tip': '用户最后登录时间',
  'pages.list.sys.user.userLastLoginTime.placeholder': '用户最后登录时间',

  'pages.list.sys.user.userLastLoginIp.label': '用户最后登录Ip',
  'pages.list.sys.user.userLastLoginIp.tip': '用户最后登录Ip',
  'pages.list.sys.user.userLastLoginIp.placeholder': '用户最后登录Ip',

  'pages.list.sys.user.userUnit.label': '单位',
  'pages.list.sys.user.userUnit.tip': '单位',
  'pages.list.sys.user.userUnit.placeholder': '单位',

  'pages.list.sys.user.userJobTitle.label': '职务',
  'pages.list.sys.user.userJobTitle.tip': '职务',
  'pages.list.sys.user.userJobTitle.placeholder': '职务',

  'pages.list.sys.user.userAddr.label': '地址',
  'pages.list.sys.user.userAddr.tip': '地址',
  'pages.list.sys.user.userAddr.placeholder': '地址',

  'pages.list.sys.user.userPostcode.label': '邮编',
  'pages.list.sys.user.userPostcode.tip': '邮编',
  'pages.list.sys.user.userPostcode.placeholder': '邮编',

  'pages.list.sys.user.userSamSpecies.label': '种属',
  'pages.list.sys.user.userSamSpecies.tip': '种属',
  'pages.list.sys.user.userSamSpecies.placeholder': '种属',

  'pages.list.sys.user.userCurrSpecies.label': '当前绑定种属',
  'pages.list.sys.user.userCurrSpecies.tip': '当前绑定种属',
  'pages.list.sys.user.userCurrSpecies.placeholder': '当前绑定种属',

  'pages.list.sys.user.option.delete':'删除',
  'pages.list.sys.user.batch.delete':'批量删除',
  'pages.list.sys.user.item':'项',
  'pages.list.sys.user.chosen':'已选择',
  'pages.list.sys.user.new':'创建',
  'pages.list.sys.user.title':'查询',
  'pages.list.sys.user.option.delete.confirm':'删除操作将不可恢复，您确定要删除记录吗?',
  'pages.list.sys.user.option.edit':'编辑',
  'pages.list.sys.user.option.title':'操作',

  'pages.change.password.userPassword.label':'原密码',
  'pages.change.password.userPassword.length.tip':'原密码长度必须为6~24个字符!',
  'pages.change.password.userPassword.tip':'原密码为必填字段',
  'pages.change.password.userPassword.placeholder':'原密码',

  'pages.change.password.userPasswordNew.label':'新密码',
  'pages.change.password.userPasswordNew.length.tip':'新密码长度必须为6~24个字符!',
  'pages.change.password.userPasswordNew.tip':'新密码为必填字段',
  'pages.change.password.userPasswordNew.placeholder':'新密码',

  'pages.change.password.userPasswordRepeat.label':'确认新密码',
  'pages.change.password.userPasswordRepeat.length.tip':'确认新密码长度必须为6~24个字符!',
  'pages.change.password.userPasswordRepeat.tip':'确认新密码为必填字段',
  'pages.change.password.userPasswordRepeat.placeholder':'确认新密码',

  'pages.change.password.form.title':'修改密码',
  'pages.change.password.userPassword.right.tip':'原密码不正确',

  'pages.create.sys.user.userPassword.length.tip': '原密码长度必须为6~24个字符!',

  'pages.create.sys.user.userLoginName.length.tip':'用户帐号长度必须为6~24个字符!',
  'pages.create.sys.user.userLoginName.valid.tip':'用户帐号已存在!',

  'pages.update.sys.user.userLoginName.length.tip':'用户帐号长度必须为6~24个字符!',
  'pages.update.sys.user.userLoginName.valid.tip':'用户帐号已存在!',
  'pages.change.password.userPasswordRepeat.same.tip':'输入值与新密码不一致，请重新输入!',
};
