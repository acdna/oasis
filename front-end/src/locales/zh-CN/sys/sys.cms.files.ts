export default {
  'pages.sys.cms.files.create.form.title': '创建',

  'pages.create.sys.cms.files.scFileId.label': '文件编号',
  'pages.create.sys.cms.files.scFileId.tip': '文件编号',
  'pages.create.sys.cms.files.scFileId.placeholder': '文件编号',

  'pages.create.sys.cms.files.scFileName.label': '文件名称',
  'pages.create.sys.cms.files.scFileName.tip': '文件名称',
  'pages.create.sys.cms.files.scFileName.placeholder': '文件名称',

  'pages.create.sys.cms.files.scFileNameCn.label': '文件英文名',
  'pages.create.sys.cms.files.scFileNameCn.tip': '文件英文名',
  'pages.create.sys.cms.files.scFileNameCn.placeholder': '文件英文名',

  'pages.create.sys.cms.files.scFileType.label': '文件类型',
  'pages.create.sys.cms.files.scFileType.tip': '文件类型',
  'pages.create.sys.cms.files.scFileType.placeholder': '文件类型',

  'pages.create.sys.cms.files.scFilePath.label': '文件路径',
  'pages.create.sys.cms.files.scFilePath.tip': '文件路径',
  'pages.create.sys.cms.files.scFilePath.placeholder': '文件路径',

  'pages.create.sys.cms.files.scFileIsNew.label': '最新?',
  'pages.create.sys.cms.files.scFileIsNew.tip': '最新?',
  'pages.create.sys.cms.files.scFileIsNew.placeholder': '最新?',

  'pages.create.sys.cms.files.scFileManager.label': '上传者',
  'pages.create.sys.cms.files.scFileManager.tip': '上传者',
  'pages.create.sys.cms.files.scFileManager.placeholder': '上传者',

  'pages.create.sys.cms.files.scFileCreateDate.label': '创建日期',
  'pages.create.sys.cms.files.scFileCreateDate.tip': '创建日期',
  'pages.create.sys.cms.files.scFileCreateDate.placeholder': '创建日期',

  'pages.create.sys.cms.files.scFielUpdateDate.label': '更新日期',
  'pages.create.sys.cms.files.scFielUpdateDate.tip': '更新日期',
  'pages.create.sys.cms.files.scFielUpdateDate.placeholder': '更新日期',

  'pages.create.sys.cms.files.scFileSpecies.label': '种属',
  'pages.create.sys.cms.files.scFileSpecies.tip': '种属',
  'pages.create.sys.cms.files.scFileSpecies.placeholder': '种属',


  'pages.sys.cms.files.update.form.title': '更新',

  'pages.update.sys.cms.files.scFileId.label': '文件编号',
  'pages.update.sys.cms.files.scFileId.tip': '文件编号',
  'pages.update.sys.cms.files.scFileId.placeholder': '文件编号',

  'pages.update.sys.cms.files.scFileName.label': '文件名称',
  'pages.update.sys.cms.files.scFileName.tip': '文件名称',
  'pages.update.sys.cms.files.scFileName.placeholder': '文件名称',

  'pages.update.sys.cms.files.scFileNameCn.label': '文件英文名',
  'pages.update.sys.cms.files.scFileNameCn.tip': '文件英文名',
  'pages.update.sys.cms.files.scFileNameCn.placeholder': '文件英文名',

  'pages.update.sys.cms.files.scFileType.label': '文件类型',
  'pages.update.sys.cms.files.scFileType.tip': '文件类型',
  'pages.update.sys.cms.files.scFileType.placeholder': '文件类型',

  'pages.update.sys.cms.files.scFilePath.label': '文件路径',
  'pages.update.sys.cms.files.scFilePath.tip': '文件路径',
  'pages.update.sys.cms.files.scFilePath.placeholder': '文件路径',

  'pages.update.sys.cms.files.scFileIsNew.label': '最新?',
  'pages.update.sys.cms.files.scFileIsNew.tip': '最新?',
  'pages.update.sys.cms.files.scFileIsNew.placeholder': '最新?',

  'pages.update.sys.cms.files.scFileManager.label': '上传者',
  'pages.update.sys.cms.files.scFileManager.tip': '上传者',
  'pages.update.sys.cms.files.scFileManager.placeholder': '上传者',

  'pages.update.sys.cms.files.scFileCreateDate.label': '创建日期',
  'pages.update.sys.cms.files.scFileCreateDate.tip': '创建日期',
  'pages.update.sys.cms.files.scFileCreateDate.placeholder': '创建日期',

  'pages.update.sys.cms.files.scFielUpdateDate.label': '更新日期',
  'pages.update.sys.cms.files.scFielUpdateDate.tip': '更新日期',
  'pages.update.sys.cms.files.scFielUpdateDate.placeholder': '更新日期',

  'pages.update.sys.cms.files.scFileSpecies.label': '种属',
  'pages.update.sys.cms.files.scFileSpecies.tip': '种属',
  'pages.update.sys.cms.files.scFileSpecies.placeholder': '种属',


  'pages.sys.cms.files.list.form.title': '查询',

  'pages.list.sys.cms.files.scFileId.label': '文件编号',
  'pages.list.sys.cms.files.scFileId.tip': '文件编号',
  'pages.list.sys.cms.files.scFileId.placeholder': '文件编号',

  'pages.list.sys.cms.files.scFileName.label': '文件名称',
  'pages.list.sys.cms.files.scFileName.tip': '文件名称',
  'pages.list.sys.cms.files.scFileName.placeholder': '文件名称',

  'pages.list.sys.cms.files.scFileNameCn.label': '文件英文名',
  'pages.list.sys.cms.files.scFileNameCn.tip': '文件英文名',
  'pages.list.sys.cms.files.scFileNameCn.placeholder': '文件英文名',

  'pages.list.sys.cms.files.scFileType.label': '文件类型',
  'pages.list.sys.cms.files.scFileType.tip': '文件类型',
  'pages.list.sys.cms.files.scFileType.placeholder': '文件类型',

  'pages.list.sys.cms.files.scFilePath.label': '文件路径',
  'pages.list.sys.cms.files.scFilePath.tip': '文件路径',
  'pages.list.sys.cms.files.scFilePath.placeholder': '文件路径',

  'pages.list.sys.cms.files.scFileIsNew.label': '最新?',
  'pages.list.sys.cms.files.scFileIsNew.tip': '最新?',
  'pages.list.sys.cms.files.scFileIsNew.placeholder': '最新?',

  'pages.list.sys.cms.files.scFileManager.label': '上传者',
  'pages.list.sys.cms.files.scFileManager.tip': '上传者',
  'pages.list.sys.cms.files.scFileManager.placeholder': '上传者',

  'pages.list.sys.cms.files.scFileCreateDate.label': '创建日期',
  'pages.list.sys.cms.files.scFileCreateDate.tip': '创建日期',
  'pages.list.sys.cms.files.scFileCreateDate.placeholder': '创建日期',

  'pages.list.sys.cms.files.scFielUpdateDate.label': '更新日期',
  'pages.list.sys.cms.files.scFielUpdateDate.tip': '更新日期',
  'pages.list.sys.cms.files.scFielUpdateDate.placeholder': '更新日期',

  'pages.list.sys.cms.files.scFileSpecies.label': '种属',
  'pages.list.sys.cms.files.scFileSpecies.tip': '种属',
  'pages.list.sys.cms.files.scFileSpecies.placeholder': '种属',


  'pages.list.sys.cms.files.option.delete':'删除',
  'pages.list.sys.cms.files.batch.delete':'批量删除',
  'pages.list.sys.cms.files.item':'项',
  'pages.list.sys.cms.files.chosen':'已选择',
  'pages.list.sys.cms.files.new':'新建',
  'pages.list.sys.cms.files.title':'查询',
  'pages.list.sys.cms.files.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.cms.files.option.edit':'修改',
  'pages.list.sys.cms.files.option.title':'操作',
  'pages.list.sys.cms.files.index.label':'序号',
  'pages.list.sys.cms.files.index.download':'下载',

  'pages.create.sys.cms.files.title': '创建',
  'pages.update.sys.cms.files.title': '更新',
  'pages.create.sys.cms.files.cancel': '取消',
  'pages.update.sys.cms.files.cancel': '取消',
  'pages.create.sys.cms.files.submit': '提交',
  'pages.update.sys.cms.files.submit': '提交',
};
