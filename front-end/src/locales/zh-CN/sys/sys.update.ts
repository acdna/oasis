export default {
  'pages.sys.update.create.form.title': '创建',
  
  'pages.create.sys.update.updateId.label': '系统升级日志主键ID',
  'pages.create.sys.update.updateId.tip': '系统升级日志主键ID',
  'pages.create.sys.update.updateId.placeholder': '系统升级日志主键ID',
  
  'pages.create.sys.update.updateVersion.label': '升级版本号',
  'pages.create.sys.update.updateVersion.tip': '升级版本号',
  'pages.create.sys.update.updateVersion.placeholder': '升级版本号',
  
  'pages.create.sys.update.updateContent.label': '升级内容',
  'pages.create.sys.update.updateContent.tip': '升级内容',
  'pages.create.sys.update.updateContent.placeholder': '升级内容',
  
  'pages.create.sys.update.updateManager.label': '升级负责人',
  'pages.create.sys.update.updateManager.tip': '升级负责人',
  'pages.create.sys.update.updateManager.placeholder': '升级负责人',
  
  'pages.create.sys.update.updateCreateTime.label': '创建日期',
  'pages.create.sys.update.updateCreateTime.tip': '创建日期',
  'pages.create.sys.update.updateCreateTime.placeholder': '创建日期',
  

  'pages.sys.update.update.form.title': '更新',
  
  'pages.update.sys.update.updateId.label': '系统升级日志主键ID',
  'pages.update.sys.update.updateId.tip': '系统升级日志主键ID',
  'pages.update.sys.update.updateId.placeholder': '系统升级日志主键ID',
  
  'pages.update.sys.update.updateVersion.label': '升级版本号',
  'pages.update.sys.update.updateVersion.tip': '升级版本号',
  'pages.update.sys.update.updateVersion.placeholder': '升级版本号',
  
  'pages.update.sys.update.updateContent.label': '升级内容',
  'pages.update.sys.update.updateContent.tip': '升级内容',
  'pages.update.sys.update.updateContent.placeholder': '升级内容',
  
  'pages.update.sys.update.updateManager.label': '升级负责人',
  'pages.update.sys.update.updateManager.tip': '升级负责人',
  'pages.update.sys.update.updateManager.placeholder': '升级负责人',
  
  'pages.update.sys.update.updateCreateTime.label': '创建日期',
  'pages.update.sys.update.updateCreateTime.tip': '创建日期',
  'pages.update.sys.update.updateCreateTime.placeholder': '创建日期',
  

  'pages.sys.update.list.form.title': '查询',
  
  'pages.list.sys.update.updateId.label': '系统升级日志主键ID',
  'pages.list.sys.update.updateId.tip': '系统升级日志主键ID',
  'pages.list.sys.update.updateId.placeholder': '系统升级日志主键ID',
  
  'pages.list.sys.update.updateVersion.label': '升级版本号',
  'pages.list.sys.update.updateVersion.tip': '升级版本号',
  'pages.list.sys.update.updateVersion.placeholder': '升级版本号',
  
  'pages.list.sys.update.updateContent.label': '升级内容',
  'pages.list.sys.update.updateContent.tip': '升级内容',
  'pages.list.sys.update.updateContent.placeholder': '升级内容',
  
  'pages.list.sys.update.updateManager.label': '升级负责人',
  'pages.list.sys.update.updateManager.tip': '升级负责人',
  'pages.list.sys.update.updateManager.placeholder': '升级负责人',
  
  'pages.list.sys.update.updateCreateTime.label': '创建日期',
  'pages.list.sys.update.updateCreateTime.tip': '创建日期',
  'pages.list.sys.update.updateCreateTime.placeholder': '创建日期',
  

  'pages.list.sys.update.option.delete':'删除',
  'pages.list.sys.update.batch.delete':'批量删除',
  'pages.list.sys.update.item':'项',
  'pages.list.sys.update.chosen':'已选择',
  'pages.list.sys.update.new':'新建',
  'pages.list.sys.update.title':'查询',
  'pages.list.sys.update.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.update.option.edit':'修改',
  'pages.list.sys.update.option.title':'操作',
  'pages.list.sys.update.index.label':'序号',

};
