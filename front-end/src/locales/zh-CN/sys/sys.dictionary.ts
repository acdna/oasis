export default {
  'pages.create.sys.dictionary.dict.form.title': '创建字典',
  'pages.create.sys.dictionary.item.form.title': '创建字典项',
  'pages.sys.dictionary.create.form.title': '创建',
  'pages.list.sys.dictionary.option.create.item':'创建',

  'pages.create.sys.dictionary.dicId.label': 'ID',
  'pages.create.sys.dictionary.dicId.tip': 'ID',
  'pages.create.sys.dictionary.dicId.placeholder': 'ID',

  'pages.create.sys.dictionary.dicName.label': '字典项名',
  'pages.create.sys.dictionary.dicName.tip': '字典项名',
  'pages.create.sys.dictionary.dicName.placeholder': '字典项名',

  'pages.create.sys.dictionary.dicNameEn.label': '字典项英文名',
  'pages.create.sys.dictionary.dicNameEn.tip': '字典项英文名',
  'pages.create.sys.dictionary.dicNameEn.placeholder': '字典项英文名',

  'pages.create.sys.dictionary.dicValue.label': '字典值',
  'pages.create.sys.dictionary.dicValue.tip': '字典值',
  'pages.create.sys.dictionary.dicValue.placeholder': '字典值',

  'pages.create.sys.dictionary.dicGroup.label': '所属组',
  'pages.create.sys.dictionary.dicGroup.tip': '所属组',
  'pages.create.sys.dictionary.dicGroup.placeholder': '所属组',

  'pages.create.sys.dictionary.dicParentId.label': '父级ID',
  'pages.create.sys.dictionary.dicParentId.tip': '父级ID',
  'pages.create.sys.dictionary.dicParentId.placeholder': '父级ID',

  'pages.create.sys.dictionary.dicType.label': '字典类型',
  'pages.create.sys.dictionary.dicType.tip': '字典类型',
  'pages.create.sys.dictionary.dicType.placeholder': '字典类型',

  'pages.create.sys.dictionary.dicOrder.label': '排序',
  'pages.create.sys.dictionary.dicOrder.tip': '排序',
  'pages.create.sys.dictionary.dicOrder.placeholder': '排序',

  'pages.create.sys.dictionary.dicState.label': '启用?',
  'pages.create.sys.dictionary.dicState.tip': '启用?',
  'pages.create.sys.dictionary.dicState.placeholder': '启用?',

  'pages.create.sys.dictionary.dicParams.label': '参数',
  'pages.create.sys.dictionary.dicParams.tip': '参数',
  'pages.create.sys.dictionary.dicParams.placeholder': '参数',

  'pages.create.sys.dictionary.dicSpecies.label': '种属',
  'pages.create.sys.dictionary.dicSpecies.tip': '种属',
  'pages.create.sys.dictionary.dicSpecies.placeholder': '种属',

  'pages.create.sys.dictionary.dicCreateDate.label': '创建日期',
  'pages.create.sys.dictionary.dicCreateDate.tip': '创建日期',
  'pages.create.sys.dictionary.dicCreateDate.placeholder': '创建日期',

  'pages.create.sys.dictionary.dicUpdateDate.label': '日期',
  'pages.create.sys.dictionary.dicUpdateDate.tip': '日期',
  'pages.create.sys.dictionary.dicUpdateDate.placeholder': '日期',


  'pages.sys.dictionary.update.dict.form.title': '修改字典',
  'pages.sys.dictionary.update.item.form.title': '修改字典项',
  'pages.sys.dictionary.update.form.title': '修改',

  'pages.update.sys.dictionary.dicId.label': 'ID',
  'pages.update.sys.dictionary.dicId.tip': 'ID',
  'pages.update.sys.dictionary.dicId.placeholder': 'ID',

  'pages.update.sys.dictionary.dicName.label': '字典名称',
  'pages.update.sys.dictionary.dicName.tip': '字典名称',
  'pages.update.sys.dictionary.dicName.placeholder': '字典名称',

  'pages.update.sys.dictionary.dicNameEn.label': '英文名',
  'pages.update.sys.dictionary.dicNameEn.tip': '英文名',
  'pages.update.sys.dictionary.dicNameEn.placeholder': '英文名',

  'pages.update.sys.dictionary.dicValue.label': '字典值',
  'pages.update.sys.dictionary.dicValue.tip': '字典值',
  'pages.update.sys.dictionary.dicValue.placeholder': '字典值',

  'pages.update.sys.dictionary.dicGroup.label': '所属组',
  'pages.update.sys.dictionary.dicGroup.tip': '所属组',
  'pages.update.sys.dictionary.dicGroup.placeholder': '所属组',

  'pages.update.sys.dictionary.dicParentId.label': '父级ID',
  'pages.update.sys.dictionary.dicParentId.tip': '父级ID',
  'pages.update.sys.dictionary.dicParentId.placeholder': '父级ID',

  'pages.update.sys.dictionary.dicType.label': '字典类型',
  'pages.update.sys.dictionary.dicType.tip': '字典类型',
  'pages.update.sys.dictionary.dicType.placeholder': '字典类型',

  'pages.update.sys.dictionary.dicOrder.label': '排序',
  'pages.update.sys.dictionary.dicOrder.tip': '排序',
  'pages.update.sys.dictionary.dicOrder.placeholder': '排序',

  'pages.update.sys.dictionary.dicState.label': '状态',
  'pages.update.sys.dictionary.dicState.tip': '状态',
  'pages.update.sys.dictionary.dicState.placeholder': '状态',

  'pages.update.sys.dictionary.dicParams.label': 'dicParams',
  'pages.update.sys.dictionary.dicParams.tip': 'dicParams',
  'pages.update.sys.dictionary.dicParams.placeholder': 'dicParams',

  'pages.update.sys.dictionary.dicSpecies.label': '关联种属',
  'pages.update.sys.dictionary.dicSpecies.tip': '关联种属',
  'pages.update.sys.dictionary.dicSpecies.placeholder': '关联种属',

  'pages.update.sys.dictionary.dicCreateDate.label': '创建日期',
  'pages.update.sys.dictionary.dicCreateDate.tip': '创建日期',
  'pages.update.sys.dictionary.dicCreateDate.placeholder': '创建日期',

  'pages.update.sys.dictionary.dicUpdateDate.label': '更新日期',
  'pages.update.sys.dictionary.dicUpdateDate.tip': '更新日期',
  'pages.update.sys.dictionary.dicUpdateDate.placeholder': '更新日期',

  'pages.list.sys.dictionary.new.dict': '创建字典',
  'pages.list.sys.dictionary.init.dict': '初始化字典',

  'pages.sys.dictionary.list.form.title': '查询',

  'pages.list.sys.dictionary.dicId.label': 'ID',
  'pages.list.sys.dictionary.dicId.tip': 'ID',
  'pages.list.sys.dictionary.dicId.placeholder': 'ID',

  'pages.list.sys.dictionary.dicName.label': '字典名称',
  'pages.list.sys.dictionary.dicName.tip': '字典名称',
  'pages.list.sys.dictionary.dicName.placeholder': '字典名称',

  'pages.list.sys.dictionary.dicNameLike.label': '字典名称',
  'pages.list.sys.dictionary.dicNameLike.tip': '字典名称',
  'pages.list.sys.dictionary.dicNameLike.placeholder': '字典名称',

  'pages.list.sys.dictionary.dicNameEn.label': '英文名',
  'pages.list.sys.dictionary.dicNameEn.tip': '英文名',
  'pages.list.sys.dictionary.dicNameEn.placeholder': '英文名',

  'pages.list.sys.dictionary.dicNameEnLike.label': '英文名',
  'pages.list.sys.dictionary.dicNameEnLike.tip': '英文名',
  'pages.list.sys.dictionary.dicNameEnLike.placeholder': '英文名',

  'pages.list.sys.dictionary.dicValue.label': '字典值',
  'pages.list.sys.dictionary.dicValue.tip': '字典值',
  'pages.list.sys.dictionary.dicValue.placeholder': '字典值',

  'pages.list.sys.dictionary.dicGroup.label': '所属组',
  'pages.list.sys.dictionary.dicGroup.tip': '所属组',
  'pages.list.sys.dictionary.dicGroup.placeholder': '所属组',

  'pages.list.sys.dictionary.dicParentId.label': '父级ID',
  'pages.list.sys.dictionary.dicParentId.tip': '父级ID',
  'pages.list.sys.dictionary.dicParentId.placeholder': '父级ID',

  'pages.list.sys.dictionary.dicType.label': '字典类型',
  'pages.list.sys.dictionary.dicType.tip': '字典类型',
  'pages.list.sys.dictionary.dicType.placeholder': '字典类型',

  'pages.list.sys.dictionary.dicOrder.label': '排序',
  'pages.list.sys.dictionary.dicOrder.tip': '排序',
  'pages.list.sys.dictionary.dicOrder.placeholder': '排序',

  'pages.list.sys.dictionary.dicState.label': '状态',
  'pages.list.sys.dictionary.dicState.tip': '状态',
  'pages.list.sys.dictionary.dicState.placeholder': '状态',

  'pages.list.sys.dictionary.dicParams.label': 'dicParams',
  'pages.list.sys.dictionary.dicParams.tip': 'dicParams',
  'pages.list.sys.dictionary.dicParams.placeholder': 'dicParams',

  'pages.list.sys.dictionary.dicSpecies.label': '关联种属',
  'pages.list.sys.dictionary.dicSpecies.tip': '关联种属',
  'pages.list.sys.dictionary.dicSpecies.placeholder': '关联种属',

  'pages.list.sys.dictionary.dicCreateDate.label': '创建日期',
  'pages.list.sys.dictionary.dicCreateDate.tip': '创建日期',
  'pages.list.sys.dictionary.dicCreateDate.placeholder': '创建日期',

  'pages.list.sys.dictionary.dicUpdateDate.label': '更新日期',
  'pages.list.sys.dictionary.dicUpdateDate.tip': '更新日期',
  'pages.list.sys.dictionary.dicUpdateDate.placeholder': '更新日期',


  'pages.list.sys.dictionary.option.delete':'删除',
  'pages.list.sys.dictionary.batch.delete':'批量删除',
  'pages.list.sys.dictionary.item':'项',
  'pages.list.sys.dictionary.chosen':'已选择',
  'pages.list.sys.dictionary.new':'新建',
  'pages.list.sys.dictionary.title':'查询',
  'pages.list.sys.dictionary.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.dictionary.option.init.confirm':'将先删除已有字典再进行初始化操作,请确定是否继续?',
  'pages.list.sys.dictionary.option.edit':'修改',
  'pages.list.sys.dictionary.option.title':'操作',

};
