export default {
  'pages.list.sys.permission.option.init.confirm.front': '将先删除已有前端权限再进行初始化操作,请确定是否继续?',
  'pages.list.sys.permission.option.init.confirm.back': '将先删除已有后端权限再进行初始化操作,请确定是否继续?',
  'pages.create.sys.permission.item.title': '创建权限项',
  'pages.update.sys.permission.item.title': '更新权限项',

  'pages.create.sys.permission.form.title': '创建',
  'pages.create.sys.permission.cancel': '取消',
  'pages.update.sys.permission.cancel': '取消',
  'pages.create.sys.permission.submit': '提交',
  'pages.update.sys.permission.submit': '提交',

  'pages.create.sys.permission.title': '创建',
  'pages.update.sys.permission.title': '更新',
  'pages.sys.permission.create.form.title': '创建',

  'pages.create.sys.permission.perId.label': '权限ID',
  'pages.create.sys.permission.perId.tip': '权限ID',
  'pages.create.sys.permission.perId.placeholder': '权限ID',

  'pages.create.sys.permission.perName.label': '名称',
  'pages.create.sys.permission.perName.tip': '名称',
  'pages.create.sys.permission.perName.placeholder': '名称',

  'pages.create.sys.permission.perNameEn.label': '英文名',
  'pages.create.sys.permission.perNameEn.tip': '英文名',
  'pages.create.sys.permission.perNameEn.placeholder': '英文名',

  'pages.create.sys.permission.perType.label': '类型',
  'pages.create.sys.permission.perType.tip': '类型',
  'pages.create.sys.permission.perType.placeholder': '类型',

  'pages.create.sys.permission.perUrl.label': 'URL',
  'pages.create.sys.permission.perUrl.tip': 'URL',
  'pages.create.sys.permission.perUrl.placeholder': 'URL',

  'pages.create.sys.permission.perMethod.label': '方法',
  'pages.create.sys.permission.perMethod.tip': '方法',
  'pages.create.sys.permission.perMethod.placeholder': '方法',

  'pages.create.sys.permission.perParentId.label': '父节点ID',
  'pages.create.sys.permission.perParentId.tip': '父节点ID',
  'pages.create.sys.permission.perParentId.placeholder': '父节点ID',

  'pages.create.sys.permission.perOrder.label': '排序',
  'pages.create.sys.permission.perOrder.tip': '排序',
  'pages.create.sys.permission.perOrder.placeholder': '排序',

  'pages.create.sys.permission.perRemark.label': '备注',
  'pages.create.sys.permission.perRemark.tip': '备注',
  'pages.create.sys.permission.perRemark.placeholder': '备注',

  'pages.create.sys.permission.perState.label': '状态',
  'pages.create.sys.permission.perState.tip': '状态',
  'pages.create.sys.permission.perState.placeholder': '状态',

  'pages.create.sys.permission.perSystem.label': '子系统',
  'pages.create.sys.permission.perSystem.tip': '子系统',
  'pages.create.sys.permission.perSystem.placeholder': '子系统',

  'pages.create.sys.permission.perModule.label': '分组',
  'pages.create.sys.permission.perModule.tip': '分组',
  'pages.create.sys.permission.perModule.placeholder': '分组',


  'pages.sys.permission.update.form.title': '更新',

  'pages.update.sys.permission.perId.label': '权限ID',
  'pages.update.sys.permission.perId.tip': '权限ID',
  'pages.update.sys.permission.perId.placeholder': '权限ID',

  'pages.update.sys.permission.perName.label': '名称',
  'pages.update.sys.permission.perName.tip': '名称',
  'pages.update.sys.permission.perName.placeholder': '名称',

  'pages.update.sys.permission.perNameEn.label': '英文名',
  'pages.update.sys.permission.perNameEn.tip': '英文名',
  'pages.update.sys.permission.perNameEn.placeholder': '英文名',

  'pages.update.sys.permission.perType.label': '类型',
  'pages.update.sys.permission.perType.tip': '类型',
  'pages.update.sys.permission.perType.placeholder': '类型',

  'pages.update.sys.permission.perUrl.label': 'URL',
  'pages.update.sys.permission.perUrl.tip': 'URL',
  'pages.update.sys.permission.perUrl.placeholder': 'URL',

  'pages.update.sys.permission.perMethod.label': '方法',
  'pages.update.sys.permission.perMethod.tip': '方法',
  'pages.update.sys.permission.perMethod.placeholder': '方法',

  'pages.update.sys.permission.perParentId.label': '父节点ID',
  'pages.update.sys.permission.perParentId.tip': '父节点ID',
  'pages.update.sys.permission.perParentId.placeholder': '父节点ID',

  'pages.update.sys.permission.perOrder.label': '排序',
  'pages.update.sys.permission.perOrder.tip': '排序',
  'pages.update.sys.permission.perOrder.placeholder': '排序',

  'pages.update.sys.permission.perRemark.label': '备注',
  'pages.update.sys.permission.perRemark.tip': '备注',
  'pages.update.sys.permission.perRemark.placeholder': '备注',

  'pages.update.sys.permission.perState.label': '状态',
  'pages.update.sys.permission.perState.tip': '状态',
  'pages.update.sys.permission.perState.placeholder': '状态',

  'pages.update.sys.permission.perSystem.label': '子系统',
  'pages.update.sys.permission.perSystem.tip': '子系统',
  'pages.update.sys.permission.perSystem.placeholder': '子系统',

  'pages.update.sys.permission.perModule.label': '分组',
  'pages.update.sys.permission.perModule.tip': '分组',
  'pages.update.sys.permission.perModule.placeholder': '分组',


  'pages.sys.permission.list.form.title': '查询',

  'pages.list.sys.permission.perId.label': '权限ID',
  'pages.list.sys.permission.perId.tip': '权限ID',
  'pages.list.sys.permission.perId.placeholder': '权限ID',

  'pages.list.sys.permission.perName.label': '名称',
  'pages.list.sys.permission.perName.tip': '名称',
  'pages.list.sys.permission.perName.placeholder': '名称',

  'pages.list.sys.permission.perNameEn.label': '英文名',
  'pages.list.sys.permission.perNameEn.tip': '英文名',
  'pages.list.sys.permission.perNameEn.placeholder': '英文名',

  'pages.list.sys.permission.perType.label': '类型',
  'pages.list.sys.permission.perType.tip': '类型',
  'pages.list.sys.permission.perType.placeholder': '类型',

  'pages.list.sys.permission.perUrl.label': 'URL',
  'pages.list.sys.permission.perUrl.tip': 'URL',
  'pages.list.sys.permission.perUrl.placeholder': 'URL',

  'pages.list.sys.permission.perMethod.label': '方法',
  'pages.list.sys.permission.perMethod.tip': '方法',
  'pages.list.sys.permission.perMethod.placeholder': '方法',

  'pages.list.sys.permission.perParentId.label': '父节点ID',
  'pages.list.sys.permission.perParentId.tip': '父节点ID',
  'pages.list.sys.permission.perParentId.placeholder': '父节点ID',

  'pages.list.sys.permission.perOrder.label': '排序',
  'pages.list.sys.permission.perOrder.tip': '排序',
  'pages.list.sys.permission.perOrder.placeholder': '排序',

  'pages.list.sys.permission.perRemark.label': '备注',
  'pages.list.sys.permission.perRemark.tip': '备注',
  'pages.list.sys.permission.perRemark.placeholder': '备注',

  'pages.list.sys.permission.perState.label': '状态',
  'pages.list.sys.permission.perState.tip': '状态',
  'pages.list.sys.permission.perState.placeholder': '状态',

  'pages.list.sys.permission.perSystem.label': '子系统',
  'pages.list.sys.permission.perSystem.tip': '子系统',
  'pages.list.sys.permission.perSystem.placeholder': '子系统',

  'pages.list.sys.permission.perModule.label': '分组',
  'pages.list.sys.permission.perModule.tip': '分组',
  'pages.list.sys.permission.perModule.placeholder': '分组',


  'pages.list.sys.permission.option.delete':'删除',
  'pages.list.sys.permission.batch.delete':'批量删除',
  'pages.list.sys.permission.item':'项',
  'pages.list.sys.permission.chosen':'已选择',
  'pages.list.sys.permission.new':'新建',
  'pages.list.sys.permission.title':'查询',
  'pages.list.sys.permission.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.permission.option.edit':'修改',
  'pages.list.sys.permission.option.title':'操作',
  'pages.list.sys.permission.index.label':'序号',

  'pages.list.sys.permission.init.back':'初始化后端权限',
  'pages.list.sys.permission.init.front':'初始化前端权限',
  'pages.list.sys.permission.option.create':'新建',

};
