export default {
  'pages.list.sys.menu.tree.tab': '菜单树',
  'pages.list.sys.menu.tree.add': '添加',
  'pages.list.sys.menu.tree.delete': '删除',
  'pages.list.sys.menu.tree.btn.delete.confirm':'删除不可恢复，你确定要删除吗?',

  'pages.sys.menu.create.form.title': '创建',
  'pages.create.sys.menu.form.item.title': '创建字典项',

  'pages.create.sys.menu.menuId.label': '菜单ID号',
  'pages.create.sys.menu.menuId.tip': '菜单ID号',
  'pages.create.sys.menu.menuId.placeholder': '菜单ID号',

  'pages.create.sys.menu.menuName.label': '菜单名称',
  'pages.create.sys.menu.menuName.tip': '菜单名称',
  'pages.create.sys.menu.menuName.placeholder': '菜单名称',

  'pages.create.sys.menu.menuNameEn.label': '英文菜单名',
  'pages.create.sys.menu.menuNameEn.tip': '英文菜单名',
  'pages.create.sys.menu.menuNameEn.placeholder': '英文菜单名',

  'pages.create.sys.menu.menuUrl.label': 'URL',
  'pages.create.sys.menu.menuUrl.tip': 'URL',
  'pages.create.sys.menu.menuUrl.placeholder': 'URL',

  'pages.create.sys.menu.menuParentId.label': '父菜单ID',
  'pages.create.sys.menu.menuParentId.tip': '父菜单ID',
  'pages.create.sys.menu.menuParentId.placeholder': '父菜单ID',

  'pages.create.sys.menu.menuOrder.label': '顺序',
  'pages.create.sys.menu.menuOrder.tip': '顺序',
  'pages.create.sys.menu.menuOrder.placeholder': '顺序',

  'pages.create.sys.menu.menuRemark.label': '备注',
  'pages.create.sys.menu.menuRemark.tip': '备注',
  'pages.create.sys.menu.menuRemark.placeholder': '备注',

  'pages.create.sys.menu.menuState.label': '状态',
  'pages.create.sys.menu.menuState.tip': '状态',
  'pages.create.sys.menu.menuState.placeholder': '状态',

  'pages.create.sys.menu.menuType.label': '类型',
  'pages.create.sys.menu.menuType.tip': '类型',
  'pages.create.sys.menu.menuType.placeholder': '类型',

  'pages.create.sys.menu.menuIconClass.label': '图标',
  'pages.create.sys.menu.menuIconClass.tip': '图标',
  'pages.create.sys.menu.menuIconClass.placeholder': '图标',


  'pages.sys.menu.update.form.title': '更新',

  'pages.update.sys.menu.menuId.label': '菜单ID号',
  'pages.update.sys.menu.menuId.tip': '菜单ID号',
  'pages.update.sys.menu.menuId.placeholder': '菜单ID号',

  'pages.update.sys.menu.menuName.label': '菜单名称',
  'pages.update.sys.menu.menuName.tip': '菜单名称',
  'pages.update.sys.menu.menuName.placeholder': '菜单名称',

  'pages.update.sys.menu.menuNameEn.label': '英文菜单名',
  'pages.update.sys.menu.menuNameEn.tip': '英文菜单名',
  'pages.update.sys.menu.menuNameEn.placeholder': '英文菜单名',

  'pages.update.sys.menu.menuUrl.label': 'URL',
  'pages.update.sys.menu.menuUrl.tip': 'URL',
  'pages.update.sys.menu.menuUrl.placeholder': 'URL',

  'pages.update.sys.menu.menuParentId.label': '父菜单ID',
  'pages.update.sys.menu.menuParentId.tip': '父菜单ID',
  'pages.update.sys.menu.menuParentId.placeholder': '父菜单ID',

  'pages.update.sys.menu.menuOrder.label': '顺序',
  'pages.update.sys.menu.menuOrder.tip': '顺序',
  'pages.update.sys.menu.menuOrder.placeholder': '顺序',

  'pages.update.sys.menu.menuRemark.label': '备注',
  'pages.update.sys.menu.menuRemark.tip': '备注',
  'pages.update.sys.menu.menuRemark.placeholder': '备注',

  'pages.update.sys.menu.menuState.label': '状态',
  'pages.update.sys.menu.menuState.tip': '状态',
  'pages.update.sys.menu.menuState.placeholder': '状态',

  'pages.update.sys.menu.menuType.label': '类型',
  'pages.update.sys.menu.menuType.tip': '类型',
  'pages.update.sys.menu.menuType.placeholder': '类型',

  'pages.update.sys.menu.menuIconClass.label': '图标',
  'pages.update.sys.menu.menuIconClass.tip': '图标',
  'pages.update.sys.menu.menuIconClass.placeholder': '图标',


  'pages.sys.menu.list.form.title': '查询',

  'pages.list.sys.menu.menuId.label': '菜单ID号',
  'pages.list.sys.menu.menuId.tip': '菜单ID号',
  'pages.list.sys.menu.menuId.placeholder': '菜单ID号',

  'pages.list.sys.menu.menuName.label': '菜单名称',
  'pages.list.sys.menu.menuName.tip': '菜单名称',
  'pages.list.sys.menu.menuName.placeholder': '菜单名称',

  'pages.list.sys.menu.menuNameEn.label': '英文菜单名',
  'pages.list.sys.menu.menuNameEn.tip': '英文菜单名',
  'pages.list.sys.menu.menuNameEn.placeholder': '英文菜单名',

  'pages.list.sys.menu.menuUrl.label': 'URL',
  'pages.list.sys.menu.menuUrl.tip': 'URL',
  'pages.list.sys.menu.menuUrl.placeholder': 'URL',

  'pages.list.sys.menu.menuParentId.label': '父菜单ID',
  'pages.list.sys.menu.menuParentId.tip': '父菜单ID',
  'pages.list.sys.menu.menuParentId.placeholder': '父菜单ID',

  'pages.list.sys.menu.menuOrder.label': '顺序',
  'pages.list.sys.menu.menuOrder.tip': '顺序',
  'pages.list.sys.menu.menuOrder.placeholder': '顺序',

  'pages.list.sys.menu.menuRemark.label': '备注',
  'pages.list.sys.menu.menuRemark.tip': '备注',
  'pages.list.sys.menu.menuRemark.placeholder': '备注',

  'pages.list.sys.menu.menuState.label': '状态',
  'pages.list.sys.menu.menuState.tip': '状态',
  'pages.list.sys.menu.menuState.placeholder': '状态',

  'pages.list.sys.menu.menuType.label': '类型',
  'pages.list.sys.menu.menuType.tip': '类型',
  'pages.list.sys.menu.menuType.placeholder': '类型',

  'pages.list.sys.menu.menuIconClass.label': '图标',
  'pages.list.sys.menu.menuIconClass.tip': '图标',
  'pages.list.sys.menu.menuIconClass.placeholder': '图标',


  'pages.list.sys.menu.option.delete':'删除',
  'pages.list.sys.menu.batch.delete':'批量删除',
  'pages.list.sys.menu.item':'项',
  'pages.list.sys.menu.chosen':'已选择',
  'pages.list.sys.menu.new':'新建',
  'pages.list.sys.menu.title':'查询',
  'pages.list.sys.menu.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.menu.option.edit':'修改',
  'pages.list.sys.menu.option.title':'操作',
  'pages.list.sys.menu.index.label':'序号',

};
