export default {
  'pages.sys.col.detail.create.form.title': '创建',
  
  'pages.create.sys.col.detail.cdId.label': '主键ID',
  'pages.create.sys.col.detail.cdId.tip': '主键ID',
  'pages.create.sys.col.detail.cdId.placeholder': '主键ID',
  
  'pages.create.sys.col.detail.cdTableName.label': '表名',
  'pages.create.sys.col.detail.cdTableName.tip': '表名',
  'pages.create.sys.col.detail.cdTableName.placeholder': '表名',
  
  'pages.create.sys.col.detail.cdColName.label': '列名',
  'pages.create.sys.col.detail.cdColName.tip': '列名',
  'pages.create.sys.col.detail.cdColName.placeholder': '列名',
  
  'pages.create.sys.col.detail.cdDescName.label': '列中文名',
  'pages.create.sys.col.detail.cdDescName.tip': '列中文名',
  'pages.create.sys.col.detail.cdDescName.placeholder': '列中文名',
  
  'pages.create.sys.col.detail.cdDictName.label': '字典名',
  'pages.create.sys.col.detail.cdDictName.tip': '字典名',
  'pages.create.sys.col.detail.cdDictName.placeholder': '字典名',
  
  'pages.create.sys.col.detail.cdMode.label': '模式',
  'pages.create.sys.col.detail.cdMode.tip': '模式',
  'pages.create.sys.col.detail.cdMode.placeholder': '模式',
  
  'pages.create.sys.col.detail.cdCreateDate.label': '创建日期',
  'pages.create.sys.col.detail.cdCreateDate.tip': '创建日期',
  'pages.create.sys.col.detail.cdCreateDate.placeholder': '创建日期',
  
  'pages.create.sys.col.detail.cdUpdateDate.label': '更新日期',
  'pages.create.sys.col.detail.cdUpdateDate.tip': '更新日期',
  'pages.create.sys.col.detail.cdUpdateDate.placeholder': '更新日期',
  

  'pages.sys.col.detail.update.form.title': '更新',
  
  'pages.update.sys.col.detail.cdId.label': '主键ID',
  'pages.update.sys.col.detail.cdId.tip': '主键ID',
  'pages.update.sys.col.detail.cdId.placeholder': '主键ID',
  
  'pages.update.sys.col.detail.cdTableName.label': '表名',
  'pages.update.sys.col.detail.cdTableName.tip': '表名',
  'pages.update.sys.col.detail.cdTableName.placeholder': '表名',
  
  'pages.update.sys.col.detail.cdColName.label': '列名',
  'pages.update.sys.col.detail.cdColName.tip': '列名',
  'pages.update.sys.col.detail.cdColName.placeholder': '列名',
  
  'pages.update.sys.col.detail.cdDescName.label': '列中文名',
  'pages.update.sys.col.detail.cdDescName.tip': '列中文名',
  'pages.update.sys.col.detail.cdDescName.placeholder': '列中文名',
  
  'pages.update.sys.col.detail.cdDictName.label': '字典名',
  'pages.update.sys.col.detail.cdDictName.tip': '字典名',
  'pages.update.sys.col.detail.cdDictName.placeholder': '字典名',
  
  'pages.update.sys.col.detail.cdMode.label': '模式',
  'pages.update.sys.col.detail.cdMode.tip': '模式',
  'pages.update.sys.col.detail.cdMode.placeholder': '模式',
  
  'pages.update.sys.col.detail.cdCreateDate.label': '创建日期',
  'pages.update.sys.col.detail.cdCreateDate.tip': '创建日期',
  'pages.update.sys.col.detail.cdCreateDate.placeholder': '创建日期',
  
  'pages.update.sys.col.detail.cdUpdateDate.label': '更新日期',
  'pages.update.sys.col.detail.cdUpdateDate.tip': '更新日期',
  'pages.update.sys.col.detail.cdUpdateDate.placeholder': '更新日期',
  

  'pages.sys.col.detail.list.form.title': '查询',
  
  'pages.list.sys.col.detail.cdId.label': '主键ID',
  'pages.list.sys.col.detail.cdId.tip': '主键ID',
  'pages.list.sys.col.detail.cdId.placeholder': '主键ID',
  
  'pages.list.sys.col.detail.cdTableName.label': '表名',
  'pages.list.sys.col.detail.cdTableName.tip': '表名',
  'pages.list.sys.col.detail.cdTableName.placeholder': '表名',
  
  'pages.list.sys.col.detail.cdColName.label': '列名',
  'pages.list.sys.col.detail.cdColName.tip': '列名',
  'pages.list.sys.col.detail.cdColName.placeholder': '列名',
  
  'pages.list.sys.col.detail.cdDescName.label': '列中文名',
  'pages.list.sys.col.detail.cdDescName.tip': '列中文名',
  'pages.list.sys.col.detail.cdDescName.placeholder': '列中文名',
  
  'pages.list.sys.col.detail.cdDictName.label': '字典名',
  'pages.list.sys.col.detail.cdDictName.tip': '字典名',
  'pages.list.sys.col.detail.cdDictName.placeholder': '字典名',
  
  'pages.list.sys.col.detail.cdMode.label': '模式',
  'pages.list.sys.col.detail.cdMode.tip': '模式',
  'pages.list.sys.col.detail.cdMode.placeholder': '模式',
  
  'pages.list.sys.col.detail.cdCreateDate.label': '创建日期',
  'pages.list.sys.col.detail.cdCreateDate.tip': '创建日期',
  'pages.list.sys.col.detail.cdCreateDate.placeholder': '创建日期',
  
  'pages.list.sys.col.detail.cdUpdateDate.label': '更新日期',
  'pages.list.sys.col.detail.cdUpdateDate.tip': '更新日期',
  'pages.list.sys.col.detail.cdUpdateDate.placeholder': '更新日期',
  

  'pages.list.sys.col.detail.option.delete':'删除',
  'pages.list.sys.col.detail.batch.delete':'批量删除',
  'pages.list.sys.col.detail.item':'项',
  'pages.list.sys.col.detail.chosen':'已选择',
  'pages.list.sys.col.detail.new':'新建',
  'pages.list.sys.col.detail.title':'查询',
  'pages.list.sys.col.detail.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.col.detail.option.edit':'修改',
  'pages.list.sys.col.detail.option.title':'操作',
  'pages.list.sys.col.detail.index.label':'序号',

};
