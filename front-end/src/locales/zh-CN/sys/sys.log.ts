export default {
  'pages.sys.log.create.form.title': '创建',

  'pages.create.sys.log.logId.label': '日志ID号',
  'pages.create.sys.log.logId.tip': '日志ID号',
  'pages.create.sys.log.logId.placeholder': '日志ID号',

  'pages.create.sys.log.logUser.label': '操作人',
  'pages.create.sys.log.logUser.tip': '操作人',
  'pages.create.sys.log.logUser.placeholder': '操作人',

  'pages.create.sys.log.logTime.label': '操作时间',
  'pages.create.sys.log.logTime.tip': '操作时间',
  'pages.create.sys.log.logTime.placeholder': '操作时间',

  'pages.create.sys.log.logIp.label': 'IP地址',
  'pages.create.sys.log.logIp.tip': 'IP地址',
  'pages.create.sys.log.logIp.placeholder': 'IP地址',

  'pages.create.sys.log.logUrl.label': '操作URL',
  'pages.create.sys.log.logUrl.tip': '操作URL',
  'pages.create.sys.log.logUrl.placeholder': '操作URL',

  'pages.create.sys.log.logTitle.label': '操作结果',
  'pages.create.sys.log.logTitle.tip': '操作结果',
  'pages.create.sys.log.logTitle.placeholder': '操作结果',

  'pages.create.sys.log.logContent.label': '内容',
  'pages.create.sys.log.logContent.tip': '内容',
  'pages.create.sys.log.logContent.placeholder': '内容',

  'pages.create.sys.log.logType.label': '操作类型',
  'pages.create.sys.log.logType.tip': '操作类型',
  'pages.create.sys.log.logType.placeholder': '操作类型',


  'pages.sys.log.update.form.title': '更新',

  'pages.update.sys.log.logId.label': '日志ID号',
  'pages.update.sys.log.logId.tip': '日志ID号',
  'pages.update.sys.log.logId.placeholder': '日志ID号',

  'pages.update.sys.log.logUser.label': '操作人',
  'pages.update.sys.log.logUser.tip': '操作人',
  'pages.update.sys.log.logUser.placeholder': '操作人',

  'pages.update.sys.log.logTime.label': '操作时间',
  'pages.update.sys.log.logTime.tip': '操作时间',
  'pages.update.sys.log.logTime.placeholder': '操作时间',

  'pages.update.sys.log.logIp.label': 'IP地址',
  'pages.update.sys.log.logIp.tip': 'IP地址',
  'pages.update.sys.log.logIp.placeholder': 'IP地址',

  'pages.update.sys.log.logUrl.label': '操作URL',
  'pages.update.sys.log.logUrl.tip': '操作URL',
  'pages.update.sys.log.logUrl.placeholder': '操作URL',

  'pages.update.sys.log.logTitle.label': '操作结果',
  'pages.update.sys.log.logTitle.tip': '操作结果',
  'pages.update.sys.log.logTitle.placeholder': '操作结果',

  'pages.update.sys.log.logContent.label': '内容',
  'pages.update.sys.log.logContent.tip': '内容',
  'pages.update.sys.log.logContent.placeholder': '内容',

  'pages.update.sys.log.logType.label': '操作类型',
  'pages.update.sys.log.logType.tip': '操作类型',
  'pages.update.sys.log.logType.placeholder': '操作类型',


  'pages.sys.log.list.form.title': '查询',

  'pages.list.sys.log.logId.label': '日志ID号',
  'pages.list.sys.log.logId.tip': '日志ID号',
  'pages.list.sys.log.logId.placeholder': '日志ID号',

  'pages.list.sys.log.logUser.label': '操作人',
  'pages.list.sys.log.logUser.tip': '操作人',
  'pages.list.sys.log.logUser.placeholder': '操作人',

  'pages.list.sys.log.logTime.label': '操作时间',
  'pages.list.sys.log.logTime.tip': '操作时间',
  'pages.list.sys.log.logTime.placeholder': '操作时间',

  'pages.list.sys.log.logIp.label': 'IP地址',
  'pages.list.sys.log.logIp.tip': 'IP地址',
  'pages.list.sys.log.logIp.placeholder': 'IP地址',

  'pages.list.sys.log.logUrl.label': '操作URL',
  'pages.list.sys.log.logUrl.tip': '操作URL',
  'pages.list.sys.log.logUrl.placeholder': '操作URL',

  'pages.list.sys.log.logTitle.label': '操作结果',
  'pages.list.sys.log.logTitle.tip': '操作结果',
  'pages.list.sys.log.logTitle.placeholder': '操作结果',

  'pages.list.sys.log.logContent.label': '内容',
  'pages.list.sys.log.logContent.tip': '内容',
  'pages.list.sys.log.logContent.placeholder': '内容',

  'pages.list.sys.log.logType.label': '操作类型',
  'pages.list.sys.log.logType.tip': '操作类型',
  'pages.list.sys.log.logType.placeholder': '操作类型',


  'pages.list.sys.log.option.delete':'删除',
  'pages.list.sys.log.batch.delete':'批量删除',
  'pages.list.sys.log.item':'项',
  'pages.list.sys.log.chosen':'已选择',
  'pages.list.sys.log.new':'新建',
  'pages.list.sys.log.title':'查询',
  'pages.list.sys.log.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.log.option.edit':'修改',
  'pages.list.sys.log.option.title':'操作',
  'pages.list.sys.log.index.label':'序号',

};
