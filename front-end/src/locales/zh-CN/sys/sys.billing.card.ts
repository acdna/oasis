export default {
  'pages.sys.billing.card.create.form.title': '创建',
  
  'pages.create.sys.billing.card.bcId.label': 'id',
  'pages.create.sys.billing.card.bcId.tip': 'id',
  'pages.create.sys.billing.card.bcId.placeholder': 'id',
  
  'pages.create.sys.billing.card.bcSerialNumber.label': '序列号信息',
  'pages.create.sys.billing.card.bcSerialNumber.tip': '序列号信息',
  'pages.create.sys.billing.card.bcSerialNumber.placeholder': '序列号信息',
  
  'pages.create.sys.billing.card.bcAmount.label': '总额',
  'pages.create.sys.billing.card.bcAmount.tip': '总额',
  'pages.create.sys.billing.card.bcAmount.placeholder': '总额',
  
  'pages.create.sys.billing.card.bcTotalTime.label': '总时间',
  'pages.create.sys.billing.card.bcTotalTime.tip': '总时间',
  'pages.create.sys.billing.card.bcTotalTime.placeholder': '总时间',
  
  'pages.create.sys.billing.card.bcType.label': '类型',
  'pages.create.sys.billing.card.bcType.tip': '类型',
  'pages.create.sys.billing.card.bcType.placeholder': '类型',
  
  'pages.create.sys.billing.card.bcAccount.label': '绑定帐号',
  'pages.create.sys.billing.card.bcAccount.tip': '绑定帐号',
  'pages.create.sys.billing.card.bcAccount.placeholder': '绑定帐号',
  
  'pages.create.sys.billing.card.bcPassword.label': '绑定密码',
  'pages.create.sys.billing.card.bcPassword.tip': '绑定密码',
  'pages.create.sys.billing.card.bcPassword.placeholder': '绑定密码',
  
  'pages.create.sys.billing.card.bcState.label': '状态',
  'pages.create.sys.billing.card.bcState.tip': '状态',
  'pages.create.sys.billing.card.bcState.placeholder': '状态',
  
  'pages.create.sys.billing.card.bcBiggestGeneCount.label': '最大指纹数',
  'pages.create.sys.billing.card.bcBiggestGeneCount.tip': '最大指纹数',
  'pages.create.sys.billing.card.bcBiggestGeneCount.placeholder': '最大指纹数',
  
  'pages.create.sys.billing.card.bcCreateDate.label': '创建日期',
  'pages.create.sys.billing.card.bcCreateDate.tip': '创建日期',
  'pages.create.sys.billing.card.bcCreateDate.placeholder': '创建日期',
  

  'pages.sys.billing.card.update.form.title': '更新',
  
  'pages.update.sys.billing.card.bcId.label': 'id',
  'pages.update.sys.billing.card.bcId.tip': 'id',
  'pages.update.sys.billing.card.bcId.placeholder': 'id',
  
  'pages.update.sys.billing.card.bcSerialNumber.label': '序列号信息',
  'pages.update.sys.billing.card.bcSerialNumber.tip': '序列号信息',
  'pages.update.sys.billing.card.bcSerialNumber.placeholder': '序列号信息',
  
  'pages.update.sys.billing.card.bcAmount.label': '总额',
  'pages.update.sys.billing.card.bcAmount.tip': '总额',
  'pages.update.sys.billing.card.bcAmount.placeholder': '总额',
  
  'pages.update.sys.billing.card.bcTotalTime.label': '总时间',
  'pages.update.sys.billing.card.bcTotalTime.tip': '总时间',
  'pages.update.sys.billing.card.bcTotalTime.placeholder': '总时间',
  
  'pages.update.sys.billing.card.bcType.label': '类型',
  'pages.update.sys.billing.card.bcType.tip': '类型',
  'pages.update.sys.billing.card.bcType.placeholder': '类型',
  
  'pages.update.sys.billing.card.bcAccount.label': '绑定帐号',
  'pages.update.sys.billing.card.bcAccount.tip': '绑定帐号',
  'pages.update.sys.billing.card.bcAccount.placeholder': '绑定帐号',
  
  'pages.update.sys.billing.card.bcPassword.label': '绑定密码',
  'pages.update.sys.billing.card.bcPassword.tip': '绑定密码',
  'pages.update.sys.billing.card.bcPassword.placeholder': '绑定密码',
  
  'pages.update.sys.billing.card.bcState.label': '状态',
  'pages.update.sys.billing.card.bcState.tip': '状态',
  'pages.update.sys.billing.card.bcState.placeholder': '状态',
  
  'pages.update.sys.billing.card.bcBiggestGeneCount.label': '最大指纹数',
  'pages.update.sys.billing.card.bcBiggestGeneCount.tip': '最大指纹数',
  'pages.update.sys.billing.card.bcBiggestGeneCount.placeholder': '最大指纹数',
  
  'pages.update.sys.billing.card.bcCreateDate.label': '创建日期',
  'pages.update.sys.billing.card.bcCreateDate.tip': '创建日期',
  'pages.update.sys.billing.card.bcCreateDate.placeholder': '创建日期',
  

  'pages.sys.billing.card.list.form.title': '查询',
  
  'pages.list.sys.billing.card.bcId.label': 'id',
  'pages.list.sys.billing.card.bcId.tip': 'id',
  'pages.list.sys.billing.card.bcId.placeholder': 'id',
  
  'pages.list.sys.billing.card.bcSerialNumber.label': '序列号信息',
  'pages.list.sys.billing.card.bcSerialNumber.tip': '序列号信息',
  'pages.list.sys.billing.card.bcSerialNumber.placeholder': '序列号信息',
  
  'pages.list.sys.billing.card.bcAmount.label': '总额',
  'pages.list.sys.billing.card.bcAmount.tip': '总额',
  'pages.list.sys.billing.card.bcAmount.placeholder': '总额',
  
  'pages.list.sys.billing.card.bcTotalTime.label': '总时间',
  'pages.list.sys.billing.card.bcTotalTime.tip': '总时间',
  'pages.list.sys.billing.card.bcTotalTime.placeholder': '总时间',
  
  'pages.list.sys.billing.card.bcType.label': '类型',
  'pages.list.sys.billing.card.bcType.tip': '类型',
  'pages.list.sys.billing.card.bcType.placeholder': '类型',
  
  'pages.list.sys.billing.card.bcAccount.label': '绑定帐号',
  'pages.list.sys.billing.card.bcAccount.tip': '绑定帐号',
  'pages.list.sys.billing.card.bcAccount.placeholder': '绑定帐号',
  
  'pages.list.sys.billing.card.bcPassword.label': '绑定密码',
  'pages.list.sys.billing.card.bcPassword.tip': '绑定密码',
  'pages.list.sys.billing.card.bcPassword.placeholder': '绑定密码',
  
  'pages.list.sys.billing.card.bcState.label': '状态',
  'pages.list.sys.billing.card.bcState.tip': '状态',
  'pages.list.sys.billing.card.bcState.placeholder': '状态',
  
  'pages.list.sys.billing.card.bcBiggestGeneCount.label': '最大指纹数',
  'pages.list.sys.billing.card.bcBiggestGeneCount.tip': '最大指纹数',
  'pages.list.sys.billing.card.bcBiggestGeneCount.placeholder': '最大指纹数',
  
  'pages.list.sys.billing.card.bcCreateDate.label': '创建日期',
  'pages.list.sys.billing.card.bcCreateDate.tip': '创建日期',
  'pages.list.sys.billing.card.bcCreateDate.placeholder': '创建日期',
  

  'pages.list.sys.billing.card.option.delete':'删除',
  'pages.list.sys.billing.card.batch.delete':'批量删除',
  'pages.list.sys.billing.card.item':'项',
  'pages.list.sys.billing.card.chosen':'已选择',
  'pages.list.sys.billing.card.new':'新建',
  'pages.list.sys.billing.card.title':'查询',
  'pages.list.sys.billing.card.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.billing.card.option.edit':'修改',
  'pages.list.sys.billing.card.option.title':'操作',
  'pages.list.sys.billing.card.index.label':'序号',

};
