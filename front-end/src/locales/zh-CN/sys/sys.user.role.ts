export default {
  'pages.sys.user.role.create.form.title': '创建',

  'pages.create.sys.user.role.urId.label': '用户角色ID',
  'pages.create.sys.user.role.urId.tip': '用户角色ID',
  'pages.create.sys.user.role.urId.placeholder': '用户角色ID',

  'pages.create.sys.user.role.urRoleId.label': '角色ID',
  'pages.create.sys.user.role.urRoleId.tip': '角色ID',
  'pages.create.sys.user.role.urRoleId.placeholder': '角色ID',

  'pages.create.sys.user.role.urUserLoginName.label': '用户帐号',
  'pages.create.sys.user.role.urUserLoginName.tip': '用户帐号',
  'pages.create.sys.user.role.urUserLoginName.placeholder': '用户帐号',


  'pages.sys.user.role.update.form.title': '更新',

  'pages.update.sys.user.role.urId.label': '用户角色ID',
  'pages.update.sys.user.role.urId.tip': '用户角色ID',
  'pages.update.sys.user.role.urId.placeholder': '用户角色ID',

  'pages.update.sys.user.role.urRoleId.label': '角色ID',
  'pages.update.sys.user.role.urRoleId.tip': '角色ID',
  'pages.update.sys.user.role.urRoleId.placeholder': '角色ID',

  'pages.update.sys.user.role.urUserLoginName.label': '用户帐号',
  'pages.update.sys.user.role.urUserLoginName.tip': '用户帐号',
  'pages.update.sys.user.role.urUserLoginName.placeholder': '用户帐号',


  'pages.sys.user.role.list.form.title': '查询',

  'pages.list.sys.user.role.urId.label': '用户角色ID',
  'pages.list.sys.user.role.urId.tip': '用户角色ID',
  'pages.list.sys.user.role.urId.placeholder': '用户角色ID',

  'pages.list.sys.user.role.urRoleId.label': '角色ID',
  'pages.list.sys.user.role.urRoleId.tip': '角色ID',
  'pages.list.sys.user.role.urRoleId.placeholder': '角色ID',

  'pages.list.sys.user.role.urUserLoginName.label': '用户帐号',
  'pages.list.sys.user.role.urUserLoginName.tip': '用户帐号',
  'pages.list.sys.user.role.urUserLoginName.placeholder': '用户帐号',


  'pages.list.sys.user.role.option.delete':'删除',
  'pages.list.sys.user.role.batch.delete':'批量删除',
  'pages.list.sys.user.role.item':'项',
  'pages.list.sys.user.role.chosen':'已选择',
  'pages.list.sys.user.role.new':'新建',
  'pages.list.sys.user.role.title':'查询',
  'pages.list.sys.user.role.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.user.role.option.edit':'修改',
  'pages.list.sys.user.role.option.title':'操作',
  'pages.list.sys.user.role.index.label':'序号',

};
