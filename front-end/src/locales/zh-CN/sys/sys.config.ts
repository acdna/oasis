export default {
  'pages.create.sys.config.form.item.title': '创建参数项',
  'pages.update.sys.config.form.item.title': '修改参数项',
  'pages.create.sys.config.form.title': '创建',
  'pages.update.sys.config.form.title': '创建',
  'pages.create.sys.config.form.cancel': '取消',
  'pages.create.sys.config.form.submit': '提交',
  'pages.create.sys.config.form.item.cancel': '取消',
  'pages.create.sys.config.form.item.submit': '提交',
  'pages.update.sys.config.form.cancel': '取消',
  'pages.update.sys.config.form.submit': '提交',
  'pages.update.sys.config.form.item.cancel': '取消',
  'pages.update.sys.config.form.item.submit': '提交',

  'pages.create.sys.config.conId.label': 'ID',
  'pages.create.sys.config.conId.tip': 'ID',
  'pages.create.sys.config.conId.placeholder': 'ID',

  'pages.create.sys.config.conName.label': '名称',
  'pages.create.sys.config.conName.tip': '参数名称',
  'pages.create.sys.config.conName.placeholder': '参数名称',

  'pages.create.sys.config.conNameEn.label': '英文参数名称',
  'pages.create.sys.config.conNameEn.tip': '英文参数名称',
  'pages.create.sys.config.conNameEn.placeholder': '英文参数名称',

  'pages.create.sys.config.conValue.label': '值',
  'pages.create.sys.config.conValue.tip': '参数值',
  'pages.create.sys.config.conValue.placeholder': '参数值',

  'pages.create.sys.config.conGroup.label': '所属组',
  'pages.create.sys.config.conGroup.tip': '所属组',
  'pages.create.sys.config.conGroup.placeholder': '所属组',

  'pages.create.sys.config.conParentId.label': '父级ID',
  'pages.create.sys.config.conParentId.tip': '父级ID',
  'pages.create.sys.config.conParentId.placeholder': '父级ID',

  'pages.create.sys.config.conType.label': '类型',
  'pages.create.sys.config.conType.tip': '类型',
  'pages.create.sys.config.conType.placeholder': '类型',

  'pages.create.sys.config.conParamType.label': '参数类型',
  'pages.create.sys.config.conParamType.tip': '参数类型',
  'pages.create.sys.config.conParamType.placeholder': '参数类型',

  'pages.create.sys.config.conCreateDate.label': '创建日期',
  'pages.create.sys.config.conCreateDate.tip': '创建日期',
  'pages.create.sys.config.conCreateDate.placeholder': '创建日期',

  'pages.create.sys.config.conUpdateDate.label': '更新日期',
  'pages.create.sys.config.conUpdateDate.tip': '更新日期',
  'pages.create.sys.config.conUpdateDate.placeholder': '更新日期',

  'pages.create.sys.config.conManager.label': '用户',
  'pages.create.sys.config.conManager.tip': '用户',
  'pages.create.sys.config.conManager.placeholder': '用户',

  'pages.create.sys.config.conSpecies.label': '种属',
  'pages.create.sys.config.conSpecies.tip': '种属',
  'pages.create.sys.config.conSpecies.placeholder': '种属',

  'pages.create.sys.config.conOrder.label': '排序',
  'pages.create.sys.config.conOrder.tip': '排序',
  'pages.create.sys.config.conOrder.placeholder': '排序',

  'pages.create.sys.config.conState.label': '状态',
  'pages.create.sys.config.conState.tip': '状态',
  'pages.create.sys.config.conState.placeholder': '状态',

  'pages.create.sys.config.conRemark.label': '描述',
  'pages.create.sys.config.conRemark.tip': '描述',
  'pages.create.sys.config.conRemark.placeholder': '描述',


  'pages.sys.config.update.form.title': '更新',

  'pages.update.sys.config.conId.label': 'ID',
  'pages.update.sys.config.conId.tip': 'ID',
  'pages.update.sys.config.conId.placeholder': 'ID',

  'pages.update.sys.config.conName.label': '名称',
  'pages.update.sys.config.conName.tip': '参数名称',
  'pages.update.sys.config.conName.placeholder': '参数名称',

  'pages.update.sys.config.conNameEn.label': '英文参数名称',
  'pages.update.sys.config.conNameEn.tip': '英文参数名称',
  'pages.update.sys.config.conNameEn.placeholder': '英文参数名称',

  'pages.update.sys.config.conValue.label': '值',
  'pages.update.sys.config.conValue.tip': '参数值',
  'pages.update.sys.config.conValue.placeholder': '参数值',

  'pages.update.sys.config.conGroup.label': '所属组',
  'pages.update.sys.config.conGroup.tip': '所属组',
  'pages.update.sys.config.conGroup.placeholder': '所属组',

  'pages.update.sys.config.conParentId.label': '父级ID',
  'pages.update.sys.config.conParentId.tip': '父级ID',
  'pages.update.sys.config.conParentId.placeholder': '父级ID',

  'pages.update.sys.config.conType.label': '类型',
  'pages.update.sys.config.conType.tip': '参数类型',
  'pages.update.sys.config.conType.placeholder': '参数类型',

  'pages.update.sys.config.conParamType.label': '参数类型',
  'pages.update.sys.config.conParamType.tip': '参数类型',
  'pages.update.sys.config.conParamType.placeholder': '参数类型',

  'pages.update.sys.config.conCreateDate.label': '创建日期',
  'pages.update.sys.config.conCreateDate.tip': '创建日期',
  'pages.update.sys.config.conCreateDate.placeholder': '创建日期',

  'pages.update.sys.config.conUpdateDate.label': '更新日期',
  'pages.update.sys.config.conUpdateDate.tip': '更新日期',
  'pages.update.sys.config.conUpdateDate.placeholder': '更新日期',

  'pages.update.sys.config.conManager.label': '用户',
  'pages.update.sys.config.conManager.tip': '用户',
  'pages.update.sys.config.conManager.placeholder': '用户',

  'pages.update.sys.config.conSpecies.label': '种属',
  'pages.update.sys.config.conSpecies.tip': '种属',
  'pages.update.sys.config.conSpecies.placeholder': '种属',

  'pages.update.sys.config.conOrder.label': '排序',
  'pages.update.sys.config.conOrder.tip': '排序',
  'pages.update.sys.config.conOrder.placeholder': '排序',

  'pages.update.sys.config.conState.label': '状态',
  'pages.update.sys.config.conState.tip': '状态',
  'pages.update.sys.config.conState.placeholder': '状态',

  'pages.update.sys.config.conRemark.label': '描述',
  'pages.update.sys.config.conRemark.tip': '描述',
  'pages.update.sys.config.conRemark.placeholder': '描述',


  'pages.sys.config.list.form.title': '查询',
  'pages.list.sys.config.option.create.item': '创建',

  'pages.list.sys.config.conId.label': 'ID',
  'pages.list.sys.config.conId.tip': 'ID',
  'pages.list.sys.config.conId.placeholder': 'ID',

  'pages.list.sys.config.conName.label': '名称',
  'pages.list.sys.config.conName.tip': '参数名称',
  'pages.list.sys.config.conName.placeholder': '参数名称',

  'pages.list.sys.config.conNameEn.label': '英文参数名称',
  'pages.list.sys.config.conNameEn.tip': '英文参数名称',
  'pages.list.sys.config.conNameEn.placeholder': '英文参数名称',

  'pages.list.sys.config.conValue.label': '值',
  'pages.list.sys.config.conValue.tip': '参数值',
  'pages.list.sys.config.conValue.placeholder': '参数值',

  'pages.list.sys.config.conGroup.label': '所属组',
  'pages.list.sys.config.conGroup.tip': '所属组',
  'pages.list.sys.config.conGroup.placeholder': '所属组',

  'pages.list.sys.config.conParentId.label': '父级ID',
  'pages.list.sys.config.conParentId.tip': '父级ID',
  'pages.list.sys.config.conParentId.placeholder': '父级ID',

  'pages.list.sys.config.conType.label': '节点类型',
  'pages.list.sys.config.conType.tip': '节点类型',
  'pages.list.sys.config.conType.placeholder': '节点类型',

  'pages.list.sys.config.conParamType.label': '参数类型',
  'pages.list.sys.config.conParamType.tip': '参数类型',
  'pages.list.sys.config.conParamType.placeholder': '参数类型',

  'pages.list.sys.config.conCreateDate.label': '创建日期',
  'pages.list.sys.config.conCreateDate.tip': '创建日期',
  'pages.list.sys.config.conCreateDate.placeholder': '创建日期',

  'pages.list.sys.config.conUpdateDate.label': '更新日期',
  'pages.list.sys.config.conUpdateDate.tip': '更新日期',
  'pages.list.sys.config.conUpdateDate.placeholder': '更新日期',

  'pages.list.sys.config.conManager.label': '用户',
  'pages.list.sys.config.conManager.tip': '用户',
  'pages.list.sys.config.conManager.placeholder': '用户',

  'pages.list.sys.config.conSpecies.label': '种属',
  'pages.list.sys.config.conSpecies.tip': '种属',
  'pages.list.sys.config.conSpecies.placeholder': '种属',

  'pages.list.sys.config.conOrder.label': '排序',
  'pages.list.sys.config.conOrder.tip': '排序',
  'pages.list.sys.config.conOrder.placeholder': '排序',

  'pages.list.sys.config.conState.label': '状态',
  'pages.list.sys.config.conState.tip': '状态',
  'pages.list.sys.config.conState.placeholder': '状态',

  'pages.list.sys.config.conRemark.label': '描述',
  'pages.list.sys.config.conRemark.tip': '描述',
  'pages.list.sys.config.conRemark.placeholder': '描述',


  'pages.list.sys.config.option.delete':'删除',
  'pages.list.sys.config.batch.delete':'批量删除',
  'pages.list.sys.config.item':'项',
  'pages.list.sys.config.chosen':'已选择',
  'pages.list.sys.config.new':'新建',
  'pages.list.sys.config.title':'查询',
  'pages.list.sys.config.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.config.option.edit':'修改',
  'pages.list.sys.config.option.title':'操作',
  'pages.list.sys.config.index.label':'序号',
  'pages.list.sys.config.option.init.front.confirm':'将先删除已有参数再进行初始化操作,请确定是否继续?',
  'pages.list.sys.config.init':'初始化参数',

};
