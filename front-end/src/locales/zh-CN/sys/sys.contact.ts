export default {
  'pages.create.sys.contact.form.title': '创建',

  'pages.create.sys.contact.contactId.label': '编号',
  'pages.create.sys.contact.contactId.tip': '反馈编号',
  'pages.create.sys.contact.contactId.placeholder': '反馈编号',

  'pages.create.sys.contact.contactAuthor.label': '用户',
  'pages.create.sys.contact.contactAuthor.tip': '反馈用户',
  'pages.create.sys.contact.contactAuthor.placeholder': '反馈用户',

  'pages.create.sys.contact.contactEmail.label': '邮箱',
  'pages.create.sys.contact.contactEmail.tip': '反馈邮箱',
  'pages.create.sys.contact.contactEmail.placeholder': '反馈邮箱',

  'pages.create.sys.contact.contactSubject.label': '主题',
  'pages.create.sys.contact.contactSubject.tip': '反馈主题',
  'pages.create.sys.contact.contactSubject.placeholder': '反馈主题',

  'pages.create.sys.contact.contactText.label': '内容',
  'pages.create.sys.contact.contactText.tip': '反馈内容',
  'pages.create.sys.contact.contactText.placeholder': '反馈内容',

  'pages.create.sys.contact.contactCreateDate.label': '创建日期',
  'pages.create.sys.contact.contactCreateDate.tip': '创建日期',
  'pages.create.sys.contact.contactCreateDate.placeholder': '创建日期',


  'pages.update.sys.contact.form.title': '更新',

  'pages.update.sys.contact.contactId.label': '编号',
  'pages.update.sys.contact.contactId.tip': '反馈编号',
  'pages.update.sys.contact.contactId.placeholder': '反馈编号',

  'pages.update.sys.contact.contactAuthor.label': '用户',
  'pages.update.sys.contact.contactAuthor.tip': '反馈用户',
  'pages.update.sys.contact.contactAuthor.placeholder': '反馈用户',

  'pages.update.sys.contact.contactEmail.label': '邮箱',
  'pages.update.sys.contact.contactEmail.tip': '反馈邮箱',
  'pages.update.sys.contact.contactEmail.placeholder': '反馈邮箱',

  'pages.update.sys.contact.contactSubject.label': '主题',
  'pages.update.sys.contact.contactSubject.tip': '反馈主题',
  'pages.update.sys.contact.contactSubject.placeholder': '反馈主题',

  'pages.update.sys.contact.contactText.label': '内容',
  'pages.update.sys.contact.contactText.tip': '反馈内容',
  'pages.update.sys.contact.contactText.placeholder': '反馈内容',

  'pages.update.sys.contact.contactCreateDate.label': '创建日期',
  'pages.update.sys.contact.contactCreateDate.tip': '创建日期',
  'pages.update.sys.contact.contactCreateDate.placeholder': '创建日期',


  'pages.list.sys.contact.form.title': '查询',

  'pages.list.sys.contact.contactId.label': '编号',
  'pages.list.sys.contact.contactId.tip': '反馈编号',
  'pages.list.sys.contact.contactId.placeholder': '反馈编号',

  'pages.list.sys.contact.contactAuthor.label': '用户',
  'pages.list.sys.contact.contactAuthor.tip': '反馈用户',
  'pages.list.sys.contact.contactAuthor.placeholder': '反馈用户',

  'pages.list.sys.contact.contactEmail.label': '邮箱',
  'pages.list.sys.contact.contactEmail.tip': '反馈邮箱',
  'pages.list.sys.contact.contactEmail.placeholder': '反馈邮箱',

  'pages.list.sys.contact.contactSubject.label': '主题',
  'pages.list.sys.contact.contactSubject.tip': '反馈主题',
  'pages.list.sys.contact.contactSubject.placeholder': '反馈主题',

  'pages.list.sys.contact.contactText.label': '内容',
  'pages.list.sys.contact.contactText.tip': '反馈内容',
  'pages.list.sys.contact.contactText.placeholder': '反馈内容',

  'pages.list.sys.contact.contactCreateDate.label': '创建日期',
  'pages.list.sys.contact.contactCreateDate.tip': '创建日期',
  'pages.list.sys.contact.contactCreateDate.placeholder': '创建日期',


  'pages.list.sys.contact.option.delete':'删除',
  'pages.list.sys.contact.batch.delete':'批量删除',
  'pages.list.sys.contact.item':'项',
  'pages.list.sys.contact.chosen':'已选择',
  'pages.list.sys.contact.new':'新建',
  'pages.list.sys.contact.title':'查询',
  'pages.list.sys.contact.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.contact.option.edit':'修改',
  'pages.list.sys.contact.option.title':'操作',
  'pages.list.sys.contact.index.label':'序号',

};
