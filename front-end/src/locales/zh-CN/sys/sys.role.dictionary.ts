export default {
  'pages.sys.role.dictionary.relate.title':'字典授权',
  'pages.sys.role.dictionary.relate.chosen':'已选择',
  'pages.sys.role.dictionary.relate.item':'项',
  'pages.sys.role.dictionary.relate.cancel.all':'取消选择',
  'pages.sys.role.dictionary.relate.cancel':'关闭',
  'pages.sys.role.dictionary.relate.submit':'授权',
  'pages.sys.role.dictionary.relate.loading':'正在授权',
  'pages.sys.role.dictionary.relate.success':'授权成功',
  'pages.sys.role.dictionary.relate.error':'授权失败，请重试',

  'pages.sys.role.dictionary.create.form.title': '创建',

  'pages.create.sys.role.dictionary.rdId.label': '角色字典ID',
  'pages.create.sys.role.dictionary.rdId.tip': '角色字典ID',
  'pages.create.sys.role.dictionary.rdId.placeholder': '角色字典ID',

  'pages.create.sys.role.dictionary.rdRoleId.label': '角色ID',
  'pages.create.sys.role.dictionary.rdRoleId.tip': '角色ID',
  'pages.create.sys.role.dictionary.rdRoleId.placeholder': '角色ID',

  'pages.create.sys.role.dictionary.rdDictionaryId.label': '字典ID',
  'pages.create.sys.role.dictionary.rdDictionaryId.tip': '字典ID',
  'pages.create.sys.role.dictionary.rdDictionaryId.placeholder': '字典ID',


  'pages.sys.role.dictionary.update.form.title': '更新',

  'pages.update.sys.role.dictionary.rdId.label': '角色字典ID',
  'pages.update.sys.role.dictionary.rdId.tip': '角色字典ID',
  'pages.update.sys.role.dictionary.rdId.placeholder': '角色字典ID',

  'pages.update.sys.role.dictionary.rdRoleId.label': '角色ID',
  'pages.update.sys.role.dictionary.rdRoleId.tip': '角色ID',
  'pages.update.sys.role.dictionary.rdRoleId.placeholder': '角色ID',

  'pages.update.sys.role.dictionary.rdDictionaryId.label': '字典ID',
  'pages.update.sys.role.dictionary.rdDictionaryId.tip': '字典ID',
  'pages.update.sys.role.dictionary.rdDictionaryId.placeholder': '字典ID',


  'pages.sys.role.dictionary.list.form.title': '查询',

  'pages.list.sys.role.dictionary.rdId.label': '角色字典ID',
  'pages.list.sys.role.dictionary.rdId.tip': '角色字典ID',
  'pages.list.sys.role.dictionary.rdId.placeholder': '角色字典ID',

  'pages.list.sys.role.dictionary.rdRoleId.label': '角色ID',
  'pages.list.sys.role.dictionary.rdRoleId.tip': '角色ID',
  'pages.list.sys.role.dictionary.rdRoleId.placeholder': '角色ID',

  'pages.list.sys.role.dictionary.rdDictionaryId.label': '字典ID',
  'pages.list.sys.role.dictionary.rdDictionaryId.tip': '字典ID',
  'pages.list.sys.role.dictionary.rdDictionaryId.placeholder': '字典ID',


  'pages.list.sys.role.dictionary.option.delete':'删除',
  'pages.list.sys.role.dictionary.batch.delete':'批量删除',
  'pages.list.sys.role.dictionary.item':'项',
  'pages.list.sys.role.dictionary.chosen':'已选择',
  'pages.list.sys.role.dictionary.new':'新建',
  'pages.list.sys.role.dictionary.title':'查询',
  'pages.list.sys.role.dictionary.option.delete.confirm':'删除不可恢复，你确定要删除吗?',
  'pages.list.sys.role.dictionary.option.edit':'修改',
  'pages.list.sys.role.dictionary.option.title':'操作',
  'pages.list.sys.role.dictionary.index.label':'序号',

};
