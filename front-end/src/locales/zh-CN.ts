import component from './zh-CN/component';
import globalHeader from './zh-CN/globalHeader';
import menu from './zh-CN/menu';
import pwa from './zh-CN/pwa';
import settingDrawer from './zh-CN/settingDrawer';
import settings from './zh-CN/settings';
import pages from './zh-CN/pages';
import file from './zh-CN/common/file';
import system from './zh-CN/common/system';

import SysBillingCard from './zh-CN/sys/sys.billing.card';
import SysCacheTrace from './zh-CN/sys/sys.cache.trace';
import SysCmsFiles from './zh-CN/sys/sys.cms.files';
import SysCmsNotice from './zh-CN/sys/sys.cms.notice';
import SysColDetail from './zh-CN/sys/sys.col.detail';
import SysConfig from './zh-CN/sys/sys.config';
import SysContact from './zh-CN/sys/sys.contact';
import SysDictionary from './zh-CN/sys/sys.dictionary';
import SysFiles from './zh-CN/sys/sys.files';
import SysHelp from './zh-CN/sys/sys.help';
import SysInstrument from './zh-CN/sys/sys.instrument';
import SysLog from './zh-CN/sys/sys.log';
import SysMemory from './zh-CN/sys/sys.memory';
import SysMenu from './zh-CN/sys/sys.menu';
import SysNotice from './zh-CN/sys/sys.notice';
import SysPermission from './zh-CN/sys/sys.permission';
import SysRole from './zh-CN/sys/sys.role';
import SysRoleDictionary from './zh-CN/sys/sys.role.dictionary';
import SysRoleMenu from './zh-CN/sys/sys.role.menu';
import SysRolePermission from './zh-CN/sys/sys.role.permission';
import SysTip from './zh-CN/sys/sys.tip';
import SysUpdate from './zh-CN/sys/sys.update';
import SysUser from './zh-CN/sys/sys.user';
import SysUserRole from './zh-CN/sys/sys.user.role';
import SysUserTopUp from './zh-CN/sys/sys.user.top.up';
import OnpForecast from './zh-CN/onp/onp.forecast';
import OnpStatistics from './zh-CN/onp/onp.statistics';
import OnpVerification from './zh-CN/onp/onp.verification';
import OnpChloroplastHaplotype from "../../../dmt/dist/front-end/locales/zh-CN/onp/onp.chloroplast.haplotype";
import OnpChloroplastOverall from "../../../dmt/dist/front-end/locales/zh-CN/onp/onp.chloroplast.overall";
import OnpNucleusHaplotype from "../../../dmt/dist/front-end/locales/zh-CN/onp/onp.nucleus.haplotype";

export default {
  'navBar.lang': '语言',
  'layout.user.link.help': '帮助',
  'layout.user.link.privacy': '隐私',
  'layout.user.link.terms': '条款',
  'layout.basic.result.subTitle': '对不起，你没有权限访问这个页面',
  'layout.basic.result.redirect.login': '登录跳转',
  'app.preview.down.block': '下载此页面到本地项目',
  'app.welcome.link.fetch-blocks': '获取全部区块',
  'app.welcome.link.block-list': '基于 block 开发，快速构建标准页面',
  'global.header.avatar.user.center': '个人中心',
  'global.header.avatar.user.setting': '个人设置',
  'global.header.avatar.change.password': '修改密码',
  'global.header.avatar.system.info': '系统信息',
  'global.header.avatar.logout': '退出登录',

  ...pages,
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...file,
  ...system,

  ...SysBillingCard,
  ...SysCacheTrace,
  ...SysCmsFiles,
  ...SysCmsNotice,
  ...SysColDetail,
  ...SysConfig,
  ...SysContact,
  ...SysDictionary,
  ...SysFiles,
  ...SysHelp,
  ...SysInstrument,
  ...SysLog,
  ...SysMemory,
  ...SysMenu,
  ...SysNotice,
  ...SysPermission,
  ...SysRole,
  ...SysRoleDictionary,
  ...SysRoleMenu,
  ...SysRolePermission,
  ...SysTip,
  ...SysUpdate,
  ...SysUser,
  ...SysUserRole,
  ...SysUserTopUp,

  ...OnpChloroplastHaplotype,
  ...OnpChloroplastOverall,
  ...OnpForecast,
  ...OnpNucleusHaplotype,
  ...OnpStatistics,
  ...OnpVerification,
};
