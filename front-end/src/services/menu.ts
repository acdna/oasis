import { AuthContext } from '@/common/auth/auth.context';

/**
 * 获取授权访问的菜单信息
 * @author: jiangbin
 * @date: 2020-12-09 13:57:06
 **/
export async function getMenuAuth(){
  return AuthContext.menu;
}
