import request from '@/utils/request';

/**
 * 登录参数类型定义
 * @author: jiangbin
 * @date: 2020-12-09 14:00:48
 **/
export interface LoginParamsType {
  username: string;
  password: string;
  mobile: string;
  captcha: string;
}

/**
 * 登录请求用户信息、令牌、菜单、角色等信息
 * @param params 帐号密码参数
 * @author: jiangbin
 * @date: 2020-12-09 12:49:03
 **/
export async function login(params: LoginParamsType) {
  return request('/auth/login', {
    method: 'POST',
    data: { ...params },
  });
}
