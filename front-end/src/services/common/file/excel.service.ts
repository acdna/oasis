import request from '@/utils/request';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const rest_url = 'excel';

/**
 * 文件服务类
 *
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export const ExcelService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  get rest_url() {
    return rest_url;
  },

  /**
   * 获取Excel模板的相对存储路径功能的URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:33
   **/
  get get_template_path_url() {
    return `${rest_url}/get/template/path`;
  },

  /**
   * 下载指定ID的模板功能的URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:36
   **/
  get by_download_template_url() {
    return `${rest_url}/download/template`;
  },

  /**
   * 获取模板路径
   * @param path 文件的相对存储路径
   * @author: WuHaotian
   * @date: 2021-01-14
   */
  async getTemplatePath(templateId: string) {
    return request(this.get_template_path_url, {
      method: 'GET',
      params: { templateId },
    });
  },

  /**
   * 下载指定ID的模板文件
   *
   * @param templateId 模板ID，定义模板文件信息的常量名，如:sys_sample
   * @return
   * @author jiang
   * @date 2021-01-25 00:50:11
   **/
  async downloadTemplate(templateId: string) {
    return request(this.by_download_template_url, {
      method: 'GET',
      params: { templateId },
      responseType: 'blob',
    });
  },
};
