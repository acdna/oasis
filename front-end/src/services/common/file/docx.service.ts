import request from '@/utils/request';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const rest_url = 'docx';

/**
 * Word文件服务类
 *
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export const DocxService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  get rest_url() {
    return rest_url;
  },

  /**
   * 基于相对路径Word模板合并数据的功能URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:33
   **/
  get by_relative_path_url() {
    return `${rest_url}/create/by/relative/path`;
  },

  /**
   * 基于全路径Word模板合并数据的功能URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:33
   **/
  get by_full_path_url() {
    return `${rest_url}/create/by/full/path`;
  },

  /**
   * 基于ID的Word模板合并数据的功能URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:36
   **/
  get by_template_id_url() {
    return `${rest_url}/create/by/templateId`;
  },

  /**
   * 基于相对路径Word模板合并数据
   * @param relativePath Word模板的相对路径
   * @param data 合并时使用的JSON数据
   * @return
   * @author: WuHaotian
   * @date: 2021-01-14
   */
  async createByRelativePath(relativePath: string, data: any) {
    return request(this.by_relative_path_url, {
      method: 'POST',
      data: {tplPath: relativePath, data},
    });
  },
  /**
   * 基于全路径Word模板合并数据
   * @param fullPath Word模板的全路径
   * @param data 合并时使用的JSON数据
   * @return
   * @author: WuHaotian
   * @date: 2021-01-14
   */
  async createByFullPath(fullPath: string, data: any) {
    return request(this.by_full_path_url, {
      method: 'POST',
      data: {tplPath: fullPath, data},
    });
  },


  /**
   * 基于ID的Word模板合并数据
   * @param templateId Word模板ID
   * @param data Word模板使用的数据
   * @return 生成的Word文件路径信息:
   * @author: jiangbin
   * @date: 2021-02-04 16:45:59
   **/
  async createByTemplateId(templateId: string, data: any) {
    return request(this.by_template_id_url, {
      method: 'POST',
      data: {templateId, data},
    });
  },
};
