import request from '@/utils/request';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const rest_url = 'file';

/**
 * 文件服务类
 *
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export const FileService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  get rest_url() {
    return rest_url;
  },

  /**
   * @description:
   * @author: jiangbin
   * @date: 2021-01-26 14:01:33
   **/
  get by_path_url() {
    return `${rest_url}/download/by/path`;
  },

  /**
   * 下载指定ID的URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:36
   **/
  get by_id_url() {
    return `${rest_url}/download/by/id`;
  },

  /**
   * 访问后端静态资源文件的URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:36
   **/
  static_source_url(relativePath: string) {
    let url = `static/${relativePath}`;
    return url.replaceAll('//', '');
  },

  /**
   * 下载文件数据
   * @param path 文件的相对存储路径
   * @author: WuHaotian
   * @date: 2021-01-14
   */
  async downloadByPath(path: string) {
    return request(this.by_path_url, {
      method: 'GET',
      params: { path },
      responseType: 'blob',
    });
  },

  /**
   * 下载文件数据
   *
   * @param fileId sys_files表的文件记录ID
   * @return
   * @author jiang
   * @date 2021-01-25 00:50:20
   **/
  async downloadById(fileId: string) {
    return request(this.by_id_url, {
      method: 'GET',
      params: { fileId },
      responseType: 'blob',
    });
  },

  /**
   * 下载后端的静态资源文件,实际是使用静态资源下载路径进行下载，实际路径示例如下：
   *
   * http://localhost:3000/static/template/sample.xlsx
   *
   * @param relativePath 后端项目中相对于"back-end/public"的相对文件路径
   * @return
   * @author jiang
   * @date 2021-01-25 00:50:11
   **/
  async downloadStatic(relativePath: string) {
    return request(this.static_source_url(relativePath), {
      method: 'GET',
      responseType: 'blob',
    });
  },
};
