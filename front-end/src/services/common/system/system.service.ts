import request from '@/utils/request';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const rest_url = 'system';

/**
 * System服务类
 *
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export const SystemService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  get rest_url() {
    return rest_url;
  },

  /**
   * 刷新缓存数据的功能URL
   * @author: jiangbin
   * @date: 2021-01-26 14:01:33
   **/
  get refresh_cache_url() {
    return `${rest_url}/refresh/cache`;
  },

  /**
   * 刷新缓存数据
   * @return
   * @author: WuHaotian
   * @date: 2021-01-14
   */
  async refreshCache() {
    return request(this.refresh_cache_url, {
      method: 'GET',
    });
  },
};
