import request from '@/utils/request';
import {OnpForecastItem, OnpForecastParams} from '@/pages/onp/forecast/data';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const rest_url = 'onp/forecast';
/**
 * ONP_FORECAST服务类
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpForecastService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  get rest_url() {
    return rest_url;
  },
  /**
   * 添加新记录
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async add(params: OnpForecastItem | Partial<OnpForecastItem>) {
    return request(`${rest_url}/create`, {
      method: 'POST',
      data: params,
    });
  },
  /**
   * 批量添加新记录
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async addList(params: OnpForecastItem[] | Partial<OnpForecastItem[]>) {
    return request(`${rest_url}/create/list`, {
      method: 'POST',
      data: params,
    });
  },
  /**
   * 根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async update(params: OnpForecastParams | Partial<OnpForecastParams>) {
    return request(`${rest_url}/update/by/id`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 根据ID列表更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async updateByIds(params: OnpForecastParams | Partial<OnpForecastParams>) {
    return request(`${rest_url}/update/by/ids`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 更新ID更新有值字段
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async updateSelective(params: OnpForecastParams | Partial<OnpForecastParams>) {
    return request(`${rest_url}/update/by/id/selective`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 批量根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async updateList(params: OnpForecastParams[] | Partial<OnpForecastParams[]>) {
    return request(`${rest_url}/update/list`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 批量根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async updateListSelective(params: OnpForecastParams[] | Partial<OnpForecastParams[]>) {
    return request(`${rest_url}/update/list/selective`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 删除指定ID的记录
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async deleteById(id: string) {
    return request(`${rest_url}/delete/by/id`, {
      method: 'DELETE',
      params: {id: id},
    });
  },
  /**
   * 删除指定ID列表的记录
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async deleteByIds(ids: string[]) {
    return request(`${rest_url}/delete/by/ids`, {
      method: 'DELETE',
      data: {ids: ids},
    });
  },
  /**
   * 删除指定条件限定的记录列表
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async delete(params ?: OnpForecastParams | Partial<OnpForecastParams>) {
    return request(`${rest_url}/delete`, {
      method: 'DELETE',
      params,
    });
  },
  /**
   * 分页查询用户信息
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async findPage(params ?: OnpForecastParams | Partial<OnpForecastParams>) {
    //计算记录偏移量
    if (params) {
      let pageSize = 20,
        currentPage = 1;
      if (params.pageSize) pageSize = +(params.pageSize) || 20;
      if (params.current) currentPage = +(params.current) || 1;
      params['offset'] = pageSize * (currentPage - 1);
    }
    return request(`${rest_url}/find/page`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 条件查询记录列表
   * @param params
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async findList(params ?: OnpForecastParams | Partial<OnpForecastParams>) {
    return request(`${rest_url}/find/list`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 查找所有记录
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async findAll() {
    return request(`${rest_url}/find/all`, {
      method: 'GET',
    });
  },
  /**
   * 条件查询并返回第一条记录
   * @param params 查询条件
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async findOne(params ?: OnpForecastParams | Partial<OnpForecastParams>) {
    return request(`${rest_url}/find/one`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 查找指定ID记录
   * @param id 记录ID
   * @author: jiangbin
   * @date: 4/6/2021, 10:21:58 AM
   **/
  async findById(id: string) {
    return request(`${rest_url}/find/by/id`, {
      method: 'GET',
      params: {foId: id},
    });
  },

  /**
   * @Description: 导入信息
   * @author zhengenze
   * @date 2021/4/6 3:48 下午
   **/
  async upload(params: Partial<OnpForecastItem>) {
    return request(`${rest_url}/upload`, {
      method: 'POST',
      data: params,
    });
  },

  /**
   * @Description: 比对数据
   * @author zhengenze
   * @date 2021/4/14 10:07 上午
   **/
  async comparison(id: string,filePath:string) {
    console.log('request params:', id)
    return request(`${rest_url}/comparison`, {
      method: 'POST',
      data: {id,filePath},
    });
  }
};
