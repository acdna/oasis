import request from '@/utils/request';
import { SysInstrumentItem, SysInstrumentParams } from '@/pages/sys/instrument/data';
/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const rest_url = 'sys/instrument';
/**
 * SYS_INSTRUMENT服务类
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export const SysInstrumentService = {
    /**
     * 获取Rest风格路径前缀
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    get rest_url() {
        return rest_url;
    },
    /**
     * 添加新记录
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async add(params: SysInstrumentItem | Partial < SysInstrumentItem > ) {
        return request(`${rest_url}/create`, {
            method: 'POST',
            data: params,
        });
    },
    /**
     * 批量添加新记录
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async addList(params: SysInstrumentItem[] | Partial < SysInstrumentItem[] > ) {
        return request(`${rest_url}/create/list`, {
            method: 'POST',
            data: params,
        });
    },
    /**
     * 根据ID更新记录所有字段
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async update(params: SysInstrumentParams | Partial < SysInstrumentParams > ) {
        return request(`${rest_url}/update/by/id`, {
            method: 'PUT',
            data: params,
        });
    },
    /**
     * 根据ID列表更新记录所有字段
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async updateByIds(params: SysInstrumentParams | Partial < SysInstrumentParams > ) {
        return request(`${rest_url}/update/by/ids`, {
            method: 'PUT',
            data: params,
        });
    },
    /**
     * 更新ID更新有值字段
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async updateSelective(params: SysInstrumentParams | Partial < SysInstrumentParams > ) {
        return request(`${rest_url}/update/by/id/selective`, {
            method: 'PUT',
            data: params,
        });
    },
    /**
     * 批量根据ID更新记录所有字段
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async updateList(params: SysInstrumentParams[] | Partial < SysInstrumentParams[] > ) {
        return request(`${rest_url}/update/list`, {
            method: 'PUT',
            data: params,
        });
    },
    /**
     * 批量根据ID更新记录所有字段
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async updateListSelective(params: SysInstrumentParams[] | Partial < SysInstrumentParams[] > ) {
        return request(`${rest_url}/update/list/selective`, {
            method: 'PUT',
            data: params,
        });
    },
    /**
     * 删除指定ID的记录
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async deleteById(id: string) {
        return request(`${rest_url}/delete/by/id`, {
            method: 'DELETE',
            params: { id: id },
        });
    },
    /**
     * 删除指定ID列表的记录
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async deleteByIds(ids: string[]) {
        return request(`${rest_url}/delete/by/ids`, {
            method: 'DELETE',
            params: { ids: ids },
        });
    },
    /**
     * 删除指定条件限定的记录列表
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async delete(params ? : SysInstrumentParams | Partial < SysInstrumentParams > ) {
        return request(`${rest_url}/delete`, {
            method: 'DELETE',
            params,
        });
    },
    /**
     * 分页查询用户信息
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async findPage(params ? : SysInstrumentParams | Partial < SysInstrumentParams > ) {
        //计算记录偏移量
        if (params) {
            let pageSize = 20,
                currentPage = 1;
            if (params.pageSize) pageSize = +(params.pageSize) || 20;
            if (params.current) currentPage = +(params.current) || 1;
            params['offset'] = pageSize * (currentPage - 1);
        }
        return request(`${rest_url}/find/page`, {
            method: 'GET',
            params,
        });
    },
    /**
     * 条件查询记录列表
     * @param params
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async findList(params ? : SysInstrumentParams | Partial < SysInstrumentParams > ) {
        return request(`${rest_url}/find/list`, {
            method: 'GET',
            params,
        });
    },
    /**
     * 查找所有记录
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async findAll() {
        return request(`${rest_url}/find/all`, {
            method: 'GET',
        });
    },
    /**
     * 条件查询并返回第一条记录
     * @param params 查询条件
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async findOne(params ? : SysInstrumentParams | Partial < SysInstrumentParams > ) {
        return request(`${rest_url}/find/one`, {
            method: 'GET',
            params,
        });
    },
    /**
     * 查找指定ID记录
     * @param id 记录ID
     * @author: jiangbin
     * @date: 12/30/2020, 5:14:27 PM
     **/
    async findById(id: string) {
        return request(`${rest_url}/find/by/id`, {
            method: 'GET',
            params: { insId: id },
        });
    },
};