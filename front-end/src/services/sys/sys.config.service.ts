import request from '@/utils/request';
import { SysConfigItem, SysConfigParams } from '@/pages/sys/config/data';
import { getLocale } from 'umi';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
const rest_url = 'sys/config';
/**
 * SYS_CONFIG服务类
 * @author jiangbin
 * @date 12/30/2020, 5:14:27 PM
 **/
export const SysConfigService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  get rest_url() {
    return rest_url;
  },
  /**
   * 添加新记录
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async add(params: SysConfigItem | Partial<SysConfigItem>) {
    return request(`${rest_url}/create`, {
      method: 'POST',
      data: params,
    });
  },
  /**
   * 批量添加新记录
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async addList(params: SysConfigItem[] | Partial<SysConfigItem[]>) {
    return request(`${rest_url}/create/list`, {
      method: 'POST',
      data: params,
    });
  },
  /**
   * 根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async update(params: SysConfigParams | Partial<SysConfigParams>) {
    return request(`${rest_url}/update/by/id`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 根据ID列表更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async updateByIds(params: SysConfigParams | Partial<SysConfigParams>) {
    return request(`${rest_url}/update/by/ids`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 更新ID更新有值字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async updateSelective(params: SysConfigParams | Partial<SysConfigParams>) {
    return request(`${rest_url}/update/by/id/selective`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 批量根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async updateList(params: SysConfigParams[] | Partial<SysConfigParams[]>) {
    return request(`${rest_url}/update/list`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 批量根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async updateListSelective(params: SysConfigParams[] | Partial<SysConfigParams[]>) {
    return request(`${rest_url}/update/list/selective`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 删除指定ID的记录
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async deleteById(id: string) {
    return request(`${rest_url}/delete/by/id`, {
      method: 'DELETE',
      params: { id: id },
    });
  },
  /**
   * 删除指定ID列表的记录
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async deleteByIds(ids: string[]) {
    return request(`${rest_url}/delete/by/ids`, {
      method: 'DELETE',
      params: { ids: ids },
    });
  },
  /**
   * 删除指定条件限定的记录列表
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async delete(params ?: SysConfigParams | Partial<SysConfigParams>) {
    return request(`${rest_url}/delete`, {
      method: 'DELETE',
      params,
    });
  },
  /**
   * 分页查询用户信息
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async findPage(params ?: SysConfigParams | Partial<SysConfigParams>) {
    //计算记录偏移量
    if (params) {
      let pageSize = 20,
        currentPage = 1;
      if (params.pageSize) pageSize = +(params.pageSize) || 20;
      if (params.current) currentPage = +(params.current) || 1;
      params['offset'] = pageSize * (currentPage - 1);
    }
    return request(`${rest_url}/find/page`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 条件查询记录列表
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async findList(params ?: SysConfigParams | Partial<SysConfigParams>) {
    return request(`${rest_url}/find/list`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 查找所有记录
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async findAll() {
    return request(`${rest_url}/find/all`, {
      method: 'GET',
    });
  },
  /**
   * 条件查询并返回第一条记录
   * @param params 查询条件
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async findOne(params ?: SysConfigParams | Partial<SysConfigParams>) {
    return request(`${rest_url}/find/one`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 查找指定ID记录
   * @param id 记录ID
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async findById(id: string) {
    return request(`${rest_url}/find/by/id`, {
      method: 'GET',
      params: { conId: id },
    });
  },

  /**
   * 分页查询用户信息
   * @param params
   * @author: jiangbin
   * @date: 2020/12/26 下午2:47:36
   **/
  async findPageDetail(params ?: SysConfigParams | Partial<SysConfigParams>) {
    //计算记录偏移量
    if (params) {
      let pageSize = 20,
        currentPage = 1;
      if (params.pageSize) pageSize = +(params.pageSize) || 20;
      if (params.current) currentPage = +(params.current) || 1;
      params['offset'] = pageSize * (currentPage - 1);
    }
    return request(`${rest_url}/find/page/detail`, {
      method: 'GET',
      params,
    });
  },

  /**
   * 初始化字典
   * @author: jiangbin
   * @date: 2021-01-07 21:34:42
   **/
  async init() {
    return request(`${rest_url}/init`, {
      method: 'POST',
    });
  },

  /**
   * 获取配置的系统默认语言参数值
   * @author: jiangbin
   * @date: 2021-01-07 21:34:42
   **/
  async defaultLocale() {
    return request(`${rest_url}/default/locale`, {
      method: 'GET',
    });
  },

  /**
   * 获取配置的系统默认语言参数值
   * @author: jiangbin
   * @date: 2021-01-07 21:34:42
   **/
  async defaultCopyright() {
    return request(`${rest_url}/default/copyright/${getLocale()}`, {
      method: 'GET',
    });
  },

  /**
   * 更新当前用户的语言
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:27 PM
   **/
  async updateLocale(locale: string) {
    return request(`${rest_url}/update/locale`, {
      method: 'PUT',
      data: { locale },
    });
  },

  /**
   * 获取系统版本
   * @author: jiangbin
   * @date: 2021-01-26 10:50:22
   **/
  async version() {
    return request(`${rest_url}/version`, {
      method: 'GET',
    });
  },
};
