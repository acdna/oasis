import request from '@/utils/request';
import { SysPermissionItem, SysPermissionParams } from '@/pages/sys/permission/data';
import { PmsParser } from '@/common/pms/pms.parser';

/**
 * rest风格路径前缀
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
const rest_url = 'sys/permission';
/**
 * SYS_PERMISSION服务类
 * @author jiangbin
 * @date 12/30/2020, 5:14:28 PM
 **/
export const SysPermissionService = {
  /**
   * 获取Rest风格路径前缀
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  get rest_url() {
    return rest_url;
  },
  /**
   * 添加新记录
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async add(params: SysPermissionItem | Partial<SysPermissionItem>) {
    return request(`${rest_url}/create`, {
      method: 'POST',
      data: params,
    });
  },
  /**
   * 批量添加新记录
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async addList(params: SysPermissionItem[] | Partial<SysPermissionItem[]>) {
    return request(`${rest_url}/create/list`, {
      method: 'POST',
      data: params,
    });
  },
  /**
   * 根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async update(params: SysPermissionParams | Partial<SysPermissionParams>) {
    return request(`${rest_url}/update/by/id`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 根据ID列表更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async updateByIds(params: SysPermissionParams | Partial<SysPermissionParams>) {
    return request(`${rest_url}/update/by/ids`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 更新ID更新有值字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async updateSelective(params: SysPermissionParams | Partial<SysPermissionParams>) {
    return request(`${rest_url}/update/by/id/selective`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 批量根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async updateList(params: SysPermissionParams[] | Partial<SysPermissionParams[]>) {
    return request(`${rest_url}/update/list`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 批量根据ID更新记录所有字段
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async updateListSelective(params: SysPermissionParams[] | Partial<SysPermissionParams[]>) {
    return request(`${rest_url}/update/list/selective`, {
      method: 'PUT',
      data: params,
    });
  },
  /**
   * 删除指定ID的记录
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async deleteById(id: string) {
    return request(`${rest_url}/delete/by/id`, {
      method: 'DELETE',
      params: { id: id },
    });
  },
  /**
   * 删除指定ID列表的记录
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async deleteByIds(ids: string[]) {
    return request(`${rest_url}/delete/by/ids`, {
      method: 'DELETE',
      data: { ids: ids },
    });
  },
  /**
   * 删除指定条件限定的记录列表
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async delete(params ?: SysPermissionParams | Partial<SysPermissionParams>) {
    return request(`${rest_url}/delete`, {
      method: 'DELETE',
      params,
    });
  },
  /**
   * 分页查询用户信息
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async findPage(params ?: SysPermissionParams | Partial<SysPermissionParams>) {
    //计算记录偏移量
    if (params) {
      let pageSize = 20,
        currentPage = 1;
      if (params.pageSize) pageSize = +(params.pageSize) || 20;
      if (params.current) currentPage = +(params.current) || 1;
      params['offset'] = pageSize * (currentPage - 1);
    }
    return request(`${rest_url}/find/page`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 条件查询记录列表
   * @param params
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async findList(params ?: SysPermissionParams | Partial<SysPermissionParams>) {
    return request(`${rest_url}/find/list`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 查找所有记录
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async findAll() {
    return request(`${rest_url}/find/all`, {
      method: 'GET',
    });
  },
  /**
   * 条件查询并返回第一条记录
   * @param params 查询条件
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async findOne(params ?: SysPermissionParams | Partial<SysPermissionParams>) {
    return request(`${rest_url}/find/one`, {
      method: 'GET',
      params,
    });
  },
  /**
   * 查找指定ID记录
   * @param id 记录ID
   * @author: jiangbin
   * @date: 12/30/2020, 5:14:28 PM
   **/
  async findById(id: string) {
    return request(`${rest_url}/find/by/id`, {
      method: 'GET',
      params: { perId: id },
    });
  },

  /**
   * 分页查询用户信息
   * @param params
   * @author: jiangbin
   * @date: 2020/12/26 下午2:47:36
   **/
  async findPageDetail(params ?: SysPermissionParams | Partial<SysPermissionParams>) {
    //计算记录偏移量
    if (params) {
      let pageSize = 20,
        currentPage = 1;
      if (params.pageSize) pageSize = +(params.pageSize) || 20;
      if (params.current) currentPage = +(params.current) || 1;
      params['offset'] = pageSize * (currentPage - 1);
    }
    return request(`${rest_url}/find/page/detail`, {
      method: 'GET',
      params,
    });
  },

  /**
   * 初始化前端权限记录
   * @author: jiangbin
   * @date: 2020/12/27 下午12:28:20
   **/
  async initFont() {
    //解析前端权限列表
    let records = PmsParser.parserAll();
    //批量插入
    return await request(`${rest_url}/init/front`, {
      method: 'POST',
      data: records,
    });
  },

  /**
   * 初始化后端权限记录
   * @author: jiangbin
   * @date: 2020/12/27 下午12:28:20
   **/
  async initBack() {
    return await request(`${rest_url}/init/back`, {
      method: 'POST',
    });
  },
};
