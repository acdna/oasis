import request from '@/utils/request';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * 查询所有用户信息
 * @author: jiangbin
 * @date: 2020-12-09 12:50:11
 **/
export async function queryAll(): Promise<any> {
  return request('/sys/user/find/all');
}

/**
 * 查询当前用户信息，直接从本地localstorage中取得
 * @author: jiangbin
 * @date: 2020-12-09 12:50:19
 **/
export async function queryCurrent(): Promise<any> {
  return AuthContext.user;
}

/**
 * 查询通知信息列表
 * @author: jiangbin
 * @date: 2020-12-09 12:50:40
 **/
export async function queryNotices(): Promise<any> {
  return request('/api/notices');
}
