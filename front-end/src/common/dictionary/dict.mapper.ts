import {getLocaleMessage} from '@/common/locales/locale';
import {dictValueEnum} from "@/components/common/select.options";
import {SysDictionaryItem} from "@/pages/sys/dictionary/data";

/**
 * 字典映射表，用于快速获取字典值和字典名，字典基于字典分组名进行标记，字典分组名必需唯一不可重复
 * @author: jiangbin
 * @date: 2020-12-24 13:39:12
 **/
export default class DictMapper {
  items: SysDictionaryItem[];
  nameMap: any;
  valueMap: any;
  nameEnMap: any;
  valueEnMap: any;


  constructor(items: SysDictionaryItem[] = []) {
    this.items = items;
    this.nameMap = this.createNameMap(items);
    this.valueMap = this.createValueMap(items);
    this.nameEnMap = this.createNameEnMap(items);
    this.valueEnMap = this.createValueEnMap(items);
  }

  /**
   * 获取key-value映射表
   * @param itemss
   * @return {[]}
   */
  createNameMap = (items: SysDictionaryItem[] = []) => {
    let map = {};
    items.forEach(item => {
      let dict = map[item.dicGroup] || {};
      dict[item.dicName] = item.dicValue;
      map[item.dicGroup] = dict;
    });
    return map;
  };

  /**
   * 获取value-key映射表
   * @param items
   * @return {[]}
   */
  createValueMap = (items: SysDictionaryItem[] = []) => {
    let map = {};
    items.forEach(item => {
      let dict = map[item.dicGroup] || {};
      dict[item.dicValue] = item.dicName;
      map[item.dicGroup] = dict;
    });
    return map;
  };

  /**
   * 获取key-value映射表
   * @param itemss
   * @return {[]}
   */
  createNameEnMap = (items: SysDictionaryItem[] = []) => {
    let map = {};
    items.forEach(item => {
      let dict = map[item.dicGroup] || {};
      dict[item.dicNameEn] = item.dicValue;
      map[item.dicGroup] = dict;
    });
    return map;
  };

  /**
   * 获取value-key映射表
   * @param items
   * @return {[]}
   */
  createValueEnMap = (items: SysDictionaryItem[] = []) => {
    let map = {};
    items.forEach(item => {
      let dict = map[item.dicGroup] || {};
      dict[item.dicValue] = item.dicNameEn;
      map[item.dicGroup] = dict;
    });
    return map;
  };

  /**
   * 获取指定字典分组的字典信息
   * @param dicGroup 字典分组
   * @return {*|*[]}
   */
  getDictItems = (dicGroup: string) => {
    if (!dicGroup) return [];
    let targets: SysDictionaryItem[] = [];
    this.items.forEach(item => {
      if (item.dicGroup === dicGroup) {
        targets.push(item);
      }
    });
    return targets || [];
  };

  /**
   * 构建ProColumns的valueEnum字段值，用于转换字典项值为字典项名，支持中英国际化
   * @param dicGroup
   * @return {{}|*[]}
   */
  valueEnum = (dicGroup: string) => {
    return dictValueEnum(this.getDictItems(dicGroup));
  };

  /**
   * 获取指定字典分组的所有字典名列表
   * @param dicGroup 字典分组
   */
  getDictNames = (dicGroup: string) => {
    let items = this.getDictItems(dicGroup);
    if (items.length == 0) return [];
    return items.map(item => {
      return getLocaleMessage({en: item.dicNameEn, cn: item.dicName});
    });
  };

  /**
   * 获取指定字典分组的所有字典值列表
   * @param dicGroup 字典分组
   */
  getDictValues = (dicGroup: string) => {
    let items = this.getDictItems(dicGroup);
    if (items.length == 0) return [];
    return items.map(item => {
      return item.dicValue;
    });
  };

  /**
   * 获取字典项名
   * @param dicGroup 字典分组
   * @param dicValue 字典项值
   * @return {*|string}
   */
  getItemName = (dicGroup: string, dicValue: string) => {
    if (!dicGroup || !dicValue) return '';
    let map = this.getLocaleValueMap(dicGroup);
    return map[dicValue] || '';
  };

  /**
   * 获取字典项
   *
   * @param dicGroup 字典分组
   * @param dicValue 字典项值
   * @return {*|string}
   */
  getDictItem = (dicGroup: string, dicValue: string) => {
    if (!dicGroup || !dicValue || !this.items || this.items.length === 0) {
      return undefined;
    }
    for (let index = 0; index < this.items.length; index++) {
      let item = this.items[index];
      if (item.dicGroup === dicGroup && item.dicValue === dicValue) {
        return item;
      }
    }
    return undefined;
  };

  /**
   * 获取字典项图标
   *
   * @param dicGroup 字典分组
   * @param dicValue 字典项值
   * @return {*|string}
   * @author jiang
   * @date 2021-03-28 00:22:18
   **/
  getDictItemIcon = (dicGroup: string, dicValue: string) => {
    let item = this.getDictItem(dicGroup, dicValue);
    return (!item || item.dicParams === null || item.dicParams === undefined) ? undefined : item.dicParams;
  };

  /**
   * 获取多个字典项名
   * @param dicGroup 字典分组
   * @param dicValues 字典项值数组
   * @return {*|string}
   */
  getItemNames = (dicGroup: string, dicValues: string[] = []) => {
    if (!dicGroup || !dicValues || dicValues.length == 0) return [];
    let map = this.getLocaleValueMap(dicGroup);
    let names: string[] = [];
    dicValues.forEach(dicValue => {
      if (map[dicValue]) names.push(map[dicValue]);
    });
    return names;
  };

  /**
   * 获取多个字典项名的字符串，以逗号分隔，如:A,B,C
   * @param dicGroup 字典分组
   * @param dicValues 字典项值数组
   * @return {*|string}
   */
  getItemNamesStr = (dicGroup: string, dicValues: string[] = []) => {
    if (!dicGroup || !dicValues || dicValues.length == 0) return '';
    let names = this.getItemNames(dicGroup, dicValues) || {};
    return names.join(',');
  };

  /**
   * 获取字典项值
   * @param dicGroup 字典分组
   * @param dicName 字典项名
   * @return {*|string}
   */
  getItemValue = (dicGroup: string, dicName: string) => {
    if (!dicGroup || !dicName) return '';
    let map = this.getLocaleNameMap(dicGroup);
    return map[dicName] || '';
  };

  /**
   * 获取多个字典项值列表
   * @param dicGroup 字典分组
   * @param dicNames 字典项名数组
   * @return {*|string}
   */
  getItemValues = (dicGroup: string, dicNames: string[] = []) => {
    if (!dicGroup || !dicNames || dicNames.length == 0) return '';
    let map = this.getLocaleNameMap(dicGroup);
    let values: any[] = [];
    dicNames.forEach(dicName => {
      if (map[dicName]) values.push(map[dicName]);
    });
    return values;
  };

  /**
   * 获取获取多个字典项值列表的字符串，以逗号分隔，如:A,B,C
   * @param dicGroup
   * @param dicNames
   * @return {string}
   */
  getItemValuesStr = (dicGroup: string, dicNames: string[] = []) => {
    if (!dicGroup || !dicNames || dicNames.length == 0) return '';
    let values: string[] = this.getItemValues(dicGroup, dicNames) || [];
    return values.join(',');
  };

  /**
   * 根据当前语言类型获取值映射表对象
   * @param dicGroup
   * @return {any}
   */
  getLocaleValueMap(dicGroup: string) {
    return getLocaleMessage({cn: (this.valueMap[dicGroup] || {}), en: (this.valueEnMap[dicGroup] || {})});
  }

  /**
   * 根据当前语言类型获取字典项名映射表对象
   * @param dicGroup
   * @return {any}
   */
  getLocaleNameMap(dicGroup: string) {
    return getLocaleMessage({cn: (this.nameMap[dicGroup] || {}), en: (this.nameEnMap[dicGroup] || {})});
  }

  /**
   * 转换成字符串，用于打印数据
   */
  toString = () => {
    return `items==>${JSON.stringify(this.items)}
    nameMap==>${JSON.stringify(this.nameMap)}
    valueMap==>${JSON.stringify(this.valueMap)}`;
  };
};
