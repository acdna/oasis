/**
 * 定义模糊查询列的属性值
 * @author: jiangbin
 * @date: 2021-03-26 15:11:18
 **/
export const FUZZY_COL_PROPS={
  hideInForm:true,
  hideInSetting:true,
  hideInDescriptions:true,
  hideInTable:true,
};

/**
 * 隐藏字段列
 * @author: jiangbin
 * @date: 2021-03-28 18:47:38
 **/
export const HIDDEN_COL_PROPS={
  hideInForm:true,
  hideInSetting:true,
  hideInDescriptions:true,
  hideInTable:true,
  hideInSearch:true,
};

/**
 * 在查询条件和表格中隐藏字段列，只在显示页面显示出来
 * @author: jiangbin
 * @date: 2021-03-28 18:47:38
 **/
export const VIEW_COL_PROPS={
  hideInForm:true,
  hideInSetting:true,
  hideInTable:true,
  hideInSearch:true,
};
