/**
 * 字符串处理
 * @author: jiangbin
 * @date: 2020-12-28 21:13:40
 **/
export const StringUtils = {
  /**
   * 是空字符串
   *
   * @param str
   * @return
   * @author jiang
   * @date 2021-03-27 13:40:03
   **/
  isBlank(str: string|undefined) {
    return !str || str.length === 0;
  },

  /**
   * 不是空字符串
   *
   * @param str
   * @return
   * @author jiang
   * @date 2021-03-27 13:40:03
   **/
  isNotBlank(str: string|undefined) {
    return !this.isBlank(str);
  },

  /**
   * 数据去重
   * @param array 数组数据
   * @return {T[]}
   * @author: jiangbin
   * @date: 2021-01-03 17:45:45
   **/
  unique<T>(array: T[]): T[] {
    if (!array || array.length == 0) return [];
    let targets = new Array<T>();
    array.forEach(item => {
      if (!targets.includes(item)) {
        targets.push(item);
      }
    });
    return targets;
  },
}
