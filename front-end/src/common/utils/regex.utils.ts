/**
 * 常用正则式
 * @author: jiangbin
 * @date: 2021-03-03 16:35:07
 **/
export const RegexPatterns = {
  /**
   * 邮箱正则式
   * @author: jiangbin
   * @date: 2021-03-03 16:35:22
   **/
  EMAIL: /[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+/,
  /**
   * 条码号正则式
   * @author: jiangbin
   * @date: 2021-04-01 18:20:10
   **/
  BARCODE: /^[0-9A-Z_\\-]+$/g,
}
