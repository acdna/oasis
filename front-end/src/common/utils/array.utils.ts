/**
 * 数组操作工具方法
 * @author: jiangbin
 * @date: 2020-12-08 13:41:22
 **/
export const ArrayUtils = {
  /**
   * 从最前面添加数据到数组，本方法不会修改原数组
   * @param data 待插入数据
   * @param array 目标数组
   * @return 合并后的新数组
   * @author: jiangbin
   * @date: 2020-12-08 13:38:09
   **/
  addFirst<T>(data: T, array: T[]): T[] {
    return [data].concat(array);
  },

  /**
   * 数组是否为空
   *
   * @param items 数组
   * @return true/false--空数组/非空数组
   * @author jiang
   * @date 2021-03-27 13:41:07
   **/
  isEmpty(items: any[] | undefined | null) {
    if (items === null || items === undefined) {
      return true;
    }
    return !items || items.length === 0;
  },

  /**
   * 数组不为空
   *
   * @param items 数组
   * @return false/true--空数组/非空数组
   * @author jiang
   * @date 2021-03-27 15:40:36
   **/
  isNotEmpty(items: any[] | undefined | null) {
    return !this.isEmpty(items);
  },
};
