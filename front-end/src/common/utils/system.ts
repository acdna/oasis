import {FileService} from "@/services/common/file/file.service";
import type PathResult from "@/common/path/path.result";
import {message} from "antd";
import {LocaleContext} from "@/common/locales/locale";

const MD5 = require('md5');

/**
 * 获取服务器URL前缀地址，默认地址为：http://localhost:3000
 * @return {string|string}
 */
export const getServerBaseUrl = () => {
  return SERVER_BASE_URL || `http://localhost:3000`;
};

/**
 * 获取服务器请求全路径URL
 * @param url 请求功能URL，例如：sys/role/findList
 * @return {string}
 */
export const getServerUrl = (url: string) => {
  let baseUrl = getServerBaseUrl();
  return url.startsWith('/') ? (baseUrl + url) : (`${baseUrl}/${url}`);
};

/**
 * 服务器一些通用URL定义，例如文件上传和下载，
 * 若获取全路径则需要调用方法:getServerUrl(url)
 */
export const SERVER_URLS = {
  UPLOAD_FILE: 'file/upload',
  UPLOAD_FILES: 'file/uploads',
  DOWNLOAD_FILE: 'file/download/by/path',
};

/**
 * MD5加密密码
 * @param password 密码
 * @author: jiangbin
 * @date: 2020-12-09 17:55:07
 **/
export const md5 = (password: string) => {
  return MD5(password);
};

/**
 * 下载文件，给定文件名和文件内容
 * @param params
 * @author: jiangbin
 * @date: 2020-12-23 09:21:45
 **/
export const downloadFile = (params: {buf: Buffer,fileName: string}) => {
  const {buf, fileName} = params;
  const url = URL.createObjectURL(new Blob([buf], {type: ''}));
  const a = document.createElement('a');
  a.target = '_self';
  a.href = url;
  a.download = fileName;
  a.click(URL.revokeObjectURL(buf));
};

/**
 * 根据给定的路径信息下载文件
 * @param pathResult
 * @return
 * @author: jiangbin
 * @date: 2021-04-14 10:43:10
 **/
export const downloadByPathResult = async (pathResult: PathResult) => {
  if (pathResult.code != 200) {
    throw new Error('创建文件失败!');
  }
  let fileData: Buffer = await FileService.downloadByPath(pathResult.relativePath);
  if(fileData?.length===0){
    throw new Error('下载文件失败!');
    return;
  }
  downloadFile({buf: fileData, fileName: pathResult.fileName});
};

/**
 * 读取文件内容并编码成BASE64编码
 * @param file
 * @return {Promise<any>}
 */
export const getFileBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
};

/**
 * 打开一个新窗口
 * @param url
 * @author: jiangbin
 * @date: 2021-03-04 14:30:03
 **/
export const openWin = (url: string) => {
  url = encodeURI(url);
  const a = document.createElement('a');
  a.target = '_blank';
  a.href = url;
  a.click(URL.revokeObjectURL(url));
};
