import {LocaleContext} from "@/common/locales/locale";
import {RegexPatterns} from "@/common/utils/regex.utils";

/**
 * 表单规则定义
 * @author: jiangbin
 * @date: 2021-04-01 18:33:03
 **/
export const RuleUtils = {
  /**
   * 条码号验证类
   * @author: jiangbin
   * @date: 2021-04-01 18:31:46
   **/
  BARCODE: {
    pattern: RegexPatterns.BARCODE,
    message: LocaleContext.message({id: 'global.form.input.barcode', defaultMessage: '只允许输入数字、大写字母、下划线和短横线!'})
  }
}
