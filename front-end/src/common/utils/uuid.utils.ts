const uuid_create = require('uuid');

/**
 * 创建UUID，用作主键
 */
export const uuid = () => {
  const id = uuid_create.v1();
  return id.replace(/\-/g, ''); //替换掉UUID中的-
};
