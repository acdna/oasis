const randomcolor = require('randomcolores')

const rgbcolor = randomcolor.RGBColor
const hexcolor = randomcolor.HEXColor

/**
 * 随机颜色生成器
 * @author: jiangbin
 * @date: 2021-03-10 16:01:14
 **/
export const RandomColor = {
  /**
   * 获取一个随机的RGB颜色
   * @param alpha 颜色的透明度，0为不透明度，取值范围是0-1，默认值为0
   * @return
   * @author: jiangbin
   * @date: 2021-03-10 15:59:41
   **/
  rgb: (alpha = 0): string  => {
    return rgbcolor(alpha);
  },
  /**
   * 获取一个随机的RGB颜色
   * @param alpha 颜色的透明度，0为不透明度，取值范围是0-1，默认值为0
   * @return
   * @author: jiangbin
   * @date: 2021-03-10 15:59:41
   **/
  rgbs: (alpha = 0, count = 1): string[] => {
    let colors = [];
    for (let i = 0; i++; i < count) {
      colors.push(rgbcolor(alpha));
    }
    return colors;
  },
  /**
   * 获取一个随机的十六进制颜色
   * @return
   * @author: jiangbin
   * @date: 2021-03-10 16:00:52
   **/
  hex: (): string => {
    return hexcolor();
  },
  /**
   * 获取一个随机的十六进制颜色
   * @return
   * @author: jiangbin
   * @date: 2021-03-10 16:00:52
   **/
  hexs: (count = 1): string[] => {
    let colors = [];
    for (let i = 0; i++; i < count) {
      colors.push(hexcolor());
    }
    return colors;
  },
}
