import * as moment from "moment";

const path = require('path');

/**
 * 文件工具类
 * @Date 2020/12/5 下午6:56:17
 * @author jiangbin
 * @export
 * @class FileUtils
 */
export const FileUtils = {
  /**
   * 获取文件的上级目录路径
   * @param filePath 文件路径
   * @author: jiangbin
   * @date: 2021-01-14 09:59:42
   **/
  getParentPath: (filePath: string) => {
    return path.join(filePath, '..');
  },
  /**
   * 获取文件扩展名
   * @param filePath 文件路径
   * @return {string} 扩展名，例如：xlsx，若无扩展名则返回空字符串
   * @author: jiangbin
   * @date: 2021-01-14 12:53:55
   **/
  getFileExt: (filePath: string) => {
    if (!filePath || filePath.length == 0 || filePath.lastIndexOf('.') == -1) return filePath||'';
    return filePath.substring(filePath.lastIndexOf('.'));
  },
  /**
   * 获取文件路径中包含的文件名，例如：/common/2021/01/17/样品信息表.docx==>样品信息表.docx
   * @author: jiangbin
   * @date: 2021-01-17 16:25:41
   **/
  getFileName: (filePath: string) => {
    if (!filePath || filePath.length == 0) return '';
    filePath = filePath.replaceAll('\\', '/');
    if (filePath.indexOf('/') == -1) return filePath;
    if (filePath.endsWith('/')) {
      filePath = filePath.substring(0, filePath.length - 1);
    }
    return filePath.substring(filePath.lastIndexOf('/') + 1);
  },
  /**
   * 获取文件路径中包含的简单文件名，例如：/common/2021/01/17/样品信息表.docx==>样品信息表
   * @author: jiangbin
   * @date: 2021-01-17 16:25:41
   **/
  getSimpleFileName: (filePath: string) => {
    let fileName = FileUtils.getFileName(filePath);
    if (!fileName || fileName.length == 0 || fileName.lastIndexOf('.') == -1) return fileName;
    return fileName.substring(0, fileName.lastIndexOf('.'));
  },
  /**
   * 在文件名称中添加日期信息，以提供唯一性标识，避免重复文件名时操作系统自动添加序号
   * @param record
   * @return
   * @author: jiangbin
   * @date: 2021-04-14 16:13:16
   **/
  addDate:(record: { path: string, fileName: string, fileExt?: string })=>{
    let { fileName, fileExt} = record;
    let ext = fileExt || FileUtils.getFileExt(record.path);
    ext = ext.startsWith('.')?ext:`.${ext}`;
    fileName = FileUtils.getSimpleFileName(fileName);
    const date = moment(new Date()).format('YYYYMMDDHHmmss');
    return `${fileName}-${date}${ext}`;
  }
};
