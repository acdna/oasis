import {getLocaleMessage} from '@/common/locales/locale';
import moment from "moment";
import {LocalCache} from "@/common/cache/local.cache";

const MD5 = require('md5');
/**
 * 用户认证信息上下文对象
 * @author: jiangbin
 * @date: 2020-12-08 16:41:10
 **/
export const AuthContext = {
  /**
   * 获取用户信息
   * @author: jiangbin
   * @date: 2020-12-08 16:33:28
   **/
  get user() {
    let content = LocalCache.getItem('user');
    if (content === null || content === undefined || content.length === 0) {
      return '';
    }

    //解析
    return JSON.parse(content.toString());
  },

  /**
   * 设置用户信息
   * @param user 用户信息字符串
   * @author: jiangbin
   * @date: 2020-12-08 16:39:59
   **/
  set user(user: string) {
    if (!user || user.length == 0) return;
    LocalCache.setItem('user', user);
  },

  /**
   * 用户登录帐号
   * @author: jiangbin
   * @date: 2020-12-31 20:51:00
   **/
  get loginName() {
    return AuthContext.user['userLoginName'] || '';
  },

  /**
   * 用户姓名
   * @author: jiangbin
   * @date: 2020-12-31 20:51:00
   **/
  get name() {
    return AuthContext.user['userName'] || '';
  },

  /**
   * 用户记录ID
   * @author: jiangbin
   * @date: 2020-12-31 20:51:00
   **/
  get id() {
    return AuthContext.user['userId'] || '';
  },

  /**
   * 用户当前绑定的种属
   * @author: jiangbin
   * @date: 2021-01-09 14:45:22
   **/
  get species() {
    return AuthContext.user['userCurrSpecies'] || '';
  },

  /**
   * 用户缩写代码
   * @author: jiangbin
   * @date: 2021-04-06 17:03:25
   **/
  get acronym() {
    return AuthContext.user['userNameAbbr'] || '';
  },

  /**
   * 设置用户当前种属
   * @author: jiangbin
   * @date: 2021-03-26 17:24:24
   **/
  set species(species: string) {
    let cuser = AuthContext.user;
    cuser['userCurrSpecies'] = species;
    AuthContext.user = JSON.stringify(cuser);
  },

  /**
   * 用户已被授权的种属
   * @author: jiangbin
   * @date: 2021-01-09 14:45:22
   **/
  get authSpecies(): string[] {
    return (AuthContext.user['userSamSpecies'] || '').split(',');
  },

  /**
   * 获取用户的菜单信息
   * @author: jiangbin
   * @date: 2020-12-08 16:40:44
   **/
  get menu(): [] {
    return AuthContext.user['menu'] || '';
  },

  /**
   * 获取用户权限列表
   * @author: jiangbin
   * @date: 2020-12-25 13:47:52
   **/
  get pms(): [] {
    return AuthContext.user['pms'] || '';
  },

  /**
   * 获取用户角色信息
   * @author: jiangbin
   * @date: 2020-12-08 16:40:28
   **/
  get role(): [] {
    return AuthContext.user['role'] || '';
  },

  /**
   * 获取token字符串
   * @author: jiangbin
   * @date: 2020-12-08 16:40:16
   **/
  get token() {
    return AuthContext.user['token'] || '';
  },

  /**
   * 默认版权信息，在后端数据库参数表中配置时会覆盖该参数值
   * @author: jiangbin
   * @date: 2021-01-19 16:13:05
   **/
  get copyright() {
    return getLocaleMessage({
      en: `Maize Research Center, Beijing Academy of Agriculture and Forestry Sciences, China @2005-${moment(new Date()).format('YYYY')}`,
      cn: `北京市农林科学院玉米研究中心 @2005-${moment(new Date()).format('YYYY')}`,
    });
  },

  /**
   * 清空Session信息，如用户信息
   * @author: jiangbin
   * @date: 2020-12-08 16:48:49
   **/
  logout: () => {
    LocalCache.clearAll();
  },

  /**
   * 是否已经登录
   * @author: jiangbin
   * @date: 2020-12-09 17:54:29
   **/
  get isLogin() {
    let val = AuthContext.token;
    return val && val.length > 0;
  },

  /**
   * 获取头部信息，包含了token令牌参数，用于JWT身份认证服务
   * @author: jiangbin
   * @date: 2020-12-08 16:45:07
   **/
  get headers() {
    return {'token': AuthContext.token};
  },

  /**
   * MD5加密密码
   * @param password 密码
   * @author: jiangbin
   * @date: 2020-12-09 17:55:07
   **/
  md5: (password: string) => {
    return MD5(password);
  },

  /**
   * 获取用户的首页URL
   * @author: jiangbin
   * @date: 2021-02-02 13:49:01
   **/
  get indexUrl() {
    return AuthContext.user['index'] || '/';
  },

};
