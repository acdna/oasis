/**
 * 常量定义
 * @author: jiangbin
 * @date: 2021-04-14 15:05:23
 **/
export const CONSTATNS = {
  /**
   * 空白孔位占位DNA条码号
   * @author: jiangbin
   * @date: 2021-04-18 18:15:22
   **/
  NTC_WELL: 'NTC',
};

/**
 * 参数名定义
 * @author: jiangbin
 * @date: 2021-04-14 15:05:57
 **/
export const PARAM_NAMES = {
  LANGUAGE: 'language',
};
