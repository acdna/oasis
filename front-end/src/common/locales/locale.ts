import { getLocale } from 'umi';
import { SysConfigService } from '@/services/sys/sys.config.service';
import zh_cn from '@/locales/zh-CN';
import en_us from '@/locales/en-US';

/**
 * 系统语言定义
 * @author: jiangbin
 * @date: 2021-01-18 09:42:50
 * */
export const LOCALES = {
  zh_cn: 'zh-CN',
  en_us: 'en-US',
};

/**
 * 语言上下文对象，可以获取系统参数，提供判定语言类型的方法
 * @author: jiangbin
 * @date: 2021-01-20 14:02:42
 * */
export const LocaleContext = {
  /**
   * 获取用户语言:'zh-CN', 'en-US'
   * @author: jiangbin
   * @date: 2021-01-17 20:25:29
   * */
  get locale() {
    return getLocale();
  },

  /**
   * 设置用户语言:'zh-CN', 'en-US',会更新到后端数据库中进行记录
   * @author: jiangbin
   * @date: 2021-01-17 20:25:32
   * */
  setLocale(locale?: string) {
    (async ()=>{
     await SysConfigService.updateLocale(locale || LocaleContext.locale);
    })();
  },

  /**
   * 用户当前是否为英文语言
   * @author: jiangbin
   * @date: 2021-01-17 20:27:09
   * */
  get isEnUs() {
    return LocaleContext.locale === LOCALES.en_us;
  },

  /**
   * 用户当前是否为中文语言
   * @author: jiangbin
   * @date: 2021-01-17 20:27:09
   * */
  get isZhCn() {
    return LocaleContext.locale === LOCALES.zh_cn;
  },

  /**
   * 后端配置的默认系统语言
   * @author: jiangbin
   * @date: 2021-01-20 17:03:43
   * */
  defaultLocale: LOCALES.zh_cn,

  getLocaleMessage : (params: { en: any, cn: any }): any => {
    return LocaleContext.isEnUs ? (params.en || '') : (params.cn || '');
  },

  /**
   * 国际化消息，从antd的locales目录中加载消息
   * @author: jiangbin
   * @date: 2021-01-25 18:26:36
   * */
  message: (params: { id: any, defaultMessage: any }): any => {
    return LocaleContext.getLocaleMessage({
      en: (en_us[params.id] || params.defaultMessage),
      cn: (zh_cn[params.id] || params.defaultMessage),
    });
  },
};

/**
 * 获取语言环境下的消息，用于国际化，会根据用户当前的中英文语言选择消息
 * @param params.en 英文消息
 * @param params.cn 中文消息
 * @return {any}
 * @author: jiangbin
 * @date: 2021-01-19 10:48:40
 * */
export const {getLocaleMessage} = LocaleContext;

/**
 * moment日期对象语言定义
 * @author: jiangbin
 * @date: 2021-01-18 09:28:02
 * */
export const MLOCALES = {
  zh_cn: 'zh-cn',
  en: 'en',
  /**
   * 获取moment日期格式化pattern
   * @author: jiangbin
   * @date: 2021-01-18 09:42:07
   * */
  get pattern() {
    return getLocaleMessage({ en: 'YYYY-MM-DD', cn: 'YYYY年MM月DD日' });
  },
};
