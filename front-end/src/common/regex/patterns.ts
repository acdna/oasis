import { WellUtils } from '../plate/well.utils';

/**
 * 正则式匹配模式接口定义
 * @author: jiangbin
 * @date: 2021-04-17 15:08:30
 **/
export interface Pattern {
  /**
   * @description:
   * @author: jiangbin
   * @date: 2021-04-17 15:08:43
   **/
  pattern: RegExp;
  /**
   * 解析字符串中匹配的内容
   * @author: jiangbin
   * @date: 2021-04-17 15:08:46
   **/
  match: (source: string) => any;
}

/**
 * 系统常用的一些正则式匹配模式
 * @author: jiangbin
 * @date: 2021-04-17 15:01:49
 **/
export const PATTERNS = {
  /**
   * 孔位号正则式
   * @author: jiangbin
   * @date: 2021-03-18 14:07:53
   **/
  well: {
    /**
     * 匹配孔位号，用于解析出其中的行号和列号部分，例如:A01 => (A,01)
     * @author: jiangbin
     * @date: 2021-04-17 15:15:26
     **/
    pattern: /^([a-zA-Z]+)([0-9]+)$/,
    /**
     * 解析孔位字符串的行列号信息
     * @param well 孔位号字符串，如:A1或A01
     * @return
     * @author: jiangbin
     * @date: 2021-04-17 15:11:03
     **/
    match(source: string) {
      let match = source.match(this.pattern);
      if (match) {
        let rowName = match[1];
        let row = WellUtils.getRowNum(rowName);
        return { rowName, row, col: parseInt(match[2], 10), well: source };
      }
      return null;
    },
  } as Pattern,
};
