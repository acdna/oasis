/**
 * 创建树形结构，用于将查询出来的具有上下级节点关系的记录转换成树形嵌套结构,
 * 这个功能要求以树形结构组织的树必需以ID=1的节点作为唯一的根节点，否则无法转换成树形结构
 * @author: jiangbin
 * @date: 2020-12-07 13:04:37
 **/
import { ROOT_NODE_ID } from '@/common/tree/tree.node.type';

export class TreeCreator {

  /**
   * 将根据节点列表构建成树形结构
   * @author: jiangbin
   * @date: 2020-12-07 13:03:37
   **/
  tree(nodes: any[]): any[] {
    if (!nodes || nodes.length == 0) return [];
    //查找根结点列表
    let roots = this.roots(nodes);
    if (!roots || roots.length == 0) return [];
    roots.forEach(root => {
      this.children(root, nodes);
    });
    return roots;
  }

  /**
   * 查找子节点列表
   * @param parentId 父结点
   * @param nodes 全部节点列表
   * @author: jiangbin
   * @date: 2020-12-07 12:19:56
   **/
  children(parent: any, nodes: any[]): any[] {
    if (!parent) return [];
    let childrenNodes = new Array();
    nodes.forEach(node => {
      if (node.parentId === parent.id) {
        childrenNodes.push(node);
        node.children = this.children(node, nodes);
      }
    });
    if (childrenNodes.length > 0) {
      parent.children = childrenNodes;
    }
    return this.sort(childrenNodes);
  }

  /**
   * 查找根结点列表
   * @author: jiangbin
   * @date: 2020-12-07 12:59:14
   **/
  roots(nodes: any[]): any[] {
    let roots = new Array();
    nodes.forEach(node => {
      if (node.parentId === ROOT_NODE_ID) {
        roots.push(node);
      }
    });
    return this.sort(roots);
  }

  /**
   * 树形节点升序排序
   * @author: jiangbin
   * @date: 2020-12-07 13:22:22
   **/
  sort(nodes: any[]): any[] {
    return nodes.sort(function(source, target) {
      if (source.order > target.order) {
        return 1;
      } else if (source.order == target.order) {
        return 0;
      } else {
        return -1;
      }
    });
  }
}
