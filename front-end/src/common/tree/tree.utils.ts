import { TreeNode } from '@/common/tree/tree.node';
import {StringUtils} from "@/common/utils/string.utils";

/**
 * 获取父结点列表，因为获取Tree组件的选中组件的ID列表中并不包含父级结点，
 * 所以必需额外从树列表中过滤出来才保证后续生成用户菜单时是正确的
 * @param nodes 树结构包含的所有节点列表
 * @param ids 待筛选父结点ID的节点ID列表
 * @return [string[]] 筛选出来的父结点ID列表，会去掉id=1的根节点
 * @author: jiangbin
 * @date: 2021-01-03 16:54:03
 **/
export const getPrentIds = (nodes: Partial<TreeNode>[] | TreeNode[], ids: string[]) => {
  let parentIds: string[] = [];
  for (let i = 0; i < ids.length; i++) {
    //过滤直接父级节点ID列表
    let pIds: string[] = [];
    for (let j = 0; j < nodes.length; j++) {
      let node = nodes[j];
      if (node.id === ids[i] && node.parentId) {
        pIds.push(node.parentId);
      }
    }

    //无父级节点
    if (!pIds || pIds.length == 0) {
      return [];
    }

    //合并父节点ID列表
    parentIds = parentIds.concat(pIds);

    //递归查找父节点ID列表
    let spIds = getPrentIds(nodes, pIds);
    if (spIds && spIds.length > 0) {
      parentIds = parentIds.concat(spIds);
    }
  }

  //过滤掉根结点
  parentIds = parentIds.filter(parentId => parentId != '1');

  //数据去重
  return StringUtils.unique(parentIds);
};

/**
 * 获取子节点的ID列表
 * @param nodes 所有节点信息列表
 * @param ids 节点ID列表
 * @return {string[]} 子节点ID列表
 * @author: jiangbin
 * @date: 2021-01-05 10:44:21
 **/
export const getChildrenIds = (nodes: Partial<any>[] | any[], ids: string[]): string[] => {
  if (!ids || ids.length == 0 || !nodes || nodes.length == 0) {
    return [];
  }
  let childrenIds: string[] = [];
  //遍历节点
  for (let i = 0; i < ids.length; i++) {
    let cIds: string[] = [];

    //匹配节点并过滤子节点ID列表
    for (let j = 0; j < nodes.length; j++) {
      let node = nodes[j];
      let {children} = node;

      if (node.id != ids[i] || !children || children.length == 0) {
        continue;
      }

      //查找直接子节点ID列表
      let rsubIds = children.map((value: any) => `${value.id  }`);
      if (!rsubIds || rsubIds.length == 0) {
        continue;
      }
      cIds = cIds.concat(rsubIds);

      //递归查找非直接子节点ID列表
      let nrsubIds = getChildrenIds(nodes, rsubIds);
      if (nrsubIds && nrsubIds.length > 0) {
        cIds = cIds.concat(nrsubIds);
      }
    }

    //合并子节点ID列表
    if (cIds && cIds.length > 0) {
      childrenIds = childrenIds.concat(cIds);
    }
  }

  return childrenIds;
};
