
/**
 * 树节点实现类
 * @author jiang
 * @date 2020-12-26 22:42:19
 **/
export interface TreeNode {

  /**
   * 节点ID
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  id?: string;

  /**
   * 节点序号
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  order?: number;

  /**
   * 父节点ID
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  parentId?: string;

  /**
   * 子节点列表
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  children ?: TreeNode[];

  ///////////用于树形结构显示////////////
  /**
   * 节点标题
   * @param title 节点标题
   * @author: jiangbin
   * @date: 2021-01-02 19:04:21
   **/
  title?: string;

  /**
   * 节点Key值
   * @param key 节点Key值
   * @author: jiangbin
   * @date: 2021-01-02 19:04:21
   **/
  key?: string;
}
