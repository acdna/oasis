/**
 * 参数类型定义
 *
 * @author jiang
 * @date 2021-01-06 22:21:45
 **/
export enum CON_TYPE {
  /**
   * 系统参数
   *
   * @author jiang
   * @date 2021-01-06 22:22:01
   **/
  SYSTEM = 'SYSTEM',
  /**
   * 用户参数
   *
   * @author jiang
   * @date 2021-01-06 22:22:01
   **/
  USER = 'USER',
  /**
   * 种属参数
   *
   * @author jiang
   * @date 2021-01-06 22:22:01
   **/
  SPECIES = 'SPECIES',
}

/**
 * 参数分组信息
 * @author jiang
 * @date 2021-01-06 22:35:45
 **/
export interface ConfigGroup {
  nameCn: string;
  nameEn: string;
  group: string;
  type: CON_TYPE;
}

/**
 * 参数项信息
 * @author jiang
 * @date 2021-01-06 22:35:57
 **/
export interface ConfigItem {
  name: string;
  value?: string;
  remark?: string;
  species?: string;
  group: ConfigGroup;
}
