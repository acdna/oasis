import {CON_TYPE, ConfigItem} from './sys.config.info';
import {SysConfigItem} from "@/pages/sys/config/data";
import {AuthContext} from "@/common/auth/auth.context";

/**
 * 参数容器对象
 *
 * @author jiang
 * @date 2021-01-06 20:15:40
 **/
export class ConfigMapper {
  configMap: Map<CON_TYPE, SysConfigItem[]>;

  constructor(items: SysConfigItem[]=[]) {
    this.configMap = new Map<CON_TYPE, SysConfigItem[]>();
    this.addList(items);
  }

  /**
   * 获取指定参数的参数记录
   * @param conItem 参数对象
   * @return SysConfig
   * @author: jiangbin
   * @date: 2021-01-07 12:00:54
   **/
  getConfig(conItem: ConfigItem | Partial<ConfigItem>): SysConfigItem | undefined {
    if (!conItem || !conItem.group || !conItem.group.group || !this.configMap || this.configMap.size == 0) {
      return undefined;
    }
    let configs = this.configMap.get(conItem.group.type);
    if (!configs || configs.length == 0) {
      return undefined;
    }
    configs = configs.filter((config) => {
      if (!conItem.group) return false;
      return config.conGroup === conItem.group.group && config.conName === conItem.name;
    });
    if (!configs || configs.length == 0) {
      return undefined;
    }
    if (conItem.group.type == CON_TYPE.SPECIES) {
      //过滤出种属匹配的参数列表
      configs = configs.filter((config) => {
        return !config.conSpecies || config.conSpecies === AuthContext.species;
      });
    } else if (conItem.group.type == CON_TYPE.USER) {
      //过滤出种属匹配的参数列表
      configs = configs.filter((config) => {
        return !config.conManager || config.conManager === AuthContext.loginName;
      });
    }
    return configs && configs.length > 0 ? configs[0] : undefined;
  }

  /**
   * 获取参数值
   *
   * @param conGroup 参数分组
   * @param conName 参数名
   * @param conSpecies 种属
   * @return string 字符串参数值，若不存在参数记录则返回参数定义的默认值
   * @author jiang
   * @date 2021-01-06 20:03:35
   **/
  getValue(conItem: ConfigItem | Partial<ConfigItem>): string | undefined {
    //获取当前分组参数列表
    const config = this.getConfig(conItem);
    //判定参数
    return config?.conValue || conItem.value;
  }

  /**
   * 重新加载全部参数
   * @author: jiangbin
   * @date: 2021-01-07 08:46:52
   **/
  addList(configs: SysConfigItem[]) {
    this.configMap = new Map<CON_TYPE, SysConfigItem[]>();
    if (!configs || configs.length == 0) return;
    this.configMap.set(
      CON_TYPE.SYSTEM,
      configs.filter((config: SysConfigItem) => config.conParamType == CON_TYPE.SYSTEM),
    );
    this.configMap.set(
      CON_TYPE.USER,
      configs.filter((config: SysConfigItem) => config.conParamType == CON_TYPE.USER),
    );
    this.configMap.set(
      CON_TYPE.SPECIES,
      configs.filter((config: SysConfigItem) => config.conParamType == CON_TYPE.SPECIES),
    );
  }
}
