import {CON_TYPE, ConfigGroup} from './sys.config.info';
import {LOCALES} from '@/common/locales/locale';
import {SYS_SAM_SPECIES_DICT} from "@/common/dictionary/sys.dict.defined";

export const SYS_SYSTEM_GROUP: ConfigGroup = {
  nameCn: '系统参数',
  nameEn: 'System Params',
  group: 'SYS_SYSTEM',
  type: CON_TYPE.SYSTEM,
};
export const SYS_SYSTEM = {
  SYS_SAM_SPECIES: {name: 'SYS_SAM_SPECIES', value: 'M', remark: '系统默认种属', group: SYS_SYSTEM_GROUP},
  SYS_NAME_ACRONYM: { name: 'SYS_NAME_ACRONYM', value: 'SCDMS', remark: '系统名称缩写', group: SYS_SYSTEM_GROUP },
  SYS_NAME_CN: { name: 'SYS_NAME_CN', value: 'SNP指纹数据库管理系统', remark: '中文系统名称', group: SYS_SYSTEM_GROUP },
  SYS_NAME_EN: { name: 'SYS_NAME_EN', value: 'SNP Fingerprint Database Management System', remark: '英文系统名称', group: SYS_SYSTEM_GROUP },
  SYS_COPYRIGHT_CN: {
    name: 'SYS_COPYRIGHT_CN',
    value: '北京市农林科学院玉米研究中心版权所有 ©2005-',
    remark: '版权信息',
    group: SYS_SYSTEM_GROUP,
  },
  SYS_COPYRIGHT_EN: {
    name: 'SYS_COPYRIGHT_EN',
    value: 'Maize Research Center, Beijing Academy of Agriculture and Forestry Sciences ©2005-',
    remark: '版权信息',
    group: SYS_SYSTEM_GROUP,
  },
  SYS_DEPT_CODE: {name: 'SYS_DEPT_CODE', value: 'BJY', remark: '单位代码', group: SYS_SYSTEM_GROUP},
  SYS_VERSION: {name: 'SYS_VERSION', value: 'v1.0.0', remark: '系统版本', group: SYS_SYSTEM_GROUP},
  SYS_PAGE_SIZE: {name: 'SYS_PAGE_SIZE', value: '20', remark: '分页数', group: SYS_SYSTEM_GROUP},
  SYS_LANGUAGE: {name: 'SYS_LANGUAGE', value: 'zh-CN', remark: '默认语言', group: SYS_SYSTEM_GROUP},
};

export const SYS_PAGE_GROUP: ConfigGroup = {
  nameCn: '页面控制参数',
  nameEn: 'Page Control Params ',
  group: 'SYS_PAGE',
  type: CON_TYPE.SYSTEM,
};
export const SYS_PAGE = {
  LOCK_SPECIES: {name: 'LOCK_SPECIES', value: 'false', remark: '锁定种属', group: SYS_PAGE_GROUP},
  NAVIGATE_LAYOUT: {name: 'NAVIGATE_LAYOUT', value: 'side', remark: '导航布局:side|top|mix', group: SYS_PAGE_GROUP},
};

export const SYS_USER_GROUP: ConfigGroup = {
  nameCn: '用户参数',
  nameEn: 'User Params',
  group: 'SYS_USER',
  type: CON_TYPE.USER,
};
export const SYS_USER = {
  USER_SAM_SPECIES: {
    name: 'USER_SAM_SPECIES',
    value: SYS_SAM_SPECIES_DICT.M.value,
    remark: '用户当前种属',
    group: SYS_USER_GROUP,
  },
  USER_LANGUAGE: {name: 'USER_LANGUAGE', value: LOCALES.zh_cn, remark: '用户当前语言', group: SYS_USER_GROUP},
  USER_INDEX_URL: {name: 'USER_INDEX_URL', value: '/', remark: '用户登录首页面', group: SYS_USER_GROUP},
};

export const SYS_PROBATION_GROUP: ConfigGroup = {
  nameCn: '试用模式参数',
  nameEn: 'Probation Params',
  group: 'SYS_PROBATION',
  type: CON_TYPE.SYSTEM,
};
export const SYS_PROBATION = {
  PROBATION_MODE: {name: 'PROBATION_MODE', value: 'false', remark: '试用模式', group: SYS_PROBATION_GROUP},
  PROBATION_START_DATE: {name: 'PROBATION_START_DATE', value: '', remark: '开始时间', group: SYS_PROBATION_GROUP},
  PROBATION_END_DATE: {name: 'PROBATION_END_DATE', value: '', remark: '结束时间', group: SYS_PROBATION_GROUP},
};

export const SYS_FILE_PATH_GROUP: ConfigGroup = {
  nameCn: '文件目录参数',
  nameEn: 'File Path Params',
  group: 'SYS_FILE_PATH',
  type: CON_TYPE.SYSTEM,
};
export const SYS_FILE_PATH = {
  MAIN_FOLDER: {name: 'MAIN_FOLDER', value: '/work/scdms/uploads/', remark: '文件主目录', group: SYS_FILE_PATH_GROUP},
  SUB_COMMON: {name: 'SUB_COMMON', value: 'common', remark: '默认二级目录', group: SYS_FILE_PATH_GROUP},
  SUB_TEMP: {name: 'SUB_TEMP', value: 'temp', remark: '临时文件目录', group: SYS_FILE_PATH_GROUP},
  SUB_GENES: {name: 'SUB_GENES', value: 'genes', remark: '指纹文件二级目录', group: SYS_FILE_PATH_GROUP},
  SUB_WORDS: {name: 'SUB_WORDS', value: 'words', remark: 'Word文件二级目录', group: SYS_FILE_PATH_GROUP},
  SUB_EXCELS: {name: 'SUB_EXCELS', value: 'excels', remark: 'Excel文件二级目录', group: SYS_FILE_PATH_GROUP},
  SUB_WORD_TPLS: {name: 'SUB_WORD_TPLS', value: 'words-tpls', remark: 'Word模板文件二级目录', group: SYS_FILE_PATH_GROUP},
  SUB_IMAGES: {
    name: 'SUB_IMAGES',
    value: 'images',
    remark: '图片文件二级目录',
    group: SYS_FILE_PATH_GROUP,
  },
};
