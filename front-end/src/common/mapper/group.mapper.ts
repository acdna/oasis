/**
 * 分组映射表组件
 * @author: jiangbin
 * @date: 2021-02-19 17:28:03
 **/
export class GroupMapper<K, V> extends Map<K, V[]> {
  /**
   * 添加分组数据，若key存在则合并数据，否则添加新键值对
   * @param key
   * @param value
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:27:21
   **/
  add(key: K, value: V[]): this {
    if (!key || !value || value.length == 0) {
      return this;
    }
    let exists = this.get(key);
    if (exists && exists.length > 0) {
      exists = exists.concat(value);
    } else {
      exists = [];
    }
    this.set(key, exists);
    return this;
  }

  /**
   * 添加记录列表
   * @param records
   * @param getKey
   * @return
   * @author: jiangbin
   * @date: 2021-03-24 15:03:24
   **/
  addList(records: V[], getKey: (record: V) => K) {
    records?.forEach((item) => {
      let key = getKey(item);
      if (!key) return;
      let exists = this.get(key);
      if (!exists) {
        exists = new Array<V>();
      }
      exists.push(item);
      this.set(key, exists);
    });
  }

  /**
   * 获取所有值列表
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:38:45
   **/
  getAllValues(): V[] {
    let values = new Array();
    if (this.size === 0) {
      return [];
    }
    this.forEach((value) => {
      if (value?.length > 0) {
        values.push(value);
      }
    });
    return values;
  }

  /**
   * 获取所有值列表
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:38:45
   **/
  getAllValueList(): V[] {
    let values = new Array<V>();
    if (this.size == 0) {
      return values;
    }
    this.forEach((value) => {
      if (value?.length > 0) {
        values = values.concat(value);
      }
    });
    return values;
  }

  /**
   * 获取值列表
   * @param keys
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:44:29
   **/
  getValues(keys: K[]) {
    if (this.size === 0 || !keys || keys.length == 0) {
      return undefined;
    }
    let values = new Array();
    keys.forEach((key) => {
      let items = this.get(key);
      if (items) {
        values.push(items);
      }
    });
    return values;
  }

  /**
   * 获取值列表并合并元素成数组
   * @param keys
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:44:29
   **/
  getValueLists(keys: K[]): V[] {
    if (this.size === 0 || !keys || keys.length == 0) {
      return [];
    }
    let values = new Array<V>();
    keys.forEach((key) => {
      let items = this.get(key);
      if (items) {
        values = values.concat(items);
      }
    });
    return values;
  }

  /**
   * 删除数据列表
   * @param keys
   * @author: jiangbin
   * @date: 2021-02-20 15:37:31
   **/
  deleteList(keys: K[]) {
    if (this.size === 0 || !keys || keys.length == 0) {
      return;
    }
    keys.forEach((key) => {
      this.delete(key);
    });
  }
}
