/**
 * 分组映射表组件
 * @author: jiangbin
 * @date: 2021-02-19 17:28:03
 **/
export class Mapper<K, V> extends Map<K, V> {
  /**
   * 获取所有值列表
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:38:45
   **/
  getAllValues(): V[] {
    let values = new Array<V>();
    if (this.size === 0) {
      return [];
    }
    this.forEach((value) => {
      if (value) {
        values.push(value);
      }
    });
    return values;
  }

  /**
   * 获取值列表
   * @param keys
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 17:44:29
   **/
  getValues(keys: K[]): V[] {
    if (this.size === 0 || !keys || keys.length == 0 || !Array.isArray(keys)) {
      return [];
    }
    let values = new Array<V>();
    keys.forEach((key) => {
      let items = this.get(key);
      if (items) {
        values.push(items);
      }
    });
    return values;
  }

  /**
   * 删除数据列表
   * @param keys
   * @author: jiangbin
   * @date: 2021-02-20 15:37:31
   **/
  deleteList(keys: K[]) {
    if (this.size === 0 || !keys || keys.length == 0 || !Array.isArray(keys)) {
      return;
    }
    keys?.forEach((key) => {
      this.delete(key);
    });
  }

  /**
   * 添加记录列表
   * @param records
   * @param getKey
   * @return
   * @author: jiangbin
   * @date: 2021-03-24 15:03:24
   **/
  addList(records: V[], getKey: (record: V) => K) {
    if (Array.isArray(records)) {
      records?.forEach((item) => {
        this.set(getKey(item), item);
      });
    }
  }

  /**
   * 获取主键列表
   * @author: jiangbin
   * @date: 2021-03-24 15:12:16
   **/
  getKeys() {
    let keys = new Array<K>();
    this.forEach((value, key) => keys.push(key));
    return keys;
  }
}
