/**
 * 文件路径构建结果
 * @author jiang
 * @date 2020-12-06 21:53:01
 **/
export default interface PathResult {
  /**
   * 请求后端后的响应代码，成功响应则值为200
   * @author: jiangbin
   * @date: 2021-04-14 10:26:06
   **/
  code: number;
  /**
   * 导出文件的文件名称
   * @author: jiangbin
   * @date: 2021-04-14 10:38:38
   **/
  fileName: string;
  /**
   * 完整的文件路径
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  path?: string;
  /**
   * 存储文件的主目录
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  mainFolder?: string;
  /**
   * 文件相对路径
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  relativePath: string;
  /**
   * 文件扩展名
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  fileExt?: string;
  /**
   * 是文件还是目录，true/false--文件/目录
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  isFile: boolean;
  /**
   * 文件大小
   * @author: jiangbin
   * @date: 2021-01-29 10:49:37
   **/
  size?: number;
  /**
   * 原文件名称
   * @author: jiangbin
   * @date: 2021-01-29 10:49:40
   **/
  originalName?: string;
  /**
   * 文件绑定的前端字段名
   * @author: jiangbin
   * @date: 2021-01-29 10:49:43
   **/
  fieldName?: string;
}
