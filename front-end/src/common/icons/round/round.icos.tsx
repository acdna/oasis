import {getIconFont} from "@/common/icons/utils/icon.font.utils";

require('@/common/icons/round/iconfont');

/**
 * @Description: 位点图标CSS名称定义（圆）
 * @author zhengenze
 * @date 2021/4/21 10:51 上午
 **/
export const RoundIconNames = {
  ICON_YUANDIANXIAO: "#iconyuandianxiao",
  ICON_YUANDIANZHONG: "#iconyuandianzhong",
  ICON_YUANHUAN: "#iconyuanhuan",
  ICON_YUANJIAO_RECT: "#iconyuanjiao-rect",
  ICON_YUANDIAN: "#iconyuandian",
  ICON_YUANDIAN1: "#iconyuandian1",
  ICON_YUAN: "#iconyuan",
  ICON_YUANJIAOJUXING: "#iconyuanjiaojuxing",
  ICON_YUAN1: "#iconyuan1",
  ICON_RADIO_OFF_FULL: "#iconradio-off-full",
  ICON_DOUBLE_CIRCLE_FULL: "#icondouble-circle-full",
  ICON_CIRCLE: "#iconcircle",
  ICON_ROUND: "#iconround",
};

const IconOptions = {width: '2em', height: '2em'};

/**
 * @Description: 圆
 * @author zhengenze
 * @date 2021/4/21 10:52 上午
 **/
export const RoundIcons = {
  ICON_YUANDIANXIAO: getIconFont(RoundIconNames.ICON_YUANDIANXIAO, IconOptions),
  ICON_YUANDIANZHONG: getIconFont(RoundIconNames.ICON_YUANDIANZHONG, IconOptions),
  ICON_YUANHUAN: getIconFont(RoundIconNames.ICON_YUANHUAN, IconOptions),
  ICON_YUANJIAO_RECT: getIconFont(RoundIconNames.ICON_YUANJIAO_RECT, IconOptions),
  ICON_YUANDIAN: getIconFont(RoundIconNames.ICON_YUANDIAN, IconOptions),

  ICON_YUANDIAN1: getIconFont(RoundIconNames.ICON_YUANDIAN1, IconOptions),
  ICON_YUAN: getIconFont(RoundIconNames.ICON_YUAN, IconOptions),
  ICON_YUANJIAOJUXING: getIconFont(RoundIconNames.ICON_YUANJIAOJUXING, IconOptions),
  ICON_YUAN1: getIconFont(RoundIconNames.ICON_YUAN1, IconOptions),
  ICON_RADIO_OFF_FULL: getIconFont(RoundIconNames.ICON_RADIO_OFF_FULL, IconOptions),

  ICON_DOUBLE_CIRCLE_FULL: getIconFont(RoundIconNames.ICON_DOUBLE_CIRCLE_FULL, IconOptions),
  ICON_CIRCLE: getIconFont(RoundIconNames.ICON_CIRCLE, IconOptions),
  ICON_ROUND: getIconFont(RoundIconNames.ICON_ROUND, IconOptions),
};
