import iconfont_css from "@/common/icons/plants/iconfont.css";

/**
 * 获取引用图标的代码
 * @author: jiangbin
 * @date: 2021-03-26 18:51:45
 **/
export const getIconFont = (xlinkHref: string, options?: { width?: number | string, height?: number | string }) => {
  return (
    <svg className={iconfont_css.icon} aria-hidden="true" style={options}>
      <use xlinkHref={xlinkHref}></use>
    </svg>
  );
};
