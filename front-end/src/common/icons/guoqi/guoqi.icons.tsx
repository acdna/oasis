import {getIconFont} from "@/common/icons/utils/icon.font.utils";

require('@/common/icons/guoqi/iconfont');

/**
 * 国旗图标CSS名称定义
 * @author: jiangbin
 * @date: 2021-03-28 15:35:26
 **/
export const GuoQiIconCss = {
  ICON_CN: '#icon-guoqi-',

  ICON_EN: '#icon-guoqi-05-01',
};

const IconOptions = {width: '2em', height: '2em'};

/**
 * 国旗图标定义
 * @author: jiangbin
 * @date: 2021-03-26 19:02:42
 **/
export const GuoQiIcons = {
  ICON_CN: getIconFont(GuoQiIconCss.ICON_CN, IconOptions),

  ICON_EN: getIconFont(GuoQiIconCss.ICON_EN, IconOptions),
};
