import {getIconFont} from "@/common/icons/utils/icon.font.utils";

require('@/common/icons/plants/iconfont');

/**
 * 植物种属图标CSS名称定义
 * @author: jiangbin
 * @date: 2021-03-28 15:44:16
 **/
export const PlantIconNames = {
  ICON_YUMI1: '#icon-yumi',

  ICON_YUMI2: '#icon-shengxian-shucai',

  ICON_HUANGGUA: '#icon-huanggua',

  ICON_PUTAO: '#icon-putao',

  ICON_XIGUA: '#icon-xigua',

  ICON_BOCAIYE: '#icon-bocaiye',

  ICON_QIEZI: '#icon-qiezi',

  ICON_GAOLIANG: '#icon-zuowu-gaoliang',

  ICON_DADOU: '#icon-zuowu-dadou',

  ICON_GANZHE: '#icon-ganzhe',

  ICON_LUOBU: '#icon-luobu',

  ICON_NANGUA: '#icon-nangua',

  ICON_XIHULU: '#icon-xihulu',

  ICON_FANQIE: '#icon-fanqie',

  ICON_LAJIAO: '#icon-lajiao',

  ICON_XIAOMAI: '#icon-xiaomai',

  ICON_QINCAI: '#icon-shucai-',

  ICON_SIGUA: '#icon-shucai-1',

  ICON_TIANJIAO: '#icon-tianjiao',

  ICON_ZIGANLAN: '#icon-ziganlan',

  ICON_SILIAO: '#icon-yusiliao',

  ICON_LVDOU: '#icon-lvdou',

  ICON_HUASHENG: '#icon-huasheng',

  ICON_HULUOBU: '#icon-huluobu',

  ICON_WANDOU: '#icon-wandou',

  ICON_CANDOU: '#icon-candou',

  ICON_TIANGUA: '#icon-ICON-',

  ICON_BAICAI: '#icon-ICON-1',

  ICON_GANJU: '#icon-ICON-2',

  ICON_CAIDOU: '#icon-ICON-3',

  ICON_SHUIDAO: '#icon-ICON-4',

  ICON_KUGUA: '#icon-kugua',

  ICON_YOUCAI: '#icon-youcaizi',

  ICON_ZHIMA: '#icon-zhima',

  ICON_MALINGSHU: '#icon-malingshu',

  ICON_MIANHUA: '#icon-mianhua',

  ICON_XIANGRIKUI: '#icon-xiangrikuiSVG',

  ICON_DONGGUA: '#icon-donggua',

  ICON_SHENGCAI: '#icon-shengcai',

  ICON_MUGUA: '#icon-mugua',

  ICON_DNA_JIANCE: '#icon-DNAjiance',

  ICON_DNA: '#icon-ziyuan',

  ICON_DNA_ANALYSIS: '#icon-dnaanalysis',
};

const IconOptions = {width: '2em', height: '2em'};

/**
 * 植物图标定义
 * @author: jiangbin
 * @date: 2021-03-26 19:02:42
 **/
export const PlantIcons = {
  ICON_YUMI1: getIconFont(PlantIconNames.ICON_YUMI1, IconOptions),

  ICON_YUMI2: getIconFont(PlantIconNames.ICON_YUMI2, IconOptions),

  ICON_HUANGGUA: getIconFont(PlantIconNames.ICON_HUANGGUA, IconOptions),

  ICON_PUTAO: getIconFont(PlantIconNames.ICON_PUTAO, IconOptions),

  ICON_XIGUA: getIconFont(PlantIconNames.ICON_XIGUA, IconOptions),

  ICON_BOCAIYE: getIconFont(PlantIconNames.ICON_BOCAIYE, IconOptions),

  ICON_QIEZI: getIconFont(PlantIconNames.ICON_QIEZI, IconOptions),

  ICON_GAOLIANG: getIconFont(PlantIconNames.ICON_GAOLIANG, IconOptions),

  ICON_DADOU: getIconFont(PlantIconNames.ICON_DADOU, IconOptions),

  ICON_GANZHE: getIconFont(PlantIconNames.ICON_GANZHE, IconOptions),

  ICON_LUOBU: getIconFont(PlantIconNames.ICON_LUOBU, IconOptions),

  ICON_NANGUA: getIconFont(PlantIconNames.ICON_NANGUA, IconOptions),

  ICON_XIHULU: getIconFont(PlantIconNames.ICON_XIHULU, IconOptions),

  ICON_FANQIE: getIconFont(PlantIconNames.ICON_FANQIE, IconOptions),

  ICON_LAJIAO: getIconFont(PlantIconNames.ICON_LAJIAO, IconOptions),

  ICON_XIAOMAI: getIconFont(PlantIconNames.ICON_XIAOMAI, IconOptions),

  ICON_QINCAI: getIconFont(PlantIconNames.ICON_QINCAI, IconOptions),

  ICON_SIGUA: getIconFont(PlantIconNames.ICON_SIGUA, IconOptions),

  ICON_TIANJIAO: getIconFont(PlantIconNames.ICON_TIANJIAO, IconOptions),

  ICON_ZIGANLAN: getIconFont(PlantIconNames.ICON_ZIGANLAN, IconOptions),

  ICON_SILIAO: getIconFont(PlantIconNames.ICON_SILIAO, IconOptions),

  ICON_LVDOU: getIconFont(PlantIconNames.ICON_LVDOU, IconOptions),

  ICON_HUASHENG: getIconFont(PlantIconNames.ICON_HUASHENG, IconOptions),

  ICON_HULUOBU: getIconFont(PlantIconNames.ICON_HULUOBU, IconOptions),

  ICON_WANDOU: getIconFont(PlantIconNames.ICON_WANDOU, IconOptions),

  ICON_CANDOU: getIconFont(PlantIconNames.ICON_CANDOU, IconOptions),

  ICON_TIANGUA: getIconFont(PlantIconNames.ICON_TIANGUA, IconOptions),

  ICON_BAICAI: getIconFont(PlantIconNames.ICON_BAICAI, IconOptions),

  ICON_GANJU: getIconFont(PlantIconNames.ICON_GANJU, IconOptions),

  ICON_CAIDOU: getIconFont(PlantIconNames.ICON_CAIDOU, IconOptions),

  ICON_SHUIDAO: getIconFont(PlantIconNames.ICON_SHUIDAO, IconOptions),

  ICON_KUGUA: getIconFont(PlantIconNames.ICON_KUGUA, IconOptions),

  ICON_YOUCAI: getIconFont(PlantIconNames.ICON_YOUCAI, IconOptions),

  ICON_ZHIMA: getIconFont(PlantIconNames.ICON_ZHIMA, IconOptions),

  ICON_MALINGSHU: getIconFont(PlantIconNames.ICON_MALINGSHU, IconOptions),

  ICON_MIANHUA: getIconFont(PlantIconNames.ICON_MIANHUA, IconOptions),

  ICON_XIANGRIKUI: getIconFont(PlantIconNames.ICON_XIANGRIKUI, IconOptions),

  ICON_DONGGUA: getIconFont(PlantIconNames.ICON_DONGGUA, IconOptions),

  ICON_SHENGCAI: getIconFont(PlantIconNames.ICON_SHENGCAI, IconOptions),

  ICON_MUGUA: getIconFont(PlantIconNames.ICON_MUGUA, IconOptions),

  ICON_DNA_JIANCE: getIconFont(PlantIconNames.ICON_DNA_JIANCE, IconOptions),

  ICON_DNA: getIconFont(PlantIconNames.ICON_DNA, IconOptions),

  ICON_DNA_ANALYSIS: getIconFont(PlantIconNames.ICON_DNA_ANALYSIS, IconOptions),
};
