/**
 * 前端的本地缓存，基于前端的{@link localStorage}对象进行包装
 * @author: jiangbin
 * @date: 2021-02-24 10:28:58
 **/
export const LocalCache = {
  /**
   * 获取localStorage参数值
   * @author: jiangbin
   * @date: 2020-12-08 16:35:04
   **/
  getItem: (key: string) => {
    let value = localStorage.getItem(key);
    return value == null ? '' : value;
  },

  /**
   * 设置localStorage参数
   * @author: jiangbin
   * @date: 2020-12-08 16:37:20
   **/
  setItem: (key: string, value: string) => {
    localStorage.setItem(key, value);
  },

  /**
   * 清除localStorage参数
   * @author: jiangbin
   * @date: 2020-12-09 20:21:31
   **/
  clearItem: (key: string) => {
    localStorage.setItem(key, '');
  },
  /**
   * 清除localStorage所有参数
   * @author: jiangbin
   * @date: 2020-12-09 20:21:31
   **/
  clearAll: () => {
    localStorage.clear();
  },
}
