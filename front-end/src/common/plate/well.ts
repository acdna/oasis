/**
 * 孔位位置
 * @author: jiangbin
 * @date: 2021-04-07 16:19:27
 **/
export interface WellPosition {
  /**
   * 行顺序号，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 14:59:59
   **/
  row: number;
  /**
   * 列顺序号，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 15:00:01
   **/
  col: number;
}

/**
 * 孔位信息对象
 * @author: jiangbin
 * @date: 2021-03-18 14:23:15
 **/
export interface Well extends WellPosition {
  /**
   * 行号名，如A01孔位号中的A
   * @author: jiangbin
   * @date: 2021-03-18 14:59:56
   **/
  rowName: string;
  /**
   * 孔位名称，如A01
   * @author: jiangbin
   * @date: 2021-04-16 14:17:47
   **/
  well: string;
  /**
   * 上样顺序，由1开始
   * @author: jiangbin
   * @date: 2021-04-16 17:00:07
   **/
  order?: number;
  /**
   * 子板上的孔位信息
   * @author: jiangbin
   * @date: 2021-04-16 16:55:41
   **/
  sub?: Well;
}