import type {Plate} from './plate';

/**
 * 上样板规格
 * @author: jiangbin
 * @date: 2021-04-07 16:32:18
 **/
export const PLATE_SPECS: Record<string, Plate> = {
  /**
   * 96孔上样板
   *
   * @author jiang
   * @date 2021-04-16 23:08:52
   **/
  P96: {
    rowCount: 8,
    colCount: 12,
    count: 8 * 12,
  } as Plate,
  /**
   * 384孔上样板
   *
   * @author jiang
   * @date 2021-04-16 23:09:09
   **/
  P384: {
    rowCount: 8 * 2,
    colCount: 12 * 2,
    count: 8 * 12 * 4,
  } as Plate,
  /**
   * 1538孔位上样板
   *
   * @author jiang
   * @date 2021-04-16 23:09:29
   **/
  P1536: {
    rowCount: 8 * 4,
    colCount: 12 * 4,
    count: 8 * 12 * 16,
  } as Plate,
};

/**
 * 板规格工具方法
 *
 * @author jiang
 * @date 2021-04-16 22:27:32
 **/
export const PlateUtils = {
  /**
   * 获取电泳板规格详细定义信息
   * @param plateSpecs 板规格，例如:96/384/1536
   * @return
   * @author: jiangbin
   * @date: 2021-04-16 10:40:13
   **/
  get(plateSpecs: number) {
    switch (plateSpecs) {
      case PLATE_SPECS.P96.count:
        return PLATE_SPECS.P96;
      case PLATE_SPECS.P384.count:
        return PLATE_SPECS.P384;
      case PLATE_SPECS.P1536.count:
        return PLATE_SPECS.P1536;
      default:
        return PLATE_SPECS.P96;
    }
    return PLATE_SPECS.P96;
  },
};
