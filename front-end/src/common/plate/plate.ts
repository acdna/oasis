/**
 * 上样板规格定义
 * @author: jiangbin
 * @date: 2021-04-07 16:19:27
 **/
export interface Plate {
  /**
   * 总行数，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 14:59:59
   **/
  rowCount: number;
  /**
   * 总列数，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 15:00:01
   **/
  colCount: number;
  /**
   * 孔位总数
   * @author: jiangbin
   * @date: 2021-04-16 14:00:33
   **/
  count: number;
}
