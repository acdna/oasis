import {Well} from './well';
import {PATTERNS} from '../regex/patterns';

/**
 * 孔位号工具类，用于解析孔位号的行和列信息
 * @author: jiangbin
 * @date: 2021-03-18 14:07:33
 **/
export const WellUtils = {
  /**
   * 解析孔位号
   * @param well 孔位号字符串，如:A1或A01
   * @return 孔位信息
   * @author: jiangbin
   * @date: 2021-03-18 14:07:56
   **/
  parse(well: string): Well | null {
    return PATTERNS.well.match(well);
  },
  /**
   * 获取行号名称,例如给定行顺序号为2，则得到行号名称为B
   * @param row
   * @return
   * @author: jiangbin
   * @date: 2021-03-18 15:09:43
   **/
  getRowName(row: number): string {
    return String.fromCharCode(row + 'A'.charCodeAt(0) - 1);
  },
  /**
   * 获取行顺序号，例如给定:B行号，得到行顺序号为2
   * @param rowName
   * @return
   * @author: jiangbin
   * @date: 2021-03-18 15:09:48
   **/
  getRowNum(rowName: string): number {
    return rowName.charCodeAt(0) - 'A'.charCodeAt(0) + 1;
  },
  /**
   * 格式化成孔位号字符串
   * @param well 孔位信息
   * @param doubleCol true/false-是否为双列列号，例如：A01，默认为双列的
   * @return 格式化后的孔位号字符串，例如:A1或A01
   * @author: jiangbin
   * @date: 2021-03-18 14:16:59
   **/
  format(well: { row: number; col: number }, doubleCol = true): string {
    return doubleCol ? `${this.getRowName(well.row) || ''}${well?.col?.toString().padStart(2, '0') || ''}` : `${this.getRowName(well.row) || ''}${well?.col || ''}`;
  },
};
