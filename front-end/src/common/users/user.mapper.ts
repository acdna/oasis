import {getLocaleMessage} from '@/common/locales/locale';
import {mapValueEnum} from "@/components/common/select.options";
import {SysUserItem} from "@/pages/sys/user/data";

/**
 * 用户映射表，用于快速获取用户名和用户登录帐号
 * @author: jiangbin
 * @date: 2020-12-24 13:39:12
 **/
export default class UserMapper {
  users: SysUserItem[];
  nameMap: any;

  constructor(users: SysUserItem[] = []) {
    this.users = users;
    this.nameMap = this.createNameMap(users);
  }

  /**
   * 获取userLoginName-SysUserData映射表
   * @param users
   * @return {[]}
   */
  createNameMap = (users: SysUserItem[] = []) => {
    let map = {};
    if (!users || users.length == 0) return map;
    users.forEach(user => {
      map[user.userLoginName] = user;
    });
    return map;
  };

  /**
   * 获取指定登录帐号的用户信息对象
   * @param userLoginName 用户登录帐号
   * @return {string}
   */
  getUserName = (userLoginName: string) => {
    if (!userLoginName || userLoginName.length == 0) return '';
    let user = this.getUser(userLoginName);
    return getLocaleMessage({
      cn: user?.userName,
      en: user?.userNameEn,
    });
  };

  /**
   * 获取用户信息对象
   * @param userLoginName 用户登录帐号
   * @return {{}|*}
   */
  getUser = (userLoginName: string) => {
    if (!userLoginName || userLoginName.length == 0) return {};
    return this.nameMap[userLoginName];
  };

  /**
   * 获取所有用户信息
   */
  getAllUsers = () => {
    return this.users || [];
  };

  /**
   * 构建ProColumns的valueEnum字段值，用于转换登录帐号为用户姓名，支持中英国际化
   * @return {{}}
   */
  valueEnum = () => {
    return mapValueEnum({
      map: this.getAllUsers(),
      cnTextName: 'userName',
      enTextName: 'userNameEn',
      valueName: 'userLoginName',
    })
  };
};
