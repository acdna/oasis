import { uuid } from '../utils/uuid.utils';
import { SysPermissionItem } from '@/pages/sys/permission/data';
import { SYS_PMS } from '@/permission/sys/sys.pms';
import { SSR_PMS } from '@/permission/ssr/ssr.pms';
import { PmsInfo, PmsRoot } from '@/common/pms/pms.info';
import { SNP_PMS } from '@/permission/snp/snp.pms';

/**
 * 权限数据解析器
 * @author: jiangbin
 * @date: 2021-01-02 16:41:00
 **/
export const PmsParser = {
  /**
   * 创建后端所有待初始化的权限
   * @author: jiangbin
   * @date: 2021-01-02 15:21:54
   **/
  parserAll: () => {
    let records = new Array<SysPermissionItem>();

    let sys = PmsParser.parser(SYS_PMS);
    if (sys?.length > 0) {
      records = records.concat(sys);
    }

    let ssr = PmsParser.parser(SSR_PMS);
    if (ssr?.length > 0) {
      records = records.concat(ssr);
    }
    
    let snp = PmsParser.parser(SNP_PMS);
    if (snp?.length > 0) {
      records = records.concat(snp);
    }

    return records;
  },

  /**
   * 创建权限记录列表，用于初始化权限表
   * @author: jiangbin
   * @date: 2021-01-02 15:17:20
   **/
  parser: (pmsList: PmsInfo[]) => {
    if (!pmsList || pmsList?.length == 0) return [];
    let roots = new Array<SysPermissionItem>();
    let records = new Array<SysPermissionItem>();
    pmsList.forEach((pms: any, index: number) => {
      let root = {} as PmsRoot;
      let rootId = uuid();
      //权限项节点列表
      let keys = Object.keys(pms);
      keys.forEach((key: any, index: number) => {
        let item = pms[key];
        root = item.root;
        records.push({
          perId: uuid(),
          perName: item.name,
          perNameEn: item.nameEn,
          perType: 'DATA',
          perUrl: item.id,
          perMethod: '',
          perParentId: rootId,
          perOrder: (index + 1),
          perRemark: '',
          perState: item.enable ? 'ON' : 'OFF',
          perSystem: root.system,
          perModule: root.group,
        });
      });

      //权限分组节点
      let record = {
        perId: rootId,
        perName: root.name,
        perNameEn: root.nameEn,
        perType: 'DIR',
        perUrl: '',
        perMethod: '',
        perParentId: '1',
        perOrder: (index + 1),
        perRemark: '',
        perState: 'ON',
        perSystem: root.system,
        perModule: root.group,
      };
      roots.push(record);
    });
    return roots.concat(records);
  },
};
