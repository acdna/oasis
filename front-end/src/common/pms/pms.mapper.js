/**
 * 权限映射表，用于快速获取指定权限记录
 * @author: jiangbin
 * @date: 2020-12-24 13:39:12
 **/
export default class PmsMapper {
  constructor(pmsList = []) {
    this.pmsList = pmsList.filter(pms => {
      return pms.perState === 'ON';
    });
    this.pmsMap = this.createMap(this.pmsList);
  }

  /**
   * 获取pmsGroup-[pmsName:pmsValue]映射表
   * @param users
   * @return {[]}
   */
  createMap = (pmsList = []) => {
    let map = {};
    pmsList.forEach(pms => {
      //获取模块的权限映射表
      let moduleMap = map[pms.perModule] || [];

      //设置权限映射
      moduleMap[pms.perUrl] = pms;

      //设置模块权限映射表
      map[pms.perModule] = moduleMap;
    });
    return map;
  };

  getPms = (pms) => {

  };

  filter = (pmsList = []) => {
    return this.pmsList.filter(pms => {
      let target = this.getPms(pms);
      if (target) return true;
      return false;
    });
  };

};
