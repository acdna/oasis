import { AuthContext } from '@/common/auth/auth.context';
import { SysPermissionItem } from '@/pages/sys/permission/data';
import { PmsItem } from '@/common/pms/pms.info';

/**
 * 权限上下文信息
 * @author: jiangbin
 * @date: 2021-01-24 18:32:16
 **/
export const PmsContext = {

  /**
   * 判定用户是否存在给定权限列表
   * @param limits 权限列表
   * @return boolean true/false--允许用户访问/禁止用户访问
   * @author: jiangbin
   * @date: 2021-01-24 18:32:37
   **/
  has: (limits: PmsItem[] | PmsItem) => {
    if (!limits) return true;

    if (!Array.isArray(limits)) {
      limits = [limits];
    }

    if (limits.length == 0) return true;

    //当前用户权限列表
    let pms = AuthContext.pms as SysPermissionItem[];
    if (!pms || pms.length == 0) return false;

    //过滤出当前用户拥有的授权项列表
    let hasList = limits.filter((limit) => {
      let spmsId = limit.id;
      let ssystem = limit.root.system;
      let sgroup = limit.root.group;
      let exists = pms.filter((item) => {
        let tpmsId = item.perUrl;
        let tsystem = item.perSystem;
        let tgroup = item.perModule;
        return (spmsId === tpmsId && ssystem === tsystem && sgroup === tgroup);
      });
      return exists && exists.length > 0;
    });

    //用户包含给定全部的权限列表
    return hasList && hasList.length === limits.length;
  },
};
