export interface PmsRoot {
  system: string;
  group: string;
  name: string;
  nameEn: string;
  type: string;
}

export interface PmsItem {
  id: string;
  name: string;
  nameEn: string;
  enable: boolean;
  root: PmsRoot;
}

/**
 * 权限信息
 * @author: jiangbin
 * @date: 2021-01-12 17:21:01
 **/
export interface PmsInfo {
  [key: string]: PmsItem
}
