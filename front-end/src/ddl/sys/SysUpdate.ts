/**
 * SYS_UPDATE-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUpdateColNames
 */
export interface SysUpdateColNames {
  /**
   * @description 系统升级日志主键ID
   * @field UPDATE_ID
   */
  UPDATE_ID: string, //
  /**
   * @description 升级版本号
   * @field UPDATE_VERSION
   */
  UPDATE_VERSION: string, //
  /**
   * @description 升级内容
   * @field UPDATE_CONTENT
   */
  UPDATE_CONTENT: string, //
  /**
   * @description 升级负责人
   * @field UPDATE_MANAGER
   */
  UPDATE_MANAGER: string, //
  /**
   * @description 创建日期
   * @field UPDATE_CREATE_TIME
   */
  UPDATE_CREATE_TIME: Date, //
}

/**
 * SYS_UPDATE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUpdateColProps
 */
export interface SysUpdateColProps {
  /**
   * @description 系统升级日志主键ID
   * @field updateId
   */
  updateId: string, //
  /**
   * @description 升级版本号
   * @field updateVersion
   */
  updateVersion: string, //
  /**
   * @description 升级内容
   * @field updateContent
   */
  updateContent: string, //
  /**
   * @description 升级负责人
   * @field updateManager
   */
  updateManager: string, //
  /**
   * @description 创建日期
   * @field updateCreateTime
   */
  updateCreateTime: Date, //
}

/**
 * SYS_UPDATE-表列名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUpdate
 */
export const SysUpdateColName = {
  /**
   * @description 系统升级日志主键ID
   * @field UPDATE_ID
   */
  UPDATE_ID: 'UPDATE_ID', //
  /**
   * @description 升级版本号
   * @field UPDATE_VERSION
   */
  UPDATE_VERSION: 'UPDATE_VERSION', //
  /**
   * @description 升级内容
   * @field UPDATE_CONTENT
   */
  UPDATE_CONTENT: 'UPDATE_CONTENT', //
  /**
   * @description 升级负责人
   * @field UPDATE_MANAGER
   */
  UPDATE_MANAGER: 'UPDATE_MANAGER', //
  /**
   * @description 创建日期
   * @field UPDATE_CREATE_TIME
   */
  UPDATE_CREATE_TIME: 'UPDATE_CREATE_TIME', //
};
/**
 * SYS_UPDATE-表列属性名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUpdate
 */
export const SysUpdateColProp = {
  /**
   * @description 系统升级日志主键ID
   * @field updateId
   */
  updateId: 'updateId', //
  /**
   * @description 升级版本号
   * @field updateVersion
   */
  updateVersion: 'updateVersion', //
  /**
   * @description 升级内容
   * @field updateContent
   */
  updateContent: 'updateContent', //
  /**
   * @description 升级负责人
   * @field updateManager
   */
  updateManager: 'updateManager', //
  /**
   * @description 创建日期
   * @field updateCreateTime
   */
  updateCreateTime: 'updateCreateTime', //
};
/**
 * SYS_UPDATE-表信息
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @export
 * @class SysUpdateTable
 */
export const SysUpdateTable = {
  /**
   * @description UPDATE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'UPDATE_ID',
  /**
   * @description 系统升级日志主键ID
   * @field primerKey
   */
  primerKey: 'updateId',
  /**
   * @description SYS_UPDATE
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_UPDATE',
  /**
   * @description SysUpdate
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysUpdate',
};
