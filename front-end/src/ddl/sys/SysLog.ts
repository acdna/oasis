/**
 * SYS_LOG-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysLogColNames
 */
export interface SysLogColNames {
  /**
   * @description 日志ID号
   * @field LOG_ID
   */
  LOG_ID: string, //
  /**
   * @description 操作人员名称
   * @field LOG_USER
   */
  LOG_USER: string, //
  /**
   * @description 操作时间
   * @field LOG_TIME
   */
  LOG_TIME: Date, //
  /**
   * @description IP地址
   * @field LOG_IP
   */
  LOG_IP: string, //
  /**
   * @description 操作URL
   * @field LOG_URL
   */
  LOG_URL: string, //
  /**
   * @description 模块
   * @field LOG_TITLE
   */
  LOG_TITLE: string, //
  /**
   * @description 内容
   * @field LOG_CONTENT
   */
  LOG_CONTENT: string, //
  /**
   * @description 操作类型
   * @field LOG_TYPE
   */
  LOG_TYPE: number, //
}

/**
 * SYS_LOG-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysLogColProps
 */
export interface SysLogColProps {
  /**
   * @description 日志ID号
   * @field logId
   */
  logId: string, //
  /**
   * @description 操作人员名称
   * @field logUser
   */
  logUser: string, //
  /**
   * @description 操作时间
   * @field logTime
   */
  logTime: Date, //
  /**
   * @description IP地址
   * @field logIp
   */
  logIp: string, //
  /**
   * @description 操作URL
   * @field logUrl
   */
  logUrl: string, //
  /**
   * @description 模块
   * @field logTitle
   */
  logTitle: string, //
  /**
   * @description 内容
   * @field logContent
   */
  logContent: string, //
  /**
   * @description 操作类型
   * @field logType
   */
  logType: number, //
}

/**
 * SYS_LOG-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysLog
 */
export const SysLogColName = {
  /**
   * @description 日志ID号
   * @field LOG_ID
   */
  LOG_ID: 'LOG_ID', //
  /**
   * @description 操作人员名称
   * @field LOG_USER
   */
  LOG_USER: 'LOG_USER', //
  /**
   * @description 操作时间
   * @field LOG_TIME
   */
  LOG_TIME: 'LOG_TIME', //
  /**
   * @description IP地址
   * @field LOG_IP
   */
  LOG_IP: 'LOG_IP', //
  /**
   * @description 操作URL
   * @field LOG_URL
   */
  LOG_URL: 'LOG_URL', //
  /**
   * @description 模块
   * @field LOG_TITLE
   */
  LOG_TITLE: 'LOG_TITLE', //
  /**
   * @description 内容
   * @field LOG_CONTENT
   */
  LOG_CONTENT: 'LOG_CONTENT', //
  /**
   * @description 操作类型
   * @field LOG_TYPE
   */
  LOG_TYPE: 'LOG_TYPE', //
};
/**
 * SYS_LOG-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysLog
 */
export const SysLogColProp = {
  /**
   * @description 日志ID号
   * @field logId
   */
  logId: 'logId', //
  /**
   * @description 操作人员名称
   * @field logUser
   */
  logUser: 'logUser', //
  /**
   * @description 操作时间
   * @field logTime
   */
  logTime: 'logTime', //
  /**
   * @description IP地址
   * @field logIp
   */
  logIp: 'logIp', //
  /**
   * @description 操作URL
   * @field logUrl
   */
  logUrl: 'logUrl', //
  /**
   * @description 模块
   * @field logTitle
   */
  logTitle: 'logTitle', //
  /**
   * @description 内容
   * @field logContent
   */
  logContent: 'logContent', //
  /**
   * @description 操作类型
   * @field logType
   */
  logType: 'logType', //
};
/**
 * SYS_LOG-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysLogTable
 */
export const SysLogTable = {
  /**
   * @description LOG_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'LOG_ID',
  /**
   * @description 日志ID号
   * @field primerKey
   */
  primerKey: 'logId',
  /**
   * @description SYS_LOG
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_LOG',
  /**
   * @description SysLog
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysLog',
};
