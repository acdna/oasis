/**
 * SYS_DICTIONARY-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysDictionaryColNames
 */
export interface SysDictionaryColNames {
  /**
   * @description ID
   * @field DIC_ID
   */
  DIC_ID: string, //
  /**
   * @description 字典名称
   * @field DIC_NAME
   */
  DIC_NAME: string, //
  /**
   * @description
   * @field DIC_NAME_EN
   */
  DIC_NAME_EN: string, //
  /**
   * @description 字典值
   * @field DIC_VALUE
   */
  DIC_VALUE: string, //
  /**
   * @description 所属组
   * @field DIC_GROUP
   */
  DIC_GROUP: string, //
  /**
   * @description 模块名
   * @field DIC_MODULE
   */
  DIC_MODULE: string, //
  /**
   * @description 父级ID
   * @field DIC_PARENT_ID
   */
  DIC_PARENT_ID: string, //
  /**
   * @description 字典类型
   * @field DIC_TYPE
   */
  DIC_TYPE: string, //
  /**
   * @description 排序
   * @field DIC_ORDER
   */
  DIC_ORDER: number, //
  /**
   * @description 状态
   * @field DIC_STATE
   */
  DIC_STATE: string, //
  /**
   * @description
   * @field DIC_PARAMS
   */
  DIC_PARAMS: string, //
  /**
   * @description 关联种属
   * @field DIC_SPECIES
   */
  DIC_SPECIES: string, //
  /**
   * @description 创建日期
   * @field DIC_CREATE_DATE
   */
  DIC_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field DIC_UPDATE_DATE
   */
  DIC_UPDATE_DATE: Date, //
}

/**
 * SYS_DICTIONARY-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysDictionaryColProps
 */
export interface SysDictionaryColProps {
  /**
   * @description ID
   * @field dicId
   */
  dicId: string, //
  /**
   * @description 字典名称
   * @field dicName
   */
  dicName: string, //
  /**
   * @description
   * @field dicNameEn
   */
  dicNameEn: string, //
  /**
   * @description 字典值
   * @field dicValue
   */
  dicValue: string, //
  /**
   * @description 所属组
   * @field dicGroup
   */
  dicGroup: string, //
  /**
   * @description 模块名
   * @field dicModule
   */
  dicModule: string, //
  /**
   * @description 父级ID
   * @field dicParentId
   */
  dicParentId: string, //
  /**
   * @description 字典类型
   * @field dicType
   */
  dicType: string, //
  /**
   * @description 排序
   * @field dicOrder
   */
  dicOrder: number, //
  /**
   * @description 状态
   * @field dicState
   */
  dicState: string, //
  /**
   * @description
   * @field dicParams
   */
  dicParams: string, //
  /**
   * @description 关联种属
   * @field dicSpecies
   */
  dicSpecies: string, //
  /**
   * @description 创建日期
   * @field dicCreateDate
   */
  dicCreateDate: Date, //
  /**
   * @description 更新日期
   * @field dicUpdateDate
   */
  dicUpdateDate: Date, //
}

/**
 * SYS_DICTIONARY-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysDictionary
 */
export const SysDictionaryColName = {
  /**
   * @description ID
   * @field DIC_ID
   */
  DIC_ID: 'DIC_ID', //
  /**
   * @description 字典名称
   * @field DIC_NAME
   */
  DIC_NAME: 'DIC_NAME', //
  /**
   * @description
   * @field DIC_NAME_EN
   */
  DIC_NAME_EN: 'DIC_NAME_EN', //
  /**
   * @description 字典值
   * @field DIC_VALUE
   */
  DIC_VALUE: 'DIC_VALUE', //
  /**
   * @description 所属组
   * @field DIC_GROUP
   */
  DIC_GROUP: 'DIC_GROUP', //
  /**
   * @description 模块名
   * @field DIC_MODULE
   */
  DIC_MODULE: 'DIC_MODULE', //
  /**
   * @description 父级ID
   * @field DIC_PARENT_ID
   */
  DIC_PARENT_ID: 'DIC_PARENT_ID', //
  /**
   * @description 字典类型
   * @field DIC_TYPE
   */
  DIC_TYPE: 'DIC_TYPE', //
  /**
   * @description 排序
   * @field DIC_ORDER
   */
  DIC_ORDER: 'DIC_ORDER', //
  /**
   * @description 状态
   * @field DIC_STATE
   */
  DIC_STATE: 'DIC_STATE', //
  /**
   * @description
   * @field DIC_PARAMS
   */
  DIC_PARAMS: 'DIC_PARAMS', //
  /**
   * @description 关联种属
   * @field DIC_SPECIES
   */
  DIC_SPECIES: 'DIC_SPECIES', //
  /**
   * @description 创建日期
   * @field DIC_CREATE_DATE
   */
  DIC_CREATE_DATE: 'DIC_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field DIC_UPDATE_DATE
   */
  DIC_UPDATE_DATE: 'DIC_UPDATE_DATE', //
};
/**
 * SYS_DICTIONARY-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysDictionary
 */
export const SysDictionaryColProp = {
  /**
   * @description ID
   * @field dicId
   */
  dicId: 'dicId', //
  /**
   * @description 字典名称
   * @field dicName
   */
  dicName: 'dicName', //
  /**
   * @description
   * @field dicNameEn
   */
  dicNameEn: 'dicNameEn', //
  /**
   * @description 字典值
   * @field dicValue
   */
  dicValue: 'dicValue', //
  /**
   * @description 所属组
   * @field dicGroup
   */
  dicGroup: 'dicGroup', //
  /**
   * @description 模块名
   * @field dicModule
   */
  dicModule: 'dicModule', //
  /**
   * @description 父级ID
   * @field dicParentId
   */
  dicParentId: 'dicParentId', //
  /**
   * @description 字典类型
   * @field dicType
   */
  dicType: 'dicType', //
  /**
   * @description 排序
   * @field dicOrder
   */
  dicOrder: 'dicOrder', //
  /**
   * @description 状态
   * @field dicState
   */
  dicState: 'dicState', //
  /**
   * @description
   * @field dicParams
   */
  dicParams: 'dicParams', //
  /**
   * @description 关联种属
   * @field dicSpecies
   */
  dicSpecies: 'dicSpecies', //
  /**
   * @description 创建日期
   * @field dicCreateDate
   */
  dicCreateDate: 'dicCreateDate', //
  /**
   * @description 更新日期
   * @field dicUpdateDate
   */
  dicUpdateDate: 'dicUpdateDate', //
};
/**
 * SYS_DICTIONARY-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysDictionaryTable
 */
export const SysDictionaryTable = {
  /**
   * @description DIC_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'DIC_ID',
  /**
   * @description ID
   * @field primerKey
   */
  primerKey: 'dicId',
  /**
   * @description SYS_DICTIONARY
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_DICTIONARY',
  /**
   * @description SysDictionary
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysDictionary',
};
