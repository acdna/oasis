/**
 * SYS_PERMISSION-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysPermissionColNames
 */
export interface SysPermissionColNames {
  /**
   * @description 权限ID
   * @field PER_ID
   */
  PER_ID: string, //
  /**
   * @description 权限名称
   * @field PER_NAME
   */
  PER_NAME: string, //
  /**
   * @description 权限英文名
   * @field PER_NAME_EN
   */
  PER_NAME_EN: string, //
  /**
   * @description 权限类型
   * @field PER_TYPE
   */
  PER_TYPE: string, //
  /**
   * @description 权限URL
   * @field PER_URL
   */
  PER_URL: string, //
  /**
   * @description 权限访问方法
   * @field PER_METHOD
   */
  PER_METHOD: string, //
  /**
   * @description 父节点ID
   * @field PER_PARENT_ID
   */
  PER_PARENT_ID: string, //
  /**
   * @description 排序
   * @field PER_ORDER
   */
  PER_ORDER: number, //
  /**
   * @description 备注信息
   * @field PER_REMARK
   */
  PER_REMARK: string, //
  /**
   * @description 状态信息
   * @field PER_STATE
   */
  PER_STATE: string, //
  /**
   * @description 所属系统：前端
   * @field PER_SYSTEM
   */
  PER_SYSTEM: string, //
  /**
   * @description 模块
   * @field PER_MODULE
   */
  PER_MODULE: string, //
}

/**
 * SYS_PERMISSION-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysPermissionColProps
 */
export interface SysPermissionColProps {
  /**
   * @description 权限ID
   * @field perId
   */
  perId: string, //
  /**
   * @description 权限名称
   * @field perName
   */
  perName: string, //
  /**
   * @description 权限英文名
   * @field perNameEn
   */
  perNameEn: string, //
  /**
   * @description 权限类型
   * @field perType
   */
  perType: string, //
  /**
   * @description 权限URL
   * @field perUrl
   */
  perUrl: string, //
  /**
   * @description 权限访问方法
   * @field perMethod
   */
  perMethod: string, //
  /**
   * @description 父节点ID
   * @field perParentId
   */
  perParentId: string, //
  /**
   * @description 排序
   * @field perOrder
   */
  perOrder: number, //
  /**
   * @description 备注信息
   * @field perRemark
   */
  perRemark: string, //
  /**
   * @description 状态信息
   * @field perState
   */
  perState: string, //
  /**
   * @description 所属系统：前端
   * @field perSystem
   */
  perSystem: string, //
  /**
   * @description 模块
   * @field perModule
   */
  perModule: string, //
}

/**
 * SYS_PERMISSION-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysPermission
 */
export const SysPermissionColName = {
  /**
   * @description 权限ID
   * @field PER_ID
   */
  PER_ID: 'PER_ID', //
  /**
   * @description 权限名称
   * @field PER_NAME
   */
  PER_NAME: 'PER_NAME', //
  /**
   * @description 权限英文名
   * @field PER_NAME_EN
   */
  PER_NAME_EN: 'PER_NAME_EN', //
  /**
   * @description 权限类型
   * @field PER_TYPE
   */
  PER_TYPE: 'PER_TYPE', //
  /**
   * @description 权限URL
   * @field PER_URL
   */
  PER_URL: 'PER_URL', //
  /**
   * @description 权限访问方法
   * @field PER_METHOD
   */
  PER_METHOD: 'PER_METHOD', //
  /**
   * @description 父节点ID
   * @field PER_PARENT_ID
   */
  PER_PARENT_ID: 'PER_PARENT_ID', //
  /**
   * @description 排序
   * @field PER_ORDER
   */
  PER_ORDER: 'PER_ORDER', //
  /**
   * @description 备注信息
   * @field PER_REMARK
   */
  PER_REMARK: 'PER_REMARK', //
  /**
   * @description 状态信息
   * @field PER_STATE
   */
  PER_STATE: 'PER_STATE', //
  /**
   * @description 所属系统：前端
   * @field PER_SYSTEM
   */
  PER_SYSTEM: 'PER_SYSTEM', //
  /**
   * @description 模块
   * @field PER_MODULE
   */
  PER_MODULE: 'PER_MODULE', //
};
/**
 * SYS_PERMISSION-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysPermission
 */
export const SysPermissionColProp = {
  /**
   * @description 权限ID
   * @field perId
   */
  perId: 'perId', //
  /**
   * @description 权限名称
   * @field perName
   */
  perName: 'perName', //
  /**
   * @description 权限英文名
   * @field perNameEn
   */
  perNameEn: 'perNameEn', //
  /**
   * @description 权限类型
   * @field perType
   */
  perType: 'perType', //
  /**
   * @description 权限URL
   * @field perUrl
   */
  perUrl: 'perUrl', //
  /**
   * @description 权限访问方法
   * @field perMethod
   */
  perMethod: 'perMethod', //
  /**
   * @description 父节点ID
   * @field perParentId
   */
  perParentId: 'perParentId', //
  /**
   * @description 排序
   * @field perOrder
   */
  perOrder: 'perOrder', //
  /**
   * @description 备注信息
   * @field perRemark
   */
  perRemark: 'perRemark', //
  /**
   * @description 状态信息
   * @field perState
   */
  perState: 'perState', //
  /**
   * @description 所属系统：前端
   * @field perSystem
   */
  perSystem: 'perSystem', //
  /**
   * @description 模块
   * @field perModule
   */
  perModule: 'perModule', //
};
/**
 * SYS_PERMISSION-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysPermissionTable
 */
export const SysPermissionTable = {
  /**
   * @description PER_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'PER_ID',
  /**
   * @description 权限ID
   * @field primerKey
   */
  primerKey: 'perId',
  /**
   * @description SYS_PERMISSION
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_PERMISSION',
  /**
   * @description SysPermission
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysPermission',
};
