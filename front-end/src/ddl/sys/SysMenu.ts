/**
 * SYS_MENU-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMenuColNames
 */
export interface SysMenuColNames {
  /**
   * @description 菜单ID号
   * @field MENU_ID
   */
  MENU_ID: string, //
  /**
   * @description 菜单名称
   * @field MENU_NAME
   */
  MENU_NAME: string, //
  /**
   * @description 英文菜单名
   * @field MENU_NAME_EN
   */
  MENU_NAME_EN: string, //
  /**
   * @description 菜单连接地址
   * @field MENU_URL
   */
  MENU_URL: string, //
  /**
   * @description 父菜单ID
   * @field MENU_PARENT_ID
   */
  MENU_PARENT_ID: string, //
  /**
   * @description 菜单顺序
   * @field MENU_ORDER
   */
  MENU_ORDER: number, //
  /**
   * @description 菜单备注
   * @field MENU_REMARK
   */
  MENU_REMARK: string, //
  /**
   * @description 菜单状态
   * @field MENU_STATE
   */
  MENU_STATE: string, //
  /**
   * @description 菜单类型
   * @field MENU_TYPE
   */
  MENU_TYPE: string, //
  /**
   * @description 菜单图标CSS名称
   * @field MENU_ICON_CLASS
   */
  MENU_ICON_CLASS: string, //
}

/**
 * SYS_MENU-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMenuColProps
 */
export interface SysMenuColProps {
  /**
   * @description 菜单ID号
   * @field menuId
   */
  menuId: string, //
  /**
   * @description 菜单名称
   * @field menuName
   */
  menuName: string, //
  /**
   * @description 英文菜单名
   * @field menuNameEn
   */
  menuNameEn: string, //
  /**
   * @description 菜单连接地址
   * @field menuUrl
   */
  menuUrl: string, //
  /**
   * @description 父菜单ID
   * @field menuParentId
   */
  menuParentId: string, //
  /**
   * @description 菜单顺序
   * @field menuOrder
   */
  menuOrder: number, //
  /**
   * @description 菜单备注
   * @field menuRemark
   */
  menuRemark: string, //
  /**
   * @description 菜单状态
   * @field menuState
   */
  menuState: string, //
  /**
   * @description 菜单类型
   * @field menuType
   */
  menuType: string, //
  /**
   * @description 菜单图标CSS名称
   * @field menuIconClass
   */
  menuIconClass: string, //
}

/**
 * SYS_MENU-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMenu
 */
export const SysMenuColName = {
  /**
   * @description 菜单ID号
   * @field MENU_ID
   */
  MENU_ID: 'MENU_ID', //
  /**
   * @description 菜单名称
   * @field MENU_NAME
   */
  MENU_NAME: 'MENU_NAME', //
  /**
   * @description 英文菜单名
   * @field MENU_NAME_EN
   */
  MENU_NAME_EN: 'MENU_NAME_EN', //
  /**
   * @description 菜单连接地址
   * @field MENU_URL
   */
  MENU_URL: 'MENU_URL', //
  /**
   * @description 父菜单ID
   * @field MENU_PARENT_ID
   */
  MENU_PARENT_ID: 'MENU_PARENT_ID', //
  /**
   * @description 菜单顺序
   * @field MENU_ORDER
   */
  MENU_ORDER: 'MENU_ORDER', //
  /**
   * @description 菜单备注
   * @field MENU_REMARK
   */
  MENU_REMARK: 'MENU_REMARK', //
  /**
   * @description 菜单状态
   * @field MENU_STATE
   */
  MENU_STATE: 'MENU_STATE', //
  /**
   * @description 菜单类型
   * @field MENU_TYPE
   */
  MENU_TYPE: 'MENU_TYPE', //
  /**
   * @description 菜单图标CSS名称
   * @field MENU_ICON_CLASS
   */
  MENU_ICON_CLASS: 'MENU_ICON_CLASS', //
};
/**
 * SYS_MENU-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMenu
 */
export const SysMenuColProp = {
  /**
   * @description 菜单ID号
   * @field menuId
   */
  menuId: 'menuId', //
  /**
   * @description 菜单名称
   * @field menuName
   */
  menuName: 'menuName', //
  /**
   * @description 英文菜单名
   * @field menuNameEn
   */
  menuNameEn: 'menuNameEn', //
  /**
   * @description 菜单连接地址
   * @field menuUrl
   */
  menuUrl: 'menuUrl', //
  /**
   * @description 父菜单ID
   * @field menuParentId
   */
  menuParentId: 'menuParentId', //
  /**
   * @description 菜单顺序
   * @field menuOrder
   */
  menuOrder: 'menuOrder', //
  /**
   * @description 菜单备注
   * @field menuRemark
   */
  menuRemark: 'menuRemark', //
  /**
   * @description 菜单状态
   * @field menuState
   */
  menuState: 'menuState', //
  /**
   * @description 菜单类型
   * @field menuType
   */
  menuType: 'menuType', //
  /**
   * @description 菜单图标CSS名称
   * @field menuIconClass
   */
  menuIconClass: 'menuIconClass', //
};
/**
 * SYS_MENU-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysMenuTable
 */
export const SysMenuTable = {
  /**
   * @description MENU_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'MENU_ID',
  /**
   * @description 菜单ID号
   * @field primerKey
   */
  primerKey: 'menuId',
  /**
   * @description SYS_MENU
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_MENU',
  /**
   * @description SysMenu
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysMenu',
};
