/**
 * SYS_CACHE_TRACE-缓存信息表表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCacheTraceColNames
 */
export interface SysCacheTraceColNames {
  /**
   * @description 缓存记录ID
   * @field CT_ID
   */
  CT_ID: string, //
  /**
   * @description 缓存类型代码
   * @field CT_CODE
   */
  CT_CODE: string, //
  /**
   * @description 缓存操作类型代码
   * @field CT_OPERATE
   */
  CT_OPERATE: string, //
  /**
   * @description 缓存需要操作的目标记录ID
   * @field CT_TARGET_ID
   */
  CT_TARGET_ID: string, //
  /**
   * @description 创建日期
   * @field CT_CREATE_DATE
   */
  CT_CREATE_DATE: Date, //
  /**
   * @description 缓存备注信息
   * @field CT_COMMENT
   */
  CT_COMMENT: string, //
}

/**
 * SYS_CACHE_TRACE-缓存信息表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCacheTraceColProps
 */
export interface SysCacheTraceColProps {
  /**
   * @description 缓存记录ID
   * @field ctId
   */
  ctId: string, //
  /**
   * @description 缓存类型代码
   * @field ctCode
   */
  ctCode: string, //
  /**
   * @description 缓存操作类型代码
   * @field ctOperate
   */
  ctOperate: string, //
  /**
   * @description 缓存需要操作的目标记录ID
   * @field ctTargetId
   */
  ctTargetId: string, //
  /**
   * @description 创建日期
   * @field ctCreateDate
   */
  ctCreateDate: Date, //
  /**
   * @description 缓存备注信息
   * @field ctComment
   */
  ctComment: string, //
}

/**
 * SYS_CACHE_TRACE-缓存信息表表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCacheTrace
 */
export const SysCacheTraceColName = {
  /**
   * @description 缓存记录ID
   * @field CT_ID
   */
  CT_ID: 'CT_ID', //
  /**
   * @description 缓存类型代码
   * @field CT_CODE
   */
  CT_CODE: 'CT_CODE', //
  /**
   * @description 缓存操作类型代码
   * @field CT_OPERATE
   */
  CT_OPERATE: 'CT_OPERATE', //
  /**
   * @description 缓存需要操作的目标记录ID
   * @field CT_TARGET_ID
   */
  CT_TARGET_ID: 'CT_TARGET_ID', //
  /**
   * @description 创建日期
   * @field CT_CREATE_DATE
   */
  CT_CREATE_DATE: 'CT_CREATE_DATE', //
  /**
   * @description 缓存备注信息
   * @field CT_COMMENT
   */
  CT_COMMENT: 'CT_COMMENT', //
};
/**
 * SYS_CACHE_TRACE-缓存信息表表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCacheTrace
 */
export const SysCacheTraceColProp = {
  /**
   * @description 缓存记录ID
   * @field ctId
   */
  ctId: 'ctId', //
  /**
   * @description 缓存类型代码
   * @field ctCode
   */
  ctCode: 'ctCode', //
  /**
   * @description 缓存操作类型代码
   * @field ctOperate
   */
  ctOperate: 'ctOperate', //
  /**
   * @description 缓存需要操作的目标记录ID
   * @field ctTargetId
   */
  ctTargetId: 'ctTargetId', //
  /**
   * @description 创建日期
   * @field ctCreateDate
   */
  ctCreateDate: 'ctCreateDate', //
  /**
   * @description 缓存备注信息
   * @field ctComment
   */
  ctComment: 'ctComment', //
};
/**
 * SYS_CACHE_TRACE-缓存信息表表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysCacheTraceTable
 */
export const SysCacheTraceTable = {
  /**
   * @description CT_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'CT_ID',
  /**
   * @description 缓存记录ID
   * @field primerKey
   */
  primerKey: 'ctId',
  /**
   * @description SYS_CACHE_TRACE
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_CACHE_TRACE',
  /**
   * @description SysCacheTrace
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysCacheTrace',
};
