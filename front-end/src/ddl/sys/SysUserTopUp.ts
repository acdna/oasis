/**
 * SYS_USER_TOP_UP-用户充值信息表表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserTopUpColNames
 */
export interface SysUserTopUpColNames {
  /**
   * @description id
   * @field UTU_ID
   */
  UTU_ID: string, //
  /**
   * @description 用户登录名
   * @field UTU_LOGIN_NAME
   */
  UTU_LOGIN_NAME: string, //
  /**
   * @description 序列号
   * @field UTU_SERIAL_NUMBER
   */
  UTU_SERIAL_NUMBER: string, //
  /**
   * @description 计费开始时间
   * @field UTU_BILLING_START_DATE
   */
  UTU_BILLING_START_DATE: Date, //
  /**
   * @description 计费结束时间
   * @field UTU_BILLING_END_DATE
   */
  UTU_BILLING_END_DATE: Date, //
  /**
   * @description 状态
   * @field UTU_STATE
   */
  UTU_STATE: string, //
  /**
   * @description 创建日期
   * @field UTU_CREATE_DATE
   */
  UTU_CREATE_DATE: Date, //
}

/**
 * SYS_USER_TOP_UP-用户充值信息表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserTopUpColProps
 */
export interface SysUserTopUpColProps {
  /**
   * @description id
   * @field utuId
   */
  utuId: string, //
  /**
   * @description 用户登录名
   * @field utuLoginName
   */
  utuLoginName: string, //
  /**
   * @description 序列号
   * @field utuSerialNumber
   */
  utuSerialNumber: string, //
  /**
   * @description 计费开始时间
   * @field utuBillingStartDate
   */
  utuBillingStartDate: Date, //
  /**
   * @description 计费结束时间
   * @field utuBillingEndDate
   */
  utuBillingEndDate: Date, //
  /**
   * @description 状态
   * @field utuState
   */
  utuState: string, //
  /**
   * @description 创建日期
   * @field utuCreateDate
   */
  utuCreateDate: Date, //
}

/**
 * SYS_USER_TOP_UP-用户充值信息表表列名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserTopUp
 */
export const SysUserTopUpColName = {
  /**
   * @description id
   * @field UTU_ID
   */
  UTU_ID: 'UTU_ID', //
  /**
   * @description 用户登录名
   * @field UTU_LOGIN_NAME
   */
  UTU_LOGIN_NAME: 'UTU_LOGIN_NAME', //
  /**
   * @description 序列号
   * @field UTU_SERIAL_NUMBER
   */
  UTU_SERIAL_NUMBER: 'UTU_SERIAL_NUMBER', //
  /**
   * @description 计费开始时间
   * @field UTU_BILLING_START_DATE
   */
  UTU_BILLING_START_DATE: 'UTU_BILLING_START_DATE', //
  /**
   * @description 计费结束时间
   * @field UTU_BILLING_END_DATE
   */
  UTU_BILLING_END_DATE: 'UTU_BILLING_END_DATE', //
  /**
   * @description 状态
   * @field UTU_STATE
   */
  UTU_STATE: 'UTU_STATE', //
  /**
   * @description 创建日期
   * @field UTU_CREATE_DATE
   */
  UTU_CREATE_DATE: 'UTU_CREATE_DATE', //
};
/**
 * SYS_USER_TOP_UP-用户充值信息表表列属性名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserTopUp
 */
export const SysUserTopUpColProp = {
  /**
   * @description id
   * @field utuId
   */
  utuId: 'utuId', //
  /**
   * @description 用户登录名
   * @field utuLoginName
   */
  utuLoginName: 'utuLoginName', //
  /**
   * @description 序列号
   * @field utuSerialNumber
   */
  utuSerialNumber: 'utuSerialNumber', //
  /**
   * @description 计费开始时间
   * @field utuBillingStartDate
   */
  utuBillingStartDate: 'utuBillingStartDate', //
  /**
   * @description 计费结束时间
   * @field utuBillingEndDate
   */
  utuBillingEndDate: 'utuBillingEndDate', //
  /**
   * @description 状态
   * @field utuState
   */
  utuState: 'utuState', //
  /**
   * @description 创建日期
   * @field utuCreateDate
   */
  utuCreateDate: 'utuCreateDate', //
};
/**
 * SYS_USER_TOP_UP-用户充值信息表表信息
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @export
 * @class SysUserTopUpTable
 */
export const SysUserTopUpTable = {
  /**
   * @description UTU_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'UTU_ID',
  /**
   * @description id
   * @field primerKey
   */
  primerKey: 'utuId',
  /**
   * @description SYS_USER_TOP_UP
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_USER_TOP_UP',
  /**
   * @description SysUserTopUp
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysUserTopUp',
};
