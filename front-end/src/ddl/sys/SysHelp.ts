/**
 * SYS_HELP-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysHelpColNames
 */
export interface SysHelpColNames {
  /**
   * @description 帮助编号
   * @field HELP_ID
   */
  HELP_ID: string, //
  /**
   * @description 帮助问题
   * @field HELP_QUESTION
   */
  HELP_QUESTION: string, //
  /**
   * @description 帮助回答
   * @field HELP_ANSWER
   */
  HELP_ANSWER: string, //
  /**
   * @description 问题编号父级编号
   * @field HELP_PARENT_ID
   */
  HELP_PARENT_ID: string, //
  /**
   * @description 帮助类别
   * @field HELP_TYPE
   */
  HELP_TYPE: string, //
  /**
   * @description 帮助匹配路径
   * @field HELP_PATH
   */
  HELP_PATH: string, //
  /**
   * @description 排序
   * @field HELP_SORT
   */
  HELP_SORT: string, //
  /**
   * @description 创建日期
   * @field HELP_CREATE_DATE
   */
  HELP_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field HELP_UPDATE_DATE
   */
  HELP_UPDATE_DATE: Date, //
}

/**
 * SYS_HELP-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysHelpColProps
 */
export interface SysHelpColProps {
  /**
   * @description 帮助编号
   * @field helpId
   */
  helpId: string, //
  /**
   * @description 帮助问题
   * @field helpQuestion
   */
  helpQuestion: string, //
  /**
   * @description 帮助回答
   * @field helpAnswer
   */
  helpAnswer: string, //
  /**
   * @description 问题编号父级编号
   * @field helpParentId
   */
  helpParentId: string, //
  /**
   * @description 帮助类别
   * @field helpType
   */
  helpType: string, //
  /**
   * @description 帮助匹配路径
   * @field helpPath
   */
  helpPath: string, //
  /**
   * @description 排序
   * @field helpSort
   */
  helpSort: string, //
  /**
   * @description 创建日期
   * @field helpCreateDate
   */
  helpCreateDate: Date, //
  /**
   * @description 更新日期
   * @field helpUpdateDate
   */
  helpUpdateDate: Date, //
}

/**
 * SYS_HELP-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysHelp
 */
export const SysHelpColName = {
  /**
   * @description 帮助编号
   * @field HELP_ID
   */
  HELP_ID: 'HELP_ID', //
  /**
   * @description 帮助问题
   * @field HELP_QUESTION
   */
  HELP_QUESTION: 'HELP_QUESTION', //
  /**
   * @description 帮助回答
   * @field HELP_ANSWER
   */
  HELP_ANSWER: 'HELP_ANSWER', //
  /**
   * @description 问题编号父级编号
   * @field HELP_PARENT_ID
   */
  HELP_PARENT_ID: 'HELP_PARENT_ID', //
  /**
   * @description 帮助类别
   * @field HELP_TYPE
   */
  HELP_TYPE: 'HELP_TYPE', //
  /**
   * @description 帮助匹配路径
   * @field HELP_PATH
   */
  HELP_PATH: 'HELP_PATH', //
  /**
   * @description 排序
   * @field HELP_SORT
   */
  HELP_SORT: 'HELP_SORT', //
  /**
   * @description 创建日期
   * @field HELP_CREATE_DATE
   */
  HELP_CREATE_DATE: 'HELP_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field HELP_UPDATE_DATE
   */
  HELP_UPDATE_DATE: 'HELP_UPDATE_DATE', //
};
/**
 * SYS_HELP-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysHelp
 */
export const SysHelpColProp = {
  /**
   * @description 帮助编号
   * @field helpId
   */
  helpId: 'helpId', //
  /**
   * @description 帮助问题
   * @field helpQuestion
   */
  helpQuestion: 'helpQuestion', //
  /**
   * @description 帮助回答
   * @field helpAnswer
   */
  helpAnswer: 'helpAnswer', //
  /**
   * @description 问题编号父级编号
   * @field helpParentId
   */
  helpParentId: 'helpParentId', //
  /**
   * @description 帮助类别
   * @field helpType
   */
  helpType: 'helpType', //
  /**
   * @description 帮助匹配路径
   * @field helpPath
   */
  helpPath: 'helpPath', //
  /**
   * @description 排序
   * @field helpSort
   */
  helpSort: 'helpSort', //
  /**
   * @description 创建日期
   * @field helpCreateDate
   */
  helpCreateDate: 'helpCreateDate', //
  /**
   * @description 更新日期
   * @field helpUpdateDate
   */
  helpUpdateDate: 'helpUpdateDate', //
};
/**
 * SYS_HELP-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysHelpTable
 */
export const SysHelpTable = {
  /**
   * @description HELP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'HELP_ID',
  /**
   * @description 帮助编号
   * @field primerKey
   */
  primerKey: 'helpId',
  /**
   * @description SYS_HELP
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_HELP',
  /**
   * @description SysHelp
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysHelp',
};
