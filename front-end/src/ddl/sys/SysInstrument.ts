/**
 * SYS_INSTRUMENT-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysInstrumentColNames
 */
export interface SysInstrumentColNames {
  /**
   * @description 仪器编号
   * @field INS_ID
   */
  INS_ID: string, //
  /**
   * @description 仪器条码号
   * @field INS_BARCODE
   */
  INS_BARCODE: string, //
  /**
   * @description 仪器名称
   * @field INS_NAME
   */
  INS_NAME: string, //
  /**
   * @description 仪器型号
   * @field INS_MODEL
   */
  INS_MODEL: string, //
  /**
   * @description 仪器负责人
   * @field INS_MANAGER
   */
  INS_MANAGER: string, //
  /**
   * @description 仪器类型
   * @field INS_TYPE
   */
  INS_TYPE: string, //
  /**
   * @description 仪器描述
   * @field INS_REMARK
   */
  INS_REMARK: string, //
  /**
   * @description 仪器购买日期
   * @field INS_BUY_DATE
   */
  INS_BUY_DATE: Date, //
  /**
   * @description 仪器维护周期
   * @field INS_MAINTAIN_PERIOD
   */
  INS_MAINTAIN_PERIOD: string, //
  /**
   * @description 仪器厂商
   * @field INS_MAKER
   */
  INS_MAKER: string, //
  /**
   * @description 仪器价格
   * @field INS_PRICE
   */
  INS_PRICE: string, //
  /**
   * @description
   * @field INS_EXTRA_PROVIDE
   */
  INS_EXTRA_PROVIDE: string, //
  /**
   * @description 创建日期
   * @field INS_CREATE_DATE
   */
  INS_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field INS_UPDATE_DATE
   */
  INS_UPDATE_DATE: Date, //
}

/**
 * SYS_INSTRUMENT-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysInstrumentColProps
 */
export interface SysInstrumentColProps {
  /**
   * @description 仪器编号
   * @field insId
   */
  insId: string, //
  /**
   * @description 仪器条码号
   * @field insBarcode
   */
  insBarcode: string, //
  /**
   * @description 仪器名称
   * @field insName
   */
  insName: string, //
  /**
   * @description 仪器型号
   * @field insModel
   */
  insModel: string, //
  /**
   * @description 仪器负责人
   * @field insManager
   */
  insManager: string, //
  /**
   * @description 仪器类型
   * @field insType
   */
  insType: string, //
  /**
   * @description 仪器描述
   * @field insRemark
   */
  insRemark: string, //
  /**
   * @description 仪器购买日期
   * @field insBuyDate
   */
  insBuyDate: Date, //
  /**
   * @description 仪器维护周期
   * @field insMaintainPeriod
   */
  insMaintainPeriod: string, //
  /**
   * @description 仪器厂商
   * @field insMaker
   */
  insMaker: string, //
  /**
   * @description 仪器价格
   * @field insPrice
   */
  insPrice: string, //
  /**
   * @description
   * @field insExtraProvide
   */
  insExtraProvide: string, //
  /**
   * @description 创建日期
   * @field insCreateDate
   */
  insCreateDate: Date, //
  /**
   * @description 更新日期
   * @field insUpdateDate
   */
  insUpdateDate: Date, //
}

/**
 * SYS_INSTRUMENT-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysInstrument
 */
export const SysInstrumentColName = {
  /**
   * @description 仪器编号
   * @field INS_ID
   */
  INS_ID: 'INS_ID', //
  /**
   * @description 仪器条码号
   * @field INS_BARCODE
   */
  INS_BARCODE: 'INS_BARCODE', //
  /**
   * @description 仪器名称
   * @field INS_NAME
   */
  INS_NAME: 'INS_NAME', //
  /**
   * @description 仪器型号
   * @field INS_MODEL
   */
  INS_MODEL: 'INS_MODEL', //
  /**
   * @description 仪器负责人
   * @field INS_MANAGER
   */
  INS_MANAGER: 'INS_MANAGER', //
  /**
   * @description 仪器类型
   * @field INS_TYPE
   */
  INS_TYPE: 'INS_TYPE', //
  /**
   * @description 仪器描述
   * @field INS_REMARK
   */
  INS_REMARK: 'INS_REMARK', //
  /**
   * @description 仪器购买日期
   * @field INS_BUY_DATE
   */
  INS_BUY_DATE: 'INS_BUY_DATE', //
  /**
   * @description 仪器维护周期
   * @field INS_MAINTAIN_PERIOD
   */
  INS_MAINTAIN_PERIOD: 'INS_MAINTAIN_PERIOD', //
  /**
   * @description 仪器厂商
   * @field INS_MAKER
   */
  INS_MAKER: 'INS_MAKER', //
  /**
   * @description 仪器价格
   * @field INS_PRICE
   */
  INS_PRICE: 'INS_PRICE', //
  /**
   * @description
   * @field INS_EXTRA_PROVIDE
   */
  INS_EXTRA_PROVIDE: 'INS_EXTRA_PROVIDE', //
  /**
   * @description 创建日期
   * @field INS_CREATE_DATE
   */
  INS_CREATE_DATE: 'INS_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field INS_UPDATE_DATE
   */
  INS_UPDATE_DATE: 'INS_UPDATE_DATE', //
};
/**
 * SYS_INSTRUMENT-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysInstrument
 */
export const SysInstrumentColProp = {
  /**
   * @description 仪器编号
   * @field insId
   */
  insId: 'insId', //
  /**
   * @description 仪器条码号
   * @field insBarcode
   */
  insBarcode: 'insBarcode', //
  /**
   * @description 仪器名称
   * @field insName
   */
  insName: 'insName', //
  /**
   * @description 仪器型号
   * @field insModel
   */
  insModel: 'insModel', //
  /**
   * @description 仪器负责人
   * @field insManager
   */
  insManager: 'insManager', //
  /**
   * @description 仪器类型
   * @field insType
   */
  insType: 'insType', //
  /**
   * @description 仪器描述
   * @field insRemark
   */
  insRemark: 'insRemark', //
  /**
   * @description 仪器购买日期
   * @field insBuyDate
   */
  insBuyDate: 'insBuyDate', //
  /**
   * @description 仪器维护周期
   * @field insMaintainPeriod
   */
  insMaintainPeriod: 'insMaintainPeriod', //
  /**
   * @description 仪器厂商
   * @field insMaker
   */
  insMaker: 'insMaker', //
  /**
   * @description 仪器价格
   * @field insPrice
   */
  insPrice: 'insPrice', //
  /**
   * @description
   * @field insExtraProvide
   */
  insExtraProvide: 'insExtraProvide', //
  /**
   * @description 创建日期
   * @field insCreateDate
   */
  insCreateDate: 'insCreateDate', //
  /**
   * @description 更新日期
   * @field insUpdateDate
   */
  insUpdateDate: 'insUpdateDate', //
};
/**
 * SYS_INSTRUMENT-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysInstrumentTable
 */
export const SysInstrumentTable = {
  /**
   * @description INS_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'INS_ID',
  /**
   * @description 仪器编号
   * @field primerKey
   */
  primerKey: 'insId',
  /**
   * @description SYS_INSTRUMENT
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_INSTRUMENT',
  /**
   * @description SysInstrument
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysInstrument',
};
