/**
 * SYS_FILES-文件管理表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysFilesColNames
 */
export interface SysFilesColNames {
  /**
   * @description 记录ID
   * @field FILE_ID
   */
  FILE_ID: string, //
  /**
   * @description 文件条码号
   * @field FILE_BARCODE
   */
  FILE_BARCODE: string, //
  /**
   * @description 关联的其它记录信息ID号
   * @field FILE_RELATE_ID
   */
  FILE_RELATE_ID: string, //
  /**
   * @description 文件名称
   * @field FILE_NAME
   */
  FILE_NAME: string, //
  /**
   * @description 文件名
   * @field FILE_NAME_EN
   */
  FILE_NAME_EN: string, //
  /**
   * @description 文件存储路径
   * @field FILE_PATH
   */
  FILE_PATH: string, //
  /**
   * @description 创建日期
   * @field FILE_CREATE_DATE
   */
  FILE_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field FILE_UPDATE_DATE
   */
  FILE_UPDATE_DATE: Date, //
  /**
   * @description 是否被启用
   * @field FILE_IS_USED
   */
  FILE_IS_USED: string, //
  /**
   * @description 所属用户
   * @field FILE_OWNER
   */
  FILE_OWNER: string, //
  /**
   * @description 文件大小
   * @field FILE_SIZE
   */
  FILE_SIZE: number, //
  /**
   * @description 用户权限表
   * @field FILE_GRANTS
   */
  FILE_GRANTS: string, //
  /**
   * @description 文件分类
   * @field FILE_CLASSES
   */
  FILE_CLASSES: string, //
  /**
   * @description 文件状态信息
   * @field FILE_STATE
   */
  FILE_STATE: string, //
  /**
   * @description 文件类型
   * @field FILE_TYPE
   */
  FILE_TYPE: string, //
  /**
   * @description 文件备注信息
   * @field FILE_COMMENTS
   */
  FILE_COMMENTS: string, //
  /**
   * @description 种属
   * @field FILE_SPECIES
   */
  FILE_SPECIES: string, //
}

/**
 * SYS_FILES-文件管理表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysFilesColProps
 */
export interface SysFilesColProps {
  /**
   * @description 记录ID
   * @field fileId
   */
  fileId: string, //
  /**
   * @description 文件条码号
   * @field fileBarcode
   */
  fileBarcode: string, //
  /**
   * @description 关联的其它记录信息ID号
   * @field fileRelateId
   */
  fileRelateId: string, //
  /**
   * @description 文件名称
   * @field fileName
   */
  fileName: string, //
  /**
   * @description 文件名
   * @field fileNameEn
   */
  fileNameEn: string, //
  /**
   * @description 文件存储路径
   * @field filePath
   */
  filePath: string, //
  /**
   * @description 创建日期
   * @field fileCreateDate
   */
  fileCreateDate: Date, //
  /**
   * @description 更新日期
   * @field fileUpdateDate
   */
  fileUpdateDate: Date, //
  /**
   * @description 是否被启用
   * @field fileIsUsed
   */
  fileIsUsed: string, //
  /**
   * @description 所属用户
   * @field fileOwner
   */
  fileOwner: string, //
  /**
   * @description 文件大小
   * @field fileSize
   */
  fileSize: number, //
  /**
   * @description 用户权限表
   * @field fileGrants
   */
  fileGrants: string, //
  /**
   * @description 文件分类
   * @field fileClasses
   */
  fileClasses: string, //
  /**
   * @description 文件状态信息
   * @field fileState
   */
  fileState: string, //
  /**
   * @description 文件类型
   * @field fileType
   */
  fileType: string, //
  /**
   * @description 文件备注信息
   * @field fileComments
   */
  fileComments: string, //
  /**
   * @description 种属
   * @field fileSpecies
   */
  fileSpecies: string, //
}

/**
 * SYS_FILES-文件管理表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysFiles
 */
export const SysFilesColName = {
  /**
   * @description 记录ID
   * @field FILE_ID
   */
  FILE_ID: 'FILE_ID', //
  /**
   * @description 文件条码号
   * @field FILE_BARCODE
   */
  FILE_BARCODE: 'FILE_BARCODE', //
  /**
   * @description 关联的其它记录信息ID号
   * @field FILE_RELATE_ID
   */
  FILE_RELATE_ID: 'FILE_RELATE_ID', //
  /**
   * @description 文件名称
   * @field FILE_NAME
   */
  FILE_NAME: 'FILE_NAME', //
  /**
   * @description 文件名
   * @field FILE_NAME_EN
   */
  FILE_NAME_EN: 'FILE_NAME_EN', //
  /**
   * @description 文件存储路径
   * @field FILE_PATH
   */
  FILE_PATH: 'FILE_PATH', //
  /**
   * @description 创建日期
   * @field FILE_CREATE_DATE
   */
  FILE_CREATE_DATE: 'FILE_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field FILE_UPDATE_DATE
   */
  FILE_UPDATE_DATE: 'FILE_UPDATE_DATE', //
  /**
   * @description 是否被启用
   * @field FILE_IS_USED
   */
  FILE_IS_USED: 'FILE_IS_USED', //
  /**
   * @description 所属用户
   * @field FILE_OWNER
   */
  FILE_OWNER: 'FILE_OWNER', //
  /**
   * @description 文件大小
   * @field FILE_SIZE
   */
  FILE_SIZE: 'FILE_SIZE', //
  /**
   * @description 用户权限表
   * @field FILE_GRANTS
   */
  FILE_GRANTS: 'FILE_GRANTS', //
  /**
   * @description 文件分类
   * @field FILE_CLASSES
   */
  FILE_CLASSES: 'FILE_CLASSES', //
  /**
   * @description 文件状态信息
   * @field FILE_STATE
   */
  FILE_STATE: 'FILE_STATE', //
  /**
   * @description 文件类型
   * @field FILE_TYPE
   */
  FILE_TYPE: 'FILE_TYPE', //
  /**
   * @description 文件备注信息
   * @field FILE_COMMENTS
   */
  FILE_COMMENTS: 'FILE_COMMENTS', //
  /**
   * @description 种属
   * @field FILE_SPECIES
   */
  FILE_SPECIES: 'FILE_SPECIES', //
};
/**
 * SYS_FILES-文件管理表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysFiles
 */
export const SysFilesColProp = {
  /**
   * @description 记录ID
   * @field fileId
   */
  fileId: 'fileId', //
  /**
   * @description 文件条码号
   * @field fileBarcode
   */
  fileBarcode: 'fileBarcode', //
  /**
   * @description 关联的其它记录信息ID号
   * @field fileRelateId
   */
  fileRelateId: 'fileRelateId', //
  /**
   * @description 文件名称
   * @field fileName
   */
  fileName: 'fileName', //
  /**
   * @description 文件名
   * @field fileNameEn
   */
  fileNameEn: 'fileNameEn', //
  /**
   * @description 文件存储路径
   * @field filePath
   */
  filePath: 'filePath', //
  /**
   * @description 创建日期
   * @field fileCreateDate
   */
  fileCreateDate: 'fileCreateDate', //
  /**
   * @description 更新日期
   * @field fileUpdateDate
   */
  fileUpdateDate: 'fileUpdateDate', //
  /**
   * @description 是否被启用
   * @field fileIsUsed
   */
  fileIsUsed: 'fileIsUsed', //
  /**
   * @description 所属用户
   * @field fileOwner
   */
  fileOwner: 'fileOwner', //
  /**
   * @description 文件大小
   * @field fileSize
   */
  fileSize: 'fileSize', //
  /**
   * @description 用户权限表
   * @field fileGrants
   */
  fileGrants: 'fileGrants', //
  /**
   * @description 文件分类
   * @field fileClasses
   */
  fileClasses: 'fileClasses', //
  /**
   * @description 文件状态信息
   * @field fileState
   */
  fileState: 'fileState', //
  /**
   * @description 文件类型
   * @field fileType
   */
  fileType: 'fileType', //
  /**
   * @description 文件备注信息
   * @field fileComments
   */
  fileComments: 'fileComments', //
  /**
   * @description 种属
   * @field fileSpecies
   */
  fileSpecies: 'fileSpecies', //
};
/**
 * SYS_FILES-文件管理表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysFilesTable
 */
export const SysFilesTable = {
  /**
   * @description FILE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'FILE_ID',
  /**
   * @description 记录ID
   * @field primerKey
   */
  primerKey: 'fileId',
  /**
   * @description SYS_FILES
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_FILES',
  /**
   * @description SysFiles
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysFiles',
};
