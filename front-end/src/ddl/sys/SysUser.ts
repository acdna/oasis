/**
 * SYS_USER-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserColNames
 */
export interface SysUserColNames {
  /**
   * @description 用户ID
   * @field USER_ID
   */
  USER_ID: string, //
  /**
   * @description 用户登录名称
   * @field USER_LOGIN_NAME
   */
  USER_LOGIN_NAME: string, //
  /**
   * @description 用户密码
   * @field USER_PASSWORD
   */
  USER_PASSWORD: string, //
  /**
   * @description
   * @field USER_PASSWORD_PRIVATE
   */
  USER_PASSWORD_PRIVATE: string, //
  /**
   * @description 姓名缩写
   * @field USER_NAME_ABBR
   */
  USER_NAME_ABBR: string, //
  /**
   * @description 用户名称
   * @field USER_NAME
   */
  USER_NAME: string, //
  /**
   * @description 英文名
   * @field USER_NAME_EN
   */
  USER_NAME_EN: string, //
  /**
   * @description 用户的性别
   * @field USER_SEX
   */
  USER_SEX: string, //
  /**
   * @description 用户手机号
   * @field USER_PHONE
   */
  USER_PHONE: string, //
  /**
   * @description 用户的邮箱
   * @field USER_EMAIL
   */
  USER_EMAIL: string, //
  /**
   * @description 用户头像路径
   * @field USER_IMAGE_URL
   */
  USER_IMAGE_URL: string, //
  /**
   * @description 用户排序
   * @field USER_ORDER
   */
  USER_ORDER: number, //
  /**
   * @description 状态
   * @field USER_STATE
   */
  USER_STATE: string, //
  /**
   * @description 用户代码
   * @field USER_CODE
   */
  USER_CODE: string, //
  /**
   * @description 创建日期
   * @field USER_CREATE_DATE
   */
  USER_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field USER_UPDATE_DATE
   */
  USER_UPDATE_DATE: Date, //
  /**
   * @description 用户最后登录时间
   * @field USER_LAST_LOGIN_TIME
   */
  USER_LAST_LOGIN_TIME: Date, //
  /**
   * @description 用户最后登录Ip
   * @field USER_LAST_LOGIN_IP
   */
  USER_LAST_LOGIN_IP: string, //
  /**
   * @description 用户所属单位
   * @field USER_UNIT
   */
  USER_UNIT: string, //
  /**
   * @description 用户职务/职称
   * @field USER_JOB_TITLE
   */
  USER_JOB_TITLE: string, //
  /**
   * @description 用户通讯地址
   * @field USER_ADDR
   */
  USER_ADDR: string, //
  /**
   * @description 邮编
   * @field USER_POSTCODE
   */
  USER_POSTCODE: string, //
  /**
   * @description 用户绑定种属
   * @field USER_SAM_SPECIES
   */
  USER_SAM_SPECIES: string, //
  /**
   * @description 当前种属
   * @field USER_CURR_SPECIES
   */
  USER_CURR_SPECIES: string, //
}

/**
 * SYS_USER-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserColProps
 */
export interface SysUserColProps {
  /**
   * @description 用户ID
   * @field userId
   */
  userId: string, //
  /**
   * @description 用户登录名称
   * @field userLoginName
   */
  userLoginName: string, //
  /**
   * @description 用户密码
   * @field userPassword
   */
  userPassword: string, //
  /**
   * @description
   * @field userPasswordPrivate
   */
  userPasswordPrivate: string, //
  /**
   * @description 姓名缩写
   * @field userNameAbbr
   */
  userNameAbbr: string, //
  /**
   * @description 用户名称
   * @field userName
   */
  userName: string, //
  /**
   * @description 英文名
   * @field userNameEn
   */
  userNameEn: string, //
  /**
   * @description 用户的性别
   * @field userSex
   */
  userSex: string, //
  /**
   * @description 用户手机号
   * @field userPhone
   */
  userPhone: string, //
  /**
   * @description 用户的邮箱
   * @field userEmail
   */
  userEmail: string, //
  /**
   * @description 用户头像路径
   * @field userImageUrl
   */
  userImageUrl: string, //
  /**
   * @description 用户排序
   * @field userOrder
   */
  userOrder: number, //
  /**
   * @description 状态
   * @field userState
   */
  userState: string, //
  /**
   * @description 用户代码
   * @field userCode
   */
  userCode: string, //
  /**
   * @description 创建日期
   * @field userCreateDate
   */
  userCreateDate: Date, //
  /**
   * @description 更新日期
   * @field userUpdateDate
   */
  userUpdateDate: Date, //
  /**
   * @description 用户最后登录时间
   * @field userLastLoginTime
   */
  userLastLoginTime: Date, //
  /**
   * @description 用户最后登录Ip
   * @field userLastLoginIp
   */
  userLastLoginIp: string, //
  /**
   * @description 用户所属单位
   * @field userUnit
   */
  userUnit: string, //
  /**
   * @description 用户职务/职称
   * @field userJobTitle
   */
  userJobTitle: string, //
  /**
   * @description 用户通讯地址
   * @field userAddr
   */
  userAddr: string, //
  /**
   * @description 邮编
   * @field userPostcode
   */
  userPostcode: string, //
  /**
   * @description 用户绑定种属
   * @field userSamSpecies
   */
  userSamSpecies: string, //
  /**
   * @description 当前种属
   * @field userCurrSpecies
   */
  userCurrSpecies: string, //
}

/**
 * SYS_USER-表列名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUser
 */
export const SysUserColName = {
  /**
   * @description 用户ID
   * @field USER_ID
   */
  USER_ID: 'USER_ID', //
  /**
   * @description 用户登录名称
   * @field USER_LOGIN_NAME
   */
  USER_LOGIN_NAME: 'USER_LOGIN_NAME', //
  /**
   * @description 用户密码
   * @field USER_PASSWORD
   */
  USER_PASSWORD: 'USER_PASSWORD', //
  /**
   * @description
   * @field USER_PASSWORD_PRIVATE
   */
  USER_PASSWORD_PRIVATE: 'USER_PASSWORD_PRIVATE', //
  /**
   * @description 姓名缩写
   * @field USER_NAME_ABBR
   */
  USER_NAME_ABBR: 'USER_NAME_ABBR', //
  /**
   * @description 用户名称
   * @field USER_NAME
   */
  USER_NAME: 'USER_NAME', //
  /**
   * @description 英文名
   * @field USER_NAME_EN
   */
  USER_NAME_EN: 'USER_NAME_EN', //
  /**
   * @description 用户的性别
   * @field USER_SEX
   */
  USER_SEX: 'USER_SEX', //
  /**
   * @description 用户手机号
   * @field USER_PHONE
   */
  USER_PHONE: 'USER_PHONE', //
  /**
   * @description 用户的邮箱
   * @field USER_EMAIL
   */
  USER_EMAIL: 'USER_EMAIL', //
  /**
   * @description 用户头像路径
   * @field USER_IMAGE_URL
   */
  USER_IMAGE_URL: 'USER_IMAGE_URL', //
  /**
   * @description 用户排序
   * @field USER_ORDER
   */
  USER_ORDER: 'USER_ORDER', //
  /**
   * @description 状态
   * @field USER_STATE
   */
  USER_STATE: 'USER_STATE', //
  /**
   * @description 用户代码
   * @field USER_CODE
   */
  USER_CODE: 'USER_CODE', //
  /**
   * @description 创建日期
   * @field USER_CREATE_DATE
   */
  USER_CREATE_DATE: 'USER_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field USER_UPDATE_DATE
   */
  USER_UPDATE_DATE: 'USER_UPDATE_DATE', //
  /**
   * @description 用户最后登录时间
   * @field USER_LAST_LOGIN_TIME
   */
  USER_LAST_LOGIN_TIME: 'USER_LAST_LOGIN_TIME', //
  /**
   * @description 用户最后登录Ip
   * @field USER_LAST_LOGIN_IP
   */
  USER_LAST_LOGIN_IP: 'USER_LAST_LOGIN_IP', //
  /**
   * @description 用户所属单位
   * @field USER_UNIT
   */
  USER_UNIT: 'USER_UNIT', //
  /**
   * @description 用户职务/职称
   * @field USER_JOB_TITLE
   */
  USER_JOB_TITLE: 'USER_JOB_TITLE', //
  /**
   * @description 用户通讯地址
   * @field USER_ADDR
   */
  USER_ADDR: 'USER_ADDR', //
  /**
   * @description 邮编
   * @field USER_POSTCODE
   */
  USER_POSTCODE: 'USER_POSTCODE', //
  /**
   * @description 用户绑定种属
   * @field USER_SAM_SPECIES
   */
  USER_SAM_SPECIES: 'USER_SAM_SPECIES', //
  /**
   * @description 当前种属
   * @field USER_CURR_SPECIES
   */
  USER_CURR_SPECIES: 'USER_CURR_SPECIES', //
};
/**
 * SYS_USER-表列属性名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUser
 */
export const SysUserColProp = {
  /**
   * @description 用户ID
   * @field userId
   */
  userId: 'userId', //
  /**
   * @description 用户登录名称
   * @field userLoginName
   */
  userLoginName: 'userLoginName', //
  /**
   * @description 用户密码
   * @field userPassword
   */
  userPassword: 'userPassword', //
  /**
   * @description
   * @field userPasswordPrivate
   */
  userPasswordPrivate: 'userPasswordPrivate', //
  /**
   * @description 姓名缩写
   * @field userNameAbbr
   */
  userNameAbbr: 'userNameAbbr', //
  /**
   * @description 用户名称
   * @field userName
   */
  userName: 'userName', //
  /**
   * @description 英文名
   * @field userNameEn
   */
  userNameEn: 'userNameEn', //
  /**
   * @description 用户的性别
   * @field userSex
   */
  userSex: 'userSex', //
  /**
   * @description 用户手机号
   * @field userPhone
   */
  userPhone: 'userPhone', //
  /**
   * @description 用户的邮箱
   * @field userEmail
   */
  userEmail: 'userEmail', //
  /**
   * @description 用户头像路径
   * @field userImageUrl
   */
  userImageUrl: 'userImageUrl', //
  /**
   * @description 用户排序
   * @field userOrder
   */
  userOrder: 'userOrder', //
  /**
   * @description 状态
   * @field userState
   */
  userState: 'userState', //
  /**
   * @description 用户代码
   * @field userCode
   */
  userCode: 'userCode', //
  /**
   * @description 创建日期
   * @field userCreateDate
   */
  userCreateDate: 'userCreateDate', //
  /**
   * @description 更新日期
   * @field userUpdateDate
   */
  userUpdateDate: 'userUpdateDate', //
  /**
   * @description 用户最后登录时间
   * @field userLastLoginTime
   */
  userLastLoginTime: 'userLastLoginTime', //
  /**
   * @description 用户最后登录Ip
   * @field userLastLoginIp
   */
  userLastLoginIp: 'userLastLoginIp', //
  /**
   * @description 用户所属单位
   * @field userUnit
   */
  userUnit: 'userUnit', //
  /**
   * @description 用户职务/职称
   * @field userJobTitle
   */
  userJobTitle: 'userJobTitle', //
  /**
   * @description 用户通讯地址
   * @field userAddr
   */
  userAddr: 'userAddr', //
  /**
   * @description 邮编
   * @field userPostcode
   */
  userPostcode: 'userPostcode', //
  /**
   * @description 用户绑定种属
   * @field userSamSpecies
   */
  userSamSpecies: 'userSamSpecies', //
  /**
   * @description 当前种属
   * @field userCurrSpecies
   */
  userCurrSpecies: 'userCurrSpecies', //
};
/**
 * SYS_USER-表信息
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @export
 * @class SysUserTable
 */
export const SysUserTable = {
  /**
   * @description USER_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'USER_ID',
  /**
   * @description 用户ID
   * @field primerKey
   */
  primerKey: 'userId',
  /**
   * @description SYS_USER
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_USER',
  /**
   * @description SysUser
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysUser',
};
