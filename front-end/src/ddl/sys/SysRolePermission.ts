/**
 * SYS_ROLE_PERMISSION-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRolePermissionColNames
 */
export interface SysRolePermissionColNames {
  /**
   * @description 角色权限表Id
   * @field RP_ID
   */
  RP_ID: string, //
  /**
   * @description 角色ID号
   * @field RP_ROLE_ID
   */
  RP_ROLE_ID: string, //
  /**
   * @description 权限ID
   * @field RP_PERMISSION_ID
   */
  RP_PERMISSION_ID: string, //
}

/**
 * SYS_ROLE_PERMISSION-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRolePermissionColProps
 */
export interface SysRolePermissionColProps {
  /**
   * @description 角色权限表Id
   * @field rpId
   */
  rpId: string, //
  /**
   * @description 角色ID号
   * @field rpRoleId
   */
  rpRoleId: string, //
  /**
   * @description 权限ID
   * @field rpPermissionId
   */
  rpPermissionId: string, //
}

/**
 * SYS_ROLE_PERMISSION-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRolePermission
 */
export const SysRolePermissionColName = {
  /**
   * @description 角色权限表Id
   * @field RP_ID
   */
  RP_ID: 'RP_ID', //
  /**
   * @description 角色ID号
   * @field RP_ROLE_ID
   */
  RP_ROLE_ID: 'RP_ROLE_ID', //
  /**
   * @description 权限ID
   * @field RP_PERMISSION_ID
   */
  RP_PERMISSION_ID: 'RP_PERMISSION_ID', //
};
/**
 * SYS_ROLE_PERMISSION-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRolePermission
 */
export const SysRolePermissionColProp = {
  /**
   * @description 角色权限表Id
   * @field rpId
   */
  rpId: 'rpId', //
  /**
   * @description 角色ID号
   * @field rpRoleId
   */
  rpRoleId: 'rpRoleId', //
  /**
   * @description 权限ID
   * @field rpPermissionId
   */
  rpPermissionId: 'rpPermissionId', //
};
/**
 * SYS_ROLE_PERMISSION-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysRolePermissionTable
 */
export const SysRolePermissionTable = {
  /**
   * @description RP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'RP_ID',
  /**
   * @description 角色权限表Id
   * @field primerKey
   */
  primerKey: 'rpId',
  /**
   * @description SYS_ROLE_PERMISSION
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_ROLE_PERMISSION',
  /**
   * @description SysRolePermission
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysRolePermission',
};
