/**
 * SYS_CMS_NOTICE-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCmsNoticeColNames
 */
export interface SysCmsNoticeColNames {
  /**
   * @description 公告编号
   * @field SC_NOTICE_ID
   */
  SC_NOTICE_ID: string, //
  /**
   * @description 公告标题
   * @field SC_NOTICE_TITLE
   */
  SC_NOTICE_TITLE: string, //
  /**
   * @description 公告内容
   * @field SC_NOTICE_CONTENT
   */
  SC_NOTICE_CONTENT: string, //
  /**
   * @description 是否是新文件
   * @field SC_NOTICE_IS_NEW
   */
  SC_NOTICE_IS_NEW: string, //
  /**
   * @description 公告发布者
   * @field SC_NOTICE_MANAGER
   */
  SC_NOTICE_MANAGER: string, //
  /**
   * @description 创建日期
   * @field SC_NOTICE_CREATE_DATE
   */
  SC_NOTICE_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field SC_NOTICE_UPDATE_DATE
   */
  SC_NOTICE_UPDATE_DATE: Date, //
}

/**
 * SYS_CMS_NOTICE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCmsNoticeColProps
 */
export interface SysCmsNoticeColProps {
  /**
   * @description 公告编号
   * @field scNoticeId
   */
  scNoticeId: string, //
  /**
   * @description 公告标题
   * @field scNoticeTitle
   */
  scNoticeTitle: string, //
  /**
   * @description 公告内容
   * @field scNoticeContent
   */
  scNoticeContent: string, //
  /**
   * @description 是否是新文件
   * @field scNoticeIsNew
   */
  scNoticeIsNew: string, //
  /**
   * @description 公告发布者
   * @field scNoticeManager
   */
  scNoticeManager: string, //
  /**
   * @description 创建日期
   * @field scNoticeCreateDate
   */
  scNoticeCreateDate: Date, //
  /**
   * @description 更新日期
   * @field scNoticeUpdateDate
   */
  scNoticeUpdateDate: Date, //
}

/**
 * SYS_CMS_NOTICE-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCmsNotice
 */
export const SysCmsNoticeColName = {
  /**
   * @description 公告编号
   * @field SC_NOTICE_ID
   */
  SC_NOTICE_ID: 'SC_NOTICE_ID', //
  /**
   * @description 公告标题
   * @field SC_NOTICE_TITLE
   */
  SC_NOTICE_TITLE: 'SC_NOTICE_TITLE', //
  /**
   * @description 公告内容
   * @field SC_NOTICE_CONTENT
   */
  SC_NOTICE_CONTENT: 'SC_NOTICE_CONTENT', //
  /**
   * @description 是否是新文件
   * @field SC_NOTICE_IS_NEW
   */
  SC_NOTICE_IS_NEW: 'SC_NOTICE_IS_NEW', //
  /**
   * @description 公告发布者
   * @field SC_NOTICE_MANAGER
   */
  SC_NOTICE_MANAGER: 'SC_NOTICE_MANAGER', //
  /**
   * @description 创建日期
   * @field SC_NOTICE_CREATE_DATE
   */
  SC_NOTICE_CREATE_DATE: 'SC_NOTICE_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field SC_NOTICE_UPDATE_DATE
   */
  SC_NOTICE_UPDATE_DATE: 'SC_NOTICE_UPDATE_DATE', //
};
/**
 * SYS_CMS_NOTICE-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysCmsNotice
 */
export const SysCmsNoticeColProp = {
  /**
   * @description 公告编号
   * @field scNoticeId
   */
  scNoticeId: 'scNoticeId', //
  /**
   * @description 公告标题
   * @field scNoticeTitle
   */
  scNoticeTitle: 'scNoticeTitle', //
  /**
   * @description 公告内容
   * @field scNoticeContent
   */
  scNoticeContent: 'scNoticeContent', //
  /**
   * @description 是否是新文件
   * @field scNoticeIsNew
   */
  scNoticeIsNew: 'scNoticeIsNew', //
  /**
   * @description 公告发布者
   * @field scNoticeManager
   */
  scNoticeManager: 'scNoticeManager', //
  /**
   * @description 创建日期
   * @field scNoticeCreateDate
   */
  scNoticeCreateDate: 'scNoticeCreateDate', //
  /**
   * @description 更新日期
   * @field scNoticeUpdateDate
   */
  scNoticeUpdateDate: 'scNoticeUpdateDate', //
};
/**
 * SYS_CMS_NOTICE-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysCmsNoticeTable
 */
export const SysCmsNoticeTable = {
  /**
   * @description SC_NOTICE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'SC_NOTICE_ID',
  /**
   * @description 公告编号
   * @field primerKey
   */
  primerKey: 'scNoticeId',
  /**
   * @description SYS_CMS_NOTICE
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_CMS_NOTICE',
  /**
   * @description SysCmsNotice
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysCmsNotice',
};
