/**
 * SYS_NOTICE-用户提示消息表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysNoticeColNames
 */
export interface SysNoticeColNames {
  /**
   * @description 消息编号
   * @field NOTICE_ID
   */
  NOTICE_ID: string, //
  /**
   * @description 消息标题
   * @field NOTICE_TITLE
   */
  NOTICE_TITLE: string, //
  /**
   * @description 消息内容
   * @field NOTICE_CONTENT
   */
  NOTICE_CONTENT: string, //
  /**
   * @description 消息发布者
   * @field NOTICE_MANAGER
   */
  NOTICE_MANAGER: string, //
  /**
   * @description 是否是新消息
   * @field NOTICE_IS_NEW
   */
  NOTICE_IS_NEW: string, //
  /**
   * @description 创建日期
   * @field NOTICE_CREATE_DATE
   */
  NOTICE_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field NOTICE_UPDATE_DATE
   */
  NOTICE_UPDATE_DATE: Date, //
  /**
   * @description 消息接收者
   * @field NOTICE_RECEIVER
   */
  NOTICE_RECEIVER: string, //
}

/**
 * SYS_NOTICE-用户提示消息表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysNoticeColProps
 */
export interface SysNoticeColProps {
  /**
   * @description 消息编号
   * @field noticeId
   */
  noticeId: string, //
  /**
   * @description 消息标题
   * @field noticeTitle
   */
  noticeTitle: string, //
  /**
   * @description 消息内容
   * @field noticeContent
   */
  noticeContent: string, //
  /**
   * @description 消息发布者
   * @field noticeManager
   */
  noticeManager: string, //
  /**
   * @description 是否是新消息
   * @field noticeIsNew
   */
  noticeIsNew: string, //
  /**
   * @description 创建日期
   * @field noticeCreateDate
   */
  noticeCreateDate: Date, //
  /**
   * @description 更新日期
   * @field noticeUpdateDate
   */
  noticeUpdateDate: Date, //
  /**
   * @description 消息接收者
   * @field noticeReceiver
   */
  noticeReceiver: string, //
}

/**
 * SYS_NOTICE-用户提示消息表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysNotice
 */
export const SysNoticeColName = {
  /**
   * @description 消息编号
   * @field NOTICE_ID
   */
  NOTICE_ID: 'NOTICE_ID', //
  /**
   * @description 消息标题
   * @field NOTICE_TITLE
   */
  NOTICE_TITLE: 'NOTICE_TITLE', //
  /**
   * @description 消息内容
   * @field NOTICE_CONTENT
   */
  NOTICE_CONTENT: 'NOTICE_CONTENT', //
  /**
   * @description 消息发布者
   * @field NOTICE_MANAGER
   */
  NOTICE_MANAGER: 'NOTICE_MANAGER', //
  /**
   * @description 是否是新消息
   * @field NOTICE_IS_NEW
   */
  NOTICE_IS_NEW: 'NOTICE_IS_NEW', //
  /**
   * @description 创建日期
   * @field NOTICE_CREATE_DATE
   */
  NOTICE_CREATE_DATE: 'NOTICE_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field NOTICE_UPDATE_DATE
   */
  NOTICE_UPDATE_DATE: 'NOTICE_UPDATE_DATE', //
  /**
   * @description 消息接收者
   * @field NOTICE_RECEIVER
   */
  NOTICE_RECEIVER: 'NOTICE_RECEIVER', //
};
/**
 * SYS_NOTICE-用户提示消息表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysNotice
 */
export const SysNoticeColProp = {
  /**
   * @description 消息编号
   * @field noticeId
   */
  noticeId: 'noticeId', //
  /**
   * @description 消息标题
   * @field noticeTitle
   */
  noticeTitle: 'noticeTitle', //
  /**
   * @description 消息内容
   * @field noticeContent
   */
  noticeContent: 'noticeContent', //
  /**
   * @description 消息发布者
   * @field noticeManager
   */
  noticeManager: 'noticeManager', //
  /**
   * @description 是否是新消息
   * @field noticeIsNew
   */
  noticeIsNew: 'noticeIsNew', //
  /**
   * @description 创建日期
   * @field noticeCreateDate
   */
  noticeCreateDate: 'noticeCreateDate', //
  /**
   * @description 更新日期
   * @field noticeUpdateDate
   */
  noticeUpdateDate: 'noticeUpdateDate', //
  /**
   * @description 消息接收者
   * @field noticeReceiver
   */
  noticeReceiver: 'noticeReceiver', //
};
/**
 * SYS_NOTICE-用户提示消息表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysNoticeTable
 */
export const SysNoticeTable = {
  /**
   * @description NOTICE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'NOTICE_ID',
  /**
   * @description 消息编号
   * @field primerKey
   */
  primerKey: 'noticeId',
  /**
   * @description SYS_NOTICE
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_NOTICE',
  /**
   * @description SysNotice
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysNotice',
};
