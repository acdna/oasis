/**
 * SYS_ROLE-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleColNames
 */
export interface SysRoleColNames {
  /**
   * @description 角色ID
   * @field ROLE_ID
   */
  ROLE_ID: string, //
  /**
   * @description 角色名称
   * @field ROLE_NAME
   */
  ROLE_NAME: string, //
  /**
   * @description
   * @field ROLE_NAME_EN
   */
  ROLE_NAME_EN: string, //
  /**
   * @description 状态
   * @field ROLE_STATE
   */
  ROLE_STATE: string, //
  /**
   * @description 角色所属系统
   * @field ROLE_SYS
   */
  ROLE_SYS: string, //
}

/**
 * SYS_ROLE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleColProps
 */
export interface SysRoleColProps {
  /**
   * @description 角色ID
   * @field roleId
   */
  roleId: string, //
  /**
   * @description 角色名称
   * @field roleName
   */
  roleName: string, //
  /**
   * @description
   * @field roleNameEn
   */
  roleNameEn: string, //
  /**
   * @description 状态
   * @field roleState
   */
  roleState: string, //
  /**
   * @description 角色所属系统
   * @field roleSys
   */
  roleSys: string, //
}

/**
 * SYS_ROLE-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRole
 */
export const SysRoleColName = {
  /**
   * @description 角色ID
   * @field ROLE_ID
   */
  ROLE_ID: 'ROLE_ID', //
  /**
   * @description 角色名称
   * @field ROLE_NAME
   */
  ROLE_NAME: 'ROLE_NAME', //
  /**
   * @description
   * @field ROLE_NAME_EN
   */
  ROLE_NAME_EN: 'ROLE_NAME_EN', //
  /**
   * @description 状态
   * @field ROLE_STATE
   */
  ROLE_STATE: 'ROLE_STATE', //
  /**
   * @description 角色所属系统
   * @field ROLE_SYS
   */
  ROLE_SYS: 'ROLE_SYS', //
};
/**
 * SYS_ROLE-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRole
 */
export const SysRoleColProp = {
  /**
   * @description 角色ID
   * @field roleId
   */
  roleId: 'roleId', //
  /**
   * @description 角色名称
   * @field roleName
   */
  roleName: 'roleName', //
  /**
   * @description
   * @field roleNameEn
   */
  roleNameEn: 'roleNameEn', //
  /**
   * @description 状态
   * @field roleState
   */
  roleState: 'roleState', //
  /**
   * @description 角色所属系统
   * @field roleSys
   */
  roleSys: 'roleSys', //
};
/**
 * SYS_ROLE-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysRoleTable
 */
export const SysRoleTable = {
  /**
   * @description ROLE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'ROLE_ID',
  /**
   * @description 角色ID
   * @field primerKey
   */
  primerKey: 'roleId',
  /**
   * @description SYS_ROLE
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_ROLE',
  /**
   * @description SysRole
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysRole',
};
