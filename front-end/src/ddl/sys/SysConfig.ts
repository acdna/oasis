/**
 * SYS_CONFIG-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysConfigColNames
 */
export interface SysConfigColNames {
  /**
   * @description ID
   * @field CON_ID
   */
  CON_ID: string, //
  /**
   * @description 参数名称
   * @field CON_NAME
   */
  CON_NAME: string, //
  /**
   * @description 参数名称
   * @field CON_NAME_EN
   */
  CON_NAME_EN: string, //
  /**
   * @description 参数值
   * @field CON_VALUE
   */
  CON_VALUE: string, //
  /**
   * @description 所属组
   * @field CON_GROUP
   */
  CON_GROUP: string, //
  /**
   * @description 参数类型
   * @field CON_PARAM_TYPE
   */
  CON_PARAM_TYPE: string, //
  /**
   * @description 父级ID
   * @field CON_PARENT_ID
   */
  CON_PARENT_ID: string, //
  /**
   * @description 参数类型
   * @field CON_TYPE
   */
  CON_TYPE: string, //
  /**
   * @description 创建日期
   * @field CON_CREATE_DATE
   */
  CON_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field CON_UPDATE_DATE
   */
  CON_UPDATE_DATE: Date, //
  /**
   * @description 用户
   * @field CON_MANAGER
   */
  CON_MANAGER: string, //
  /**
   * @description 种属
   * @field CON_SPECIES
   */
  CON_SPECIES: string, //
  /**
   * @description 排序
   * @field CON_ORDER
   */
  CON_ORDER: number, //
  /**
   * @description 状态
   * @field CON_STATE
   */
  CON_STATE: string, //
  /**
   * @description 描述
   * @field CON_REMARK
   */
  CON_REMARK: string, //
}

/**
 * SYS_CONFIG-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysConfigColProps
 */
export interface SysConfigColProps {
  /**
   * @description ID
   * @field conId
   */
  conId: string, //
  /**
   * @description 参数名称
   * @field conName
   */
  conName: string, //
  /**
   * @description 参数名称
   * @field conNameEn
   */
  conNameEn: string, //
  /**
   * @description 参数值
   * @field conValue
   */
  conValue: string, //
  /**
   * @description 所属组
   * @field conGroup
   */
  conGroup: string, //
  /**
   * @description 参数类型
   * @field conParamType
   */
  conParamType: string, //
  /**
   * @description 父级ID
   * @field conParentId
   */
  conParentId: string, //
  /**
   * @description 参数类型
   * @field conType
   */
  conType: string, //
  /**
   * @description 创建日期
   * @field conCreateDate
   */
  conCreateDate: Date, //
  /**
   * @description 更新日期
   * @field conUpdateDate
   */
  conUpdateDate: Date, //
  /**
   * @description 用户
   * @field conManager
   */
  conManager: string, //
  /**
   * @description 种属
   * @field conSpecies
   */
  conSpecies: string, //
  /**
   * @description 排序
   * @field conOrder
   */
  conOrder: number, //
  /**
   * @description 状态
   * @field conState
   */
  conState: string, //
  /**
   * @description 描述
   * @field conRemark
   */
  conRemark: string, //
}

/**
 * SYS_CONFIG-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysConfig
 */
export const SysConfigColName = {
  /**
   * @description ID
   * @field CON_ID
   */
  CON_ID: 'CON_ID', //
  /**
   * @description 参数名称
   * @field CON_NAME
   */
  CON_NAME: 'CON_NAME', //
  /**
   * @description 参数名称
   * @field CON_NAME_EN
   */
  CON_NAME_EN: 'CON_NAME_EN', //
  /**
   * @description 参数值
   * @field CON_VALUE
   */
  CON_VALUE: 'CON_VALUE', //
  /**
   * @description 所属组
   * @field CON_GROUP
   */
  CON_GROUP: 'CON_GROUP', //
  /**
   * @description 参数类型
   * @field CON_PARAM_TYPE
   */
  CON_PARAM_TYPE: 'CON_PARAM_TYPE', //
  /**
   * @description 父级ID
   * @field CON_PARENT_ID
   */
  CON_PARENT_ID: 'CON_PARENT_ID', //
  /**
   * @description 参数类型
   * @field CON_TYPE
   */
  CON_TYPE: 'CON_TYPE', //
  /**
   * @description 创建日期
   * @field CON_CREATE_DATE
   */
  CON_CREATE_DATE: 'CON_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field CON_UPDATE_DATE
   */
  CON_UPDATE_DATE: 'CON_UPDATE_DATE', //
  /**
   * @description 用户
   * @field CON_MANAGER
   */
  CON_MANAGER: 'CON_MANAGER', //
  /**
   * @description 种属
   * @field CON_SPECIES
   */
  CON_SPECIES: 'CON_SPECIES', //
  /**
   * @description 排序
   * @field CON_ORDER
   */
  CON_ORDER: 'CON_ORDER', //
  /**
   * @description 状态
   * @field CON_STATE
   */
  CON_STATE: 'CON_STATE', //
  /**
   * @description 描述
   * @field CON_REMARK
   */
  CON_REMARK: 'CON_REMARK', //
};
/**
 * SYS_CONFIG-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysConfig
 */
export const SysConfigColProp = {
  /**
   * @description ID
   * @field conId
   */
  conId: 'conId', //
  /**
   * @description 参数名称
   * @field conName
   */
  conName: 'conName', //
  /**
   * @description 参数名称
   * @field conNameEn
   */
  conNameEn: 'conNameEn', //
  /**
   * @description 参数值
   * @field conValue
   */
  conValue: 'conValue', //
  /**
   * @description 所属组
   * @field conGroup
   */
  conGroup: 'conGroup', //
  /**
   * @description 参数类型
   * @field conParamType
   */
  conParamType: 'conParamType', //
  /**
   * @description 父级ID
   * @field conParentId
   */
  conParentId: 'conParentId', //
  /**
   * @description 参数类型
   * @field conType
   */
  conType: 'conType', //
  /**
   * @description 创建日期
   * @field conCreateDate
   */
  conCreateDate: 'conCreateDate', //
  /**
   * @description 更新日期
   * @field conUpdateDate
   */
  conUpdateDate: 'conUpdateDate', //
  /**
   * @description 用户
   * @field conManager
   */
  conManager: 'conManager', //
  /**
   * @description 种属
   * @field conSpecies
   */
  conSpecies: 'conSpecies', //
  /**
   * @description 排序
   * @field conOrder
   */
  conOrder: 'conOrder', //
  /**
   * @description 状态
   * @field conState
   */
  conState: 'conState', //
  /**
   * @description 描述
   * @field conRemark
   */
  conRemark: 'conRemark', //
};
/**
 * SYS_CONFIG-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysConfigTable
 */
export const SysConfigTable = {
  /**
   * @description CON_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'CON_ID',
  /**
   * @description ID
   * @field primerKey
   */
  primerKey: 'conId',
  /**
   * @description SYS_CONFIG
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_CONFIG',
  /**
   * @description SysConfig
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysConfig',
};
