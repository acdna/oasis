/**
 * SYS_ROLE_MENU-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleMenuColNames
 */
export interface SysRoleMenuColNames {
  /**
   * @description 角色菜单ID
   * @field RM_ID
   */
  RM_ID: string, //
  /**
   * @description 角色ID
   * @field RM_ROLE_ID
   */
  RM_ROLE_ID: string, //
  /**
   * @description 菜单ID
   * @field RM_MENU_ID
   */
  RM_MENU_ID: string, //
}

/**
 * SYS_ROLE_MENU-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleMenuColProps
 */
export interface SysRoleMenuColProps {
  /**
   * @description 角色菜单ID
   * @field rmId
   */
  rmId: string, //
  /**
   * @description 角色ID
   * @field rmRoleId
   */
  rmRoleId: string, //
  /**
   * @description 菜单ID
   * @field rmMenuId
   */
  rmMenuId: string, //
}

/**
 * SYS_ROLE_MENU-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleMenu
 */
export const SysRoleMenuColName = {
  /**
   * @description 角色菜单ID
   * @field RM_ID
   */
  RM_ID: 'RM_ID', //
  /**
   * @description 角色ID
   * @field RM_ROLE_ID
   */
  RM_ROLE_ID: 'RM_ROLE_ID', //
  /**
   * @description 菜单ID
   * @field RM_MENU_ID
   */
  RM_MENU_ID: 'RM_MENU_ID', //
};
/**
 * SYS_ROLE_MENU-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleMenu
 */
export const SysRoleMenuColProp = {
  /**
   * @description 角色菜单ID
   * @field rmId
   */
  rmId: 'rmId', //
  /**
   * @description 角色ID
   * @field rmRoleId
   */
  rmRoleId: 'rmRoleId', //
  /**
   * @description 菜单ID
   * @field rmMenuId
   */
  rmMenuId: 'rmMenuId', //
};
/**
 * SYS_ROLE_MENU-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysRoleMenuTable
 */
export const SysRoleMenuTable = {
  /**
   * @description RM_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'RM_ID',
  /**
   * @description 角色菜单ID
   * @field primerKey
   */
  primerKey: 'rmId',
  /**
   * @description SYS_ROLE_MENU
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_ROLE_MENU',
  /**
   * @description SysRoleMenu
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysRoleMenu',
};
