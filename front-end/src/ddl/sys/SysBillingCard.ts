/**
 * SYS_BILLING_CARD-计费卡信息表表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:35 PM
 * @author jiangbin
 * @class SysBillingCardColNames
 */
export interface SysBillingCardColNames {
  /**
   * @description id
   * @field BC_ID
   */
  BC_ID: string, //
  /**
   * @description 序列号信息
   * @field BC_SERIAL_NUMBER
   */
  BC_SERIAL_NUMBER: string, //
  /**
   * @description 总额
   * @field BC_AMOUNT
   */
  BC_AMOUNT: string, //
  /**
   * @description 总时间
   * @field BC_TOTAL_TIME
   */
  BC_TOTAL_TIME: number, //
  /**
   * @description 类型
   * @field BC_TYPE
   */
  BC_TYPE: string, //
  /**
   * @description 绑定帐号
   * @field BC_ACCOUNT
   */
  BC_ACCOUNT: string, //
  /**
   * @description 绑定密码
   * @field BC_PASSWORD
   */
  BC_PASSWORD: string, //
  /**
   * @description 状态
   * @field BC_STATE
   */
  BC_STATE: string, //
  /**
   * @description 最大指纹数
   * @field BC_BIGGEST_GENE_COUNT
   */
  BC_BIGGEST_GENE_COUNT: string, //
  /**
   * @description 创建日期
   * @field BC_CREATE_DATE
   */
  BC_CREATE_DATE: Date, //
}

/**
 * SYS_BILLING_CARD-计费卡信息表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:35 PM
 * @author jiangbin
 * @class SysBillingCardColProps
 */
export interface SysBillingCardColProps {
  /**
   * @description id
   * @field bcId
   */
  bcId: string, //
  /**
   * @description 序列号信息
   * @field bcSerialNumber
   */
  bcSerialNumber: string, //
  /**
   * @description 总额
   * @field bcAmount
   */
  bcAmount: string, //
  /**
   * @description 总时间
   * @field bcTotalTime
   */
  bcTotalTime: number, //
  /**
   * @description 类型
   * @field bcType
   */
  bcType: string, //
  /**
   * @description 绑定帐号
   * @field bcAccount
   */
  bcAccount: string, //
  /**
   * @description 绑定密码
   * @field bcPassword
   */
  bcPassword: string, //
  /**
   * @description 状态
   * @field bcState
   */
  bcState: string, //
  /**
   * @description 最大指纹数
   * @field bcBiggestGeneCount
   */
  bcBiggestGeneCount: string, //
  /**
   * @description 创建日期
   * @field bcCreateDate
   */
  bcCreateDate: Date, //
}

/**
 * SYS_BILLING_CARD-计费卡信息表表列名
 * @date 2/2/2021, 5:21:35 PM
 * @author jiangbin
 * @class SysBillingCard
 */
export const SysBillingCardColName = {
  /**
   * @description id
   * @field BC_ID
   */
  BC_ID: 'BC_ID', //
  /**
   * @description 序列号信息
   * @field BC_SERIAL_NUMBER
   */
  BC_SERIAL_NUMBER: 'BC_SERIAL_NUMBER', //
  /**
   * @description 总额
   * @field BC_AMOUNT
   */
  BC_AMOUNT: 'BC_AMOUNT', //
  /**
   * @description 总时间
   * @field BC_TOTAL_TIME
   */
  BC_TOTAL_TIME: 'BC_TOTAL_TIME', //
  /**
   * @description 类型
   * @field BC_TYPE
   */
  BC_TYPE: 'BC_TYPE', //
  /**
   * @description 绑定帐号
   * @field BC_ACCOUNT
   */
  BC_ACCOUNT: 'BC_ACCOUNT', //
  /**
   * @description 绑定密码
   * @field BC_PASSWORD
   */
  BC_PASSWORD: 'BC_PASSWORD', //
  /**
   * @description 状态
   * @field BC_STATE
   */
  BC_STATE: 'BC_STATE', //
  /**
   * @description 最大指纹数
   * @field BC_BIGGEST_GENE_COUNT
   */
  BC_BIGGEST_GENE_COUNT: 'BC_BIGGEST_GENE_COUNT', //
  /**
   * @description 创建日期
   * @field BC_CREATE_DATE
   */
  BC_CREATE_DATE: 'BC_CREATE_DATE', //
};
/**
 * SYS_BILLING_CARD-计费卡信息表表列属性名
 * @date 2/2/2021, 5:21:35 PM
 * @author jiangbin
 * @class SysBillingCard
 */
export const SysBillingCardColProp = {
  /**
   * @description id
   * @field bcId
   */
  bcId: 'bcId', //
  /**
   * @description 序列号信息
   * @field bcSerialNumber
   */
  bcSerialNumber: 'bcSerialNumber', //
  /**
   * @description 总额
   * @field bcAmount
   */
  bcAmount: 'bcAmount', //
  /**
   * @description 总时间
   * @field bcTotalTime
   */
  bcTotalTime: 'bcTotalTime', //
  /**
   * @description 类型
   * @field bcType
   */
  bcType: 'bcType', //
  /**
   * @description 绑定帐号
   * @field bcAccount
   */
  bcAccount: 'bcAccount', //
  /**
   * @description 绑定密码
   * @field bcPassword
   */
  bcPassword: 'bcPassword', //
  /**
   * @description 状态
   * @field bcState
   */
  bcState: 'bcState', //
  /**
   * @description 最大指纹数
   * @field bcBiggestGeneCount
   */
  bcBiggestGeneCount: 'bcBiggestGeneCount', //
  /**
   * @description 创建日期
   * @field bcCreateDate
   */
  bcCreateDate: 'bcCreateDate', //
};
/**
 * SYS_BILLING_CARD-计费卡信息表表信息
 * @date 2/2/2021, 5:21:35 PM
 * @author jiangbin
 * @export
 * @class SysBillingCardTable
 */
export const SysBillingCardTable = {
  /**
   * @description BC_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'BC_ID',
  /**
   * @description id
   * @field primerKey
   */
  primerKey: 'bcId',
  /**
   * @description SYS_BILLING_CARD
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_BILLING_CARD',
  /**
   * @description SysBillingCard
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysBillingCard',
};
