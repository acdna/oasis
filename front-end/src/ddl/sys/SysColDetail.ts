/**
 * SYS_COL_DETAIL-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysColDetailColNames
 */
export interface SysColDetailColNames {
  /**
   * @description 主键ID
   * @field CD_ID
   */
  CD_ID: string, //
  /**
   * @description 表名
   * @field CD_TABLE_NAME
   */
  CD_TABLE_NAME: string, //
  /**
   * @description 列名
   * @field CD_COL_NAME
   */
  CD_COL_NAME: string, //
  /**
   * @description 列中文名
   * @field CD_DESC_NAME
   */
  CD_DESC_NAME: string, //
  /**
   * @description 字典名
   * @field CD_DICT_NAME
   */
  CD_DICT_NAME: string, //
  /**
   * @description 模式
   * @field CD_MODE
   */
  CD_MODE: string, //
  /**
   * @description 创建日期
   * @field CD_CREATE_DATE
   */
  CD_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field CD_UPDATE_DATE
   */
  CD_UPDATE_DATE: Date, //
}

/**
 * SYS_COL_DETAIL-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysColDetailColProps
 */
export interface SysColDetailColProps {
  /**
   * @description 主键ID
   * @field cdId
   */
  cdId: string, //
  /**
   * @description 表名
   * @field cdTableName
   */
  cdTableName: string, //
  /**
   * @description 列名
   * @field cdColName
   */
  cdColName: string, //
  /**
   * @description 列中文名
   * @field cdDescName
   */
  cdDescName: string, //
  /**
   * @description 字典名
   * @field cdDictName
   */
  cdDictName: string, //
  /**
   * @description 模式
   * @field cdMode
   */
  cdMode: string, //
  /**
   * @description 创建日期
   * @field cdCreateDate
   */
  cdCreateDate: Date, //
  /**
   * @description 更新日期
   * @field cdUpdateDate
   */
  cdUpdateDate: Date, //
}

/**
 * SYS_COL_DETAIL-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysColDetail
 */
export const SysColDetailColName = {
  /**
   * @description 主键ID
   * @field CD_ID
   */
  CD_ID: 'CD_ID', //
  /**
   * @description 表名
   * @field CD_TABLE_NAME
   */
  CD_TABLE_NAME: 'CD_TABLE_NAME', //
  /**
   * @description 列名
   * @field CD_COL_NAME
   */
  CD_COL_NAME: 'CD_COL_NAME', //
  /**
   * @description 列中文名
   * @field CD_DESC_NAME
   */
  CD_DESC_NAME: 'CD_DESC_NAME', //
  /**
   * @description 字典名
   * @field CD_DICT_NAME
   */
  CD_DICT_NAME: 'CD_DICT_NAME', //
  /**
   * @description 模式
   * @field CD_MODE
   */
  CD_MODE: 'CD_MODE', //
  /**
   * @description 创建日期
   * @field CD_CREATE_DATE
   */
  CD_CREATE_DATE: 'CD_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field CD_UPDATE_DATE
   */
  CD_UPDATE_DATE: 'CD_UPDATE_DATE', //
};
/**
 * SYS_COL_DETAIL-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysColDetail
 */
export const SysColDetailColProp = {
  /**
   * @description 主键ID
   * @field cdId
   */
  cdId: 'cdId', //
  /**
   * @description 表名
   * @field cdTableName
   */
  cdTableName: 'cdTableName', //
  /**
   * @description 列名
   * @field cdColName
   */
  cdColName: 'cdColName', //
  /**
   * @description 列中文名
   * @field cdDescName
   */
  cdDescName: 'cdDescName', //
  /**
   * @description 字典名
   * @field cdDictName
   */
  cdDictName: 'cdDictName', //
  /**
   * @description 模式
   * @field cdMode
   */
  cdMode: 'cdMode', //
  /**
   * @description 创建日期
   * @field cdCreateDate
   */
  cdCreateDate: 'cdCreateDate', //
  /**
   * @description 更新日期
   * @field cdUpdateDate
   */
  cdUpdateDate: 'cdUpdateDate', //
};
/**
 * SYS_COL_DETAIL-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysColDetailTable
 */
export const SysColDetailTable = {
  /**
   * @description CD_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'CD_ID',
  /**
   * @description 主键ID
   * @field primerKey
   */
  primerKey: 'cdId',
  /**
   * @description SYS_COL_DETAIL
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_COL_DETAIL',
  /**
   * @description SysColDetail
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysColDetail',
};
