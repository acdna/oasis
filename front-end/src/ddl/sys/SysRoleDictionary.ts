/**
 * SYS_ROLE_DICTIONARY-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleDictionaryColNames
 */
export interface SysRoleDictionaryColNames {
  /**
   * @description 角色字典ID
   * @field RD_ID
   */
  RD_ID: string, //
  /**
   * @description 角色ID
   * @field RD_ROLE_ID
   */
  RD_ROLE_ID: string, //
  /**
   * @description 字典ID
   * @field RD_DICTIONARY_ID
   */
  RD_DICTIONARY_ID: string, //
}

/**
 * SYS_ROLE_DICTIONARY-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleDictionaryColProps
 */
export interface SysRoleDictionaryColProps {
  /**
   * @description 角色字典ID
   * @field rdId
   */
  rdId: string, //
  /**
   * @description 角色ID
   * @field rdRoleId
   */
  rdRoleId: string, //
  /**
   * @description 字典ID
   * @field rdDictionaryId
   */
  rdDictionaryId: string, //
}

/**
 * SYS_ROLE_DICTIONARY-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleDictionary
 */
export const SysRoleDictionaryColName = {
  /**
   * @description 角色字典ID
   * @field RD_ID
   */
  RD_ID: 'RD_ID', //
  /**
   * @description 角色ID
   * @field RD_ROLE_ID
   */
  RD_ROLE_ID: 'RD_ROLE_ID', //
  /**
   * @description 字典ID
   * @field RD_DICTIONARY_ID
   */
  RD_DICTIONARY_ID: 'RD_DICTIONARY_ID', //
};
/**
 * SYS_ROLE_DICTIONARY-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysRoleDictionary
 */
export const SysRoleDictionaryColProp = {
  /**
   * @description 角色字典ID
   * @field rdId
   */
  rdId: 'rdId', //
  /**
   * @description 角色ID
   * @field rdRoleId
   */
  rdRoleId: 'rdRoleId', //
  /**
   * @description 字典ID
   * @field rdDictionaryId
   */
  rdDictionaryId: 'rdDictionaryId', //
};
/**
 * SYS_ROLE_DICTIONARY-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysRoleDictionaryTable
 */
export const SysRoleDictionaryTable = {
  /**
   * @description RD_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'RD_ID',
  /**
   * @description 角色字典ID
   * @field primerKey
   */
  primerKey: 'rdId',
  /**
   * @description SYS_ROLE_DICTIONARY
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_ROLE_DICTIONARY',
  /**
   * @description SysRoleDictionary
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysRoleDictionary',
};
