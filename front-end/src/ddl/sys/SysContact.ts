/**
 * SYS_CONTACT-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysContactColNames
 */
export interface SysContactColNames {
  /**
   * @description 反馈编号
   * @field CONTACT_ID
   */
  CONTACT_ID: string, //
  /**
   * @description 反馈用户
   * @field CONTACT_AUTHOR
   */
  CONTACT_AUTHOR: string, //
  /**
   * @description 反馈邮箱
   * @field CONTACT_EMAIL
   */
  CONTACT_EMAIL: string, //
  /**
   * @description 反馈主题
   * @field CONTACT_SUBJECT
   */
  CONTACT_SUBJECT: string, //
  /**
   * @description 反馈内容
   * @field CONTACT_TEXT
   */
  CONTACT_TEXT: string, //
  /**
   * @description 创建日期
   * @field CONTACT_CREATE_DATE
   */
  CONTACT_CREATE_DATE: Date, //
}

/**
 * SYS_CONTACT-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysContactColProps
 */
export interface SysContactColProps {
  /**
   * @description 反馈编号
   * @field contactId
   */
  contactId: string, //
  /**
   * @description 反馈用户
   * @field contactAuthor
   */
  contactAuthor: string, //
  /**
   * @description 反馈邮箱
   * @field contactEmail
   */
  contactEmail: string, //
  /**
   * @description 反馈主题
   * @field contactSubject
   */
  contactSubject: string, //
  /**
   * @description 反馈内容
   * @field contactText
   */
  contactText: string, //
  /**
   * @description 创建日期
   * @field contactCreateDate
   */
  contactCreateDate: Date, //
}

/**
 * SYS_CONTACT-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysContact
 */
export const SysContactColName = {
  /**
   * @description 反馈编号
   * @field CONTACT_ID
   */
  CONTACT_ID: 'CONTACT_ID', //
  /**
   * @description 反馈用户
   * @field CONTACT_AUTHOR
   */
  CONTACT_AUTHOR: 'CONTACT_AUTHOR', //
  /**
   * @description 反馈邮箱
   * @field CONTACT_EMAIL
   */
  CONTACT_EMAIL: 'CONTACT_EMAIL', //
  /**
   * @description 反馈主题
   * @field CONTACT_SUBJECT
   */
  CONTACT_SUBJECT: 'CONTACT_SUBJECT', //
  /**
   * @description 反馈内容
   * @field CONTACT_TEXT
   */
  CONTACT_TEXT: 'CONTACT_TEXT', //
  /**
   * @description 创建日期
   * @field CONTACT_CREATE_DATE
   */
  CONTACT_CREATE_DATE: 'CONTACT_CREATE_DATE', //
};
/**
 * SYS_CONTACT-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysContact
 */
export const SysContactColProp = {
  /**
   * @description 反馈编号
   * @field contactId
   */
  contactId: 'contactId', //
  /**
   * @description 反馈用户
   * @field contactAuthor
   */
  contactAuthor: 'contactAuthor', //
  /**
   * @description 反馈邮箱
   * @field contactEmail
   */
  contactEmail: 'contactEmail', //
  /**
   * @description 反馈主题
   * @field contactSubject
   */
  contactSubject: 'contactSubject', //
  /**
   * @description 反馈内容
   * @field contactText
   */
  contactText: 'contactText', //
  /**
   * @description 创建日期
   * @field contactCreateDate
   */
  contactCreateDate: 'contactCreateDate', //
};
/**
 * SYS_CONTACT-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysContactTable
 */
export const SysContactTable = {
  /**
   * @description CONTACT_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'CONTACT_ID',
  /**
   * @description 反馈编号
   * @field primerKey
   */
  primerKey: 'contactId',
  /**
   * @description SYS_CONTACT
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_CONTACT',
  /**
   * @description SysContact
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysContact',
};
