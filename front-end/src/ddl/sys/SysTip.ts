/**
 * SYS_TIP-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysTipColNames
 */
export interface SysTipColNames {
  /**
   * @description 提示信息ID
   * @field TIP_ID
   */
  TIP_ID: string, //
  /**
   * @description 提示次数
   * @field TIP_COUNT
   */
  TIP_COUNT: number, //
  /**
   * @description 提示信息内容
   * @field TIP_CONTENTS
   */
  TIP_CONTENTS: string, //
  /**
   * @description 所属者
   * @field TIP_MANAGER
   */
  TIP_MANAGER: string, //
  /**
   * @description 消息受众
   * @field TIP_AUDIENCE
   */
  TIP_AUDIENCE: string, //
  /**
   * @description 创建日期
   * @field TIP_CREATE_DATE
   */
  TIP_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field TIP_UPDATE_DATE
   */
  TIP_UPDATE_DATE: Date, //
}

/**
 * SYS_TIP-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysTipColProps
 */
export interface SysTipColProps {
  /**
   * @description 提示信息ID
   * @field tipId
   */
  tipId: string, //
  /**
   * @description 提示次数
   * @field tipCount
   */
  tipCount: number, //
  /**
   * @description 提示信息内容
   * @field tipContents
   */
  tipContents: string, //
  /**
   * @description 所属者
   * @field tipManager
   */
  tipManager: string, //
  /**
   * @description 消息受众
   * @field tipAudience
   */
  tipAudience: string, //
  /**
   * @description 创建日期
   * @field tipCreateDate
   */
  tipCreateDate: Date, //
  /**
   * @description 更新日期
   * @field tipUpdateDate
   */
  tipUpdateDate: Date, //
}

/**
 * SYS_TIP-表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysTip
 */
export const SysTipColName = {
  /**
   * @description 提示信息ID
   * @field TIP_ID
   */
  TIP_ID: 'TIP_ID', //
  /**
   * @description 提示次数
   * @field TIP_COUNT
   */
  TIP_COUNT: 'TIP_COUNT', //
  /**
   * @description 提示信息内容
   * @field TIP_CONTENTS
   */
  TIP_CONTENTS: 'TIP_CONTENTS', //
  /**
   * @description 所属者
   * @field TIP_MANAGER
   */
  TIP_MANAGER: 'TIP_MANAGER', //
  /**
   * @description 消息受众
   * @field TIP_AUDIENCE
   */
  TIP_AUDIENCE: 'TIP_AUDIENCE', //
  /**
   * @description 创建日期
   * @field TIP_CREATE_DATE
   */
  TIP_CREATE_DATE: 'TIP_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field TIP_UPDATE_DATE
   */
  TIP_UPDATE_DATE: 'TIP_UPDATE_DATE', //
};
/**
 * SYS_TIP-表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysTip
 */
export const SysTipColProp = {
  /**
   * @description 提示信息ID
   * @field tipId
   */
  tipId: 'tipId', //
  /**
   * @description 提示次数
   * @field tipCount
   */
  tipCount: 'tipCount', //
  /**
   * @description 提示信息内容
   * @field tipContents
   */
  tipContents: 'tipContents', //
  /**
   * @description 所属者
   * @field tipManager
   */
  tipManager: 'tipManager', //
  /**
   * @description 消息受众
   * @field tipAudience
   */
  tipAudience: 'tipAudience', //
  /**
   * @description 创建日期
   * @field tipCreateDate
   */
  tipCreateDate: 'tipCreateDate', //
  /**
   * @description 更新日期
   * @field tipUpdateDate
   */
  tipUpdateDate: 'tipUpdateDate', //
};
/**
 * SYS_TIP-表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysTipTable
 */
export const SysTipTable = {
  /**
   * @description TIP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'TIP_ID',
  /**
   * @description 提示信息ID
   * @field primerKey
   */
  primerKey: 'tipId',
  /**
   * @description SYS_TIP
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_TIP',
  /**
   * @description SysTip
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysTip',
};
