/**
 * SYS_USER_ROLE-表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserRoleColNames
 */
export interface SysUserRoleColNames {
  /**
   * @description 用户角色ID
   * @field UR_ID
   */
  UR_ID: string, //
  /**
   * @description 角色ID
   * @field UR_ROLE_ID
   */
  UR_ROLE_ID: string, //
  /**
   * @description 用户登录名称
   * @field UR_USER_LOGIN_NAME
   */
  UR_USER_LOGIN_NAME: string, //
}

/**
 * SYS_USER_ROLE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserRoleColProps
 */
export interface SysUserRoleColProps {
  /**
   * @description 用户角色ID
   * @field urId
   */
  urId: string, //
  /**
   * @description 角色ID
   * @field urRoleId
   */
  urRoleId: string, //
  /**
   * @description 用户登录名称
   * @field urUserLoginName
   */
  urUserLoginName: string, //
}

/**
 * SYS_USER_ROLE-表列名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserRole
 */
export const SysUserRoleColName = {
  /**
   * @description 用户角色ID
   * @field UR_ID
   */
  UR_ID: 'UR_ID', //
  /**
   * @description 角色ID
   * @field UR_ROLE_ID
   */
  UR_ROLE_ID: 'UR_ROLE_ID', //
  /**
   * @description 用户登录名称
   * @field UR_USER_LOGIN_NAME
   */
  UR_USER_LOGIN_NAME: 'UR_USER_LOGIN_NAME', //
};
/**
 * SYS_USER_ROLE-表列属性名
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @class SysUserRole
 */
export const SysUserRoleColProp = {
  /**
   * @description 用户角色ID
   * @field urId
   */
  urId: 'urId', //
  /**
   * @description 角色ID
   * @field urRoleId
   */
  urRoleId: 'urRoleId', //
  /**
   * @description 用户登录名称
   * @field urUserLoginName
   */
  urUserLoginName: 'urUserLoginName', //
};
/**
 * SYS_USER_ROLE-表信息
 * @date 2/2/2021, 5:21:37 PM
 * @author jiangbin
 * @export
 * @class SysUserRoleTable
 */
export const SysUserRoleTable = {
  /**
   * @description UR_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'UR_ID',
  /**
   * @description 用户角色ID
   * @field primerKey
   */
  primerKey: 'urId',
  /**
   * @description SYS_USER_ROLE
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_USER_ROLE',
  /**
   * @description SysUserRole
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysUserRole',
};
