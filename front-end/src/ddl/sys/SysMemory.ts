/**
 * SYS_MEMORY-系统JVM内存表表列名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMemoryColNames
 */
export interface SysMemoryColNames {
  /**
   * @description
   * @field MEM_ID
   */
  MEM_ID: string, //
  /**
   * @description 总内存
   * @field MEM_TOTAL
   */
  MEM_TOTAL: number, //
  /**
   * @description 可用内存
   * @field MEM_FREE
   */
  MEM_FREE: number, //
  /**
   * @description 已使用内存
   * @field MEM_USED
   */
  MEM_USED: number, //
  /**
   * @description 内存用量是否预警
   * @field MEM_IS_WARNING
   */
  MEM_IS_WARNING: string, //
  /**
   * @description 最大内存
   * @field MEM_MAX
   */
  MEM_MAX: number, //
  /**
   * @description 创建日期
   * @field MEM_CREATE_DATE
   */
  MEM_CREATE_DATE: Date, //
  /**
   * @description 更新日期
   * @field MEM_UPDATE_DATE
   */
  MEM_UPDATE_DATE: Date, //
}

/**
 * SYS_MEMORY-系统JVM内存表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMemoryColProps
 */
export interface SysMemoryColProps {
  /**
   * @description
   * @field memId
   */
  memId: string, //
  /**
   * @description 总内存
   * @field memTotal
   */
  memTotal: number, //
  /**
   * @description 可用内存
   * @field memFree
   */
  memFree: number, //
  /**
   * @description 已使用内存
   * @field memUsed
   */
  memUsed: number, //
  /**
   * @description 内存用量是否预警
   * @field memIsWarning
   */
  memIsWarning: string, //
  /**
   * @description 最大内存
   * @field memMax
   */
  memMax: number, //
  /**
   * @description 创建日期
   * @field memCreateDate
   */
  memCreateDate: Date, //
  /**
   * @description 更新日期
   * @field memUpdateDate
   */
  memUpdateDate: Date, //
}

/**
 * SYS_MEMORY-系统JVM内存表表列名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMemory
 */
export const SysMemoryColName = {
  /**
   * @description
   * @field MEM_ID
   */
  MEM_ID: 'MEM_ID', //
  /**
   * @description 总内存
   * @field MEM_TOTAL
   */
  MEM_TOTAL: 'MEM_TOTAL', //
  /**
   * @description 可用内存
   * @field MEM_FREE
   */
  MEM_FREE: 'MEM_FREE', //
  /**
   * @description 已使用内存
   * @field MEM_USED
   */
  MEM_USED: 'MEM_USED', //
  /**
   * @description 内存用量是否预警
   * @field MEM_IS_WARNING
   */
  MEM_IS_WARNING: 'MEM_IS_WARNING', //
  /**
   * @description 最大内存
   * @field MEM_MAX
   */
  MEM_MAX: 'MEM_MAX', //
  /**
   * @description 创建日期
   * @field MEM_CREATE_DATE
   */
  MEM_CREATE_DATE: 'MEM_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field MEM_UPDATE_DATE
   */
  MEM_UPDATE_DATE: 'MEM_UPDATE_DATE', //
};
/**
 * SYS_MEMORY-系统JVM内存表表列属性名
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @class SysMemory
 */
export const SysMemoryColProp = {
  /**
   * @description
   * @field memId
   */
  memId: 'memId', //
  /**
   * @description 总内存
   * @field memTotal
   */
  memTotal: 'memTotal', //
  /**
   * @description 可用内存
   * @field memFree
   */
  memFree: 'memFree', //
  /**
   * @description 已使用内存
   * @field memUsed
   */
  memUsed: 'memUsed', //
  /**
   * @description 内存用量是否预警
   * @field memIsWarning
   */
  memIsWarning: 'memIsWarning', //
  /**
   * @description 最大内存
   * @field memMax
   */
  memMax: 'memMax', //
  /**
   * @description 创建日期
   * @field memCreateDate
   */
  memCreateDate: 'memCreateDate', //
  /**
   * @description 更新日期
   * @field memUpdateDate
   */
  memUpdateDate: 'memUpdateDate', //
};
/**
 * SYS_MEMORY-系统JVM内存表表信息
 * @date 2/2/2021, 5:21:36 PM
 * @author jiangbin
 * @export
 * @class SysMemoryTable
 */
export const SysMemoryTable = {
  /**
   * @description MEM_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY: 'MEM_ID',
  /**
   * @description
   * @field primerKey
   */
  primerKey: 'memId',
  /**
   * @description SYS_MEMORY
   * @field TABLE_NAME
   */
  TABLE_NAME: 'SYS_MEMORY',
  /**
   * @description SysMemory
   * @field ENTITY_NAME
   */
  ENTITY_NAME: 'SysMemory',
};
