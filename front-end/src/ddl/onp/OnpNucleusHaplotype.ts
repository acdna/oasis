/**
 * ONP_NUCLEUS_HAPLOTYPE-ONP_NUCLEUS_HAPLOTYPE表列名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpNucleusHaplotypeColNames
 */
export interface OnpNucleusHaplotypeColNames {
    /**
     * @description id
     * @field ONH_ID
     */
    ONH_ID: string, //
        /**
         * @description Haplotype Index
         * @field ONH_HAPLOTYPE_INDEX
         */
        ONH_HAPLOTYPE_INDEX: string, //
        /**
         * @description Chr
         * @field ONH_CHR
         */
        ONH_CHR: string, //
        /**
         * @description ONP ID
         * @field ONH_ONP_ID
         */
        ONH_ONP_ID: string, //
        /**
         * @description Haplotype Sequence
         * @field ONH_HAPLOTYPE_SEQUENCE
         */
        ONH_HAPLOTYPE_SEQUENCE: string, //
        /**
         * @description Haplotype Tag
         * @field ONH_HAPLOTYPE_TAG_SEQUENCE
         */
        ONH_HAPLOTYPE_TAG_SEQUENCE: string, //
        /**
         * @description Frequency
         * @field ONH_FREQUENCY
         */
        ONH_FREQUENCY: string, //
        /**
         * @description Column1
         * @field ONH_COLUMN1
         */
        ONH_COLUMN1: string, //
        /**
         * @description Column2
         * @field ONH_COLUMN2
         */
        ONH_COLUMN2: string, //
        /**
         * @description Column3
         * @field ONH_COLUMN3
         */
        ONH_COLUMN3: string, //
        /**
         * @description ID
         * @field ONH_ORDER
         */
        ONH_ORDER: number, //
        /**
         * @description Species
         * @field ONH_SPECIES
         */
        ONH_SPECIES: string, //
        /**
         * @description Remark
         * @field ONH_REMARK
         */
        ONH_REMARK: string, //
        /**
         * @description 创建日期
         * @field ONH_CREATE_DATE
         */
        ONH_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field ONH_UPDATE_DATE
         */
        ONH_UPDATE_DATE: Date, //
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-ONP_NUCLEUS_HAPLOTYPE表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpNucleusHaplotypeColProps
 */
export interface OnpNucleusHaplotypeColProps {
    /**
     * @description id
     * @field onhId
     */
    onhId: string, //
        /**
         * @description Haplotype Index
         * @field onhHaplotypeIndex
         */
        onhHaplotypeIndex: string, //
        /**
         * @description Chr
         * @field onhChr
         */
        onhChr: string, //
        /**
         * @description ONP ID
         * @field onhOnpId
         */
        onhOnpId: string, //
        /**
         * @description Haplotype Sequence
         * @field onhHaplotypeSequence
         */
        onhHaplotypeSequence: string, //
        /**
         * @description Haplotype Tag
         * @field onhHaplotypeTagSequence
         */
        onhHaplotypeTagSequence: string, //
        /**
         * @description Frequency
         * @field onhFrequency
         */
        onhFrequency: string, //
        /**
         * @description Column1
         * @field onhColumn1
         */
        onhColumn1: string, //
        /**
         * @description Column2
         * @field onhColumn2
         */
        onhColumn2: string, //
        /**
         * @description Column3
         * @field onhColumn3
         */
        onhColumn3: string, //
        /**
         * @description ID
         * @field onhOrder
         */
        onhOrder: number, //
        /**
         * @description Species
         * @field onhSpecies
         */
        onhSpecies: string, //
        /**
         * @description Remark
         * @field onhRemark
         */
        onhRemark: string, //
        /**
         * @description 创建日期
         * @field onhCreateDate
         */
        onhCreateDate: Date, //
        /**
         * @description 更新日期
         * @field onhUpdateDate
         */
        onhUpdateDate: Date, //
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-ONP_NUCLEUS_HAPLOTYPE表列名
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpNucleusHaplotype
 */
export const OnpNucleusHaplotypeColName = {
    /**
     * @description id
     * @field ONH_ID
     */
    ONH_ID: "ONH_ID", //
    /**
     * @description Haplotype Index
     * @field ONH_HAPLOTYPE_INDEX
     */
    ONH_HAPLOTYPE_INDEX: "ONH_HAPLOTYPE_INDEX", //
    /**
     * @description Chr
     * @field ONH_CHR
     */
    ONH_CHR: "ONH_CHR", //
    /**
     * @description ONP ID
     * @field ONH_ONP_ID
     */
    ONH_ONP_ID: "ONH_ONP_ID", //
    /**
     * @description Haplotype Sequence
     * @field ONH_HAPLOTYPE_SEQUENCE
     */
    ONH_HAPLOTYPE_SEQUENCE: "ONH_HAPLOTYPE_SEQUENCE", //
    /**
     * @description Haplotype Tag
     * @field ONH_HAPLOTYPE_TAG_SEQUENCE
     */
    ONH_HAPLOTYPE_TAG_SEQUENCE: "ONH_HAPLOTYPE_TAG_SEQUENCE", //
    /**
     * @description Frequency
     * @field ONH_FREQUENCY
     */
    ONH_FREQUENCY: "ONH_FREQUENCY", //
    /**
     * @description Column1
     * @field ONH_COLUMN1
     */
    ONH_COLUMN1: "ONH_COLUMN1", //
    /**
     * @description Column2
     * @field ONH_COLUMN2
     */
    ONH_COLUMN2: "ONH_COLUMN2", //
    /**
     * @description Column3
     * @field ONH_COLUMN3
     */
    ONH_COLUMN3: "ONH_COLUMN3", //
    /**
     * @description ID
     * @field ONH_ORDER
     */
    ONH_ORDER: "ONH_ORDER", //
    /**
     * @description Species
     * @field ONH_SPECIES
     */
    ONH_SPECIES: "ONH_SPECIES", //
    /**
     * @description Remark
     * @field ONH_REMARK
     */
    ONH_REMARK: "ONH_REMARK", //
    /**
     * @description 创建日期
     * @field ONH_CREATE_DATE
     */
    ONH_CREATE_DATE: "ONH_CREATE_DATE", //
    /**
     * @description 更新日期
     * @field ONH_UPDATE_DATE
     */
    ONH_UPDATE_DATE: "ONH_UPDATE_DATE", //
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-ONP_NUCLEUS_HAPLOTYPE表列属性名
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpNucleusHaplotype
 */
export const OnpNucleusHaplotypeColProp = {
    /**
     * @description id
     * @field onhId
     */
    onhId: "onhId", //
    /**
     * @description Haplotype Index
     * @field onhHaplotypeIndex
     */
    onhHaplotypeIndex: "onhHaplotypeIndex", //
    /**
     * @description Chr
     * @field onhChr
     */
    onhChr: "onhChr", //
    /**
     * @description ONP ID
     * @field onhOnpId
     */
    onhOnpId: "onhOnpId", //
    /**
     * @description Haplotype Sequence
     * @field onhHaplotypeSequence
     */
    onhHaplotypeSequence: "onhHaplotypeSequence", //
    /**
     * @description Haplotype Tag
     * @field onhHaplotypeTagSequence
     */
    onhHaplotypeTagSequence: "onhHaplotypeTagSequence", //
    /**
     * @description Frequency
     * @field onhFrequency
     */
    onhFrequency: "onhFrequency", //
    /**
     * @description Column1
     * @field onhColumn1
     */
    onhColumn1: "onhColumn1", //
    /**
     * @description Column2
     * @field onhColumn2
     */
    onhColumn2: "onhColumn2", //
    /**
     * @description Column3
     * @field onhColumn3
     */
    onhColumn3: "onhColumn3", //
    /**
     * @description ID
     * @field onhOrder
     */
    onhOrder: "onhOrder", //
    /**
     * @description Species
     * @field onhSpecies
     */
    onhSpecies: "onhSpecies", //
    /**
     * @description Remark
     * @field onhRemark
     */
    onhRemark: "onhRemark", //
    /**
     * @description 创建日期
     * @field onhCreateDate
     */
    onhCreateDate: "onhCreateDate", //
    /**
     * @description 更新日期
     * @field onhUpdateDate
     */
    onhUpdateDate: "onhUpdateDate", //
}
/**
 * ONP_NUCLEUS_HAPLOTYPE-ONP_NUCLEUS_HAPLOTYPE表信息
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export
 * @class OnpNucleusHaplotypeTable
 */
export const OnpNucleusHaplotypeTable = {
    /**
     * @description ONH_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY: 'ONH_ID',
    /**
     * @description id
     * @field primerKey
     */
    primerKey: 'onhId',
    /**
     * @description ONP_NUCLEUS_HAPLOTYPE
     * @field TABLE_NAME
     */
    TABLE_NAME: 'ONP_NUCLEUS_HAPLOTYPE',
    /**
     * @description OnpNucleusHaplotype
     * @field ENTITY_NAME
     */
    ENTITY_NAME: 'OnpNucleusHaplotype',
}