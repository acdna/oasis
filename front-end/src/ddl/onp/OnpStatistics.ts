/**
 * ONP_STATISTICS-ONP统计数据表表列名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatisticsColNames
 */
export interface OnpStatisticsColNames {
    /**
     * @description id
     * @field ST_ID
     */
    ST_ID: string, //
        /**
         * @description Bin
         * @field ST_BIN
         */
        ST_BIN: string, //
        /**
         * @description Chr
         * @field ST_CHR
         */
        ST_CHR: string, //
        /**
         * @description Start
         * @field ST_START
         */
        ST_START: string, //
        /**
         * @description Stop
         * @field ST_STOP
         */
        ST_STOP: string, //
        /**
         * @description Bin
         * @field ST_BIN_SIZE
         */
        ST_BIN_SIZE: string, //
        /**
         * @description Desc
         * @field ST_DESC
         */
        ST_DESC: string, //
        /**
         * @description ALL
         * @field ST_ALL_MARKERS
         */
        ST_ALL_MARKERS: string, //
        /**
         * @description SNP
         * @field ST_SNP_MARKERS
         */
        ST_SNP_MARKERS: string, //
        /**
         * @description INDEL
         * @field ST_INDEL_MARKERS
         */
        ST_INDEL_MARKERS: string, //
        /**
         * @description Block
         * @field ST_BLOCK_MARKERS
         */
        ST_BLOCK_MARKERS: string, //
        /**
         * @description Tags
         * @field ST_TAGS
         */
        ST_TAGS: string, //
        /**
         * @description Genotypes
         * @field ST_GENOTYPES
         */
        ST_GENOTYPES: string, //
        /**
         * @description Max
         * @field ST_MAX_GENOTYPES_FREQ
         */
        ST_MAX_GENOTYPES_FREQ: string, //
        /**
         * @description Min
         * @field ST_MIN_GENOTYPES_FREQ
         */
        ST_MIN_GENOTYPES_FREQ: string, //
        /**
         * @description PIC
         * @field ST_PIC
         */
        ST_PIC: string, //
        /**
         * @description Genetic
         * @field ST_GENETIC_MAP
         */
        ST_GENETIC_MAP: string, //
        /**
         * @description ORDER
         * @field ST_ORDER
         */
        ST_ORDER: number, //
        /**
         * @description Column1
         * @field ST_COLUMN1
         */
        ST_COLUMN1: string, //
        /**
         * @description Column2
         * @field ST_COLUMN2
         */
        ST_COLUMN2: string, //
        /**
         * @description Column3
         * @field ST_COLUMN3
         */
        ST_COLUMN3: string, //
        /**
         * @description Species
         * @field ST_SPECIES
         */
        ST_SPECIES: string, //
        /**
         * @description Remark
         * @field ST_REMARK
         */
        ST_REMARK: string, //
        /**
         * @description 创建日期
         * @field ST_CREATE_DATE
         */
        ST_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field ST_UPDATE_DATE
         */
        ST_UPDATE_DATE: Date, //
}
/**
 * ONP_STATISTICS-ONP统计数据表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatisticsColProps
 */
export interface OnpStatisticsColProps {
    /**
     * @description id
     * @field stId
     */
    stId: string, //
        /**
         * @description Bin
         * @field stBin
         */
        stBin: string, //
        /**
         * @description Chr
         * @field stChr
         */
        stChr: string, //
        /**
         * @description Start
         * @field stStart
         */
        stStart: string, //
        /**
         * @description Stop
         * @field stStop
         */
        stStop: string, //
        /**
         * @description Bin
         * @field stBinSize
         */
        stBinSize: string, //
        /**
         * @description Desc
         * @field stDesc
         */
        stDesc: string, //
        /**
         * @description ALL
         * @field stAllMarkers
         */
        stAllMarkers: string, //
        /**
         * @description SNP
         * @field stSnpMarkers
         */
        stSnpMarkers: string, //
        /**
         * @description INDEL
         * @field stIndelMarkers
         */
        stIndelMarkers: string, //
        /**
         * @description Block
         * @field stBlockMarkers
         */
        stBlockMarkers: string, //
        /**
         * @description Tags
         * @field stTags
         */
        stTags: string, //
        /**
         * @description Genotypes
         * @field stGenotypes
         */
        stGenotypes: string, //
        /**
         * @description Max
         * @field stMaxGenotypesFreq
         */
        stMaxGenotypesFreq: string, //
        /**
         * @description Min
         * @field stMinGenotypesFreq
         */
        stMinGenotypesFreq: string, //
        /**
         * @description PIC
         * @field stPic
         */
        stPic: string, //
        /**
         * @description Genetic
         * @field stGeneticMap
         */
        stGeneticMap: string, //
        /**
         * @description ORDER
         * @field stOrder
         */
        stOrder: number, //
        /**
         * @description Column1
         * @field stColumn1
         */
        stColumn1: string, //
        /**
         * @description Column2
         * @field stColumn2
         */
        stColumn2: string, //
        /**
         * @description Column3
         * @field stColumn3
         */
        stColumn3: string, //
        /**
         * @description Species
         * @field stSpecies
         */
        stSpecies: string, //
        /**
         * @description Remark
         * @field stRemark
         */
        stRemark: string, //
        /**
         * @description 创建日期
         * @field stCreateDate
         */
        stCreateDate: Date, //
        /**
         * @description 更新日期
         * @field stUpdateDate
         */
        stUpdateDate: Date, //
}
/**
 * ONP_STATISTICS-ONP统计数据表表列名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatistics
 */
export const OnpStatisticsColName = {
    /**
     * @description id
     * @field ST_ID
     */
    ST_ID: "ST_ID", //
    /**
     * @description Bin
     * @field ST_BIN
     */
    ST_BIN: "ST_BIN", //
    /**
     * @description Chr
     * @field ST_CHR
     */
    ST_CHR: "ST_CHR", //
    /**
     * @description Start
     * @field ST_START
     */
    ST_START: "ST_START", //
    /**
     * @description Stop
     * @field ST_STOP
     */
    ST_STOP: "ST_STOP", //
    /**
     * @description Bin
     * @field ST_BIN_SIZE
     */
    ST_BIN_SIZE: "ST_BIN_SIZE", //
    /**
     * @description Desc
     * @field ST_DESC
     */
    ST_DESC: "ST_DESC", //
    /**
     * @description ALL
     * @field ST_ALL_MARKERS
     */
    ST_ALL_MARKERS: "ST_ALL_MARKERS", //
    /**
     * @description SNP
     * @field ST_SNP_MARKERS
     */
    ST_SNP_MARKERS: "ST_SNP_MARKERS", //
    /**
     * @description INDEL
     * @field ST_INDEL_MARKERS
     */
    ST_INDEL_MARKERS: "ST_INDEL_MARKERS", //
    /**
     * @description Block
     * @field ST_BLOCK_MARKERS
     */
    ST_BLOCK_MARKERS: "ST_BLOCK_MARKERS", //
    /**
     * @description Tags
     * @field ST_TAGS
     */
    ST_TAGS: "ST_TAGS", //
    /**
     * @description Genotypes
     * @field ST_GENOTYPES
     */
    ST_GENOTYPES: "ST_GENOTYPES", //
    /**
     * @description Max
     * @field ST_MAX_GENOTYPES_FREQ
     */
    ST_MAX_GENOTYPES_FREQ: "ST_MAX_GENOTYPES_FREQ", //
    /**
     * @description Min
     * @field ST_MIN_GENOTYPES_FREQ
     */
    ST_MIN_GENOTYPES_FREQ: "ST_MIN_GENOTYPES_FREQ", //
    /**
     * @description PIC
     * @field ST_PIC
     */
    ST_PIC: "ST_PIC", //
    /**
     * @description Genetic
     * @field ST_GENETIC_MAP
     */
    ST_GENETIC_MAP: "ST_GENETIC_MAP", //
    /**
     * @description ORDER
     * @field ST_ORDER
     */
    ST_ORDER: "ST_ORDER", //
    /**
     * @description Column1
     * @field ST_COLUMN1
     */
    ST_COLUMN1: "ST_COLUMN1", //
    /**
     * @description Column2
     * @field ST_COLUMN2
     */
    ST_COLUMN2: "ST_COLUMN2", //
    /**
     * @description Column3
     * @field ST_COLUMN3
     */
    ST_COLUMN3: "ST_COLUMN3", //
    /**
     * @description Species
     * @field ST_SPECIES
     */
    ST_SPECIES: "ST_SPECIES", //
    /**
     * @description Remark
     * @field ST_REMARK
     */
    ST_REMARK: "ST_REMARK", //
    /**
     * @description 创建日期
     * @field ST_CREATE_DATE
     */
    ST_CREATE_DATE: "ST_CREATE_DATE", //
    /**
     * @description 更新日期
     * @field ST_UPDATE_DATE
     */
    ST_UPDATE_DATE: "ST_UPDATE_DATE", //
}
/**
 * ONP_STATISTICS-ONP统计数据表表列属性名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatistics
 */
export const OnpStatisticsColProp = {
    /**
     * @description id
     * @field stId
     */
    stId: "stId", //
    /**
     * @description Bin
     * @field stBin
     */
    stBin: "stBin", //
    /**
     * @description Chr
     * @field stChr
     */
    stChr: "stChr", //
    /**
     * @description Start
     * @field stStart
     */
    stStart: "stStart", //
    /**
     * @description Stop
     * @field stStop
     */
    stStop: "stStop", //
    /**
     * @description Bin
     * @field stBinSize
     */
    stBinSize: "stBinSize", //
    /**
     * @description Desc
     * @field stDesc
     */
    stDesc: "stDesc", //
    /**
     * @description ALL
     * @field stAllMarkers
     */
    stAllMarkers: "stAllMarkers", //
    /**
     * @description SNP
     * @field stSnpMarkers
     */
    stSnpMarkers: "stSnpMarkers", //
    /**
     * @description INDEL
     * @field stIndelMarkers
     */
    stIndelMarkers: "stIndelMarkers", //
    /**
     * @description Block
     * @field stBlockMarkers
     */
    stBlockMarkers: "stBlockMarkers", //
    /**
     * @description Tags
     * @field stTags
     */
    stTags: "stTags", //
    /**
     * @description Genotypes
     * @field stGenotypes
     */
    stGenotypes: "stGenotypes", //
    /**
     * @description Max
     * @field stMaxGenotypesFreq
     */
    stMaxGenotypesFreq: "stMaxGenotypesFreq", //
    /**
     * @description Min
     * @field stMinGenotypesFreq
     */
    stMinGenotypesFreq: "stMinGenotypesFreq", //
    /**
     * @description PIC
     * @field stPic
     */
    stPic: "stPic", //
    /**
     * @description Genetic
     * @field stGeneticMap
     */
    stGeneticMap: "stGeneticMap", //
    /**
     * @description ORDER
     * @field stOrder
     */
    stOrder: "stOrder", //
    /**
     * @description Column1
     * @field stColumn1
     */
    stColumn1: "stColumn1", //
    /**
     * @description Column2
     * @field stColumn2
     */
    stColumn2: "stColumn2", //
    /**
     * @description Column3
     * @field stColumn3
     */
    stColumn3: "stColumn3", //
    /**
     * @description Species
     * @field stSpecies
     */
    stSpecies: "stSpecies", //
    /**
     * @description Remark
     * @field stRemark
     */
    stRemark: "stRemark", //
    /**
     * @description 创建日期
     * @field stCreateDate
     */
    stCreateDate: "stCreateDate", //
    /**
     * @description 更新日期
     * @field stUpdateDate
     */
    stUpdateDate: "stUpdateDate", //
}
/**
 * ONP_STATISTICS-ONP统计数据表表信息
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpStatisticsTable
 */
export const OnpStatisticsTable = {
    /**
     * @description ST_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY: 'ST_ID',
    /**
     * @description id
     * @field primerKey
     */
    primerKey: 'stId',
    /**
     * @description ONP_STATISTICS
     * @field TABLE_NAME
     */
    TABLE_NAME: 'ONP_STATISTICS',
    /**
     * @description OnpStatistics
     * @field ENTITY_NAME
     */
    ENTITY_NAME: 'OnpStatistics',
}