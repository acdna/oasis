/**
 * ONP_FORECAST-预测表列名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecastColNames
 */
export interface OnpForecastColNames {
    /**
     * @description id
     * @field FO_ID
     */
    FO_ID: string, //
        /**
         * @description ID
         * @field FO_ORDER
         */
        FO_ORDER: number, //
        /**
         * @description Column1
         * @field FO_COLUMN1
         */
        FO_COLUMN1: string, //
        /**
         * @description Column2
         * @field FO_COLUMN2
         */
        FO_COLUMN2: string, //
        /**
         * @description Column3
         * @field FO_COLUMN3
         */
        FO_COLUMN3: string, //
        /**
         * @description Column1
         * @field FO_COLUMN4
         */
        FO_COLUMN4: string, //
        /**
         * @description Column2
         * @field FO_COLUMN5
         */
        FO_COLUMN5: string, //
        /**
         * @description Column3
         * @field FO_COLUMN6
         */
        FO_COLUMN6: string, //
        /**
         * @description Species
         * @field FO_SPECIES
         */
        FO_SPECIES: string, //
        /**
         * @description Remark
         * @field FO_REMARK
         */
        FO_REMARK: string, //
        /**
         * @description 创建日期
         * @field FO_CREATE_DATE
         */
        FO_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field FO_UPDATE_DATE
         */
        FO_UPDATE_DATE: Date, //
}
/**
 * ONP_FORECAST-预测表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecastColProps
 */
export interface OnpForecastColProps {
    /**
     * @description id
     * @field foId
     */
    foId: string, //
        /**
         * @description ID
         * @field foOrder
         */
        foOrder: number, //
        /**
         * @description Column1
         * @field foColumn1
         */
        foColumn1: string, //
        /**
         * @description Column2
         * @field foColumn2
         */
        foColumn2: string, //
        /**
         * @description Column3
         * @field foColumn3
         */
        foColumn3: string, //
        /**
         * @description Column1
         * @field foColumn4
         */
        foColumn4: string, //
        /**
         * @description Column2
         * @field foColumn5
         */
        foColumn5: string, //
        /**
         * @description Column3
         * @field foColumn6
         */
        foColumn6: string, //
        /**
         * @description Species
         * @field foSpecies
         */
        foSpecies: string, //
        /**
         * @description Remark
         * @field foRemark
         */
        foRemark: string, //
        /**
         * @description 创建日期
         * @field foCreateDate
         */
        foCreateDate: Date, //
        /**
         * @description 更新日期
         * @field foUpdateDate
         */
        foUpdateDate: Date, //
}
/**
 * ONP_FORECAST-预测表列名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecast
 */
export const OnpForecastColName = {
    /**
     * @description id
     * @field FO_ID
     */
    FO_ID: "FO_ID", //
    /**
     * @description ID
     * @field FO_ORDER
     */
    FO_ORDER: "FO_ORDER", //
    /**
     * @description Column1
     * @field FO_COLUMN1
     */
    FO_COLUMN1: "FO_COLUMN1", //
    /**
     * @description Column2
     * @field FO_COLUMN2
     */
    FO_COLUMN2: "FO_COLUMN2", //
    /**
     * @description Column3
     * @field FO_COLUMN3
     */
    FO_COLUMN3: "FO_COLUMN3", //
    /**
     * @description Column1
     * @field FO_COLUMN4
     */
    FO_COLUMN4: "FO_COLUMN4", //
    /**
     * @description Column2
     * @field FO_COLUMN5
     */
    FO_COLUMN5: "FO_COLUMN5", //
    /**
     * @description Column3
     * @field FO_COLUMN6
     */
    FO_COLUMN6: "FO_COLUMN6", //
    /**
     * @description Species
     * @field FO_SPECIES
     */
    FO_SPECIES: "FO_SPECIES", //
    /**
     * @description Remark
     * @field FO_REMARK
     */
    FO_REMARK: "FO_REMARK", //
    /**
     * @description 创建日期
     * @field FO_CREATE_DATE
     */
    FO_CREATE_DATE: "FO_CREATE_DATE", //
    /**
     * @description 更新日期
     * @field FO_UPDATE_DATE
     */
    FO_UPDATE_DATE: "FO_UPDATE_DATE", //
}
/**
 * ONP_FORECAST-预测表列属性名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecast
 */
export const OnpForecastColProp = {
    /**
     * @description id
     * @field foId
     */
    foId: "foId", //
    /**
     * @description ID
     * @field foOrder
     */
    foOrder: "foOrder", //
    /**
     * @description Column1
     * @field foColumn1
     */
    foColumn1: "foColumn1", //
    /**
     * @description Column2
     * @field foColumn2
     */
    foColumn2: "foColumn2", //
    /**
     * @description Column3
     * @field foColumn3
     */
    foColumn3: "foColumn3", //
    /**
     * @description Column1
     * @field foColumn4
     */
    foColumn4: "foColumn4", //
    /**
     * @description Column2
     * @field foColumn5
     */
    foColumn5: "foColumn5", //
    /**
     * @description Column3
     * @field foColumn6
     */
    foColumn6: "foColumn6", //
    /**
     * @description Species
     * @field foSpecies
     */
    foSpecies: "foSpecies", //
    /**
     * @description Remark
     * @field foRemark
     */
    foRemark: "foRemark", //
    /**
     * @description 创建日期
     * @field foCreateDate
     */
    foCreateDate: "foCreateDate", //
    /**
     * @description 更新日期
     * @field foUpdateDate
     */
    foUpdateDate: "foUpdateDate", //
}
/**
 * ONP_FORECAST-预测表信息
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpForecastTable
 */
export const OnpForecastTable = {
    /**
     * @description FO_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY: 'FO_ID',
    /**
     * @description id
     * @field primerKey
     */
    primerKey: 'foId',
    /**
     * @description ONP_FORECAST
     * @field TABLE_NAME
     */
    TABLE_NAME: 'ONP_FORECAST',
    /**
     * @description OnpForecast
     * @field ENTITY_NAME
     */
    ENTITY_NAME: 'OnpForecast',
}