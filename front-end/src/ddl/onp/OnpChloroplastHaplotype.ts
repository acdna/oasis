/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotypeColNames
 */
export interface OnpChloroplastHaplotypeColNames {
    /**
     * @description id
     * @field OCH_ID
     */
    OCH_ID: string, //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_HAPLOTYPE
         */
        OCH_CHLOROPLAST_HAPLOTYPE: string, //
        /**
         * @description Chr
         * @field OCH_CHR
         */
        OCH_CHR: string, //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_ONP
         */
        OCH_CHLOROPLAST_ONP: string, //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE
         */
        OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE: string, //
        /**
         * @description Frequency
         * @field OCH_FREQUENCY
         */
        OCH_FREQUENCY: string, //
        /**
         * @description Column1
         * @field OCH_COLUMN1
         */
        OCH_COLUMN1: string, //
        /**
         * @description Column2
         * @field OCH_COLUMN2
         */
        OCH_COLUMN2: string, //
        /**
         * @description Column3
         * @field OCH_COLUMN3
         */
        OCH_COLUMN3: string, //
        /**
         * @description ID
         * @field OCH_ORDER
         */
        OCH_ORDER: number, //
        /**
         * @description Species
         * @field OCH_SPECIES
         */
        OCH_SPECIES: string, //
        /**
         * @description Remark
         * @field OCH_REMARK
         */
        OCH_REMARK: string, //
        /**
         * @description 创建日期
         * @field OCH_CREATE_DATE
         */
        OCH_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field OCH_UPDATE_DATE
         */
        OCH_UPDATE_DATE: Date, //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotypeColProps
 */
export interface OnpChloroplastHaplotypeColProps {
    /**
     * @description id
     * @field ochId
     */
    ochId: string, //
        /**
         * @description Chloroplast
         * @field ochChloroplastHaplotype
         */
        ochChloroplastHaplotype: string, //
        /**
         * @description Chr
         * @field ochChr
         */
        ochChr: string, //
        /**
         * @description Chloroplast
         * @field ochChloroplastOnp
         */
        ochChloroplastOnp: string, //
        /**
         * @description Chloroplast
         * @field ochChloroplastHaplotypeSequence
         */
        ochChloroplastHaplotypeSequence: string, //
        /**
         * @description Frequency
         * @field ochFrequency
         */
        ochFrequency: string, //
        /**
         * @description Column1
         * @field ochColumn1
         */
        ochColumn1: string, //
        /**
         * @description Column2
         * @field ochColumn2
         */
        ochColumn2: string, //
        /**
         * @description Column3
         * @field ochColumn3
         */
        ochColumn3: string, //
        /**
         * @description ID
         * @field ochOrder
         */
        ochOrder: number, //
        /**
         * @description Species
         * @field ochSpecies
         */
        ochSpecies: string, //
        /**
         * @description Remark
         * @field ochRemark
         */
        ochRemark: string, //
        /**
         * @description 创建日期
         * @field ochCreateDate
         */
        ochCreateDate: Date, //
        /**
         * @description 更新日期
         * @field ochUpdateDate
         */
        ochUpdateDate: Date, //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列名
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotype
 */
export const OnpChloroplastHaplotypeColName = {
    /**
     * @description id
     * @field OCH_ID
     */
    OCH_ID: "OCH_ID", //
    /**
     * @description Chloroplast
     * @field OCH_CHLOROPLAST_HAPLOTYPE
     */
    OCH_CHLOROPLAST_HAPLOTYPE: "OCH_CHLOROPLAST_HAPLOTYPE", //
    /**
     * @description Chr
     * @field OCH_CHR
     */
    OCH_CHR: "OCH_CHR", //
    /**
     * @description Chloroplast
     * @field OCH_CHLOROPLAST_ONP
     */
    OCH_CHLOROPLAST_ONP: "OCH_CHLOROPLAST_ONP", //
    /**
     * @description Chloroplast
     * @field OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE
     */
    OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE: "OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE", //
    /**
     * @description Frequency
     * @field OCH_FREQUENCY
     */
    OCH_FREQUENCY: "OCH_FREQUENCY", //
    /**
     * @description Column1
     * @field OCH_COLUMN1
     */
    OCH_COLUMN1: "OCH_COLUMN1", //
    /**
     * @description Column2
     * @field OCH_COLUMN2
     */
    OCH_COLUMN2: "OCH_COLUMN2", //
    /**
     * @description Column3
     * @field OCH_COLUMN3
     */
    OCH_COLUMN3: "OCH_COLUMN3", //
    /**
     * @description ID
     * @field OCH_ORDER
     */
    OCH_ORDER: "OCH_ORDER", //
    /**
     * @description Species
     * @field OCH_SPECIES
     */
    OCH_SPECIES: "OCH_SPECIES", //
    /**
     * @description Remark
     * @field OCH_REMARK
     */
    OCH_REMARK: "OCH_REMARK", //
    /**
     * @description 创建日期
     * @field OCH_CREATE_DATE
     */
    OCH_CREATE_DATE: "OCH_CREATE_DATE", //
    /**
     * @description 更新日期
     * @field OCH_UPDATE_DATE
     */
    OCH_UPDATE_DATE: "OCH_UPDATE_DATE", //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列属性名
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotype
 */
export const OnpChloroplastHaplotypeColProp = {
    /**
     * @description id
     * @field ochId
     */
    ochId: "ochId", //
    /**
     * @description Chloroplast
     * @field ochChloroplastHaplotype
     */
    ochChloroplastHaplotype: "ochChloroplastHaplotype", //
    /**
     * @description Chr
     * @field ochChr
     */
    ochChr: "ochChr", //
    /**
     * @description Chloroplast
     * @field ochChloroplastOnp
     */
    ochChloroplastOnp: "ochChloroplastOnp", //
    /**
     * @description Chloroplast
     * @field ochChloroplastHaplotypeSequence
     */
    ochChloroplastHaplotypeSequence: "ochChloroplastHaplotypeSequence", //
    /**
     * @description Frequency
     * @field ochFrequency
     */
    ochFrequency: "ochFrequency", //
    /**
     * @description Column1
     * @field ochColumn1
     */
    ochColumn1: "ochColumn1", //
    /**
     * @description Column2
     * @field ochColumn2
     */
    ochColumn2: "ochColumn2", //
    /**
     * @description Column3
     * @field ochColumn3
     */
    ochColumn3: "ochColumn3", //
    /**
     * @description ID
     * @field ochOrder
     */
    ochOrder: "ochOrder", //
    /**
     * @description Species
     * @field ochSpecies
     */
    ochSpecies: "ochSpecies", //
    /**
     * @description Remark
     * @field ochRemark
     */
    ochRemark: "ochRemark", //
    /**
     * @description 创建日期
     * @field ochCreateDate
     */
    ochCreateDate: "ochCreateDate", //
    /**
     * @description 更新日期
     * @field ochUpdateDate
     */
    ochUpdateDate: "ochUpdateDate", //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表信息
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastHaplotypeTable
 */
export const OnpChloroplastHaplotypeTable = {
    /**
     * @description OCH_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY: 'OCH_ID',
    /**
     * @description id
     * @field primerKey
     */
    primerKey: 'ochId',
    /**
     * @description ONP_CHLOROPLAST_HAPLOTYPE
     * @field TABLE_NAME
     */
    TABLE_NAME: 'ONP_CHLOROPLAST_HAPLOTYPE',
    /**
     * @description OnpChloroplastHaplotype
     * @field ENTITY_NAME
     */
    ENTITY_NAME: 'OnpChloroplastHaplotype',
}