/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpChloroplastOverallColNames
 */
export interface OnpChloroplastOverallColNames {
    /**
     * @description ID
     * @field OCO_ID
     */
    OCO_ID: string, //
        /**
         * @description ONP ID
         * @field OCO_ONP_ID
         */
        OCO_ONP_ID: string, //
        /**
         * @description Chr
         * @field OCO_CHR
         */
        OCO_CHR: string, //
        /**
         * @description Start
         * @field OCO_START
         */
        OCO_START: string, //
        /**
         * @description Stop
         * @field OCO_STOP
         */
        OCO_STOP: string, //
        /**
         * @description ONP
         * @field OCO_ONP_SIZE
         */
        OCO_ONP_SIZE: string, //
        /**
         * @description Location
         * @field OCO_LOCATION
         */
        OCO_LOCATION: string, //
        /**
         * @description ALL
         * @field OCO_ALL_MARKERS
         */
        OCO_ALL_MARKERS: string, //
        /**
         * @description SNP
         * @field OCO_SNP_MARKERS
         */
        OCO_SNP_MARKERS: string, //
        /**
         * @description INDEL
         * @field OCO_INDEL_MARKERS
         */
        OCO_INDEL_MARKERS: string, //
        /**
         * @description Block
         * @field OCO_BLOCK_MARKERS
         */
        OCO_BLOCK_MARKERS: string, //
        /**
         * @description Tags
         * @field OCO_TAGS
         */
        OCO_TAGS: string, //
        /**
         * @description Genotypes
         * @field OCO_GENOTYPES
         */
        OCO_GENOTYPES: string, //
        /**
         * @description Max
         * @field OCO_MAX_GENOTYPES_FREQ
         */
        OCO_MAX_GENOTYPES_FREQ: string, //
        /**
         * @description Min
         * @field OCO_MIN_GENOTYPES_FREQ
         */
        OCO_MIN_GENOTYPES_FREQ: string, //
        /**
         * @description PIC
         * @field OCO_PIC
         */
        OCO_PIC: string, //
        /**
         * @description Column1
         * @field OCO_COLUMN1
         */
        OCO_COLUMN1: string, //
        /**
         * @description Column2
         * @field OCO_COLUMN2
         */
        OCO_COLUMN2: string, //
        /**
         * @description Column3
         * @field OCO_COLUMN3
         */
        OCO_COLUMN3: string, //
        /**
         * @description ID
         * @field OCO_ORDER
         */
        OCO_ORDER: number, //
        /**
         * @description Species
         * @field OCO_SPECIES
         */
        OCO_SPECIES: string, //
        /**
         * @description Remark
         * @field OCO_REMARK
         */
        OCO_REMARK: string, //
        /**
         * @description 创建日期
         * @field OCO_CREATE_DATE
         */
        OCO_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field OCO_UPDATE_DATE
         */
        OCO_UPDATE_DATE: Date, //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpChloroplastOverallColProps
 */
export interface OnpChloroplastOverallColProps {
    /**
     * @description ID
     * @field ocoId
     */
    ocoId: string, //
        /**
         * @description ONP ID
         * @field ocoOnpId
         */
        ocoOnpId: string, //
        /**
         * @description Chr
         * @field ocoChr
         */
        ocoChr: string, //
        /**
         * @description Start
         * @field ocoStart
         */
        ocoStart: string, //
        /**
         * @description Stop
         * @field ocoStop
         */
        ocoStop: string, //
        /**
         * @description ONP
         * @field ocoOnpSize
         */
        ocoOnpSize: string, //
        /**
         * @description Location
         * @field ocoLocation
         */
        ocoLocation: string, //
        /**
         * @description ALL
         * @field ocoAllMarkers
         */
        ocoAllMarkers: string, //
        /**
         * @description SNP
         * @field ocoSnpMarkers
         */
        ocoSnpMarkers: string, //
        /**
         * @description INDEL
         * @field ocoIndelMarkers
         */
        ocoIndelMarkers: string, //
        /**
         * @description Block
         * @field ocoBlockMarkers
         */
        ocoBlockMarkers: string, //
        /**
         * @description Tags
         * @field ocoTags
         */
        ocoTags: string, //
        /**
         * @description Genotypes
         * @field ocoGenotypes
         */
        ocoGenotypes: string, //
        /**
         * @description Max
         * @field ocoMaxGenotypesFreq
         */
        ocoMaxGenotypesFreq: string, //
        /**
         * @description Min
         * @field ocoMinGenotypesFreq
         */
        ocoMinGenotypesFreq: string, //
        /**
         * @description PIC
         * @field ocoPic
         */
        ocoPic: string, //
        /**
         * @description Column1
         * @field ocoColumn1
         */
        ocoColumn1: string, //
        /**
         * @description Column2
         * @field ocoColumn2
         */
        ocoColumn2: string, //
        /**
         * @description Column3
         * @field ocoColumn3
         */
        ocoColumn3: string, //
        /**
         * @description ID
         * @field ocoOrder
         */
        ocoOrder: number, //
        /**
         * @description Species
         * @field ocoSpecies
         */
        ocoSpecies: string, //
        /**
         * @description Remark
         * @field ocoRemark
         */
        ocoRemark: string, //
        /**
         * @description 创建日期
         * @field ocoCreateDate
         */
        ocoCreateDate: Date, //
        /**
         * @description 更新日期
         * @field ocoUpdateDate
         */
        ocoUpdateDate: Date, //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列名
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpChloroplastOverall
 */
export const OnpChloroplastOverallColName = {
    /**
     * @description ID
     * @field OCO_ID
     */
    OCO_ID: "OCO_ID", //
    /**
     * @description ONP ID
     * @field OCO_ONP_ID
     */
    OCO_ONP_ID: "OCO_ONP_ID", //
    /**
     * @description Chr
     * @field OCO_CHR
     */
    OCO_CHR: "OCO_CHR", //
    /**
     * @description Start
     * @field OCO_START
     */
    OCO_START: "OCO_START", //
    /**
     * @description Stop
     * @field OCO_STOP
     */
    OCO_STOP: "OCO_STOP", //
    /**
     * @description ONP
     * @field OCO_ONP_SIZE
     */
    OCO_ONP_SIZE: "OCO_ONP_SIZE", //
    /**
     * @description Location
     * @field OCO_LOCATION
     */
    OCO_LOCATION: "OCO_LOCATION", //
    /**
     * @description ALL
     * @field OCO_ALL_MARKERS
     */
    OCO_ALL_MARKERS: "OCO_ALL_MARKERS", //
    /**
     * @description SNP
     * @field OCO_SNP_MARKERS
     */
    OCO_SNP_MARKERS: "OCO_SNP_MARKERS", //
    /**
     * @description INDEL
     * @field OCO_INDEL_MARKERS
     */
    OCO_INDEL_MARKERS: "OCO_INDEL_MARKERS", //
    /**
     * @description Block
     * @field OCO_BLOCK_MARKERS
     */
    OCO_BLOCK_MARKERS: "OCO_BLOCK_MARKERS", //
    /**
     * @description Tags
     * @field OCO_TAGS
     */
    OCO_TAGS: "OCO_TAGS", //
    /**
     * @description Genotypes
     * @field OCO_GENOTYPES
     */
    OCO_GENOTYPES: "OCO_GENOTYPES", //
    /**
     * @description Max
     * @field OCO_MAX_GENOTYPES_FREQ
     */
    OCO_MAX_GENOTYPES_FREQ: "OCO_MAX_GENOTYPES_FREQ", //
    /**
     * @description Min
     * @field OCO_MIN_GENOTYPES_FREQ
     */
    OCO_MIN_GENOTYPES_FREQ: "OCO_MIN_GENOTYPES_FREQ", //
    /**
     * @description PIC
     * @field OCO_PIC
     */
    OCO_PIC: "OCO_PIC", //
    /**
     * @description Column1
     * @field OCO_COLUMN1
     */
    OCO_COLUMN1: "OCO_COLUMN1", //
    /**
     * @description Column2
     * @field OCO_COLUMN2
     */
    OCO_COLUMN2: "OCO_COLUMN2", //
    /**
     * @description Column3
     * @field OCO_COLUMN3
     */
    OCO_COLUMN3: "OCO_COLUMN3", //
    /**
     * @description ID
     * @field OCO_ORDER
     */
    OCO_ORDER: "OCO_ORDER", //
    /**
     * @description Species
     * @field OCO_SPECIES
     */
    OCO_SPECIES: "OCO_SPECIES", //
    /**
     * @description Remark
     * @field OCO_REMARK
     */
    OCO_REMARK: "OCO_REMARK", //
    /**
     * @description 创建日期
     * @field OCO_CREATE_DATE
     */
    OCO_CREATE_DATE: "OCO_CREATE_DATE", //
    /**
     * @description 更新日期
     * @field OCO_UPDATE_DATE
     */
    OCO_UPDATE_DATE: "OCO_UPDATE_DATE", //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列属性名
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @class OnpChloroplastOverall
 */
export const OnpChloroplastOverallColProp = {
    /**
     * @description ID
     * @field ocoId
     */
    ocoId: "ocoId", //
    /**
     * @description ONP ID
     * @field ocoOnpId
     */
    ocoOnpId: "ocoOnpId", //
    /**
     * @description Chr
     * @field ocoChr
     */
    ocoChr: "ocoChr", //
    /**
     * @description Start
     * @field ocoStart
     */
    ocoStart: "ocoStart", //
    /**
     * @description Stop
     * @field ocoStop
     */
    ocoStop: "ocoStop", //
    /**
     * @description ONP
     * @field ocoOnpSize
     */
    ocoOnpSize: "ocoOnpSize", //
    /**
     * @description Location
     * @field ocoLocation
     */
    ocoLocation: "ocoLocation", //
    /**
     * @description ALL
     * @field ocoAllMarkers
     */
    ocoAllMarkers: "ocoAllMarkers", //
    /**
     * @description SNP
     * @field ocoSnpMarkers
     */
    ocoSnpMarkers: "ocoSnpMarkers", //
    /**
     * @description INDEL
     * @field ocoIndelMarkers
     */
    ocoIndelMarkers: "ocoIndelMarkers", //
    /**
     * @description Block
     * @field ocoBlockMarkers
     */
    ocoBlockMarkers: "ocoBlockMarkers", //
    /**
     * @description Tags
     * @field ocoTags
     */
    ocoTags: "ocoTags", //
    /**
     * @description Genotypes
     * @field ocoGenotypes
     */
    ocoGenotypes: "ocoGenotypes", //
    /**
     * @description Max
     * @field ocoMaxGenotypesFreq
     */
    ocoMaxGenotypesFreq: "ocoMaxGenotypesFreq", //
    /**
     * @description Min
     * @field ocoMinGenotypesFreq
     */
    ocoMinGenotypesFreq: "ocoMinGenotypesFreq", //
    /**
     * @description PIC
     * @field ocoPic
     */
    ocoPic: "ocoPic", //
    /**
     * @description Column1
     * @field ocoColumn1
     */
    ocoColumn1: "ocoColumn1", //
    /**
     * @description Column2
     * @field ocoColumn2
     */
    ocoColumn2: "ocoColumn2", //
    /**
     * @description Column3
     * @field ocoColumn3
     */
    ocoColumn3: "ocoColumn3", //
    /**
     * @description ID
     * @field ocoOrder
     */
    ocoOrder: "ocoOrder", //
    /**
     * @description Species
     * @field ocoSpecies
     */
    ocoSpecies: "ocoSpecies", //
    /**
     * @description Remark
     * @field ocoRemark
     */
    ocoRemark: "ocoRemark", //
    /**
     * @description 创建日期
     * @field ocoCreateDate
     */
    ocoCreateDate: "ocoCreateDate", //
    /**
     * @description 更新日期
     * @field ocoUpdateDate
     */
    ocoUpdateDate: "ocoUpdateDate", //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表信息
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastOverallTable
 */
export const OnpChloroplastOverallTable = {
    /**
     * @description OCO_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY: 'OCO_ID',
    /**
     * @description ID
     * @field primerKey
     */
    primerKey: 'ocoId',
    /**
     * @description ONP_CHLOROPLAST_OVERALL
     * @field TABLE_NAME
     */
    TABLE_NAME: 'ONP_CHLOROPLAST_OVERALL',
    /**
     * @description OnpChloroplastOverall
     * @field ENTITY_NAME
     */
    ENTITY_NAME: 'OnpChloroplastOverall',
}