import { PmsInfo } from '@/common/pms/pms.info';
import { SYS_BILLING_CARD_PMS } from '@/permission/sys/sys.billing.card.pms';
import { SYS_CACHE_TRACE_PMS } from '@/permission/sys/sys.cache.trace.pms';
import { SYS_CMS_FILES_PMS } from '@/permission/sys/sys.cms.files.pms';
import { SYS_CMS_NOTICE_PMS } from '@/permission/sys/sys.cms.notice.pms';
import { SYS_COL_DETAIL_PMS } from '@/permission/sys/sys.col.detail.pms';
import { SYS_CONFIG_PMS } from '@/permission/sys/sys.config.pms';
import { SYS_CONTACT_PMS } from '@/permission/sys/sys.contact.pms';
import { SYS_DICTIONARY_PMS } from '@/permission/sys/sys.dictionary.pms';
import { SYS_FILES_PMS } from '@/permission/sys/sys.files.pms';
import { SYS_HELP_PMS } from '@/permission/sys/sys.help.pms';
import { SYS_INSTRUMENT_PMS } from '@/permission/sys/sys.instrument.pms';
import { SYS_LOG_PMS } from '@/permission/sys/sys.log.pms';
import { SYS_MEMORY_PMS } from '@/permission/sys/sys.memory.pms';
import { SYS_MENU_PMS } from '@/permission/sys/sys.menu.pms';
import { SYS_NOTICE_PMS } from '@/permission/sys/sys.notice.pms';
import { SYS_PERMISSION_PMS } from '@/permission/sys/sys.permission.pms';
import { SYS_ROLE_PMS } from '@/permission/sys/sys.role.pms';
import { SYS_ROLE_DICTIONARY_PMS } from '@/permission/sys/sys.role.dictionary.pms';
import { SYS_ROLE_MENU_PMS } from '@/permission/sys/sys.role.menu.pms';
import { SYS_ROLE_PERMISSION_PMS } from '@/permission/sys/sys.role.permission.pms';
import { SYS_TIP_PMS } from '@/permission/sys/sys.tip.pms';
import { SYS_UPDATE_PMS } from '@/permission/sys/sys.update.pms';
import { SYS_USER_PMS } from '@/permission/sys/sys.user.pms';
import { SYS_USER_ROLE_PMS } from '@/permission/sys/sys.user.role.pms';
import { SYS_USER_TOP_UP_PMS } from '@/permission/sys/sys.user.top.up.pms';
/**
 * SYS模块页面权限注册类，在权限注册功能里会将本类中定义的所有页面权限写入数据库，以便在页面权限授权功能里提供自由授权的机制
 * @date 1/25/2021, 9:00:42 AM
 * @author jiangbin
 **/
export const SYS_PMS: PmsInfo[] = [
    SYS_BILLING_CARD_PMS,
    SYS_CACHE_TRACE_PMS,
    SYS_CMS_FILES_PMS,
    SYS_CMS_NOTICE_PMS,
    SYS_COL_DETAIL_PMS,
    SYS_CONFIG_PMS,
    SYS_CONTACT_PMS,
    SYS_DICTIONARY_PMS,
    SYS_FILES_PMS,
    SYS_HELP_PMS,
    SYS_INSTRUMENT_PMS,
    SYS_LOG_PMS,
    SYS_MEMORY_PMS,
    SYS_MENU_PMS,
    SYS_NOTICE_PMS,
    SYS_PERMISSION_PMS,
    SYS_ROLE_PMS,
    SYS_ROLE_DICTIONARY_PMS,
    SYS_ROLE_MENU_PMS,
    SYS_ROLE_PERMISSION_PMS,
    SYS_TIP_PMS,
    SYS_UPDATE_PMS,
    SYS_USER_PMS,
    SYS_USER_ROLE_PMS,
    SYS_USER_TOP_UP_PMS,
];