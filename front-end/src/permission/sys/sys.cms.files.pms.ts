import { PmsRoot } from '@/common/pms/pms.info';
/**
 * SYS_CMS_FILES-页面权限注册类节点信息
 * @author jiangbin
 * @date 1/25/2021, 9:00:42 AM
 **/
export const SYS_CMS_FILES_PMS_ROOT: PmsRoot = {
    system: 'FRONT_END',
    group: 'SYS_CMS_FILES',
    name: 'SYS_CMS_FILES',
    nameEn: 'SYS_CMS_FILES',
    type: 'front-end',
};
/**
 * SYS_CMS_FILES-页面权限注册类，在权限注册功能里会将本类中定义的所有页面权限写入数据库，以便在页面权限授权功能里提供自由授权的机制
 * @author jiangbin
 * @date 1/25/2021, 9:00:42 AM
 **/
export const SYS_CMS_FILES_PMS = {
    QUERY: { id: 'QUERY', name: '查询记录列表', nameEn: 'Query record list', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    QUERY_ALL: { id: 'QUERY_ALL', name: '查询所有记录列表', nameEn: 'Query all records', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    CREATE: { id: 'CREATE', name: '创建记录', nameEn: 'Create a record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    DEL_ONE: { id: 'DEL_ONE', name: '删除记录', nameEn: 'Delete a record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    DEL_LIST: { id: 'DEL_LIST', name: '批量删除记录', nameEn: 'Batch delete records', enable: true, root: SYS_CMS_FILES_PMS_ROOT, },
    UPDATE: { id: 'UPDATE', name: '更新记录', nameEn: 'Update record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    CREATE_ITEM: { id: 'CREATE_ITEM', name: '创建子项记录', nameEn: 'Create sub record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    UPDATE_ITEM: { id: 'UPDATE_ITEM', name: '更新子项记录', nameEn: 'Update sub record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    UPLOAD: { id: 'UPLOAD', name: '上传记录', nameEn: 'Upload record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    EXPORT: { id: 'EXPORT', name: '导出记录', nameEn: 'Export record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    BATCH_UPLOAD: { id: 'BATCH_UPLOAD', name: '批量上传记录', nameEn: 'Batch upload records', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    EXPORT_ALL: { id: 'EXPORT_ALL', name: '导出所有记录', nameEn: 'Export all records', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    INIT: { id: 'INIT', name: '初始化记录', nameEn: 'Init records', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    INIT_FRONT: { id: 'INIT_FRONT', name: '初始化前端记录', nameEn: 'Init front record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
    INIT_BACK: { id: 'INIT_BACK', name: '初始化后端记录', nameEn: 'Init back record', enable: true, root: SYS_CMS_FILES_PMS_ROOT },
};