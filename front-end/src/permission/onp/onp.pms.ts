import { PmsInfo } from '@/common/pms/pms.info';
import { ONP_FORECAST_PMS } from '@/permission/onp/onp.forecast.pms';
import { ONP_STATISTICS_PMS } from '@/permission/onp/onp.statistics.pms';
import { ONP_VERIFICATION_PMS } from '@/permission/onp/onp.verification.pms';
import {ONP_CHLOROPLAST_OVERALL_PMS} from "@/permission/onp/onp.chloroplast.overall.pms";
import {ONP_CHLOROPLAST_HAPLOTYPE_PMS} from "@/permission/onp/onp.chloroplast.haplotype.pms";
import {ONP_NUCLEUS_HAPLOTYPE_PMS} from "@/permission/onp/onp.nucleus.haplotype.pms";
/**
 * ONP模块页面权限注册类，在权限注册功能里会将本类中定义的所有页面权限写入数据库，以便在页面权限授权功能里提供自由授权的机制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 **/
export const ONP_PMS: PmsInfo[] = [
    ONP_FORECAST_PMS,
    ONP_STATISTICS_PMS,
    ONP_VERIFICATION_PMS,
    ONP_CHLOROPLAST_HAPLOTYPE_PMS,
    ONP_CHLOROPLAST_OVERALL_PMS,
    ONP_NUCLEUS_HAPLOTYPE_PMS,
];
