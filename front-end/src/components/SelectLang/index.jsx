import { Menu } from 'antd';
import { connect, getLocale, setLocale } from 'umi';
import React, { useEffect } from 'react';
import classNames from 'classnames';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import { AuthContext } from '@/common/auth/auth.context';
import { LocaleContext } from '@/common/locales/locale';

/**
 * 设置系统右上角的语言菜单
 * @param props
 * @return {JSX.Element}
 * @constructor
 */
const SelectLang = (props) => {
  const { className } = props;
  const selectedLang = getLocale();

  useEffect(() => {
    AuthContext.isLogin && LocaleContext.setLocale();
  }, []);

  /**
   * 修改当前页面语言
   * @param key
   */
  const changeLang = ({ key }) => {
    //更新用户的语言
    AuthContext.isLogin && LocaleContext.setLocale(key);
    //切换页面语言
    setLocale(key);
  };

  const locales = ['zh-CN', 'en-US'];
  const languageLabels = {
    'zh-CN': '简体中文',
    'en-US': 'English',
  };
  const languageIcons = {
    'zh-CN': '🇨🇳',
    'en-US': '🇺🇸',
  };
  const langMenu = (
    <Menu className={styles.menu} selectedKeys={[selectedLang]} onClick={changeLang}>
      {locales.map((locale) => (
        <Menu.Item key={locale}>
          <span role="img" aria-label={languageLabels[locale]}>
            {languageIcons[locale]}
          </span>{' '}
          {languageLabels[locale]}
        </Menu.Item>
      ))}
    </Menu>
  );
  return (
    <HeaderDropdown overlay={langMenu} placement="bottomRight">
      <span className={classNames(styles.dropDown, className)}>
        {languageIcons[selectedLang]} {languageLabels[selectedLang]}
      </span>
    </HeaderDropdown>
  );
};

const mapStateToProps = ({ global }) => {
  return { defaultLocale: global.defaultLocale };
};

export default connect(mapStateToProps)(SelectLang);
