import { LogoutOutlined, SettingOutlined, TeamOutlined, ToolOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Menu, Spin } from 'antd';
import React from 'react';
import { connect, ConnectProps, history } from 'umi';
import { CurrentUser } from '@/models/user';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import avatarImg from '../../assets/avatar.png';
import { getLocaleMessage } from '@/common/locales/locale';
import { FormattedMessage } from '@@/plugin-locale/localeExports';
import { GlobalHeaderHandles } from '@/components/GlobalHeader/handles';
import { ConnectState } from '@/models/connect';
import ChangePasswordModal from '@/components/GlobalHeader/change.password';
import SystemInfoModal from '@/components/GlobalHeader/system.info';

export interface GlobalHeaderRightProps extends Partial<ConnectProps> {
  currentUser?: CurrentUser;
  menu?: boolean;
  user?: boolean;
}

/**
 * 顶部最右边的下拉菜单
 * @author: jiangbin
 * @date: 2020-12-09 20:17:50
 **/
class AvatarDropdown extends React.Component<GlobalHeaderRightProps> {
  private handles: GlobalHeaderHandles;

  constructor(props: any) {
    super(props);
    this.handles = new GlobalHeaderHandles();
    this.state = { centerVisible: false, settingsVisible: false, passwordVisible: false, systemVisible: false };
  }

  onMenuClick = (event: {
    key: React.Key;
    keyPath: React.Key[];
    item: React.ReactInstance;
    domEvent: React.MouseEvent<HTMLElement>;
  }) => {
    const { key } = event;

    //用户登出
    if (key === 'logout') {
      const { dispatch } = this.props;

      if (dispatch) {
        dispatch({
          type: 'login/logout',
        });
      }

      return;
    }

    //用户中心
    if (key === 'center') {
      this.setState({ centerVisible: true });
      return;
    }
    //设置用户
    if (key === 'settings') {
      this.setState({ settingsVisible: true });
      return;
    }
    //修改密码
    if (key === 'password') {
      this.setState({ passwordVisible: true });
      return;
    }
    //系统信息
    if (key === 'system') {
      this.setState({ systemVisible: true });
      return;
    }

    history.push(`/account/${key}`);
  };

  render(): React.ReactNode {
    const { currentUser = { avatar: '', name: '' }, menu = true, user = false } = this.props;
    const menuHeaderDropdown = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.onMenuClick}>
        {user && (
          <Menu.Item key="center">
            <UserOutlined />
            <FormattedMessage id='global.header.avatar.user.center' defaultMessage='个人中心' />
          </Menu.Item>
        )}
        {user && (
          <Menu.Item key="settings">
            <ToolOutlined />
            <FormattedMessage id='global.header.avatar.user.setting' defaultMessage='个人设置' />
          </Menu.Item>
        )}
        {menu && (
          <Menu.Item key="password">
            <SettingOutlined />
            <FormattedMessage id='global.header.avatar.change.password' defaultMessage='修改密码' />
          </Menu.Item>
        )}
        {menu && <Menu.Divider />}
        {menu && (
          <Menu.Item key="system">
            <TeamOutlined />
            <FormattedMessage id='global.header.avatar.system.info' defaultMessage='系统信息' />
          </Menu.Item>
        )}
        {menu && <Menu.Divider />}

        <Menu.Item key="logout">
          <LogoutOutlined />
          <FormattedMessage id='global.header.avatar.logout' defaultMessage='退出登录' />
        </Menu.Item>
      </Menu>
    );
    return (
      <>
        <ChangePasswordModal visible={this.state['passwordVisible']}
                             onSubmit={async (values) => {
                               let success = await this.handles.changePassword(values);
                               if (success) {
                                 this.setState({ passwordVisible: false });
                               }
                             }}
                             onCancel={async () => {
                               this.setState({ passwordVisible: false });
                             }} />

        <SystemInfoModal visible={this.state['systemVisible']}
                         onCancel={async () => {
                           this.setState({ systemVisible: false });
                         }} />

        {(currentUser && currentUser['userName']) ? (
          <HeaderDropdown overlay={menuHeaderDropdown}>
            <span className={`${styles.action} ${styles.account}`}>
              <Avatar size="small" className={styles.avatar} src={currentUser.avatar || avatarImg} alt="avatar" />
              <span className={`${styles.name} anticon`}>
                {getLocaleMessage({
                  cn: currentUser['userName'],
                  en: currentUser['userNameEn'],
                }) || currentUser['userName']}</span>
            </span>
          </HeaderDropdown>
        ) : (
          <span className={`${styles.action} ${styles.account}`}>
          <Spin size="small" style={{ marginLeft: 8, marginRight: 8 }} />
      </span>
        )}
      </>
    );
  }
}

export default connect(({ user }: ConnectState) => ({
  currentUser: user.currentUser,
}))(AvatarDropdown);
