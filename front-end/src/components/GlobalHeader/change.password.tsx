import { useForm } from 'antd/es/form/Form';
import { FormattedMessage } from 'umi';
import ProForm from '@ant-design/pro-form';
import { Form, Input, Modal } from 'antd';
import React from 'react';
import { useIntl } from '@@/plugin-locale/localeExports';
import { AuthContext } from '@/common/auth/auth.context';
import { SYS_USER_VALIDATOR } from '@/pages/sys/user/validator';
import { RuleObject, StoreValue } from 'rc-field-form/lib/interface';

export interface PasswordData {
  userId: string,
  userPassword: string,
  userPasswordNew: string,
  userPasswordRepeat: string,
}

export interface ChangePasswordProps {
  visible: boolean;
  onSubmit: (fields: PasswordData | Partial<PasswordData>) => void,
  onCancel: () => void;
}

/**
 * 修改用户密码
 * @author: jiangbin
 * @date: 2021-01-25 17:56:40
 **/
const ChangePasswordModal: React.FC<ChangePasswordProps> = (props) => {
  const { visible, onSubmit, onCancel } = props;
  const intl = useIntl();
  const [form] = useForm();

  /**
   * 提交按钮
   * @author: jiangbin
   * @date: 2021-01-25 17:22:48
   **/
  const handleOk = async () => {
    const values = await form.validateFields();
    onSubmit({ userId: AuthContext.id, ...values });
  };

  /**
   * 取消按钮
   * @author: jiangbin
   * @date: 2021-01-25 17:22:51
   **/
  const handleCancel = async () => {
    await form.resetFields();
    onCancel();
  };

  return (
    <Modal
      destroyOnClose
      title={
        <FormattedMessage id='pages.change.password.form.title' defaultMessage='修改密码' />
      }
      visible={visible}
      onCancel={() => handleCancel()}
      onOk={() => handleOk()}
    >
      <ProForm form={form} submitter={false}>
        <Form.Item
          name="userPassword"
          label={<FormattedMessage id="pages.change.password.userPassword.label" defaultMessage="原密码" />}
          rules={[
            {
              min: 6,
              max: 24,
              message: (intl.formatMessage({
                id: 'pages.change.password.userPassword.length.tip',
                defaultMessage: '密码长度必须为6~24个字符!',
              })),
            },
            {
              required: true,
              message: (<FormattedMessage id="pages.change.password.userPassword.tip" defaultMessage="原密码" />),
            },
            SYS_USER_VALIDATOR.USER_PASSWORD(intl.formatMessage({
              id: 'pages.change.password.userPassword.right.tip',
              defaultMessage: '密码不正确!',
            }), { userId: AuthContext.id }),
          ]}
        >
          <Input.Password placeholder={intl.formatMessage({
            id: 'pages.change.password.userPassword.placeholder',
            defaultMessage: '原密码',
          })} style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item
          name="userPasswordNew"
          label={<FormattedMessage id="pages.change.password.userPasswordNew.label" defaultMessage="新密码" />}
          rules={[
            {
              min: 6,
              max: 24,
              message: (intl.formatMessage({
                id: 'pages.change.password.userPassword.length.tip',
                defaultMessage: '密码长度必须为6~24个字符!',
              })),
            },
            {
              required: true,
              message: (<FormattedMessage id="pages.change.password.userPasswordNew.tip" defaultMessage="新密码" />),
            },
          ]}
        >
          <Input.Password placeholder={intl.formatMessage({
            id: 'pages.change.password.userPasswordNew.placeholder',
            defaultMessage: '新密码',
          })} style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item
          name="userPasswordRepeat"
          label={<FormattedMessage id="pages.change.password.userPasswordRepeat.label" defaultMessage="确认新密码" />}
          rules={[
            {
              min: 6,
              max: 24,
              message: (intl.formatMessage({
                id: 'pages.change.password.userPassword.length.tip',
                defaultMessage: '密码长度必须为6~24个字符!',
              })),
            },
            {
              required: true,
              message: (<FormattedMessage id="pages.change.password.userPasswordRepeat.tip" defaultMessage="确认新密码" />),
            },
            {
              validator: (rule: RuleObject, value: StoreValue, callback: (error ?: string) => void) => {
                let source = form.getFieldValue('userPasswordNew');
                return (source === value) ? Promise.resolve() : Promise.reject(
                  intl.formatMessage({
                    id: 'pages.change.password.userPasswordRepeat.same.tip',
                    defaultMessage: '输入值与新密码不一致，请重新输入!',
                  }));
              },
            },
          ]}
        >
          <Input.Password placeholder={intl.formatMessage({
            id: 'pages.change.password.userPasswordRepeat.placeholder',
            defaultMessage: '确认新密码',
          })} style={{ width: '100%' }} />
        </Form.Item>
      </ProForm>
    </Modal>
  );
};

export default ChangePasswordModal;
