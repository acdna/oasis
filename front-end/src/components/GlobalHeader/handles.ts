import { message } from 'antd';
import { PasswordData } from '@/components/GlobalHeader/change.password';
import { SysUserService } from '@/services/sys/sys.user.service';
import { LocaleContext } from '@/common/locales/locale';
import { AuthContext } from '@/common/auth/auth.context';

/**
 * GlobalHeader的菜单操作句柄方法
 * @author jiangbin
 * @date 12/22/2020, 2:34:07 PM
 **/
export class GlobalHeaderHandles {
  /**
   * 添加记录
   * @param fields
   */
  changePassword = async (fields: PasswordData | Partial<PasswordData>) => {
    const hide = message.loading(LocaleContext.message({
      id: 'page.change.password.loading',
      defaultMessage: '正在修改密码',
    }));
    try {
      let newPassword = AuthContext.md5(fields.userPasswordNew || '');
      await SysUserService.update({
        cols: ['userPassword'],
        userPassword: newPassword,
        userId: fields.userId,
      });
      hide();
      message.success(LocaleContext.message({
        id: 'page.change.password.success',
        defaultMessage: '修改密码成功',
      }));
      return true;
    } catch (error) {
      hide();
      message.error(LocaleContext.message({
        id: 'page.change.password.error',
        defaultMessage: '修改密码失败请重试！',
      }));
      return false;
    }
  };
};
