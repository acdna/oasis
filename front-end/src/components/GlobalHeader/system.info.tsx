import {Button, Descriptions, Image, Modal} from 'antd';
import {getLocaleMessage, LocaleContext} from '@/common/locales/locale';
import {useEffect, useState} from 'react';
import {FormattedMessage} from '@@/plugin-locale/localeExports';
import {SysConfigService} from '@/services/sys/sys.config.service';
import {CloseCircleOutlined} from '@ant-design/icons';
import ymslogo from '../../../public/ymslogo.png';
import {AuthContext} from "@/common/auth/auth.context";

interface SystemInfoModalProps {
  visible: boolean;
  onCancel: () => void;
}

/**
 * 显示系统信息
 * @author: jiangbin
 * @date: 2021-01-26 09:24:25
 **/
const SystemInfoModal: React.FC<SystemInfoModalProps> = (props) => {
  const {visible, onCancel} = props;
  const [version, setVersion] = useState<string>('v1.0.0');

  useEffect(() => {
    (async () => {
      const sysVersion = await SysConfigService.version();
      setVersion(sysVersion);
    })();
  });

  const handleCancel = () => {
    onCancel();
  };

  const contents = (
    <>
      <Descriptions size={'middle'} bordered column={1}>
        <Descriptions.Item
          label={
            <div style={{width: '120px'}}>
              {LocaleContext.message({id: 'pages.system.info.technical.support.label', defaultMessage: '技术支持'})}
            </div>
          }>
          <FormattedMessage id='pages.system.info.technical.support.address' defaultMessage='北京玉米种子检测中心软件研发室'/>
        </Descriptions.Item>
        <Descriptions.Item
          label={LocaleContext.message({id: 'pages.system.info.address.label', defaultMessage: '地址'})}>
          <FormattedMessage id={'pages.system.info.dept.address'} defaultMessage={'北京市海淀区曙光花园中路9号北京市农林科学院玉米研究中心'}/>
        </Descriptions.Item>
        <Descriptions.Item label={'Email'}>
          <div><a href="mailto:holibut@gmail.com">holibut@gmail.com</a></div>
          <div><a href="mailto:j13311151986@gmail.com">j13311151986@gmail.com</a></div>
        </Descriptions.Item>
        <Descriptions.Item
          label={LocaleContext.message({id: 'pages.system.info.iphone.label', defaultMessage: '联系电话'})}>
          <div>{'010 - 51503710'}</div>
          <div>{'010 - 51503986'}</div>
        </Descriptions.Item>
        <Descriptions.Item
          label={LocaleContext.message({id: 'pages.system.info.copyright.label', defaultMessage: '版权'})}>
          <div style={{lineHeight: '100%'}}>
            <div style={{width: '50px', height: '50px', float: 'left', marginRight: '10px'}}>
              <Image
                width={50}
                height={50}
                src={ymslogo}
                placeholder={
                  <Image
                    preview={true}
                    src={ymslogo}
                    width={200}
                    height={200}
                  />
                }
              />
            </div>
            <div style={{paddingTop: '14px'}}> {AuthContext.copyright}</div>
          </div>
        </Descriptions.Item>
      </Descriptions>
    </>
  );

  return (
    <Modal
      title={`${getLocaleMessage({en: 'System Info', cn: '系统信息'})}(${getLocaleMessage({
        en: 'Version',
        cn: '版本',
      })}:${version})`}
      visible={visible}
      width={720}
      destroyOnClose
      closable={true}
      onCancel={handleCancel}
      footer={[
        <Button type={'primary'} key={'cancelBtn'} onClick={handleCancel}>
          <CloseCircleOutlined/> {getLocaleMessage({en: 'Close', cn: '关闭'})}
        </Button>,
      ]}
    >
      {contents}
    </Modal>
  );
};
export default SystemInfoModal;
