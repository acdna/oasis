import DictMapper from '@/common/dictionary/dict.mapper';
import React from 'react';
import {FormattedMessage} from 'umi';
import {SysDictionaryItem} from '@/pages/sys/dictionary/data';
import {getLocaleMessage} from '@/common/locales/locale';
import {Select} from "antd";


/**
 * 根据给定字典分组号创建下拉列表的选项列表组件
 *
 * @author jiang
 * @date 2020-12-26 18:47:11
 **/
export const options = (dictGroup: string, dictMap: DictMapper) => {
  let items = dictMap.getDictItems(dictGroup) || [];
  const children = [];
  for (let i = 0; i < items.length; i++) {
    let labelName = getLocaleMessage({en: items[i].dicNameEn, cn: items[i].dicName});
    children.push(<Select.Option value={items[i].dicValue} key={i.toString()}>{labelName}</Select.Option>);
  }
  return children;
};

/**
 * 根据给定数据记录列表构建下拉列表的选项列表组件
 * @param records 记录列表
 * @param cnLabelKey 下拉项显示的标签名在记录中的中文属性名
 * @param enLabelKey 下拉项显示的标签名在记录中的英文属性名
 * @param valueKey 下拉项的值在记录中的属性名
 * @author: jiangbin
 * @date: 2021-01-01 23:53:12
 **/
export const recordOptions = (params: { records: any[], cnLabelKey: string, enLabelKey: string, valueKey: string }) => {
  if (!params.records || params.records.length === 0) return [];
  const children = [];
  for (let i = 0; i < params.records.length; i++) {
    children.push(
      <Select.Option value={params.records[i][params.valueKey] || ''}
                     key={i.toString()}>
        {getLocaleMessage({
          en: params.records[i][params.enLabelKey] || '',
          cn: params.records[i][params.cnLabelKey] || '',
        })}
      </Select.Option>,
    );
  }
  return children;
};

/**
 * 根据给定数据记录列表转换给定值列表由值列数据转为标签列数据，
 * 例如：给定角色ID列表和角色数据，转换成角色名列表用于页面显示
 * @param records 记录列表
 * @param values 待转换的值数据列表
 * @param valueKey 下拉项的值在记录中的属性名
 * @param cnLabelKey 下拉项显示的标签名在记录中的中文属性名
 * @param enLabelKey 下拉项显示的标签名在记录中的英文属性名
 * @author: jiangbin
 * @date: 2021-01-02 11:04:30
 **/
export const mapValue2LabelStr = (params: { records: any[], values: any[] | any, valueKey: string, cnLabelKey: string, enLabelKey: string }) => {
  if (!Array.isArray(params.values)) params.values = [params.values];
  if (!params.records || params.records.length === 0 || !params.values || params.values.length == 0) return '';
  return params.records.filter(record => params.values.includes(record[params.valueKey])).map(record => {
    return getLocaleMessage({en: (record[params.enLabelKey] || ''), cn: (record[params.cnLabelKey] || '')});
  }).join(',');
};

/**
 * 下拉框选项
 * @author: jiangbin
 * @date: 2020-12-29 14:53:35
 **/
export const dictValueEnum = (items: SysDictionaryItem[]) => {
  return mapValueEnum({map: items, cnTextName: 'dicName', enTextName: 'dicNameEn', valueName: 'dicValue'});
};

/**
 * 下拉框选项
 * @author: jiangbin
 * @date: 2020-12-29 14:53:35
 **/
export const mapValueEnum = (params: { map: any[], cnTextName: string, enTextName: string, valueName: string }) => {
  if (!params.map || params.map?.length == 0) return {};
  const children = {};
  const textName = getLocaleMessage({en: params.enTextName, cn: params.cnTextName});
  children[''] = {text: (<FormattedMessage id='pages.list.select.all' defaultMessage='全部'/>)};
  params.map.forEach(value => {
    return children[value[params.valueName]] = {text: value[textName] || ''};
  });
  return children;
};
/**
 * 下拉框选项
 * @author: jiangbin
 * @date: 2020-12-29 14:53:35
 **/
export const listValueEnum = (params: { itemsCn: string[], itemsEn: string[] }) => {
  if (!params.itemsCn || params.itemsCn.length == 0) return {};
  if (!params.itemsEn || params.itemsEn.length == 0) return {};
  let items = getLocaleMessage({cn: params.itemsCn, en: params.itemsEn});
  const children = {};
  children[''] = {text: (<FormattedMessage id='pages.list.select.all' defaultMessage='全部'/>)};
  for (let i = 0; i < items.length; i++) {
    children[items[i]] = {text: items[i]};
  }
  return children;
};
