import { OnpVerificationService } from '@/services/onp/onp.verification.service';
import { OnpVerificationItem } from '@/pages/onp/verification/data';
import { Effect, Reducer } from 'umi';
/**
 * ONP_VERIFICATION--Model数据定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export interface OnpVerificationData {
    veId ? : string;
    veOrder ? : number;
    veColumn1 ? : string;
    veColumn2 ? : string;
    veColumn3 ? : string;
    veColumn4 ? : string;
    veColumn5 ? : string;
    veColumn6 ? : string;
    veColumn7 ? : string;
    veColumn8 ? : string;
    veColumn9 ? : string;
    veSpecies ? : string;
    veRemark ? : string;
    veCreateDate ? : Date;
    veUpdateDate ? : Date;
}
export interface OnpVerificationModelState {
    all: OnpVerificationItem[];
}
/**
 * ONP_VERIFICATION--Model类型接口定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export interface OnpVerificationModelType {
    namespace: string;
    state: OnpVerificationModelState;
    effects: { fetchAll: Effect; };
    reducers: { save: Reducer };
}
/**
 * ONP_VERIFICATION--Model对象
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpVerificationModel: OnpVerificationModelType = {
    namespace: 'onp_verification',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(OnpVerificationService.findAll);
            yield put({
                type: 'save',
                payload: { all: res || [] },
            });
        },
    },
    reducers: {
        save(state, action) {
            return { ...state, ...action.payload };
        },
    },
};
export default OnpVerificationModel;