import { OnpChloroplastOverallService } from '@/services/onp/onp.chloroplast.overall.service';
import { OnpChloroplastOverallItem } from '@/pages/onp/chloroplast/overall/data';
import { Effect, Reducer } from 'umi';
/**
 * ONP_CHLOROPLAST_OVERALL--Model数据定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export interface OnpChloroplastOverallData {
    ocoId ? : string;
    ocoOnpId ? : string;
    ocoChr ? : string;
    ocoStart ? : string;
    ocoStop ? : string;
    ocoOnpSize ? : string;
    ocoLocation ? : string;
    ocoAllMarkers ? : string;
    ocoSnpMarkers ? : string;
    ocoIndelMarkers ? : string;
    ocoBlockMarkers ? : string;
    ocoTags ? : string;
    ocoGenotypes ? : string;
    ocoMaxGenotypesFreq ? : string;
    ocoMinGenotypesFreq ? : string;
    ocoPic ? : string;
    ocoColumn1 ? : string;
    ocoColumn2 ? : string;
    ocoColumn3 ? : string;
    ocoOrder ? : number;
    ocoSpecies ? : string;
    ocoRemark ? : string;
    ocoCreateDate ? : Date;
    ocoUpdateDate ? : Date;
}
export interface OnpChloroplastOverallModelState {
    all: OnpChloroplastOverallItem[];
}
/**
 * ONP_CHLOROPLAST_OVERALL--Model类型接口定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export interface OnpChloroplastOverallModelType {
    namespace: string;
    state: OnpChloroplastOverallModelState;
    effects: { fetchAll: Effect; };
    reducers: { save: Reducer };
}
/**
 * ONP_CHLOROPLAST_OVERALL--Model对象
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpChloroplastOverallModel: OnpChloroplastOverallModelType = {
    namespace: 'onp_chloroplast_overall',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(OnpChloroplastOverallService.findAll);
            yield put({
                type: 'save',
                payload: { all: res || [] },
            });
        },
    },
    reducers: {
        save(state, action) {
            return { ...state, ...action.payload };
        },
    },
};
export default OnpChloroplastOverallModel;