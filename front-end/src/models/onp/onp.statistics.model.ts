import { OnpStatisticsService } from '@/services/onp/onp.statistics.service';
import { OnpStatisticsItem } from '@/pages/onp/statistics/data';
import { Effect, Reducer } from 'umi';
/**
 * ONP_STATISTICS--Model数据定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export interface OnpStatisticsData {
    stId ? : string;
    stBin ? : string;
    stChr ? : string;
    stStart ? : string;
    stStop ? : string;
    stBinSize ? : string;
    stDesc ? : string;
    stAllMarkers ? : string;
    stSnpMarkers ? : string;
    stIndelMarkers ? : string;
    stBlockMarkers ? : string;
    stTags ? : string;
    stGenotypes ? : string;
    stMaxGenotypesFreq ? : string;
    stMinGenotypesFreq ? : string;
    stPic ? : string;
    stGeneticMap ? : string;
    stOrder ? : number;
    stColumn1 ? : string;
    stColumn2 ? : string;
    stColumn3 ? : string;
    stSpecies ? : string;
    stRemark ? : string;
    stCreateDate ? : Date;
    stUpdateDate ? : Date;
}
export interface OnpStatisticsModelState {
    all: OnpStatisticsItem[];
}
/**
 * ONP_STATISTICS--Model类型接口定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export interface OnpStatisticsModelType {
    namespace: string;
    state: OnpStatisticsModelState;
    effects: { fetchAll: Effect; };
    reducers: { save: Reducer };
}
/**
 * ONP_STATISTICS--Model对象
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpStatisticsModel: OnpStatisticsModelType = {
    namespace: 'onp_statistics',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(OnpStatisticsService.findAll);
            yield put({
                type: 'save',
                payload: { all: res || [] },
            });
        },
    },
    reducers: {
        save(state, action) {
            return { ...state, ...action.payload };
        },
    },
};
export default OnpStatisticsModel;