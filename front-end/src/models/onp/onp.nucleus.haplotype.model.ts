import { OnpNucleusHaplotypeService } from '@/services/onp/onp.nucleus.haplotype.service';
import { OnpNucleusHaplotypeItem } from '@/pages/onp/nucleus/haplotype/data';
import { Effect, Reducer } from 'umi';
/**
 * ONP_NUCLEUS_HAPLOTYPE--Model数据定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export interface OnpNucleusHaplotypeData {
    onhId ? : string;
    onhHaplotypeIndex ? : string;
    onhChr ? : string;
    onhOnpId ? : string;
    onhHaplotypeSequence ? : string;
    onhHaplotypeTagSequence ? : string;
    onhFrequency ? : string;
    onhColumn1 ? : string;
    onhColumn2 ? : string;
    onhColumn3 ? : string;
    onhOrder ? : number;
    onhSpecies ? : string;
    onhRemark ? : string;
    onhCreateDate ? : Date;
    onhUpdateDate ? : Date;
}
export interface OnpNucleusHaplotypeModelState {
    all: OnpNucleusHaplotypeItem[];
}
/**
 * ONP_NUCLEUS_HAPLOTYPE--Model类型接口定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
export interface OnpNucleusHaplotypeModelType {
    namespace: string;
    state: OnpNucleusHaplotypeModelState;
    effects: { fetchAll: Effect; };
    reducers: { save: Reducer };
}
/**
 * ONP_NUCLEUS_HAPLOTYPE--Model对象
 * @author jiangbin
 * @date 4/29/2021, 1:57:38 PM
 **/
const OnpNucleusHaplotypeModel: OnpNucleusHaplotypeModelType = {
    namespace: 'onp_nucleus_haplotype',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(OnpNucleusHaplotypeService.findAll);
            yield put({
                type: 'save',
                payload: { all: res || [] },
            });
        },
    },
    reducers: {
        save(state, action) {
            return { ...state, ...action.payload };
        },
    },
};
export default OnpNucleusHaplotypeModel;