import {OnpChloroplastHaplotypeService} from '@/services/onp/onp.chloroplast.haplotype.service';
import {OnpChloroplastHaplotypeItem} from '@/pages/onp/chloroplast/haplotype/data';
import {Effect, Reducer} from 'umi';

/**
 * ONP_CHLOROPLAST_HAPLOTYPE--Model数据定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export interface OnpChloroplastHaplotypeData {
  ochId?: string;
  ochChloroplastHaplotype?: string;
  ochChr?: string;
  ochChloroplastOnp?: string;
  ochChloroplastHaplotypeSequence?: string;
  ochFrequency?: string;
  ochColumn1?: string;
  ochColumn2?: string;
  ochColumn3?: string;
  ochOrder?: number;
  ochSpecies?: string;
  ochRemark?: string;
  ochCreateDate?: Date;
  ochUpdateDate?: Date;
}

export interface OnpChloroplastHaplotypeModelState {
  all: OnpChloroplastHaplotypeItem[];
}

/**
 * ONP_CHLOROPLAST_HAPLOTYPE--Model类型接口定义
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export interface OnpChloroplastHaplotypeModelType {
  namespace: string;
  state: OnpChloroplastHaplotypeModelState;
  effects: { fetchAll: Effect; };
  reducers: { save: Reducer };
}

/**
 * ONP_CHLOROPLAST_HAPLOTYPE--Model对象
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
const OnpChloroplastHaplotypeModel: OnpChloroplastHaplotypeModelType = {
  namespace: 'onp_chloroplast_haplotype',
  state: {
    all: [],
  },
  effects: {
    * fetchAll(_, {put, call}) {
      let res = yield call(OnpChloroplastHaplotypeService.findAll);
      yield put({
        type: 'save',
        payload: {all: res || []},
      });
    },
  },
  reducers: {
    save(state, action) {
      return {...state, ...action.payload};
    },
  },
};
export default OnpChloroplastHaplotypeModel;
