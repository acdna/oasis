import { OnpForecastService } from '@/services/onp/onp.forecast.service';
import { OnpForecastItem } from '@/pages/onp/forecast/data';
import { Effect, Reducer } from 'umi';
/**
 * ONP_FORECAST--Model数据定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export interface OnpForecastData {
    foId ? : string;
    foOrder ? : number;
    foColumn1 ? : string;
    foColumn2 ? : string;
    foColumn3 ? : string;
    foColumn4 ? : string;
    foColumn5 ? : string;
    foColumn6 ? : string;
    foSpecies ? : string;
    foRemark ? : string;
    foCreateDate ? : Date;
    foUpdateDate ? : Date;
}
export interface OnpForecastModelState {
    all: OnpForecastItem[];
}
/**
 * ONP_FORECAST--Model类型接口定义
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export interface OnpForecastModelType {
    namespace: string;
    state: OnpForecastModelState;
    effects: { fetchAll: Effect; };
    reducers: { save: Reducer };
}
/**
 * ONP_FORECAST--Model对象
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
const OnpForecastModel: OnpForecastModelType = {
    namespace: 'onp_forecast',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(OnpForecastService.findAll);
            yield put({
                type: 'save',
                payload: { all: res || [] },
            });
        },
    },
    reducers: {
        save(state, action) {
            return { ...state, ...action.payload };
        },
    },
};
export default OnpForecastModel;