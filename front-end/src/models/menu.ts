import { Effect, Reducer } from 'umi';
import { MenuDataItem } from '@ant-design/pro-layout';
import { AuthContext } from '@/common/auth/auth.context';

export interface MenuModelState {
  menuData: MenuDataItem[];
  loading: boolean,
}

export interface MenuModelType {
  namespace: 'menu';
  state: MenuModelState;
  effects: {
    getMenuData: Effect;
    clearMenuData: Effect;
  };
  reducers: {
    save: Reducer<MenuModelState>;
  };
}

/**
 * 菜单模型对象
 * @author: jiangbin
 * @date: 2020-12-09 19:28:43
 **/
const MenuModel: MenuModelType = {
  namespace: 'menu',
  state: {
    menuData: [],
    loading: true,
  },
  effects: {
    * getMenuData(_, { call, put }) {
      const menus = AuthContext.menu;
      // const response = yield call(getMenuAuth);
      yield put({
        type: 'save',
        payload: menus,
      });
    },
    * clearMenuData(_, { put }) {
      yield put({
        type: 'save',
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        menuData: action.payload || [],
        loading: false,
      };
    },
  },
};
export default MenuModel;
