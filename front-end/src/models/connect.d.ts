import { MenuDataItem, Settings as ProSettings } from '@ant-design/pro-layout';
import { GlobalModelState } from './global';
import { UserModelState } from './user';
import { StateType } from './login';
import { MenuModelState } from './menu';

import { SnpChipFilesModelState } from '@/models/snp/snp.chip.files.model';
import { SnpChipSamplesModelState } from '@/models/snp/snp.chip.samples.model';
import { SnpGeneChipModelState } from '@/models/snp/snp.gene.chip.model';

import { SysBillingCardModelState } from '@/models/sys/sys.billing.card.model';
import { SysCacheTraceModelState } from '@/models/sys/sys.cache.trace.model';
import { SysCmsFilesModelState } from '@/models/sys/sys.cms.files.model';
import { SysCmsNoticeModelState } from '@/models/sys/sys.cms.notice.model';
import { SysColDetailModelState } from '@/models/sys/sys.col.detail.model';
import { SysConfigModelState } from '@/models/sys/sys.config.model';
import { SysContactModelState } from '@/models/sys/sys.contact.model';
import { SysDictionaryModelState } from '@/models/sys/sys.dictionary.model';
import { SysFilesModelState } from '@/models/sys/sys.files.model';
import { SysHelpModelState } from '@/models/sys/sys.help.model';
import { SysInstrumentModelState } from '@/models/sys/sys.instrument.model';
import { SysLogModelState } from '@/models/sys/sys.log.model';
import { SysMemoryModelState } from '@/models/sys/sys.memory.model';
import { SysMenuModelState } from '@/models/sys/sys.menu.model';
import { SysNoticeModelState } from '@/models/sys/sys.notice.model';
import { SysPermissionModelState } from '@/models/sys/sys.permission.model';
import { SysRoleModelState } from '@/models/sys/sys.role.model';
import { SysRoleDictionaryModelState } from '@/models/sys/sys.role.dictionary.model';
import { SysRoleMenuModelState } from '@/models/sys/sys.role.menu.model';
import { SysRolePermissionModelState } from '@/models/sys/sys.role.permission.model';
import { SysTipModelState } from '@/models/sys/sys.tip.model';
import { SysUpdateModelState } from '@/models/sys/sys.update.model';
import { SysUserModelState } from '@/models/sys/sys.user.model';
import { SysUserRoleModelState } from '@/models/sys/sys.user.role.model';
import { SysUserTopUpModelState } from '@/models/sys/sys.user.top.up.model';

export { GlobalModelState, UserModelState };

export interface Loading {
  global: boolean;
  effects: { [key: string]: boolean | undefined };
  models: {
    global?: boolean;
    menu?: boolean;
    setting?: boolean;
    user?: boolean;
    login?: boolean;
  };
}

export interface ConnectState {
  global: GlobalModelState;
  loading: Loading;
  settings: ProSettings;
  user: UserModelState;
  login: StateType;
  menu: MenuModelState;

  //SYS模块
  sys_billing_card: SysBillingCardModelState;
  sys_cache_trace: SysCacheTraceModelState;
  sys_cms_files: SysCmsFilesModelState;
  sys_cms_notice: SysCmsNoticeModelState;
  sys_col_detail: SysColDetailModelState;
  sys_config: SysConfigModelState;
  sys_contact: SysContactModelState;
  sys_dictionary: SysDictionaryModelState;
  sys_files: SysFilesModelState;
  sys_help: SysHelpModelState;
  sys_instrument: SysInstrumentModelState;
  sys_log: SysLogModelState;
  sys_memory: SysMemoryModelState;
  sys_menu: SysMenuModelState;
  sys_notice: SysNoticeModelState;
  sys_permission: SysPermissionModelState;
  sys_role: SysRoleModelState;
  sys_role_dictionary: SysRoleDictionaryModelState;
  sys_role_menu: SysRoleMenuModelState;
  sys_role_permission: SysRolePermissionModelState;
  sys_tip: SysTipModelState;
  sys_update: SysUpdateModelState;
  sys_user: SysUserModelState;
  sys_user_role: SysUserRoleModelState;
  sys_user_top_up: SysUserTopUpModelState;

  //SNP模块
  snp_chip_files: SnpChipFilesModelState;
  snp_chip_samples: SnpChipSamplesModelState;
  snp_gene_chip: SnpGeneChipModelState;
}

export interface Route extends MenuDataItem {
  routes?: Route[];
}
