import { Effect, Reducer } from 'umi';

import { queryCurrent } from '@/services/user';

/**
 * 当前用户信息对象
 * @author: jiangbin
 * @date: 2020-12-09 13:40:16
 **/
export interface CurrentUser {
  avatar?: string;
  name?: string;
  title?: string;
  group?: string;
  signature?: string;
  tags?: {
    key: string;
    label: string;
  }[];
  userid?: string;
  unreadCount?: number;
}

export interface UserModelState {
  currentUser?: CurrentUser;
}

export interface UserModelType {
  namespace: 'user';
  state: UserModelState;
  effects: {
    fetchCurrent: Effect;
    clearCurrUser: Effect;
  };
  reducers: {
    saveCurrentUser: Reducer<UserModelState>;
    changeNotifyCount: Reducer<UserModelState>;
  };
}

/**
 * 用户数据模型对象
 * @author: jiangbin
 * @date: 2020-12-09 18:22:16
 **/
const UserModel: UserModelType = {
  namespace: 'user',

  state: {
    currentUser: {},
  },

  effects: {
    /**
     * 获取当前用户信息
     * @author: jiangbin
     * @date: 2020-12-09 13:10:07
     **/
    * fetchCurrent(action, { call, put }) {
      const response = yield call(queryCurrent);
      yield put({
        type: 'saveCurrentUser',
        payload: response,
      });
    },
    /**
     * 清除当前用户信息，避免切换用户登录时菜单未刷新的问题
     * @author: jiangbin
     * @date: 2021-01-04 14:59:58
     **/
    * clearCurrUser(_,{put}){
      yield put({
        type: 'saveCurrentUser',
        payload:{currentUser:{}}
      });
    }
  },

  reducers: {
    /**
     * 保存当前用户信息
     * @author: jiangbin
     * @date: 2020-12-09 13:41:49
     **/
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },

    /**
     * 修改通知总数和未读通知数
     * @author: jiangbin
     * @date: 2020-12-09 13:38:49
     **/
    changeNotifyCount(
      state = {
        currentUser: {},
      },
      action,
    ) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};

export default UserModel;
