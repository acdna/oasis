/**
 * SYS_DICTIONARY--Model数据定义
 * @author jiangbin
 * @date 12/22/2020, 4:03:09 PM
 **/
import DictMapper from '@/common/dictionary/dict.mapper';
import { Effect, Reducer } from 'umi';
import { SysDictionaryService } from '@/services/sys/sys.dictionary.service';

export interface SysDictionaryData {
  dicId?: string;
  dicName?: string;
  dicNameEn?: string;
  dicValue?: string;
  dicGroup?: string;
  dicParentId?: string;
  dicModule?: string;
  dicType?: string;
  dicOrder?: number;
  dicState?: string;
  dicParams?: string;
  dicSpecies?: string;
  dicCreateDate?: Date;
  dicUpdateDate?: Date;
}

export interface SysDictionaryModelState {
  items: SysDictionaryData[];
  dictMap: DictMapper
}

/**
 * SYS_DICTIONARY--Model类型接口定义
 * @author jiangbin
 * @date 12/22/2020, 4:03:09 PM
 **/
export interface SysDictionaryModelType {
  namespace: string;
  state: SysDictionaryModelState;
  effects: {
    fetchAll: Effect
  };
  reducers: {
    saveAll: Reducer<SysDictionaryModelState>
  };
}

/**
 * SYS_DICTIONARY--Model对象
 * @author jiangbin
 * @date 12/22/2020, 4:03:09 PM
 **/
const SysDictionaryModel: SysDictionaryModelType = {
  namespace: 'sys_dictionary',
  state: {
    items: [],
    dictMap: new DictMapper(),
  },
  effects: {
    /**
     * 加载字典列表
     * @author: jiangbin
     * @date: 2020-12-24 15:02:54
     **/
    * fetchAll(_, { call, put }) {
      const response: [] = yield call(SysDictionaryService.findAll);
      const items = response.filter(item => item['dicType'] === 'DATA');
      const dictMap = new DictMapper(items);
      yield put({ type: 'saveAll', payload: { dictMap, items: response } });
    },
  },
  reducers: {
    /**
     * 保存字典映射表
     * @author: jiangbin
     * @date: 2020-12-24 15:03:34
     **/
    saveAll(state, { payload }): SysDictionaryModelState {
      return { ...state, ...payload };
    },
  },
};
export default SysDictionaryModel;
