import { SysCacheTraceService } from '@/services/sys/sys.cache.trace.service';
import { SysCacheTraceItem } from '@/pages/sys/cache/trace/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_CACHE_TRACE--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysCacheTraceData {
    ctId ? : string;
    ctCode ? : string;
    ctOperate ? : string;
    ctTargetId ? : string;
    ctCreateDate ? : Date;
    ctComment ? : string;
}
export interface SysCacheTraceModelState {
    all: SysCacheTraceItem[];
}
/**
 * SYS_CACHE_TRACE--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysCacheTraceModelType {
    namespace: string;
    state: SysCacheTraceModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_CACHE_TRACE--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysCacheTraceModel: SysCacheTraceModelType = {
    namespace: 'sys_cache_trace',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysCacheTraceService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysCacheTraceModel;