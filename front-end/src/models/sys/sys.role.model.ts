/**
 * SYS_ROLE--Model数据定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
import { Effect, Reducer } from 'umi';
import { SysRoleService } from '@/services/sys/sys.role.service';

export interface SysRoleData {
  roleId?: string;
  roleName?: string;
  roleNameEn?: string;
  roleState?: string;
  roleSys?: string;
}

export interface SysRoleModelState {
  roles: SysRoleData[];
}

/**
 * SYS_ROLE--Model类型接口定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysRoleModelType {
  namespace: string;
  state: SysRoleModelState;
  effects: { fetchAll: Effect; };
  reducers: { saveAll: Reducer<SysRoleModelState> };
}

/**
 * SYS_ROLE--Model对象
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
const SysRoleModel: SysRoleModelType = {
  namespace: 'sys_role',
  state: {
    roles: [],
  },
  effects: {
    /**
     * 加载系统中所有角色信息
     * @author: jiangbin
     * @date: 2021-01-02 00:43:06
     **/
    * fetchAll(_, { call, put }) {
      const response: [] = yield call(SysRoleService.findAll);
      yield put({ type: 'saveAll', payload: { roles: response } });
    },
  },
  reducers: {
    saveAll(state, { payload }): SysRoleModelState {
      return { ...state, ...payload };
    },
  },
};
export default SysRoleModel;
