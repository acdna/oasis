import { SysUserRoleService } from '@/services/sys/sys.user.role.service';
import { SysUserRoleItem } from '@/pages/sys/user/role/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_USER_ROLE--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysUserRoleData {
    urId ? : string;
    urRoleId ? : string;
    urUserLoginName ? : string;
}
export interface SysUserRoleModelState {
    all: SysUserRoleItem[];
}
/**
 * SYS_USER_ROLE--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysUserRoleModelType {
    namespace: string;
    state: SysUserRoleModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_USER_ROLE--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysUserRoleModel: SysUserRoleModelType = {
    namespace: 'sys_user_role',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysUserRoleService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysUserRoleModel;