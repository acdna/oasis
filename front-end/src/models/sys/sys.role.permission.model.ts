import { SysRolePermissionService } from '@/services/sys/sys.role.permission.service';
import { SysRolePermissionItem } from '@/pages/sys/role/permission/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_ROLE_PERMISSION--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysRolePermissionData {
    rpId ? : string;
    rpRoleId ? : string;
    rpPermissionId ? : string;
}
export interface SysRolePermissionModelState {
    all: SysRolePermissionItem[];
}
/**
 * SYS_ROLE_PERMISSION--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysRolePermissionModelType {
    namespace: string;
    state: SysRolePermissionModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_ROLE_PERMISSION--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysRolePermissionModel: SysRolePermissionModelType = {
    namespace: 'sys_role_permission',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysRolePermissionService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysRolePermissionModel;