import { SysRoleDictionaryService } from '@/services/sys/sys.role.dictionary.service';
import { SysRoleDictionaryItem } from '@/pages/sys/role/dictionary/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_ROLE_DICTIONARY--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysRoleDictionaryData {
    rdId ? : string;
    rdRoleId ? : string;
    rdDictionaryId ? : string;
}
export interface SysRoleDictionaryModelState {
    all: SysRoleDictionaryItem[];
}
/**
 * SYS_ROLE_DICTIONARY--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysRoleDictionaryModelType {
    namespace: string;
    state: SysRoleDictionaryModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_ROLE_DICTIONARY--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysRoleDictionaryModel: SysRoleDictionaryModelType = {
    namespace: 'sys_role_dictionary',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysRoleDictionaryService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysRoleDictionaryModel;