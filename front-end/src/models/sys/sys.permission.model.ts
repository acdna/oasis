import { SysPermissionService } from '@/services/sys/sys.permission.service';
import { SysPermissionItem } from '@/pages/sys/permission/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_PERMISSION--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysPermissionData {
    perId ? : string;
    perName ? : string;
    perNameEn ? : string;
    perType ? : string;
    perUrl ? : string;
    perMethod ? : string;
    perParentId ? : string;
    perOrder ? : number;
    perRemark ? : string;
    perState ? : string;
    perSystem ? : string;
    perModule ? : string;
}
export interface SysPermissionModelState {
    all: SysPermissionItem[];
}
/**
 * SYS_PERMISSION--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysPermissionModelType {
    namespace: string;
    state: SysPermissionModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_PERMISSION--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysPermissionModel: SysPermissionModelType = {
    namespace: 'sys_permission',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysPermissionService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysPermissionModel;