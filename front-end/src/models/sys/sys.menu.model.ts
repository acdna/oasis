import { SysMenuService } from '@/services/sys/sys.menu.service';
import { Effect, Reducer } from 'umi';
import { SysMenuItem } from '@/pages/sys/menu/data';

/**
 * SYS_MENU--Model数据定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysMenuData {
  menuId?: string;
  menuName?: string;
  menuNameEn?: string;
  menuUrl?: string;
  menuParentId?: string;
  menuOrder?: number;
  menuRemark?: string;
  menuState?: string;
  menuType?: string;
  menuIconClass?: string;
}

export interface SysMenuModelState {
  allMenus: SysMenuItem[];
}

/**
 * SYS_MENU--Model类型接口定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysMenuModelType {
  namespace: string;
  state: SysMenuModelState;
  effects: { fetchAll: Effect };
  reducers: { saveAll: Reducer };
}

/**
 * SYS_MENU--Model对象
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
const SysMenuModel: SysMenuModelType = {
  namespace: 'sys_menu',
  state: {
    allMenus: [],
  },
  effects: {
    * fetchAll(_, { put, call }) {
      const response:SysMenuData[] = yield call(SysMenuService.findAll);
      yield put({
        type: 'saveAll',
        payload: response,
      });
    },
  },
  reducers: {
    saveAll(state, action) {
      return {
        ...state,
        allMenus: action.payload || [],
      };
    },
  },
};
export default SysMenuModel;
