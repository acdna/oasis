import { SysTipService } from '@/services/sys/sys.tip.service';
import { SysTipItem } from '@/pages/sys/tip/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_TIP--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysTipData {
    tipId ? : string;
    tipCount ? : number;
    tipContents ? : string;
    tipManager ? : string;
    tipAudience ? : string;
    tipCreateDate ? : Date;
    tipUpdateDate ? : Date;
}
export interface SysTipModelState {
    all: SysTipItem[];
}
/**
 * SYS_TIP--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysTipModelType {
    namespace: string;
    state: SysTipModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_TIP--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysTipModel: SysTipModelType = {
    namespace: 'sys_tip',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysTipService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysTipModel;