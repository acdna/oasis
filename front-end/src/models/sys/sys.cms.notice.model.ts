import { SysCmsNoticeService } from '@/services/sys/sys.cms.notice.service';
import { SysCmsNoticeItem } from '@/pages/sys/cms/notice/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_CMS_NOTICE--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysCmsNoticeData {
    scNoticeId ? : string;
    scNoticeTitle ? : string;
    scNoticeContent ? : string;
    scNoticeIsNew ? : string;
    scNoticeManager ? : string;
    scNoticeCreateDate ? : Date;
    scNoticeUpdateDate ? : Date;
}
export interface SysCmsNoticeModelState {
    all: SysCmsNoticeItem[];
}
/**
 * SYS_CMS_NOTICE--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysCmsNoticeModelType {
    namespace: string;
    state: SysCmsNoticeModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_CMS_NOTICE--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysCmsNoticeModel: SysCmsNoticeModelType = {
    namespace: 'sys_cms_notice',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysCmsNoticeService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysCmsNoticeModel;