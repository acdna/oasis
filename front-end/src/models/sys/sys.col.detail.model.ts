import { SysColDetailService } from '@/services/sys/sys.col.detail.service';
import { SysColDetailItem } from '@/pages/sys/col/detail/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_COL_DETAIL--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysColDetailData {
    cdId ? : string;
    cdTableName ? : string;
    cdColName ? : string;
    cdDescName ? : string;
    cdDictName ? : string;
    cdMode ? : string;
    cdCreateDate ? : Date;
    cdUpdateDate ? : Date;
}
export interface SysColDetailModelState {
    all: SysColDetailItem[];
}
/**
 * SYS_COL_DETAIL--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysColDetailModelType {
    namespace: string;
    state: SysColDetailModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_COL_DETAIL--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysColDetailModel: SysColDetailModelType = {
    namespace: 'sys_col_detail',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysColDetailService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysColDetailModel;