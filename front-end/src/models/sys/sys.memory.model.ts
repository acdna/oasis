/**
 * SYS_MEMORY--Model数据定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysMemoryData {
    memId ? : string;
    memTotal ? : number;
    memFree ? : number;
    memUsed ? : number;
    memIsWarning ? : string;
    memMax ? : number;
    memCreateDate ? : Date;
    memUpdateDate ? : Date;
}
export interface SysMemoryModelState {
    data ? : SysMemoryData;
}
/**
 * SYS_MEMORY--Model类型接口定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysMemoryModelType {
    namespace: string;
    state: SysMemoryModelState;
    effects: {};
    reducers: {};
}
/**
 * SYS_MEMORY--Model对象
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
const SysMemoryModel: SysMemoryModelType = {
    namespace: 'sys_memory',
    state: {
        data: {},
    },
    effects: {},
    reducers: {},
};
export default SysMemoryModel;