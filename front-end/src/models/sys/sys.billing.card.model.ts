import { SysBillingCardService } from '@/services/sys/sys.billing.card.service';
import { SysBillingCardItem } from '@/pages/sys/billing/card/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_BILLING_CARD--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysBillingCardData {
    bcId ? : string;
    bcSerialNumber ? : string;
    bcAmount ? : string;
    bcTotalTime ? : number;
    bcType ? : string;
    bcAccount ? : string;
    bcPassword ? : string;
    bcState ? : string;
    bcBiggestGeneCount ? : string;
    bcCreateDate ? : Date;
}
export interface SysBillingCardModelState {
    all: SysBillingCardItem[];
}
/**
 * SYS_BILLING_CARD--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysBillingCardModelType {
    namespace: string;
    state: SysBillingCardModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_BILLING_CARD--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysBillingCardModel: SysBillingCardModelType = {
    namespace: 'sys_billing_card',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysBillingCardService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysBillingCardModel;