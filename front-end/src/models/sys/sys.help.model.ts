import { SysHelpService } from '@/services/sys/sys.help.service';
import { SysHelpItem } from '@/pages/sys/help/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_HELP--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysHelpData {
    helpId ? : string;
    helpQuestion ? : string;
    helpAnswer ? : string;
    helpParentId ? : string;
    helpType ? : string;
    helpPath ? : string;
    helpSort ? : string;
    helpCreateDate ? : Date;
    helpUpdateDate ? : Date;
}
export interface SysHelpModelState {
    all: SysHelpItem[];
}
/**
 * SYS_HELP--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysHelpModelType {
    namespace: string;
    state: SysHelpModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_HELP--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysHelpModel: SysHelpModelType = {
    namespace: 'sys_help',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysHelpService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysHelpModel;