import { SysLogService } from '@/services/sys/sys.log.service';
import { SysLogItem } from '@/pages/sys/log/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_LOG--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysLogData {
    logId ? : string;
    logUser ? : string;
    logTime ? : Date;
    logIp ? : string;
    logUrl ? : string;
    logTitle ? : string;
    logContent ? : string;
    logType ? : number;
}
export interface SysLogModelState {
    all: SysLogItem[];
}
/**
 * SYS_LOG--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysLogModelType {
    namespace: string;
    state: SysLogModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_LOG--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysLogModel: SysLogModelType = {
    namespace: 'sys_log',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysLogService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysLogModel;