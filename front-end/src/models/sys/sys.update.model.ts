import { SysUpdateService } from '@/services/sys/sys.update.service';
import { SysUpdateItem } from '@/pages/sys/update/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_UPDATE--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysUpdateData {
    updateId ? : string;
    updateVersion ? : string;
    updateContent ? : string;
    updateManager ? : string;
    updateCreateTime ? : Date;
}
export interface SysUpdateModelState {
    all: SysUpdateItem[];
}
/**
 * SYS_UPDATE--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysUpdateModelType {
    namespace: string;
    state: SysUpdateModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_UPDATE--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysUpdateModel: SysUpdateModelType = {
    namespace: 'sys_update',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysUpdateService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysUpdateModel;