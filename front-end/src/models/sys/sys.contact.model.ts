import { SysContactService } from '@/services/sys/sys.contact.service';
import { SysContactItem } from '@/pages/sys/contact/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_CONTACT--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysContactData {
    contactId ? : string;
    contactAuthor ? : string;
    contactEmail ? : string;
    contactSubject ? : string;
    contactText ? : string;
    contactCreateDate ? : Date;
}
export interface SysContactModelState {
    all: SysContactItem[];
}
/**
 * SYS_CONTACT--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysContactModelType {
    namespace: string;
    state: SysContactModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_CONTACT--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysContactModel: SysContactModelType = {
    namespace: 'sys_contact',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysContactService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysContactModel;