import { SysUserService } from '@/services/sys/sys.user.service';
import { Effect, Reducer } from 'umi';
import UserMapper from '@/common/users/user.mapper';

/**
 * SYS_USER--Model数据定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysUserData {
  userId?: string;
  userLoginName?: string;
  userPassword?: string;
  userPasswordPrivate?: string;
  userNameAbbr?: string;
  userName?: string;
  userNameEn?: string;
  userSex?: string;
  userPhone?: string;
  userEmail?: string;
  userImageUrl?: string;
  userOrder?: number;
  userState?: string;
  userCode?: string;
  userCreateDate?: Date;
  userUpdateDate?: Date;
  userLastLoginTime?: Date;
  userLastLoginIp?: string;
  userUnit?: string;
  userJobTitle?: string;
  userAddr?: string;
  userPostcode?: string;
  userSamSpecies?: string;
}

export interface SysUserModelState {
  userMap: UserMapper;
}

/**
 * SYS_USER--Model类型接口定义
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
export interface SysUserModelType {
  namespace: string;
  state: SysUserModelState;
  effects: { fetchAll: Effect; };
  reducers: { saveAll: Reducer };
}

/**
 * SYS_USER--Model对象
 * @author jiangbin
 * @date 12/30/2020, 4:59:08 PM
 **/
const SysUserModel: SysUserModelType = {
  namespace: 'sys_user',
  state: {
    userMap: new UserMapper(),
  },
  effects: {
    * fetchAll(_, { put, call }) {
      let res = yield call(SysUserService.findAll);
      yield put({
        type: 'saveAll',
        payload: {
          userMap: new UserMapper(res),
        },
      });
    },
  },
  reducers: {
    saveAll(state, action) {
      return { ...state, ...action.payload };
    },
  },
};
export default SysUserModel;
