import { SysInstrumentService } from '@/services/sys/sys.instrument.service';
import { SysInstrumentItem } from '@/pages/sys/instrument/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_INSTRUMENT--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysInstrumentData {
    insId ? : string;
    insBarcode ? : string;
    insName ? : string;
    insModel ? : string;
    insManager ? : string;
    insType ? : string;
    insRemark ? : string;
    insBuyDate ? : Date;
    insMaintainPeriod ? : string;
    insMaker ? : string;
    insPrice ? : string;
    insExtraProvide ? : string;
    insCreateDate ? : Date;
    insUpdateDate ? : Date;
}
export interface SysInstrumentModelState {
    all: SysInstrumentItem[];
}
/**
 * SYS_INSTRUMENT--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysInstrumentModelType {
    namespace: string;
    state: SysInstrumentModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_INSTRUMENT--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysInstrumentModel: SysInstrumentModelType = {
    namespace: 'sys_instrument',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysInstrumentService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysInstrumentModel;