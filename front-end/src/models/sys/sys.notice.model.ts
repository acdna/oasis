import { SysNoticeService } from '@/services/sys/sys.notice.service';
import { SysNoticeItem } from '@/pages/sys/notice/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_NOTICE--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysNoticeData {
    noticeId ? : string;
    noticeTitle ? : string;
    noticeContent ? : string;
    noticeManager ? : string;
    noticeIsNew ? : string;
    noticeCreateDate ? : Date;
    noticeUpdateDate ? : Date;
    noticeReceiver ? : string;
}
export interface SysNoticeModelState {
    all: SysNoticeItem[];
}
/**
 * SYS_NOTICE--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysNoticeModelType {
    namespace: string;
    state: SysNoticeModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_NOTICE--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysNoticeModel: SysNoticeModelType = {
    namespace: 'sys_notice',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysNoticeService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysNoticeModel;