import { SysCmsFilesService } from '@/services/sys/sys.cms.files.service';
import { SysCmsFilesItem } from '@/pages/sys/cms/files/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_CMS_FILES--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysCmsFilesData {
    scFileId ? : string;
    scFileName ? : string;
    scFileNameCn ? : string;
    scFileType ? : string;
    scFilePath ? : string;
    scFileIsNew ? : string;
    scFileManager ? : string;
    scFileCreateDate ? : Date;
    scFielUpdateDate ? : Date;
    scFileSpecies ? : string;
}
export interface SysCmsFilesModelState {
    all: SysCmsFilesItem[];
}
/**
 * SYS_CMS_FILES--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysCmsFilesModelType {
    namespace: string;
    state: SysCmsFilesModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_CMS_FILES--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysCmsFilesModel: SysCmsFilesModelType = {
    namespace: 'sys_cms_files',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysCmsFilesService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysCmsFilesModel;