import { SysUserTopUpService } from '@/services/sys/sys.user.top.up.service';
import { SysUserTopUpItem } from '@/pages/sys/user/top/up/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_USER_TOP_UP--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysUserTopUpData {
    utuId ? : string;
    utuLoginName ? : string;
    utuSerialNumber ? : string;
    utuBillingStartDate ? : Date;
    utuBillingEndDate ? : Date;
    utuState ? : string;
    utuCreateDate ? : Date;
}
export interface SysUserTopUpModelState {
    all: SysUserTopUpItem[];
}
/**
 * SYS_USER_TOP_UP--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysUserTopUpModelType {
    namespace: string;
    state: SysUserTopUpModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_USER_TOP_UP--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysUserTopUpModel: SysUserTopUpModelType = {
    namespace: 'sys_user_top_up',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysUserTopUpService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysUserTopUpModel;