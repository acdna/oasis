import { SysFilesService } from '@/services/sys/sys.files.service';
import { SysFilesItem } from '@/pages/sys/files/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_FILES--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysFilesData {
    fileId ? : string;
    fileBarcode ? : string;
    fileRelateId ? : string;
    fileName ? : string;
    fileNameEn ? : string;
    filePath ? : string;
    fileCreateDate ? : Date;
    fileUpdateDate ? : Date;
    fileIsUsed ? : string;
    fileOwner ? : string;
    fileSize ? : number;
    fileGrants ? : string;
    fileClasses ? : string;
    fileState ? : string;
    fileType ? : string;
    fileComments ? : string;
    fileSpecies ? : string;
}
export interface SysFilesModelState {
    all: SysFilesItem[];
}
/**
 * SYS_FILES--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysFilesModelType {
    namespace: string;
    state: SysFilesModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_FILES--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysFilesModel: SysFilesModelType = {
    namespace: 'sys_files',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysFilesService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysFilesModel;