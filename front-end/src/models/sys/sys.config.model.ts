import { SysConfigService } from '@/services/sys/sys.config.service';
import { SysConfigItem } from '@/pages/sys/config/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_CONFIG--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysConfigData {
    conId ? : string;
    conName ? : string;
    conNameEn ? : string;
    conValue ? : string;
    conGroup ? : string;
    conParamType ? : string;
    conParentId ? : string;
    conType ? : string;
    conCreateDate ? : Date;
    conUpdateDate ? : Date;
    conManager ? : string;
    conSpecies ? : string;
    conOrder ? : number;
    conState ? : string;
    conRemark ? : string;
}
export interface SysConfigModelState {
    all: SysConfigItem[];
}
/**
 * SYS_CONFIG--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
export interface SysConfigModelType {
    namespace: string;
    state: SysConfigModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_CONFIG--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:32 PM
 **/
const SysConfigModel: SysConfigModelType = {
    namespace: 'sys_config',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysConfigService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysConfigModel;