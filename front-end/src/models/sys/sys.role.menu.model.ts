import { SysRoleMenuService } from '@/services/sys/sys.role.menu.service';
import { SysRoleMenuItem } from '@/pages/sys/role/menu/data';
import { Effect, Reducer } from 'umi';
/**
 * SYS_ROLE_MENU--Model数据定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysRoleMenuData {
    rmId ? : string;
    rmRoleId ? : string;
    rmMenuId ? : string;
}
export interface SysRoleMenuModelState {
    all: SysRoleMenuItem[];
}
/**
 * SYS_ROLE_MENU--Model类型接口定义
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
export interface SysRoleMenuModelType {
    namespace: string;
    state: SysRoleMenuModelState;
    effects: { fetchAll: Effect; };
    reducers: { saveAll: Reducer };
}
/**
 * SYS_ROLE_MENU--Model对象
 * @author jiangbin
 * @date 1/11/2021, 1:33:33 PM
 **/
const SysRoleMenuModel: SysRoleMenuModelType = {
    namespace: 'sys_role_menu',
    state: {
        all: [],
    },
    effects: {
        * fetchAll(_, { put, call }) {
            let res = yield call(SysRoleMenuService.findAll);
            yield put({
                type: 'saveAll',
                payload: res,
            });
        },
    },
    reducers: {
        saveAll(state, action) {
            return { ...state, all: action.payload || [] };
        },
    },
};
export default SysRoleMenuModel;