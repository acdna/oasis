import { stringify } from 'querystring';
import { Effect, history, Reducer } from 'umi';
import { login } from '@/services/login';
import { getPageQuery } from '@/utils/utils';
import { message } from 'antd';
import { AuthContext } from '@/common/auth/auth.context';
import { md5 } from '@/common/utils/system';

export interface StateType {
  status?: 'ok' | 'error';
  type?: string;
  currentAuthority?: 'user' | 'guest' | 'admin';
}

export interface LoginModelType {
  namespace: string;
  state: StateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {
    changeLoginStatus: Reducer<StateType>;
  };
}

const Model: LoginModelType = {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    /**
     * 用户登录验证业务方法
     * @author: jiangbin
     * @date: 2020-12-09 14:49:45
     **/
    * login({ payload }, { call, put }) {
      payload.password = md5(payload.password);

      //调用登录验证服务层方法
      const response = yield call(login, payload);

      //修改登录状态信息
      yield put({
        type: 'changeLoginStatus',
        payload: response,
      });

      //登录成功后提示和跳转页面
      if (response.statusCode === 200) {

        //缓存用户信息
        AuthContext.user = JSON.stringify(response.message);

        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        message.success('🎉 🎉 🎉  登录成功！');

        // 跳转到用户指定的登录首页面，若未指定则跳转到'/'页面
        window.location.href = AuthContext.indexUrl;
        return;

        //获取跳转的路径
        let { redirect } = params as { redirect: string };
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }
        history.replace(redirect || '/');
      }
    },

    /**
     * 用户登出
     * @author: jiangbin
     * @date: 2020-12-09 18:06:39
     **/
    * logout(_, { put }) {
      // 清空token
      AuthContext.logout();

      //清空当前用户
      yield put({
        type: 'user/clearCurrUser',
      });

      //清空用户菜单
      yield put({
        type: 'menu/clearMenuData',
      });

      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {

        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },

  reducers: {
    /**
     * 修改登录状态信息
     * @author: jiangbin
     * @date: 2020-12-09 18:26:48
     **/
    changeLoginStatus(state, { payload }) {
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};

export default Model;
