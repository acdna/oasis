import DictMapper from '@/common/dictionary/dict.mapper';
import UserMapper from '@/common/users/user.mapper';

export class DataCache{
  static dictMap:DictMapper;
  static userMap:UserMapper;
  static pmsMap:[];
}
