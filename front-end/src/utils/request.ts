/**
 * request 网络请求工具
 * 更详细的 api 文档: https://github.com/umijs/umi-request
 */
import { extend } from 'umi-request';
import { notification } from 'antd';
import { AuthContext } from '@/common/auth/auth.context';
import { getServerUrl } from '@/common/utils/system';
import { getLocaleMessage } from '@/common/locales/locale';

/**
 * 中文消息
 * @author: jiangbin
 * @date: 2021-01-19 11:01:36
 **/
const codeMessageCn = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是功能访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

/**
 * 英文消息
 * @author: jiangbin
 * @date: 2021-01-19 11:01:24
 **/
const codeMessageEn = {
  200: 'The server successfully returned the requested data.',
  201: 'New or modified data was successful.',
  202: 'A request has been queued in the background (asynchronous task).',
  204: 'Deleting data was successful.',
  400: 'There was an error in the outgoing request, the server did not perform a new or modified data operation.',
  401: 'The user does not have permission (wrong token, username, password).',
  403: 'The user is authorized, but function access is forbidden.',
  404: 'The request was sent for a record that does not exist and the server did not perform the operation.',
  406: 'The format of the request was not available.',
  410: 'The requested resource was permanently deleted and will not be available again.',
  422: 'A validation error occurred when an object was created.',
  500: 'An error occurred on the server, please check the server.',
  502: 'Gateway error.',
  503: 'Service is not available, the server is temporarily overloaded or under maintenance.',
  504: 'Gateway timeout.',
};


/**
 * 异常处理程序
 */
const errorHandler = (error: { response: Response }): Response => {
  const { response } = error;
  if (response && response.status) {
    const { status, url } = response;
    notification.error({
      message: getLocaleMessage({ cn: `请求错误 ${status}: ${url}`, en: `Request Error ${status}: ${url}` }),
      description: getLocaleMessage({
        en: (codeMessageEn[response.status] || response.statusText),
        cn: (codeMessageCn[response.status] || response.statusText),
      }),
    });
    //用户认证失败时，跳转到登录页面
    if (status == 401 || status == 403) {
      AuthContext.logout();//清理掉用户信息，避免后面跳转时的死循环
    }
  } else if (!response) {
    notification.error({
      description: getLocaleMessage({
        en: 'Your network is abnormal and you cannot connect to the server',
        cn: '您的网络发生异常，无法连接服务器',
      }),
      message: getLocaleMessage({ en: 'Network Anomaly', cn: '网络异常' }),
    });
  }
  return response;
};

/**
 * 配置request请求时的默认参数
 */
const request = extend({
  errorHandler, // 默认错误处理
  // credentials: 'include', // 默认请求是否带上cookie
});

/**
 * 请求指定URL
 * @author: jiangbin
 * @date: 2020-12-09 13:21:24
 **/
// @ts-ignore
request.interceptors.request.use((url, options) => {
  url = getServerUrl(url);
  let headers = AuthContext.headers;
  return ({
      url,
      options: { ...options, headers: headers },
    }
  );
});

/**
 * 克隆响应对象做解析处理
 * @author: jiangbin
 * @date: 2020-12-10 14:08:31
 **/
request.interceptors.response.use((response) => {
  return response;
});

export default request;
