export default {
  '/api/auth_routes': {
    '/form/advanced-form': {authority: ['admin', 'user']},
  },

  '/api/route/menu': [
    {
      path: "/welcome",
      name: "welcome",
      icon: "smile",
      authority: ['admin', 'user'],
    },
    {
      path: "/list",
      name: "list.table-list",
      icon: "gift",
      authority: ['admin'],
    },
    {
      path: '/admin',
      name: 'admin',
      icon: 'crown',
      component: './Admin',
      routes: [
        {
          path: '/admin/sub-page',
          name: 'sub-page',
          component: './Welcome',
          authority: ['admin'],
        },
      ],
    },
    {
      path: '/system',
      name: 'system',
      icon: 'SettingOutlined',
      routes: [
        {
          path: '/system/user',
          name: 'user',
          icon: 'TeamOutlined',
          component: './sys/user/listUser',
        },
        {
          path: '/system/dict',
          name: 'dict',
          icon: 'BookFilled',
          component: './sys/dict/Welcome',
        },

      ],
    },

  ],



};
