import { uuid } from '@/common/utils/uuid.utils';

const SYS_PMS = require('../src/permission/sys/sys.pms');
const SSR_PMS = require('../src/permission/ssr/ssr.pms');

const createPmsRecords = (pmsList) => {
  if (!pmsList || pmsList?.length == 0) return [];
  let records = new Array();
  pmsList.forEach((value, index) => {
    let root = value.ROOT;
    if (!root || root.length == 0) return;
    //权限节点
    let record = {
      perId: uuid(),
      perName: root.name,
      perNameEn: root.nameEn,
      perType: 'DIR',
      perUrl: '',
      perMethod: 'FRONT',
      perParentId: '1',
      perOrder: (index + 1),
      perRemark: '',
      perState: 'ON',
      perSystem: root.system,
      perModule: root.group,
    };
    records.push(record);

    //权限项节点列表
    let items = value.ITEMS;
    items.forEach((item, index) => {
      records.push({
        perId: uuid(),
        perName: item.name,
        perNameEn: item.nameEn,
        perType: 'DATA',
        perUrl: item.id,
        perMethod: 'FRONT',
        perParentId: record.perId,
        perOrder: (index + 1),
        perRemark: '',
        perState: item.enable ? 'ON' : 'OFF',
        perSystem: record.perSystem,
        perModule: record.perModule,
      });
    });
  });
  return records;
};


let records = new Array();
console.log('xxxxx');
let targets = createPmsRecords(SYS_PMS);
if (targets?.length > 0) {
  records.concat(targets);
}
console.log(JSON.stringify(records));
targets = createPmsRecords(SSR_PMS);
if (targets?.length > 0) {
  records.concat(targets);
}
console.log(JSON.stringify(records));
