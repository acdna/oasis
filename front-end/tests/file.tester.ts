const path = require('path');
/**
 * 获取文件的上级目录路径
 * @param filePath 文件路径
 * @author: jiangbin
 * @date: 2021-01-14 09:59:42
 **/
const getParentPath = (filePath: string) => {
  return path.join(filePath, '..');
};
/**
 * 获取文件扩展名
 * @param filePath 文件路径
 * @return {string} 扩展名，例如：xlsx，若无扩展名则返回空字符串
 * @author: jiangbin
 * @date: 2021-01-14 12:53:55
 **/
const getFileExt = (filePath: string) => {
  if (!filePath || filePath.length == 0 || filePath.lastIndexOf('.') == -1) return '';
  return filePath.substring(filePath.lastIndexOf('.'));
};
/**
 * 获取文件路径中包含的文件名
 * @author: jiangbin
 * @date: 2021-01-17 16:25:41
 **/
const getFileName = (filePath: string) => {
  if (!filePath || filePath.length == 0) return '';
  filePath = filePath.replaceAll('\\', '/');
  console.log(filePath);
  if (filePath.lastIndexOf('/') == -1) return filePath;
  if (filePath.endsWith('/')) {
    filePath = filePath.substring(0, filePath.length - 1);
  }
  return filePath.substring(filePath.lastIndexOf('/') + 1);
};
/**
 * 获取文件路径中包含的简单文件名，例如：/common/2021/01/17/样品信息表.docx==>样品信息表
 * @author: jiangbin
 * @date: 2021-01-17 16:25:41
 **/
const getSimpleFileName= (filePath: string) => {
  let fileName = getFileName(filePath);
  if (!fileName || fileName.length == 0 || fileName.lastIndexOf('.') == -1) return '';
  return fileName.substring(0, fileName.lastIndexOf('.'));
}

let path2 = `/Volumes/work/scdms\\uploads/common/2021/01/17/161518\\22d5ee00589c11ebb6b81fa0ae96dd90.docx`;
console.log(getFileName(path2));
console.log(getSimpleFileName(path2));
