﻿export default [
  // 登陆页面
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/login',
      },
    ],
  },
  {
    path: '/',
    routes: [
      {
        path: '/',
        component: '../layouts/BlankLayout',
        routes: [
          {path: '/', redirect: '/oasis'},
          {
            path: '/oasis',
            name: 'Home',
            component: './Home/index',
          },
          {
            path: '/oasis/onp/forecast',
            name: 'OnpForecast',
            icon: 'TeamOutlined',
            component: './onp/forecast/forecast.index',
          },
          {
            path: '/oasis/onp/statistics',
            name: 'OnpStatistics',
            icon: 'TeamOutlined',
            component: './onp/statistics',
          },
          {
            path: '/oasis/onp/verification',
            name: 'OnpVerification',
            icon: 'TeamOutlined',
            component: './onp/verification',
          },
          {
            path: '/onp/chloroplast/haplotype',
            name: 'OnpChloroplastHaplotype',
            icon: 'TeamOutlined',
            component: './onp/chloroplast/haplotype',
          },
          {
            path: '/onp/chloroplast/overall',
            name: 'OnpChloroplastOverall',
            icon: 'TeamOutlined',
            component: './onp/chloroplast/overall',
          },
          {
            path: '/onp/nucleus/haplotype',
            name: 'OnpNucleusHaplotype',
            icon: 'TeamOutlined',
            component: './onp/nucleus/haplotype',
          },
          ],
      },

      {
        path: '/',
        component: '../layouts/BasicLayout',
        authority: ['admin', 'oasis'],
        routes: [
          {
            path: '/sys',
            name: 'system',
            icon: 'SettingOutlined',
            routes: [
              {
                path: '/sys/menu',
                name: 'SysMenu',
                icon: 'TeamOutlined',
                component: './sys/menu',
              },
              {
                path: '/sys/role',
                name: 'SysRole',
                icon: 'TeamOutlined',
                component: './sys/role',
              },
              {
                path: '/sys/user',
                name: 'SysUser',
                icon: 'TeamOutlined',
                component: './sys/user',
              },
              {
                path: '/sys/permission',
                name: 'SysPermission',
                icon: 'TeamOutlined',
                component: './sys/permission',
              },
              {
                path: '/sys/dictionary',
                name: 'SysDictionary',
                icon: 'TeamOutlined',
                component: './sys/dictionary',
              },
              {
                path: '/sys/files',
                name: 'SysFiles',
                icon: 'TeamOutlined',
                component: './sys/files',
              },
              {
                path: '/sys/memory',
                name: 'SysMemory',
                icon: 'TeamOutlined',
                component: './sys/memory',
              },
              {
                path: '/sys/contact',
                name: 'SysContact',
                icon: 'TeamOutlined',
                component: './sys/contact',
              },
            ],
          },
        ],
      },
    ]},
  {
    component: './404',
  },

];
