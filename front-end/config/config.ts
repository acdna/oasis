// https://umijs.org/config/
import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';
import routes from './routes';

/**
 * <pre>SERVER_BASE_URL参数的优先序为:
 * 1、根据"REACT_APP_ENV"参数类型自动选择使用"DEV_SERVER_BASE_URL"还是"PRO_SERVER_BASE_URL"
 * 2、最后使用默认"http://localhost:3000"</pre>
 * @author: jiangbin
 * @date: 2021-02-10 17:33:54
 **/
const { REACT_APP_ENV='dev',DEV_SERVER_BASE_URL,PRO_SERVER_BASE_URL } = process.env;

//当前请求服务器的地址
const SERVER_BASE_URL = (REACT_APP_ENV ==='pre' ) ? PRO_SERVER_BASE_URL : DEV_SERVER_BASE_URL;

console.log(`REACT_APP_ENV=>${REACT_APP_ENV}`);
console.log(`CURR_SERVER_BASE_URL=>${SERVER_BASE_URL}`);
console.log(`DEV_SERVER_BASE_URL=>${DEV_SERVER_BASE_URL}`);
console.log(`PRO_SERVER_BASE_URL=>${PRO_SERVER_BASE_URL}`);

export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  history: {
    type: 'browser',
  },
  locale: {
    // 国际化语言
    default: 'zh-CN',
    antd: true,
    // default true, when it is true, will use `navigator.language` overwrite default
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes,
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    'primary-color': defaultSettings.primaryColor,
  },
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
  esbuild: {},
  define :{
    SERVER_BASE_URL: SERVER_BASE_URL
  }
});
