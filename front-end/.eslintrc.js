module.exports = {
  extends: [require.resolve('@umijs/fabric/dist/eslint')],
  globals: {
    ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION: true,
    page: true,
    REACT_APP_ENV: true,
    SERVER_BASE_URL: true,
  },
  plugins: [
    'react-hooks',
  ],
  //
  rules: {
    '@eslint-disable-next-line': 'off',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ["off"],
    'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
    'react-hooks/exhaustive-deps': 'warn', // Checks effect dependencies
    'prefer-const': 'off',
    'no-param-reassign': 'off',
    'spaced-comment': 'off',
    'eqeqeq': 'off',
  },
};
