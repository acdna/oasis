#!/bin/bash

echo "本脚本用于编译和打包前端和后端程序，请使用yarn install命令下载依赖!"

#定义版本号
VERSION=v1.0.0-`date +%Y%m%d%H%M%S`

#获取整个项目的根目录
cd ..
ROOT_DIR=`pwd`
echo "根目录路径==>$ROOT_DIR"

#定义前端和后端程序文件主目录
FC_DIR_NAME=front-end
BC_DIR_NAME=back-end
FRONT_END_DIR=$ROOT_DIR/$FC_DIR_NAME
BACK_END_DIR=$ROOT_DIR/$BC_DIR_NAME
FC_TAR_GZ=$FC_DIR_NAME.tar.gz
BC_TAR_GZ=$BC_DIR_NAME.tar.gz
DEPLOY_DIR=deploy
DEPLOY_TAR_GZ=$DEPLOY_DIR/oasis-$VERSION.tar.gz

#编译和打包后端程序
cd $BACK_END_DIR
echo "开始编译后端代码==>$BACK_END_DIR"
yarn install
yarn build
echo "完成后端代码的编译==>$BACK_END_DIR"

#编译和打包前端程序
cd $FRONT_END_DIR
echo "开始编译前端代码==>$FRONT_END_DIR"
yarn install
yarn pre:build
echo "完成前端代码的编译==>$FRONT_END_DIR"

#待打包的前端相关程序的目录和文件
BC_DIST=$BC_DIR_NAME/dist
BC_SRC=$BC_DIR_NAME/src
BC_CONFIG=$BC_DIR_NAME/config
BC_PUBLIC=$BC_DIR_NAME/public
BC_RESOURCES=$BC_DIR_NAME/resources
BC_PACKAGE=$BC_DIR_NAME/package.json
BC_TSCONFIG=$BC_DIR_NAME/tsconfig.json
BC_TSCONFIG_BUILD=$BC_DIR_NAME/tsconfig.build.json
BC_README=$BC_DIR_NAME/README.md
BC_YARN_LOCK=$BC_DIR_NAME/yarn.lock
BC_NODE_MODULES=$BC_DIR_NAME/node_modules

#若已存在压缩包则删除之
cd $ROOT_DIR
if [ -f "$ROOT_DIR/$BC_TAR_GZ" ]; then
  rm -rfv $ROOT_DIR/$BC_DIR_NAME.tar.gz
  echo "删除已存在的后端代码程序包==>$ROOT_DIR/$BC_TAR_GZ"
fi

#打包后端程序的目录和文件
#切换到后端项目目录
cd $ROOT_DIR
echo "开始压缩打包后端代码==>$ROOT_DIR/$BC_TAR_GZ"
tar -zcvf $BC_TAR_GZ $BC_SRC $BC_DIST $BC_CONFIG $BC_PUBLIC $BC_RESOURCES $BC_PACKAGE $BC_TSCONFIG $BC_TSCONFIG_BUILD $BC_README $BC_YARN_LOCK $BC_NODE_MODULES
echo "开始压缩打包后端代码==>$ROOT_DIR/$BC_TAR_GZ"

#待打包的前端相关程序的目录和文件
FC_DIST=$FC_DIR_NAME/dist/*

cd $ROOT_DIR

#若已存在压缩包则删除之
if [ -f "$ROOT_DIR/$FC_TAR_GZ" ]; then
  rm -rfv $ROOT_DIR/$FC_TAR_GZ
  echo "删除已存在的前端代码程序包==>$ROOT_DIR/$FC_TAR_GZ"
fi

#切换到前端项目目录
cd $ROOT_DIR

#打包后端程序的目录和文件
echo "开始压缩打包前端代码==>$ROOT_DIR/$FC_TAR_GZ"
tar -zcvf $FC_TAR_GZ $FC_DIST
echo "开始压缩打包前端代码==>$ROOT_DIR/$FC_TAR_GZ"

#打包前端和后端的两各程序包及dock-compose.yml、.env、docker等文件和目录
ROOT_DOCK_DIR=docker
ROOT_ENV=.env
ROOT_DOCKER_COMPOSE=docker-compose.yml
ROOT_README=README.md
ROOT_SQL=sql

#切换到项目根目录
cd $ROOT_DIR

#若已存在压缩包则删除之
if [ ! -f "$ROOT_DIR/$DEPLOY_DIR" ]; then
  mkdir -pv $ROOT_DIR/$DEPLOY_DIR
  echo "创建最终程序包目录==>$ROOT_DIR/$DEPLOY_DIR"
fi

#压缩成部署文件压缩包
echo "开始压缩打包前端和后端程序包等文件==>$ROOT_DIR/$DEPLOY_TAR_GZ"
tar -zcvf $DEPLOY_TAR_GZ $ROOT_DOCK_DIR $ROOT_ENV $ROOT_DOCKER_COMPOSE $ROOT_README $BC_TAR_GZ $FC_TAR_GZ $ROOT_SQL
echo "压缩打包前端和后端程序包等文件成功==>$ROOT_DIR/$DEPLOY_TAR_GZ"

#清除文件
echo "开始清理文件..."
rm -rfv $BC_TAR_GZ $FC_TAR_GZ
echo "完成清理文件!"
echo "打包前后端代码成功==>$ROOT_DIR/$DEPLOY_TAR_GZ"

