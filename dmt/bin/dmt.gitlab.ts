const axios = require('axios');
const https = require('https');
const moment = require('moment');
/**
 * 查询GitLab开发中的任务和关闭任务,maize项目ID为869702
 * @author: jiangbin
 * @date: 2020-12-28 17:31:53
 **/
const GIT_LAB = {
  JB_TOKEN: 'A--oDYLAgukJx6k4GxqH',
  WUHT_TOKEN: 'CM75FTuoY3sX-6DUz7MK',
  OPENED_URL: 'https://gitlab.com/api/v4/projects/869702/issues?state=opened',
  CLOSED_URL: 'https://gitlab.com/api/v4/projects/869702/issues?state=closed',
  /**
   * 查询gitlab开发中的任务
   * @param token
   * @param userName
   * @return {Promise<T | void>}
   */
  queryOpened: (token, userName) => {
    return axios.get(GIT_LAB.OPENED_URL, {
      params: {private_token: token, assignee_username: userName},
    }).then(function (response) {
      let results = response.data;
      console.log(`打印【${userName}】开发中的任务列表:`);
      results.forEach(result => {
        console.log(`Fixed acdna/maize#${result.iid}:${result.title}`);
      });
    }).catch(function (error) {
      console.log(error);
    });
  },
  /**
   * 查询gitlab关闭的任务
   * @param token
   * @param userName
   * @param userNameEn
   * @return {Promise<T | void>}
   */
  queryClosed: (token, userName, day, projectName) => {
    let date = moment().set({day: -day}).format();
    return axios.get(GIT_LAB.CLOSED_URL, {
      params: {
        private_token: token,
        assignee_username: userName,
        labels: `I-Do,II-Plan,III-Avoid,${projectName}`,
        created_after: date,
      },
    }).then(function (response) {
      let results = response.data;
      console.log(`打印【${userName}】已关闭的任务列表:`);
      results.forEach(result => {
        console.log(`Fixed acdna/maize#${result.iid}:${result.title}`);
      });
    }).catch(function (error) {
      console.log(error);
    });
  },
};

GIT_LAB.queryOpened(GIT_LAB.JB_TOKEN, 'jiangbinboy');
//GIT_LAB.queryOpened(GIT_LAB.WUHT_TOKEN, 'nghsky');
//GIT_LAB.queryClosed(GIT_LAB.JB_TOKEN, 'jiangbinboy', 'SSR3', 3);
//GIT_LAB.queryClosed(GIT_LAB.WUHT_TOKEN, 'nghsky', 'MIPMS3', 3);