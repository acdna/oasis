import {createConnection} from 'typeorm';
import {
  BE_CLASS_NAME,
  camelCase,
  firstUpperCase,
  getColJsType,
  getEntityFileName,
  getModualName,
  getRestUrlPrefix,
  hasPrimerKey,
  isCreateTimeCol,
  isEnum,
  isLongString,
  isPasswordCol,
  isPrimerKey,
  isUpdateTimeCol,
} from './dmt.string.utils';
import {ORM_CONF} from './dmt.constants';
import {createDDL, createFiles} from './dmt.js.saver';
import {JsonUtils} from "./dmt.json.utils";


/**
 * mysql数据库表和列元信息查询
 */

/**
 * 数据库表信息
 *
 * @interface databaseTable
 */
export interface Table {
  tableName: string;
  TABLE_CATALOG: string;
  TABLE_SCHEMA: string;
  TABLE_NAME: string;
  TABLE_TYPE: string;
  ENGINE: string;
  VERSION: string;
  ROW_FORMAT: string;
  TABLE_ROWS: string;
  AVG_ROW_LENGTH: string;
  DATA_LENGTH: string;
  MAX_DATA_LENGTH: string;
  INDEX_LENGTH: string;
  DATA_FREE: string;
  AUTO_INCREMENT: string;
  CREATE_TIME: string;
  UPDATE_TIME: string;
  CHECK_TIME: string;
  TABLE_COLLATION: string;
  CHECKSUM: string;
  CREATE_OPTIONS: string;
  TABLE_COMMENT: string;
  ENTITY_CLASS_NAME: string;//实体类名如：SsrSample
  ENTITY_FILE_NAME: string; //实体类文件名前缀如：ssr.sample
  MODULE_NAME: string;//表所属模块名
  REST_URL_PREFIX: string;//表REST格式URL前缀
  HAS_PRIMER_KEY: boolean;//存在ID主键
}

/**
 * 数据库表对应的字段信息
 *
 * @interface databaseTableColumn
 */
export interface Column {
  columnName: string;
  TABLE_CATALOG: string;
  TABLE_SCHEMA: string;
  TABLE_NAME: string;
  COLUMN_NAME: string;
  ORDINAL_POSITION: string;
  COLUMN_DEFAULT: string;
  IS_NULLABLE: string;
  DATA_TYPE: string;
  CHARACTER_MAXIMUM_LENGTH: string;
  CHARACTER_OCTET_LENGTH: string;
  NUMERIC_PRECISION: string;
  NUMERIC_SCALE: string;
  DATETIME_PRECISION: string;
  CHARACTER_SET_NAME: string;
  COLLATION_NAME: string;
  COLUMN_TYPE: string;
  COLUMN_KEY: string;
  EXTRA: string;
  PRIVILEGES: string;
  COLUMN_COMMENT: string;
  GENERATION_EXPRESSION: string;
  JS_TYPE: string;//列数据对应的JS类型
  COLUMN_LEN: number;//列长度
  HAS_COLUMN_LEN: boolean;//是否长度大于0
  IS_PRIMER_KEY: boolean;//是否为主键
  IS_UPDATE_TIME: boolean;//是否为更新时间字段
  IS_CREATE_TIME: boolean;//是否为创建时间字段
  ENTITY_CLASS_NAME: string;//实体类名如：SsrSample
  ENTITY_FILE_NAME: string; //实体类文件名前缀如：ssr.sample
  MODULE_NAME: string;//表所属模块名
  COLUMN_CAML_CASE: string//驼峰格式，首字母大写，如sam_name==>SamName,用于生成模糊查询条件
  IS_PASSWORD_COL: boolean;//是否为密码字段
}

/**
 * 启动DMT构建工具
 * @param ormConf
 */
export var startDmt = () => {
  const sql = `select * from information_schema.tables where table_schema='${ORM_CONF.database}' and table_type='base table'`;
  createConnection(ORM_CONF).then(async connect => {
    const startTime = Date.now();
    //查询表数据
    let tables: Table[] = await connect.manager.query(sql);

    //校正表数据
    reviseTables(tables);

    //过滤掉视图
    const views: string[] = await findViews(ORM_CONF.database, connect);
    if (views && views.length > 0) {
      tables = tables.filter(table => {
        if (views.indexOf(table.TABLE_NAME.toLowerCase()) == -1) {
          return table;
        }
      });
    }

    let ddlMap = new Map<string, string[]>();

    //遍历表数据
    for (let table of tables) {

      //表的删除语句
      let deleteDDL = `DROP TABLE IF EXISTS \`${table.TABLE_NAME}\``;
      addDDL(table.MODULE_NAME, deleteDDL, ddlMap);

      //表的创建语句
      let tableDDl = await findCreateTableDDL(connect, table);
      addDDL(table.MODULE_NAME, tableDDl, ddlMap);

      //表的触发器定义
      let triggerDDl = await findTriggerDDL(connect, ORM_CONF.database, [table.TABLE_NAME]);
      addDDL(table.MODULE_NAME, triggerDDl, ddlMap);

      //查询列数据
      await findColumns(ORM_CONF.database, table, connect).then((cols) => {
        //忽略未定义主键的表
        table.HAS_PRIMER_KEY = hasPrimerKey(cols);
        if (!table.HAS_PRIMER_KEY) {
          console.log(`数据库【${ORM_CONF.database}】中的表【${table.TABLE_NAME.toLowerCase()}】未包含主键，已忽略!`);
          return;
        }

        //校正数据
        cols = reviseCol(cols);

        //创建各种类代码文件
        createFiles(table, cols, tables);
        console.log(`数据库【${ORM_CONF.database}】中的表【${table.TABLE_NAME.toLowerCase()}】已生成!`);
      });
    }

    //创建DDL信息文件
    createDDL(ddlMap);

    //关闭数据库连接
    await connect.close();

    let time = (Date.now() - startTime) / 1000;//计算代码耗时
    console.log(`一共创建了【${tables.length}】张表对应的类代码，一共用时【${time}】秒!`);
  });
};

/**
 * 添加DDL信息
 *
 * @param module
 * @param ddl
 * @param ddlMap
 * @return
 * @author jiang
 * @date 2021-01-30 15:42:54
 **/
export let addDDL = (module: string, ddl: string, ddlMap: Map<string, string[]>) => {
  if (!ddl || ddl.length == 0) return;
  let tItems = ddlMap.get(module);
  if (!tItems) tItems = [];
  tItems.push(ddl);
  ddlMap.set(module, tItems);
}

/**
 * 查询表字段
 * @param database
 * @param table
 * @param connect
 */
export var findColumns = async (database: string, table: Table, connect) => {
  const sql = `select  * from information_schema.columns where table_schema ='${database}'  and table_name = '${table.TABLE_NAME}'`;
  const columns: Column[] = await connect.manager.query(sql);
  return columns;
};

/**
 * 查询建表DDL定义
 * @param connect
 * @param table
 * @return
 */
export let findCreateTableDDL = async (connect, table: Table) => {
  const sql = `show create table ${table.TABLE_NAME}`;
  let results = await connect.manager.query(sql);
  return JsonUtils.first(results)['Create Table'];
};

/**
 * 查询触发器DDL定义
 *
 * @param connect
 * @param database
 * @param tableNames
 * @return
 * @author jiang
 * @date 2021-01-30 13:51:19
 **/
export let findTriggerDDL = async (connect, database: string, tableNames: string[]) => {
  let sql = `select * from information_schema.triggers where TRIGGER_SCHEMA = ' ${database} '`;
  if (tableNames && tableNames.length > 0) {
    sql = `${sql} and EVENT_OBJECT_TABLE in (${tableNames.map(tableName => `'${tableName}'`).join(',')}) `;
  }
  let results = await connect.manager.query(sql);
  let first = JsonUtils.first(results);
  return first ? first['TRIGGER_NAME'] : '';
}


/**
 * 查询视图名列表
 * @param database
 * @param connect
 */
export var findViews = async (database: string, connect) => {
  const sql = `select  TABLE_NAME from information_schema.views where table_schema ='${database}'`;
  const views: string[] = await connect.manager.query(sql);
  let viewNames = [];
  views.forEach(value => {
    viewNames.push(value['TABLE_NAME'].toLowerCase());
  });
  return viewNames;
};

/**
 * 校正列数据中的列名为驼峰字符串和设置列的JS类型
 * @param columns
 */
export const reviseCol = (columns: Column[]): Column[] => {
  let newColumns = [];
  columns.forEach(column => {
    //备注修正，只取第一个逗号、括号等特殊字符前面的部分
    column.COLUMN_COMMENT = column.COLUMN_COMMENT.match(/([^,（\(\.;_\-，\[:]*)/)[0];

    //列名大写
    column.COLUMN_NAME = column.COLUMN_NAME.toUpperCase();

    //改成驼峰格式列名
    column.columnName = camelCase(column.COLUMN_NAME);

    //设置字段对应的JS类型
    column.JS_TYPE = getColJsType(column);

    //是否为主键
    column.IS_PRIMER_KEY = isPrimerKey(column);

    //是否为更新日期字段
    column.IS_UPDATE_TIME = isUpdateTimeCol(column);
    if (column.IS_UPDATE_TIME) column.COLUMN_COMMENT = '更新日期';

    //是否为创建日期字段
    column.IS_CREATE_TIME = isCreateTimeCol(column);
    if (column.IS_CREATE_TIME) column.COLUMN_COMMENT = '创建日期';

    column.ENTITY_CLASS_NAME = BE_CLASS_NAME.getEntityClassName(column.TABLE_NAME);
    column.ENTITY_FILE_NAME = getEntityFileName(column.TABLE_NAME);
    column.MODULE_NAME = getModualName(column.TABLE_NAME);
    column.IS_PASSWORD_COL = isPasswordCol(column);

    //首字母大写的驼峰格式列名称，如:SamName
    column.COLUMN_CAML_CASE = firstUpperCase(column.columnName);

    if (isLongString(column) || isEnum(column)) {//长字符串和枚举类型不能设置长度属性值
      column.HAS_COLUMN_LEN = false;
      column.COLUMN_LEN = 0;
    } else {//解析出列长度，用于实体类作验证
      let len = Number(column.CHARACTER_MAXIMUM_LENGTH);
      column.HAS_COLUMN_LEN = len > 0;
      column.COLUMN_LEN = column.HAS_COLUMN_LEN ? len : 0;
    }
    newColumns.push(column);
  });
  return newColumns;
};

/**
 * 校正表信息，如设置驼峰格式的表名
 * @param table
 */
export var reviseTable = (table: Table) => {
  table.TABLE_NAME = table.TABLE_NAME.toUpperCase();
  table.tableName = camelCase(table.TABLE_NAME);
  table.ENTITY_CLASS_NAME = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
  table.ENTITY_FILE_NAME = getEntityFileName(table.TABLE_NAME);
  table.MODULE_NAME = getModualName(table.TABLE_NAME);
  table.REST_URL_PREFIX = getRestUrlPrefix(table.TABLE_NAME);
  return table;
};

/**
 * 校正表信息，如设置驼峰格式的表名
 * @param table
 */
export var reviseTables = (tables: Table[]) => {
  tables.forEach(value => {
    reviseTable(value);
  });
};
