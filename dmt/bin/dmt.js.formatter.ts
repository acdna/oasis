import {Column, Table} from './dmt.db.utils';
import {ORM_CONF} from './dmt.constants';
import {
  BE_CLASS_NAME,
  camelCase,
  getEntityFileName,
  getFColumns,
  getFCUColumns,
  getFDColumns,
  getModualName,
  getPrimerKey,
  getRestUrlPrefix,
  getUpdateTimeCol,
  hasCreateTimeCol,
  hasNumberCol,
  hasPasswordCol,
  hasUpdateTimeCol,
} from './dmt.string.utils';

/**
 * 格式化JS代码格式
 */

const beautify = require('js-beautify').js;
const BEAUTIFY_CONFIG = { // 格式化代码的配置
  'indent_size': '4',
  'indent_char': ' ',
  'max_preserve_newlines': '-1',
  'preserve_newlines': false,
  'keep_array_indentation': false,
  'break_chained_methods': false,
  'indent_scripts': 'normal',
  'brace_style': 'none,preserve-inline',
  'space_before_conditional': true,
  'unescape_strings': false,
  'jslint_happy': false,
  'end_with_newline': false,
  'wrap_line_length': '0',
  'indent_inner_html': false,
  'comma_first': false,
  'e4x': false,
  'indent_empty_lines': false,
};

/**
 * 创建JS脚本文件
 * @param template
 * @param table
 * @param columns
 * @param tables
 * @param isBeautify 是否格式化js代码，默认为true，表示格式化代码
 */
export var createJs = (template: any, className: string, table: Table, columns: Column[], tables: Table[], isBeautify = true) => {
  let tableName = table.TABLE_NAME;
  let fileName = getEntityFileName(tableName);
  let entityFieldName = camelCase(tableName);
  let entityCLassName = BE_CLASS_NAME.getEntityClassName(tableName);
  let moduleName = getModualName(tableName);
  let moduleClassName = BE_CLASS_NAME.getModuleClassName(tableName);
  let restUrlPrefix = getRestUrlPrefix(tableName);
  let primerKey = getPrimerKey(columns);
  let updateCol = getUpdateTimeCol(columns);
  let updateTimeCol = hasUpdateTimeCol(columns);
  let createTimeCol = hasCreateTimeCol(columns);
  let hasTimeCol = updateTimeCol || createTimeCol;
  let hasPassword = hasPasswordCol(columns);
  let hasNumber = hasNumberCol(columns);
  let fcolumns = getFColumns(columns);
  let fdcolumns = getFDColumns(columns);//用于采用Drawer组件进行创建和更新记录的页面
  let fcucolumns = getFCUColumns(columns);//用于模态窗口组件进行创建和更新记录的页面
  let js = template({
    className,
    tableComment: table.TABLE_COMMENT,
    date: new Date().toLocaleString(),
    author: ORM_CONF.author,
    tableName,
    columns,
    fcolumns,
    fcucolumns,
    fdcolumns,
    addClassValidate: ORM_CONF.addClassValidate,
    entityCLassName,
    entityFileName: fileName,
    entityFieldName,
    restUrlPrefix,//URL前缀
    tables,
    moduleName,
    primerKey,
    updateCol,
    updateTimeCol,
    createTimeCol,
    hasTimeCol,
    moduleClassName,
    hasPassword,
    hasNumberCol: hasNumber,
  });
  if (!isBeautify) return js;//某些情况下不能格式化代码，如tsx页面文件
  return beautify(js, BEAUTIFY_CONFIG);//格式化生成的代码
};