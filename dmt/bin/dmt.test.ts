//这个文件用于测试一些脚本
import {SysConfigParser} from '../../back-end/src/sys/config/sys.config.parser';

let a = null;
let b;
let c = 'null';
console.log(null === a);
console.log(null === b);
console.log(null === c);
console.log(typeof c);

let str = `'甜椒('
            SP ')'`;
console.log(`===${str.replace(/[\r\n]/g, '')}===`);
console.log(`===${str.replace(/[\s]/g, '')}===`);
console.log(`===${str.replace(/[\']/g, '')}===`);
console.log(`===${str.replace(/[\'\s]/g, '')}===`);

let configs = SysConfigParser.parser();
console.log(JSON.stringify(configs));