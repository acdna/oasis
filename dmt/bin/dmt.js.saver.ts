import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import { ORM_CONF, TEMPLATES } from './dmt.constants';
import { createJs } from './dmt.js.formatter';
import { Column, Table } from './dmt.db.utils';
import { BE_CLASS_NAME, BE_CLASS_PATH, FE_CLASS_PATH } from './dmt.string.utils';

/**
 * 保存JS到文件中
 * @param table
 * @param filePath
 * @param js
 */
export var saveJs = (table: Table, filePath: string, js: string) => {
  const path = require('path');
  const dir = path.join(filePath, '..');
  mkdirp(dir, (err) => {//先递归创建文件夹
    if (err) {
      console.error(err);
    } else {
      fs.writeFileSync(filePath, js);
    }
  });
};

/**
 * 创建代码文件列表
 * @param table
 * @param cols
 * @param tables
 */
export let createFiles = (table: Table, cols: Column[], tables: Table[]) => {
  //后端文件
  BE_CREATE.createEntity(table, cols, tables);
  BE_CREATE.createColsDDL(table, cols, tables);
  BE_CREATE.createSQL(table, cols, tables);
  if (ORM_CONF.nestDto) {
    BE_CREATE.createCDto(table, cols, tables);
    BE_CREATE.createUDto(table, cols, tables);
    BE_CREATE.createQDto(table, cols, tables);
  }
  if (ORM_CONF.nestModule) {
    BE_CREATE.createModule(table, cols, tables);
    BE_CREATE.createModuleServices(table, cols, tables);
  }
  if (ORM_CONF.nestService) {
    BE_CREATE.createService(table, cols, tables);
  }
  if (ORM_CONF.nestController) {
    BE_CREATE.createController(table, cols, tables);
    BE_CREATE.createControllerE2E(table, cols, tables);
    BE_CREATE.createPms(table, cols, tables);
    BE_CREATE.createModulePms(table, cols, tables);
  }

  //前端文件
  if (ORM_CONF.FEService) {
    FE_CREATE.createService(table, cols, tables);
  }
  if (ORM_CONF.FEModel) {
    FE_CREATE.createModel(table, cols, tables);
  }
  if (ORM_CONF.FEPageData) {
    FE_CREATE.createPageData(table, cols, tables);
  }
  if (ORM_CONF.FERoutes) {
    FE_CREATE.createRoutes(table, cols, tables);
  }
  if (ORM_CONF.FECrud) {
    if (ORM_CONF.FEModal) {//采用模态窗口组件方式进行创建和更新记录
      FE_CREATE.createUpdateForm(table, cols, tables);
      FE_CREATE.createCreateForm(table, cols, tables);
    } else {//采用Drawer组件方式进行创建和更新记录
      FE_CREATE.createDrawerCreateForm(table, cols, tables);
      FE_CREATE.createDrawerUpdateForm(table, cols, tables);
    }
    FE_CREATE.createListForm(table, cols, tables);
    FE_CREATE.createPageLocaleEn(table, cols, tables);
    FE_CREATE.createPageLocaleZh(table, cols, tables);
    FE_CREATE.createLocaleEn(table, cols, tables);
    FE_CREATE.createLocaleZh(table, cols, tables);
    FE_CREATE.createPms(table, cols, tables);
    FE_CREATE.createModulePms(table, cols, tables);
    FE_CREATE.createHandles(table, cols, tables);
    FE_CREATE.createEffects(table, cols, tables);
    FE_CREATE.createConnect(table, cols, tables);
    FE_CREATE.createValidator(table, cols, tables);
    FE_CREATE.createDDL(table, cols, tables);
  }
};

/**
 * 创建DDL文件
 *
 * @param ddlMap
 * @return
 * @author jiang
 * @date 2021-01-30 15:28:45
 **/
export let createDDL = (ddlMap: Map<string, string[]>) => {
  if (!ddlMap || ddlMap.size == 0) return;
  ddlMap.forEach((values: string[], key: string, map) => {
    BE_CREATE.createDDLFile(key, `${values.join(';\n\r\n')};`);
  });
};

/**
 * 前端创建器
 * @author: jiangbin
 * @date: 2020-12-11 12:10:57
 **/
export const FE_CREATE = {
  /**
   * 创建前端Service类
   * @param table
   * @param cols
   * @param tables
   */
  createService(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getServiceClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_SERVICE_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端pages目录中的data类
   * @param table
   * @param cols
   * @param tables
   */
  createPageData(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getPageDataClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_PAGE_DATA_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端Model类
   * @param table
   * @param cols
   * @param tables
   */
  createModel(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getModelClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_MODEL_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端routes配置信息
   * @param table
   * @param cols
   * @param tables
   */
  createRoutes(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getRoutesPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_ROUTES_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端更新记录表单组件
   * @param table
   * @param cols
   * @param tables
   */
  createUpdateForm(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getUpdateFormPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_UPDATE_FORM_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端创建记录表单组件
   * @param table
   * @param cols
   * @param tables
   */
  createCreateForm(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getCreateFormPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_CREATE_FORM_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },
  /**
   * 创建前端更新记录表单组件
   * @param table
   * @param cols
   * @param tables
   */
  createDrawerUpdateForm(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getUpdateFormPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_UPDATE_DRAWER_FORM_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端创建记录表单组件
   * @param table
   * @param cols
   * @param tables
   */
  createDrawerCreateForm(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getCreateFormPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_CREATE_DRAWER_FORM_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端查询列表表单组件
   * @param table
   * @param cols
   * @param tables
   */
  createListForm(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getListFormPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_LIST_FORM_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端页面增删改操作方法类
   * @param table
   * @param cols
   * @param tables
   */
  createHandles(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getHandlesClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getHandlesClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_HANDLES_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },
  /**
   * 创建前端页面增删改操作方法类
   * @param table
   * @param cols
   * @param tables
   */
  createEffects(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEffectsClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getEffectsClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_EFFECT_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端页面Model注册类connect.d.ts
   * @param table
   * @param cols
   * @param tables
   */
  createConnect(table: Table, cols: Column[], tables: Table[]) {
    let className = 'connect.d.ts';
    let filePath = FE_CLASS_PATH.getConnectClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_CONNECT_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },
  /**
   * 创建前端页面字段校验类validator.d.ts
   * @param table
   * @param cols
   * @param tables
   */
  createValidator(table: Table, cols: Column[], tables: Table[]) {
    let className = 'validator.tsx';
    let filePath = FE_CLASS_PATH.getValidatorClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_VALIDATOR_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端使用的表字段DDL定义文件
   * @param table
   * @param cols
   * @param tables
   */
  createDDL(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getDDLClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_DDL_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端页面国际化英文文件
   * @param table
   * @param cols
   * @param tables
   */
  createPageLocaleEn(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getPageLocaleEnPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_PAGES_LOCALE_EN_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端页面国际化中文文件
   * @param table
   * @param cols
   * @param tables
   */
  createPageLocaleZh(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getPageLocaleZhPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_PAGES_LOCALE_ZH_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端国际化中文集成文件
   * @param table
   * @param cols
   * @param tables
   */
  createLocaleZh(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getLocaleZhPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_ZH_CN_LOCALE_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端国际化英文集成文件
   * @param table
   * @param cols
   * @param tables
   */
  createLocaleEn(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getLocaleEnPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_EN_US_LOCALE_TPL, className, table, cols, tables, false);
    saveJs(table, filePath, js);
  },


  /**
   * 创建前端页面功能权限定义文件
   * @param table
   * @param cols
   * @param tables
   */
  createPms(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getPmsClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getPmsClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_PMS_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建前端页面功能权限类的模块集中注册文件
   * @param table
   * @param cols
   * @param tables
   */
  createModulePms(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getModulePmsClassName(table.TABLE_NAME);
    let filePath = FE_CLASS_PATH.getModulePmsClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.FE_MODULE_PMS_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

};

/**
 * 后端创建器
 * @author: jiangbin
 * @date: 2020-12-11 12:10:57
 **/
export const BE_CREATE = {
  /**
   * 创建前端Service类
   * @param table
   * @param cols
   * @param tables
   */
  createFrontEndService(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getQDtoClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_QDTO_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建QDTO数据传输类
   * @param table
   * @param cols
   * @param tables
   */
  createQDto(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getQDtoClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_QDTO_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },


  /**
   * 创建CDTO数据传输类
   * @param table
   * @param cols
   * @param tables
   */
  createCDto(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getCDtoClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_CDTO_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建UDTO数据传输类
   * @param table
   * @param cols
   * @param tables
   */
  createUDto(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getUDtoClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_UDTO_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建实体类
   * @param table
   * @param cols
   * @param tables
   */
  createEntity(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getEntityClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getEntityClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_ENTITY_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建实体类对应的表列名枚举类
   * @param table
   * @param cols
   * @param tables
   */
  createColsDDL(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getColsDDLClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getColsDDLClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_COLUMNS_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建SQL脚本类
   * @param table
   * @param cols
   * @param tables
   */
  createSQL(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getSQLClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getSQLClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_SQL_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建Service
   * @param table
   * @param cols
   * @param tables
   */
  createService(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getServiceClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getServiceClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_SERVICE_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建Controller
   * @param table
   * @param cols
   * @param tables
   */
  createController(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getControllerClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getControllerClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_CONTROLLER_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建Controller的E2E测试文件
   * @param table
   * @param cols
   * @param tables
   */
  createControllerE2E(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getControllerE2EClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getControllerE2EClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_CONTROLLER_E2E_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建功能权限定义文件
   * @param table
   * @param cols
   * @param tables
   */
  createPms(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getPmsClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getPmsClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_PMS_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建功能权限类的模块集中注册文件
   * @param table
   * @param cols
   * @param tables
   */
  createModulePms(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getModulePmsClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getModulePmsClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_MODULE_PMS_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建Module
   * @param table
   * @param cols
   * @param tables
   */
  createModule(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getModuleClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getModuleClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_MODULE_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建Module
   * @param table
   * @param cols
   * @param tables
   */
  createModuleServices(table: Table, cols: Column[], tables: Table[]) {
    let className = BE_CLASS_NAME.getModuleServicesClassName(table.TABLE_NAME);
    let filePath = BE_CLASS_PATH.getModuleServicesClassPath(table.TABLE_NAME);
    let js = createJs(TEMPLATES.BE_MODULE_SERVICE_TPL, className, table, cols, tables);
    saveJs(table, filePath, js);
  },

  /**
   * 创建DDL文件
   * @param module
   * @param ddlContents
   */
  createDDLFile(module: string, ddlContents: string) {
    let filePath = BE_CLASS_PATH.getDDLClassPath(module);
    saveJs(undefined, filePath, ddlContents);
  },
};