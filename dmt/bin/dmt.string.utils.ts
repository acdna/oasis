//字符串处理
import {Column} from './dmt.db.utils';
import {PROJECT} from './dmt.constants';
import {SpliterUtils} from "./dmt.spliter.utils";

const path = require('path');

/**
 * 将下划线命名转换为驼峰命名
 * @description 'SAM_NAME' => 'samName'
 * @param {string} string
 */
export const camelCase = (str: string) => {
  return str.toLowerCase().replace(/_([a-z])/g, function (all: string, letter: string) {
    return letter.toUpperCase();
  });
};

/**
 * 第一个字母大写
 * @param str
 */
export const firstUpperCase = (str: string) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

/**
 * 获取表名中的模块名前缀，例如：SSR_SAMPLE => ssr
 * @param tableName
 */
export const getModualName = (tableName: string) => {
  let arr = tableName.split('_');
  if (arr.length >= 1) {
    return arr[0].toLowerCase();
  }
  return tableName.toLowerCase();
};

/**
 * 获取实体类文件名称，不包含扩展名，如:ssr_sample==>ssr.sample
 * @param tableName
 */
export const getEntityFileName = (tableName: string) => {
  return tableName.toLowerCase().replace(/(\-|_)/g, '.');
};

/**
 * 获取后端类文件名
 * @author: jiangbin
 * @date: 2020-12-11 11:18:05
 **/
export const BE_CLASS_NAME = {
  /**
   * 获取后端实体类名称
   * @param tableName
   */
  getEntityClassName(tableName: string) {
    let str = camelCase(tableName);
    return str.charAt(0).toUpperCase() + str.substr(1);
  },

  /**
   * 获取后端QDto类名称
   * @param tableName
   */
  getQDtoClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}QDto`;
  },

  /**
   * 获取后端CDto类名称
   * @param tableName
   */
  getCDtoClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}CDto`;
  },

  /**
   * 获取后端UDto类名称
   * @param tableName
   */
  getUDtoClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}UDto`;
  },

  /**
   * 获取模块类名
   * @param tableName
   */
  getModuleClassName(tableName: string) {
    return firstUpperCase(`${getModualName(tableName)}Module`);
  },

  /**
   * 获取模块静态服务类名
   * @param tableName
   */
  getModuleServicesClassName(tableName: string) {
    return firstUpperCase(`${getModualName(tableName)}Services`);
  },

  /**
   * 获取后端Service类名
   * @param tableName
   */
  getServiceClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}Service`;
  },

  /**
   * 获取后端控制器类名
   * @param tableName
   */
  getControllerClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}Controller`;
  },

  /**
   * 获取E2E测试类名，注：这个参数实际没用上
   * @param tableName
   */
  getControllerE2EClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}ControllerTest`;
  },
  /**
   * 获取功能权限定义类名
   * @param tableName
   */
  getPmsClassName(tableName: string) {
    return `${tableName.toUpperCase()}_PMS`;
  },
  /**
   * 获取模块中功能权限注册类名
   * @param tableName
   */
  getModulePmsClassName(tableName: string) {
    return `${getModualName(tableName)}_PMS`;
  },

  /**
   * 获取增删改操作方法类名
   * @param tableName
   */
  getHandlesClassName(tableName: string) {
    return `handles`;
  },

  /**
   * 获取effects类名
   * @param tableName
   */
  getEffectsClassName(tableName: string) {
    return `effects`;
  },

  /**
   * 获取后端表的列名枚举类名
   * @param tableName
   */
  getColsDDLClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}Cols`;
  },

  /**
   * 获取后端SQL脚本类名
   * @param tableName
   */
  getSQLClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}SQL`;
  },
};

/**
 * 后端类文件路径
 * @author: jiangbin
 * @date: 2020-12-11 11:21:23
 **/
export const BE_CLASS_PATH = {
  /**
   * 获取后端实体类文件称
   * @param tableName
   */
  getEntityClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/entity/${fileName}.entity.ts`);
  },

  /**
   * 获取模块类文件名
   * @param tableName
   */
  getModuleClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/${moduleName}.module.ts`);
  },

  /**
   * 获取模块集中注册的静态服务类文件名
   * @param tableName
   */
  getModuleServicesClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/${moduleName}.services.ts`);
  },

  /**
   * 获取后端QDto对象，用于前端查询记录
   * @param tableName
   */
  getQDtoClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/dto/query/${fileName}.qdto.ts`);
  },

  /**
   * 获取CDto类路径，用于前端创建记录
   * @param tableName
   */
  getCDtoClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/dto/create/${fileName}.cdto.ts`);
  },

  /**
   * 获取后端UDto类路径，用于前端更新记录
   * @param tableName
   */
  getUDtoClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/dto/update/${fileName}.udto.ts`);
  },

  /**
   * 获取后端Service类文件名
   * @param tableName
   */
  getServiceClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/service/${fileName}.service.ts`);
  },

  /**
   * 获取后端控制器类文件名
   * @param tableName
   */
  getControllerClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/controller/${fileName}.controller.ts`);
  },

  /**
   * 获取后端E2E测试文件路径
   * @param tableName
   */
  getControllerE2EClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/test/${moduleName}/${fileName}.controller.e2e-spec.ts`);
  },

  /**
   * 获取后端模块中权限定义类文件路径
   * @param tableName
   */
  getPmsClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/permission/${fileName}.pms.ts`);
  },

  /**
   * 获取后端模块中所有权限定义类的注册类文件路径
   * @param tableName
   */
  getModulePmsClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/permission/${moduleName}.pms.ts`);
  },

  /**
   * 获取后端列名枚举类文件路径
   * @param tableName
   */
  getColsDDLClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/entity/ddl/${fileName}.cols.ts`);
  },

  /**
   * 获取后端SQL脚本类文件名
   * @param tableName
   */
  getSQLClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/back-end/${moduleName}/entity/sql/${fileName}.sql.ts`);
  },

  /**
   * 获取后端各模块的DDL文件路径
   *
   * @param moduleName
   * @return
   * @author jiang
   * @date 2021-01-30 15:35:44
   **/
  getDDLClassPath(moduleName: string) {
    return path.join(PROJECT, `./dist/back-end/${moduleName}/entity/sql/${moduleName}.ddl.sql`);
  },

};

/**
 * 获取前端类文件名
 * @author: jiangbin
 * @date: 2020-12-11 11:18:05
 **/
export const FE_CLASS_NAME = {
  /**
   * 获取前端Service类名
   * @param tableName
   */
  getServiceClassName(tableName: string) {
    return `${this.getEntityClassName(tableName)}Service`;
  },
};

/**
 * 前端类文件路径
 * @author: jiangbin
 * @date: 2020-12-11 11:51:58
 **/
export const FE_CLASS_PATH = {
  /**
   * 获取前端Service类文件名
   * @param tableName
   */
  getServiceClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/front-end/services/${moduleName}/${fileName}.service.ts`);
  },

  /**
   * 获取前端Model类文件名
   * @param tableName
   */
  getModelClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/front-end/models/${moduleName}/${fileName}.model.ts`);
  },

  /**
   * 获取前端routes文件名
   * @param tableName
   */
  getRoutesPath(tableName: string) {
    let moduleName = getModualName(tableName);
    return path.join(PROJECT, `./dist/front-end/config/${moduleName}.routes.ts`);
  },

  /**
   * 获取前端Update表单文件名
   * @param tableName
   */
  getUpdateFormPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/update.form.tsx`);
  },

  /**
   * 获取前端Create表单文件名
   * @param tableName
   */
  getCreateFormPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/create.form.tsx`);
  },
  /**
   * 获取前端Update表单文件名
   * @param tableName
   */
  getUpdateDrawerFormPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/update.drawer.form.tsx`);
  },

  /**
   * 获取前端Create表单文件名
   * @param tableName
   */
  getCreateDrawerFormPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/create.drawer.form.tsx`);
  },

  /**
   * 获取前端List表单文件名
   * @param tableName
   */
  getListFormPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/index.tsx`);
  },

  /**
   * 获取前端中文国际化信息集中加载文件
   * @param tableName
   */
  getHandlesClassPath(tableName: string) {
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/handles.ts`);
  },
  /**
   * 获取前端effects文件
   * @param tableName
   */
  getEffectsClassPath(tableName: string) {
    let fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/effects.tsx`);
  },

  /**
   * 获取前端Data类文件名
   * @param tableName
   */
  getPageDataClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/data.ts`);
  },

  /**
   * 获取前端页面英文国际化信息文件
   * @param tableName
   */
  getPageLocaleEnPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/front-end/locales/en-US/${moduleName}/${fileName}.ts`);
  },

  /**
   * 获取前端页面中文国际化信息文件
   * @param tableName
   */
  getPageLocaleZhPath(tableName: string) {
    let moduleName = getModualName(tableName);
    let fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/front-end/locales/zh-CN/${moduleName}/${fileName}.ts`);
  },

  /**
   * 获取前端英文国际化信息集中加载文件
   * @param tableName
   */
  getLocaleEnPath(tableName: string) {
    return path.join(PROJECT, `./dist/front-end/locales/en-US.ts`);
  },

  /**
   * 获取前端中文国际化信息集中加载文件
   * @param tableName
   */
  getLocaleZhPath(tableName: string) {
    return path.join(PROJECT, `./dist/front-end/locales/zh-CN.ts`);
  },

  /**
   * 获取前端页面功能权限信息文件
   * @param tableName
   */
  getPmsClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    const fileName = getEntityFileName(tableName);
    return path.join(PROJECT, `./dist/front-end/permission/${moduleName}/${fileName}.pms.ts`);
  },

  /**
   * 获取前端中文国际化信息集中加载文件
   * @param tableName
   */
  getModulePmsClassPath(tableName: string) {
    let moduleName = getModualName(tableName);
    return path.join(PROJECT, `./dist/front-end/permission/${moduleName}/${moduleName}.pms.ts`);
  },

  /**
   * 获取前端Models注册文件connect.d.ts
   * @param tableName
   */
  getConnectClassPath(tableName: string) {
    return path.join(PROJECT, `./dist/front-end/models/connect.d.ts`);
  },

  /**
   * 获取前端字段验证文件validator.tsx
   * @param tableName
   */
  getValidatorClassPath(tableName: string) {
    const fileName = getRestUrlPrefix(tableName);
    return path.join(PROJECT, `./dist/front-end/pages/${fileName}/validator.tsx`);
  },

  /**
   * 获取前端表字段定义DDL文件
   * @param tableName
   */
  getDDLClassPath(tableName: string) {
    const moduleName=getModualName(tableName);
    const entityName = BE_CLASS_NAME.getEntityClassName(tableName);
    return path.join(PROJECT, `./dist/front-end/ddl/${moduleName}/${entityName}.ts`);
  },

};

/**
 * 获取控制器类的URL前缀，如：SSR_SAMPLE==>ssr/sample
 * @param tableName
 */
export const getRestUrlPrefix = (tableName: string) => {
  return tableName.toLowerCase().replace(/(\-|_)/g, '/');
};

/**
 * 判定列类型
 * @param colTypes
 * @param col
 */
export const isColType = (colTypes: string[], col: Column) => {
  return colTypes.filter((value, index, array) => {
    return value.toUpperCase() == col.DATA_TYPE.toUpperCase();
  }).length > 0;
};

/**
 * 是否为日期列类型
 * @param col
 */
export const isDate = (col: Column) => {
  return isColType(['datetime', 'date', 'time', 'timestamp', 'year'], col);
};

/**
 * 是否为数字类型
 * @param col
 */
export const isNumber = (col: Column) => {
  return isColType(['int', 'tinyint', 'smallint', 'mediumint', 'bigint', 'float', 'double', 'decimal'], col);
};

/**
 * 是否为字符串类型列
 * @param col
 */
export const isString = (col: Column) => {
  return isColType(['varchar', 'char'], col);
};

/**
 * 是否为长字符串类型列
 * @param col
 */
export const isLongString = (col: Column) => {
  return isColType(['longtext', 'text', 'blob'], col);
};

/**
 * 是否为枚举类型
 * @param col
 */
export const isEnum = (col: Column) => {
  return isColType(['enum'], col);
};

/**
 * 是否包含主键
 * @param cols
 */
export const hasPrimerKey = (cols: Column[]) => {
  return cols.filter((col, index, array) => {
    return col.COLUMN_KEY == 'PRI';
  }).length > 0;
};

/**
 * 获取主键
 * @param cols
 */
export const getPrimerKey = (cols: Column[]): Column => {
  return cols.filter(value => {
    if (isPrimerKey(value)) {
      return value;
    }
  })[0];
};

/**
 * 是否存在更新日期字段
 * @param cols
 */
export const hasUpdateTimeCol = (cols: Column[]) => {
  return cols.filter(value => {
    if (isUpdateTimeCol(value)) {
      return value;
    }
  }).length > 0;
};

/**
 * 过滤出前端使用的列，即去掉主键列
 * @param cols
 */
export const getFColumns = (cols: Column[]) => {
  return cols.filter(value => {
    if (!isPrimerKey(value)) {
      return value;
    }
  });
};

/**
 * 过滤出前端Drawer页面使用的列，即去掉主键列，一行两个字段
 * @param cols
 */
export const getFDColumns = (cols: Column[]) => {
  cols = getFCUColumns(cols);
  return SpliterUtils.group(cols, 2);
};

/**
 * 过滤出前端创建和更新页面使用的列，即去掉主键列和日期列
 * @param cols
 */
export const getFCUColumns = (cols: Column[]) => {
  return cols.filter(value => {
    if (!isPrimerKey(value) && !isDate(value)) {
      return value;
    }
  });
};

/**
 * 获取更新日期字段
 * @param cols
 */
export const getUpdateTimeCol = (cols: Column[]) => {
  let targets = cols.filter(value => {
    if (isUpdateTimeCol(value)) {
      return value;
    }
  });
  return (targets?.length) > 0 ? targets[0] : null;
};

/**
 * 是否存在创建日期字段
 * @param cols
 */
export const hasCreateTimeCol = (cols: Column[]) => {
  return cols.filter(value => {
    if (isCreateTimeCol(value)) {
      return value;
    }
  }).length > 0;
};

/**
 * 是否存在数字字段
 * @param cols
 */
export const hasNumberCol = (cols: Column[]) => {
  return cols.filter(value => {
    if (isNumber(value)) {
      return value;
    }
  }).length > 0;
};

/**
 * 是否包含主键
 * @param cols
 */
export const isPrimerKey = (col: Column) => {
  return col.COLUMN_KEY == 'PRI';
};

/**
 * 是否为更新日期字段
 * @param col
 */
export const isUpdateTimeCol = (col: Column) => {
  return col.COLUMN_NAME.endsWith('UPDATE_DATE') || col.COLUMN_NAME.endsWith('UPDATE_TIME');
};

/**
 * 是否为密码字段
 * @param col
 */
export const isPasswordCol = (col: Column) => {
  const value = col.COLUMN_NAME.toLowerCase();
  return value.search('password') > 0 || value.search('passwd') > 0;
};

/**
 * 是否包含密码列
 * @param cols
 */
export const hasPasswordCol = (cols: Column[]) => {
  let target = false;
  cols.forEach(col => {
    if (target) return;
    target = isPasswordCol(col);
  });
  return target;
};

/**
 * 是否为创建日期字段
 * @param col
 */
export const isCreateTimeCol = (col: Column) => {
  return col.COLUMN_NAME.endsWith('CREATE_DATE') || col.COLUMN_NAME.endsWith('CREATE_TIME');
};

/**
 * 获取列对应的JS类型
 * @param col
 */
export const getColJsType = (col: Column) => {
  if (isString(col) || isLongString(col)) {
    return 'string';
  }
  if (isNumber(col)) {
    return 'number';
  }
  if (isDate(col)) {
    return 'Date';
  }
  return 'string';
};