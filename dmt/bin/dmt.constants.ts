import * as fs from 'fs';
import * as swig from 'swig-templates';

const path = require('path');

/**
 * 常量定义
 */

/**
 * 项目参数
 */
export const PROJECT = process.cwd();
console.log('项目根目录为==>' + PROJECT);

/**
 * 模板文件
 */
const EJS_FILE = {
  //后端文件模板
  BE_CDTO_EJS: 'back-end/create-dto-template.ejs',
  BE_UDTO_EJS: 'back-end/update-dto-template.ejs',
  BE_QDTO_EJS: 'back-end/query-dto-template.ejs',
  BE_ENTTY_EJS: 'back-end/entity-template.ejs',
  BE_SERVICE_EJS: 'back-end/service-template.ejs',
  BE_MODULE_SERVICE_EJS: 'back-end/module-services-template.ejs',
  BE_CONTROLLER_EJS: 'back-end/controller-template.ejs',
  BE_CONTROLLER_E2E_EJS: 'back-end/controller-e2e-template.ejs',
  BE_MODULE_EJS: 'back-end/module-template.ejs',
  BE_COLUMNS_EJS: 'back-end/entity-cols-template.ejs',
  BE_SQL_EJS: 'back-end/entity-sql-template.ejs',
  BE_PMS_EJS: 'back-end/pms-template.ejs',
  BE_MODULE_PMS_EJS: 'back-end/module-pms-template.ejs',
  //前端文件模板
  FE_SERVICE_EJS: 'front-end/service-template.ejs',
  FE_ROUTES_EJS: 'front-end/routes-template.ejs',
  FE_PAGE_DATA_EJS: 'front-end/data-template.ejs',
  FE_MODEL_EJS: 'front-end/model-template.ejs',
  FE_UPDATE_FORM_EJS: 'front-end/update-modal-template.ejs',
  FE_CREATE_FORM_EJS: 'front-end/create-modal-template.ejs',
  FE_UPDATE_DRAWER_FORM_EJS: 'front-end/update-drawer-template.ejs',
  FE_CREATE_DRAWER_FORM_EJS: 'front-end/create-drawer-template.ejs',
  FE_LIST_FORM_EJS: 'front-end/list-template.ejs',
  FE_PAGES_LOCALE_EN_EJS: 'front-end/pages-locale-en-template.ejs',
  FE_PAGES_LOCALE_ZH_EJS: 'front-end/pages-locale-zh-template.ejs',
  FE_ZH_CN_LOCALE_EJS: 'front-end/zh-CN-template.ejs',
  FE_EN_US_LOCALE_EJS: 'front-end/en-US-template.ejs',
  FE_PMS_EJS: 'front-end/pms-template.ejs',
  FE_MODULE_PMS_EJS: 'front-end/module-pms-template.ejs',
  FE_HANDLES_EJS: 'front-end/handles-template.ejs',
  FE_EFFECT_EJS: 'front-end/effects-template.ejs',
  FE_CONNECT_EJS: 'front-end/connect-template.ejs',
  FE_VALIDATOR_EJS: 'front-end/validator-template.ejs',
  FE_DDL_EJS: 'front-end/ddl-template.ejs',
};
/**
 * 模板文件路径
 */
const EJS_PATH = {
  //后端文件模板
  BE_CDTO_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_CDTO_EJS}`),
  BE_UDTO_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_UDTO_EJS}`),
  BE_QDTO_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_QDTO_EJS}`),
  BE_ENTITY_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_ENTTY_EJS}`),
  BE_SERVICE_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_SERVICE_EJS}`),
  BE_MODULE_SERVICE_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_MODULE_SERVICE_EJS}`),
  BE_CONTROLLER_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_CONTROLLER_EJS}`),
  BE_CONTROLLER_E2E_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_CONTROLLER_E2E_EJS}`),
  BE_MODULE_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_MODULE_EJS}`),
  BE_COLUMNS_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_COLUMNS_EJS}`),
  BE_SQL_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_SQL_EJS}`),
  BE_PMS_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_PMS_EJS}`),
  BE_MODULE_PMS_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.BE_MODULE_PMS_EJS}`),
  //前端文件模板
  FE_SERVICE_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_SERVICE_EJS}`),
  FE_ROUTES_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_ROUTES_EJS}`),
  FE_PAGE_DATA_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_PAGE_DATA_EJS}`),
  FE_MODEL_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_MODEL_EJS}`),
  FE_UPDATE_FORM_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_UPDATE_FORM_EJS}`),
  FE_CREATE_FORM_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_CREATE_FORM_EJS}`),
  FE_UPDATE_DRAWER_FORM_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_UPDATE_DRAWER_FORM_EJS}`),
  FE_CREATE_DRAWER_FORM_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_CREATE_DRAWER_FORM_EJS}`),
  FE_LIST_FORM_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_LIST_FORM_EJS}`),
  FE_PAGES_LOCALE_EN_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_PAGES_LOCALE_EN_EJS}`),
  FE_PAGES_LOCALE_ZH_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_PAGES_LOCALE_ZH_EJS}`),
  FE_ZH_CN_LOCALE_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_ZH_CN_LOCALE_EJS}`),
  FE_EN_US_LOCALE_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_EN_US_LOCALE_EJS}`),
  FE_PMS_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_PMS_EJS}`),
  FE_MODULE_PMS_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_MODULE_PMS_EJS}`),
  FE_HANDLES_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_HANDLES_EJS}`),
  FE_EFFECT_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_EFFECT_EJS}`),
  FE_CONNECT_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_CONNECT_EJS}`),
  FE_VALIDATOR_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_VALIDATOR_EJS}`),
  FE_DDL_TPL_PATH: path.join(PROJECT, `./templates/${EJS_FILE.FE_DDL_EJS}`),
};
/**
 * 模板对象
 */
export var TEMPLATES = {
  //后端文件模板
  BE_CDTO_TPL: swig.compileFile(EJS_PATH.BE_CDTO_TPL_PATH),
  BE_UDTO_TPL: swig.compileFile(EJS_PATH.BE_UDTO_TPL_PATH),
  BE_QDTO_TPL: swig.compileFile(EJS_PATH.BE_QDTO_TPL_PATH),
  BE_ENTITY_TPL: swig.compileFile(EJS_PATH.BE_ENTITY_TPL_PATH),
  BE_SERVICE_TPL: swig.compileFile(EJS_PATH.BE_SERVICE_TPL_PATH),
  BE_MODULE_SERVICE_TPL: swig.compileFile(EJS_PATH.BE_MODULE_SERVICE_TPL_PATH),
  BE_CONTROLLER_TPL: swig.compileFile(EJS_PATH.BE_CONTROLLER_TPL_PATH),
  BE_CONTROLLER_E2E_TPL: swig.compileFile(EJS_PATH.BE_CONTROLLER_E2E_TPL_PATH),
  BE_MODULE_TPL: swig.compileFile(EJS_PATH.BE_MODULE_TPL_PATH),
  BE_COLUMNS_TPL: swig.compileFile(EJS_PATH.BE_COLUMNS_TPL_PATH),
  BE_SQL_TPL: swig.compileFile(EJS_PATH.BE_SQL_TPL_PATH),
  BE_PMS_TPL: swig.compileFile(EJS_PATH.BE_PMS_TPL_PATH),
  BE_MODULE_PMS_TPL: swig.compileFile(EJS_PATH.BE_MODULE_PMS_TPL_PATH),
  //前端文件模板
  FE_SERVICE_TPL: swig.compileFile(EJS_PATH.FE_SERVICE_TPL_PATH),
  FE_ROUTES_TPL: swig.compileFile(EJS_PATH.FE_ROUTES_TPL_PATH),
  FE_PAGE_DATA_TPL: swig.compileFile(EJS_PATH.FE_PAGE_DATA_TPL_PATH),
  FE_MODEL_TPL: swig.compileFile(EJS_PATH.FE_MODEL_TPL_PATH),
  FE_UPDATE_FORM_TPL: swig.compileFile(EJS_PATH.FE_UPDATE_FORM_TPL_PATH),
  FE_CREATE_FORM_TPL: swig.compileFile(EJS_PATH.FE_CREATE_FORM_TPL_PATH),
  FE_UPDATE_DRAWER_FORM_TPL: swig.compileFile(EJS_PATH.FE_UPDATE_DRAWER_FORM_TPL_PATH),
  FE_CREATE_DRAWER_FORM_TPL: swig.compileFile(EJS_PATH.FE_CREATE_DRAWER_FORM_TPL_PATH),
  FE_LIST_FORM_TPL: swig.compileFile(EJS_PATH.FE_LIST_FORM_TPL_PATH),
  FE_PAGES_LOCALE_EN_TPL: swig.compileFile(EJS_PATH.FE_PAGES_LOCALE_EN_TPL_PATH),
  FE_PAGES_LOCALE_ZH_TPL: swig.compileFile(EJS_PATH.FE_PAGES_LOCALE_ZH_TPL_PATH),
  FE_ZH_CN_LOCALE_TPL: swig.compileFile(EJS_PATH.FE_ZH_CN_LOCALE_TPL_PATH),
  FE_EN_US_LOCALE_TPL: swig.compileFile(EJS_PATH.FE_EN_US_LOCALE_TPL_PATH),
  FE_PMS_TPL: swig.compileFile(EJS_PATH.FE_PMS_TPL_PATH),
  FE_MODULE_PMS_TPL: swig.compileFile(EJS_PATH.FE_MODULE_PMS_TPL_PATH),
  FE_HANDLES_TPL: swig.compileFile(EJS_PATH.FE_HANDLES_TPL_PATH),
  FE_EFFECT_TPL: swig.compileFile(EJS_PATH.FE_EFFECT_TPL_PATH),
  FE_CONNECT_TPL: swig.compileFile(EJS_PATH.FE_CONNECT_TPL_PATH),
  FE_VALIDATOR_TPL: swig.compileFile(EJS_PATH.FE_VALIDATOR_TPL_PATH),
  FE_DDL_TPL: swig.compileFile(EJS_PATH.FE_DDL_TPL_PATH),
};

/**
 * ORM参数
 */
export var ORM_CONF = getOrmConfig();

/**
 * 读取ORM配置文件参数信息
 */
export function getOrmConfig() {
  const ormConfPath = path.join(PROJECT, './ormconfig.json');
  const ormConfig = JSON.parse(fs.readFileSync(ormConfPath, 'utf8'));
  delete ormConfig.entities;
  delete ormConfig.migrations;
  delete ormConfig.subscribers;
  return ormConfig;
}

