import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import {JsonUtils} from "./dmt.json.utils";

const path = require('path');

const beautify = require('js-beautify').js;
const BEAUTIFY_CONFIG = { // 格式化代码的配置
  'indent_size': '4',
  'indent_char': ' ',
  'max_preserve_newlines': '-1',
  'preserve_newlines': false,
  'keep_array_indentation': false,
  'break_chained_methods': false,
  'indent_scripts': 'normal',
  'brace_style': 'none,preserve-inline',
  'space_before_conditional': true,
  'unescape_strings': false,
  'jslint_happy': false,
  'end_with_newline': false,
  'wrap_line_length': '0',
  'indent_inner_html': false,
  'comma_first': false,
  'e4x': false,
  'indent_empty_lines': false,
};

const mysql = require('mysql');

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'ssr3',
});

/**
 * 提供一个可使用await关键字进行同步数据库查询功能
 * @author: jiangbin
 * @date: 2020-12-22 12:50:41
 **/
const query = function (sql, params) {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        reject(err);
      } else {
        connection.query(sql, params, (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
          // 结束会话
          connection.release();
        });
      }
    });
  });
};

const dir_sql = 'select * from sys_dictionary where DIC_TYPE=\'DIR\' order by DIC_GROUP';
const data_sql = 'select * from sys_dictionary where DIC_TYPE=\'DATA\' and DIC_PARENT_ID=? order by DIC_ORDER';

/**
 * 查询数据库
 * @param filePath
 * @param modulaName
 * @author: jiangbin
 * @date: 2020-12-22 09:45:15
 **/
const queryDicts = async (filePath, modulaName) => {
  const start = Date.now();

  //查询DIR级数据
  let dirs = await query(dir_sql, []);
  let records = JsonUtils.format(dirs);

  let groups = [];

  let targets = [];
  for (const value of records) {
    let index = records.indexOf(value);

    let list = [];
    let rows = await query(data_sql, [value.DIC_ID]);
    if (!rows) {
      console.log('没查询到相关数据==>' + index);
      continue;
    }
    let items = JsonUtils.format(rows);
    let DIC_GROUP = '';
    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      DIC_GROUP = item.DIC_GROUP;
      const dicName = item.DIC_NAME?.replace(/[\'\s]/g, '');
      const dicValue = item.DIC_VALUE?.replace(/[\'\s]/g, '');
      list.push(`{ name: '${dicName}', nameEn: '${dicName}', value: '${dicValue}', order: '${item.DIC_ORDER}', type: '', species: '' }`);
    }
    if (DIC_GROUP == '' || !DIC_GROUP.startsWith(modulaName) || groups.includes(DIC_GROUP)) {
      continue;
    }

    //字典分组去重
    groups.push(DIC_GROUP);

    let str = `${DIC_GROUP.toUpperCase()}: {
          name: '${value.DIC_NAME}',
          nameEn: '${value.DIC_NAME}',
          group: '${DIC_GROUP.toUpperCase()}',
          enable: 'ON',
          items: [
            ${list.join(',\n')}
          ],
        },`;
    targets.push(str);
  }

  const js =
    `export const ${modulaName.toUpperCase()}_DICTS={${targets.join('')}}
   export const ${modulaName.toUpperCase()}_DICT_GROUPS=Object.keys(${modulaName.toUpperCase()}_DICTS);
   export const ${modulaName.toUpperCase()}_DICT_MODULE="${modulaName.toUpperCase()}";
  `;

  saveJs(filePath, js);

  console.log('[Query SUCCESS]--耗时==>' + ((Date.now() - start) / 1000) + ' s ');
};

/**
 * 保存脚本文件
 * @param filePath
 * @param js
 * @author: jiangbin
 * @date: 2020-12-22 11:16:29
 **/
export var saveJs = (filePath: string, js: string) => {
  //格式化代码
  js = beautify(js, BEAUTIFY_CONFIG);

  let targetPath = './dist/back-end/common/dictionary/' + filePath;
  const dir = path.join(targetPath, '..');
  console.log('写文件==>' + targetPath);
  mkdirp(dir, (err) => {//先递归创建文件夹
    if (err) {
      console.error(err);
    } else {
      fs.writeFileSync(targetPath, js);
    }
  });
};

queryDicts('sys.dict.js', 'sys').then(value => {
  console.log('SYS-处理完毕!');
});

queryDicts('ssr.dict.js', 'ssr').then(value => {
  console.log('SSR-处理完毕!');
});

