import { Logger } from '@nestjs/common';

let XLSX = require('node-xlsx');
let fs = require('fs');

/**
 * Excel处理
 * @author: jiangbin
 * @date: 2020-12-07 20:19:05
 **/
export class Excel {
  sheetCount: number;
  sheets: any[];

  /**
   * 写Excel文件
   * @author: jiangbin
   * @date: 2020-12-07 20:14:47
   **/
  write(workbook, path: string) {
    let buffer = XLSX.build(this.sheets);
    fs.writeFile(path, buffer, function (err) {
      if (err) {
        Logger.log('写Excel文件出错:', err);
        return;
      }
    });
  }

  /**
   * 解析Excel文件
   * @author: jiangbin
   * @date: 2020-12-07 20:14:28
   **/
  parse(path: string) {
    this.sheets = XLSX.parse(path);
    this.sheetCount = this.sheets.length;
    this.sheets.forEach((sheet, index) => {
      console.log('开始解析sheet==>' + index);
      this.parseSheet(sheet);
    });
  }

  /**
   * 解析Sheet的数据
   * @author: jiangbin
   * @date: 2020-12-07 20:49:14
   **/
  parseSheet(sheet) {
    let maxRowCIndex = 0;
    let maxColIndex = 0;
    sheet.data.forEach((row, rowIndex) => {
      if (rowIndex > maxRowCIndex) maxRowCIndex = rowIndex;
      row.forEach((col, colIndex) => {
        if (colIndex > maxColIndex) maxColIndex = colIndex;
        console.log(`[${rowIndex}行][${colIndex}列]==>${sheet.data[rowIndex][colIndex]}`);
      });
    });
    console.log(`行列数==>[${maxRowCIndex}行][${maxColIndex}列]`);
  }
}

let path = '/Volumes/work/js/workspace/scdms/back-end/resources/样品信息表.xls';
let excel = new Excel();
excel.parse(path);
