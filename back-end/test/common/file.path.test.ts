import { PathCreator } from '../../src/common/path/path.creator';

/**
 * 测试路径构建器
 * @author jiang
 * @date 2020-12-06 21:43:58
 **/
let options = {
  mainFolder: 'D://work/upload',
  subFolder: 'CommonFile',
  filePrefix: 'Prefix', //可以不给
  fileSuffix: 'Suffix', //可以不给
  fileExt: 'xls',
};
let path = PathCreator.file(options);
console.log(path);
path = PathCreator.dir(options);
console.log(path);

let options2 = {
  mainFolder: 'D://work/upload',
  subFolder: 'CommonFile',
  fileExt: 'xls',
};
path = PathCreator.file(options2);
console.log(path);
path = PathCreator.dir(options2);
console.log(path);

/**
 *
 //加前缀和后缀的常规路径
 {
  path: 'D://work/upload/CommonFile/2020/12/06/214624/Prefix/6ea6791037c911eb9ab5f5c0842858b5/Suffix.xls',
  mainFolder: 'D://work/upload',
  relativePath: 'CommonFile/2020/12/06/214624/Prefix/6ea6791037c911eb9ab5f5c0842858b5/Suffix.xls',
  isFile: false
 }
 {
  path: 'D://work/upload/CommonFile/2020/12/06/214624/Prefix/6ea73c6037c911eb9ab5f5c0842858b5/Suffix',
  mainFolder: 'D://work/upload',
  relativePath: 'CommonFile/2020/12/06/214624/Prefix/6ea73c6037c911eb9ab5f5c0842858b5/Suffix',
  isFile: false
 }
 //不加前缀和后缀的常规路径
 {
  path: 'D://work/upload/CommonFile/2020/12/06/214624/6ea7637037c911eb9ab5f5c0842858b5.xls',
  mainFolder: 'D://work/upload',
  relativePath: 'CommonFile/2020/12/06/214624/6ea7637037c911eb9ab5f5c0842858b5.xls',
  isFile: false
 }
 {
  path: 'D://work/upload/CommonFile/2020/12/06/214624/6ea78a8037c911eb9ab5f5c0842858b5',
  mainFolder: 'D://work/upload',
  relativePath: 'CommonFile/2020/12/06/214624/6ea78a8037c911eb9ab5f5c0842858b5',
  isFile: false
 }
 **/
