import { Excel } from '../../src/common/excel/tools/excel';
import { SysUser } from '../../src/sys/entity/sys.user.entity';

//测试示例代码
let excel = new Excel();
excel.write(
  [
    {
      name: 'sheet1',
      data: [
        [new Date(), 2, 3],
        [4, 'jiangbin', 6],
      ],
    },
    {
      name: 'sheet2',
      data: [
        [11, 22, '33'],
        ['44', 55, 66],
      ],
    },
  ],
  '/work/test.xlsx',
);

let arr = [];
arr.push([1, 2, 4, 2, 4]);
arr.push([3, 4, 43]);
console.log(arr);
arr.splice(0, 0, ['A', 'B', 'C']);
console.log(arr);

let str = 'xxxx/xadfa/xadf/.xls/aaa.xls';
console.log(str);
let index = str.lastIndexOf('.xls');
console.log(index);
console.log(str.substr(index));

let users = [];

let user = new SysUser();
user.userPassword = '2222';
user.userLoginName = '1';
user.userAddr = '3adads';
user.userEmail = 'fdsagfdsa';
users.push(user);

user = new SysUser();
user.userPassword = '2222';
user.userLoginName = '2';
user.userAddr = '3adads';
user.userEmail = 'fdsagfdsa';
users.push(user);

user = new SysUser();
user.userPassword = '2222';
user.userLoginName = '3';
user.userAddr = '3adads';
user.userEmail = 'fdsagfdsa';
users.push(user);

let workbook = new Excel();
let sheets = workbook.createSheets('用户', ['帐号', '密码', '地址', 'email'], ['userLoginName', 'userPassword', 'userAddr', 'userEmail'], users, 2);
console.log(sheets);
const options = { '!cols': [{ wch: 10 }, { wch: 15 }, { wch: 20 }, { wch: 20 }] };

workbook.write(sheets, '/work/excel.xlsx', options);
