import { SpliterUtils } from '../../src/common/utils/spliter.utils';

let rows = [
  [1, 2, 3],
  [4, 5, 6],
  [11, 22, 33],
  [44, 55, 66],
  [111, 222, 333],
];
let targets = SpliterUtils.split(rows, 2);
console.log(targets);
targets.forEach((row) => {
  console.log(row);
});
