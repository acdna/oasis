import { SysUser } from '../../src/sys/entity/sys.user.entity';
import { FindConditions } from 'typeorm';
import { FindOperator } from 'typeorm/find-options/FindOperator';

let json = {
  userId: '09bbd790b66e11e3700c5a86bda0265c',
  userLoginName: 'renjie',
  userPassword: 'e10adc3949ba59abbe56e057f20f883e',
  userPasswordPrivate: null,
  userNameAbbr: 'RJ',
  userName: '任洁',
  userSex: '2',
  userPhone: '13683644321',
  userEmail: 'renjie3170@sina.com',
  userImageUrl: '',
  userOrder: 5,
  userState: 'ON',
  userCode: 'MTC0004',
  userCreateDate: '2014-06-24T04:42:53.000Z',
  userLastLoginTime: '2019-07-25T13:35:57.000Z',
  userLastLoginIp: '127.0.0.1',
  userUnit: null,
  userJobTitle: null,
  userAddr: null,
  userPostcode: null,
  userSamSpecies: null,
};
const { ...a } = json;
console.log(a);

const options = (user: SysUser | Partial<SysUser>) => {
  console.log(user);
};

options({ userAddr: 'aaa' });

export declare type Partial<T> = {
  [P in keyof T]?: T[P];
};
