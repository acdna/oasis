import { SysServices } from '../sys.services';
import { SysDictionary } from '../entity/sys.dictionary.entity';
import { LogUtils } from '../../common/log4js/log4js.utils';

/**
 * 参数容器对象
 *
 * @author jiang
 * @date 2021-01-06 20:15:40
 **/
export class DictMapper {
  private static dictMap: Map<String, SysDictionary[]> = new Map<String, SysDictionary[]>();

  /**
   * 加载全部字典项数据
   * @author: jiangbin
   * @date: 2021-01-15 21:23:09
   **/
  static reload() {
    LogUtils.info(DictMapper.name, '开始加载全部字典项...');
    DictMapper.dictMap = new Map<string, SysDictionary[]>();
    SysServices.sysDictionaryService
      .findList({ dicType: 'DATA', dicState: 'ON' })
      .then((res) => {
        if (!res || res.length == 0) return;
        res.forEach((dict) => {
          const items = DictMapper.dictMap.get(dict.dicGroup) || [];
          items.length > 0 && items.push(dict);
          DictMapper.dictMap.set(dict.dicGroup, items);
        });
        LogUtils.info(DictMapper.name, '成功加载全部字典数据!');
      })
      .catch((err) => {
        LogUtils.error(DictMapper.name, '加载全部字典数据失败!', err);
      });
  }

  /**
   * 获取字典项名
   * @param params
   * @return 字典项名
   * @author: jiangbin
   * @date: 2021-01-15 21:50:55
   **/
  static getName(params: { dicGroup: string; dicValue: string }): string {
    let items = DictMapper.dictMap.get(params.dicGroup);
    items = items.filter((item) => item.dicValue === params.dicValue);
    return items && items.length > 0 ? items[0].dicName : '';
  }

  /**
   * 获取字典项值
   * @param params
   * @return 字典项值
   * @author: jiangbin
   * @date: 2021-01-15 21:50:52
   **/
  static getValue(params: { dicGroup: string; dicName: string }): string {
    let items = DictMapper.dictMap.get(params.dicGroup);
    items = items.filter((item) => item.dicName === params.dicName);
    return items && items.length > 0 ? items[0].dicValue : '';
  }

  /**
   * 获取字典项名列表
   * @param params
   * @return 字典项名列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:49
   **/
  static getNames(params: { dicGroup: string; dicValues: string[] }): string[] {
    return DictMapper.getItems(params.dicGroup).filter((item) => (item.dicValue ? params.dicValues.includes(item.dicValue) : false)) || [].map((item) => item.dicName) || [];
  }

  /**
   * 获取字典项名列表
   * @param params
   * @return 字典项名列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:49
   **/
  static getValues(params: { dicGroup: string; dicNames: string[] }): string[] {
    return DictMapper.getItems(params.dicGroup).filter((item) => (item.dicValue ? params.dicNames.includes(item.dicName) : false)) || [].map((item) => item.dicValue) || [];
  }

  /**
   * 获取字典项值列表
   * @param dicGroup 字典分组
   * @return 字典项值列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:46
   **/
  static getGroupValues(dicGroup: string): string[] {
    return DictMapper.getItems(dicGroup).map((item) => item.dicValue) || [];
  }

  /**
   * 获取字典项名列表
   * @param dicGroup 字典分组
   * @return 字典项名列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:49
   **/
  static getGroupNames(dicGroup: string): string[] {
    return DictMapper.getItems(dicGroup).map((item) => item.dicName) || [];
  }

  /**
   * 获取字典项列表
   * @param dicGroup 字典分组
   * @return 字典项列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:20
   **/
  static getItems(dicGroup: string): SysDictionary[] {
    return DictMapper.dictMap.get(dicGroup) || [];
  }
}
