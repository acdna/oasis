import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * 用户充值信息表
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysUserTopUp
 */
@Entity({
  name: 'SYS_USER_TOP_UP',
})
export class SysUserTopUp {
  /**
   * id-主键
   *
   * @type { string }
   * @memberof SysUserTopUp
   */
  @Column({ name: 'UTU_ID', type: 'varchar', length: '255', primary: true })
  @IsNotEmpty({ message: '【id】不能为空' })
  @Max(255, { message: '【id】长度不能超过255' })
  utuId: string;
  /**
   * 用户登录名
   *
   * @type { string }
   * @memberof SysUserTopUp
   */
  @Column({ name: 'UTU_LOGIN_NAME', type: 'varchar', length: '255' })
  @IsNotEmpty({ message: '【用户登录名】不能为空' })
  @Max(255, { message: '【用户登录名】长度不能超过255' })
  utuLoginName: string;
  /**
   * 序列号
   *
   * @type { string }
   * @memberof SysUserTopUp
   */
  @Column({ name: 'UTU_SERIAL_NUMBER', type: 'varchar', length: '255' })
  @IsNotEmpty({ message: '【序列号】不能为空' })
  @Max(255, { message: '【序列号】长度不能超过255' })
  utuSerialNumber: string;
  /**
   * 计费开始时间
   *
   * @type { Date }
   * @memberof SysUserTopUp
   */
  @Type(() => Date)
  @Column({ name: 'UTU_BILLING_START_DATE', type: 'datetime' })
  utuBillingStartDate: Date;
  /**
   * 计费结束时间
   *
   * @type { Date }
   * @memberof SysUserTopUp
   */
  @Type(() => Date)
  @Column({ name: 'UTU_BILLING_END_DATE', type: 'datetime' })
  utuBillingEndDate: Date;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUserTopUp
   */
  @Column({ name: 'UTU_STATE', type: 'varchar', length: '255' })
  @Max(255, { message: '【状态】长度不能超过255' })
  utuState: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserTopUp
   */
  @Type(() => Date)
  @Column({ name: 'UTU_CREATE_DATE', type: 'datetime' })
  utuCreateDate: Date;
}
