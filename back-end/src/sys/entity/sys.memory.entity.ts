import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * 系统JVM内存表
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysMemory
 */
@Entity({
  name: 'SYS_MEMORY',
})
export class SysMemory {
  /**
   * memId-主键
   *
   * @type { string }
   * @memberof SysMemory
   */
  @Column({ name: 'MEM_ID', type: 'varchar', length: '32', primary: true })
  @IsNotEmpty({ message: '【memId】不能为空' })
  @Max(32, { message: '【memId】长度不能超过32' })
  memId: string;
  /**
   * 总内存
   *
   * @type { number }
   * @memberof SysMemory
   */
  @Type(() => Number)
  @Column({ name: 'MEM_TOTAL', type: 'bigint' })
  memTotal: number;
  /**
   * 可用内存
   *
   * @type { number }
   * @memberof SysMemory
   */
  @Type(() => Number)
  @Column({ name: 'MEM_FREE', type: 'bigint' })
  memFree: number;
  /**
   * 已使用内存
   *
   * @type { number }
   * @memberof SysMemory
   */
  @Type(() => Number)
  @Column({ name: 'MEM_USED', type: 'bigint' })
  memUsed: number;
  /**
   * 内存用量是否预警
   *
   * @type { string }
   * @memberof SysMemory
   */
  @Column({ name: 'MEM_IS_WARNING', type: 'varchar', length: '32' })
  @Max(32, { message: '【内存用量是否预警】长度不能超过32' })
  memIsWarning: string;
  /**
   * 最大内存
   *
   * @type { number }
   * @memberof SysMemory
   */
  @Type(() => Number)
  @Column({ name: 'MEM_MAX', type: 'bigint' })
  memMax: number;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysMemory
   */
  @Type(() => Date)
  @Column({ name: 'MEM_CREATE_DATE', type: 'datetime' })
  memCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysMemory
   */
  @Type(() => Date)
  @Column({ name: 'MEM_UPDATE_DATE', type: 'datetime' })
  memUpdateDate: Date;
}
