import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_INSTRUMENT
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysInstrument
 */
@Entity({
  name: 'SYS_INSTRUMENT',
})
export class SysInstrument {
  /**
   * 仪器编号-主键
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【仪器编号】不能为空' })
  @Max(64, { message: '【仪器编号】长度不能超过64' })
  insId: string;
  /**
   * 仪器条码号
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_BARCODE', type: 'varchar', length: '128' })
  @Max(128, { message: '【仪器条码号】长度不能超过128' })
  insBarcode: string;
  /**
   * 仪器名称
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_NAME', type: 'varchar', length: '64' })
  @Max(64, { message: '【仪器名称】长度不能超过64' })
  insName: string;
  /**
   * 仪器型号
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_MODEL', type: 'varchar', length: '64' })
  @Max(64, { message: '【仪器型号】长度不能超过64' })
  insModel: string;
  /**
   * 仪器负责人
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_MANAGER', type: 'varchar', length: '64' })
  @Max(64, { message: '【仪器负责人】长度不能超过64' })
  insManager: string;
  /**
   * 仪器类型
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_TYPE', type: 'varchar', length: '64' })
  @Max(64, { message: '【仪器类型】长度不能超过64' })
  insType: string;
  /**
   * 仪器描述
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_REMARK', type: 'varchar', length: '128' })
  @Max(128, { message: '【仪器描述】长度不能超过128' })
  insRemark: string;
  /**
   * 仪器购买日期
   *
   * @type { Date }
   * @memberof SysInstrument
   */
  @Type(() => Date)
  @Column({ name: 'INS_BUY_DATE', type: 'datetime' })
  insBuyDate: Date;
  /**
   * 仪器维护周期
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_MAINTAIN_PERIOD', type: 'varchar', length: '32' })
  @Max(32, { message: '【仪器维护周期】长度不能超过32' })
  insMaintainPeriod: string;
  /**
   * 仪器厂商
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_MAKER', type: 'varchar', length: '64' })
  @Max(64, { message: '【仪器厂商】长度不能超过64' })
  insMaker: string;
  /**
   * 仪器价格
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_PRICE', type: 'varchar', length: '255' })
  @Max(255, { message: '【仪器价格】长度不能超过255' })
  insPrice: string;
  /**
   * insExtraProvide
   *
   * @type { string }
   * @memberof SysInstrument
   */
  @Column({ name: 'INS_EXTRA_PROVIDE', type: 'varchar', length: '128' })
  @Max(128, { message: '【insExtraProvide】长度不能超过128' })
  insExtraProvide: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysInstrument
   */
  @Type(() => Date)
  @Column({ name: 'INS_CREATE_DATE', type: 'datetime' })
  insCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysInstrument
   */
  @Type(() => Date)
  @Column({ name: 'INS_UPDATE_DATE', type: 'datetime' })
  insUpdateDate: Date;
}
