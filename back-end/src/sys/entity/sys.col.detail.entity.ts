import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_COL_DETAIL
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysColDetail
 */
@Entity({
  name: 'SYS_COL_DETAIL',
})
export class SysColDetail {
  /**
   * 主键ID-主键
   *
   * @type { string }
   * @memberof SysColDetail
   */
  @Column({ name: 'CD_ID', type: 'varchar', length: '32', primary: true })
  @IsNotEmpty({ message: '【主键ID】不能为空' })
  @Max(32, { message: '【主键ID】长度不能超过32' })
  cdId: string;
  /**
   * 表名
   *
   * @type { string }
   * @memberof SysColDetail
   */
  @Column({ name: 'CD_TABLE_NAME', type: 'varchar', length: '255' })
  @IsNotEmpty({ message: '【表名】不能为空' })
  @Max(255, { message: '【表名】长度不能超过255' })
  cdTableName: string;
  /**
   * 列名
   *
   * @type { string }
   * @memberof SysColDetail
   */
  @Column({ name: 'CD_COL_NAME', type: 'varchar', length: '255' })
  @IsNotEmpty({ message: '【列名】不能为空' })
  @Max(255, { message: '【列名】长度不能超过255' })
  cdColName: string;
  /**
   * 列中文名
   *
   * @type { string }
   * @memberof SysColDetail
   */
  @Column({ name: 'CD_DESC_NAME', type: 'varchar', length: '64' })
  @Max(64, { message: '【列中文名】长度不能超过64' })
  cdDescName: string;
  /**
   * 字典名
   *
   * @type { string }
   * @memberof SysColDetail
   */
  @Column({ name: 'CD_DICT_NAME', type: 'varchar', length: '255' })
  @Max(255, { message: '【字典名】长度不能超过255' })
  cdDictName: string;
  /**
   * 模式
   *
   * @type { string }
   * @memberof SysColDetail
   */
  @Column({ name: 'CD_MODE', type: 'varchar', length: '64' })
  @Max(64, { message: '【模式】长度不能超过64' })
  cdMode: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysColDetail
   */
  @Type(() => Date)
  @Column({ name: 'CD_CREATE_DATE', type: 'datetime' })
  cdCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysColDetail
   */
  @Type(() => Date)
  @Column({ name: 'CD_UPDATE_DATE', type: 'datetime' })
  cdUpdateDate: Date;
}
