import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_MENU
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysMenu
 */
@Entity({
  name: 'SYS_MENU',
})
export class SysMenu {
  /**
   * 菜单ID号-主键
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【菜单ID号】不能为空' })
  @Max(64, { message: '【菜单ID号】长度不能超过64' })
  menuId: string;
  /**
   * 菜单名称
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_NAME', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【菜单名称】不能为空' })
  @Max(64, { message: '【菜单名称】长度不能超过64' })
  menuName: string;
  /**
   * 英文菜单名
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_NAME_EN', type: 'varchar', length: '64' })
  @Max(64, { message: '【英文菜单名】长度不能超过64' })
  menuNameEn: string;
  /**
   * 菜单连接地址
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_URL', type: 'varchar', length: '256' })
  @Max(256, { message: '【菜单连接地址】长度不能超过256' })
  menuUrl: string;
  /**
   * 父菜单ID
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_PARENT_ID', type: 'varchar', length: '64' })
  @Max(64, { message: '【父菜单ID】长度不能超过64' })
  menuParentId: string;
  /**
   * 菜单顺序
   *
   * @type { number }
   * @memberof SysMenu
   */
  @Type(() => Number)
  @Column({ name: 'MENU_ORDER', type: 'int' })
  @IsNotEmpty({ message: '【菜单顺序】不能为空' })
  menuOrder: number;
  /**
   * 菜单备注
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_REMARK', type: 'varchar', length: '1024' })
  @Max(1024, { message: '【菜单备注】长度不能超过1024' })
  menuRemark: string;
  /**
   * 菜单状态
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_STATE', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【菜单状态】不能为空' })
  @Max(64, { message: '【菜单状态】长度不能超过64' })
  menuState: string;
  /**
   * 菜单类型
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_TYPE', type: 'varchar', length: '10' })
  @Max(10, { message: '【菜单类型】长度不能超过10' })
  menuType: string;
  /**
   * 菜单图标CSS名称
   *
   * @type { string }
   * @memberof SysMenu
   */
  @Column({ name: 'MENU_ICON_CLASS', type: 'varchar', length: '256' })
  @Max(256, { message: '【菜单图标CSS名称】长度不能超过256' })
  menuIconClass: string;
}
