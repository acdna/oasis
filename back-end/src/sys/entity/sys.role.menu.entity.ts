import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
/**
 * SYS_ROLE_MENU
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysRoleMenu
 */
@Entity({
  name: 'SYS_ROLE_MENU',
})
export class SysRoleMenu {
  /**
   * 角色菜单ID-主键
   *
   * @type { string }
   * @memberof SysRoleMenu
   */
  @Column({ name: 'RM_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【角色菜单ID】不能为空' })
  @Max(64, { message: '【角色菜单ID】长度不能超过64' })
  rmId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleMenu
   */
  @Column({ name: 'RM_ROLE_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【角色ID】不能为空' })
  @Max(64, { message: '【角色ID】长度不能超过64' })
  rmRoleId: string;
  /**
   * 菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenu
   */
  @Column({ name: 'RM_MENU_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【菜单ID】不能为空' })
  @Max(64, { message: '【菜单ID】长度不能超过64' })
  rmMenuId: string;
}
