import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * 文件管理
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysFiles
 */
@Entity({
  name: 'SYS_FILES',
})
export class SysFiles {
  /**
   * 记录ID-主键
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_ID', type: 'varchar', length: '32', primary: true })
  @IsNotEmpty({ message: '【记录ID】不能为空' })
  @Max(32, { message: '【记录ID】长度不能超过32' })
  fileId: string;
  /**
   * 文件条码号
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_BARCODE', type: 'varchar', length: '128' })
  @Max(128, { message: '【文件条码号】长度不能超过128' })
  fileBarcode: string;
  /**
   * 关联的其它记录信息ID号
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_RELATE_ID', type: 'varchar', length: '64' })
  @Max(64, { message: '【关联的其它记录信息ID号】长度不能超过64' })
  fileRelateId: string;
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_NAME', type: 'varchar', length: '256' })
  @IsNotEmpty({ message: '【文件名称】不能为空' })
  @Max(256, { message: '【文件名称】长度不能超过256' })
  fileName: string;
  /**
   * 文件名
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_NAME_EN', type: 'varchar', length: '256' })
  @Max(256, { message: '【文件名】长度不能超过256' })
  fileNameEn: string;
  /**
   * 文件存储路径
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_PATH', type: 'varchar', length: '1024' })
  @IsNotEmpty({ message: '【文件存储路径】不能为空' })
  @Max(1024, { message: '【文件存储路径】长度不能超过1024' })
  filePath: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysFiles
   */
  @Type(() => Date)
  @Column({ name: 'FILE_CREATE_DATE', type: 'datetime' })
  @IsNotEmpty({ message: '【创建日期】不能为空' })
  fileCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysFiles
   */
  @Type(() => Date)
  @Column({ name: 'FILE_UPDATE_DATE', type: 'datetime' })
  @IsNotEmpty({ message: '【更新日期】不能为空' })
  fileUpdateDate: Date;
  /**
   * 是否被启用
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_IS_USED', type: 'varchar', length: '10' })
  @IsNotEmpty({ message: '【是否被启用】不能为空' })
  @Max(10, { message: '【是否被启用】长度不能超过10' })
  fileIsUsed: string;
  /**
   * 所属用户
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_OWNER', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【所属用户】不能为空' })
  @Max(64, { message: '【所属用户】长度不能超过64' })
  fileOwner: string;
  /**
   * 文件大小
   *
   * @type { number }
   * @memberof SysFiles
   */
  @Type(() => Number)
  @Column({ name: 'FILE_SIZE', type: 'bigint' })
  @IsNotEmpty({ message: '【文件大小】不能为空' })
  fileSize: number;
  /**
   * 用户权限表
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_GRANTS', type: 'varchar', length: '256' })
  @Max(256, { message: '【用户权限表】长度不能超过256' })
  fileGrants: string;
  /**
   * 文件分类
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_CLASSES', type: 'varchar', length: '32' })
  @Max(32, { message: '【文件分类】长度不能超过32' })
  fileClasses: string;
  /**
   * 文件状态信息
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_STATE', type: 'varchar', length: '20' })
  @Max(20, { message: '【文件状态信息】长度不能超过20' })
  fileState: string;
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_TYPE', type: 'varchar', length: '50' })
  @Max(50, { message: '【文件类型】长度不能超过50' })
  fileType: string;
  /**
   * 文件备注信息
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_COMMENTS', type: 'varchar', length: '64' })
  @Max(64, { message: '【文件备注信息】长度不能超过64' })
  fileComments: string;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysFiles
   */
  @Column({ name: 'FILE_SPECIES', type: 'varchar', length: '32' })
  @Max(32, { message: '【种属】长度不能超过32' })
  fileSpecies: string;
}
