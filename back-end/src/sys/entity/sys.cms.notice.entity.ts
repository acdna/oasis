import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_CMS_NOTICE
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysCmsNotice
 */
@Entity({
  name: 'SYS_CMS_NOTICE',
})
export class SysCmsNotice {
  /**
   * 公告编号-主键
   *
   * @type { string }
   * @memberof SysCmsNotice
   */
  @Column({ name: 'SC_NOTICE_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【公告编号】不能为空' })
  @Max(64, { message: '【公告编号】长度不能超过64' })
  scNoticeId: string;
  /**
   * 公告标题
   *
   * @type { string }
   * @memberof SysCmsNotice
   */
  @Column({ name: 'SC_NOTICE_TITLE', type: 'varchar', length: '128' })
  @Max(128, { message: '【公告标题】长度不能超过128' })
  scNoticeTitle: string;
  /**
   * 公告内容
   *
   * @type { string }
   * @memberof SysCmsNotice
   */
  @Column({ name: 'SC_NOTICE_CONTENT', type: 'varchar', length: '20000' })
  @Max(20000, { message: '【公告内容】长度不能超过20000' })
  scNoticeContent: string;
  /**
   * 是否是新文件
   *
   * @type { string }
   * @memberof SysCmsNotice
   */
  @Column({ name: 'SC_NOTICE_IS_NEW', type: 'varchar', length: '8' })
  @Max(8, { message: '【是否是新文件】长度不能超过8' })
  scNoticeIsNew: string;
  /**
   * 公告发布者
   *
   * @type { string }
   * @memberof SysCmsNotice
   */
  @Column({ name: 'SC_NOTICE_MANAGER', type: 'varchar', length: '128' })
  @Max(128, { message: '【公告发布者】长度不能超过128' })
  scNoticeManager: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsNotice
   */
  @Type(() => Date)
  @Column({ name: 'SC_NOTICE_CREATE_DATE', type: 'datetime' })
  scNoticeCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsNotice
   */
  @Type(() => Date)
  @Column({ name: 'SC_NOTICE_UPDATE_DATE', type: 'datetime' })
  scNoticeUpdateDate: Date;
}
