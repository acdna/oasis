import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_LOG
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysLog
 */
@Entity({
  name: 'SYS_LOG',
})
export class SysLog {
  /**
   * 日志ID号-主键
   *
   * @type { string }
   * @memberof SysLog
   */
  @Column({ name: 'LOG_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【日志ID号】不能为空' })
  @Max(64, { message: '【日志ID号】长度不能超过64' })
  logId: string;
  /**
   * 操作人员名称
   *
   * @type { string }
   * @memberof SysLog
   */
  @Column({ name: 'LOG_USER', type: 'varchar', length: '64' })
  @Max(64, { message: '【操作人员名称】长度不能超过64' })
  logUser: string;
  /**
   * 操作时间
   *
   * @type { Date }
   * @memberof SysLog
   */
  @Type(() => Date)
  @Column({ name: 'LOG_TIME', type: 'timestamp' })
  @IsNotEmpty({ message: '【操作时间】不能为空' })
  logTime: Date;
  /**
   * IP地址
   *
   * @type { string }
   * @memberof SysLog
   */
  @Column({ name: 'LOG_IP', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【IP地址】不能为空' })
  @Max(64, { message: '【IP地址】长度不能超过64' })
  logIp: string;
  /**
   * 操作URL
   *
   * @type { string }
   * @memberof SysLog
   */
  @Column({ name: 'LOG_URL', type: 'varchar', length: '255' })
  @IsNotEmpty({ message: '【操作URL】不能为空' })
  @Max(255, { message: '【操作URL】长度不能超过255' })
  logUrl: string;
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysLog
   */
  @Column({ name: 'LOG_TITLE', type: 'varchar', length: '128' })
  @IsNotEmpty({ message: '【模块】不能为空' })
  @Max(128, { message: '【模块】长度不能超过128' })
  logTitle: string;
  /**
   * 内容
   *
   * @type { string }
   * @memberof SysLog
   */
  @Column({ name: 'LOG_CONTENT', type: 'longtext' })
  @IsNotEmpty({ message: '【内容】不能为空' })
  logContent: string;
  /**
   * 操作类型
   *
   * @type { number }
   * @memberof SysLog
   */
  @Type(() => Number)
  @Column({ name: 'LOG_TYPE', type: 'tinyint' })
  @IsNotEmpty({ message: '【操作类型】不能为空' })
  logType: number;
}
