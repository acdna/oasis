import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_PERMISSION
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysPermission
 */
@Entity({
  name: 'SYS_PERMISSION',
})
export class SysPermission {
  /**
   * 权限ID-主键
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【权限ID】不能为空' })
  @Max(64, { message: '【权限ID】长度不能超过64' })
  perId: string;
  /**
   * 权限名称
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_NAME', type: 'varchar', length: '30' })
  @IsNotEmpty({ message: '【权限名称】不能为空' })
  @Max(30, { message: '【权限名称】长度不能超过30' })
  perName: string;
  /**
   * 权限英文名
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_NAME_EN', type: 'varchar', length: '30' })
  @Max(30, { message: '【权限英文名】长度不能超过30' })
  perNameEn: string;
  /**
   * 权限类型
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_TYPE', type: 'varchar', length: '30' })
  @IsNotEmpty({ message: '【权限类型】不能为空' })
  @Max(30, { message: '【权限类型】长度不能超过30' })
  perType: string;
  /**
   * 权限URL
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_URL', type: 'varchar', length: '500' })
  @Max(500, { message: '【权限URL】长度不能超过500' })
  perUrl: string;
  /**
   * 权限访问方法
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_METHOD', type: 'varchar', length: '10' })
  @Max(10, { message: '【权限访问方法】长度不能超过10' })
  perMethod: string;
  /**
   * 父节点ID
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_PARENT_ID', type: 'varchar', length: '64' })
  @Max(64, { message: '【父节点ID】长度不能超过64' })
  perParentId: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysPermission
   */
  @Type(() => Number)
  @Column({ name: 'PER_ORDER', type: 'int' })
  @IsNotEmpty({ message: '【排序】不能为空' })
  perOrder: number;
  /**
   * 备注信息
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_REMARK', type: 'varchar', length: '300' })
  @Max(300, { message: '【备注信息】长度不能超过300' })
  perRemark: string;
  /**
   * 状态信息
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_STATE', type: 'varchar', length: '10' })
  @IsNotEmpty({ message: '【状态信息】不能为空' })
  @Max(10, { message: '【状态信息】长度不能超过10' })
  perState: string;
  /**
   * 所属系统：前端
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_SYSTEM', type: 'varchar', length: '6' })
  @Max(6, { message: '【所属系统：前端】长度不能超过6' })
  perSystem: string;
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysPermission
   */
  @Column({ name: 'PER_MODULE', type: 'varchar', length: '32' })
  @Max(32, { message: '【模块】长度不能超过32' })
  perModule: string;
}
