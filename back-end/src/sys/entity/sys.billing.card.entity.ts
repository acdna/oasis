import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Exclude } from 'class-transformer';
import { Type } from 'class-transformer';
/**
 * 计费卡信息表
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysBillingCard
 */
@Entity({
  name: 'SYS_BILLING_CARD',
})
export class SysBillingCard {
  /**
   * id-主键
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_ID', type: 'varchar', length: '255', primary: true })
  @IsNotEmpty({ message: '【id】不能为空' })
  @Max(255, { message: '【id】长度不能超过255' })
  bcId: string;
  /**
   * 序列号信息
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_SERIAL_NUMBER', type: 'varchar', length: '255' })
  @IsNotEmpty({ message: '【序列号信息】不能为空' })
  @Max(255, { message: '【序列号信息】长度不能超过255' })
  bcSerialNumber: string;
  /**
   * 总额
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_AMOUNT', type: 'varchar', length: '255' })
  @Max(255, { message: '【总额】长度不能超过255' })
  bcAmount: string;
  /**
   * 总时间
   *
   * @type { number }
   * @memberof SysBillingCard
   */
  @Type(() => Number)
  @Column({ name: 'BC_TOTAL_TIME', type: 'int' })
  bcTotalTime: number;
  /**
   * 类型
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_TYPE', type: 'varchar', length: '255' })
  @Max(255, { message: '【类型】长度不能超过255' })
  bcType: string;
  /**
   * 绑定帐号
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_ACCOUNT', type: 'varchar', length: '255' })
  @Max(255, { message: '【绑定帐号】长度不能超过255' })
  bcAccount: string;
  /**
   * 绑定密码
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Exclude({ toPlainOnly: true })
  @Column({ name: 'BC_PASSWORD', type: 'varchar', length: '255' })
  @Max(255, { message: '【绑定密码】长度不能超过255' })
  bcPassword: string;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_STATE', type: 'varchar', length: '255' })
  @Max(255, { message: '【状态】长度不能超过255' })
  bcState: string;
  /**
   * 最大指纹数
   *
   * @type { string }
   * @memberof SysBillingCard
   */
  @Column({ name: 'BC_BIGGEST_GENE_COUNT', type: 'varchar', length: '255' })
  @Max(255, { message: '【最大指纹数】长度不能超过255' })
  bcBiggestGeneCount: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysBillingCard
   */
  @Type(() => Date)
  @Column({ name: 'BC_CREATE_DATE', type: 'datetime' })
  bcCreateDate: Date;
}
