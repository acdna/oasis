import {Column, Entity} from 'typeorm';
import {IsNotEmpty, Max} from 'class-validator';
import {Type} from 'class-transformer';

/**
 * SYS_DICTIONARY
 * @date 12/28/2020, 6:29:16 PM
 * @author jiangbin
 * @export
 * @class SysDictionary
 */
@Entity({
  name: 'SYS_DICTIONARY',
})
export class SysDictionary {
  /**
   * ID-主键
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_ID', type: 'varchar', length: '64', primary: true})
  @IsNotEmpty({message: '【ID】不能为空'})
  @Max(64, {message: '【ID】长度不能超过64'})
  dicId: string;
  /**
   * 字典名称
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_NAME', type: 'varchar', length: '128'})
  @IsNotEmpty({message: '【字典名称】不能为空'})
  @Max(128, {message: '【字典名称】长度不能超过128'})
  dicName: string;
  /**
   * dicNameEn
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_NAME_EN', type: 'varchar', length: '128'})
  @Max(128, {message: '【dicNameEn】长度不能超过128'})
  dicNameEn: string;
  /**
   * 字典值
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_VALUE', type: 'varchar', length: '128'})
  @Max(128, {message: '【字典值】长度不能超过128'})
  dicValue: string;
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_GROUP', type: 'varchar', length: '128'})
  @Max(128, {message: '【所属组】长度不能超过128'})
  dicGroup: string;
  /**
   * 模块名
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_MODULE', type: 'varchar', length: '32'})
  @Max(32, {message: '【模块名】长度不能超过32'})
  dicModule: string;
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_PARENT_ID', type: 'varchar', length: '64'})
  @Max(64, {message: '【父级ID】长度不能超过64'})
  dicParentId: string;
  /**
   * 字典类型
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column('enum', {name: 'DIC_TYPE', enum: ['DIR', 'DATA']})
  @IsNotEmpty({message: '【字典类型】不能为空'})
  dicType: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysDictionary
   */
  @Type(() => Number)
  @Column({name: 'DIC_ORDER', type: 'int'})
  dicOrder: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column('enum', {name: 'DIC_STATE', enum: ['OFF','ON']})
  @IsNotEmpty({message: '【状态】不能为空'})
  dicState: string;
  /**
   * dicParams
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_PARAMS', type: 'varchar', length: '512'})
  @Max(512, {message: '【dicParams】长度不能超过512'})
  dicParams: string;
  /**
   * 关联种属
   *
   * @type { string }
   * @memberof SysDictionary
   */
  @Column({name: 'DIC_SPECIES', type: 'varchar', length: '512'})
  @Max(512, {message: '【关联种属】长度不能超过512'})
  dicSpecies: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysDictionary
   */
  @Type(() => Date)
  @Column({name: 'DIC_CREATE_DATE', type: 'datetime'})
  dicCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysDictionary
   */
  @Type(() => Date)
  @Column({name: 'DIC_UPDATE_DATE', type: 'datetime'})
  dicUpdateDate: Date;
}
