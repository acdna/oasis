import { SysPermissionQDto } from '../../dto/query/sys.permission.qdto';
import { SysPermissionCDto } from '../../dto/create/sys.permission.cdto';
import { SysPermissionUDto } from '../../dto/update/sys.permission.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysPermissionColNameEnum, SysPermissionColPropEnum, SysPermissionTable, SysPermissionColNames, SysPermissionColProps } from '../ddl/sys.permission.cols';
/**
 * SYS_PERMISSION--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysPermissionSQL
 */
export const SysPermissionSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let cols = SysPermissionSQL.COL_MAPPER_SQL(dto);
    let where = SysPermissionSQL.WHERE_SQL(dto);
    let group = SysPermissionSQL.GROUP_BY_SQL(dto);
    let order = SysPermissionSQL.ORDER_BY_SQL(dto);
    let limit = SysPermissionSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let where = SysPermissionSQL.WHERE_SQL(dto);
    let group = SysPermissionSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let cols = SysPermissionSQL.COL_MAPPER_SQL(dto);
    let group = SysPermissionSQL.GROUP_BY_SQL(dto);
    let order = SysPermissionSQL.ORDER_BY_SQL(dto);
    let limit = SysPermissionSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysPermissionSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysPermissionSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysPermissionSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let where = SysPermissionSQL.WHERE_SQL(dto);
    let group = SysPermissionSQL.GROUP_BY_SQL(dto);
    let order = SysPermissionSQL.ORDER_BY_SQL(dto);
    let limit = SysPermissionSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysPermissionColNames, SysPermissionColProps);
    if (!cols || cols.length == 0) cols = SysPermissionColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[perId]?.length > 0) items.push(` ${PER_ID} = '${dto[perId]}'`);
    if (dto['nperId']?.length > 0) items.push(` ${PER_ID} != '${dto['nperId']}'`);
    if (dto[perName]?.length > 0) items.push(` ${PER_NAME} = '${dto[perName]}'`);
    if (dto['nperName']?.length > 0) items.push(` ${PER_NAME} != '${dto['nperName']}'`);
    if (dto[perNameEn]?.length > 0) items.push(` ${PER_NAME_EN} = '${dto[perNameEn]}'`);
    if (dto['nperNameEn']?.length > 0) items.push(` ${PER_NAME_EN} != '${dto['nperNameEn']}'`);
    if (dto[perType]?.length > 0) items.push(` ${PER_TYPE} = '${dto[perType]}'`);
    if (dto['nperType']?.length > 0) items.push(` ${PER_TYPE} != '${dto['nperType']}'`);
    if (dto[perUrl]?.length > 0) items.push(` ${PER_URL} = '${dto[perUrl]}'`);
    if (dto['nperUrl']?.length > 0) items.push(` ${PER_URL} != '${dto['nperUrl']}'`);
    if (dto[perMethod]?.length > 0) items.push(` ${PER_METHOD} = '${dto[perMethod]}'`);
    if (dto['nperMethod']?.length > 0) items.push(` ${PER_METHOD} != '${dto['nperMethod']}'`);
    if (dto[perParentId]?.length > 0) items.push(` ${PER_PARENT_ID} = '${dto[perParentId]}'`);
    if (dto['nperParentId']?.length > 0) items.push(` ${PER_PARENT_ID} != '${dto['nperParentId']}'`);
    if (dto[perOrder]) items.push(` ${PER_ORDER} = '${dto[perOrder]}'`);
    if (dto['nperOrder']) items.push(` ${PER_ORDER} != '${dto['nperOrder']}'`);
    if (dto[perRemark]?.length > 0) items.push(` ${PER_REMARK} = '${dto[perRemark]}'`);
    if (dto['nperRemark']?.length > 0) items.push(` ${PER_REMARK} != '${dto['nperRemark']}'`);
    if (dto[perState]?.length > 0) items.push(` ${PER_STATE} = '${dto[perState]}'`);
    if (dto['nperState']?.length > 0) items.push(` ${PER_STATE} != '${dto['nperState']}'`);
    if (dto[perSystem]?.length > 0) items.push(` ${PER_SYSTEM} = '${dto[perSystem]}'`);
    if (dto['nperSystem']?.length > 0) items.push(` ${PER_SYSTEM} != '${dto['nperSystem']}'`);
    if (dto[perModule]?.length > 0) items.push(` ${PER_MODULE} = '${dto[perModule]}'`);
    if (dto['nperModule']?.length > 0) items.push(` ${PER_MODULE} != '${dto['nperModule']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //PER_ID--字符串字段
    if (dto['perIdList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_ID, <string[]>dto['perIdList']));
    if (dto['nperIdList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_ID, <string[]>dto['nperIdList'], true));
    if (dto['perIdLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_ID, dto['perIdLike'])); //For MysSQL
    if (dto['nperIdLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_ID, dto['nperIdLike'], true)); //For MysSQL
    if (dto['perIdLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_ID, <string[]>dto['perIdLikeList'])); //For MysSQL
    //PER_NAME--字符串字段
    if (dto['perNameList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_NAME, <string[]>dto['perNameList']));
    if (dto['nperNameList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_NAME, <string[]>dto['nperNameList'], true));
    if (dto['perNameLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_NAME, dto['perNameLike'])); //For MysSQL
    if (dto['nperNameLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_NAME, dto['nperNameLike'], true)); //For MysSQL
    if (dto['perNameLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_NAME, <string[]>dto['perNameLikeList'])); //For MysSQL
    //PER_NAME_EN--字符串字段
    if (dto['perNameEnList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_NAME_EN, <string[]>dto['perNameEnList']));
    if (dto['nperNameEnList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_NAME_EN, <string[]>dto['nperNameEnList'], true));
    if (dto['perNameEnLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_NAME_EN, dto['perNameEnLike'])); //For MysSQL
    if (dto['nperNameEnLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_NAME_EN, dto['nperNameEnLike'], true)); //For MysSQL
    if (dto['perNameEnLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_NAME_EN, <string[]>dto['perNameEnLikeList'])); //For MysSQL
    //PER_TYPE--字符串字段
    if (dto['perTypeList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_TYPE, <string[]>dto['perTypeList']));
    if (dto['nperTypeList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_TYPE, <string[]>dto['nperTypeList'], true));
    if (dto['perTypeLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_TYPE, dto['perTypeLike'])); //For MysSQL
    if (dto['nperTypeLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_TYPE, dto['nperTypeLike'], true)); //For MysSQL
    if (dto['perTypeLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_TYPE, <string[]>dto['perTypeLikeList'])); //For MysSQL
    //PER_URL--字符串字段
    if (dto['perUrlList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_URL, <string[]>dto['perUrlList']));
    if (dto['nperUrlList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_URL, <string[]>dto['nperUrlList'], true));
    if (dto['perUrlLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_URL, dto['perUrlLike'])); //For MysSQL
    if (dto['nperUrlLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_URL, dto['nperUrlLike'], true)); //For MysSQL
    if (dto['perUrlLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_URL, <string[]>dto['perUrlLikeList'])); //For MysSQL
    //PER_METHOD--字符串字段
    if (dto['perMethodList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_METHOD, <string[]>dto['perMethodList']));
    if (dto['nperMethodList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_METHOD, <string[]>dto['nperMethodList'], true));
    if (dto['perMethodLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_METHOD, dto['perMethodLike'])); //For MysSQL
    if (dto['nperMethodLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_METHOD, dto['nperMethodLike'], true)); //For MysSQL
    if (dto['perMethodLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_METHOD, <string[]>dto['perMethodLikeList'])); //For MysSQL
    //PER_PARENT_ID--字符串字段
    if (dto['perParentIdList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_PARENT_ID, <string[]>dto['perParentIdList']));
    if (dto['nperParentIdList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_PARENT_ID, <string[]>dto['nperParentIdList'], true));
    if (dto['perParentIdLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_PARENT_ID, dto['perParentIdLike'])); //For MysSQL
    if (dto['nperParentIdLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_PARENT_ID, dto['nperParentIdLike'], true)); //For MysSQL
    if (dto['perParentIdLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_PARENT_ID, <string[]>dto['perParentIdLikeList'])); //For MysSQL
    //PER_ORDER--数字字段
    if (dto['perOrderList']) items.push(SysPermissionSQL.IN_SQL(PER_ORDER, dto['perOrderList']));
    if (dto['nperOrderList']) items.push(SysPermissionSQL.IN_SQL(PER_ORDER, dto['nperOrderList'], true));
    //PER_REMARK--字符串字段
    if (dto['perRemarkList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_REMARK, <string[]>dto['perRemarkList']));
    if (dto['nperRemarkList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_REMARK, <string[]>dto['nperRemarkList'], true));
    if (dto['perRemarkLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_REMARK, dto['perRemarkLike'])); //For MysSQL
    if (dto['nperRemarkLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_REMARK, dto['nperRemarkLike'], true)); //For MysSQL
    if (dto['perRemarkLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_REMARK, <string[]>dto['perRemarkLikeList'])); //For MysSQL
    //PER_STATE--字符串字段
    if (dto['perStateList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_STATE, <string[]>dto['perStateList']));
    if (dto['nperStateList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_STATE, <string[]>dto['nperStateList'], true));
    if (dto['perStateLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_STATE, dto['perStateLike'])); //For MysSQL
    if (dto['nperStateLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_STATE, dto['nperStateLike'], true)); //For MysSQL
    if (dto['perStateLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_STATE, <string[]>dto['perStateLikeList'])); //For MysSQL
    //PER_SYSTEM--字符串字段
    if (dto['perSystemList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_SYSTEM, <string[]>dto['perSystemList']));
    if (dto['nperSystemList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_SYSTEM, <string[]>dto['nperSystemList'], true));
    if (dto['perSystemLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_SYSTEM, dto['perSystemLike'])); //For MysSQL
    if (dto['nperSystemLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_SYSTEM, dto['nperSystemLike'], true)); //For MysSQL
    if (dto['perSystemLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_SYSTEM, <string[]>dto['perSystemLikeList'])); //For MysSQL
    //PER_MODULE--字符串字段
    if (dto['perModuleList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_MODULE, <string[]>dto['perModuleList']));
    if (dto['nperModuleList']?.length > 0) items.push(SysPermissionSQL.IN_SQL(PER_MODULE, <string[]>dto['nperModuleList'], true));
    if (dto['perModuleLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_MODULE, dto['perModuleLike'])); //For MysSQL
    if (dto['nperModuleLike']?.length > 0) items.push(SysPermissionSQL.LIKE_SQL(PER_MODULE, dto['nperModuleLike'], true)); //For MysSQL
    if (dto['perModuleLikeList']?.length > 0) items.push(SysPermissionSQL.LIKE_LIST_SQL(PER_MODULE, <string[]>dto['perModuleLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysPermissionSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysPermissionCDto | Partial<SysPermissionCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(PER_ID);
    cols.push(PER_NAME);
    cols.push(PER_NAME_EN);
    cols.push(PER_TYPE);
    cols.push(PER_URL);
    cols.push(PER_METHOD);
    cols.push(PER_PARENT_ID);
    cols.push(PER_ORDER);
    cols.push(PER_REMARK);
    cols.push(PER_STATE);
    cols.push(PER_SYSTEM);
    cols.push(PER_MODULE);
    values.push(toSqlValue(dto[perId]));
    values.push(toSqlValue(dto[perName]));
    values.push(toSqlValue(dto[perNameEn]));
    values.push(toSqlValue(dto[perType]));
    values.push(toSqlValue(dto[perUrl]));
    values.push(toSqlValue(dto[perMethod]));
    values.push(toSqlValue(dto[perParentId]));
    values.push(toSqlValue(dto[perOrder]));
    values.push(toSqlValue(dto[perRemark]));
    values.push(toSqlValue(dto[perState]));
    values.push(toSqlValue(dto[perSystem]));
    values.push(toSqlValue(dto[perModule]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysPermissionCDto[] | Partial<SysPermissionCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(PER_ID);
    cols.push(PER_NAME);
    cols.push(PER_NAME_EN);
    cols.push(PER_TYPE);
    cols.push(PER_URL);
    cols.push(PER_METHOD);
    cols.push(PER_PARENT_ID);
    cols.push(PER_ORDER);
    cols.push(PER_REMARK);
    cols.push(PER_STATE);
    cols.push(PER_SYSTEM);
    cols.push(PER_MODULE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[perId]));
      values.push(toSqlValue(dto[perName]));
      values.push(toSqlValue(dto[perNameEn]));
      values.push(toSqlValue(dto[perType]));
      values.push(toSqlValue(dto[perUrl]));
      values.push(toSqlValue(dto[perMethod]));
      values.push(toSqlValue(dto[perParentId]));
      values.push(toSqlValue(dto[perOrder]));
      values.push(toSqlValue(dto[perRemark]));
      values.push(toSqlValue(dto[perState]));
      values.push(toSqlValue(dto[perSystem]));
      values.push(toSqlValue(dto[perModule]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysPermissionUDto | Partial<SysPermissionUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysPermissionColNames, SysPermissionColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${PER_NAME} = ${toSqlValue(dto[perName])}`);
    items.push(`${PER_NAME_EN} = ${toSqlValue(dto[perNameEn])}`);
    items.push(`${PER_TYPE} = ${toSqlValue(dto[perType])}`);
    items.push(`${PER_URL} = ${toSqlValue(dto[perUrl])}`);
    items.push(`${PER_METHOD} = ${toSqlValue(dto[perMethod])}`);
    items.push(`${PER_PARENT_ID} = ${toSqlValue(dto[perParentId])}`);
    items.push(`${PER_ORDER} = ${toSqlValue(dto[perOrder])}`);
    items.push(`${PER_REMARK} = ${toSqlValue(dto[perRemark])}`);
    items.push(`${PER_STATE} = ${toSqlValue(dto[perState])}`);
    items.push(`${PER_SYSTEM} = ${toSqlValue(dto[perSystem])}`);
    items.push(`${PER_MODULE} = ${toSqlValue(dto[perModule])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysPermissionUDto | Partial<SysPermissionUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysPermissionColNames, SysPermissionColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysPermissionSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${PER_NAME} = ${toSqlValue(dto[perName])}`);
    items.push(`${PER_NAME_EN} = ${toSqlValue(dto[perNameEn])}`);
    items.push(`${PER_TYPE} = ${toSqlValue(dto[perType])}`);
    items.push(`${PER_URL} = ${toSqlValue(dto[perUrl])}`);
    items.push(`${PER_METHOD} = ${toSqlValue(dto[perMethod])}`);
    items.push(`${PER_PARENT_ID} = ${toSqlValue(dto[perParentId])}`);
    items.push(`${PER_ORDER} = ${toSqlValue(dto[perOrder])}`);
    items.push(`${PER_REMARK} = ${toSqlValue(dto[perRemark])}`);
    items.push(`${PER_STATE} = ${toSqlValue(dto[perState])}`);
    items.push(`${PER_SYSTEM} = ${toSqlValue(dto[perSystem])}`);
    items.push(`${PER_MODULE} = ${toSqlValue(dto[perModule])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysPermissionSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysPermissionUDto[] | Partial<SysPermissionUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysPermissionSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysPermissionUDto | Partial<SysPermissionUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[perName]?.length > 0) items.push(`${PER_NAME} = ${toSqlValue(dto[perName])}`);
    if (dto[perNameEn]?.length > 0) items.push(`${PER_NAME_EN} = ${toSqlValue(dto[perNameEn])}`);
    if (dto[perType]?.length > 0) items.push(`${PER_TYPE} = ${toSqlValue(dto[perType])}`);
    if (dto[perUrl]?.length > 0) items.push(`${PER_URL} = ${toSqlValue(dto[perUrl])}`);
    if (dto[perMethod]?.length > 0) items.push(`${PER_METHOD} = ${toSqlValue(dto[perMethod])}`);
    if (dto[perParentId]?.length > 0) items.push(`${PER_PARENT_ID} = ${toSqlValue(dto[perParentId])}`);
    if (dto[perOrder]) items.push(`${PER_ORDER} = ${toSqlValue(dto[perOrder])}`);
    if (dto[perRemark]?.length > 0) items.push(`${PER_REMARK} = ${toSqlValue(dto[perRemark])}`);
    if (dto[perState]?.length > 0) items.push(`${PER_STATE} = ${toSqlValue(dto[perState])}`);
    if (dto[perSystem]?.length > 0) items.push(`${PER_SYSTEM} = ${toSqlValue(dto[perSystem])}`);
    if (dto[perModule]?.length > 0) items.push(`${PER_MODULE} = ${toSqlValue(dto[perModule])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysPermissionUDto[] | Partial<SysPermissionUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysPermissionSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysPermissionQDto | Partial<SysPermissionQDto>): string => {
    if (!dto) return '';
    let where = SysPermissionSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysPermissionSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_PERMISSION';
/**
 * 列名常量字段
 */
const PER_ID = SysPermissionColNameEnum.PER_ID;
const PER_NAME = SysPermissionColNameEnum.PER_NAME;
const PER_NAME_EN = SysPermissionColNameEnum.PER_NAME_EN;
const PER_TYPE = SysPermissionColNameEnum.PER_TYPE;
const PER_URL = SysPermissionColNameEnum.PER_URL;
const PER_METHOD = SysPermissionColNameEnum.PER_METHOD;
const PER_PARENT_ID = SysPermissionColNameEnum.PER_PARENT_ID;
const PER_ORDER = SysPermissionColNameEnum.PER_ORDER;
const PER_REMARK = SysPermissionColNameEnum.PER_REMARK;
const PER_STATE = SysPermissionColNameEnum.PER_STATE;
const PER_SYSTEM = SysPermissionColNameEnum.PER_SYSTEM;
const PER_MODULE = SysPermissionColNameEnum.PER_MODULE;
/**
 * 实体类属性名
 */
const perId = SysPermissionColPropEnum.perId;
const perName = SysPermissionColPropEnum.perName;
const perNameEn = SysPermissionColPropEnum.perNameEn;
const perType = SysPermissionColPropEnum.perType;
const perUrl = SysPermissionColPropEnum.perUrl;
const perMethod = SysPermissionColPropEnum.perMethod;
const perParentId = SysPermissionColPropEnum.perParentId;
const perOrder = SysPermissionColPropEnum.perOrder;
const perRemark = SysPermissionColPropEnum.perRemark;
const perState = SysPermissionColPropEnum.perState;
const perSystem = SysPermissionColPropEnum.perSystem;
const perModule = SysPermissionColPropEnum.perModule;
/**
 * 主键信息
 */
const PRIMER_KEY = SysPermissionTable.PRIMER_KEY;
const primerKey = SysPermissionTable.primerKey;
