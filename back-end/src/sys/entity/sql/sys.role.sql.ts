import { SysRoleQDto } from '../../dto/query/sys.role.qdto';
import { SysRoleCDto } from '../../dto/create/sys.role.cdto';
import { SysRoleUDto } from '../../dto/update/sys.role.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysRoleColNameEnum, SysRoleColPropEnum, SysRoleTable, SysRoleColNames, SysRoleColProps } from '../ddl/sys.role.cols';
/**
 * SYS_ROLE--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysRoleSQL
 */
export const SysRoleSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let cols = SysRoleSQL.COL_MAPPER_SQL(dto);
    let where = SysRoleSQL.WHERE_SQL(dto);
    let group = SysRoleSQL.GROUP_BY_SQL(dto);
    let order = SysRoleSQL.ORDER_BY_SQL(dto);
    let limit = SysRoleSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let where = SysRoleSQL.WHERE_SQL(dto);
    let group = SysRoleSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let cols = SysRoleSQL.COL_MAPPER_SQL(dto);
    let group = SysRoleSQL.GROUP_BY_SQL(dto);
    let order = SysRoleSQL.ORDER_BY_SQL(dto);
    let limit = SysRoleSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysRoleSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysRoleSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysRoleSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let where = SysRoleSQL.WHERE_SQL(dto);
    let group = SysRoleSQL.GROUP_BY_SQL(dto);
    let order = SysRoleSQL.ORDER_BY_SQL(dto);
    let limit = SysRoleSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysRoleColNames, SysRoleColProps);
    if (!cols || cols.length == 0) cols = SysRoleColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[roleId]?.length > 0) items.push(` ${ROLE_ID} = '${dto[roleId]}'`);
    if (dto['nroleId']?.length > 0) items.push(` ${ROLE_ID} != '${dto['nroleId']}'`);
    if (dto[roleName]?.length > 0) items.push(` ${ROLE_NAME} = '${dto[roleName]}'`);
    if (dto['nroleName']?.length > 0) items.push(` ${ROLE_NAME} != '${dto['nroleName']}'`);
    if (dto[roleNameEn]?.length > 0) items.push(` ${ROLE_NAME_EN} = '${dto[roleNameEn]}'`);
    if (dto['nroleNameEn']?.length > 0) items.push(` ${ROLE_NAME_EN} != '${dto['nroleNameEn']}'`);
    if (dto[roleState]?.length > 0) items.push(` ${ROLE_STATE} = '${dto[roleState]}'`);
    if (dto['nroleState']?.length > 0) items.push(` ${ROLE_STATE} != '${dto['nroleState']}'`);
    if (dto[roleSys]?.length > 0) items.push(` ${ROLE_SYS} = '${dto[roleSys]}'`);
    if (dto['nroleSys']?.length > 0) items.push(` ${ROLE_SYS} != '${dto['nroleSys']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //ROLE_ID--字符串字段
    if (dto['roleIdList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_ID, <string[]>dto['roleIdList']));
    if (dto['nroleIdList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_ID, <string[]>dto['nroleIdList'], true));
    if (dto['roleIdLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_ID, dto['roleIdLike'])); //For MysSQL
    if (dto['nroleIdLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_ID, dto['nroleIdLike'], true)); //For MysSQL
    if (dto['roleIdLikeList']?.length > 0) items.push(SysRoleSQL.LIKE_LIST_SQL(ROLE_ID, <string[]>dto['roleIdLikeList'])); //For MysSQL
    //ROLE_NAME--字符串字段
    if (dto['roleNameList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_NAME, <string[]>dto['roleNameList']));
    if (dto['nroleNameList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_NAME, <string[]>dto['nroleNameList'], true));
    if (dto['roleNameLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_NAME, dto['roleNameLike'])); //For MysSQL
    if (dto['nroleNameLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_NAME, dto['nroleNameLike'], true)); //For MysSQL
    if (dto['roleNameLikeList']?.length > 0) items.push(SysRoleSQL.LIKE_LIST_SQL(ROLE_NAME, <string[]>dto['roleNameLikeList'])); //For MysSQL
    //ROLE_NAME_EN--字符串字段
    if (dto['roleNameEnList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_NAME_EN, <string[]>dto['roleNameEnList']));
    if (dto['nroleNameEnList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_NAME_EN, <string[]>dto['nroleNameEnList'], true));
    if (dto['roleNameEnLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_NAME_EN, dto['roleNameEnLike'])); //For MysSQL
    if (dto['nroleNameEnLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_NAME_EN, dto['nroleNameEnLike'], true)); //For MysSQL
    if (dto['roleNameEnLikeList']?.length > 0) items.push(SysRoleSQL.LIKE_LIST_SQL(ROLE_NAME_EN, <string[]>dto['roleNameEnLikeList'])); //For MysSQL
    //ROLE_STATE--字符串字段
    if (dto['roleStateList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_STATE, <string[]>dto['roleStateList']));
    if (dto['nroleStateList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_STATE, <string[]>dto['nroleStateList'], true));
    if (dto['roleStateLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_STATE, dto['roleStateLike'])); //For MysSQL
    if (dto['nroleStateLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_STATE, dto['nroleStateLike'], true)); //For MysSQL
    if (dto['roleStateLikeList']?.length > 0) items.push(SysRoleSQL.LIKE_LIST_SQL(ROLE_STATE, <string[]>dto['roleStateLikeList'])); //For MysSQL
    //ROLE_SYS--字符串字段
    if (dto['roleSysList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_SYS, <string[]>dto['roleSysList']));
    if (dto['nroleSysList']?.length > 0) items.push(SysRoleSQL.IN_SQL(ROLE_SYS, <string[]>dto['nroleSysList'], true));
    if (dto['roleSysLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_SYS, dto['roleSysLike'])); //For MysSQL
    if (dto['nroleSysLike']?.length > 0) items.push(SysRoleSQL.LIKE_SQL(ROLE_SYS, dto['nroleSysLike'], true)); //For MysSQL
    if (dto['roleSysLikeList']?.length > 0) items.push(SysRoleSQL.LIKE_LIST_SQL(ROLE_SYS, <string[]>dto['roleSysLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysRoleSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysRoleCDto | Partial<SysRoleCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(ROLE_ID);
    cols.push(ROLE_NAME);
    cols.push(ROLE_NAME_EN);
    cols.push(ROLE_STATE);
    cols.push(ROLE_SYS);
    values.push(toSqlValue(dto[roleId]));
    values.push(toSqlValue(dto[roleName]));
    values.push(toSqlValue(dto[roleNameEn]));
    values.push(toSqlValue(dto[roleState]));
    values.push(toSqlValue(dto[roleSys]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysRoleCDto[] | Partial<SysRoleCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(ROLE_ID);
    cols.push(ROLE_NAME);
    cols.push(ROLE_NAME_EN);
    cols.push(ROLE_STATE);
    cols.push(ROLE_SYS);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[roleId]));
      values.push(toSqlValue(dto[roleName]));
      values.push(toSqlValue(dto[roleNameEn]));
      values.push(toSqlValue(dto[roleState]));
      values.push(toSqlValue(dto[roleSys]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysRoleUDto | Partial<SysRoleUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysRoleColNames, SysRoleColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${ROLE_NAME} = ${toSqlValue(dto[roleName])}`);
    items.push(`${ROLE_NAME_EN} = ${toSqlValue(dto[roleNameEn])}`);
    items.push(`${ROLE_STATE} = ${toSqlValue(dto[roleState])}`);
    items.push(`${ROLE_SYS} = ${toSqlValue(dto[roleSys])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysRoleUDto | Partial<SysRoleUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysRoleColNames, SysRoleColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysRoleSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${ROLE_NAME} = ${toSqlValue(dto[roleName])}`);
    items.push(`${ROLE_NAME_EN} = ${toSqlValue(dto[roleNameEn])}`);
    items.push(`${ROLE_STATE} = ${toSqlValue(dto[roleState])}`);
    items.push(`${ROLE_SYS} = ${toSqlValue(dto[roleSys])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysRoleSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysRoleUDto[] | Partial<SysRoleUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysRoleSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysRoleUDto | Partial<SysRoleUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[roleName]?.length > 0) items.push(`${ROLE_NAME} = ${toSqlValue(dto[roleName])}`);
    if (dto[roleNameEn]?.length > 0) items.push(`${ROLE_NAME_EN} = ${toSqlValue(dto[roleNameEn])}`);
    if (dto[roleState]?.length > 0) items.push(`${ROLE_STATE} = ${toSqlValue(dto[roleState])}`);
    if (dto[roleSys]?.length > 0) items.push(`${ROLE_SYS} = ${toSqlValue(dto[roleSys])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysRoleUDto[] | Partial<SysRoleUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysRoleSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysRoleQDto | Partial<SysRoleQDto>): string => {
    if (!dto) return '';
    let where = SysRoleSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysRoleSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_ROLE';
/**
 * 列名常量字段
 */
const ROLE_ID = SysRoleColNameEnum.ROLE_ID;
const ROLE_NAME = SysRoleColNameEnum.ROLE_NAME;
const ROLE_NAME_EN = SysRoleColNameEnum.ROLE_NAME_EN;
const ROLE_STATE = SysRoleColNameEnum.ROLE_STATE;
const ROLE_SYS = SysRoleColNameEnum.ROLE_SYS;
/**
 * 实体类属性名
 */
const roleId = SysRoleColPropEnum.roleId;
const roleName = SysRoleColPropEnum.roleName;
const roleNameEn = SysRoleColPropEnum.roleNameEn;
const roleState = SysRoleColPropEnum.roleState;
const roleSys = SysRoleColPropEnum.roleSys;
/**
 * 主键信息
 */
const PRIMER_KEY = SysRoleTable.PRIMER_KEY;
const primerKey = SysRoleTable.primerKey;
