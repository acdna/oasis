import { SysConfigQDto } from '../../dto/query/sys.config.qdto';
import { SysConfigCDto } from '../../dto/create/sys.config.cdto';
import { SysConfigUDto } from '../../dto/update/sys.config.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysConfigColNameEnum, SysConfigColPropEnum, SysConfigTable, SysConfigColNames, SysConfigColProps } from '../ddl/sys.config.cols';
/**
 * SYS_CONFIG--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysConfigSQL
 */
export const SysConfigSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let cols = SysConfigSQL.COL_MAPPER_SQL(dto);
    let where = SysConfigSQL.WHERE_SQL(dto);
    let group = SysConfigSQL.GROUP_BY_SQL(dto);
    let order = SysConfigSQL.ORDER_BY_SQL(dto);
    let limit = SysConfigSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let where = SysConfigSQL.WHERE_SQL(dto);
    let group = SysConfigSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let cols = SysConfigSQL.COL_MAPPER_SQL(dto);
    let group = SysConfigSQL.GROUP_BY_SQL(dto);
    let order = SysConfigSQL.ORDER_BY_SQL(dto);
    let limit = SysConfigSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysConfigSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysConfigSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysConfigSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let where = SysConfigSQL.WHERE_SQL(dto);
    let group = SysConfigSQL.GROUP_BY_SQL(dto);
    let order = SysConfigSQL.ORDER_BY_SQL(dto);
    let limit = SysConfigSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysConfigColNames, SysConfigColProps);
    if (!cols || cols.length == 0) cols = SysConfigColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[conId]?.length > 0) items.push(` ${CON_ID} = '${dto[conId]}'`);
    if (dto['nconId']?.length > 0) items.push(` ${CON_ID} != '${dto['nconId']}'`);
    if (dto[conName]?.length > 0) items.push(` ${CON_NAME} = '${dto[conName]}'`);
    if (dto['nconName']?.length > 0) items.push(` ${CON_NAME} != '${dto['nconName']}'`);
    if (dto[conNameEn]?.length > 0) items.push(` ${CON_NAME_EN} = '${dto[conNameEn]}'`);
    if (dto['nconNameEn']?.length > 0) items.push(` ${CON_NAME_EN} != '${dto['nconNameEn']}'`);
    if (dto[conValue]?.length > 0) items.push(` ${CON_VALUE} = '${dto[conValue]}'`);
    if (dto['nconValue']?.length > 0) items.push(` ${CON_VALUE} != '${dto['nconValue']}'`);
    if (dto[conGroup]?.length > 0) items.push(` ${CON_GROUP} = '${dto[conGroup]}'`);
    if (dto['nconGroup']?.length > 0) items.push(` ${CON_GROUP} != '${dto['nconGroup']}'`);
    if (dto[conParamType]?.length > 0) items.push(` ${CON_PARAM_TYPE} = '${dto[conParamType]}'`);
    if (dto['nconParamType']?.length > 0) items.push(` ${CON_PARAM_TYPE} != '${dto['nconParamType']}'`);
    if (dto[conParentId]?.length > 0) items.push(` ${CON_PARENT_ID} = '${dto[conParentId]}'`);
    if (dto['nconParentId']?.length > 0) items.push(` ${CON_PARENT_ID} != '${dto['nconParentId']}'`);
    if (dto[conType]?.length > 0) items.push(` ${CON_TYPE} = '${dto[conType]}'`);
    if (dto['nconType']?.length > 0) items.push(` ${CON_TYPE} != '${dto['nconType']}'`);
    if (dto[conCreateDate]) items.push(`${CON_CREATE_DATE} is not null and date_format(${CON_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[conCreateDate])}`);
    if (dto['nconCreateDate']) items.push(`${CON_CREATE_DATE} is not null and date_format(${CON_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nconCreateDate'])}`);
    if (dto[conUpdateDate]) items.push(`${CON_UPDATE_DATE} is not null and date_format(${CON_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[conUpdateDate])}`);
    if (dto['nconUpdateDate']) items.push(`${CON_UPDATE_DATE} is not null and date_format(${CON_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nconUpdateDate'])}`);
    if (dto[conManager]?.length > 0) items.push(` ${CON_MANAGER} = '${dto[conManager]}'`);
    if (dto['nconManager']?.length > 0) items.push(` ${CON_MANAGER} != '${dto['nconManager']}'`);
    if (dto[conSpecies]?.length > 0) items.push(` ${CON_SPECIES} = '${dto[conSpecies]}'`);
    if (dto['nconSpecies']?.length > 0) items.push(` ${CON_SPECIES} != '${dto['nconSpecies']}'`);
    if (dto[conOrder]) items.push(` ${CON_ORDER} = '${dto[conOrder]}'`);
    if (dto['nconOrder']) items.push(` ${CON_ORDER} != '${dto['nconOrder']}'`);
    if (dto[conState]?.length > 0) items.push(` ${CON_STATE} = '${dto[conState]}'`);
    if (dto['nconState']?.length > 0) items.push(` ${CON_STATE} != '${dto['nconState']}'`);
    if (dto[conRemark]?.length > 0) items.push(` ${CON_REMARK} = '${dto[conRemark]}'`);
    if (dto['nconRemark']?.length > 0) items.push(` ${CON_REMARK} != '${dto['nconRemark']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //CON_ID--字符串字段
    if (dto['conIdList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_ID, <string[]>dto['conIdList']));
    if (dto['nconIdList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_ID, <string[]>dto['nconIdList'], true));
    if (dto['conIdLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_ID, dto['conIdLike'])); //For MysSQL
    if (dto['nconIdLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_ID, dto['nconIdLike'], true)); //For MysSQL
    if (dto['conIdLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_ID, <string[]>dto['conIdLikeList'])); //For MysSQL
    //CON_NAME--字符串字段
    if (dto['conNameList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_NAME, <string[]>dto['conNameList']));
    if (dto['nconNameList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_NAME, <string[]>dto['nconNameList'], true));
    if (dto['conNameLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_NAME, dto['conNameLike'])); //For MysSQL
    if (dto['nconNameLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_NAME, dto['nconNameLike'], true)); //For MysSQL
    if (dto['conNameLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_NAME, <string[]>dto['conNameLikeList'])); //For MysSQL
    //CON_NAME_EN--字符串字段
    if (dto['conNameEnList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_NAME_EN, <string[]>dto['conNameEnList']));
    if (dto['nconNameEnList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_NAME_EN, <string[]>dto['nconNameEnList'], true));
    if (dto['conNameEnLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_NAME_EN, dto['conNameEnLike'])); //For MysSQL
    if (dto['nconNameEnLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_NAME_EN, dto['nconNameEnLike'], true)); //For MysSQL
    if (dto['conNameEnLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_NAME_EN, <string[]>dto['conNameEnLikeList'])); //For MysSQL
    //CON_VALUE--字符串字段
    if (dto['conValueList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_VALUE, <string[]>dto['conValueList']));
    if (dto['nconValueList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_VALUE, <string[]>dto['nconValueList'], true));
    if (dto['conValueLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_VALUE, dto['conValueLike'])); //For MysSQL
    if (dto['nconValueLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_VALUE, dto['nconValueLike'], true)); //For MysSQL
    if (dto['conValueLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_VALUE, <string[]>dto['conValueLikeList'])); //For MysSQL
    //CON_GROUP--字符串字段
    if (dto['conGroupList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_GROUP, <string[]>dto['conGroupList']));
    if (dto['nconGroupList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_GROUP, <string[]>dto['nconGroupList'], true));
    if (dto['conGroupLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_GROUP, dto['conGroupLike'])); //For MysSQL
    if (dto['nconGroupLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_GROUP, dto['nconGroupLike'], true)); //For MysSQL
    if (dto['conGroupLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_GROUP, <string[]>dto['conGroupLikeList'])); //For MysSQL
    //CON_PARAM_TYPE--字符串字段
    if (dto['conParamTypeList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_PARAM_TYPE, <string[]>dto['conParamTypeList']));
    if (dto['nconParamTypeList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_PARAM_TYPE, <string[]>dto['nconParamTypeList'], true));
    if (dto['conParamTypeLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_PARAM_TYPE, dto['conParamTypeLike'])); //For MysSQL
    if (dto['nconParamTypeLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_PARAM_TYPE, dto['nconParamTypeLike'], true)); //For MysSQL
    if (dto['conParamTypeLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_PARAM_TYPE, <string[]>dto['conParamTypeLikeList'])); //For MysSQL
    //CON_PARENT_ID--字符串字段
    if (dto['conParentIdList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_PARENT_ID, <string[]>dto['conParentIdList']));
    if (dto['nconParentIdList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_PARENT_ID, <string[]>dto['nconParentIdList'], true));
    if (dto['conParentIdLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_PARENT_ID, dto['conParentIdLike'])); //For MysSQL
    if (dto['nconParentIdLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_PARENT_ID, dto['nconParentIdLike'], true)); //For MysSQL
    if (dto['conParentIdLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_PARENT_ID, <string[]>dto['conParentIdLikeList'])); //For MysSQL
    //CON_TYPE--字符串字段
    if (dto['conTypeList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_TYPE, <string[]>dto['conTypeList']));
    if (dto['nconTypeList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_TYPE, <string[]>dto['nconTypeList'], true));
    if (dto['conTypeLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_TYPE, dto['conTypeLike'])); //For MysSQL
    if (dto['nconTypeLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_TYPE, dto['nconTypeLike'], true)); //For MysSQL
    if (dto['conTypeLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_TYPE, <string[]>dto['conTypeLikeList'])); //For MysSQL
    //CON_CREATE_DATE--日期字段
    if (dto[conCreateDate]) items.push(`${CON_CREATE_DATE} is not null and date_format(${CON_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sConCreateDate'])}`);
    if (dto[conCreateDate]) items.push(`${CON_CREATE_DATE} is not null and date_format(${CON_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eConCreateDate'])}`);
    //CON_UPDATE_DATE--日期字段
    if (dto[conUpdateDate]) items.push(`${CON_UPDATE_DATE} is not null and date_format(${CON_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sConUpdateDate'])}`);
    if (dto[conUpdateDate]) items.push(`${CON_UPDATE_DATE} is not null and date_format(${CON_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eConUpdateDate'])}`);
    //CON_MANAGER--字符串字段
    if (dto['conManagerList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_MANAGER, <string[]>dto['conManagerList']));
    if (dto['nconManagerList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_MANAGER, <string[]>dto['nconManagerList'], true));
    if (dto['conManagerLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_MANAGER, dto['conManagerLike'])); //For MysSQL
    if (dto['nconManagerLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_MANAGER, dto['nconManagerLike'], true)); //For MysSQL
    if (dto['conManagerLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_MANAGER, <string[]>dto['conManagerLikeList'])); //For MysSQL
    //CON_SPECIES--字符串字段
    if (dto['conSpeciesList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_SPECIES, <string[]>dto['conSpeciesList']));
    if (dto['nconSpeciesList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_SPECIES, <string[]>dto['nconSpeciesList'], true));
    if (dto['conSpeciesLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_SPECIES, dto['conSpeciesLike'])); //For MysSQL
    if (dto['nconSpeciesLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_SPECIES, dto['nconSpeciesLike'], true)); //For MysSQL
    if (dto['conSpeciesLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_SPECIES, <string[]>dto['conSpeciesLikeList'])); //For MysSQL
    //CON_ORDER--数字字段
    if (dto['conOrderList']) items.push(SysConfigSQL.IN_SQL(CON_ORDER, dto['conOrderList']));
    if (dto['nconOrderList']) items.push(SysConfigSQL.IN_SQL(CON_ORDER, dto['nconOrderList'], true));
    //CON_STATE--字符串字段
    if (dto['conStateList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_STATE, <string[]>dto['conStateList']));
    if (dto['nconStateList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_STATE, <string[]>dto['nconStateList'], true));
    if (dto['conStateLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_STATE, dto['conStateLike'])); //For MysSQL
    if (dto['nconStateLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_STATE, dto['nconStateLike'], true)); //For MysSQL
    if (dto['conStateLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_STATE, <string[]>dto['conStateLikeList'])); //For MysSQL
    //CON_REMARK--字符串字段
    if (dto['conRemarkList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_REMARK, <string[]>dto['conRemarkList']));
    if (dto['nconRemarkList']?.length > 0) items.push(SysConfigSQL.IN_SQL(CON_REMARK, <string[]>dto['nconRemarkList'], true));
    if (dto['conRemarkLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_REMARK, dto['conRemarkLike'])); //For MysSQL
    if (dto['nconRemarkLike']?.length > 0) items.push(SysConfigSQL.LIKE_SQL(CON_REMARK, dto['nconRemarkLike'], true)); //For MysSQL
    if (dto['conRemarkLikeList']?.length > 0) items.push(SysConfigSQL.LIKE_LIST_SQL(CON_REMARK, <string[]>dto['conRemarkLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysConfigSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysConfigCDto | Partial<SysConfigCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(CON_ID);
    cols.push(CON_NAME);
    cols.push(CON_NAME_EN);
    cols.push(CON_VALUE);
    cols.push(CON_GROUP);
    cols.push(CON_PARAM_TYPE);
    cols.push(CON_PARENT_ID);
    cols.push(CON_TYPE);
    cols.push(CON_CREATE_DATE);
    cols.push(CON_UPDATE_DATE);
    cols.push(CON_MANAGER);
    cols.push(CON_SPECIES);
    cols.push(CON_ORDER);
    cols.push(CON_STATE);
    cols.push(CON_REMARK);
    values.push(toSqlValue(dto[conId]));
    values.push(toSqlValue(dto[conName]));
    values.push(toSqlValue(dto[conNameEn]));
    values.push(toSqlValue(dto[conValue]));
    values.push(toSqlValue(dto[conGroup]));
    values.push(toSqlValue(dto[conParamType]));
    values.push(toSqlValue(dto[conParentId]));
    values.push(toSqlValue(dto[conType]));
    values.push('NOW()');
    values.push('NOW()');
    values.push(toSqlValue(dto[conManager]));
    values.push(toSqlValue(dto[conSpecies]));
    values.push(toSqlValue(dto[conOrder]));
    values.push(toSqlValue(dto[conState]));
    values.push(toSqlValue(dto[conRemark]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysConfigCDto[] | Partial<SysConfigCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(CON_ID);
    cols.push(CON_NAME);
    cols.push(CON_NAME_EN);
    cols.push(CON_VALUE);
    cols.push(CON_GROUP);
    cols.push(CON_PARAM_TYPE);
    cols.push(CON_PARENT_ID);
    cols.push(CON_TYPE);
    cols.push(CON_CREATE_DATE);
    cols.push(CON_UPDATE_DATE);
    cols.push(CON_MANAGER);
    cols.push(CON_SPECIES);
    cols.push(CON_ORDER);
    cols.push(CON_STATE);
    cols.push(CON_REMARK);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[conId]));
      values.push(toSqlValue(dto[conName]));
      values.push(toSqlValue(dto[conNameEn]));
      values.push(toSqlValue(dto[conValue]));
      values.push(toSqlValue(dto[conGroup]));
      values.push(toSqlValue(dto[conParamType]));
      values.push(toSqlValue(dto[conParentId]));
      values.push(toSqlValue(dto[conType]));
      values.push('NOW()');
      values.push('NOW()');
      values.push(toSqlValue(dto[conManager]));
      values.push(toSqlValue(dto[conSpecies]));
      values.push(toSqlValue(dto[conOrder]));
      values.push(toSqlValue(dto[conState]));
      values.push(toSqlValue(dto[conRemark]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysConfigUDto | Partial<SysConfigUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysConfigColNames, SysConfigColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${CON_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CON_NAME} = ${toSqlValue(dto[conName])}`);
    items.push(`${CON_NAME_EN} = ${toSqlValue(dto[conNameEn])}`);
    items.push(`${CON_VALUE} = ${toSqlValue(dto[conValue])}`);
    items.push(`${CON_GROUP} = ${toSqlValue(dto[conGroup])}`);
    items.push(`${CON_PARAM_TYPE} = ${toSqlValue(dto[conParamType])}`);
    items.push(`${CON_PARENT_ID} = ${toSqlValue(dto[conParentId])}`);
    items.push(`${CON_TYPE} = ${toSqlValue(dto[conType])}`);
    items.push(`${CON_UPDATE_DATE} = NOW()`);
    items.push(`${CON_MANAGER} = ${toSqlValue(dto[conManager])}`);
    items.push(`${CON_SPECIES} = ${toSqlValue(dto[conSpecies])}`);
    items.push(`${CON_ORDER} = ${toSqlValue(dto[conOrder])}`);
    items.push(`${CON_STATE} = ${toSqlValue(dto[conState])}`);
    items.push(`${CON_REMARK} = ${toSqlValue(dto[conRemark])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysConfigUDto | Partial<SysConfigUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysConfigColNames, SysConfigColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${CON_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysConfigSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CON_NAME} = ${toSqlValue(dto[conName])}`);
    items.push(`${CON_NAME_EN} = ${toSqlValue(dto[conNameEn])}`);
    items.push(`${CON_VALUE} = ${toSqlValue(dto[conValue])}`);
    items.push(`${CON_GROUP} = ${toSqlValue(dto[conGroup])}`);
    items.push(`${CON_PARAM_TYPE} = ${toSqlValue(dto[conParamType])}`);
    items.push(`${CON_PARENT_ID} = ${toSqlValue(dto[conParentId])}`);
    items.push(`${CON_TYPE} = ${toSqlValue(dto[conType])}`);
    items.push(`${CON_UPDATE_DATE} = NOW()`);
    items.push(`${CON_MANAGER} = ${toSqlValue(dto[conManager])}`);
    items.push(`${CON_SPECIES} = ${toSqlValue(dto[conSpecies])}`);
    items.push(`${CON_ORDER} = ${toSqlValue(dto[conOrder])}`);
    items.push(`${CON_STATE} = ${toSqlValue(dto[conState])}`);
    items.push(`${CON_REMARK} = ${toSqlValue(dto[conRemark])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysConfigSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysConfigUDto[] | Partial<SysConfigUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysConfigSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysConfigUDto | Partial<SysConfigUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[conName]?.length > 0) items.push(`${CON_NAME} = ${toSqlValue(dto[conName])}`);
    if (dto[conNameEn]?.length > 0) items.push(`${CON_NAME_EN} = ${toSqlValue(dto[conNameEn])}`);
    if (dto[conValue]?.length > 0) items.push(`${CON_VALUE} = ${toSqlValue(dto[conValue])}`);
    if (dto[conGroup]?.length > 0) items.push(`${CON_GROUP} = ${toSqlValue(dto[conGroup])}`);
    if (dto[conParamType]?.length > 0) items.push(`${CON_PARAM_TYPE} = ${toSqlValue(dto[conParamType])}`);
    if (dto[conParentId]?.length > 0) items.push(`${CON_PARENT_ID} = ${toSqlValue(dto[conParentId])}`);
    if (dto[conType]?.length > 0) items.push(`${CON_TYPE} = ${toSqlValue(dto[conType])}`);
    if (dto[conUpdateDate]) items.push(`${CON_UPDATE_DATE}= NOW()`);
    if (dto[conManager]?.length > 0) items.push(`${CON_MANAGER} = ${toSqlValue(dto[conManager])}`);
    if (dto[conSpecies]?.length > 0) items.push(`${CON_SPECIES} = ${toSqlValue(dto[conSpecies])}`);
    if (dto[conOrder]) items.push(`${CON_ORDER} = ${toSqlValue(dto[conOrder])}`);
    if (dto[conState]?.length > 0) items.push(`${CON_STATE} = ${toSqlValue(dto[conState])}`);
    if (dto[conRemark]?.length > 0) items.push(`${CON_REMARK} = ${toSqlValue(dto[conRemark])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysConfigUDto[] | Partial<SysConfigUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysConfigSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysConfigQDto | Partial<SysConfigQDto>): string => {
    if (!dto) return '';
    let where = SysConfigSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysConfigSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_CONFIG';
/**
 * 列名常量字段
 */
const CON_ID = SysConfigColNameEnum.CON_ID;
const CON_NAME = SysConfigColNameEnum.CON_NAME;
const CON_NAME_EN = SysConfigColNameEnum.CON_NAME_EN;
const CON_VALUE = SysConfigColNameEnum.CON_VALUE;
const CON_GROUP = SysConfigColNameEnum.CON_GROUP;
const CON_PARAM_TYPE = SysConfigColNameEnum.CON_PARAM_TYPE;
const CON_PARENT_ID = SysConfigColNameEnum.CON_PARENT_ID;
const CON_TYPE = SysConfigColNameEnum.CON_TYPE;
const CON_CREATE_DATE = SysConfigColNameEnum.CON_CREATE_DATE;
const CON_UPDATE_DATE = SysConfigColNameEnum.CON_UPDATE_DATE;
const CON_MANAGER = SysConfigColNameEnum.CON_MANAGER;
const CON_SPECIES = SysConfigColNameEnum.CON_SPECIES;
const CON_ORDER = SysConfigColNameEnum.CON_ORDER;
const CON_STATE = SysConfigColNameEnum.CON_STATE;
const CON_REMARK = SysConfigColNameEnum.CON_REMARK;
/**
 * 实体类属性名
 */
const conId = SysConfigColPropEnum.conId;
const conName = SysConfigColPropEnum.conName;
const conNameEn = SysConfigColPropEnum.conNameEn;
const conValue = SysConfigColPropEnum.conValue;
const conGroup = SysConfigColPropEnum.conGroup;
const conParamType = SysConfigColPropEnum.conParamType;
const conParentId = SysConfigColPropEnum.conParentId;
const conType = SysConfigColPropEnum.conType;
const conCreateDate = SysConfigColPropEnum.conCreateDate;
const conUpdateDate = SysConfigColPropEnum.conUpdateDate;
const conManager = SysConfigColPropEnum.conManager;
const conSpecies = SysConfigColPropEnum.conSpecies;
const conOrder = SysConfigColPropEnum.conOrder;
const conState = SysConfigColPropEnum.conState;
const conRemark = SysConfigColPropEnum.conRemark;
/**
 * 主键信息
 */
const PRIMER_KEY = SysConfigTable.PRIMER_KEY;
const primerKey = SysConfigTable.primerKey;
