import { SysUpdateQDto } from '../../dto/query/sys.update.qdto';
import { SysUpdateCDto } from '../../dto/create/sys.update.cdto';
import { SysUpdateUDto } from '../../dto/update/sys.update.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysUpdateColNameEnum, SysUpdateColPropEnum, SysUpdateTable, SysUpdateColNames, SysUpdateColProps } from '../ddl/sys.update.cols';
/**
 * SYS_UPDATE--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysUpdateSQL
 */
export const SysUpdateSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let cols = SysUpdateSQL.COL_MAPPER_SQL(dto);
    let where = SysUpdateSQL.WHERE_SQL(dto);
    let group = SysUpdateSQL.GROUP_BY_SQL(dto);
    let order = SysUpdateSQL.ORDER_BY_SQL(dto);
    let limit = SysUpdateSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let where = SysUpdateSQL.WHERE_SQL(dto);
    let group = SysUpdateSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let cols = SysUpdateSQL.COL_MAPPER_SQL(dto);
    let group = SysUpdateSQL.GROUP_BY_SQL(dto);
    let order = SysUpdateSQL.ORDER_BY_SQL(dto);
    let limit = SysUpdateSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysUpdateSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysUpdateSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysUpdateSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let where = SysUpdateSQL.WHERE_SQL(dto);
    let group = SysUpdateSQL.GROUP_BY_SQL(dto);
    let order = SysUpdateSQL.ORDER_BY_SQL(dto);
    let limit = SysUpdateSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysUpdateColNames, SysUpdateColProps);
    if (!cols || cols.length == 0) cols = SysUpdateColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[updateId]?.length > 0) items.push(` ${UPDATE_ID} = '${dto[updateId]}'`);
    if (dto['nupdateId']?.length > 0) items.push(` ${UPDATE_ID} != '${dto['nupdateId']}'`);
    if (dto[updateVersion]?.length > 0) items.push(` ${UPDATE_VERSION} = '${dto[updateVersion]}'`);
    if (dto['nupdateVersion']?.length > 0) items.push(` ${UPDATE_VERSION} != '${dto['nupdateVersion']}'`);
    if (dto[updateContent]?.length > 0) items.push(` ${UPDATE_CONTENT} = '${dto[updateContent]}'`);
    if (dto['nupdateContent']?.length > 0) items.push(` ${UPDATE_CONTENT} != '${dto['nupdateContent']}'`);
    if (dto[updateManager]?.length > 0) items.push(` ${UPDATE_MANAGER} = '${dto[updateManager]}'`);
    if (dto['nupdateManager']?.length > 0) items.push(` ${UPDATE_MANAGER} != '${dto['nupdateManager']}'`);
    if (dto[updateCreateTime]) items.push(`${UPDATE_CREATE_TIME} is not null and date_format(${UPDATE_CREATE_TIME},'%Y-%m-%d') = ${toSqlValue(dto[updateCreateTime])}`);
    if (dto['nupdateCreateTime']) items.push(`${UPDATE_CREATE_TIME} is not null and date_format(${UPDATE_CREATE_TIME},'%Y-%m-%d') != ${toSqlValue(dto['nupdateCreateTime'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //UPDATE_ID--字符串字段
    if (dto['updateIdList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_ID, <string[]>dto['updateIdList']));
    if (dto['nupdateIdList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_ID, <string[]>dto['nupdateIdList'], true));
    if (dto['updateIdLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_ID, dto['updateIdLike'])); //For MysSQL
    if (dto['nupdateIdLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_ID, dto['nupdateIdLike'], true)); //For MysSQL
    if (dto['updateIdLikeList']?.length > 0) items.push(SysUpdateSQL.LIKE_LIST_SQL(UPDATE_ID, <string[]>dto['updateIdLikeList'])); //For MysSQL
    //UPDATE_VERSION--字符串字段
    if (dto['updateVersionList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_VERSION, <string[]>dto['updateVersionList']));
    if (dto['nupdateVersionList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_VERSION, <string[]>dto['nupdateVersionList'], true));
    if (dto['updateVersionLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_VERSION, dto['updateVersionLike'])); //For MysSQL
    if (dto['nupdateVersionLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_VERSION, dto['nupdateVersionLike'], true)); //For MysSQL
    if (dto['updateVersionLikeList']?.length > 0) items.push(SysUpdateSQL.LIKE_LIST_SQL(UPDATE_VERSION, <string[]>dto['updateVersionLikeList'])); //For MysSQL
    //UPDATE_CONTENT--字符串字段
    if (dto['updateContentList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_CONTENT, <string[]>dto['updateContentList']));
    if (dto['nupdateContentList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_CONTENT, <string[]>dto['nupdateContentList'], true));
    if (dto['updateContentLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_CONTENT, dto['updateContentLike'])); //For MysSQL
    if (dto['nupdateContentLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_CONTENT, dto['nupdateContentLike'], true)); //For MysSQL
    if (dto['updateContentLikeList']?.length > 0) items.push(SysUpdateSQL.LIKE_LIST_SQL(UPDATE_CONTENT, <string[]>dto['updateContentLikeList'])); //For MysSQL
    //UPDATE_MANAGER--字符串字段
    if (dto['updateManagerList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_MANAGER, <string[]>dto['updateManagerList']));
    if (dto['nupdateManagerList']?.length > 0) items.push(SysUpdateSQL.IN_SQL(UPDATE_MANAGER, <string[]>dto['nupdateManagerList'], true));
    if (dto['updateManagerLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_MANAGER, dto['updateManagerLike'])); //For MysSQL
    if (dto['nupdateManagerLike']?.length > 0) items.push(SysUpdateSQL.LIKE_SQL(UPDATE_MANAGER, dto['nupdateManagerLike'], true)); //For MysSQL
    if (dto['updateManagerLikeList']?.length > 0) items.push(SysUpdateSQL.LIKE_LIST_SQL(UPDATE_MANAGER, <string[]>dto['updateManagerLikeList'])); //For MysSQL
    //UPDATE_CREATE_TIME--日期字段
    if (dto[updateCreateTime]) items.push(`${UPDATE_CREATE_TIME} is not null and date_format(${UPDATE_CREATE_TIME},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUpdateCreateTime'])}`);
    if (dto[updateCreateTime]) items.push(`${UPDATE_CREATE_TIME} is not null and date_format(${UPDATE_CREATE_TIME},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUpdateCreateTime'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysUpdateSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysUpdateCDto | Partial<SysUpdateCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(UPDATE_ID);
    cols.push(UPDATE_VERSION);
    cols.push(UPDATE_CONTENT);
    cols.push(UPDATE_MANAGER);
    cols.push(UPDATE_CREATE_TIME);
    values.push(toSqlValue(dto[updateId]));
    values.push(toSqlValue(dto[updateVersion]));
    values.push(toSqlValue(dto[updateContent]));
    values.push(toSqlValue(dto[updateManager]));
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysUpdateCDto[] | Partial<SysUpdateCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(UPDATE_ID);
    cols.push(UPDATE_VERSION);
    cols.push(UPDATE_CONTENT);
    cols.push(UPDATE_MANAGER);
    cols.push(UPDATE_CREATE_TIME);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[updateId]));
      values.push(toSqlValue(dto[updateVersion]));
      values.push(toSqlValue(dto[updateContent]));
      values.push(toSqlValue(dto[updateManager]));
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysUpdateUDto | Partial<SysUpdateUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUpdateColNames, SysUpdateColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UPDATE_VERSION} = ${toSqlValue(dto[updateVersion])}`);
    items.push(`${UPDATE_CONTENT} = ${toSqlValue(dto[updateContent])}`);
    items.push(`${UPDATE_MANAGER} = ${toSqlValue(dto[updateManager])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysUpdateUDto | Partial<SysUpdateUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUpdateColNames, SysUpdateColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUpdateSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UPDATE_VERSION} = ${toSqlValue(dto[updateVersion])}`);
    items.push(`${UPDATE_CONTENT} = ${toSqlValue(dto[updateContent])}`);
    items.push(`${UPDATE_MANAGER} = ${toSqlValue(dto[updateManager])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUpdateSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysUpdateUDto[] | Partial<SysUpdateUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysUpdateSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysUpdateUDto | Partial<SysUpdateUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[updateVersion]?.length > 0) items.push(`${UPDATE_VERSION} = ${toSqlValue(dto[updateVersion])}`);
    if (dto[updateContent]?.length > 0) items.push(`${UPDATE_CONTENT} = ${toSqlValue(dto[updateContent])}`);
    if (dto[updateManager]?.length > 0) items.push(`${UPDATE_MANAGER} = ${toSqlValue(dto[updateManager])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysUpdateUDto[] | Partial<SysUpdateUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysUpdateSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysUpdateQDto | Partial<SysUpdateQDto>): string => {
    if (!dto) return '';
    let where = SysUpdateSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysUpdateSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_UPDATE';
/**
 * 列名常量字段
 */
const UPDATE_ID = SysUpdateColNameEnum.UPDATE_ID;
const UPDATE_VERSION = SysUpdateColNameEnum.UPDATE_VERSION;
const UPDATE_CONTENT = SysUpdateColNameEnum.UPDATE_CONTENT;
const UPDATE_MANAGER = SysUpdateColNameEnum.UPDATE_MANAGER;
const UPDATE_CREATE_TIME = SysUpdateColNameEnum.UPDATE_CREATE_TIME;
/**
 * 实体类属性名
 */
const updateId = SysUpdateColPropEnum.updateId;
const updateVersion = SysUpdateColPropEnum.updateVersion;
const updateContent = SysUpdateColPropEnum.updateContent;
const updateManager = SysUpdateColPropEnum.updateManager;
const updateCreateTime = SysUpdateColPropEnum.updateCreateTime;
/**
 * 主键信息
 */
const PRIMER_KEY = SysUpdateTable.PRIMER_KEY;
const primerKey = SysUpdateTable.primerKey;
