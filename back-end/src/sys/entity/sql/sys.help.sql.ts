import { SysHelpQDto } from '../../dto/query/sys.help.qdto';
import { SysHelpCDto } from '../../dto/create/sys.help.cdto';
import { SysHelpUDto } from '../../dto/update/sys.help.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysHelpColNameEnum, SysHelpColPropEnum, SysHelpTable, SysHelpColNames, SysHelpColProps } from '../ddl/sys.help.cols';
/**
 * SYS_HELP--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysHelpSQL
 */
export const SysHelpSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let cols = SysHelpSQL.COL_MAPPER_SQL(dto);
    let where = SysHelpSQL.WHERE_SQL(dto);
    let group = SysHelpSQL.GROUP_BY_SQL(dto);
    let order = SysHelpSQL.ORDER_BY_SQL(dto);
    let limit = SysHelpSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let where = SysHelpSQL.WHERE_SQL(dto);
    let group = SysHelpSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let cols = SysHelpSQL.COL_MAPPER_SQL(dto);
    let group = SysHelpSQL.GROUP_BY_SQL(dto);
    let order = SysHelpSQL.ORDER_BY_SQL(dto);
    let limit = SysHelpSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysHelpSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysHelpSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysHelpSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let where = SysHelpSQL.WHERE_SQL(dto);
    let group = SysHelpSQL.GROUP_BY_SQL(dto);
    let order = SysHelpSQL.ORDER_BY_SQL(dto);
    let limit = SysHelpSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysHelpColNames, SysHelpColProps);
    if (!cols || cols.length == 0) cols = SysHelpColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[helpId]?.length > 0) items.push(` ${HELP_ID} = '${dto[helpId]}'`);
    if (dto['nhelpId']?.length > 0) items.push(` ${HELP_ID} != '${dto['nhelpId']}'`);
    if (dto[helpQuestion]?.length > 0) items.push(` ${HELP_QUESTION} = '${dto[helpQuestion]}'`);
    if (dto['nhelpQuestion']?.length > 0) items.push(` ${HELP_QUESTION} != '${dto['nhelpQuestion']}'`);
    if (dto[helpAnswer]?.length > 0) items.push(` ${HELP_ANSWER} = '${dto[helpAnswer]}'`);
    if (dto['nhelpAnswer']?.length > 0) items.push(` ${HELP_ANSWER} != '${dto['nhelpAnswer']}'`);
    if (dto[helpParentId]?.length > 0) items.push(` ${HELP_PARENT_ID} = '${dto[helpParentId]}'`);
    if (dto['nhelpParentId']?.length > 0) items.push(` ${HELP_PARENT_ID} != '${dto['nhelpParentId']}'`);
    if (dto[helpType]?.length > 0) items.push(` ${HELP_TYPE} = '${dto[helpType]}'`);
    if (dto['nhelpType']?.length > 0) items.push(` ${HELP_TYPE} != '${dto['nhelpType']}'`);
    if (dto[helpPath]?.length > 0) items.push(` ${HELP_PATH} = '${dto[helpPath]}'`);
    if (dto['nhelpPath']?.length > 0) items.push(` ${HELP_PATH} != '${dto['nhelpPath']}'`);
    if (dto[helpSort]?.length > 0) items.push(` ${HELP_SORT} = '${dto[helpSort]}'`);
    if (dto['nhelpSort']?.length > 0) items.push(` ${HELP_SORT} != '${dto['nhelpSort']}'`);
    if (dto[helpCreateDate]) items.push(`${HELP_CREATE_DATE} is not null and date_format(${HELP_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[helpCreateDate])}`);
    if (dto['nhelpCreateDate']) items.push(`${HELP_CREATE_DATE} is not null and date_format(${HELP_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nhelpCreateDate'])}`);
    if (dto[helpUpdateDate]) items.push(`${HELP_UPDATE_DATE} is not null and date_format(${HELP_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[helpUpdateDate])}`);
    if (dto['nhelpUpdateDate']) items.push(`${HELP_UPDATE_DATE} is not null and date_format(${HELP_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nhelpUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //HELP_ID--字符串字段
    if (dto['helpIdList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_ID, <string[]>dto['helpIdList']));
    if (dto['nhelpIdList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_ID, <string[]>dto['nhelpIdList'], true));
    if (dto['helpIdLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_ID, dto['helpIdLike'])); //For MysSQL
    if (dto['nhelpIdLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_ID, dto['nhelpIdLike'], true)); //For MysSQL
    if (dto['helpIdLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_ID, <string[]>dto['helpIdLikeList'])); //For MysSQL
    //HELP_QUESTION--字符串字段
    if (dto['helpQuestionList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_QUESTION, <string[]>dto['helpQuestionList']));
    if (dto['nhelpQuestionList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_QUESTION, <string[]>dto['nhelpQuestionList'], true));
    if (dto['helpQuestionLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_QUESTION, dto['helpQuestionLike'])); //For MysSQL
    if (dto['nhelpQuestionLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_QUESTION, dto['nhelpQuestionLike'], true)); //For MysSQL
    if (dto['helpQuestionLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_QUESTION, <string[]>dto['helpQuestionLikeList'])); //For MysSQL
    //HELP_ANSWER--字符串字段
    if (dto['helpAnswerList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_ANSWER, <string[]>dto['helpAnswerList']));
    if (dto['nhelpAnswerList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_ANSWER, <string[]>dto['nhelpAnswerList'], true));
    if (dto['helpAnswerLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_ANSWER, dto['helpAnswerLike'])); //For MysSQL
    if (dto['nhelpAnswerLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_ANSWER, dto['nhelpAnswerLike'], true)); //For MysSQL
    if (dto['helpAnswerLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_ANSWER, <string[]>dto['helpAnswerLikeList'])); //For MysSQL
    //HELP_PARENT_ID--字符串字段
    if (dto['helpParentIdList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_PARENT_ID, <string[]>dto['helpParentIdList']));
    if (dto['nhelpParentIdList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_PARENT_ID, <string[]>dto['nhelpParentIdList'], true));
    if (dto['helpParentIdLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_PARENT_ID, dto['helpParentIdLike'])); //For MysSQL
    if (dto['nhelpParentIdLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_PARENT_ID, dto['nhelpParentIdLike'], true)); //For MysSQL
    if (dto['helpParentIdLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_PARENT_ID, <string[]>dto['helpParentIdLikeList'])); //For MysSQL
    //HELP_TYPE--字符串字段
    if (dto['helpTypeList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_TYPE, <string[]>dto['helpTypeList']));
    if (dto['nhelpTypeList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_TYPE, <string[]>dto['nhelpTypeList'], true));
    if (dto['helpTypeLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_TYPE, dto['helpTypeLike'])); //For MysSQL
    if (dto['nhelpTypeLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_TYPE, dto['nhelpTypeLike'], true)); //For MysSQL
    if (dto['helpTypeLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_TYPE, <string[]>dto['helpTypeLikeList'])); //For MysSQL
    //HELP_PATH--字符串字段
    if (dto['helpPathList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_PATH, <string[]>dto['helpPathList']));
    if (dto['nhelpPathList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_PATH, <string[]>dto['nhelpPathList'], true));
    if (dto['helpPathLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_PATH, dto['helpPathLike'])); //For MysSQL
    if (dto['nhelpPathLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_PATH, dto['nhelpPathLike'], true)); //For MysSQL
    if (dto['helpPathLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_PATH, <string[]>dto['helpPathLikeList'])); //For MysSQL
    //HELP_SORT--字符串字段
    if (dto['helpSortList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_SORT, <string[]>dto['helpSortList']));
    if (dto['nhelpSortList']?.length > 0) items.push(SysHelpSQL.IN_SQL(HELP_SORT, <string[]>dto['nhelpSortList'], true));
    if (dto['helpSortLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_SORT, dto['helpSortLike'])); //For MysSQL
    if (dto['nhelpSortLike']?.length > 0) items.push(SysHelpSQL.LIKE_SQL(HELP_SORT, dto['nhelpSortLike'], true)); //For MysSQL
    if (dto['helpSortLikeList']?.length > 0) items.push(SysHelpSQL.LIKE_LIST_SQL(HELP_SORT, <string[]>dto['helpSortLikeList'])); //For MysSQL
    //HELP_CREATE_DATE--日期字段
    if (dto[helpCreateDate]) items.push(`${HELP_CREATE_DATE} is not null and date_format(${HELP_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sHelpCreateDate'])}`);
    if (dto[helpCreateDate]) items.push(`${HELP_CREATE_DATE} is not null and date_format(${HELP_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eHelpCreateDate'])}`);
    //HELP_UPDATE_DATE--日期字段
    if (dto[helpUpdateDate]) items.push(`${HELP_UPDATE_DATE} is not null and date_format(${HELP_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sHelpUpdateDate'])}`);
    if (dto[helpUpdateDate]) items.push(`${HELP_UPDATE_DATE} is not null and date_format(${HELP_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eHelpUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysHelpSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysHelpCDto | Partial<SysHelpCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(HELP_ID);
    cols.push(HELP_QUESTION);
    cols.push(HELP_ANSWER);
    cols.push(HELP_PARENT_ID);
    cols.push(HELP_TYPE);
    cols.push(HELP_PATH);
    cols.push(HELP_SORT);
    cols.push(HELP_CREATE_DATE);
    cols.push(HELP_UPDATE_DATE);
    values.push(toSqlValue(dto[helpId]));
    values.push(toSqlValue(dto[helpQuestion]));
    values.push(toSqlValue(dto[helpAnswer]));
    values.push(toSqlValue(dto[helpParentId]));
    values.push(toSqlValue(dto[helpType]));
    values.push(toSqlValue(dto[helpPath]));
    values.push(toSqlValue(dto[helpSort]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysHelpCDto[] | Partial<SysHelpCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(HELP_ID);
    cols.push(HELP_QUESTION);
    cols.push(HELP_ANSWER);
    cols.push(HELP_PARENT_ID);
    cols.push(HELP_TYPE);
    cols.push(HELP_PATH);
    cols.push(HELP_SORT);
    cols.push(HELP_CREATE_DATE);
    cols.push(HELP_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[helpId]));
      values.push(toSqlValue(dto[helpQuestion]));
      values.push(toSqlValue(dto[helpAnswer]));
      values.push(toSqlValue(dto[helpParentId]));
      values.push(toSqlValue(dto[helpType]));
      values.push(toSqlValue(dto[helpPath]));
      values.push(toSqlValue(dto[helpSort]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysHelpUDto | Partial<SysHelpUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysHelpColNames, SysHelpColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${HELP_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${HELP_QUESTION} = ${toSqlValue(dto[helpQuestion])}`);
    items.push(`${HELP_ANSWER} = ${toSqlValue(dto[helpAnswer])}`);
    items.push(`${HELP_PARENT_ID} = ${toSqlValue(dto[helpParentId])}`);
    items.push(`${HELP_TYPE} = ${toSqlValue(dto[helpType])}`);
    items.push(`${HELP_PATH} = ${toSqlValue(dto[helpPath])}`);
    items.push(`${HELP_SORT} = ${toSqlValue(dto[helpSort])}`);
    items.push(`${HELP_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysHelpUDto | Partial<SysHelpUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysHelpColNames, SysHelpColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${HELP_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysHelpSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${HELP_QUESTION} = ${toSqlValue(dto[helpQuestion])}`);
    items.push(`${HELP_ANSWER} = ${toSqlValue(dto[helpAnswer])}`);
    items.push(`${HELP_PARENT_ID} = ${toSqlValue(dto[helpParentId])}`);
    items.push(`${HELP_TYPE} = ${toSqlValue(dto[helpType])}`);
    items.push(`${HELP_PATH} = ${toSqlValue(dto[helpPath])}`);
    items.push(`${HELP_SORT} = ${toSqlValue(dto[helpSort])}`);
    items.push(`${HELP_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysHelpSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysHelpUDto[] | Partial<SysHelpUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysHelpSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysHelpUDto | Partial<SysHelpUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[helpQuestion]?.length > 0) items.push(`${HELP_QUESTION} = ${toSqlValue(dto[helpQuestion])}`);
    if (dto[helpAnswer]?.length > 0) items.push(`${HELP_ANSWER} = ${toSqlValue(dto[helpAnswer])}`);
    if (dto[helpParentId]?.length > 0) items.push(`${HELP_PARENT_ID} = ${toSqlValue(dto[helpParentId])}`);
    if (dto[helpType]?.length > 0) items.push(`${HELP_TYPE} = ${toSqlValue(dto[helpType])}`);
    if (dto[helpPath]?.length > 0) items.push(`${HELP_PATH} = ${toSqlValue(dto[helpPath])}`);
    if (dto[helpSort]?.length > 0) items.push(`${HELP_SORT} = ${toSqlValue(dto[helpSort])}`);
    if (dto[helpUpdateDate]) items.push(`${HELP_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysHelpUDto[] | Partial<SysHelpUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysHelpSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysHelpQDto | Partial<SysHelpQDto>): string => {
    if (!dto) return '';
    let where = SysHelpSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysHelpSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_HELP';
/**
 * 列名常量字段
 */
const HELP_ID = SysHelpColNameEnum.HELP_ID;
const HELP_QUESTION = SysHelpColNameEnum.HELP_QUESTION;
const HELP_ANSWER = SysHelpColNameEnum.HELP_ANSWER;
const HELP_PARENT_ID = SysHelpColNameEnum.HELP_PARENT_ID;
const HELP_TYPE = SysHelpColNameEnum.HELP_TYPE;
const HELP_PATH = SysHelpColNameEnum.HELP_PATH;
const HELP_SORT = SysHelpColNameEnum.HELP_SORT;
const HELP_CREATE_DATE = SysHelpColNameEnum.HELP_CREATE_DATE;
const HELP_UPDATE_DATE = SysHelpColNameEnum.HELP_UPDATE_DATE;
/**
 * 实体类属性名
 */
const helpId = SysHelpColPropEnum.helpId;
const helpQuestion = SysHelpColPropEnum.helpQuestion;
const helpAnswer = SysHelpColPropEnum.helpAnswer;
const helpParentId = SysHelpColPropEnum.helpParentId;
const helpType = SysHelpColPropEnum.helpType;
const helpPath = SysHelpColPropEnum.helpPath;
const helpSort = SysHelpColPropEnum.helpSort;
const helpCreateDate = SysHelpColPropEnum.helpCreateDate;
const helpUpdateDate = SysHelpColPropEnum.helpUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysHelpTable.PRIMER_KEY;
const primerKey = SysHelpTable.primerKey;
