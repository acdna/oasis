import { SysDictionaryQDto } from '../../dto/query/sys.dictionary.qdto';
import { SysDictionaryCDto } from '../../dto/create/sys.dictionary.cdto';
import { SysDictionaryUDto } from '../../dto/update/sys.dictionary.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysDictionaryColNameEnum, SysDictionaryColPropEnum, SysDictionaryTable, SysDictionaryColNames, SysDictionaryColProps } from '../ddl/sys.dictionary.cols';
/**
 * SYS_DICTIONARY--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysDictionarySQL
 */
export const SysDictionarySQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let cols = SysDictionarySQL.COL_MAPPER_SQL(dto);
    let where = SysDictionarySQL.WHERE_SQL(dto);
    let group = SysDictionarySQL.GROUP_BY_SQL(dto);
    let order = SysDictionarySQL.ORDER_BY_SQL(dto);
    let limit = SysDictionarySQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let where = SysDictionarySQL.WHERE_SQL(dto);
    let group = SysDictionarySQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let cols = SysDictionarySQL.COL_MAPPER_SQL(dto);
    let group = SysDictionarySQL.GROUP_BY_SQL(dto);
    let order = SysDictionarySQL.ORDER_BY_SQL(dto);
    let limit = SysDictionarySQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysDictionarySQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysDictionarySQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysDictionarySQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let where = SysDictionarySQL.WHERE_SQL(dto);
    let group = SysDictionarySQL.GROUP_BY_SQL(dto);
    let order = SysDictionarySQL.ORDER_BY_SQL(dto);
    let limit = SysDictionarySQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysDictionaryColNames, SysDictionaryColProps);
    if (!cols || cols.length == 0) cols = SysDictionaryColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[dicId]?.length > 0) items.push(` ${DIC_ID} = '${dto[dicId]}'`);
    if (dto['ndicId']?.length > 0) items.push(` ${DIC_ID} != '${dto['ndicId']}'`);
    if (dto[dicName]?.length > 0) items.push(` ${DIC_NAME} = '${dto[dicName]}'`);
    if (dto['ndicName']?.length > 0) items.push(` ${DIC_NAME} != '${dto['ndicName']}'`);
    if (dto[dicNameEn]?.length > 0) items.push(` ${DIC_NAME_EN} = '${dto[dicNameEn]}'`);
    if (dto['ndicNameEn']?.length > 0) items.push(` ${DIC_NAME_EN} != '${dto['ndicNameEn']}'`);
    if (dto[dicValue]?.length > 0) items.push(` ${DIC_VALUE} = '${dto[dicValue]}'`);
    if (dto['ndicValue']?.length > 0) items.push(` ${DIC_VALUE} != '${dto['ndicValue']}'`);
    if (dto[dicGroup]?.length > 0) items.push(` ${DIC_GROUP} = '${dto[dicGroup]}'`);
    if (dto['ndicGroup']?.length > 0) items.push(` ${DIC_GROUP} != '${dto['ndicGroup']}'`);
    if (dto[dicModule]?.length > 0) items.push(` ${DIC_MODULE} = '${dto[dicModule]}'`);
    if (dto['ndicModule']?.length > 0) items.push(` ${DIC_MODULE} != '${dto['ndicModule']}'`);
    if (dto[dicParentId]?.length > 0) items.push(` ${DIC_PARENT_ID} = '${dto[dicParentId]}'`);
    if (dto['ndicParentId']?.length > 0) items.push(` ${DIC_PARENT_ID} != '${dto['ndicParentId']}'`);
    if (dto[dicType]?.length > 0) items.push(` ${DIC_TYPE} = '${dto[dicType]}'`);
    if (dto['ndicType']?.length > 0) items.push(` ${DIC_TYPE} != '${dto['ndicType']}'`);
    if (dto[dicOrder]) items.push(` ${DIC_ORDER} = '${dto[dicOrder]}'`);
    if (dto['ndicOrder']) items.push(` ${DIC_ORDER} != '${dto['ndicOrder']}'`);
    if (dto[dicState]?.length > 0) items.push(` ${DIC_STATE} = '${dto[dicState]}'`);
    if (dto['ndicState']?.length > 0) items.push(` ${DIC_STATE} != '${dto['ndicState']}'`);
    if (dto[dicParams]?.length > 0) items.push(` ${DIC_PARAMS} = '${dto[dicParams]}'`);
    if (dto['ndicParams']?.length > 0) items.push(` ${DIC_PARAMS} != '${dto['ndicParams']}'`);
    if (dto[dicSpecies]?.length > 0) items.push(` ${DIC_SPECIES} = '${dto[dicSpecies]}'`);
    if (dto['ndicSpecies']?.length > 0) items.push(` ${DIC_SPECIES} != '${dto['ndicSpecies']}'`);
    if (dto[dicCreateDate]) items.push(`${DIC_CREATE_DATE} is not null and date_format(${DIC_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[dicCreateDate])}`);
    if (dto['ndicCreateDate']) items.push(`${DIC_CREATE_DATE} is not null and date_format(${DIC_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ndicCreateDate'])}`);
    if (dto[dicUpdateDate]) items.push(`${DIC_UPDATE_DATE} is not null and date_format(${DIC_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[dicUpdateDate])}`);
    if (dto['ndicUpdateDate']) items.push(`${DIC_UPDATE_DATE} is not null and date_format(${DIC_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ndicUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //DIC_ID--字符串字段
    if (dto['dicIdList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_ID, <string[]>dto['dicIdList']));
    if (dto['ndicIdList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_ID, <string[]>dto['ndicIdList'], true));
    if (dto['dicIdLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_ID, dto['dicIdLike'])); //For MysSQL
    if (dto['ndicIdLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_ID, dto['ndicIdLike'], true)); //For MysSQL
    if (dto['dicIdLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_ID, <string[]>dto['dicIdLikeList'])); //For MysSQL
    //DIC_NAME--字符串字段
    if (dto['dicNameList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_NAME, <string[]>dto['dicNameList']));
    if (dto['ndicNameList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_NAME, <string[]>dto['ndicNameList'], true));
    if (dto['dicNameLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_NAME, dto['dicNameLike'])); //For MysSQL
    if (dto['ndicNameLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_NAME, dto['ndicNameLike'], true)); //For MysSQL
    if (dto['dicNameLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_NAME, <string[]>dto['dicNameLikeList'])); //For MysSQL
    //DIC_NAME_EN--字符串字段
    if (dto['dicNameEnList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_NAME_EN, <string[]>dto['dicNameEnList']));
    if (dto['ndicNameEnList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_NAME_EN, <string[]>dto['ndicNameEnList'], true));
    if (dto['dicNameEnLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_NAME_EN, dto['dicNameEnLike'])); //For MysSQL
    if (dto['ndicNameEnLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_NAME_EN, dto['ndicNameEnLike'], true)); //For MysSQL
    if (dto['dicNameEnLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_NAME_EN, <string[]>dto['dicNameEnLikeList'])); //For MysSQL
    //DIC_VALUE--字符串字段
    if (dto['dicValueList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_VALUE, <string[]>dto['dicValueList']));
    if (dto['ndicValueList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_VALUE, <string[]>dto['ndicValueList'], true));
    if (dto['dicValueLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_VALUE, dto['dicValueLike'])); //For MysSQL
    if (dto['ndicValueLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_VALUE, dto['ndicValueLike'], true)); //For MysSQL
    if (dto['dicValueLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_VALUE, <string[]>dto['dicValueLikeList'])); //For MysSQL
    //DIC_GROUP--字符串字段
    if (dto['dicGroupList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_GROUP, <string[]>dto['dicGroupList']));
    if (dto['ndicGroupList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_GROUP, <string[]>dto['ndicGroupList'], true));
    if (dto['dicGroupLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_GROUP, dto['dicGroupLike'])); //For MysSQL
    if (dto['ndicGroupLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_GROUP, dto['ndicGroupLike'], true)); //For MysSQL
    if (dto['dicGroupLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_GROUP, <string[]>dto['dicGroupLikeList'])); //For MysSQL
    //DIC_MODULE--字符串字段
    if (dto['dicModuleList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_MODULE, <string[]>dto['dicModuleList']));
    if (dto['ndicModuleList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_MODULE, <string[]>dto['ndicModuleList'], true));
    if (dto['dicModuleLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_MODULE, dto['dicModuleLike'])); //For MysSQL
    if (dto['ndicModuleLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_MODULE, dto['ndicModuleLike'], true)); //For MysSQL
    if (dto['dicModuleLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_MODULE, <string[]>dto['dicModuleLikeList'])); //For MysSQL
    //DIC_PARENT_ID--字符串字段
    if (dto['dicParentIdList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_PARENT_ID, <string[]>dto['dicParentIdList']));
    if (dto['ndicParentIdList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_PARENT_ID, <string[]>dto['ndicParentIdList'], true));
    if (dto['dicParentIdLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_PARENT_ID, dto['dicParentIdLike'])); //For MysSQL
    if (dto['ndicParentIdLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_PARENT_ID, dto['ndicParentIdLike'], true)); //For MysSQL
    if (dto['dicParentIdLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_PARENT_ID, <string[]>dto['dicParentIdLikeList'])); //For MysSQL
    //DIC_TYPE--字符串字段
    if (dto['dicTypeList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_TYPE, <string[]>dto['dicTypeList']));
    if (dto['ndicTypeList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_TYPE, <string[]>dto['ndicTypeList'], true));
    if (dto['dicTypeLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_TYPE, dto['dicTypeLike'])); //For MysSQL
    if (dto['ndicTypeLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_TYPE, dto['ndicTypeLike'], true)); //For MysSQL
    if (dto['dicTypeLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_TYPE, <string[]>dto['dicTypeLikeList'])); //For MysSQL
    //DIC_ORDER--数字字段
    if (dto['dicOrderList']) items.push(SysDictionarySQL.IN_SQL(DIC_ORDER, dto['dicOrderList']));
    if (dto['ndicOrderList']) items.push(SysDictionarySQL.IN_SQL(DIC_ORDER, dto['ndicOrderList'], true));
    //DIC_STATE--字符串字段
    if (dto['dicStateList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_STATE, <string[]>dto['dicStateList']));
    if (dto['ndicStateList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_STATE, <string[]>dto['ndicStateList'], true));
    if (dto['dicStateLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_STATE, dto['dicStateLike'])); //For MysSQL
    if (dto['ndicStateLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_STATE, dto['ndicStateLike'], true)); //For MysSQL
    if (dto['dicStateLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_STATE, <string[]>dto['dicStateLikeList'])); //For MysSQL
    //DIC_PARAMS--字符串字段
    if (dto['dicParamsList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_PARAMS, <string[]>dto['dicParamsList']));
    if (dto['ndicParamsList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_PARAMS, <string[]>dto['ndicParamsList'], true));
    if (dto['dicParamsLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_PARAMS, dto['dicParamsLike'])); //For MysSQL
    if (dto['ndicParamsLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_PARAMS, dto['ndicParamsLike'], true)); //For MysSQL
    if (dto['dicParamsLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_PARAMS, <string[]>dto['dicParamsLikeList'])); //For MysSQL
    //DIC_SPECIES--字符串字段
    if (dto['dicSpeciesList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_SPECIES, <string[]>dto['dicSpeciesList']));
    if (dto['ndicSpeciesList']?.length > 0) items.push(SysDictionarySQL.IN_SQL(DIC_SPECIES, <string[]>dto['ndicSpeciesList'], true));
    if (dto['dicSpeciesLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_SPECIES, dto['dicSpeciesLike'])); //For MysSQL
    if (dto['ndicSpeciesLike']?.length > 0) items.push(SysDictionarySQL.LIKE_SQL(DIC_SPECIES, dto['ndicSpeciesLike'], true)); //For MysSQL
    if (dto['dicSpeciesLikeList']?.length > 0) items.push(SysDictionarySQL.LIKE_LIST_SQL(DIC_SPECIES, <string[]>dto['dicSpeciesLikeList'])); //For MysSQL
    //DIC_CREATE_DATE--日期字段
    if (dto[dicCreateDate]) items.push(`${DIC_CREATE_DATE} is not null and date_format(${DIC_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sDicCreateDate'])}`);
    if (dto[dicCreateDate]) items.push(`${DIC_CREATE_DATE} is not null and date_format(${DIC_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eDicCreateDate'])}`);
    //DIC_UPDATE_DATE--日期字段
    if (dto[dicUpdateDate]) items.push(`${DIC_UPDATE_DATE} is not null and date_format(${DIC_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sDicUpdateDate'])}`);
    if (dto[dicUpdateDate]) items.push(`${DIC_UPDATE_DATE} is not null and date_format(${DIC_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eDicUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysDictionarySQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysDictionaryCDto | Partial<SysDictionaryCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(DIC_ID);
    cols.push(DIC_NAME);
    cols.push(DIC_NAME_EN);
    cols.push(DIC_VALUE);
    cols.push(DIC_GROUP);
    cols.push(DIC_MODULE);
    cols.push(DIC_PARENT_ID);
    cols.push(DIC_TYPE);
    cols.push(DIC_ORDER);
    cols.push(DIC_STATE);
    cols.push(DIC_PARAMS);
    cols.push(DIC_SPECIES);
    cols.push(DIC_CREATE_DATE);
    cols.push(DIC_UPDATE_DATE);
    values.push(toSqlValue(dto[dicId]));
    values.push(toSqlValue(dto[dicName]));
    values.push(toSqlValue(dto[dicNameEn]));
    values.push(toSqlValue(dto[dicValue]));
    values.push(toSqlValue(dto[dicGroup]));
    values.push(toSqlValue(dto[dicModule]));
    values.push(toSqlValue(dto[dicParentId]));
    values.push(toSqlValue(dto[dicType]));
    values.push(toSqlValue(dto[dicOrder]));
    values.push(toSqlValue(dto[dicState]));
    values.push(toSqlValue(dto[dicParams]));
    values.push(toSqlValue(dto[dicSpecies]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysDictionaryCDto[] | Partial<SysDictionaryCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(DIC_ID);
    cols.push(DIC_NAME);
    cols.push(DIC_NAME_EN);
    cols.push(DIC_VALUE);
    cols.push(DIC_GROUP);
    cols.push(DIC_MODULE);
    cols.push(DIC_PARENT_ID);
    cols.push(DIC_TYPE);
    cols.push(DIC_ORDER);
    cols.push(DIC_STATE);
    cols.push(DIC_PARAMS);
    cols.push(DIC_SPECIES);
    cols.push(DIC_CREATE_DATE);
    cols.push(DIC_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[dicId]));
      values.push(toSqlValue(dto[dicName]));
      values.push(toSqlValue(dto[dicNameEn]));
      values.push(toSqlValue(dto[dicValue]));
      values.push(toSqlValue(dto[dicGroup]));
      values.push(toSqlValue(dto[dicModule]));
      values.push(toSqlValue(dto[dicParentId]));
      values.push(toSqlValue(dto[dicType]));
      values.push(toSqlValue(dto[dicOrder]));
      values.push(toSqlValue(dto[dicState]));
      values.push(toSqlValue(dto[dicParams]));
      values.push(toSqlValue(dto[dicSpecies]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysDictionaryUDto | Partial<SysDictionaryUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysDictionaryColNames, SysDictionaryColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${DIC_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${DIC_NAME} = ${toSqlValue(dto[dicName])}`);
    items.push(`${DIC_NAME_EN} = ${toSqlValue(dto[dicNameEn])}`);
    items.push(`${DIC_VALUE} = ${toSqlValue(dto[dicValue])}`);
    items.push(`${DIC_GROUP} = ${toSqlValue(dto[dicGroup])}`);
    items.push(`${DIC_MODULE} = ${toSqlValue(dto[dicModule])}`);
    items.push(`${DIC_PARENT_ID} = ${toSqlValue(dto[dicParentId])}`);
    items.push(`${DIC_TYPE} = ${toSqlValue(dto[dicType])}`);
    items.push(`${DIC_ORDER} = ${toSqlValue(dto[dicOrder])}`);
    items.push(`${DIC_STATE} = ${toSqlValue(dto[dicState])}`);
    items.push(`${DIC_PARAMS} = ${toSqlValue(dto[dicParams])}`);
    items.push(`${DIC_SPECIES} = ${toSqlValue(dto[dicSpecies])}`);
    items.push(`${DIC_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysDictionaryUDto | Partial<SysDictionaryUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysDictionaryColNames, SysDictionaryColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${DIC_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysDictionarySQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${DIC_NAME} = ${toSqlValue(dto[dicName])}`);
    items.push(`${DIC_NAME_EN} = ${toSqlValue(dto[dicNameEn])}`);
    items.push(`${DIC_VALUE} = ${toSqlValue(dto[dicValue])}`);
    items.push(`${DIC_GROUP} = ${toSqlValue(dto[dicGroup])}`);
    items.push(`${DIC_MODULE} = ${toSqlValue(dto[dicModule])}`);
    items.push(`${DIC_PARENT_ID} = ${toSqlValue(dto[dicParentId])}`);
    items.push(`${DIC_TYPE} = ${toSqlValue(dto[dicType])}`);
    items.push(`${DIC_ORDER} = ${toSqlValue(dto[dicOrder])}`);
    items.push(`${DIC_STATE} = ${toSqlValue(dto[dicState])}`);
    items.push(`${DIC_PARAMS} = ${toSqlValue(dto[dicParams])}`);
    items.push(`${DIC_SPECIES} = ${toSqlValue(dto[dicSpecies])}`);
    items.push(`${DIC_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysDictionarySQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysDictionaryUDto[] | Partial<SysDictionaryUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysDictionarySQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysDictionaryUDto | Partial<SysDictionaryUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[dicName]?.length > 0) items.push(`${DIC_NAME} = ${toSqlValue(dto[dicName])}`);
    if (dto[dicNameEn]?.length > 0) items.push(`${DIC_NAME_EN} = ${toSqlValue(dto[dicNameEn])}`);
    if (dto[dicValue]?.length > 0) items.push(`${DIC_VALUE} = ${toSqlValue(dto[dicValue])}`);
    if (dto[dicGroup]?.length > 0) items.push(`${DIC_GROUP} = ${toSqlValue(dto[dicGroup])}`);
    if (dto[dicModule]?.length > 0) items.push(`${DIC_MODULE} = ${toSqlValue(dto[dicModule])}`);
    if (dto[dicParentId]?.length > 0) items.push(`${DIC_PARENT_ID} = ${toSqlValue(dto[dicParentId])}`);
    if (dto[dicType]?.length > 0) items.push(`${DIC_TYPE} = ${toSqlValue(dto[dicType])}`);
    if (dto[dicOrder]) items.push(`${DIC_ORDER} = ${toSqlValue(dto[dicOrder])}`);
    if (dto[dicState]?.length > 0) items.push(`${DIC_STATE} = ${toSqlValue(dto[dicState])}`);
    if (dto[dicParams]?.length > 0) items.push(`${DIC_PARAMS} = ${toSqlValue(dto[dicParams])}`);
    if (dto[dicSpecies]?.length > 0) items.push(`${DIC_SPECIES} = ${toSqlValue(dto[dicSpecies])}`);
    if (dto[dicUpdateDate]) items.push(`${DIC_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysDictionaryUDto[] | Partial<SysDictionaryUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysDictionarySQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): string => {
    if (!dto) return '';
    let where = SysDictionarySQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysDictionarySQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_DICTIONARY';
/**
 * 列名常量字段
 */
const DIC_ID = SysDictionaryColNameEnum.DIC_ID;
const DIC_NAME = SysDictionaryColNameEnum.DIC_NAME;
const DIC_NAME_EN = SysDictionaryColNameEnum.DIC_NAME_EN;
const DIC_VALUE = SysDictionaryColNameEnum.DIC_VALUE;
const DIC_GROUP = SysDictionaryColNameEnum.DIC_GROUP;
const DIC_MODULE = SysDictionaryColNameEnum.DIC_MODULE;
const DIC_PARENT_ID = SysDictionaryColNameEnum.DIC_PARENT_ID;
const DIC_TYPE = SysDictionaryColNameEnum.DIC_TYPE;
const DIC_ORDER = SysDictionaryColNameEnum.DIC_ORDER;
const DIC_STATE = SysDictionaryColNameEnum.DIC_STATE;
const DIC_PARAMS = SysDictionaryColNameEnum.DIC_PARAMS;
const DIC_SPECIES = SysDictionaryColNameEnum.DIC_SPECIES;
const DIC_CREATE_DATE = SysDictionaryColNameEnum.DIC_CREATE_DATE;
const DIC_UPDATE_DATE = SysDictionaryColNameEnum.DIC_UPDATE_DATE;
/**
 * 实体类属性名
 */
const dicId = SysDictionaryColPropEnum.dicId;
const dicName = SysDictionaryColPropEnum.dicName;
const dicNameEn = SysDictionaryColPropEnum.dicNameEn;
const dicValue = SysDictionaryColPropEnum.dicValue;
const dicGroup = SysDictionaryColPropEnum.dicGroup;
const dicModule = SysDictionaryColPropEnum.dicModule;
const dicParentId = SysDictionaryColPropEnum.dicParentId;
const dicType = SysDictionaryColPropEnum.dicType;
const dicOrder = SysDictionaryColPropEnum.dicOrder;
const dicState = SysDictionaryColPropEnum.dicState;
const dicParams = SysDictionaryColPropEnum.dicParams;
const dicSpecies = SysDictionaryColPropEnum.dicSpecies;
const dicCreateDate = SysDictionaryColPropEnum.dicCreateDate;
const dicUpdateDate = SysDictionaryColPropEnum.dicUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysDictionaryTable.PRIMER_KEY;
const primerKey = SysDictionaryTable.primerKey;
