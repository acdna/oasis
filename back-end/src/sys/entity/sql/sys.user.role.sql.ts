import { SysUserRoleQDto } from '../../dto/query/sys.user.role.qdto';
import { SysUserRoleCDto } from '../../dto/create/sys.user.role.cdto';
import { SysUserRoleUDto } from '../../dto/update/sys.user.role.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysUserRoleColNameEnum, SysUserRoleColPropEnum, SysUserRoleTable, SysUserRoleColNames, SysUserRoleColProps } from '../ddl/sys.user.role.cols';
/**
 * SYS_USER_ROLE--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysUserRoleSQL
 */
export const SysUserRoleSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let cols = SysUserRoleSQL.COL_MAPPER_SQL(dto);
    let where = SysUserRoleSQL.WHERE_SQL(dto);
    let group = SysUserRoleSQL.GROUP_BY_SQL(dto);
    let order = SysUserRoleSQL.ORDER_BY_SQL(dto);
    let limit = SysUserRoleSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let where = SysUserRoleSQL.WHERE_SQL(dto);
    let group = SysUserRoleSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let cols = SysUserRoleSQL.COL_MAPPER_SQL(dto);
    let group = SysUserRoleSQL.GROUP_BY_SQL(dto);
    let order = SysUserRoleSQL.ORDER_BY_SQL(dto);
    let limit = SysUserRoleSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysUserRoleSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysUserRoleSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysUserRoleSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let where = SysUserRoleSQL.WHERE_SQL(dto);
    let group = SysUserRoleSQL.GROUP_BY_SQL(dto);
    let order = SysUserRoleSQL.ORDER_BY_SQL(dto);
    let limit = SysUserRoleSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysUserRoleColNames, SysUserRoleColProps);
    if (!cols || cols.length == 0) cols = SysUserRoleColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[urId]?.length > 0) items.push(` ${UR_ID} = '${dto[urId]}'`);
    if (dto['nurId']?.length > 0) items.push(` ${UR_ID} != '${dto['nurId']}'`);
    if (dto[urRoleId]?.length > 0) items.push(` ${UR_ROLE_ID} = '${dto[urRoleId]}'`);
    if (dto['nurRoleId']?.length > 0) items.push(` ${UR_ROLE_ID} != '${dto['nurRoleId']}'`);
    if (dto[urUserLoginName]?.length > 0) items.push(` ${UR_USER_LOGIN_NAME} = '${dto[urUserLoginName]}'`);
    if (dto['nurUserLoginName']?.length > 0) items.push(` ${UR_USER_LOGIN_NAME} != '${dto['nurUserLoginName']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //UR_ID--字符串字段
    if (dto['urIdList']?.length > 0) items.push(SysUserRoleSQL.IN_SQL(UR_ID, <string[]>dto['urIdList']));
    if (dto['nurIdList']?.length > 0) items.push(SysUserRoleSQL.IN_SQL(UR_ID, <string[]>dto['nurIdList'], true));
    if (dto['urIdLike']?.length > 0) items.push(SysUserRoleSQL.LIKE_SQL(UR_ID, dto['urIdLike'])); //For MysSQL
    if (dto['nurIdLike']?.length > 0) items.push(SysUserRoleSQL.LIKE_SQL(UR_ID, dto['nurIdLike'], true)); //For MysSQL
    if (dto['urIdLikeList']?.length > 0) items.push(SysUserRoleSQL.LIKE_LIST_SQL(UR_ID, <string[]>dto['urIdLikeList'])); //For MysSQL
    //UR_ROLE_ID--字符串字段
    if (dto['urRoleIdList']?.length > 0) items.push(SysUserRoleSQL.IN_SQL(UR_ROLE_ID, <string[]>dto['urRoleIdList']));
    if (dto['nurRoleIdList']?.length > 0) items.push(SysUserRoleSQL.IN_SQL(UR_ROLE_ID, <string[]>dto['nurRoleIdList'], true));
    if (dto['urRoleIdLike']?.length > 0) items.push(SysUserRoleSQL.LIKE_SQL(UR_ROLE_ID, dto['urRoleIdLike'])); //For MysSQL
    if (dto['nurRoleIdLike']?.length > 0) items.push(SysUserRoleSQL.LIKE_SQL(UR_ROLE_ID, dto['nurRoleIdLike'], true)); //For MysSQL
    if (dto['urRoleIdLikeList']?.length > 0) items.push(SysUserRoleSQL.LIKE_LIST_SQL(UR_ROLE_ID, <string[]>dto['urRoleIdLikeList'])); //For MysSQL
    //UR_USER_LOGIN_NAME--字符串字段
    if (dto['urUserLoginNameList']?.length > 0) items.push(SysUserRoleSQL.IN_SQL(UR_USER_LOGIN_NAME, <string[]>dto['urUserLoginNameList']));
    if (dto['nurUserLoginNameList']?.length > 0) items.push(SysUserRoleSQL.IN_SQL(UR_USER_LOGIN_NAME, <string[]>dto['nurUserLoginNameList'], true));
    if (dto['urUserLoginNameLike']?.length > 0) items.push(SysUserRoleSQL.LIKE_SQL(UR_USER_LOGIN_NAME, dto['urUserLoginNameLike'])); //For MysSQL
    if (dto['nurUserLoginNameLike']?.length > 0) items.push(SysUserRoleSQL.LIKE_SQL(UR_USER_LOGIN_NAME, dto['nurUserLoginNameLike'], true)); //For MysSQL
    if (dto['urUserLoginNameLikeList']?.length > 0) items.push(SysUserRoleSQL.LIKE_LIST_SQL(UR_USER_LOGIN_NAME, <string[]>dto['urUserLoginNameLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysUserRoleSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysUserRoleCDto | Partial<SysUserRoleCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(UR_ID);
    cols.push(UR_ROLE_ID);
    cols.push(UR_USER_LOGIN_NAME);
    values.push(toSqlValue(dto[urId]));
    values.push(toSqlValue(dto[urRoleId]));
    values.push(toSqlValue(dto[urUserLoginName]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysUserRoleCDto[] | Partial<SysUserRoleCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(UR_ID);
    cols.push(UR_ROLE_ID);
    cols.push(UR_USER_LOGIN_NAME);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[urId]));
      values.push(toSqlValue(dto[urRoleId]));
      values.push(toSqlValue(dto[urUserLoginName]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysUserRoleUDto | Partial<SysUserRoleUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserRoleColNames, SysUserRoleColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UR_ROLE_ID} = ${toSqlValue(dto[urRoleId])}`);
    items.push(`${UR_USER_LOGIN_NAME} = ${toSqlValue(dto[urUserLoginName])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysUserRoleUDto | Partial<SysUserRoleUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserRoleColNames, SysUserRoleColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserRoleSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UR_ROLE_ID} = ${toSqlValue(dto[urRoleId])}`);
    items.push(`${UR_USER_LOGIN_NAME} = ${toSqlValue(dto[urUserLoginName])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserRoleSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysUserRoleUDto[] | Partial<SysUserRoleUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysUserRoleSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysUserRoleUDto | Partial<SysUserRoleUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[urRoleId]?.length > 0) items.push(`${UR_ROLE_ID} = ${toSqlValue(dto[urRoleId])}`);
    if (dto[urUserLoginName]?.length > 0) items.push(`${UR_USER_LOGIN_NAME} = ${toSqlValue(dto[urUserLoginName])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysUserRoleUDto[] | Partial<SysUserRoleUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysUserRoleSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysUserRoleQDto | Partial<SysUserRoleQDto>): string => {
    if (!dto) return '';
    let where = SysUserRoleSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysUserRoleSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_USER_ROLE';
/**
 * 列名常量字段
 */
const UR_ID = SysUserRoleColNameEnum.UR_ID;
const UR_ROLE_ID = SysUserRoleColNameEnum.UR_ROLE_ID;
const UR_USER_LOGIN_NAME = SysUserRoleColNameEnum.UR_USER_LOGIN_NAME;
/**
 * 实体类属性名
 */
const urId = SysUserRoleColPropEnum.urId;
const urRoleId = SysUserRoleColPropEnum.urRoleId;
const urUserLoginName = SysUserRoleColPropEnum.urUserLoginName;
/**
 * 主键信息
 */
const PRIMER_KEY = SysUserRoleTable.PRIMER_KEY;
const primerKey = SysUserRoleTable.primerKey;
