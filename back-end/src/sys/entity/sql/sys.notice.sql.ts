import { SysNoticeQDto } from '../../dto/query/sys.notice.qdto';
import { SysNoticeCDto } from '../../dto/create/sys.notice.cdto';
import { SysNoticeUDto } from '../../dto/update/sys.notice.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysNoticeColNameEnum, SysNoticeColPropEnum, SysNoticeTable, SysNoticeColNames, SysNoticeColProps } from '../ddl/sys.notice.cols';
/**
 * SYS_NOTICE--用户提示消息表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysNoticeSQL
 */
export const SysNoticeSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let cols = SysNoticeSQL.COL_MAPPER_SQL(dto);
    let where = SysNoticeSQL.WHERE_SQL(dto);
    let group = SysNoticeSQL.GROUP_BY_SQL(dto);
    let order = SysNoticeSQL.ORDER_BY_SQL(dto);
    let limit = SysNoticeSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let where = SysNoticeSQL.WHERE_SQL(dto);
    let group = SysNoticeSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let cols = SysNoticeSQL.COL_MAPPER_SQL(dto);
    let group = SysNoticeSQL.GROUP_BY_SQL(dto);
    let order = SysNoticeSQL.ORDER_BY_SQL(dto);
    let limit = SysNoticeSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysNoticeSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysNoticeSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysNoticeSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let where = SysNoticeSQL.WHERE_SQL(dto);
    let group = SysNoticeSQL.GROUP_BY_SQL(dto);
    let order = SysNoticeSQL.ORDER_BY_SQL(dto);
    let limit = SysNoticeSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysNoticeColNames, SysNoticeColProps);
    if (!cols || cols.length == 0) cols = SysNoticeColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[noticeId]?.length > 0) items.push(` ${NOTICE_ID} = '${dto[noticeId]}'`);
    if (dto['nnoticeId']?.length > 0) items.push(` ${NOTICE_ID} != '${dto['nnoticeId']}'`);
    if (dto[noticeTitle]?.length > 0) items.push(` ${NOTICE_TITLE} = '${dto[noticeTitle]}'`);
    if (dto['nnoticeTitle']?.length > 0) items.push(` ${NOTICE_TITLE} != '${dto['nnoticeTitle']}'`);
    if (dto[noticeContent]?.length > 0) items.push(` ${NOTICE_CONTENT} = '${dto[noticeContent]}'`);
    if (dto['nnoticeContent']?.length > 0) items.push(` ${NOTICE_CONTENT} != '${dto['nnoticeContent']}'`);
    if (dto[noticeManager]?.length > 0) items.push(` ${NOTICE_MANAGER} = '${dto[noticeManager]}'`);
    if (dto['nnoticeManager']?.length > 0) items.push(` ${NOTICE_MANAGER} != '${dto['nnoticeManager']}'`);
    if (dto[noticeIsNew]?.length > 0) items.push(` ${NOTICE_IS_NEW} = '${dto[noticeIsNew]}'`);
    if (dto['nnoticeIsNew']?.length > 0) items.push(` ${NOTICE_IS_NEW} != '${dto['nnoticeIsNew']}'`);
    if (dto[noticeCreateDate]) items.push(`${NOTICE_CREATE_DATE} is not null and date_format(${NOTICE_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[noticeCreateDate])}`);
    if (dto['nnoticeCreateDate']) items.push(`${NOTICE_CREATE_DATE} is not null and date_format(${NOTICE_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nnoticeCreateDate'])}`);
    if (dto[noticeUpdateDate]) items.push(`${NOTICE_UPDATE_DATE} is not null and date_format(${NOTICE_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[noticeUpdateDate])}`);
    if (dto['nnoticeUpdateDate']) items.push(`${NOTICE_UPDATE_DATE} is not null and date_format(${NOTICE_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nnoticeUpdateDate'])}`);
    if (dto[noticeReceiver]?.length > 0) items.push(` ${NOTICE_RECEIVER} = '${dto[noticeReceiver]}'`);
    if (dto['nnoticeReceiver']?.length > 0) items.push(` ${NOTICE_RECEIVER} != '${dto['nnoticeReceiver']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //NOTICE_ID--字符串字段
    if (dto['noticeIdList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_ID, <string[]>dto['noticeIdList']));
    if (dto['nnoticeIdList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_ID, <string[]>dto['nnoticeIdList'], true));
    if (dto['noticeIdLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_ID, dto['noticeIdLike'])); //For MysSQL
    if (dto['nnoticeIdLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_ID, dto['nnoticeIdLike'], true)); //For MysSQL
    if (dto['noticeIdLikeList']?.length > 0) items.push(SysNoticeSQL.LIKE_LIST_SQL(NOTICE_ID, <string[]>dto['noticeIdLikeList'])); //For MysSQL
    //NOTICE_TITLE--字符串字段
    if (dto['noticeTitleList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_TITLE, <string[]>dto['noticeTitleList']));
    if (dto['nnoticeTitleList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_TITLE, <string[]>dto['nnoticeTitleList'], true));
    if (dto['noticeTitleLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_TITLE, dto['noticeTitleLike'])); //For MysSQL
    if (dto['nnoticeTitleLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_TITLE, dto['nnoticeTitleLike'], true)); //For MysSQL
    if (dto['noticeTitleLikeList']?.length > 0) items.push(SysNoticeSQL.LIKE_LIST_SQL(NOTICE_TITLE, <string[]>dto['noticeTitleLikeList'])); //For MysSQL
    //NOTICE_CONTENT--字符串字段
    if (dto['noticeContentList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_CONTENT, <string[]>dto['noticeContentList']));
    if (dto['nnoticeContentList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_CONTENT, <string[]>dto['nnoticeContentList'], true));
    if (dto['noticeContentLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_CONTENT, dto['noticeContentLike'])); //For MysSQL
    if (dto['nnoticeContentLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_CONTENT, dto['nnoticeContentLike'], true)); //For MysSQL
    if (dto['noticeContentLikeList']?.length > 0) items.push(SysNoticeSQL.LIKE_LIST_SQL(NOTICE_CONTENT, <string[]>dto['noticeContentLikeList'])); //For MysSQL
    //NOTICE_MANAGER--字符串字段
    if (dto['noticeManagerList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_MANAGER, <string[]>dto['noticeManagerList']));
    if (dto['nnoticeManagerList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_MANAGER, <string[]>dto['nnoticeManagerList'], true));
    if (dto['noticeManagerLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_MANAGER, dto['noticeManagerLike'])); //For MysSQL
    if (dto['nnoticeManagerLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_MANAGER, dto['nnoticeManagerLike'], true)); //For MysSQL
    if (dto['noticeManagerLikeList']?.length > 0) items.push(SysNoticeSQL.LIKE_LIST_SQL(NOTICE_MANAGER, <string[]>dto['noticeManagerLikeList'])); //For MysSQL
    //NOTICE_IS_NEW--字符串字段
    if (dto['noticeIsNewList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_IS_NEW, <string[]>dto['noticeIsNewList']));
    if (dto['nnoticeIsNewList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_IS_NEW, <string[]>dto['nnoticeIsNewList'], true));
    if (dto['noticeIsNewLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_IS_NEW, dto['noticeIsNewLike'])); //For MysSQL
    if (dto['nnoticeIsNewLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_IS_NEW, dto['nnoticeIsNewLike'], true)); //For MysSQL
    if (dto['noticeIsNewLikeList']?.length > 0) items.push(SysNoticeSQL.LIKE_LIST_SQL(NOTICE_IS_NEW, <string[]>dto['noticeIsNewLikeList'])); //For MysSQL
    //NOTICE_CREATE_DATE--日期字段
    if (dto[noticeCreateDate]) items.push(`${NOTICE_CREATE_DATE} is not null and date_format(${NOTICE_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sNoticeCreateDate'])}`);
    if (dto[noticeCreateDate]) items.push(`${NOTICE_CREATE_DATE} is not null and date_format(${NOTICE_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eNoticeCreateDate'])}`);
    //NOTICE_UPDATE_DATE--日期字段
    if (dto[noticeUpdateDate]) items.push(`${NOTICE_UPDATE_DATE} is not null and date_format(${NOTICE_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sNoticeUpdateDate'])}`);
    if (dto[noticeUpdateDate]) items.push(`${NOTICE_UPDATE_DATE} is not null and date_format(${NOTICE_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eNoticeUpdateDate'])}`);
    //NOTICE_RECEIVER--字符串字段
    if (dto['noticeReceiverList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_RECEIVER, <string[]>dto['noticeReceiverList']));
    if (dto['nnoticeReceiverList']?.length > 0) items.push(SysNoticeSQL.IN_SQL(NOTICE_RECEIVER, <string[]>dto['nnoticeReceiverList'], true));
    if (dto['noticeReceiverLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_RECEIVER, dto['noticeReceiverLike'])); //For MysSQL
    if (dto['nnoticeReceiverLike']?.length > 0) items.push(SysNoticeSQL.LIKE_SQL(NOTICE_RECEIVER, dto['nnoticeReceiverLike'], true)); //For MysSQL
    if (dto['noticeReceiverLikeList']?.length > 0) items.push(SysNoticeSQL.LIKE_LIST_SQL(NOTICE_RECEIVER, <string[]>dto['noticeReceiverLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysNoticeSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysNoticeCDto | Partial<SysNoticeCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(NOTICE_ID);
    cols.push(NOTICE_TITLE);
    cols.push(NOTICE_CONTENT);
    cols.push(NOTICE_MANAGER);
    cols.push(NOTICE_IS_NEW);
    cols.push(NOTICE_CREATE_DATE);
    cols.push(NOTICE_UPDATE_DATE);
    cols.push(NOTICE_RECEIVER);
    values.push(toSqlValue(dto[noticeId]));
    values.push(toSqlValue(dto[noticeTitle]));
    values.push(toSqlValue(dto[noticeContent]));
    values.push(toSqlValue(dto[noticeManager]));
    values.push(toSqlValue(dto[noticeIsNew]));
    values.push('NOW()');
    values.push('NOW()');
    values.push(toSqlValue(dto[noticeReceiver]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysNoticeCDto[] | Partial<SysNoticeCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(NOTICE_ID);
    cols.push(NOTICE_TITLE);
    cols.push(NOTICE_CONTENT);
    cols.push(NOTICE_MANAGER);
    cols.push(NOTICE_IS_NEW);
    cols.push(NOTICE_CREATE_DATE);
    cols.push(NOTICE_UPDATE_DATE);
    cols.push(NOTICE_RECEIVER);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[noticeId]));
      values.push(toSqlValue(dto[noticeTitle]));
      values.push(toSqlValue(dto[noticeContent]));
      values.push(toSqlValue(dto[noticeManager]));
      values.push(toSqlValue(dto[noticeIsNew]));
      values.push('NOW()');
      values.push('NOW()');
      values.push(toSqlValue(dto[noticeReceiver]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysNoticeUDto | Partial<SysNoticeUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysNoticeColNames, SysNoticeColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${NOTICE_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${NOTICE_TITLE} = ${toSqlValue(dto[noticeTitle])}`);
    items.push(`${NOTICE_CONTENT} = ${toSqlValue(dto[noticeContent])}`);
    items.push(`${NOTICE_MANAGER} = ${toSqlValue(dto[noticeManager])}`);
    items.push(`${NOTICE_IS_NEW} = ${toSqlValue(dto[noticeIsNew])}`);
    items.push(`${NOTICE_UPDATE_DATE} = NOW()`);
    items.push(`${NOTICE_RECEIVER} = ${toSqlValue(dto[noticeReceiver])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysNoticeUDto | Partial<SysNoticeUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysNoticeColNames, SysNoticeColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${NOTICE_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysNoticeSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${NOTICE_TITLE} = ${toSqlValue(dto[noticeTitle])}`);
    items.push(`${NOTICE_CONTENT} = ${toSqlValue(dto[noticeContent])}`);
    items.push(`${NOTICE_MANAGER} = ${toSqlValue(dto[noticeManager])}`);
    items.push(`${NOTICE_IS_NEW} = ${toSqlValue(dto[noticeIsNew])}`);
    items.push(`${NOTICE_UPDATE_DATE} = NOW()`);
    items.push(`${NOTICE_RECEIVER} = ${toSqlValue(dto[noticeReceiver])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysNoticeSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysNoticeUDto[] | Partial<SysNoticeUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysNoticeSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysNoticeUDto | Partial<SysNoticeUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[noticeTitle]?.length > 0) items.push(`${NOTICE_TITLE} = ${toSqlValue(dto[noticeTitle])}`);
    if (dto[noticeContent]?.length > 0) items.push(`${NOTICE_CONTENT} = ${toSqlValue(dto[noticeContent])}`);
    if (dto[noticeManager]?.length > 0) items.push(`${NOTICE_MANAGER} = ${toSqlValue(dto[noticeManager])}`);
    if (dto[noticeIsNew]?.length > 0) items.push(`${NOTICE_IS_NEW} = ${toSqlValue(dto[noticeIsNew])}`);
    if (dto[noticeUpdateDate]) items.push(`${NOTICE_UPDATE_DATE}= NOW()`);
    if (dto[noticeReceiver]?.length > 0) items.push(`${NOTICE_RECEIVER} = ${toSqlValue(dto[noticeReceiver])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysNoticeUDto[] | Partial<SysNoticeUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysNoticeSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysNoticeQDto | Partial<SysNoticeQDto>): string => {
    if (!dto) return '';
    let where = SysNoticeSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysNoticeSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_NOTICE';
/**
 * 列名常量字段
 */
const NOTICE_ID = SysNoticeColNameEnum.NOTICE_ID;
const NOTICE_TITLE = SysNoticeColNameEnum.NOTICE_TITLE;
const NOTICE_CONTENT = SysNoticeColNameEnum.NOTICE_CONTENT;
const NOTICE_MANAGER = SysNoticeColNameEnum.NOTICE_MANAGER;
const NOTICE_IS_NEW = SysNoticeColNameEnum.NOTICE_IS_NEW;
const NOTICE_CREATE_DATE = SysNoticeColNameEnum.NOTICE_CREATE_DATE;
const NOTICE_UPDATE_DATE = SysNoticeColNameEnum.NOTICE_UPDATE_DATE;
const NOTICE_RECEIVER = SysNoticeColNameEnum.NOTICE_RECEIVER;
/**
 * 实体类属性名
 */
const noticeId = SysNoticeColPropEnum.noticeId;
const noticeTitle = SysNoticeColPropEnum.noticeTitle;
const noticeContent = SysNoticeColPropEnum.noticeContent;
const noticeManager = SysNoticeColPropEnum.noticeManager;
const noticeIsNew = SysNoticeColPropEnum.noticeIsNew;
const noticeCreateDate = SysNoticeColPropEnum.noticeCreateDate;
const noticeUpdateDate = SysNoticeColPropEnum.noticeUpdateDate;
const noticeReceiver = SysNoticeColPropEnum.noticeReceiver;
/**
 * 主键信息
 */
const PRIMER_KEY = SysNoticeTable.PRIMER_KEY;
const primerKey = SysNoticeTable.primerKey;
