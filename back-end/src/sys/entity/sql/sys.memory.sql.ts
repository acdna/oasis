import { SysMemoryQDto } from '../../dto/query/sys.memory.qdto';
import { SysMemoryCDto } from '../../dto/create/sys.memory.cdto';
import { SysMemoryUDto } from '../../dto/update/sys.memory.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysMemoryColNameEnum, SysMemoryColPropEnum, SysMemoryTable, SysMemoryColNames, SysMemoryColProps } from '../ddl/sys.memory.cols';
/**
 * SYS_MEMORY--系统JVM内存表表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysMemorySQL
 */
export const SysMemorySQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let cols = SysMemorySQL.COL_MAPPER_SQL(dto);
    let where = SysMemorySQL.WHERE_SQL(dto);
    let group = SysMemorySQL.GROUP_BY_SQL(dto);
    let order = SysMemorySQL.ORDER_BY_SQL(dto);
    let limit = SysMemorySQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let where = SysMemorySQL.WHERE_SQL(dto);
    let group = SysMemorySQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let cols = SysMemorySQL.COL_MAPPER_SQL(dto);
    let group = SysMemorySQL.GROUP_BY_SQL(dto);
    let order = SysMemorySQL.ORDER_BY_SQL(dto);
    let limit = SysMemorySQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysMemorySQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysMemorySQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysMemorySQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let where = SysMemorySQL.WHERE_SQL(dto);
    let group = SysMemorySQL.GROUP_BY_SQL(dto);
    let order = SysMemorySQL.ORDER_BY_SQL(dto);
    let limit = SysMemorySQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysMemoryColNames, SysMemoryColProps);
    if (!cols || cols.length == 0) cols = SysMemoryColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[memId]?.length > 0) items.push(` ${MEM_ID} = '${dto[memId]}'`);
    if (dto['nmemId']?.length > 0) items.push(` ${MEM_ID} != '${dto['nmemId']}'`);
    if (dto[memTotal]) items.push(` ${MEM_TOTAL} = '${dto[memTotal]}'`);
    if (dto['nmemTotal']) items.push(` ${MEM_TOTAL} != '${dto['nmemTotal']}'`);
    if (dto[memFree]) items.push(` ${MEM_FREE} = '${dto[memFree]}'`);
    if (dto['nmemFree']) items.push(` ${MEM_FREE} != '${dto['nmemFree']}'`);
    if (dto[memUsed]) items.push(` ${MEM_USED} = '${dto[memUsed]}'`);
    if (dto['nmemUsed']) items.push(` ${MEM_USED} != '${dto['nmemUsed']}'`);
    if (dto[memIsWarning]?.length > 0) items.push(` ${MEM_IS_WARNING} = '${dto[memIsWarning]}'`);
    if (dto['nmemIsWarning']?.length > 0) items.push(` ${MEM_IS_WARNING} != '${dto['nmemIsWarning']}'`);
    if (dto[memMax]) items.push(` ${MEM_MAX} = '${dto[memMax]}'`);
    if (dto['nmemMax']) items.push(` ${MEM_MAX} != '${dto['nmemMax']}'`);
    if (dto[memCreateDate]) items.push(`${MEM_CREATE_DATE} is not null and date_format(${MEM_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[memCreateDate])}`);
    if (dto['nmemCreateDate']) items.push(`${MEM_CREATE_DATE} is not null and date_format(${MEM_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nmemCreateDate'])}`);
    if (dto[memUpdateDate]) items.push(`${MEM_UPDATE_DATE} is not null and date_format(${MEM_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[memUpdateDate])}`);
    if (dto['nmemUpdateDate']) items.push(`${MEM_UPDATE_DATE} is not null and date_format(${MEM_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nmemUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //MEM_ID--字符串字段
    if (dto['memIdList']?.length > 0) items.push(SysMemorySQL.IN_SQL(MEM_ID, <string[]>dto['memIdList']));
    if (dto['nmemIdList']?.length > 0) items.push(SysMemorySQL.IN_SQL(MEM_ID, <string[]>dto['nmemIdList'], true));
    if (dto['memIdLike']?.length > 0) items.push(SysMemorySQL.LIKE_SQL(MEM_ID, dto['memIdLike'])); //For MysSQL
    if (dto['nmemIdLike']?.length > 0) items.push(SysMemorySQL.LIKE_SQL(MEM_ID, dto['nmemIdLike'], true)); //For MysSQL
    if (dto['memIdLikeList']?.length > 0) items.push(SysMemorySQL.LIKE_LIST_SQL(MEM_ID, <string[]>dto['memIdLikeList'])); //For MysSQL
    //MEM_TOTAL--数字字段
    if (dto['memTotalList']) items.push(SysMemorySQL.IN_SQL(MEM_TOTAL, dto['memTotalList']));
    if (dto['nmemTotalList']) items.push(SysMemorySQL.IN_SQL(MEM_TOTAL, dto['nmemTotalList'], true));
    //MEM_FREE--数字字段
    if (dto['memFreeList']) items.push(SysMemorySQL.IN_SQL(MEM_FREE, dto['memFreeList']));
    if (dto['nmemFreeList']) items.push(SysMemorySQL.IN_SQL(MEM_FREE, dto['nmemFreeList'], true));
    //MEM_USED--数字字段
    if (dto['memUsedList']) items.push(SysMemorySQL.IN_SQL(MEM_USED, dto['memUsedList']));
    if (dto['nmemUsedList']) items.push(SysMemorySQL.IN_SQL(MEM_USED, dto['nmemUsedList'], true));
    //MEM_IS_WARNING--字符串字段
    if (dto['memIsWarningList']?.length > 0) items.push(SysMemorySQL.IN_SQL(MEM_IS_WARNING, <string[]>dto['memIsWarningList']));
    if (dto['nmemIsWarningList']?.length > 0) items.push(SysMemorySQL.IN_SQL(MEM_IS_WARNING, <string[]>dto['nmemIsWarningList'], true));
    if (dto['memIsWarningLike']?.length > 0) items.push(SysMemorySQL.LIKE_SQL(MEM_IS_WARNING, dto['memIsWarningLike'])); //For MysSQL
    if (dto['nmemIsWarningLike']?.length > 0) items.push(SysMemorySQL.LIKE_SQL(MEM_IS_WARNING, dto['nmemIsWarningLike'], true)); //For MysSQL
    if (dto['memIsWarningLikeList']?.length > 0) items.push(SysMemorySQL.LIKE_LIST_SQL(MEM_IS_WARNING, <string[]>dto['memIsWarningLikeList'])); //For MysSQL
    //MEM_MAX--数字字段
    if (dto['memMaxList']) items.push(SysMemorySQL.IN_SQL(MEM_MAX, dto['memMaxList']));
    if (dto['nmemMaxList']) items.push(SysMemorySQL.IN_SQL(MEM_MAX, dto['nmemMaxList'], true));
    //MEM_CREATE_DATE--日期字段
    if (dto[memCreateDate]) items.push(`${MEM_CREATE_DATE} is not null and date_format(${MEM_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sMemCreateDate'])}`);
    if (dto[memCreateDate]) items.push(`${MEM_CREATE_DATE} is not null and date_format(${MEM_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eMemCreateDate'])}`);
    //MEM_UPDATE_DATE--日期字段
    if (dto[memUpdateDate]) items.push(`${MEM_UPDATE_DATE} is not null and date_format(${MEM_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sMemUpdateDate'])}`);
    if (dto[memUpdateDate]) items.push(`${MEM_UPDATE_DATE} is not null and date_format(${MEM_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eMemUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysMemorySQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysMemoryCDto | Partial<SysMemoryCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(MEM_ID);
    cols.push(MEM_TOTAL);
    cols.push(MEM_FREE);
    cols.push(MEM_USED);
    cols.push(MEM_IS_WARNING);
    cols.push(MEM_MAX);
    cols.push(MEM_CREATE_DATE);
    cols.push(MEM_UPDATE_DATE);
    values.push(toSqlValue(dto[memId]));
    values.push(toSqlValue(dto[memTotal]));
    values.push(toSqlValue(dto[memFree]));
    values.push(toSqlValue(dto[memUsed]));
    values.push(toSqlValue(dto[memIsWarning]));
    values.push(toSqlValue(dto[memMax]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysMemoryCDto[] | Partial<SysMemoryCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(MEM_ID);
    cols.push(MEM_TOTAL);
    cols.push(MEM_FREE);
    cols.push(MEM_USED);
    cols.push(MEM_IS_WARNING);
    cols.push(MEM_MAX);
    cols.push(MEM_CREATE_DATE);
    cols.push(MEM_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[memId]));
      values.push(toSqlValue(dto[memTotal]));
      values.push(toSqlValue(dto[memFree]));
      values.push(toSqlValue(dto[memUsed]));
      values.push(toSqlValue(dto[memIsWarning]));
      values.push(toSqlValue(dto[memMax]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysMemoryUDto | Partial<SysMemoryUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysMemoryColNames, SysMemoryColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${MEM_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${MEM_TOTAL} = ${toSqlValue(dto[memTotal])}`);
    items.push(`${MEM_FREE} = ${toSqlValue(dto[memFree])}`);
    items.push(`${MEM_USED} = ${toSqlValue(dto[memUsed])}`);
    items.push(`${MEM_IS_WARNING} = ${toSqlValue(dto[memIsWarning])}`);
    items.push(`${MEM_MAX} = ${toSqlValue(dto[memMax])}`);
    items.push(`${MEM_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysMemoryUDto | Partial<SysMemoryUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysMemoryColNames, SysMemoryColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${MEM_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysMemorySQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${MEM_TOTAL} = ${toSqlValue(dto[memTotal])}`);
    items.push(`${MEM_FREE} = ${toSqlValue(dto[memFree])}`);
    items.push(`${MEM_USED} = ${toSqlValue(dto[memUsed])}`);
    items.push(`${MEM_IS_WARNING} = ${toSqlValue(dto[memIsWarning])}`);
    items.push(`${MEM_MAX} = ${toSqlValue(dto[memMax])}`);
    items.push(`${MEM_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysMemorySQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysMemoryUDto[] | Partial<SysMemoryUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysMemorySQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysMemoryUDto | Partial<SysMemoryUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[memTotal]) items.push(`${MEM_TOTAL} = ${toSqlValue(dto[memTotal])}`);
    if (dto[memFree]) items.push(`${MEM_FREE} = ${toSqlValue(dto[memFree])}`);
    if (dto[memUsed]) items.push(`${MEM_USED} = ${toSqlValue(dto[memUsed])}`);
    if (dto[memIsWarning]?.length > 0) items.push(`${MEM_IS_WARNING} = ${toSqlValue(dto[memIsWarning])}`);
    if (dto[memMax]) items.push(`${MEM_MAX} = ${toSqlValue(dto[memMax])}`);
    if (dto[memUpdateDate]) items.push(`${MEM_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysMemoryUDto[] | Partial<SysMemoryUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysMemorySQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysMemoryQDto | Partial<SysMemoryQDto>): string => {
    if (!dto) return '';
    let where = SysMemorySQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysMemorySQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_MEMORY';
/**
 * 列名常量字段
 */
const MEM_ID = SysMemoryColNameEnum.MEM_ID;
const MEM_TOTAL = SysMemoryColNameEnum.MEM_TOTAL;
const MEM_FREE = SysMemoryColNameEnum.MEM_FREE;
const MEM_USED = SysMemoryColNameEnum.MEM_USED;
const MEM_IS_WARNING = SysMemoryColNameEnum.MEM_IS_WARNING;
const MEM_MAX = SysMemoryColNameEnum.MEM_MAX;
const MEM_CREATE_DATE = SysMemoryColNameEnum.MEM_CREATE_DATE;
const MEM_UPDATE_DATE = SysMemoryColNameEnum.MEM_UPDATE_DATE;
/**
 * 实体类属性名
 */
const memId = SysMemoryColPropEnum.memId;
const memTotal = SysMemoryColPropEnum.memTotal;
const memFree = SysMemoryColPropEnum.memFree;
const memUsed = SysMemoryColPropEnum.memUsed;
const memIsWarning = SysMemoryColPropEnum.memIsWarning;
const memMax = SysMemoryColPropEnum.memMax;
const memCreateDate = SysMemoryColPropEnum.memCreateDate;
const memUpdateDate = SysMemoryColPropEnum.memUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysMemoryTable.PRIMER_KEY;
const primerKey = SysMemoryTable.primerKey;
