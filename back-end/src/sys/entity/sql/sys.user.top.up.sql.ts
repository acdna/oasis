import { SysUserTopUpQDto } from '../../dto/query/sys.user.top.up.qdto';
import { SysUserTopUpCDto } from '../../dto/create/sys.user.top.up.cdto';
import { SysUserTopUpUDto } from '../../dto/update/sys.user.top.up.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysUserTopUpColNameEnum, SysUserTopUpColPropEnum, SysUserTopUpTable, SysUserTopUpColNames, SysUserTopUpColProps } from '../ddl/sys.user.top.up.cols';
/**
 * SYS_USER_TOP_UP--用户充值信息表表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysUserTopUpSQL
 */
export const SysUserTopUpSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let cols = SysUserTopUpSQL.COL_MAPPER_SQL(dto);
    let where = SysUserTopUpSQL.WHERE_SQL(dto);
    let group = SysUserTopUpSQL.GROUP_BY_SQL(dto);
    let order = SysUserTopUpSQL.ORDER_BY_SQL(dto);
    let limit = SysUserTopUpSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let where = SysUserTopUpSQL.WHERE_SQL(dto);
    let group = SysUserTopUpSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let cols = SysUserTopUpSQL.COL_MAPPER_SQL(dto);
    let group = SysUserTopUpSQL.GROUP_BY_SQL(dto);
    let order = SysUserTopUpSQL.ORDER_BY_SQL(dto);
    let limit = SysUserTopUpSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysUserTopUpSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysUserTopUpSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysUserTopUpSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let where = SysUserTopUpSQL.WHERE_SQL(dto);
    let group = SysUserTopUpSQL.GROUP_BY_SQL(dto);
    let order = SysUserTopUpSQL.ORDER_BY_SQL(dto);
    let limit = SysUserTopUpSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysUserTopUpColNames, SysUserTopUpColProps);
    if (!cols || cols.length == 0) cols = SysUserTopUpColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[utuId]?.length > 0) items.push(` ${UTU_ID} = '${dto[utuId]}'`);
    if (dto['nutuId']?.length > 0) items.push(` ${UTU_ID} != '${dto['nutuId']}'`);
    if (dto[utuLoginName]?.length > 0) items.push(` ${UTU_LOGIN_NAME} = '${dto[utuLoginName]}'`);
    if (dto['nutuLoginName']?.length > 0) items.push(` ${UTU_LOGIN_NAME} != '${dto['nutuLoginName']}'`);
    if (dto[utuSerialNumber]?.length > 0) items.push(` ${UTU_SERIAL_NUMBER} = '${dto[utuSerialNumber]}'`);
    if (dto['nutuSerialNumber']?.length > 0) items.push(` ${UTU_SERIAL_NUMBER} != '${dto['nutuSerialNumber']}'`);
    if (dto[utuBillingStartDate]) items.push(`${UTU_BILLING_START_DATE} is not null and date_format(${UTU_BILLING_START_DATE},'%Y-%m-%d') = ${toSqlValue(dto[utuBillingStartDate])}`);
    if (dto['nutuBillingStartDate']) items.push(`${UTU_BILLING_START_DATE} is not null and date_format(${UTU_BILLING_START_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nutuBillingStartDate'])}`);
    if (dto[utuBillingEndDate]) items.push(`${UTU_BILLING_END_DATE} is not null and date_format(${UTU_BILLING_END_DATE},'%Y-%m-%d') = ${toSqlValue(dto[utuBillingEndDate])}`);
    if (dto['nutuBillingEndDate']) items.push(`${UTU_BILLING_END_DATE} is not null and date_format(${UTU_BILLING_END_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nutuBillingEndDate'])}`);
    if (dto[utuState]?.length > 0) items.push(` ${UTU_STATE} = '${dto[utuState]}'`);
    if (dto['nutuState']?.length > 0) items.push(` ${UTU_STATE} != '${dto['nutuState']}'`);
    if (dto[utuCreateDate]) items.push(`${UTU_CREATE_DATE} is not null and date_format(${UTU_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[utuCreateDate])}`);
    if (dto['nutuCreateDate']) items.push(`${UTU_CREATE_DATE} is not null and date_format(${UTU_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nutuCreateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //UTU_ID--字符串字段
    if (dto['utuIdList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_ID, <string[]>dto['utuIdList']));
    if (dto['nutuIdList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_ID, <string[]>dto['nutuIdList'], true));
    if (dto['utuIdLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_ID, dto['utuIdLike'])); //For MysSQL
    if (dto['nutuIdLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_ID, dto['nutuIdLike'], true)); //For MysSQL
    if (dto['utuIdLikeList']?.length > 0) items.push(SysUserTopUpSQL.LIKE_LIST_SQL(UTU_ID, <string[]>dto['utuIdLikeList'])); //For MysSQL
    //UTU_LOGIN_NAME--字符串字段
    if (dto['utuLoginNameList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_LOGIN_NAME, <string[]>dto['utuLoginNameList']));
    if (dto['nutuLoginNameList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_LOGIN_NAME, <string[]>dto['nutuLoginNameList'], true));
    if (dto['utuLoginNameLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_LOGIN_NAME, dto['utuLoginNameLike'])); //For MysSQL
    if (dto['nutuLoginNameLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_LOGIN_NAME, dto['nutuLoginNameLike'], true)); //For MysSQL
    if (dto['utuLoginNameLikeList']?.length > 0) items.push(SysUserTopUpSQL.LIKE_LIST_SQL(UTU_LOGIN_NAME, <string[]>dto['utuLoginNameLikeList'])); //For MysSQL
    //UTU_SERIAL_NUMBER--字符串字段
    if (dto['utuSerialNumberList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_SERIAL_NUMBER, <string[]>dto['utuSerialNumberList']));
    if (dto['nutuSerialNumberList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_SERIAL_NUMBER, <string[]>dto['nutuSerialNumberList'], true));
    if (dto['utuSerialNumberLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_SERIAL_NUMBER, dto['utuSerialNumberLike'])); //For MysSQL
    if (dto['nutuSerialNumberLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_SERIAL_NUMBER, dto['nutuSerialNumberLike'], true)); //For MysSQL
    if (dto['utuSerialNumberLikeList']?.length > 0) items.push(SysUserTopUpSQL.LIKE_LIST_SQL(UTU_SERIAL_NUMBER, <string[]>dto['utuSerialNumberLikeList'])); //For MysSQL
    //UTU_BILLING_START_DATE--日期字段
    if (dto[utuBillingStartDate]) items.push(`${UTU_BILLING_START_DATE} is not null and date_format(${UTU_BILLING_START_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUtuBillingStartDate'])}`);
    if (dto[utuBillingStartDate]) items.push(`${UTU_BILLING_START_DATE} is not null and date_format(${UTU_BILLING_START_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUtuBillingStartDate'])}`);
    //UTU_BILLING_END_DATE--日期字段
    if (dto[utuBillingEndDate]) items.push(`${UTU_BILLING_END_DATE} is not null and date_format(${UTU_BILLING_END_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUtuBillingEndDate'])}`);
    if (dto[utuBillingEndDate]) items.push(`${UTU_BILLING_END_DATE} is not null and date_format(${UTU_BILLING_END_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUtuBillingEndDate'])}`);
    //UTU_STATE--字符串字段
    if (dto['utuStateList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_STATE, <string[]>dto['utuStateList']));
    if (dto['nutuStateList']?.length > 0) items.push(SysUserTopUpSQL.IN_SQL(UTU_STATE, <string[]>dto['nutuStateList'], true));
    if (dto['utuStateLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_STATE, dto['utuStateLike'])); //For MysSQL
    if (dto['nutuStateLike']?.length > 0) items.push(SysUserTopUpSQL.LIKE_SQL(UTU_STATE, dto['nutuStateLike'], true)); //For MysSQL
    if (dto['utuStateLikeList']?.length > 0) items.push(SysUserTopUpSQL.LIKE_LIST_SQL(UTU_STATE, <string[]>dto['utuStateLikeList'])); //For MysSQL
    //UTU_CREATE_DATE--日期字段
    if (dto[utuCreateDate]) items.push(`${UTU_CREATE_DATE} is not null and date_format(${UTU_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUtuCreateDate'])}`);
    if (dto[utuCreateDate]) items.push(`${UTU_CREATE_DATE} is not null and date_format(${UTU_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUtuCreateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysUserTopUpSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysUserTopUpCDto | Partial<SysUserTopUpCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(UTU_ID);
    cols.push(UTU_LOGIN_NAME);
    cols.push(UTU_SERIAL_NUMBER);
    cols.push(UTU_BILLING_START_DATE);
    cols.push(UTU_BILLING_END_DATE);
    cols.push(UTU_STATE);
    cols.push(UTU_CREATE_DATE);
    values.push(toSqlValue(dto[utuId]));
    values.push(toSqlValue(dto[utuLoginName]));
    values.push(toSqlValue(dto[utuSerialNumber]));
    values.push('NOW()');
    values.push('NOW()');
    values.push(toSqlValue(dto[utuState]));
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysUserTopUpCDto[] | Partial<SysUserTopUpCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(UTU_ID);
    cols.push(UTU_LOGIN_NAME);
    cols.push(UTU_SERIAL_NUMBER);
    cols.push(UTU_BILLING_START_DATE);
    cols.push(UTU_BILLING_END_DATE);
    cols.push(UTU_STATE);
    cols.push(UTU_CREATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[utuId]));
      values.push(toSqlValue(dto[utuLoginName]));
      values.push(toSqlValue(dto[utuSerialNumber]));
      values.push('NOW()');
      values.push('NOW()');
      values.push(toSqlValue(dto[utuState]));
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysUserTopUpUDto | Partial<SysUserTopUpUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserTopUpColNames, SysUserTopUpColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UTU_LOGIN_NAME} = ${toSqlValue(dto[utuLoginName])}`);
    items.push(`${UTU_SERIAL_NUMBER} = ${toSqlValue(dto[utuSerialNumber])}`);
    items.push(`${UTU_STATE} = ${toSqlValue(dto[utuState])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysUserTopUpUDto | Partial<SysUserTopUpUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserTopUpColNames, SysUserTopUpColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserTopUpSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UTU_LOGIN_NAME} = ${toSqlValue(dto[utuLoginName])}`);
    items.push(`${UTU_SERIAL_NUMBER} = ${toSqlValue(dto[utuSerialNumber])}`);
    items.push(`${UTU_STATE} = ${toSqlValue(dto[utuState])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserTopUpSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysUserTopUpUDto[] | Partial<SysUserTopUpUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysUserTopUpSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysUserTopUpUDto | Partial<SysUserTopUpUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[utuLoginName]?.length > 0) items.push(`${UTU_LOGIN_NAME} = ${toSqlValue(dto[utuLoginName])}`);
    if (dto[utuSerialNumber]?.length > 0) items.push(`${UTU_SERIAL_NUMBER} = ${toSqlValue(dto[utuSerialNumber])}`);
    if (dto[utuState]?.length > 0) items.push(`${UTU_STATE} = ${toSqlValue(dto[utuState])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysUserTopUpUDto[] | Partial<SysUserTopUpUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysUserTopUpSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): string => {
    if (!dto) return '';
    let where = SysUserTopUpSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysUserTopUpSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_USER_TOP_UP';
/**
 * 列名常量字段
 */
const UTU_ID = SysUserTopUpColNameEnum.UTU_ID;
const UTU_LOGIN_NAME = SysUserTopUpColNameEnum.UTU_LOGIN_NAME;
const UTU_SERIAL_NUMBER = SysUserTopUpColNameEnum.UTU_SERIAL_NUMBER;
const UTU_BILLING_START_DATE = SysUserTopUpColNameEnum.UTU_BILLING_START_DATE;
const UTU_BILLING_END_DATE = SysUserTopUpColNameEnum.UTU_BILLING_END_DATE;
const UTU_STATE = SysUserTopUpColNameEnum.UTU_STATE;
const UTU_CREATE_DATE = SysUserTopUpColNameEnum.UTU_CREATE_DATE;
/**
 * 实体类属性名
 */
const utuId = SysUserTopUpColPropEnum.utuId;
const utuLoginName = SysUserTopUpColPropEnum.utuLoginName;
const utuSerialNumber = SysUserTopUpColPropEnum.utuSerialNumber;
const utuBillingStartDate = SysUserTopUpColPropEnum.utuBillingStartDate;
const utuBillingEndDate = SysUserTopUpColPropEnum.utuBillingEndDate;
const utuState = SysUserTopUpColPropEnum.utuState;
const utuCreateDate = SysUserTopUpColPropEnum.utuCreateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysUserTopUpTable.PRIMER_KEY;
const primerKey = SysUserTopUpTable.primerKey;
