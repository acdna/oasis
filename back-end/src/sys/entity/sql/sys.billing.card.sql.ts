import { SysBillingCardQDto } from '../../dto/query/sys.billing.card.qdto';
import { SysBillingCardCDto } from '../../dto/create/sys.billing.card.cdto';
import { SysBillingCardUDto } from '../../dto/update/sys.billing.card.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysBillingCardColNameEnum, SysBillingCardColPropEnum, SysBillingCardTable, SysBillingCardColNames, SysBillingCardColProps } from '../ddl/sys.billing.card.cols';
/**
 * SYS_BILLING_CARD--计费卡信息表表相关SQL定义
 * @date 1/13/2021, 2:31:19 PM
 * @author jiangbin
 * @export
 * @class SysBillingCardSQL
 */
export const SysBillingCardSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let cols = SysBillingCardSQL.COL_MAPPER_SQL(dto);
    let where = SysBillingCardSQL.WHERE_SQL(dto);
    let group = SysBillingCardSQL.GROUP_BY_SQL(dto);
    let order = SysBillingCardSQL.ORDER_BY_SQL(dto);
    let limit = SysBillingCardSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let where = SysBillingCardSQL.WHERE_SQL(dto);
    let group = SysBillingCardSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let cols = SysBillingCardSQL.COL_MAPPER_SQL(dto);
    let group = SysBillingCardSQL.GROUP_BY_SQL(dto);
    let order = SysBillingCardSQL.ORDER_BY_SQL(dto);
    let limit = SysBillingCardSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysBillingCardSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysBillingCardSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysBillingCardSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let where = SysBillingCardSQL.WHERE_SQL(dto);
    let group = SysBillingCardSQL.GROUP_BY_SQL(dto);
    let order = SysBillingCardSQL.ORDER_BY_SQL(dto);
    let limit = SysBillingCardSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysBillingCardColNames, SysBillingCardColProps);
    if (!cols || cols.length == 0) cols = SysBillingCardColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[bcId]?.length > 0) items.push(` ${BC_ID} = '${dto[bcId]}'`);
    if (dto['nbcId']?.length > 0) items.push(` ${BC_ID} != '${dto['nbcId']}'`);
    if (dto[bcSerialNumber]?.length > 0) items.push(` ${BC_SERIAL_NUMBER} = '${dto[bcSerialNumber]}'`);
    if (dto['nbcSerialNumber']?.length > 0) items.push(` ${BC_SERIAL_NUMBER} != '${dto['nbcSerialNumber']}'`);
    if (dto[bcAmount]?.length > 0) items.push(` ${BC_AMOUNT} = '${dto[bcAmount]}'`);
    if (dto['nbcAmount']?.length > 0) items.push(` ${BC_AMOUNT} != '${dto['nbcAmount']}'`);
    if (dto[bcTotalTime]) items.push(` ${BC_TOTAL_TIME} = '${dto[bcTotalTime]}'`);
    if (dto['nbcTotalTime']) items.push(` ${BC_TOTAL_TIME} != '${dto['nbcTotalTime']}'`);
    if (dto[bcType]?.length > 0) items.push(` ${BC_TYPE} = '${dto[bcType]}'`);
    if (dto['nbcType']?.length > 0) items.push(` ${BC_TYPE} != '${dto['nbcType']}'`);
    if (dto[bcAccount]?.length > 0) items.push(` ${BC_ACCOUNT} = '${dto[bcAccount]}'`);
    if (dto['nbcAccount']?.length > 0) items.push(` ${BC_ACCOUNT} != '${dto['nbcAccount']}'`);
    if (dto[bcPassword]?.length > 0) items.push(` ${BC_PASSWORD} = '${dto[bcPassword]}'`);
    if (dto['nbcPassword']?.length > 0) items.push(` ${BC_PASSWORD} != '${dto['nbcPassword']}'`);
    if (dto[bcState]?.length > 0) items.push(` ${BC_STATE} = '${dto[bcState]}'`);
    if (dto['nbcState']?.length > 0) items.push(` ${BC_STATE} != '${dto['nbcState']}'`);
    if (dto[bcBiggestGeneCount]?.length > 0) items.push(` ${BC_BIGGEST_GENE_COUNT} = '${dto[bcBiggestGeneCount]}'`);
    if (dto['nbcBiggestGeneCount']?.length > 0) items.push(` ${BC_BIGGEST_GENE_COUNT} != '${dto['nbcBiggestGeneCount']}'`);
    if (dto[bcCreateDate]) items.push(`${BC_CREATE_DATE} is not null and date_format(${BC_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[bcCreateDate])}`);
    if (dto['nbcCreateDate']) items.push(`${BC_CREATE_DATE} is not null and date_format(${BC_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nbcCreateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //BC_ID--字符串字段
    if (dto['bcIdList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_ID, <string[]>dto['bcIdList']));
    if (dto['nbcIdList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_ID, <string[]>dto['nbcIdList'], true));
    if (dto['bcIdLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_ID, dto['bcIdLike'])); //For MysSQL
    if (dto['nbcIdLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_ID, dto['nbcIdLike'], true)); //For MysSQL
    if (dto['bcIdLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_ID, <string[]>dto['bcIdLikeList'])); //For MysSQL
    //BC_SERIAL_NUMBER--字符串字段
    if (dto['bcSerialNumberList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_SERIAL_NUMBER, <string[]>dto['bcSerialNumberList']));
    if (dto['nbcSerialNumberList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_SERIAL_NUMBER, <string[]>dto['nbcSerialNumberList'], true));
    if (dto['bcSerialNumberLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_SERIAL_NUMBER, dto['bcSerialNumberLike'])); //For MysSQL
    if (dto['nbcSerialNumberLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_SERIAL_NUMBER, dto['nbcSerialNumberLike'], true)); //For MysSQL
    if (dto['bcSerialNumberLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_SERIAL_NUMBER, <string[]>dto['bcSerialNumberLikeList'])); //For MysSQL
    //BC_AMOUNT--字符串字段
    if (dto['bcAmountList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_AMOUNT, <string[]>dto['bcAmountList']));
    if (dto['nbcAmountList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_AMOUNT, <string[]>dto['nbcAmountList'], true));
    if (dto['bcAmountLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_AMOUNT, dto['bcAmountLike'])); //For MysSQL
    if (dto['nbcAmountLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_AMOUNT, dto['nbcAmountLike'], true)); //For MysSQL
    if (dto['bcAmountLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_AMOUNT, <string[]>dto['bcAmountLikeList'])); //For MysSQL
    //BC_TOTAL_TIME--数字字段
    if (dto['bcTotalTimeList']) items.push(SysBillingCardSQL.IN_SQL(BC_TOTAL_TIME, dto['bcTotalTimeList']));
    if (dto['nbcTotalTimeList']) items.push(SysBillingCardSQL.IN_SQL(BC_TOTAL_TIME, dto['nbcTotalTimeList'], true));
    //BC_TYPE--字符串字段
    if (dto['bcTypeList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_TYPE, <string[]>dto['bcTypeList']));
    if (dto['nbcTypeList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_TYPE, <string[]>dto['nbcTypeList'], true));
    if (dto['bcTypeLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_TYPE, dto['bcTypeLike'])); //For MysSQL
    if (dto['nbcTypeLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_TYPE, dto['nbcTypeLike'], true)); //For MysSQL
    if (dto['bcTypeLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_TYPE, <string[]>dto['bcTypeLikeList'])); //For MysSQL
    //BC_ACCOUNT--字符串字段
    if (dto['bcAccountList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_ACCOUNT, <string[]>dto['bcAccountList']));
    if (dto['nbcAccountList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_ACCOUNT, <string[]>dto['nbcAccountList'], true));
    if (dto['bcAccountLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_ACCOUNT, dto['bcAccountLike'])); //For MysSQL
    if (dto['nbcAccountLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_ACCOUNT, dto['nbcAccountLike'], true)); //For MysSQL
    if (dto['bcAccountLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_ACCOUNT, <string[]>dto['bcAccountLikeList'])); //For MysSQL
    //BC_PASSWORD--字符串字段
    if (dto['bcPasswordList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_PASSWORD, <string[]>dto['bcPasswordList']));
    if (dto['nbcPasswordList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_PASSWORD, <string[]>dto['nbcPasswordList'], true));
    if (dto['bcPasswordLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_PASSWORD, dto['bcPasswordLike'])); //For MysSQL
    if (dto['nbcPasswordLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_PASSWORD, dto['nbcPasswordLike'], true)); //For MysSQL
    if (dto['bcPasswordLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_PASSWORD, <string[]>dto['bcPasswordLikeList'])); //For MysSQL
    //BC_STATE--字符串字段
    if (dto['bcStateList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_STATE, <string[]>dto['bcStateList']));
    if (dto['nbcStateList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_STATE, <string[]>dto['nbcStateList'], true));
    if (dto['bcStateLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_STATE, dto['bcStateLike'])); //For MysSQL
    if (dto['nbcStateLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_STATE, dto['nbcStateLike'], true)); //For MysSQL
    if (dto['bcStateLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_STATE, <string[]>dto['bcStateLikeList'])); //For MysSQL
    //BC_BIGGEST_GENE_COUNT--字符串字段
    if (dto['bcBiggestGeneCountList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_BIGGEST_GENE_COUNT, <string[]>dto['bcBiggestGeneCountList']));
    if (dto['nbcBiggestGeneCountList']?.length > 0) items.push(SysBillingCardSQL.IN_SQL(BC_BIGGEST_GENE_COUNT, <string[]>dto['nbcBiggestGeneCountList'], true));
    if (dto['bcBiggestGeneCountLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_BIGGEST_GENE_COUNT, dto['bcBiggestGeneCountLike'])); //For MysSQL
    if (dto['nbcBiggestGeneCountLike']?.length > 0) items.push(SysBillingCardSQL.LIKE_SQL(BC_BIGGEST_GENE_COUNT, dto['nbcBiggestGeneCountLike'], true)); //For MysSQL
    if (dto['bcBiggestGeneCountLikeList']?.length > 0) items.push(SysBillingCardSQL.LIKE_LIST_SQL(BC_BIGGEST_GENE_COUNT, <string[]>dto['bcBiggestGeneCountLikeList'])); //For MysSQL
    //BC_CREATE_DATE--日期字段
    if (dto[bcCreateDate]) items.push(`${BC_CREATE_DATE} is not null and date_format(${BC_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sBcCreateDate'])}`);
    if (dto[bcCreateDate]) items.push(`${BC_CREATE_DATE} is not null and date_format(${BC_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eBcCreateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysBillingCardSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysBillingCardCDto | Partial<SysBillingCardCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(BC_ID);
    cols.push(BC_SERIAL_NUMBER);
    cols.push(BC_AMOUNT);
    cols.push(BC_TOTAL_TIME);
    cols.push(BC_TYPE);
    cols.push(BC_ACCOUNT);
    cols.push(BC_PASSWORD);
    cols.push(BC_STATE);
    cols.push(BC_BIGGEST_GENE_COUNT);
    cols.push(BC_CREATE_DATE);
    values.push(toSqlValue(dto[bcId]));
    values.push(toSqlValue(dto[bcSerialNumber]));
    values.push(toSqlValue(dto[bcAmount]));
    values.push(toSqlValue(dto[bcTotalTime]));
    values.push(toSqlValue(dto[bcType]));
    values.push(toSqlValue(dto[bcAccount]));
    values.push(toSqlValue(dto[bcPassword]));
    values.push(toSqlValue(dto[bcState]));
    values.push(toSqlValue(dto[bcBiggestGeneCount]));
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysBillingCardCDto[] | Partial<SysBillingCardCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(BC_ID);
    cols.push(BC_SERIAL_NUMBER);
    cols.push(BC_AMOUNT);
    cols.push(BC_TOTAL_TIME);
    cols.push(BC_TYPE);
    cols.push(BC_ACCOUNT);
    cols.push(BC_PASSWORD);
    cols.push(BC_STATE);
    cols.push(BC_BIGGEST_GENE_COUNT);
    cols.push(BC_CREATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[bcId]));
      values.push(toSqlValue(dto[bcSerialNumber]));
      values.push(toSqlValue(dto[bcAmount]));
      values.push(toSqlValue(dto[bcTotalTime]));
      values.push(toSqlValue(dto[bcType]));
      values.push(toSqlValue(dto[bcAccount]));
      values.push(toSqlValue(dto[bcPassword]));
      values.push(toSqlValue(dto[bcState]));
      values.push(toSqlValue(dto[bcBiggestGeneCount]));
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysBillingCardUDto | Partial<SysBillingCardUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysBillingCardColNames, SysBillingCardColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${BC_SERIAL_NUMBER} = ${toSqlValue(dto[bcSerialNumber])}`);
    items.push(`${BC_AMOUNT} = ${toSqlValue(dto[bcAmount])}`);
    items.push(`${BC_TOTAL_TIME} = ${toSqlValue(dto[bcTotalTime])}`);
    items.push(`${BC_TYPE} = ${toSqlValue(dto[bcType])}`);
    items.push(`${BC_ACCOUNT} = ${toSqlValue(dto[bcAccount])}`);
    items.push(`${BC_PASSWORD} = ${toSqlValue(dto[bcPassword])}`);
    items.push(`${BC_STATE} = ${toSqlValue(dto[bcState])}`);
    items.push(`${BC_BIGGEST_GENE_COUNT} = ${toSqlValue(dto[bcBiggestGeneCount])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysBillingCardUDto | Partial<SysBillingCardUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysBillingCardColNames, SysBillingCardColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysBillingCardSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${BC_SERIAL_NUMBER} = ${toSqlValue(dto[bcSerialNumber])}`);
    items.push(`${BC_AMOUNT} = ${toSqlValue(dto[bcAmount])}`);
    items.push(`${BC_TOTAL_TIME} = ${toSqlValue(dto[bcTotalTime])}`);
    items.push(`${BC_TYPE} = ${toSqlValue(dto[bcType])}`);
    items.push(`${BC_ACCOUNT} = ${toSqlValue(dto[bcAccount])}`);
    items.push(`${BC_PASSWORD} = ${toSqlValue(dto[bcPassword])}`);
    items.push(`${BC_STATE} = ${toSqlValue(dto[bcState])}`);
    items.push(`${BC_BIGGEST_GENE_COUNT} = ${toSqlValue(dto[bcBiggestGeneCount])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysBillingCardSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysBillingCardUDto[] | Partial<SysBillingCardUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysBillingCardSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysBillingCardUDto | Partial<SysBillingCardUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[bcSerialNumber]?.length > 0) items.push(`${BC_SERIAL_NUMBER} = ${toSqlValue(dto[bcSerialNumber])}`);
    if (dto[bcAmount]?.length > 0) items.push(`${BC_AMOUNT} = ${toSqlValue(dto[bcAmount])}`);
    if (dto[bcTotalTime]) items.push(`${BC_TOTAL_TIME} = ${toSqlValue(dto[bcTotalTime])}`);
    if (dto[bcType]?.length > 0) items.push(`${BC_TYPE} = ${toSqlValue(dto[bcType])}`);
    if (dto[bcAccount]?.length > 0) items.push(`${BC_ACCOUNT} = ${toSqlValue(dto[bcAccount])}`);
    if (dto[bcPassword]?.length > 0) items.push(`${BC_PASSWORD} = ${toSqlValue(dto[bcPassword])}`);
    if (dto[bcState]?.length > 0) items.push(`${BC_STATE} = ${toSqlValue(dto[bcState])}`);
    if (dto[bcBiggestGeneCount]?.length > 0) items.push(`${BC_BIGGEST_GENE_COUNT} = ${toSqlValue(dto[bcBiggestGeneCount])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysBillingCardUDto[] | Partial<SysBillingCardUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysBillingCardSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): string => {
    if (!dto) return '';
    let where = SysBillingCardSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysBillingCardSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_BILLING_CARD';
/**
 * 列名常量字段
 */
const BC_ID = SysBillingCardColNameEnum.BC_ID;
const BC_SERIAL_NUMBER = SysBillingCardColNameEnum.BC_SERIAL_NUMBER;
const BC_AMOUNT = SysBillingCardColNameEnum.BC_AMOUNT;
const BC_TOTAL_TIME = SysBillingCardColNameEnum.BC_TOTAL_TIME;
const BC_TYPE = SysBillingCardColNameEnum.BC_TYPE;
const BC_ACCOUNT = SysBillingCardColNameEnum.BC_ACCOUNT;
const BC_PASSWORD = SysBillingCardColNameEnum.BC_PASSWORD;
const BC_STATE = SysBillingCardColNameEnum.BC_STATE;
const BC_BIGGEST_GENE_COUNT = SysBillingCardColNameEnum.BC_BIGGEST_GENE_COUNT;
const BC_CREATE_DATE = SysBillingCardColNameEnum.BC_CREATE_DATE;
/**
 * 实体类属性名
 */
const bcId = SysBillingCardColPropEnum.bcId;
const bcSerialNumber = SysBillingCardColPropEnum.bcSerialNumber;
const bcAmount = SysBillingCardColPropEnum.bcAmount;
const bcTotalTime = SysBillingCardColPropEnum.bcTotalTime;
const bcType = SysBillingCardColPropEnum.bcType;
const bcAccount = SysBillingCardColPropEnum.bcAccount;
const bcPassword = SysBillingCardColPropEnum.bcPassword;
const bcState = SysBillingCardColPropEnum.bcState;
const bcBiggestGeneCount = SysBillingCardColPropEnum.bcBiggestGeneCount;
const bcCreateDate = SysBillingCardColPropEnum.bcCreateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysBillingCardTable.PRIMER_KEY;
const primerKey = SysBillingCardTable.primerKey;
