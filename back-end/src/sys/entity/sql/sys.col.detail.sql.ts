import { SysColDetailQDto } from '../../dto/query/sys.col.detail.qdto';
import { SysColDetailCDto } from '../../dto/create/sys.col.detail.cdto';
import { SysColDetailUDto } from '../../dto/update/sys.col.detail.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysColDetailColNameEnum, SysColDetailColPropEnum, SysColDetailTable, SysColDetailColNames, SysColDetailColProps } from '../ddl/sys.col.detail.cols';
/**
 * SYS_COL_DETAIL--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysColDetailSQL
 */
export const SysColDetailSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let cols = SysColDetailSQL.COL_MAPPER_SQL(dto);
    let where = SysColDetailSQL.WHERE_SQL(dto);
    let group = SysColDetailSQL.GROUP_BY_SQL(dto);
    let order = SysColDetailSQL.ORDER_BY_SQL(dto);
    let limit = SysColDetailSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let where = SysColDetailSQL.WHERE_SQL(dto);
    let group = SysColDetailSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let cols = SysColDetailSQL.COL_MAPPER_SQL(dto);
    let group = SysColDetailSQL.GROUP_BY_SQL(dto);
    let order = SysColDetailSQL.ORDER_BY_SQL(dto);
    let limit = SysColDetailSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysColDetailSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysColDetailSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysColDetailSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let where = SysColDetailSQL.WHERE_SQL(dto);
    let group = SysColDetailSQL.GROUP_BY_SQL(dto);
    let order = SysColDetailSQL.ORDER_BY_SQL(dto);
    let limit = SysColDetailSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysColDetailColNames, SysColDetailColProps);
    if (!cols || cols.length == 0) cols = SysColDetailColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[cdId]?.length > 0) items.push(` ${CD_ID} = '${dto[cdId]}'`);
    if (dto['ncdId']?.length > 0) items.push(` ${CD_ID} != '${dto['ncdId']}'`);
    if (dto[cdTableName]?.length > 0) items.push(` ${CD_TABLE_NAME} = '${dto[cdTableName]}'`);
    if (dto['ncdTableName']?.length > 0) items.push(` ${CD_TABLE_NAME} != '${dto['ncdTableName']}'`);
    if (dto[cdColName]?.length > 0) items.push(` ${CD_COL_NAME} = '${dto[cdColName]}'`);
    if (dto['ncdColName']?.length > 0) items.push(` ${CD_COL_NAME} != '${dto['ncdColName']}'`);
    if (dto[cdDescName]?.length > 0) items.push(` ${CD_DESC_NAME} = '${dto[cdDescName]}'`);
    if (dto['ncdDescName']?.length > 0) items.push(` ${CD_DESC_NAME} != '${dto['ncdDescName']}'`);
    if (dto[cdDictName]?.length > 0) items.push(` ${CD_DICT_NAME} = '${dto[cdDictName]}'`);
    if (dto['ncdDictName']?.length > 0) items.push(` ${CD_DICT_NAME} != '${dto['ncdDictName']}'`);
    if (dto[cdMode]?.length > 0) items.push(` ${CD_MODE} = '${dto[cdMode]}'`);
    if (dto['ncdMode']?.length > 0) items.push(` ${CD_MODE} != '${dto['ncdMode']}'`);
    if (dto[cdCreateDate]) items.push(`${CD_CREATE_DATE} is not null and date_format(${CD_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[cdCreateDate])}`);
    if (dto['ncdCreateDate']) items.push(`${CD_CREATE_DATE} is not null and date_format(${CD_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ncdCreateDate'])}`);
    if (dto[cdUpdateDate]) items.push(`${CD_UPDATE_DATE} is not null and date_format(${CD_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[cdUpdateDate])}`);
    if (dto['ncdUpdateDate']) items.push(`${CD_UPDATE_DATE} is not null and date_format(${CD_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ncdUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //CD_ID--字符串字段
    if (dto['cdIdList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_ID, <string[]>dto['cdIdList']));
    if (dto['ncdIdList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_ID, <string[]>dto['ncdIdList'], true));
    if (dto['cdIdLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_ID, dto['cdIdLike'])); //For MysSQL
    if (dto['ncdIdLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_ID, dto['ncdIdLike'], true)); //For MysSQL
    if (dto['cdIdLikeList']?.length > 0) items.push(SysColDetailSQL.LIKE_LIST_SQL(CD_ID, <string[]>dto['cdIdLikeList'])); //For MysSQL
    //CD_TABLE_NAME--字符串字段
    if (dto['cdTableNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_TABLE_NAME, <string[]>dto['cdTableNameList']));
    if (dto['ncdTableNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_TABLE_NAME, <string[]>dto['ncdTableNameList'], true));
    if (dto['cdTableNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_TABLE_NAME, dto['cdTableNameLike'])); //For MysSQL
    if (dto['ncdTableNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_TABLE_NAME, dto['ncdTableNameLike'], true)); //For MysSQL
    if (dto['cdTableNameLikeList']?.length > 0) items.push(SysColDetailSQL.LIKE_LIST_SQL(CD_TABLE_NAME, <string[]>dto['cdTableNameLikeList'])); //For MysSQL
    //CD_COL_NAME--字符串字段
    if (dto['cdColNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_COL_NAME, <string[]>dto['cdColNameList']));
    if (dto['ncdColNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_COL_NAME, <string[]>dto['ncdColNameList'], true));
    if (dto['cdColNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_COL_NAME, dto['cdColNameLike'])); //For MysSQL
    if (dto['ncdColNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_COL_NAME, dto['ncdColNameLike'], true)); //For MysSQL
    if (dto['cdColNameLikeList']?.length > 0) items.push(SysColDetailSQL.LIKE_LIST_SQL(CD_COL_NAME, <string[]>dto['cdColNameLikeList'])); //For MysSQL
    //CD_DESC_NAME--字符串字段
    if (dto['cdDescNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_DESC_NAME, <string[]>dto['cdDescNameList']));
    if (dto['ncdDescNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_DESC_NAME, <string[]>dto['ncdDescNameList'], true));
    if (dto['cdDescNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_DESC_NAME, dto['cdDescNameLike'])); //For MysSQL
    if (dto['ncdDescNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_DESC_NAME, dto['ncdDescNameLike'], true)); //For MysSQL
    if (dto['cdDescNameLikeList']?.length > 0) items.push(SysColDetailSQL.LIKE_LIST_SQL(CD_DESC_NAME, <string[]>dto['cdDescNameLikeList'])); //For MysSQL
    //CD_DICT_NAME--字符串字段
    if (dto['cdDictNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_DICT_NAME, <string[]>dto['cdDictNameList']));
    if (dto['ncdDictNameList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_DICT_NAME, <string[]>dto['ncdDictNameList'], true));
    if (dto['cdDictNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_DICT_NAME, dto['cdDictNameLike'])); //For MysSQL
    if (dto['ncdDictNameLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_DICT_NAME, dto['ncdDictNameLike'], true)); //For MysSQL
    if (dto['cdDictNameLikeList']?.length > 0) items.push(SysColDetailSQL.LIKE_LIST_SQL(CD_DICT_NAME, <string[]>dto['cdDictNameLikeList'])); //For MysSQL
    //CD_MODE--字符串字段
    if (dto['cdModeList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_MODE, <string[]>dto['cdModeList']));
    if (dto['ncdModeList']?.length > 0) items.push(SysColDetailSQL.IN_SQL(CD_MODE, <string[]>dto['ncdModeList'], true));
    if (dto['cdModeLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_MODE, dto['cdModeLike'])); //For MysSQL
    if (dto['ncdModeLike']?.length > 0) items.push(SysColDetailSQL.LIKE_SQL(CD_MODE, dto['ncdModeLike'], true)); //For MysSQL
    if (dto['cdModeLikeList']?.length > 0) items.push(SysColDetailSQL.LIKE_LIST_SQL(CD_MODE, <string[]>dto['cdModeLikeList'])); //For MysSQL
    //CD_CREATE_DATE--日期字段
    if (dto[cdCreateDate]) items.push(`${CD_CREATE_DATE} is not null and date_format(${CD_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sCdCreateDate'])}`);
    if (dto[cdCreateDate]) items.push(`${CD_CREATE_DATE} is not null and date_format(${CD_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eCdCreateDate'])}`);
    //CD_UPDATE_DATE--日期字段
    if (dto[cdUpdateDate]) items.push(`${CD_UPDATE_DATE} is not null and date_format(${CD_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sCdUpdateDate'])}`);
    if (dto[cdUpdateDate]) items.push(`${CD_UPDATE_DATE} is not null and date_format(${CD_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eCdUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysColDetailSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysColDetailCDto | Partial<SysColDetailCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(CD_ID);
    cols.push(CD_TABLE_NAME);
    cols.push(CD_COL_NAME);
    cols.push(CD_DESC_NAME);
    cols.push(CD_DICT_NAME);
    cols.push(CD_MODE);
    cols.push(CD_CREATE_DATE);
    cols.push(CD_UPDATE_DATE);
    values.push(toSqlValue(dto[cdId]));
    values.push(toSqlValue(dto[cdTableName]));
    values.push(toSqlValue(dto[cdColName]));
    values.push(toSqlValue(dto[cdDescName]));
    values.push(toSqlValue(dto[cdDictName]));
    values.push(toSqlValue(dto[cdMode]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysColDetailCDto[] | Partial<SysColDetailCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(CD_ID);
    cols.push(CD_TABLE_NAME);
    cols.push(CD_COL_NAME);
    cols.push(CD_DESC_NAME);
    cols.push(CD_DICT_NAME);
    cols.push(CD_MODE);
    cols.push(CD_CREATE_DATE);
    cols.push(CD_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[cdId]));
      values.push(toSqlValue(dto[cdTableName]));
      values.push(toSqlValue(dto[cdColName]));
      values.push(toSqlValue(dto[cdDescName]));
      values.push(toSqlValue(dto[cdDictName]));
      values.push(toSqlValue(dto[cdMode]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysColDetailUDto | Partial<SysColDetailUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysColDetailColNames, SysColDetailColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${CD_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CD_TABLE_NAME} = ${toSqlValue(dto[cdTableName])}`);
    items.push(`${CD_COL_NAME} = ${toSqlValue(dto[cdColName])}`);
    items.push(`${CD_DESC_NAME} = ${toSqlValue(dto[cdDescName])}`);
    items.push(`${CD_DICT_NAME} = ${toSqlValue(dto[cdDictName])}`);
    items.push(`${CD_MODE} = ${toSqlValue(dto[cdMode])}`);
    items.push(`${CD_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysColDetailUDto | Partial<SysColDetailUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysColDetailColNames, SysColDetailColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${CD_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysColDetailSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CD_TABLE_NAME} = ${toSqlValue(dto[cdTableName])}`);
    items.push(`${CD_COL_NAME} = ${toSqlValue(dto[cdColName])}`);
    items.push(`${CD_DESC_NAME} = ${toSqlValue(dto[cdDescName])}`);
    items.push(`${CD_DICT_NAME} = ${toSqlValue(dto[cdDictName])}`);
    items.push(`${CD_MODE} = ${toSqlValue(dto[cdMode])}`);
    items.push(`${CD_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysColDetailSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysColDetailUDto[] | Partial<SysColDetailUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysColDetailSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysColDetailUDto | Partial<SysColDetailUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[cdTableName]?.length > 0) items.push(`${CD_TABLE_NAME} = ${toSqlValue(dto[cdTableName])}`);
    if (dto[cdColName]?.length > 0) items.push(`${CD_COL_NAME} = ${toSqlValue(dto[cdColName])}`);
    if (dto[cdDescName]?.length > 0) items.push(`${CD_DESC_NAME} = ${toSqlValue(dto[cdDescName])}`);
    if (dto[cdDictName]?.length > 0) items.push(`${CD_DICT_NAME} = ${toSqlValue(dto[cdDictName])}`);
    if (dto[cdMode]?.length > 0) items.push(`${CD_MODE} = ${toSqlValue(dto[cdMode])}`);
    if (dto[cdUpdateDate]) items.push(`${CD_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysColDetailUDto[] | Partial<SysColDetailUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysColDetailSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysColDetailQDto | Partial<SysColDetailQDto>): string => {
    if (!dto) return '';
    let where = SysColDetailSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysColDetailSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_COL_DETAIL';
/**
 * 列名常量字段
 */
const CD_ID = SysColDetailColNameEnum.CD_ID;
const CD_TABLE_NAME = SysColDetailColNameEnum.CD_TABLE_NAME;
const CD_COL_NAME = SysColDetailColNameEnum.CD_COL_NAME;
const CD_DESC_NAME = SysColDetailColNameEnum.CD_DESC_NAME;
const CD_DICT_NAME = SysColDetailColNameEnum.CD_DICT_NAME;
const CD_MODE = SysColDetailColNameEnum.CD_MODE;
const CD_CREATE_DATE = SysColDetailColNameEnum.CD_CREATE_DATE;
const CD_UPDATE_DATE = SysColDetailColNameEnum.CD_UPDATE_DATE;
/**
 * 实体类属性名
 */
const cdId = SysColDetailColPropEnum.cdId;
const cdTableName = SysColDetailColPropEnum.cdTableName;
const cdColName = SysColDetailColPropEnum.cdColName;
const cdDescName = SysColDetailColPropEnum.cdDescName;
const cdDictName = SysColDetailColPropEnum.cdDictName;
const cdMode = SysColDetailColPropEnum.cdMode;
const cdCreateDate = SysColDetailColPropEnum.cdCreateDate;
const cdUpdateDate = SysColDetailColPropEnum.cdUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysColDetailTable.PRIMER_KEY;
const primerKey = SysColDetailTable.primerKey;
