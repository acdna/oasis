DROP TABLE IF EXISTS `SYS_BILLING_CARD`;

CREATE TABLE `sys_billing_card` (
  `bc_id` varchar(255) NOT NULL COMMENT 'id',
  `bc_serial_number` varchar(255) NOT NULL COMMENT '序列号信息',
  `bc_amount` varchar(255) DEFAULT NULL COMMENT '总额',
  `bc_total_time` int(11) DEFAULT NULL COMMENT '总时间',
  `bc_type` varchar(255) DEFAULT NULL COMMENT '类型(0-点卡，1-月卡默认，2-年卡)',
  `bc_account` varchar(255) DEFAULT NULL COMMENT '绑定帐号',
  `bc_password` varchar(255) DEFAULT NULL COMMENT '绑定密码',
  `bc_state` varchar(255) DEFAULT NULL COMMENT '状态(0-待激活/1-已激活/2-注销)',
  `bc_biggest_gene_count` varchar(255) DEFAULT NULL COMMENT '最大指纹数',
  `bc_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`bc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计费卡信息表';

DROP TABLE IF EXISTS `SYS_CACHE_TRACE`;

CREATE TABLE `sys_cache_trace` (
  `CT_ID` varchar(32) NOT NULL COMMENT '缓存记录ID',
  `CT_CODE` varchar(12) DEFAULT NULL COMMENT '缓存类型代码',
  `CT_OPERATE` varchar(12) DEFAULT NULL COMMENT '缓存操作类型代码:I、U、D',
  `CT_TARGET_ID` varchar(64) DEFAULT NULL COMMENT '缓存需要操作的目标记录ID',
  `CT_CREATE_DATE` datetime(6) DEFAULT NULL COMMENT '缓存记录创建时间',
  `CT_COMMENT` varchar(128) DEFAULT NULL COMMENT '缓存备注信息',
  PRIMARY KEY (`CT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='缓存信息表';

DROP TABLE IF EXISTS `SYS_CMS_FILES`;

CREATE TABLE `sys_cms_files` (
  `SC_FILE_ID` varchar(64) NOT NULL DEFAULT '' COMMENT '文件编号',
  `SC_FILE_NAME` varchar(128) DEFAULT NULL COMMENT '文件名称',
  `SC_FILE_NAME_CN` varchar(128) DEFAULT NULL,
  `SC_FILE_TYPE` varchar(128) NOT NULL COMMENT '文件类型',
  `SC_FILE_PATH` varchar(1024) DEFAULT NULL COMMENT '文件路径',
  `SC_FILE_IS_NEW` varchar(4) DEFAULT '0' COMMENT '是否是最新',
  `SC_FILE_MANAGER` varchar(128) DEFAULT NULL COMMENT '文件上传者',
  `SC_FILE_CREATE_DATE` datetime DEFAULT NULL COMMENT '文件创建时间',
  `SC_FIEL_UPDATE_DATE` datetime DEFAULT NULL COMMENT '文件更新时间',
  `SC_FILE_SPECIES` varchar(32) DEFAULT NULL COMMENT '种属',
  PRIMARY KEY (`SC_FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_CMS_NOTICE`;

CREATE TABLE `sys_cms_notice` (
  `SC_NOTICE_ID` varchar(64) NOT NULL COMMENT '公告编号',
  `SC_NOTICE_TITLE` varchar(128) DEFAULT NULL COMMENT '公告标题',
  `SC_NOTICE_CONTENT` varchar(20000) DEFAULT NULL COMMENT '公告内容',
  `SC_NOTICE_IS_NEW` varchar(8) DEFAULT '0' COMMENT '是否是新文件',
  `SC_NOTICE_MANAGER` varchar(128) DEFAULT NULL COMMENT '公告发布者',
  `SC_NOTICE_CREATE_DATE` datetime DEFAULT NULL COMMENT '公告创建时间',
  `SC_NOTICE_UPDATE_DATE` datetime DEFAULT NULL COMMENT '公告修改时间',
  PRIMARY KEY (`SC_NOTICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_COL_DETAIL`;

CREATE TABLE `sys_col_detail` (
  `CD_ID` varchar(32) NOT NULL COMMENT '主键ID',
  `CD_TABLE_NAME` varchar(255) NOT NULL COMMENT '表名,$ALUVEC$',
  `CD_COL_NAME` varchar(255) NOT NULL COMMENT '列名,$ALUVEC$',
  `CD_DESC_NAME` varchar(64) DEFAULT NULL COMMENT '列中文名',
  `CD_DICT_NAME` varchar(255) DEFAULT NULL COMMENT '字典名,$ALUVEC$',
  `CD_MODE` varchar(64) DEFAULT NULL COMMENT '模式,$ALUVE$如:AULVECYN,${SysDicts.SYS_COL_MODE}',
  `CD_CREATE_DATE` datetime DEFAULT NULL COMMENT '创建日期',
  `CD_UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新日期,$LV$',
  PRIMARY KEY (`CD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_CONFIG`;

CREATE TABLE `sys_config` (
  `CON_ID` varchar(64) NOT NULL COMMENT 'ID',
  `CON_NAME` varchar(128) NOT NULL COMMENT '参数名称',
  `CON_NAME_EN` varchar(128) DEFAULT NULL COMMENT '参数名称,英文',
  `CON_VALUE` varchar(128) DEFAULT NULL COMMENT '参数值',
  `CON_GROUP` varchar(128) DEFAULT NULL COMMENT '所属组',
  `CON_PARAM_TYPE` enum('SYSTEM','USER','SPECIES') DEFAULT 'SYSTEM' COMMENT '参数类型',
  `CON_PARENT_ID` varchar(64) DEFAULT NULL COMMENT '父级ID',
  `CON_TYPE` enum('DIR','DATA') NOT NULL DEFAULT 'DATA' COMMENT '参数类型(DATA：数据，DIR 目录)',
  `CON_CREATE_DATE` datetime NOT NULL COMMENT '创建日期',
  `CON_UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新日期',
  `CON_MANAGER` varchar(64) DEFAULT NULL COMMENT '用户',
  `CON_SPECIES` varchar(64) DEFAULT NULL COMMENT '种属',
  `CON_ORDER` int(10) DEFAULT '1' COMMENT '排序',
  `CON_STATE` enum('OFF','ON') NOT NULL DEFAULT 'ON' COMMENT '状态（ ON 启用 , OFF 注销 ）',
  `CON_REMARK` varchar(128) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`CON_ID`),
  KEY `CON_GROUP` (`CON_GROUP`),
  KEY `CON_PARENT_ID` (`CON_PARENT_ID`),
  CONSTRAINT `sys_config_ibfk_1` FOREIGN KEY (`CON_PARENT_ID`) REFERENCES `sys_config` (`CON_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_CONTACT`;

CREATE TABLE `sys_contact` (
  `CONTACT_ID` varchar(64) NOT NULL COMMENT '反馈编号',
  `CONTACT_AUTHOR` varchar(128) DEFAULT NULL COMMENT '反馈用户',
  `CONTACT_EMAIL` varchar(128) DEFAULT NULL COMMENT '反馈邮箱',
  `CONTACT_SUBJECT` varchar(128) DEFAULT NULL COMMENT '反馈主题',
  `CONTACT_TEXT` varchar(256) NOT NULL COMMENT '反馈内容',
  `CONTACT_CREATE_DATE` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`CONTACT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_DICTIONARY`;

CREATE TABLE `sys_dictionary` (
  `DIC_ID` varchar(64) NOT NULL COMMENT 'ID',
  `DIC_NAME` varchar(128) NOT NULL COMMENT '字典名称',
  `DIC_NAME_EN` varchar(128) DEFAULT NULL,
  `DIC_VALUE` varchar(128) DEFAULT NULL COMMENT '字典值',
  `DIC_GROUP` varchar(128) DEFAULT NULL COMMENT '所属组',
  `DIC_MODULE` varchar(32) DEFAULT NULL COMMENT '模块名',
  `DIC_PARENT_ID` varchar(64) DEFAULT NULL COMMENT '父级ID',
  `DIC_TYPE` enum('DIR','DATA') NOT NULL DEFAULT 'DATA' COMMENT '字典类型(DATA：数据，DIR 目录)',
  `DIC_ORDER` int(10) DEFAULT '1' COMMENT '排序',
  `DIC_STATE` enum('OFF','ON') NOT NULL DEFAULT 'ON' COMMENT '状态（ ON 启用 , OFF 注销 ）',
  `dic_params` varchar(512) DEFAULT NULL,
  `DIC_SPECIES` varchar(512) DEFAULT NULL COMMENT '关联种属',
  `DIC_CREATE_DATE` datetime DEFAULT NULL,
  `DIC_UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`DIC_ID`),
  KEY `DIC_GROUP` (`DIC_GROUP`),
  KEY `DIC_PARENT_ID` (`DIC_PARENT_ID`),
  CONSTRAINT `sys_dictionary_ibfk_1` FOREIGN KEY (`DIC_PARENT_ID`) REFERENCES `sys_dictionary` (`DIC_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_FILES`;

CREATE TABLE `sys_files` (
  `FILE_ID` varchar(32) NOT NULL COMMENT '记录ID',
  `FILE_BARCODE` varchar(128) DEFAULT NULL COMMENT '文件条码号',
  `FILE_RELATE_ID` varchar(64) DEFAULT NULL COMMENT '关联的其它记录信息ID号',
  `FILE_NAME` varchar(256) NOT NULL COMMENT '文件名称',
  `FILE_NAME_EN` varchar(256) DEFAULT NULL COMMENT '文件名,英文',
  `FILE_PATH` varchar(1024) NOT NULL COMMENT '文件存储路径',
  `FILE_CREATE_DATE` datetime NOT NULL COMMENT '创建日期',
  `FILE_UPDATE_DATE` datetime NOT NULL COMMENT '文件更新日期',
  `FILE_IS_USED` varchar(10) NOT NULL COMMENT '是否被启用',
  `FILE_OWNER` varchar(64) NOT NULL COMMENT '所属用户,即谁上传的就是谁的',
  `FILE_SIZE` bigint(20) NOT NULL COMMENT '文件大小',
  `FILE_GRANTS` varchar(256) DEFAULT NULL COMMENT '用户权限表，只有符合此权限的用户才能进行访问,未指定时则任意用户均可访问',
  `FILE_CLASSES` varchar(32) DEFAULT NULL COMMENT '文件分类，这是用来区分同一关联ID下的不同类型文件',
  `FILE_STATE` varchar(20) DEFAULT NULL COMMENT '文件状态信息',
  `FILE_TYPE` varchar(50) DEFAULT NULL COMMENT '文件类型，字典中定义的值',
  `FILE_COMMENTS` varchar(64) DEFAULT NULL COMMENT '文件备注信息',
  `FILE_SPECIES` varchar(32) DEFAULT NULL COMMENT '种属',
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件管理';

DROP TABLE IF EXISTS `SYS_HELP`;

CREATE TABLE `sys_help` (
  `HELP_ID` varchar(64) NOT NULL COMMENT '帮助编号',
  `HELP_QUESTION` varchar(64) DEFAULT NULL COMMENT '帮助问题',
  `HELP_ANSWER` varchar(512) DEFAULT NULL COMMENT '帮助回答',
  `HELP_PARENT_ID` varchar(64) DEFAULT NULL COMMENT '问题编号父级编号',
  `HELP_TYPE` varchar(255) DEFAULT NULL COMMENT '帮助类别（目录 | 问题）',
  `HELP_PATH` varchar(256) DEFAULT NULL COMMENT '帮助匹配路径',
  `HELP_SORT` varchar(64) DEFAULT NULL COMMENT '排序',
  `HELP_CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `HELP_UPDATE_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`HELP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_INSTRUMENT`;

CREATE TABLE `sys_instrument` (
  `INS_ID` varchar(64) NOT NULL DEFAULT '' COMMENT '仪器编号',
  `INS_BARCODE` varchar(128) DEFAULT NULL COMMENT '仪器条码号',
  `INS_NAME` varchar(64) DEFAULT NULL COMMENT '仪器名称',
  `INS_MODEL` varchar(64) DEFAULT NULL COMMENT '仪器型号',
  `INS_MANAGER` varchar(64) DEFAULT NULL COMMENT '仪器负责人',
  `INS_TYPE` varchar(64) DEFAULT NULL COMMENT '仪器类型',
  `INS_REMARK` varchar(128) DEFAULT NULL COMMENT '仪器描述',
  `INS_BUY_DATE` datetime DEFAULT NULL COMMENT '仪器购买日期',
  `INS_MAINTAIN_PERIOD` varchar(32) DEFAULT NULL COMMENT '仪器维护周期',
  `INS_MAKER` varchar(64) DEFAULT NULL COMMENT '仪器厂商',
  `INS_PRICE` varchar(255) DEFAULT NULL COMMENT '仪器价格',
  `INS_EXTRA_PROVIDE` varchar(128) DEFAULT NULL,
  `INS_CREATE_DATE` datetime DEFAULT NULL,
  `INS_UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`INS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_LOG`;

CREATE TABLE `sys_log` (
  `log_id` varchar(64) NOT NULL COMMENT '日志ID号_Primary Key',
  `log_user` varchar(64) DEFAULT NULL COMMENT '操作人员名称',
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  `log_ip` varchar(64) NOT NULL COMMENT 'IP地址',
  `log_url` varchar(255) NOT NULL COMMENT '操作URL',
  `log_title` varchar(128) NOT NULL COMMENT '模块',
  `log_content` longtext NOT NULL COMMENT '内容',
  `log_type` tinyint(255) NOT NULL COMMENT '操作类型(1.登录成功，2.登录失败，3.普通操作）',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_MEMORY`;

CREATE TABLE `sys_memory` (
  `MEM_ID` varchar(32) NOT NULL,
  `MEM_TOTAL` bigint(20) DEFAULT NULL COMMENT '总内存',
  `MEM_FREE` bigint(20) DEFAULT NULL COMMENT '可用内存',
  `MEM_USED` bigint(20) DEFAULT NULL COMMENT '已使用内存',
  `MEM_IS_WARNING` varchar(32) DEFAULT NULL COMMENT '内存用量是否预警:0-预警，1-不预警',
  `MEM_MAX` bigint(20) DEFAULT NULL COMMENT '最大内存',
  `MEM_CREATE_DATE` datetime DEFAULT NULL,
  `MEM_UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`MEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统JVM内存表';

DROP TABLE IF EXISTS `SYS_MENU`;

CREATE TABLE `sys_menu` (
  `MENU_ID` varchar(64) NOT NULL DEFAULT '' COMMENT '菜单ID号_Primary Key',
  `MENU_NAME` varchar(64) NOT NULL COMMENT '菜单名称',
  `MENU_NAME_EN` varchar(64) DEFAULT NULL COMMENT '英文菜单名',
  `MENU_URL` varchar(256) DEFAULT NULL COMMENT '菜单连接地址',
  `MENU_PARENT_ID` varchar(64) DEFAULT NULL COMMENT '父菜单ID',
  `MENU_ORDER` int(10) NOT NULL DEFAULT '1' COMMENT '菜单顺序',
  `MENU_REMARK` varchar(1024) DEFAULT NULL COMMENT '菜单备注',
  `MENU_STATE` varchar(64) NOT NULL DEFAULT 'ON' COMMENT '菜单状态（ON 开启 ,OFF 注销）',
  `MENU_TYPE` varchar(10) DEFAULT 'DATA' COMMENT '菜单类型（DIR 目录，DATA 数据）',
  `MENU_ICON_CLASS` varchar(256) DEFAULT NULL COMMENT '菜单图标CSS名称',
  PRIMARY KEY (`MENU_ID`),
  KEY `MENU_PARENT_ID` (`MENU_PARENT_ID`),
  CONSTRAINT `sys_menu_ibfk_1` FOREIGN KEY (`MENU_PARENT_ID`) REFERENCES `sys_menu` (`MENU_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_NOTICE`;

CREATE TABLE `sys_notice` (
  `NOTICE_ID` varchar(64) NOT NULL COMMENT '消息编号',
  `NOTICE_TITLE` varchar(128) DEFAULT NULL COMMENT '消息标题',
  `NOTICE_CONTENT` varchar(20000) DEFAULT NULL COMMENT '消息内容',
  `NOTICE_MANAGER` varchar(128) DEFAULT NULL COMMENT '消息发布者',
  `NOTICE_IS_NEW` varchar(64) DEFAULT '0' COMMENT '是否是新消息',
  `NOTICE_CREATE_DATE` datetime(6) DEFAULT NULL COMMENT '消息创建时间',
  `NOTICE_UPDATE_DATE` datetime(6) DEFAULT NULL COMMENT '消息修改时间',
  `NOTICE_RECEIVER` varchar(128) DEFAULT NULL COMMENT '消息接收者',
  PRIMARY KEY (`NOTICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户提示消息';

DROP TABLE IF EXISTS `SYS_PERMISSION`;

CREATE TABLE `sys_permission` (
  `per_id` varchar(64) NOT NULL COMMENT '权限ID',
  `per_name` varchar(64) NOT NULL COMMENT '权限名称',
  `PER_NAME_EN` varchar(64) DEFAULT NULL COMMENT '权限英文名',
  `per_type` varchar(30) NOT NULL COMMENT '权限类型（DIR 目录，DATA 数据）',
  `per_url` varchar(500) DEFAULT NULL COMMENT '权限URL',
  `per_method` varchar(32) DEFAULT NULL COMMENT '权限访问方法，例如:get、post等',
  `per_parent_id` varchar(64) DEFAULT NULL COMMENT '父节点ID',
  `per_order` int(10) NOT NULL COMMENT '排序',
  `per_remark` varchar(300) DEFAULT NULL COMMENT '备注信息',
  `per_state` varchar(10) NOT NULL COMMENT '状态信息(ON，启用，OFF 注销）',
  `per_system` varchar(32) DEFAULT NULL COMMENT '所属系统：前端-0/后端-1',
  `per_module` varchar(32) DEFAULT NULL COMMENT '模块，一般是全大写表名，例如：SYS_DICTIONARY',
  PRIMARY KEY (`per_id`),
  KEY `per_parent_id` (`per_parent_id`),
  CONSTRAINT `sys_permission_ibfk_1` FOREIGN KEY (`per_parent_id`) REFERENCES `sys_permission` (`per_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_ROLE`;

CREATE TABLE `sys_role` (
  `ROLE_ID` varchar(64) NOT NULL DEFAULT '' COMMENT '角色ID',
  `ROLE_NAME` varchar(64) DEFAULT NULL COMMENT '角色名称',
  `ROLE_NAME_EN` varchar(64) DEFAULT NULL,
  `ROLE_STATE` varchar(64) DEFAULT NULL COMMENT '状态（ON 正常 ,OFF  注销）',
  `ROLE_SYS` varchar(64) DEFAULT NULL COMMENT '角色所属系统',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_ROLE_DICTIONARY`;

CREATE TABLE `sys_role_dictionary` (
  `rd_id` varchar(64) NOT NULL COMMENT '角色字典ID',
  `rd_role_id` varchar(64) NOT NULL COMMENT '角色ID',
  `rd_dictionary_id` varchar(64) NOT NULL COMMENT '字典ID',
  PRIMARY KEY (`rd_id`),
  KEY `rd_role_id` (`rd_role_id`) USING BTREE,
  KEY `rd_dictionary_id` (`rd_dictionary_id`) USING BTREE,
  CONSTRAINT `sys_role_dictionary_ibfk_1` FOREIGN KEY (`rd_role_id`) REFERENCES `sys_role` (`ROLE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_ROLE_MENU`;

CREATE TABLE `sys_role_menu` (
  `rm_id` varchar(64) NOT NULL COMMENT '角色菜单ID',
  `rm_role_id` varchar(64) NOT NULL COMMENT '角色ID',
  `rm_menu_id` varchar(64) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`rm_id`),
  KEY `rm_role_id` (`rm_role_id`),
  KEY `rm_menu_id` (`rm_menu_id`),
  CONSTRAINT `sys_role_menu_ibfk_1` FOREIGN KEY (`rm_role_id`) REFERENCES `sys_role` (`ROLE_ID`) ON DELETE CASCADE,
  CONSTRAINT `sys_role_menu_ibfk_2` FOREIGN KEY (`rm_menu_id`) REFERENCES `sys_menu` (`MENU_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_ROLE_PERMISSION`;

CREATE TABLE `sys_role_permission` (
  `rp_id` varchar(64) NOT NULL COMMENT '角色权限表Id',
  `rp_role_id` varchar(64) NOT NULL COMMENT '角色ID号',
  `rp_permission_id` varchar(64) NOT NULL COMMENT '权限ID',
  PRIMARY KEY (`rp_id`),
  KEY `rp_role_id` (`rp_role_id`),
  KEY `rp_permission_id` (`rp_permission_id`),
  CONSTRAINT `sys_role_permission_ibfk_1` FOREIGN KEY (`rp_role_id`) REFERENCES `sys_role` (`ROLE_ID`) ON DELETE CASCADE,
  CONSTRAINT `sys_role_permission_ibfk_2` FOREIGN KEY (`rp_permission_id`) REFERENCES `sys_permission` (`per_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_TIP`;

CREATE TABLE `sys_tip` (
  `TIP_ID` varchar(32) NOT NULL COMMENT '提示信息ID',
  `TIP_COUNT` int(11) DEFAULT NULL COMMENT '提示次数',
  `TIP_CONTENTS` varchar(512) DEFAULT NULL COMMENT '提示信息内容',
  `TIP_MANAGER` varchar(255) DEFAULT 'system' COMMENT '所属者，默认为系统拥有',
  `TIP_AUDIENCE` varchar(255) DEFAULT NULL COMMENT '消息受众',
  `TIP_CREATE_DATE` datetime DEFAULT NULL COMMENT '创建日期',
  `TIP_UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`TIP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_UPDATE`;

CREATE TABLE `sys_update` (
  `UPDATE_ID` varchar(36) NOT NULL DEFAULT '1' COMMENT '系统升级日志主键ID',
  `UPDATE_VERSION` varchar(36) DEFAULT NULL COMMENT '升级版本号',
  `UPDATE_CONTENT` varchar(4986) DEFAULT NULL COMMENT '升级内容',
  `UPDATE_MANAGER` varchar(36) DEFAULT NULL COMMENT '升级负责人',
  `UPDATE_CREATE_TIME` datetime DEFAULT NULL COMMENT '升级日期',
  PRIMARY KEY (`UPDATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_USER`;

CREATE TABLE `sys_user` (
  `USER_ID` varchar(64) NOT NULL COMMENT '用户ID_Primary Key',
  `USER_LOGIN_NAME` varchar(64) NOT NULL COMMENT '用户登录名称',
  `USER_PASSWORD` varchar(64) NOT NULL COMMENT '用户密码',
  `USER_PASSWORD_PRIVATE` varchar(64) DEFAULT NULL,
  `USER_NAME_ABBR` varchar(64) DEFAULT NULL COMMENT '姓名缩写',
  `USER_NAME` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `USER_NAME_EN` varchar(64) DEFAULT NULL COMMENT '英文名',
  `USER_SEX` varchar(64) DEFAULT NULL COMMENT '用户的性别',
  `USER_PHONE` varchar(64) DEFAULT NULL COMMENT '用户手机号',
  `USER_EMAIL` varchar(64) DEFAULT NULL COMMENT '用户的邮箱',
  `USER_IMAGE_URL` varchar(500) DEFAULT NULL COMMENT '用户头像路径',
  `USER_ORDER` int(11) DEFAULT '1' COMMENT '用户排序',
  `USER_STATE` varchar(10) NOT NULL COMMENT '状态（ON 正常, OFF 注销）',
  `USER_CODE` varchar(30) NOT NULL COMMENT '用户代码',
  `user_create_date` datetime NOT NULL COMMENT '用户创建时间',
  `USER_UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新日期',
  `user_last_login_time` datetime DEFAULT NULL COMMENT '用户最后登录时间',
  `user_last_login_ip` varchar(40) DEFAULT NULL COMMENT '用户最后登录Ip',
  `USER_UNIT` varchar(64) DEFAULT NULL COMMENT '用户所属单位',
  `USER_JOB_TITLE` varchar(128) DEFAULT NULL COMMENT '用户职务/职称',
  `USER_ADDR` varchar(256) DEFAULT NULL COMMENT '用户通讯地址',
  `USER_POSTCODE` varchar(32) DEFAULT NULL COMMENT '邮编',
  `USER_SAM_SPECIES` longtext COMMENT '用户绑定种属',
  `USER_CURR_SPECIES` varchar(32) DEFAULT NULL COMMENT '当前种属',
  PRIMARY KEY (`USER_ID`),
  KEY `USER_LOGIN_NAME` (`USER_LOGIN_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_USER_ROLE`;

CREATE TABLE `sys_user_role` (
  `ur_id` varchar(64) NOT NULL COMMENT '用户角色ID',
  `ur_role_id` varchar(64) NOT NULL COMMENT '角色ID',
  `ur_user_login_name` varchar(64) NOT NULL COMMENT '用户登录名称',
  PRIMARY KEY (`ur_id`),
  KEY `ur_role_id` (`ur_role_id`),
  KEY `ur_user_login_name` (`ur_user_login_name`),
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`ur_role_id`) REFERENCES `sys_role` (`ROLE_ID`) ON DELETE CASCADE,
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`ur_user_login_name`) REFERENCES `sys_user` (`USER_LOGIN_NAME`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_USER_TOP_UP`;

CREATE TABLE `sys_user_top_up` (
  `utu_id` varchar(255) NOT NULL COMMENT 'id',
  `utu_login_name` varchar(255) NOT NULL COMMENT '用户登录名',
  `utu_serial_number` varchar(255) NOT NULL COMMENT '序列号',
  `utu_billing_start_date` datetime DEFAULT NULL COMMENT '计费开始时间',
  `utu_billing_end_date` datetime DEFAULT NULL COMMENT '计费结束时间',
  `utu_state` varchar(255) DEFAULT NULL COMMENT '状态(0-已充值,1-已使用,2-已过期）',
  `utu_create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`utu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户充值信息表';