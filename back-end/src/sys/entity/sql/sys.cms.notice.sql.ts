import { SysCmsNoticeQDto } from '../../dto/query/sys.cms.notice.qdto';
import { SysCmsNoticeCDto } from '../../dto/create/sys.cms.notice.cdto';
import { SysCmsNoticeUDto } from '../../dto/update/sys.cms.notice.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysCmsNoticeColNameEnum, SysCmsNoticeColPropEnum, SysCmsNoticeTable, SysCmsNoticeColNames, SysCmsNoticeColProps } from '../ddl/sys.cms.notice.cols';
/**
 * SYS_CMS_NOTICE--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysCmsNoticeSQL
 */
export const SysCmsNoticeSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let cols = SysCmsNoticeSQL.COL_MAPPER_SQL(dto);
    let where = SysCmsNoticeSQL.WHERE_SQL(dto);
    let group = SysCmsNoticeSQL.GROUP_BY_SQL(dto);
    let order = SysCmsNoticeSQL.ORDER_BY_SQL(dto);
    let limit = SysCmsNoticeSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let where = SysCmsNoticeSQL.WHERE_SQL(dto);
    let group = SysCmsNoticeSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let cols = SysCmsNoticeSQL.COL_MAPPER_SQL(dto);
    let group = SysCmsNoticeSQL.GROUP_BY_SQL(dto);
    let order = SysCmsNoticeSQL.ORDER_BY_SQL(dto);
    let limit = SysCmsNoticeSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysCmsNoticeSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysCmsNoticeSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysCmsNoticeSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let where = SysCmsNoticeSQL.WHERE_SQL(dto);
    let group = SysCmsNoticeSQL.GROUP_BY_SQL(dto);
    let order = SysCmsNoticeSQL.ORDER_BY_SQL(dto);
    let limit = SysCmsNoticeSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysCmsNoticeColNames, SysCmsNoticeColProps);
    if (!cols || cols.length == 0) cols = SysCmsNoticeColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[scNoticeId]?.length > 0) items.push(` ${SC_NOTICE_ID} = '${dto[scNoticeId]}'`);
    if (dto['nscNoticeId']?.length > 0) items.push(` ${SC_NOTICE_ID} != '${dto['nscNoticeId']}'`);
    if (dto[scNoticeTitle]?.length > 0) items.push(` ${SC_NOTICE_TITLE} = '${dto[scNoticeTitle]}'`);
    if (dto['nscNoticeTitle']?.length > 0) items.push(` ${SC_NOTICE_TITLE} != '${dto['nscNoticeTitle']}'`);
    if (dto[scNoticeContent]?.length > 0) items.push(` ${SC_NOTICE_CONTENT} = '${dto[scNoticeContent]}'`);
    if (dto['nscNoticeContent']?.length > 0) items.push(` ${SC_NOTICE_CONTENT} != '${dto['nscNoticeContent']}'`);
    if (dto[scNoticeIsNew]?.length > 0) items.push(` ${SC_NOTICE_IS_NEW} = '${dto[scNoticeIsNew]}'`);
    if (dto['nscNoticeIsNew']?.length > 0) items.push(` ${SC_NOTICE_IS_NEW} != '${dto['nscNoticeIsNew']}'`);
    if (dto[scNoticeManager]?.length > 0) items.push(` ${SC_NOTICE_MANAGER} = '${dto[scNoticeManager]}'`);
    if (dto['nscNoticeManager']?.length > 0) items.push(` ${SC_NOTICE_MANAGER} != '${dto['nscNoticeManager']}'`);
    if (dto[scNoticeCreateDate]) items.push(`${SC_NOTICE_CREATE_DATE} is not null and date_format(${SC_NOTICE_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[scNoticeCreateDate])}`);
    if (dto['nscNoticeCreateDate']) items.push(`${SC_NOTICE_CREATE_DATE} is not null and date_format(${SC_NOTICE_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nscNoticeCreateDate'])}`);
    if (dto[scNoticeUpdateDate]) items.push(`${SC_NOTICE_UPDATE_DATE} is not null and date_format(${SC_NOTICE_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[scNoticeUpdateDate])}`);
    if (dto['nscNoticeUpdateDate']) items.push(`${SC_NOTICE_UPDATE_DATE} is not null and date_format(${SC_NOTICE_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nscNoticeUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //SC_NOTICE_ID--字符串字段
    if (dto['scNoticeIdList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_ID, <string[]>dto['scNoticeIdList']));
    if (dto['nscNoticeIdList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_ID, <string[]>dto['nscNoticeIdList'], true));
    if (dto['scNoticeIdLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_ID, dto['scNoticeIdLike'])); //For MysSQL
    if (dto['nscNoticeIdLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_ID, dto['nscNoticeIdLike'], true)); //For MysSQL
    if (dto['scNoticeIdLikeList']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_LIST_SQL(SC_NOTICE_ID, <string[]>dto['scNoticeIdLikeList'])); //For MysSQL
    //SC_NOTICE_TITLE--字符串字段
    if (dto['scNoticeTitleList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_TITLE, <string[]>dto['scNoticeTitleList']));
    if (dto['nscNoticeTitleList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_TITLE, <string[]>dto['nscNoticeTitleList'], true));
    if (dto['scNoticeTitleLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_TITLE, dto['scNoticeTitleLike'])); //For MysSQL
    if (dto['nscNoticeTitleLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_TITLE, dto['nscNoticeTitleLike'], true)); //For MysSQL
    if (dto['scNoticeTitleLikeList']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_LIST_SQL(SC_NOTICE_TITLE, <string[]>dto['scNoticeTitleLikeList'])); //For MysSQL
    //SC_NOTICE_CONTENT--字符串字段
    if (dto['scNoticeContentList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_CONTENT, <string[]>dto['scNoticeContentList']));
    if (dto['nscNoticeContentList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_CONTENT, <string[]>dto['nscNoticeContentList'], true));
    if (dto['scNoticeContentLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_CONTENT, dto['scNoticeContentLike'])); //For MysSQL
    if (dto['nscNoticeContentLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_CONTENT, dto['nscNoticeContentLike'], true)); //For MysSQL
    if (dto['scNoticeContentLikeList']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_LIST_SQL(SC_NOTICE_CONTENT, <string[]>dto['scNoticeContentLikeList'])); //For MysSQL
    //SC_NOTICE_IS_NEW--字符串字段
    if (dto['scNoticeIsNewList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_IS_NEW, <string[]>dto['scNoticeIsNewList']));
    if (dto['nscNoticeIsNewList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_IS_NEW, <string[]>dto['nscNoticeIsNewList'], true));
    if (dto['scNoticeIsNewLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_IS_NEW, dto['scNoticeIsNewLike'])); //For MysSQL
    if (dto['nscNoticeIsNewLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_IS_NEW, dto['nscNoticeIsNewLike'], true)); //For MysSQL
    if (dto['scNoticeIsNewLikeList']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_LIST_SQL(SC_NOTICE_IS_NEW, <string[]>dto['scNoticeIsNewLikeList'])); //For MysSQL
    //SC_NOTICE_MANAGER--字符串字段
    if (dto['scNoticeManagerList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_MANAGER, <string[]>dto['scNoticeManagerList']));
    if (dto['nscNoticeManagerList']?.length > 0) items.push(SysCmsNoticeSQL.IN_SQL(SC_NOTICE_MANAGER, <string[]>dto['nscNoticeManagerList'], true));
    if (dto['scNoticeManagerLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_MANAGER, dto['scNoticeManagerLike'])); //For MysSQL
    if (dto['nscNoticeManagerLike']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_SQL(SC_NOTICE_MANAGER, dto['nscNoticeManagerLike'], true)); //For MysSQL
    if (dto['scNoticeManagerLikeList']?.length > 0) items.push(SysCmsNoticeSQL.LIKE_LIST_SQL(SC_NOTICE_MANAGER, <string[]>dto['scNoticeManagerLikeList'])); //For MysSQL
    //SC_NOTICE_CREATE_DATE--日期字段
    if (dto[scNoticeCreateDate]) items.push(`${SC_NOTICE_CREATE_DATE} is not null and date_format(${SC_NOTICE_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sScNoticeCreateDate'])}`);
    if (dto[scNoticeCreateDate]) items.push(`${SC_NOTICE_CREATE_DATE} is not null and date_format(${SC_NOTICE_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eScNoticeCreateDate'])}`);
    //SC_NOTICE_UPDATE_DATE--日期字段
    if (dto[scNoticeUpdateDate]) items.push(`${SC_NOTICE_UPDATE_DATE} is not null and date_format(${SC_NOTICE_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sScNoticeUpdateDate'])}`);
    if (dto[scNoticeUpdateDate]) items.push(`${SC_NOTICE_UPDATE_DATE} is not null and date_format(${SC_NOTICE_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eScNoticeUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysCmsNoticeSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysCmsNoticeCDto | Partial<SysCmsNoticeCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(SC_NOTICE_ID);
    cols.push(SC_NOTICE_TITLE);
    cols.push(SC_NOTICE_CONTENT);
    cols.push(SC_NOTICE_IS_NEW);
    cols.push(SC_NOTICE_MANAGER);
    cols.push(SC_NOTICE_CREATE_DATE);
    cols.push(SC_NOTICE_UPDATE_DATE);
    values.push(toSqlValue(dto[scNoticeId]));
    values.push(toSqlValue(dto[scNoticeTitle]));
    values.push(toSqlValue(dto[scNoticeContent]));
    values.push(toSqlValue(dto[scNoticeIsNew]));
    values.push(toSqlValue(dto[scNoticeManager]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysCmsNoticeCDto[] | Partial<SysCmsNoticeCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(SC_NOTICE_ID);
    cols.push(SC_NOTICE_TITLE);
    cols.push(SC_NOTICE_CONTENT);
    cols.push(SC_NOTICE_IS_NEW);
    cols.push(SC_NOTICE_MANAGER);
    cols.push(SC_NOTICE_CREATE_DATE);
    cols.push(SC_NOTICE_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[scNoticeId]));
      values.push(toSqlValue(dto[scNoticeTitle]));
      values.push(toSqlValue(dto[scNoticeContent]));
      values.push(toSqlValue(dto[scNoticeIsNew]));
      values.push(toSqlValue(dto[scNoticeManager]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysCmsNoticeUDto | Partial<SysCmsNoticeUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysCmsNoticeColNames, SysCmsNoticeColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${SC_NOTICE_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${SC_NOTICE_TITLE} = ${toSqlValue(dto[scNoticeTitle])}`);
    items.push(`${SC_NOTICE_CONTENT} = ${toSqlValue(dto[scNoticeContent])}`);
    items.push(`${SC_NOTICE_IS_NEW} = ${toSqlValue(dto[scNoticeIsNew])}`);
    items.push(`${SC_NOTICE_MANAGER} = ${toSqlValue(dto[scNoticeManager])}`);
    items.push(`${SC_NOTICE_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysCmsNoticeUDto | Partial<SysCmsNoticeUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysCmsNoticeColNames, SysCmsNoticeColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${SC_NOTICE_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysCmsNoticeSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${SC_NOTICE_TITLE} = ${toSqlValue(dto[scNoticeTitle])}`);
    items.push(`${SC_NOTICE_CONTENT} = ${toSqlValue(dto[scNoticeContent])}`);
    items.push(`${SC_NOTICE_IS_NEW} = ${toSqlValue(dto[scNoticeIsNew])}`);
    items.push(`${SC_NOTICE_MANAGER} = ${toSqlValue(dto[scNoticeManager])}`);
    items.push(`${SC_NOTICE_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysCmsNoticeSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysCmsNoticeUDto[] | Partial<SysCmsNoticeUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysCmsNoticeSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysCmsNoticeUDto | Partial<SysCmsNoticeUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[scNoticeTitle]?.length > 0) items.push(`${SC_NOTICE_TITLE} = ${toSqlValue(dto[scNoticeTitle])}`);
    if (dto[scNoticeContent]?.length > 0) items.push(`${SC_NOTICE_CONTENT} = ${toSqlValue(dto[scNoticeContent])}`);
    if (dto[scNoticeIsNew]?.length > 0) items.push(`${SC_NOTICE_IS_NEW} = ${toSqlValue(dto[scNoticeIsNew])}`);
    if (dto[scNoticeManager]?.length > 0) items.push(`${SC_NOTICE_MANAGER} = ${toSqlValue(dto[scNoticeManager])}`);
    if (dto[scNoticeUpdateDate]) items.push(`${SC_NOTICE_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysCmsNoticeUDto[] | Partial<SysCmsNoticeUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysCmsNoticeSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): string => {
    if (!dto) return '';
    let where = SysCmsNoticeSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysCmsNoticeSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_CMS_NOTICE';
/**
 * 列名常量字段
 */
const SC_NOTICE_ID = SysCmsNoticeColNameEnum.SC_NOTICE_ID;
const SC_NOTICE_TITLE = SysCmsNoticeColNameEnum.SC_NOTICE_TITLE;
const SC_NOTICE_CONTENT = SysCmsNoticeColNameEnum.SC_NOTICE_CONTENT;
const SC_NOTICE_IS_NEW = SysCmsNoticeColNameEnum.SC_NOTICE_IS_NEW;
const SC_NOTICE_MANAGER = SysCmsNoticeColNameEnum.SC_NOTICE_MANAGER;
const SC_NOTICE_CREATE_DATE = SysCmsNoticeColNameEnum.SC_NOTICE_CREATE_DATE;
const SC_NOTICE_UPDATE_DATE = SysCmsNoticeColNameEnum.SC_NOTICE_UPDATE_DATE;
/**
 * 实体类属性名
 */
const scNoticeId = SysCmsNoticeColPropEnum.scNoticeId;
const scNoticeTitle = SysCmsNoticeColPropEnum.scNoticeTitle;
const scNoticeContent = SysCmsNoticeColPropEnum.scNoticeContent;
const scNoticeIsNew = SysCmsNoticeColPropEnum.scNoticeIsNew;
const scNoticeManager = SysCmsNoticeColPropEnum.scNoticeManager;
const scNoticeCreateDate = SysCmsNoticeColPropEnum.scNoticeCreateDate;
const scNoticeUpdateDate = SysCmsNoticeColPropEnum.scNoticeUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysCmsNoticeTable.PRIMER_KEY;
const primerKey = SysCmsNoticeTable.primerKey;
