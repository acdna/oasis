import { SysCmsFilesQDto } from '../../dto/query/sys.cms.files.qdto';
import { SysCmsFilesCDto } from '../../dto/create/sys.cms.files.cdto';
import { SysCmsFilesUDto } from '../../dto/update/sys.cms.files.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysCmsFilesColNameEnum, SysCmsFilesColPropEnum, SysCmsFilesTable, SysCmsFilesColNames, SysCmsFilesColProps } from '../ddl/sys.cms.files.cols';
/**
 * SYS_CMS_FILES--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysCmsFilesSQL
 */
export const SysCmsFilesSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let cols = SysCmsFilesSQL.COL_MAPPER_SQL(dto);
    let where = SysCmsFilesSQL.WHERE_SQL(dto);
    let group = SysCmsFilesSQL.GROUP_BY_SQL(dto);
    let order = SysCmsFilesSQL.ORDER_BY_SQL(dto);
    let limit = SysCmsFilesSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let where = SysCmsFilesSQL.WHERE_SQL(dto);
    let group = SysCmsFilesSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let cols = SysCmsFilesSQL.COL_MAPPER_SQL(dto);
    let group = SysCmsFilesSQL.GROUP_BY_SQL(dto);
    let order = SysCmsFilesSQL.ORDER_BY_SQL(dto);
    let limit = SysCmsFilesSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysCmsFilesSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysCmsFilesSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysCmsFilesSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let where = SysCmsFilesSQL.WHERE_SQL(dto);
    let group = SysCmsFilesSQL.GROUP_BY_SQL(dto);
    let order = SysCmsFilesSQL.ORDER_BY_SQL(dto);
    let limit = SysCmsFilesSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysCmsFilesColNames, SysCmsFilesColProps);
    if (!cols || cols.length == 0) cols = SysCmsFilesColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[scFileId]?.length > 0) items.push(` ${SC_FILE_ID} = '${dto[scFileId]}'`);
    if (dto['nscFileId']?.length > 0) items.push(` ${SC_FILE_ID} != '${dto['nscFileId']}'`);
    if (dto[scFileName]?.length > 0) items.push(` ${SC_FILE_NAME} = '${dto[scFileName]}'`);
    if (dto['nscFileName']?.length > 0) items.push(` ${SC_FILE_NAME} != '${dto['nscFileName']}'`);
    if (dto[scFileNameCn]?.length > 0) items.push(` ${SC_FILE_NAME_CN} = '${dto[scFileNameCn]}'`);
    if (dto['nscFileNameCn']?.length > 0) items.push(` ${SC_FILE_NAME_CN} != '${dto['nscFileNameCn']}'`);
    if (dto[scFileType]?.length > 0) items.push(` ${SC_FILE_TYPE} = '${dto[scFileType]}'`);
    if (dto['nscFileType']?.length > 0) items.push(` ${SC_FILE_TYPE} != '${dto['nscFileType']}'`);
    if (dto[scFilePath]?.length > 0) items.push(` ${SC_FILE_PATH} = '${dto[scFilePath]}'`);
    if (dto['nscFilePath']?.length > 0) items.push(` ${SC_FILE_PATH} != '${dto['nscFilePath']}'`);
    if (dto[scFileIsNew]?.length > 0) items.push(` ${SC_FILE_IS_NEW} = '${dto[scFileIsNew]}'`);
    if (dto['nscFileIsNew']?.length > 0) items.push(` ${SC_FILE_IS_NEW} != '${dto['nscFileIsNew']}'`);
    if (dto[scFileManager]?.length > 0) items.push(` ${SC_FILE_MANAGER} = '${dto[scFileManager]}'`);
    if (dto['nscFileManager']?.length > 0) items.push(` ${SC_FILE_MANAGER} != '${dto['nscFileManager']}'`);
    if (dto[scFileCreateDate]) items.push(`${SC_FILE_CREATE_DATE} is not null and date_format(${SC_FILE_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[scFileCreateDate])}`);
    if (dto['nscFileCreateDate']) items.push(`${SC_FILE_CREATE_DATE} is not null and date_format(${SC_FILE_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nscFileCreateDate'])}`);
    if (dto[scFielUpdateDate]) items.push(`${SC_FIEL_UPDATE_DATE} is not null and date_format(${SC_FIEL_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[scFielUpdateDate])}`);
    if (dto['nscFielUpdateDate']) items.push(`${SC_FIEL_UPDATE_DATE} is not null and date_format(${SC_FIEL_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nscFielUpdateDate'])}`);
    if (dto[scFileSpecies]?.length > 0) items.push(` ${SC_FILE_SPECIES} = '${dto[scFileSpecies]}'`);
    if (dto['nscFileSpecies']?.length > 0) items.push(` ${SC_FILE_SPECIES} != '${dto['nscFileSpecies']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //SC_FILE_ID--字符串字段
    if (dto['scFileIdList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_ID, <string[]>dto['scFileIdList']));
    if (dto['nscFileIdList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_ID, <string[]>dto['nscFileIdList'], true));
    if (dto['scFileIdLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_ID, dto['scFileIdLike'])); //For MysSQL
    if (dto['nscFileIdLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_ID, dto['nscFileIdLike'], true)); //For MysSQL
    if (dto['scFileIdLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_ID, <string[]>dto['scFileIdLikeList'])); //For MysSQL
    //SC_FILE_NAME--字符串字段
    if (dto['scFileNameList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_NAME, <string[]>dto['scFileNameList']));
    if (dto['nscFileNameList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_NAME, <string[]>dto['nscFileNameList'], true));
    if (dto['scFileNameLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_NAME, dto['scFileNameLike'])); //For MysSQL
    if (dto['nscFileNameLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_NAME, dto['nscFileNameLike'], true)); //For MysSQL
    if (dto['scFileNameLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_NAME, <string[]>dto['scFileNameLikeList'])); //For MysSQL
    //SC_FILE_NAME_CN--字符串字段
    if (dto['scFileNameCnList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_NAME_CN, <string[]>dto['scFileNameCnList']));
    if (dto['nscFileNameCnList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_NAME_CN, <string[]>dto['nscFileNameCnList'], true));
    if (dto['scFileNameCnLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_NAME_CN, dto['scFileNameCnLike'])); //For MysSQL
    if (dto['nscFileNameCnLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_NAME_CN, dto['nscFileNameCnLike'], true)); //For MysSQL
    if (dto['scFileNameCnLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_NAME_CN, <string[]>dto['scFileNameCnLikeList'])); //For MysSQL
    //SC_FILE_TYPE--字符串字段
    if (dto['scFileTypeList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_TYPE, <string[]>dto['scFileTypeList']));
    if (dto['nscFileTypeList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_TYPE, <string[]>dto['nscFileTypeList'], true));
    if (dto['scFileTypeLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_TYPE, dto['scFileTypeLike'])); //For MysSQL
    if (dto['nscFileTypeLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_TYPE, dto['nscFileTypeLike'], true)); //For MysSQL
    if (dto['scFileTypeLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_TYPE, <string[]>dto['scFileTypeLikeList'])); //For MysSQL
    //SC_FILE_PATH--字符串字段
    if (dto['scFilePathList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_PATH, <string[]>dto['scFilePathList']));
    if (dto['nscFilePathList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_PATH, <string[]>dto['nscFilePathList'], true));
    if (dto['scFilePathLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_PATH, dto['scFilePathLike'])); //For MysSQL
    if (dto['nscFilePathLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_PATH, dto['nscFilePathLike'], true)); //For MysSQL
    if (dto['scFilePathLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_PATH, <string[]>dto['scFilePathLikeList'])); //For MysSQL
    //SC_FILE_IS_NEW--字符串字段
    if (dto['scFileIsNewList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_IS_NEW, <string[]>dto['scFileIsNewList']));
    if (dto['nscFileIsNewList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_IS_NEW, <string[]>dto['nscFileIsNewList'], true));
    if (dto['scFileIsNewLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_IS_NEW, dto['scFileIsNewLike'])); //For MysSQL
    if (dto['nscFileIsNewLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_IS_NEW, dto['nscFileIsNewLike'], true)); //For MysSQL
    if (dto['scFileIsNewLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_IS_NEW, <string[]>dto['scFileIsNewLikeList'])); //For MysSQL
    //SC_FILE_MANAGER--字符串字段
    if (dto['scFileManagerList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_MANAGER, <string[]>dto['scFileManagerList']));
    if (dto['nscFileManagerList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_MANAGER, <string[]>dto['nscFileManagerList'], true));
    if (dto['scFileManagerLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_MANAGER, dto['scFileManagerLike'])); //For MysSQL
    if (dto['nscFileManagerLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_MANAGER, dto['nscFileManagerLike'], true)); //For MysSQL
    if (dto['scFileManagerLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_MANAGER, <string[]>dto['scFileManagerLikeList'])); //For MysSQL
    //SC_FILE_CREATE_DATE--日期字段
    if (dto[scFileCreateDate]) items.push(`${SC_FILE_CREATE_DATE} is not null and date_format(${SC_FILE_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sScFileCreateDate'])}`);
    if (dto[scFileCreateDate]) items.push(`${SC_FILE_CREATE_DATE} is not null and date_format(${SC_FILE_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eScFileCreateDate'])}`);
    //SC_FIEL_UPDATE_DATE--日期字段
    if (dto[scFielUpdateDate]) items.push(`${SC_FIEL_UPDATE_DATE} is not null and date_format(${SC_FIEL_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sScFielUpdateDate'])}`);
    if (dto[scFielUpdateDate]) items.push(`${SC_FIEL_UPDATE_DATE} is not null and date_format(${SC_FIEL_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eScFielUpdateDate'])}`);
    //SC_FILE_SPECIES--字符串字段
    if (dto['scFileSpeciesList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_SPECIES, <string[]>dto['scFileSpeciesList']));
    if (dto['nscFileSpeciesList']?.length > 0) items.push(SysCmsFilesSQL.IN_SQL(SC_FILE_SPECIES, <string[]>dto['nscFileSpeciesList'], true));
    if (dto['scFileSpeciesLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_SPECIES, dto['scFileSpeciesLike'])); //For MysSQL
    if (dto['nscFileSpeciesLike']?.length > 0) items.push(SysCmsFilesSQL.LIKE_SQL(SC_FILE_SPECIES, dto['nscFileSpeciesLike'], true)); //For MysSQL
    if (dto['scFileSpeciesLikeList']?.length > 0) items.push(SysCmsFilesSQL.LIKE_LIST_SQL(SC_FILE_SPECIES, <string[]>dto['scFileSpeciesLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysCmsFilesSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysCmsFilesCDto | Partial<SysCmsFilesCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(SC_FILE_ID);
    cols.push(SC_FILE_NAME);
    cols.push(SC_FILE_NAME_CN);
    cols.push(SC_FILE_TYPE);
    cols.push(SC_FILE_PATH);
    cols.push(SC_FILE_IS_NEW);
    cols.push(SC_FILE_MANAGER);
    cols.push(SC_FILE_CREATE_DATE);
    cols.push(SC_FIEL_UPDATE_DATE);
    cols.push(SC_FILE_SPECIES);
    values.push(toSqlValue(dto[scFileId]));
    values.push(toSqlValue(dto[scFileName]));
    values.push(toSqlValue(dto[scFileNameCn]));
    values.push(toSqlValue(dto[scFileType]));
    values.push(toSqlValue(dto[scFilePath]));
    values.push(toSqlValue(dto[scFileIsNew]));
    values.push(toSqlValue(dto[scFileManager]));
    values.push('NOW()');
    values.push('NOW()');
    values.push(toSqlValue(dto[scFileSpecies]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysCmsFilesCDto[] | Partial<SysCmsFilesCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(SC_FILE_ID);
    cols.push(SC_FILE_NAME);
    cols.push(SC_FILE_NAME_CN);
    cols.push(SC_FILE_TYPE);
    cols.push(SC_FILE_PATH);
    cols.push(SC_FILE_IS_NEW);
    cols.push(SC_FILE_MANAGER);
    cols.push(SC_FILE_CREATE_DATE);
    cols.push(SC_FIEL_UPDATE_DATE);
    cols.push(SC_FILE_SPECIES);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[scFileId]));
      values.push(toSqlValue(dto[scFileName]));
      values.push(toSqlValue(dto[scFileNameCn]));
      values.push(toSqlValue(dto[scFileType]));
      values.push(toSqlValue(dto[scFilePath]));
      values.push(toSqlValue(dto[scFileIsNew]));
      values.push(toSqlValue(dto[scFileManager]));
      values.push('NOW()');
      values.push('NOW()');
      values.push(toSqlValue(dto[scFileSpecies]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysCmsFilesUDto | Partial<SysCmsFilesUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysCmsFilesColNames, SysCmsFilesColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${SC_FIEL_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${SC_FILE_NAME} = ${toSqlValue(dto[scFileName])}`);
    items.push(`${SC_FILE_NAME_CN} = ${toSqlValue(dto[scFileNameCn])}`);
    items.push(`${SC_FILE_TYPE} = ${toSqlValue(dto[scFileType])}`);
    items.push(`${SC_FILE_PATH} = ${toSqlValue(dto[scFilePath])}`);
    items.push(`${SC_FILE_IS_NEW} = ${toSqlValue(dto[scFileIsNew])}`);
    items.push(`${SC_FILE_MANAGER} = ${toSqlValue(dto[scFileManager])}`);
    items.push(`${SC_FIEL_UPDATE_DATE} = NOW()`);
    items.push(`${SC_FILE_SPECIES} = ${toSqlValue(dto[scFileSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysCmsFilesUDto | Partial<SysCmsFilesUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysCmsFilesColNames, SysCmsFilesColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${SC_FIEL_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysCmsFilesSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${SC_FILE_NAME} = ${toSqlValue(dto[scFileName])}`);
    items.push(`${SC_FILE_NAME_CN} = ${toSqlValue(dto[scFileNameCn])}`);
    items.push(`${SC_FILE_TYPE} = ${toSqlValue(dto[scFileType])}`);
    items.push(`${SC_FILE_PATH} = ${toSqlValue(dto[scFilePath])}`);
    items.push(`${SC_FILE_IS_NEW} = ${toSqlValue(dto[scFileIsNew])}`);
    items.push(`${SC_FILE_MANAGER} = ${toSqlValue(dto[scFileManager])}`);
    items.push(`${SC_FIEL_UPDATE_DATE} = NOW()`);
    items.push(`${SC_FILE_SPECIES} = ${toSqlValue(dto[scFileSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysCmsFilesSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysCmsFilesUDto[] | Partial<SysCmsFilesUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysCmsFilesSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysCmsFilesUDto | Partial<SysCmsFilesUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[scFileName]?.length > 0) items.push(`${SC_FILE_NAME} = ${toSqlValue(dto[scFileName])}`);
    if (dto[scFileNameCn]?.length > 0) items.push(`${SC_FILE_NAME_CN} = ${toSqlValue(dto[scFileNameCn])}`);
    if (dto[scFileType]?.length > 0) items.push(`${SC_FILE_TYPE} = ${toSqlValue(dto[scFileType])}`);
    if (dto[scFilePath]?.length > 0) items.push(`${SC_FILE_PATH} = ${toSqlValue(dto[scFilePath])}`);
    if (dto[scFileIsNew]?.length > 0) items.push(`${SC_FILE_IS_NEW} = ${toSqlValue(dto[scFileIsNew])}`);
    if (dto[scFileManager]?.length > 0) items.push(`${SC_FILE_MANAGER} = ${toSqlValue(dto[scFileManager])}`);
    if (dto[scFielUpdateDate]) items.push(`${SC_FIEL_UPDATE_DATE}= NOW()`);
    if (dto[scFileSpecies]?.length > 0) items.push(`${SC_FILE_SPECIES} = ${toSqlValue(dto[scFileSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysCmsFilesUDto[] | Partial<SysCmsFilesUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysCmsFilesSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): string => {
    if (!dto) return '';
    let where = SysCmsFilesSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysCmsFilesSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_CMS_FILES';
/**
 * 列名常量字段
 */
const SC_FILE_ID = SysCmsFilesColNameEnum.SC_FILE_ID;
const SC_FILE_NAME = SysCmsFilesColNameEnum.SC_FILE_NAME;
const SC_FILE_NAME_CN = SysCmsFilesColNameEnum.SC_FILE_NAME_CN;
const SC_FILE_TYPE = SysCmsFilesColNameEnum.SC_FILE_TYPE;
const SC_FILE_PATH = SysCmsFilesColNameEnum.SC_FILE_PATH;
const SC_FILE_IS_NEW = SysCmsFilesColNameEnum.SC_FILE_IS_NEW;
const SC_FILE_MANAGER = SysCmsFilesColNameEnum.SC_FILE_MANAGER;
const SC_FILE_CREATE_DATE = SysCmsFilesColNameEnum.SC_FILE_CREATE_DATE;
const SC_FIEL_UPDATE_DATE = SysCmsFilesColNameEnum.SC_FIEL_UPDATE_DATE;
const SC_FILE_SPECIES = SysCmsFilesColNameEnum.SC_FILE_SPECIES;
/**
 * 实体类属性名
 */
const scFileId = SysCmsFilesColPropEnum.scFileId;
const scFileName = SysCmsFilesColPropEnum.scFileName;
const scFileNameCn = SysCmsFilesColPropEnum.scFileNameCn;
const scFileType = SysCmsFilesColPropEnum.scFileType;
const scFilePath = SysCmsFilesColPropEnum.scFilePath;
const scFileIsNew = SysCmsFilesColPropEnum.scFileIsNew;
const scFileManager = SysCmsFilesColPropEnum.scFileManager;
const scFileCreateDate = SysCmsFilesColPropEnum.scFileCreateDate;
const scFielUpdateDate = SysCmsFilesColPropEnum.scFielUpdateDate;
const scFileSpecies = SysCmsFilesColPropEnum.scFileSpecies;
/**
 * 主键信息
 */
const PRIMER_KEY = SysCmsFilesTable.PRIMER_KEY;
const primerKey = SysCmsFilesTable.primerKey;
