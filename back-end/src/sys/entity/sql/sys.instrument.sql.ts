import { SysInstrumentQDto } from '../../dto/query/sys.instrument.qdto';
import { SysInstrumentCDto } from '../../dto/create/sys.instrument.cdto';
import { SysInstrumentUDto } from '../../dto/update/sys.instrument.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysInstrumentColNameEnum, SysInstrumentColPropEnum, SysInstrumentTable, SysInstrumentColNames, SysInstrumentColProps } from '../ddl/sys.instrument.cols';
/**
 * SYS_INSTRUMENT--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysInstrumentSQL
 */
export const SysInstrumentSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let cols = SysInstrumentSQL.COL_MAPPER_SQL(dto);
    let where = SysInstrumentSQL.WHERE_SQL(dto);
    let group = SysInstrumentSQL.GROUP_BY_SQL(dto);
    let order = SysInstrumentSQL.ORDER_BY_SQL(dto);
    let limit = SysInstrumentSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let where = SysInstrumentSQL.WHERE_SQL(dto);
    let group = SysInstrumentSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let cols = SysInstrumentSQL.COL_MAPPER_SQL(dto);
    let group = SysInstrumentSQL.GROUP_BY_SQL(dto);
    let order = SysInstrumentSQL.ORDER_BY_SQL(dto);
    let limit = SysInstrumentSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysInstrumentSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysInstrumentSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysInstrumentSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let where = SysInstrumentSQL.WHERE_SQL(dto);
    let group = SysInstrumentSQL.GROUP_BY_SQL(dto);
    let order = SysInstrumentSQL.ORDER_BY_SQL(dto);
    let limit = SysInstrumentSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysInstrumentColNames, SysInstrumentColProps);
    if (!cols || cols.length == 0) cols = SysInstrumentColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[insId]?.length > 0) items.push(` ${INS_ID} = '${dto[insId]}'`);
    if (dto['ninsId']?.length > 0) items.push(` ${INS_ID} != '${dto['ninsId']}'`);
    if (dto[insBarcode]?.length > 0) items.push(` ${INS_BARCODE} = '${dto[insBarcode]}'`);
    if (dto['ninsBarcode']?.length > 0) items.push(` ${INS_BARCODE} != '${dto['ninsBarcode']}'`);
    if (dto[insName]?.length > 0) items.push(` ${INS_NAME} = '${dto[insName]}'`);
    if (dto['ninsName']?.length > 0) items.push(` ${INS_NAME} != '${dto['ninsName']}'`);
    if (dto[insModel]?.length > 0) items.push(` ${INS_MODEL} = '${dto[insModel]}'`);
    if (dto['ninsModel']?.length > 0) items.push(` ${INS_MODEL} != '${dto['ninsModel']}'`);
    if (dto[insManager]?.length > 0) items.push(` ${INS_MANAGER} = '${dto[insManager]}'`);
    if (dto['ninsManager']?.length > 0) items.push(` ${INS_MANAGER} != '${dto['ninsManager']}'`);
    if (dto[insType]?.length > 0) items.push(` ${INS_TYPE} = '${dto[insType]}'`);
    if (dto['ninsType']?.length > 0) items.push(` ${INS_TYPE} != '${dto['ninsType']}'`);
    if (dto[insRemark]?.length > 0) items.push(` ${INS_REMARK} = '${dto[insRemark]}'`);
    if (dto['ninsRemark']?.length > 0) items.push(` ${INS_REMARK} != '${dto['ninsRemark']}'`);
    if (dto[insBuyDate]) items.push(`${INS_BUY_DATE} is not null and date_format(${INS_BUY_DATE},'%Y-%m-%d') = ${toSqlValue(dto[insBuyDate])}`);
    if (dto['ninsBuyDate']) items.push(`${INS_BUY_DATE} is not null and date_format(${INS_BUY_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ninsBuyDate'])}`);
    if (dto[insMaintainPeriod]?.length > 0) items.push(` ${INS_MAINTAIN_PERIOD} = '${dto[insMaintainPeriod]}'`);
    if (dto['ninsMaintainPeriod']?.length > 0) items.push(` ${INS_MAINTAIN_PERIOD} != '${dto['ninsMaintainPeriod']}'`);
    if (dto[insMaker]?.length > 0) items.push(` ${INS_MAKER} = '${dto[insMaker]}'`);
    if (dto['ninsMaker']?.length > 0) items.push(` ${INS_MAKER} != '${dto['ninsMaker']}'`);
    if (dto[insPrice]?.length > 0) items.push(` ${INS_PRICE} = '${dto[insPrice]}'`);
    if (dto['ninsPrice']?.length > 0) items.push(` ${INS_PRICE} != '${dto['ninsPrice']}'`);
    if (dto[insExtraProvide]?.length > 0) items.push(` ${INS_EXTRA_PROVIDE} = '${dto[insExtraProvide]}'`);
    if (dto['ninsExtraProvide']?.length > 0) items.push(` ${INS_EXTRA_PROVIDE} != '${dto['ninsExtraProvide']}'`);
    if (dto[insCreateDate]) items.push(`${INS_CREATE_DATE} is not null and date_format(${INS_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[insCreateDate])}`);
    if (dto['ninsCreateDate']) items.push(`${INS_CREATE_DATE} is not null and date_format(${INS_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ninsCreateDate'])}`);
    if (dto[insUpdateDate]) items.push(`${INS_UPDATE_DATE} is not null and date_format(${INS_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[insUpdateDate])}`);
    if (dto['ninsUpdateDate']) items.push(`${INS_UPDATE_DATE} is not null and date_format(${INS_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ninsUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //INS_ID--字符串字段
    if (dto['insIdList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_ID, <string[]>dto['insIdList']));
    if (dto['ninsIdList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_ID, <string[]>dto['ninsIdList'], true));
    if (dto['insIdLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_ID, dto['insIdLike'])); //For MysSQL
    if (dto['ninsIdLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_ID, dto['ninsIdLike'], true)); //For MysSQL
    if (dto['insIdLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_ID, <string[]>dto['insIdLikeList'])); //For MysSQL
    //INS_BARCODE--字符串字段
    if (dto['insBarcodeList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_BARCODE, <string[]>dto['insBarcodeList']));
    if (dto['ninsBarcodeList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_BARCODE, <string[]>dto['ninsBarcodeList'], true));
    if (dto['insBarcodeLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_BARCODE, dto['insBarcodeLike'])); //For MysSQL
    if (dto['ninsBarcodeLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_BARCODE, dto['ninsBarcodeLike'], true)); //For MysSQL
    if (dto['insBarcodeLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_BARCODE, <string[]>dto['insBarcodeLikeList'])); //For MysSQL
    //INS_NAME--字符串字段
    if (dto['insNameList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_NAME, <string[]>dto['insNameList']));
    if (dto['ninsNameList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_NAME, <string[]>dto['ninsNameList'], true));
    if (dto['insNameLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_NAME, dto['insNameLike'])); //For MysSQL
    if (dto['ninsNameLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_NAME, dto['ninsNameLike'], true)); //For MysSQL
    if (dto['insNameLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_NAME, <string[]>dto['insNameLikeList'])); //For MysSQL
    //INS_MODEL--字符串字段
    if (dto['insModelList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MODEL, <string[]>dto['insModelList']));
    if (dto['ninsModelList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MODEL, <string[]>dto['ninsModelList'], true));
    if (dto['insModelLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MODEL, dto['insModelLike'])); //For MysSQL
    if (dto['ninsModelLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MODEL, dto['ninsModelLike'], true)); //For MysSQL
    if (dto['insModelLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_MODEL, <string[]>dto['insModelLikeList'])); //For MysSQL
    //INS_MANAGER--字符串字段
    if (dto['insManagerList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MANAGER, <string[]>dto['insManagerList']));
    if (dto['ninsManagerList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MANAGER, <string[]>dto['ninsManagerList'], true));
    if (dto['insManagerLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MANAGER, dto['insManagerLike'])); //For MysSQL
    if (dto['ninsManagerLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MANAGER, dto['ninsManagerLike'], true)); //For MysSQL
    if (dto['insManagerLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_MANAGER, <string[]>dto['insManagerLikeList'])); //For MysSQL
    //INS_TYPE--字符串字段
    if (dto['insTypeList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_TYPE, <string[]>dto['insTypeList']));
    if (dto['ninsTypeList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_TYPE, <string[]>dto['ninsTypeList'], true));
    if (dto['insTypeLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_TYPE, dto['insTypeLike'])); //For MysSQL
    if (dto['ninsTypeLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_TYPE, dto['ninsTypeLike'], true)); //For MysSQL
    if (dto['insTypeLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_TYPE, <string[]>dto['insTypeLikeList'])); //For MysSQL
    //INS_REMARK--字符串字段
    if (dto['insRemarkList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_REMARK, <string[]>dto['insRemarkList']));
    if (dto['ninsRemarkList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_REMARK, <string[]>dto['ninsRemarkList'], true));
    if (dto['insRemarkLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_REMARK, dto['insRemarkLike'])); //For MysSQL
    if (dto['ninsRemarkLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_REMARK, dto['ninsRemarkLike'], true)); //For MysSQL
    if (dto['insRemarkLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_REMARK, <string[]>dto['insRemarkLikeList'])); //For MysSQL
    //INS_BUY_DATE--日期字段
    if (dto[insBuyDate]) items.push(`${INS_BUY_DATE} is not null and date_format(${INS_BUY_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sInsBuyDate'])}`);
    if (dto[insBuyDate]) items.push(`${INS_BUY_DATE} is not null and date_format(${INS_BUY_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eInsBuyDate'])}`);
    //INS_MAINTAIN_PERIOD--字符串字段
    if (dto['insMaintainPeriodList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MAINTAIN_PERIOD, <string[]>dto['insMaintainPeriodList']));
    if (dto['ninsMaintainPeriodList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MAINTAIN_PERIOD, <string[]>dto['ninsMaintainPeriodList'], true));
    if (dto['insMaintainPeriodLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MAINTAIN_PERIOD, dto['insMaintainPeriodLike'])); //For MysSQL
    if (dto['ninsMaintainPeriodLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MAINTAIN_PERIOD, dto['ninsMaintainPeriodLike'], true)); //For MysSQL
    if (dto['insMaintainPeriodLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_MAINTAIN_PERIOD, <string[]>dto['insMaintainPeriodLikeList'])); //For MysSQL
    //INS_MAKER--字符串字段
    if (dto['insMakerList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MAKER, <string[]>dto['insMakerList']));
    if (dto['ninsMakerList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_MAKER, <string[]>dto['ninsMakerList'], true));
    if (dto['insMakerLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MAKER, dto['insMakerLike'])); //For MysSQL
    if (dto['ninsMakerLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_MAKER, dto['ninsMakerLike'], true)); //For MysSQL
    if (dto['insMakerLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_MAKER, <string[]>dto['insMakerLikeList'])); //For MysSQL
    //INS_PRICE--字符串字段
    if (dto['insPriceList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_PRICE, <string[]>dto['insPriceList']));
    if (dto['ninsPriceList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_PRICE, <string[]>dto['ninsPriceList'], true));
    if (dto['insPriceLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_PRICE, dto['insPriceLike'])); //For MysSQL
    if (dto['ninsPriceLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_PRICE, dto['ninsPriceLike'], true)); //For MysSQL
    if (dto['insPriceLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_PRICE, <string[]>dto['insPriceLikeList'])); //For MysSQL
    //INS_EXTRA_PROVIDE--字符串字段
    if (dto['insExtraProvideList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_EXTRA_PROVIDE, <string[]>dto['insExtraProvideList']));
    if (dto['ninsExtraProvideList']?.length > 0) items.push(SysInstrumentSQL.IN_SQL(INS_EXTRA_PROVIDE, <string[]>dto['ninsExtraProvideList'], true));
    if (dto['insExtraProvideLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_EXTRA_PROVIDE, dto['insExtraProvideLike'])); //For MysSQL
    if (dto['ninsExtraProvideLike']?.length > 0) items.push(SysInstrumentSQL.LIKE_SQL(INS_EXTRA_PROVIDE, dto['ninsExtraProvideLike'], true)); //For MysSQL
    if (dto['insExtraProvideLikeList']?.length > 0) items.push(SysInstrumentSQL.LIKE_LIST_SQL(INS_EXTRA_PROVIDE, <string[]>dto['insExtraProvideLikeList'])); //For MysSQL
    //INS_CREATE_DATE--日期字段
    if (dto[insCreateDate]) items.push(`${INS_CREATE_DATE} is not null and date_format(${INS_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sInsCreateDate'])}`);
    if (dto[insCreateDate]) items.push(`${INS_CREATE_DATE} is not null and date_format(${INS_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eInsCreateDate'])}`);
    //INS_UPDATE_DATE--日期字段
    if (dto[insUpdateDate]) items.push(`${INS_UPDATE_DATE} is not null and date_format(${INS_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sInsUpdateDate'])}`);
    if (dto[insUpdateDate]) items.push(`${INS_UPDATE_DATE} is not null and date_format(${INS_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eInsUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysInstrumentSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysInstrumentCDto | Partial<SysInstrumentCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(INS_ID);
    cols.push(INS_BARCODE);
    cols.push(INS_NAME);
    cols.push(INS_MODEL);
    cols.push(INS_MANAGER);
    cols.push(INS_TYPE);
    cols.push(INS_REMARK);
    cols.push(INS_BUY_DATE);
    cols.push(INS_MAINTAIN_PERIOD);
    cols.push(INS_MAKER);
    cols.push(INS_PRICE);
    cols.push(INS_EXTRA_PROVIDE);
    cols.push(INS_CREATE_DATE);
    cols.push(INS_UPDATE_DATE);
    values.push(toSqlValue(dto[insId]));
    values.push(toSqlValue(dto[insBarcode]));
    values.push(toSqlValue(dto[insName]));
    values.push(toSqlValue(dto[insModel]));
    values.push(toSqlValue(dto[insManager]));
    values.push(toSqlValue(dto[insType]));
    values.push(toSqlValue(dto[insRemark]));
    values.push('NOW()');
    values.push(toSqlValue(dto[insMaintainPeriod]));
    values.push(toSqlValue(dto[insMaker]));
    values.push(toSqlValue(dto[insPrice]));
    values.push(toSqlValue(dto[insExtraProvide]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysInstrumentCDto[] | Partial<SysInstrumentCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(INS_ID);
    cols.push(INS_BARCODE);
    cols.push(INS_NAME);
    cols.push(INS_MODEL);
    cols.push(INS_MANAGER);
    cols.push(INS_TYPE);
    cols.push(INS_REMARK);
    cols.push(INS_BUY_DATE);
    cols.push(INS_MAINTAIN_PERIOD);
    cols.push(INS_MAKER);
    cols.push(INS_PRICE);
    cols.push(INS_EXTRA_PROVIDE);
    cols.push(INS_CREATE_DATE);
    cols.push(INS_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[insId]));
      values.push(toSqlValue(dto[insBarcode]));
      values.push(toSqlValue(dto[insName]));
      values.push(toSqlValue(dto[insModel]));
      values.push(toSqlValue(dto[insManager]));
      values.push(toSqlValue(dto[insType]));
      values.push(toSqlValue(dto[insRemark]));
      values.push('NOW()');
      values.push(toSqlValue(dto[insMaintainPeriod]));
      values.push(toSqlValue(dto[insMaker]));
      values.push(toSqlValue(dto[insPrice]));
      values.push(toSqlValue(dto[insExtraProvide]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysInstrumentUDto | Partial<SysInstrumentUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysInstrumentColNames, SysInstrumentColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${INS_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${INS_BARCODE} = ${toSqlValue(dto[insBarcode])}`);
    items.push(`${INS_NAME} = ${toSqlValue(dto[insName])}`);
    items.push(`${INS_MODEL} = ${toSqlValue(dto[insModel])}`);
    items.push(`${INS_MANAGER} = ${toSqlValue(dto[insManager])}`);
    items.push(`${INS_TYPE} = ${toSqlValue(dto[insType])}`);
    items.push(`${INS_REMARK} = ${toSqlValue(dto[insRemark])}`);
    items.push(`${INS_MAINTAIN_PERIOD} = ${toSqlValue(dto[insMaintainPeriod])}`);
    items.push(`${INS_MAKER} = ${toSqlValue(dto[insMaker])}`);
    items.push(`${INS_PRICE} = ${toSqlValue(dto[insPrice])}`);
    items.push(`${INS_EXTRA_PROVIDE} = ${toSqlValue(dto[insExtraProvide])}`);
    items.push(`${INS_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysInstrumentUDto | Partial<SysInstrumentUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysInstrumentColNames, SysInstrumentColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${INS_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysInstrumentSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${INS_BARCODE} = ${toSqlValue(dto[insBarcode])}`);
    items.push(`${INS_NAME} = ${toSqlValue(dto[insName])}`);
    items.push(`${INS_MODEL} = ${toSqlValue(dto[insModel])}`);
    items.push(`${INS_MANAGER} = ${toSqlValue(dto[insManager])}`);
    items.push(`${INS_TYPE} = ${toSqlValue(dto[insType])}`);
    items.push(`${INS_REMARK} = ${toSqlValue(dto[insRemark])}`);
    items.push(`${INS_MAINTAIN_PERIOD} = ${toSqlValue(dto[insMaintainPeriod])}`);
    items.push(`${INS_MAKER} = ${toSqlValue(dto[insMaker])}`);
    items.push(`${INS_PRICE} = ${toSqlValue(dto[insPrice])}`);
    items.push(`${INS_EXTRA_PROVIDE} = ${toSqlValue(dto[insExtraProvide])}`);
    items.push(`${INS_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysInstrumentSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysInstrumentUDto[] | Partial<SysInstrumentUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysInstrumentSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysInstrumentUDto | Partial<SysInstrumentUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[insBarcode]?.length > 0) items.push(`${INS_BARCODE} = ${toSqlValue(dto[insBarcode])}`);
    if (dto[insName]?.length > 0) items.push(`${INS_NAME} = ${toSqlValue(dto[insName])}`);
    if (dto[insModel]?.length > 0) items.push(`${INS_MODEL} = ${toSqlValue(dto[insModel])}`);
    if (dto[insManager]?.length > 0) items.push(`${INS_MANAGER} = ${toSqlValue(dto[insManager])}`);
    if (dto[insType]?.length > 0) items.push(`${INS_TYPE} = ${toSqlValue(dto[insType])}`);
    if (dto[insRemark]?.length > 0) items.push(`${INS_REMARK} = ${toSqlValue(dto[insRemark])}`);
    if (dto[insMaintainPeriod]?.length > 0) items.push(`${INS_MAINTAIN_PERIOD} = ${toSqlValue(dto[insMaintainPeriod])}`);
    if (dto[insMaker]?.length > 0) items.push(`${INS_MAKER} = ${toSqlValue(dto[insMaker])}`);
    if (dto[insPrice]?.length > 0) items.push(`${INS_PRICE} = ${toSqlValue(dto[insPrice])}`);
    if (dto[insExtraProvide]?.length > 0) items.push(`${INS_EXTRA_PROVIDE} = ${toSqlValue(dto[insExtraProvide])}`);
    if (dto[insUpdateDate]) items.push(`${INS_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysInstrumentUDto[] | Partial<SysInstrumentUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysInstrumentSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysInstrumentQDto | Partial<SysInstrumentQDto>): string => {
    if (!dto) return '';
    let where = SysInstrumentSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysInstrumentSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_INSTRUMENT';
/**
 * 列名常量字段
 */
const INS_ID = SysInstrumentColNameEnum.INS_ID;
const INS_BARCODE = SysInstrumentColNameEnum.INS_BARCODE;
const INS_NAME = SysInstrumentColNameEnum.INS_NAME;
const INS_MODEL = SysInstrumentColNameEnum.INS_MODEL;
const INS_MANAGER = SysInstrumentColNameEnum.INS_MANAGER;
const INS_TYPE = SysInstrumentColNameEnum.INS_TYPE;
const INS_REMARK = SysInstrumentColNameEnum.INS_REMARK;
const INS_BUY_DATE = SysInstrumentColNameEnum.INS_BUY_DATE;
const INS_MAINTAIN_PERIOD = SysInstrumentColNameEnum.INS_MAINTAIN_PERIOD;
const INS_MAKER = SysInstrumentColNameEnum.INS_MAKER;
const INS_PRICE = SysInstrumentColNameEnum.INS_PRICE;
const INS_EXTRA_PROVIDE = SysInstrumentColNameEnum.INS_EXTRA_PROVIDE;
const INS_CREATE_DATE = SysInstrumentColNameEnum.INS_CREATE_DATE;
const INS_UPDATE_DATE = SysInstrumentColNameEnum.INS_UPDATE_DATE;
/**
 * 实体类属性名
 */
const insId = SysInstrumentColPropEnum.insId;
const insBarcode = SysInstrumentColPropEnum.insBarcode;
const insName = SysInstrumentColPropEnum.insName;
const insModel = SysInstrumentColPropEnum.insModel;
const insManager = SysInstrumentColPropEnum.insManager;
const insType = SysInstrumentColPropEnum.insType;
const insRemark = SysInstrumentColPropEnum.insRemark;
const insBuyDate = SysInstrumentColPropEnum.insBuyDate;
const insMaintainPeriod = SysInstrumentColPropEnum.insMaintainPeriod;
const insMaker = SysInstrumentColPropEnum.insMaker;
const insPrice = SysInstrumentColPropEnum.insPrice;
const insExtraProvide = SysInstrumentColPropEnum.insExtraProvide;
const insCreateDate = SysInstrumentColPropEnum.insCreateDate;
const insUpdateDate = SysInstrumentColPropEnum.insUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysInstrumentTable.PRIMER_KEY;
const primerKey = SysInstrumentTable.primerKey;
