import { SysCacheTraceQDto } from '../../dto/query/sys.cache.trace.qdto';
import { SysCacheTraceCDto } from '../../dto/create/sys.cache.trace.cdto';
import { SysCacheTraceUDto } from '../../dto/update/sys.cache.trace.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysCacheTraceColNameEnum, SysCacheTraceColPropEnum, SysCacheTraceTable, SysCacheTraceColNames, SysCacheTraceColProps } from '../ddl/sys.cache.trace.cols';
/**
 * SYS_CACHE_TRACE--缓存信息表表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysCacheTraceSQL
 */
export const SysCacheTraceSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let cols = SysCacheTraceSQL.COL_MAPPER_SQL(dto);
    let where = SysCacheTraceSQL.WHERE_SQL(dto);
    let group = SysCacheTraceSQL.GROUP_BY_SQL(dto);
    let order = SysCacheTraceSQL.ORDER_BY_SQL(dto);
    let limit = SysCacheTraceSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let where = SysCacheTraceSQL.WHERE_SQL(dto);
    let group = SysCacheTraceSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let cols = SysCacheTraceSQL.COL_MAPPER_SQL(dto);
    let group = SysCacheTraceSQL.GROUP_BY_SQL(dto);
    let order = SysCacheTraceSQL.ORDER_BY_SQL(dto);
    let limit = SysCacheTraceSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysCacheTraceSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysCacheTraceSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysCacheTraceSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let where = SysCacheTraceSQL.WHERE_SQL(dto);
    let group = SysCacheTraceSQL.GROUP_BY_SQL(dto);
    let order = SysCacheTraceSQL.ORDER_BY_SQL(dto);
    let limit = SysCacheTraceSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysCacheTraceColNames, SysCacheTraceColProps);
    if (!cols || cols.length == 0) cols = SysCacheTraceColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[ctId]?.length > 0) items.push(` ${CT_ID} = '${dto[ctId]}'`);
    if (dto['nctId']?.length > 0) items.push(` ${CT_ID} != '${dto['nctId']}'`);
    if (dto[ctCode]?.length > 0) items.push(` ${CT_CODE} = '${dto[ctCode]}'`);
    if (dto['nctCode']?.length > 0) items.push(` ${CT_CODE} != '${dto['nctCode']}'`);
    if (dto[ctOperate]?.length > 0) items.push(` ${CT_OPERATE} = '${dto[ctOperate]}'`);
    if (dto['nctOperate']?.length > 0) items.push(` ${CT_OPERATE} != '${dto['nctOperate']}'`);
    if (dto[ctTargetId]?.length > 0) items.push(` ${CT_TARGET_ID} = '${dto[ctTargetId]}'`);
    if (dto['nctTargetId']?.length > 0) items.push(` ${CT_TARGET_ID} != '${dto['nctTargetId']}'`);
    if (dto[ctCreateDate]) items.push(`${CT_CREATE_DATE} is not null and date_format(${CT_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[ctCreateDate])}`);
    if (dto['nctCreateDate']) items.push(`${CT_CREATE_DATE} is not null and date_format(${CT_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nctCreateDate'])}`);
    if (dto[ctComment]?.length > 0) items.push(` ${CT_COMMENT} = '${dto[ctComment]}'`);
    if (dto['nctComment']?.length > 0) items.push(` ${CT_COMMENT} != '${dto['nctComment']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //CT_ID--字符串字段
    if (dto['ctIdList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_ID, <string[]>dto['ctIdList']));
    if (dto['nctIdList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_ID, <string[]>dto['nctIdList'], true));
    if (dto['ctIdLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_ID, dto['ctIdLike'])); //For MysSQL
    if (dto['nctIdLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_ID, dto['nctIdLike'], true)); //For MysSQL
    if (dto['ctIdLikeList']?.length > 0) items.push(SysCacheTraceSQL.LIKE_LIST_SQL(CT_ID, <string[]>dto['ctIdLikeList'])); //For MysSQL
    //CT_CODE--字符串字段
    if (dto['ctCodeList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_CODE, <string[]>dto['ctCodeList']));
    if (dto['nctCodeList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_CODE, <string[]>dto['nctCodeList'], true));
    if (dto['ctCodeLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_CODE, dto['ctCodeLike'])); //For MysSQL
    if (dto['nctCodeLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_CODE, dto['nctCodeLike'], true)); //For MysSQL
    if (dto['ctCodeLikeList']?.length > 0) items.push(SysCacheTraceSQL.LIKE_LIST_SQL(CT_CODE, <string[]>dto['ctCodeLikeList'])); //For MysSQL
    //CT_OPERATE--字符串字段
    if (dto['ctOperateList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_OPERATE, <string[]>dto['ctOperateList']));
    if (dto['nctOperateList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_OPERATE, <string[]>dto['nctOperateList'], true));
    if (dto['ctOperateLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_OPERATE, dto['ctOperateLike'])); //For MysSQL
    if (dto['nctOperateLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_OPERATE, dto['nctOperateLike'], true)); //For MysSQL
    if (dto['ctOperateLikeList']?.length > 0) items.push(SysCacheTraceSQL.LIKE_LIST_SQL(CT_OPERATE, <string[]>dto['ctOperateLikeList'])); //For MysSQL
    //CT_TARGET_ID--字符串字段
    if (dto['ctTargetIdList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_TARGET_ID, <string[]>dto['ctTargetIdList']));
    if (dto['nctTargetIdList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_TARGET_ID, <string[]>dto['nctTargetIdList'], true));
    if (dto['ctTargetIdLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_TARGET_ID, dto['ctTargetIdLike'])); //For MysSQL
    if (dto['nctTargetIdLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_TARGET_ID, dto['nctTargetIdLike'], true)); //For MysSQL
    if (dto['ctTargetIdLikeList']?.length > 0) items.push(SysCacheTraceSQL.LIKE_LIST_SQL(CT_TARGET_ID, <string[]>dto['ctTargetIdLikeList'])); //For MysSQL
    //CT_CREATE_DATE--日期字段
    if (dto[ctCreateDate]) items.push(`${CT_CREATE_DATE} is not null and date_format(${CT_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sCtCreateDate'])}`);
    if (dto[ctCreateDate]) items.push(`${CT_CREATE_DATE} is not null and date_format(${CT_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eCtCreateDate'])}`);
    //CT_COMMENT--字符串字段
    if (dto['ctCommentList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_COMMENT, <string[]>dto['ctCommentList']));
    if (dto['nctCommentList']?.length > 0) items.push(SysCacheTraceSQL.IN_SQL(CT_COMMENT, <string[]>dto['nctCommentList'], true));
    if (dto['ctCommentLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_COMMENT, dto['ctCommentLike'])); //For MysSQL
    if (dto['nctCommentLike']?.length > 0) items.push(SysCacheTraceSQL.LIKE_SQL(CT_COMMENT, dto['nctCommentLike'], true)); //For MysSQL
    if (dto['ctCommentLikeList']?.length > 0) items.push(SysCacheTraceSQL.LIKE_LIST_SQL(CT_COMMENT, <string[]>dto['ctCommentLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysCacheTraceSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysCacheTraceCDto | Partial<SysCacheTraceCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(CT_ID);
    cols.push(CT_CODE);
    cols.push(CT_OPERATE);
    cols.push(CT_TARGET_ID);
    cols.push(CT_CREATE_DATE);
    cols.push(CT_COMMENT);
    values.push(toSqlValue(dto[ctId]));
    values.push(toSqlValue(dto[ctCode]));
    values.push(toSqlValue(dto[ctOperate]));
    values.push(toSqlValue(dto[ctTargetId]));
    values.push('NOW()');
    values.push(toSqlValue(dto[ctComment]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysCacheTraceCDto[] | Partial<SysCacheTraceCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(CT_ID);
    cols.push(CT_CODE);
    cols.push(CT_OPERATE);
    cols.push(CT_TARGET_ID);
    cols.push(CT_CREATE_DATE);
    cols.push(CT_COMMENT);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[ctId]));
      values.push(toSqlValue(dto[ctCode]));
      values.push(toSqlValue(dto[ctOperate]));
      values.push(toSqlValue(dto[ctTargetId]));
      values.push('NOW()');
      values.push(toSqlValue(dto[ctComment]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysCacheTraceUDto | Partial<SysCacheTraceUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysCacheTraceColNames, SysCacheTraceColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CT_CODE} = ${toSqlValue(dto[ctCode])}`);
    items.push(`${CT_OPERATE} = ${toSqlValue(dto[ctOperate])}`);
    items.push(`${CT_TARGET_ID} = ${toSqlValue(dto[ctTargetId])}`);
    items.push(`${CT_COMMENT} = ${toSqlValue(dto[ctComment])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysCacheTraceUDto | Partial<SysCacheTraceUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysCacheTraceColNames, SysCacheTraceColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysCacheTraceSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CT_CODE} = ${toSqlValue(dto[ctCode])}`);
    items.push(`${CT_OPERATE} = ${toSqlValue(dto[ctOperate])}`);
    items.push(`${CT_TARGET_ID} = ${toSqlValue(dto[ctTargetId])}`);
    items.push(`${CT_COMMENT} = ${toSqlValue(dto[ctComment])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysCacheTraceSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysCacheTraceUDto[] | Partial<SysCacheTraceUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysCacheTraceSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysCacheTraceUDto | Partial<SysCacheTraceUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[ctCode]?.length > 0) items.push(`${CT_CODE} = ${toSqlValue(dto[ctCode])}`);
    if (dto[ctOperate]?.length > 0) items.push(`${CT_OPERATE} = ${toSqlValue(dto[ctOperate])}`);
    if (dto[ctTargetId]?.length > 0) items.push(`${CT_TARGET_ID} = ${toSqlValue(dto[ctTargetId])}`);
    if (dto[ctComment]?.length > 0) items.push(`${CT_COMMENT} = ${toSqlValue(dto[ctComment])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysCacheTraceUDto[] | Partial<SysCacheTraceUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysCacheTraceSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): string => {
    if (!dto) return '';
    let where = SysCacheTraceSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysCacheTraceSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_CACHE_TRACE';
/**
 * 列名常量字段
 */
const CT_ID = SysCacheTraceColNameEnum.CT_ID;
const CT_CODE = SysCacheTraceColNameEnum.CT_CODE;
const CT_OPERATE = SysCacheTraceColNameEnum.CT_OPERATE;
const CT_TARGET_ID = SysCacheTraceColNameEnum.CT_TARGET_ID;
const CT_CREATE_DATE = SysCacheTraceColNameEnum.CT_CREATE_DATE;
const CT_COMMENT = SysCacheTraceColNameEnum.CT_COMMENT;
/**
 * 实体类属性名
 */
const ctId = SysCacheTraceColPropEnum.ctId;
const ctCode = SysCacheTraceColPropEnum.ctCode;
const ctOperate = SysCacheTraceColPropEnum.ctOperate;
const ctTargetId = SysCacheTraceColPropEnum.ctTargetId;
const ctCreateDate = SysCacheTraceColPropEnum.ctCreateDate;
const ctComment = SysCacheTraceColPropEnum.ctComment;
/**
 * 主键信息
 */
const PRIMER_KEY = SysCacheTraceTable.PRIMER_KEY;
const primerKey = SysCacheTraceTable.primerKey;
