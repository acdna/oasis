import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_HELP
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysHelp
 */
@Entity({
  name: 'SYS_HELP',
})
export class SysHelp {
  /**
   * 帮助编号-主键
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【帮助编号】不能为空' })
  @Max(64, { message: '【帮助编号】长度不能超过64' })
  helpId: string;
  /**
   * 帮助问题
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_QUESTION', type: 'varchar', length: '64' })
  @Max(64, { message: '【帮助问题】长度不能超过64' })
  helpQuestion: string;
  /**
   * 帮助回答
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_ANSWER', type: 'varchar', length: '512' })
  @Max(512, { message: '【帮助回答】长度不能超过512' })
  helpAnswer: string;
  /**
   * 问题编号父级编号
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_PARENT_ID', type: 'varchar', length: '64' })
  @Max(64, { message: '【问题编号父级编号】长度不能超过64' })
  helpParentId: string;
  /**
   * 帮助类别
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_TYPE', type: 'varchar', length: '255' })
  @Max(255, { message: '【帮助类别】长度不能超过255' })
  helpType: string;
  /**
   * 帮助匹配路径
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_PATH', type: 'varchar', length: '256' })
  @Max(256, { message: '【帮助匹配路径】长度不能超过256' })
  helpPath: string;
  /**
   * 排序
   *
   * @type { string }
   * @memberof SysHelp
   */
  @Column({ name: 'HELP_SORT', type: 'varchar', length: '64' })
  @Max(64, { message: '【排序】长度不能超过64' })
  helpSort: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysHelp
   */
  @Type(() => Date)
  @Column({ name: 'HELP_CREATE_DATE', type: 'datetime' })
  helpCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysHelp
   */
  @Type(() => Date)
  @Column({ name: 'HELP_UPDATE_DATE', type: 'datetime' })
  helpUpdateDate: Date;
}
