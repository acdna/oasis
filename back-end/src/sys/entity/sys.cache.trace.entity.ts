import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * 缓存信息表
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysCacheTrace
 */
@Entity({
  name: 'SYS_CACHE_TRACE',
})
export class SysCacheTrace {
  /**
   * 缓存记录ID-主键
   *
   * @type { string }
   * @memberof SysCacheTrace
   */
  @Column({ name: 'CT_ID', type: 'varchar', length: '32', primary: true })
  @IsNotEmpty({ message: '【缓存记录ID】不能为空' })
  @Max(32, { message: '【缓存记录ID】长度不能超过32' })
  ctId: string;
  /**
   * 缓存类型代码
   *
   * @type { string }
   * @memberof SysCacheTrace
   */
  @Column({ name: 'CT_CODE', type: 'varchar', length: '12' })
  @Max(12, { message: '【缓存类型代码】长度不能超过12' })
  ctCode: string;
  /**
   * 缓存操作类型代码
   *
   * @type { string }
   * @memberof SysCacheTrace
   */
  @Column({ name: 'CT_OPERATE', type: 'varchar', length: '12' })
  @Max(12, { message: '【缓存操作类型代码】长度不能超过12' })
  ctOperate: string;
  /**
   * 缓存需要操作的目标记录ID
   *
   * @type { string }
   * @memberof SysCacheTrace
   */
  @Column({ name: 'CT_TARGET_ID', type: 'varchar', length: '64' })
  @Max(64, { message: '【缓存需要操作的目标记录ID】长度不能超过64' })
  ctTargetId: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCacheTrace
   */
  @Type(() => Date)
  @Column({ name: 'CT_CREATE_DATE', type: 'datetime' })
  ctCreateDate: Date;
  /**
   * 缓存备注信息
   *
   * @type { string }
   * @memberof SysCacheTrace
   */
  @Column({ name: 'CT_COMMENT', type: 'varchar', length: '128' })
  @Max(128, { message: '【缓存备注信息】长度不能超过128' })
  ctComment: string;
}
