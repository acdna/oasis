/**
 * SYS_ROLE-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleColNames
 */
export interface SysRoleColNames {
  /**
   * @description 角色ID
   * @field ROLE_ID
   */
  ROLE_ID: string; //
  /**
   * @description 角色名称
   * @field ROLE_NAME
   */
  ROLE_NAME: string; //
  /**
   * @description
   * @field ROLE_NAME_EN
   */
  ROLE_NAME_EN: string; //
  /**
   * @description 状态
   * @field ROLE_STATE
   */
  ROLE_STATE: string; //
  /**
   * @description 角色所属系统
   * @field ROLE_SYS
   */
  ROLE_SYS: string; //
}

/**
 * SYS_ROLE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleColProps
 */
export interface SysRoleColProps {
  /**
   * @description roleId
   * @field roleId
   */
  roleId: string; //
  /**
   * @description roleName
   * @field roleName
   */
  roleName: string; //
  /**
   * @description roleNameEn
   * @field roleNameEn
   */
  roleNameEn: string; //
  /**
   * @description roleState
   * @field roleState
   */
  roleState: string; //
  /**
   * @description roleSys
   * @field roleSys
   */
  roleSys: string; //
}

/**
 * SYS_ROLE-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleCols
 */
export enum SysRoleColNameEnum {
  /**
   * @description 角色ID
   * @field ROLE_ID
   */
  ROLE_ID = 'ROLE_ID', //
  /**
   * @description 角色名称
   * @field ROLE_NAME
   */
  ROLE_NAME = 'ROLE_NAME', //
  /**
   * @description
   * @field ROLE_NAME_EN
   */
  ROLE_NAME_EN = 'ROLE_NAME_EN', //
  /**
   * @description 状态
   * @field ROLE_STATE
   */
  ROLE_STATE = 'ROLE_STATE', //
  /**
   * @description 角色所属系统
   * @field ROLE_SYS
   */
  ROLE_SYS = 'ROLE_SYS', //
}

/**
 * SYS_ROLE-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleCols
 */
export enum SysRoleColPropEnum {
  /**
   * @description roleId
   * @field roleId
   */
  roleId = 'roleId', //
  /**
   * @description roleName
   * @field roleName
   */
  roleName = 'roleName', //
  /**
   * @description roleNameEn
   * @field roleNameEn
   */
  roleNameEn = 'roleNameEn', //
  /**
   * @description roleState
   * @field roleState
   */
  roleState = 'roleState', //
  /**
   * @description roleSys
   * @field roleSys
   */
  roleSys = 'roleSys', //
}

/**
 * SYS_ROLE-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysRoleTable
 */
export enum SysRoleTable {
  /**
   * @description ROLE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'ROLE_ID',
  /**
   * @description roleId
   * @field primerKey
   */
  primerKey = 'roleId',
  /**
   * @description SYS_ROLE
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_ROLE',
  /**
   * @description SysRole
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysRole',
}

/**
 * SYS_ROLE-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleColNames = Object.keys(SysRoleColNameEnum);
/**
 * SYS_ROLE-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleColProps = Object.keys(SysRoleColPropEnum);
/**
 * SYS_ROLE-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleColMap = { names: SysRoleColNames, props: SysRoleColProps };
