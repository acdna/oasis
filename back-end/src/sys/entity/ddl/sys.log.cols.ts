/**
 * SYS_LOG-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysLogColNames
 */
export interface SysLogColNames {
  /**
   * @description 日志ID号
   * @field LOG_ID
   */
  LOG_ID: string; //
  /**
   * @description 操作人员名称
   * @field LOG_USER
   */
  LOG_USER: string; //
  /**
   * @description 操作时间
   * @field LOG_TIME
   */
  LOG_TIME: Date; //
  /**
   * @description IP地址
   * @field LOG_IP
   */
  LOG_IP: string; //
  /**
   * @description 操作URL
   * @field LOG_URL
   */
  LOG_URL: string; //
  /**
   * @description 模块
   * @field LOG_TITLE
   */
  LOG_TITLE: string; //
  /**
   * @description 内容
   * @field LOG_CONTENT
   */
  LOG_CONTENT: string; //
  /**
   * @description 操作类型
   * @field LOG_TYPE
   */
  LOG_TYPE: number; //
}

/**
 * SYS_LOG-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysLogColProps
 */
export interface SysLogColProps {
  /**
   * @description logId
   * @field logId
   */
  logId: string; //
  /**
   * @description logUser
   * @field logUser
   */
  logUser: string; //
  /**
   * @description logTime
   * @field logTime
   */
  logTime: Date; //
  /**
   * @description logIp
   * @field logIp
   */
  logIp: string; //
  /**
   * @description logUrl
   * @field logUrl
   */
  logUrl: string; //
  /**
   * @description logTitle
   * @field logTitle
   */
  logTitle: string; //
  /**
   * @description logContent
   * @field logContent
   */
  logContent: string; //
  /**
   * @description logType
   * @field logType
   */
  logType: number; //
}

/**
 * SYS_LOG-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysLogCols
 */
export enum SysLogColNameEnum {
  /**
   * @description 日志ID号
   * @field LOG_ID
   */
  LOG_ID = 'LOG_ID', //
  /**
   * @description 操作人员名称
   * @field LOG_USER
   */
  LOG_USER = 'LOG_USER', //
  /**
   * @description 操作时间
   * @field LOG_TIME
   */
  LOG_TIME = 'LOG_TIME', //
  /**
   * @description IP地址
   * @field LOG_IP
   */
  LOG_IP = 'LOG_IP', //
  /**
   * @description 操作URL
   * @field LOG_URL
   */
  LOG_URL = 'LOG_URL', //
  /**
   * @description 模块
   * @field LOG_TITLE
   */
  LOG_TITLE = 'LOG_TITLE', //
  /**
   * @description 内容
   * @field LOG_CONTENT
   */
  LOG_CONTENT = 'LOG_CONTENT', //
  /**
   * @description 操作类型
   * @field LOG_TYPE
   */
  LOG_TYPE = 'LOG_TYPE', //
}

/**
 * SYS_LOG-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysLogCols
 */
export enum SysLogColPropEnum {
  /**
   * @description logId
   * @field logId
   */
  logId = 'logId', //
  /**
   * @description logUser
   * @field logUser
   */
  logUser = 'logUser', //
  /**
   * @description logTime
   * @field logTime
   */
  logTime = 'logTime', //
  /**
   * @description logIp
   * @field logIp
   */
  logIp = 'logIp', //
  /**
   * @description logUrl
   * @field logUrl
   */
  logUrl = 'logUrl', //
  /**
   * @description logTitle
   * @field logTitle
   */
  logTitle = 'logTitle', //
  /**
   * @description logContent
   * @field logContent
   */
  logContent = 'logContent', //
  /**
   * @description logType
   * @field logType
   */
  logType = 'logType', //
}

/**
 * SYS_LOG-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysLogTable
 */
export enum SysLogTable {
  /**
   * @description LOG_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'LOG_ID',
  /**
   * @description logId
   * @field primerKey
   */
  primerKey = 'logId',
  /**
   * @description SYS_LOG
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_LOG',
  /**
   * @description SysLog
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysLog',
}

/**
 * SYS_LOG-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysLogColNames = Object.keys(SysLogColNameEnum);
/**
 * SYS_LOG-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysLogColProps = Object.keys(SysLogColPropEnum);
/**
 * SYS_LOG-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysLogColMap = { names: SysLogColNames, props: SysLogColProps };
