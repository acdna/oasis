/**
 * SYS_INSTRUMENT-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysInstrumentColNames
 */
export interface SysInstrumentColNames {
  /**
   * @description 仪器编号
   * @field INS_ID
   */
  INS_ID: string; //
  /**
   * @description 仪器条码号
   * @field INS_BARCODE
   */
  INS_BARCODE: string; //
  /**
   * @description 仪器名称
   * @field INS_NAME
   */
  INS_NAME: string; //
  /**
   * @description 仪器型号
   * @field INS_MODEL
   */
  INS_MODEL: string; //
  /**
   * @description 仪器负责人
   * @field INS_MANAGER
   */
  INS_MANAGER: string; //
  /**
   * @description 仪器类型
   * @field INS_TYPE
   */
  INS_TYPE: string; //
  /**
   * @description 仪器描述
   * @field INS_REMARK
   */
  INS_REMARK: string; //
  /**
   * @description 仪器购买日期
   * @field INS_BUY_DATE
   */
  INS_BUY_DATE: Date; //
  /**
   * @description 仪器维护周期
   * @field INS_MAINTAIN_PERIOD
   */
  INS_MAINTAIN_PERIOD: string; //
  /**
   * @description 仪器厂商
   * @field INS_MAKER
   */
  INS_MAKER: string; //
  /**
   * @description 仪器价格
   * @field INS_PRICE
   */
  INS_PRICE: string; //
  /**
   * @description
   * @field INS_EXTRA_PROVIDE
   */
  INS_EXTRA_PROVIDE: string; //
  /**
   * @description 创建日期
   * @field INS_CREATE_DATE
   */
  INS_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field INS_UPDATE_DATE
   */
  INS_UPDATE_DATE: Date; //
}

/**
 * SYS_INSTRUMENT-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysInstrumentColProps
 */
export interface SysInstrumentColProps {
  /**
   * @description insId
   * @field insId
   */
  insId: string; //
  /**
   * @description insBarcode
   * @field insBarcode
   */
  insBarcode: string; //
  /**
   * @description insName
   * @field insName
   */
  insName: string; //
  /**
   * @description insModel
   * @field insModel
   */
  insModel: string; //
  /**
   * @description insManager
   * @field insManager
   */
  insManager: string; //
  /**
   * @description insType
   * @field insType
   */
  insType: string; //
  /**
   * @description insRemark
   * @field insRemark
   */
  insRemark: string; //
  /**
   * @description insBuyDate
   * @field insBuyDate
   */
  insBuyDate: Date; //
  /**
   * @description insMaintainPeriod
   * @field insMaintainPeriod
   */
  insMaintainPeriod: string; //
  /**
   * @description insMaker
   * @field insMaker
   */
  insMaker: string; //
  /**
   * @description insPrice
   * @field insPrice
   */
  insPrice: string; //
  /**
   * @description insExtraProvide
   * @field insExtraProvide
   */
  insExtraProvide: string; //
  /**
   * @description insCreateDate
   * @field insCreateDate
   */
  insCreateDate: Date; //
  /**
   * @description insUpdateDate
   * @field insUpdateDate
   */
  insUpdateDate: Date; //
}

/**
 * SYS_INSTRUMENT-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysInstrumentCols
 */
export enum SysInstrumentColNameEnum {
  /**
   * @description 仪器编号
   * @field INS_ID
   */
  INS_ID = 'INS_ID', //
  /**
   * @description 仪器条码号
   * @field INS_BARCODE
   */
  INS_BARCODE = 'INS_BARCODE', //
  /**
   * @description 仪器名称
   * @field INS_NAME
   */
  INS_NAME = 'INS_NAME', //
  /**
   * @description 仪器型号
   * @field INS_MODEL
   */
  INS_MODEL = 'INS_MODEL', //
  /**
   * @description 仪器负责人
   * @field INS_MANAGER
   */
  INS_MANAGER = 'INS_MANAGER', //
  /**
   * @description 仪器类型
   * @field INS_TYPE
   */
  INS_TYPE = 'INS_TYPE', //
  /**
   * @description 仪器描述
   * @field INS_REMARK
   */
  INS_REMARK = 'INS_REMARK', //
  /**
   * @description 仪器购买日期
   * @field INS_BUY_DATE
   */
  INS_BUY_DATE = 'INS_BUY_DATE', //
  /**
   * @description 仪器维护周期
   * @field INS_MAINTAIN_PERIOD
   */
  INS_MAINTAIN_PERIOD = 'INS_MAINTAIN_PERIOD', //
  /**
   * @description 仪器厂商
   * @field INS_MAKER
   */
  INS_MAKER = 'INS_MAKER', //
  /**
   * @description 仪器价格
   * @field INS_PRICE
   */
  INS_PRICE = 'INS_PRICE', //
  /**
   * @description
   * @field INS_EXTRA_PROVIDE
   */
  INS_EXTRA_PROVIDE = 'INS_EXTRA_PROVIDE', //
  /**
   * @description 创建日期
   * @field INS_CREATE_DATE
   */
  INS_CREATE_DATE = 'INS_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field INS_UPDATE_DATE
   */
  INS_UPDATE_DATE = 'INS_UPDATE_DATE', //
}

/**
 * SYS_INSTRUMENT-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysInstrumentCols
 */
export enum SysInstrumentColPropEnum {
  /**
   * @description insId
   * @field insId
   */
  insId = 'insId', //
  /**
   * @description insBarcode
   * @field insBarcode
   */
  insBarcode = 'insBarcode', //
  /**
   * @description insName
   * @field insName
   */
  insName = 'insName', //
  /**
   * @description insModel
   * @field insModel
   */
  insModel = 'insModel', //
  /**
   * @description insManager
   * @field insManager
   */
  insManager = 'insManager', //
  /**
   * @description insType
   * @field insType
   */
  insType = 'insType', //
  /**
   * @description insRemark
   * @field insRemark
   */
  insRemark = 'insRemark', //
  /**
   * @description insBuyDate
   * @field insBuyDate
   */
  insBuyDate = 'insBuyDate', //
  /**
   * @description insMaintainPeriod
   * @field insMaintainPeriod
   */
  insMaintainPeriod = 'insMaintainPeriod', //
  /**
   * @description insMaker
   * @field insMaker
   */
  insMaker = 'insMaker', //
  /**
   * @description insPrice
   * @field insPrice
   */
  insPrice = 'insPrice', //
  /**
   * @description insExtraProvide
   * @field insExtraProvide
   */
  insExtraProvide = 'insExtraProvide', //
  /**
   * @description insCreateDate
   * @field insCreateDate
   */
  insCreateDate = 'insCreateDate', //
  /**
   * @description insUpdateDate
   * @field insUpdateDate
   */
  insUpdateDate = 'insUpdateDate', //
}

/**
 * SYS_INSTRUMENT-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysInstrumentTable
 */
export enum SysInstrumentTable {
  /**
   * @description INS_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'INS_ID',
  /**
   * @description insId
   * @field primerKey
   */
  primerKey = 'insId',
  /**
   * @description SYS_INSTRUMENT
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_INSTRUMENT',
  /**
   * @description SysInstrument
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysInstrument',
}

/**
 * SYS_INSTRUMENT-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysInstrumentColNames = Object.keys(SysInstrumentColNameEnum);
/**
 * SYS_INSTRUMENT-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysInstrumentColProps = Object.keys(SysInstrumentColPropEnum);
/**
 * SYS_INSTRUMENT-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysInstrumentColMap = { names: SysInstrumentColNames, props: SysInstrumentColProps };
