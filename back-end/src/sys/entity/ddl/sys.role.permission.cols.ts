/**
 * SYS_ROLE_PERMISSION-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRolePermissionColNames
 */
export interface SysRolePermissionColNames {
  /**
   * @description 角色权限表Id
   * @field RP_ID
   */
  RP_ID: string; //
  /**
   * @description 角色ID号
   * @field RP_ROLE_ID
   */
  RP_ROLE_ID: string; //
  /**
   * @description 权限ID
   * @field RP_PERMISSION_ID
   */
  RP_PERMISSION_ID: string; //
}

/**
 * SYS_ROLE_PERMISSION-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRolePermissionColProps
 */
export interface SysRolePermissionColProps {
  /**
   * @description rpId
   * @field rpId
   */
  rpId: string; //
  /**
   * @description rpRoleId
   * @field rpRoleId
   */
  rpRoleId: string; //
  /**
   * @description rpPermissionId
   * @field rpPermissionId
   */
  rpPermissionId: string; //
}

/**
 * SYS_ROLE_PERMISSION-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRolePermissionCols
 */
export enum SysRolePermissionColNameEnum {
  /**
   * @description 角色权限表Id
   * @field RP_ID
   */
  RP_ID = 'RP_ID', //
  /**
   * @description 角色ID号
   * @field RP_ROLE_ID
   */
  RP_ROLE_ID = 'RP_ROLE_ID', //
  /**
   * @description 权限ID
   * @field RP_PERMISSION_ID
   */
  RP_PERMISSION_ID = 'RP_PERMISSION_ID', //
}

/**
 * SYS_ROLE_PERMISSION-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRolePermissionCols
 */
export enum SysRolePermissionColPropEnum {
  /**
   * @description rpId
   * @field rpId
   */
  rpId = 'rpId', //
  /**
   * @description rpRoleId
   * @field rpRoleId
   */
  rpRoleId = 'rpRoleId', //
  /**
   * @description rpPermissionId
   * @field rpPermissionId
   */
  rpPermissionId = 'rpPermissionId', //
}

/**
 * SYS_ROLE_PERMISSION-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysRolePermissionTable
 */
export enum SysRolePermissionTable {
  /**
   * @description RP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'RP_ID',
  /**
   * @description rpId
   * @field primerKey
   */
  primerKey = 'rpId',
  /**
   * @description SYS_ROLE_PERMISSION
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_ROLE_PERMISSION',
  /**
   * @description SysRolePermission
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysRolePermission',
}

/**
 * SYS_ROLE_PERMISSION-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRolePermissionColNames = Object.keys(SysRolePermissionColNameEnum);
/**
 * SYS_ROLE_PERMISSION-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRolePermissionColProps = Object.keys(SysRolePermissionColPropEnum);
/**
 * SYS_ROLE_PERMISSION-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRolePermissionColMap = { names: SysRolePermissionColNames, props: SysRolePermissionColProps };
