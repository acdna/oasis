/**
 * SYS_CMS_NOTICE-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsNoticeColNames
 */
export interface SysCmsNoticeColNames {
  /**
   * @description 公告编号
   * @field SC_NOTICE_ID
   */
  SC_NOTICE_ID: string; //
  /**
   * @description 公告标题
   * @field SC_NOTICE_TITLE
   */
  SC_NOTICE_TITLE: string; //
  /**
   * @description 公告内容
   * @field SC_NOTICE_CONTENT
   */
  SC_NOTICE_CONTENT: string; //
  /**
   * @description 是否是新文件
   * @field SC_NOTICE_IS_NEW
   */
  SC_NOTICE_IS_NEW: string; //
  /**
   * @description 公告发布者
   * @field SC_NOTICE_MANAGER
   */
  SC_NOTICE_MANAGER: string; //
  /**
   * @description 创建日期
   * @field SC_NOTICE_CREATE_DATE
   */
  SC_NOTICE_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field SC_NOTICE_UPDATE_DATE
   */
  SC_NOTICE_UPDATE_DATE: Date; //
}

/**
 * SYS_CMS_NOTICE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsNoticeColProps
 */
export interface SysCmsNoticeColProps {
  /**
   * @description scNoticeId
   * @field scNoticeId
   */
  scNoticeId: string; //
  /**
   * @description scNoticeTitle
   * @field scNoticeTitle
   */
  scNoticeTitle: string; //
  /**
   * @description scNoticeContent
   * @field scNoticeContent
   */
  scNoticeContent: string; //
  /**
   * @description scNoticeIsNew
   * @field scNoticeIsNew
   */
  scNoticeIsNew: string; //
  /**
   * @description scNoticeManager
   * @field scNoticeManager
   */
  scNoticeManager: string; //
  /**
   * @description scNoticeCreateDate
   * @field scNoticeCreateDate
   */
  scNoticeCreateDate: Date; //
  /**
   * @description scNoticeUpdateDate
   * @field scNoticeUpdateDate
   */
  scNoticeUpdateDate: Date; //
}

/**
 * SYS_CMS_NOTICE-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsNoticeCols
 */
export enum SysCmsNoticeColNameEnum {
  /**
   * @description 公告编号
   * @field SC_NOTICE_ID
   */
  SC_NOTICE_ID = 'SC_NOTICE_ID', //
  /**
   * @description 公告标题
   * @field SC_NOTICE_TITLE
   */
  SC_NOTICE_TITLE = 'SC_NOTICE_TITLE', //
  /**
   * @description 公告内容
   * @field SC_NOTICE_CONTENT
   */
  SC_NOTICE_CONTENT = 'SC_NOTICE_CONTENT', //
  /**
   * @description 是否是新文件
   * @field SC_NOTICE_IS_NEW
   */
  SC_NOTICE_IS_NEW = 'SC_NOTICE_IS_NEW', //
  /**
   * @description 公告发布者
   * @field SC_NOTICE_MANAGER
   */
  SC_NOTICE_MANAGER = 'SC_NOTICE_MANAGER', //
  /**
   * @description 创建日期
   * @field SC_NOTICE_CREATE_DATE
   */
  SC_NOTICE_CREATE_DATE = 'SC_NOTICE_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field SC_NOTICE_UPDATE_DATE
   */
  SC_NOTICE_UPDATE_DATE = 'SC_NOTICE_UPDATE_DATE', //
}

/**
 * SYS_CMS_NOTICE-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsNoticeCols
 */
export enum SysCmsNoticeColPropEnum {
  /**
   * @description scNoticeId
   * @field scNoticeId
   */
  scNoticeId = 'scNoticeId', //
  /**
   * @description scNoticeTitle
   * @field scNoticeTitle
   */
  scNoticeTitle = 'scNoticeTitle', //
  /**
   * @description scNoticeContent
   * @field scNoticeContent
   */
  scNoticeContent = 'scNoticeContent', //
  /**
   * @description scNoticeIsNew
   * @field scNoticeIsNew
   */
  scNoticeIsNew = 'scNoticeIsNew', //
  /**
   * @description scNoticeManager
   * @field scNoticeManager
   */
  scNoticeManager = 'scNoticeManager', //
  /**
   * @description scNoticeCreateDate
   * @field scNoticeCreateDate
   */
  scNoticeCreateDate = 'scNoticeCreateDate', //
  /**
   * @description scNoticeUpdateDate
   * @field scNoticeUpdateDate
   */
  scNoticeUpdateDate = 'scNoticeUpdateDate', //
}

/**
 * SYS_CMS_NOTICE-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysCmsNoticeTable
 */
export enum SysCmsNoticeTable {
  /**
   * @description SC_NOTICE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'SC_NOTICE_ID',
  /**
   * @description scNoticeId
   * @field primerKey
   */
  primerKey = 'scNoticeId',
  /**
   * @description SYS_CMS_NOTICE
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_CMS_NOTICE',
  /**
   * @description SysCmsNotice
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysCmsNotice',
}

/**
 * SYS_CMS_NOTICE-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCmsNoticeColNames = Object.keys(SysCmsNoticeColNameEnum);
/**
 * SYS_CMS_NOTICE-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCmsNoticeColProps = Object.keys(SysCmsNoticeColPropEnum);
/**
 * SYS_CMS_NOTICE-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCmsNoticeColMap = { names: SysCmsNoticeColNames, props: SysCmsNoticeColProps };
