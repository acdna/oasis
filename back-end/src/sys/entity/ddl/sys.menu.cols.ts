/**
 * SYS_MENU-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMenuColNames
 */
export interface SysMenuColNames {
  /**
   * @description 菜单ID号
   * @field MENU_ID
   */
  MENU_ID: string; //
  /**
   * @description 菜单名称
   * @field MENU_NAME
   */
  MENU_NAME: string; //
  /**
   * @description 英文菜单名
   * @field MENU_NAME_EN
   */
  MENU_NAME_EN: string; //
  /**
   * @description 菜单连接地址
   * @field MENU_URL
   */
  MENU_URL: string; //
  /**
   * @description 父菜单ID
   * @field MENU_PARENT_ID
   */
  MENU_PARENT_ID: string; //
  /**
   * @description 菜单顺序
   * @field MENU_ORDER
   */
  MENU_ORDER: number; //
  /**
   * @description 菜单备注
   * @field MENU_REMARK
   */
  MENU_REMARK: string; //
  /**
   * @description 菜单状态
   * @field MENU_STATE
   */
  MENU_STATE: string; //
  /**
   * @description 菜单类型
   * @field MENU_TYPE
   */
  MENU_TYPE: string; //
  /**
   * @description 菜单图标CSS名称
   * @field MENU_ICON_CLASS
   */
  MENU_ICON_CLASS: string; //
}

/**
 * SYS_MENU-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMenuColProps
 */
export interface SysMenuColProps {
  /**
   * @description menuId
   * @field menuId
   */
  menuId: string; //
  /**
   * @description menuName
   * @field menuName
   */
  menuName: string; //
  /**
   * @description menuNameEn
   * @field menuNameEn
   */
  menuNameEn: string; //
  /**
   * @description menuUrl
   * @field menuUrl
   */
  menuUrl: string; //
  /**
   * @description menuParentId
   * @field menuParentId
   */
  menuParentId: string; //
  /**
   * @description menuOrder
   * @field menuOrder
   */
  menuOrder: number; //
  /**
   * @description menuRemark
   * @field menuRemark
   */
  menuRemark: string; //
  /**
   * @description menuState
   * @field menuState
   */
  menuState: string; //
  /**
   * @description menuType
   * @field menuType
   */
  menuType: string; //
  /**
   * @description menuIconClass
   * @field menuIconClass
   */
  menuIconClass: string; //
}

/**
 * SYS_MENU-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMenuCols
 */
export enum SysMenuColNameEnum {
  /**
   * @description 菜单ID号
   * @field MENU_ID
   */
  MENU_ID = 'MENU_ID', //
  /**
   * @description 菜单名称
   * @field MENU_NAME
   */
  MENU_NAME = 'MENU_NAME', //
  /**
   * @description 英文菜单名
   * @field MENU_NAME_EN
   */
  MENU_NAME_EN = 'MENU_NAME_EN', //
  /**
   * @description 菜单连接地址
   * @field MENU_URL
   */
  MENU_URL = 'MENU_URL', //
  /**
   * @description 父菜单ID
   * @field MENU_PARENT_ID
   */
  MENU_PARENT_ID = 'MENU_PARENT_ID', //
  /**
   * @description 菜单顺序
   * @field MENU_ORDER
   */
  MENU_ORDER = 'MENU_ORDER', //
  /**
   * @description 菜单备注
   * @field MENU_REMARK
   */
  MENU_REMARK = 'MENU_REMARK', //
  /**
   * @description 菜单状态
   * @field MENU_STATE
   */
  MENU_STATE = 'MENU_STATE', //
  /**
   * @description 菜单类型
   * @field MENU_TYPE
   */
  MENU_TYPE = 'MENU_TYPE', //
  /**
   * @description 菜单图标CSS名称
   * @field MENU_ICON_CLASS
   */
  MENU_ICON_CLASS = 'MENU_ICON_CLASS', //
}

/**
 * SYS_MENU-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMenuCols
 */
export enum SysMenuColPropEnum {
  /**
   * @description menuId
   * @field menuId
   */
  menuId = 'menuId', //
  /**
   * @description menuName
   * @field menuName
   */
  menuName = 'menuName', //
  /**
   * @description menuNameEn
   * @field menuNameEn
   */
  menuNameEn = 'menuNameEn', //
  /**
   * @description menuUrl
   * @field menuUrl
   */
  menuUrl = 'menuUrl', //
  /**
   * @description menuParentId
   * @field menuParentId
   */
  menuParentId = 'menuParentId', //
  /**
   * @description menuOrder
   * @field menuOrder
   */
  menuOrder = 'menuOrder', //
  /**
   * @description menuRemark
   * @field menuRemark
   */
  menuRemark = 'menuRemark', //
  /**
   * @description menuState
   * @field menuState
   */
  menuState = 'menuState', //
  /**
   * @description menuType
   * @field menuType
   */
  menuType = 'menuType', //
  /**
   * @description menuIconClass
   * @field menuIconClass
   */
  menuIconClass = 'menuIconClass', //
}

/**
 * SYS_MENU-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysMenuTable
 */
export enum SysMenuTable {
  /**
   * @description MENU_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'MENU_ID',
  /**
   * @description menuId
   * @field primerKey
   */
  primerKey = 'menuId',
  /**
   * @description SYS_MENU
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_MENU',
  /**
   * @description SysMenu
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysMenu',
}

/**
 * SYS_MENU-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysMenuColNames = Object.keys(SysMenuColNameEnum);
/**
 * SYS_MENU-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysMenuColProps = Object.keys(SysMenuColPropEnum);
/**
 * SYS_MENU-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysMenuColMap = { names: SysMenuColNames, props: SysMenuColProps };
