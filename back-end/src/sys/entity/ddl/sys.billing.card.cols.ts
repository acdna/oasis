/**
 * SYS_BILLING_CARD-计费卡信息表表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysBillingCardColNames
 */
export interface SysBillingCardColNames {
  /**
   * @description id
   * @field BC_ID
   */
  BC_ID: string; //
  /**
   * @description 序列号信息
   * @field BC_SERIAL_NUMBER
   */
  BC_SERIAL_NUMBER: string; //
  /**
   * @description 总额
   * @field BC_AMOUNT
   */
  BC_AMOUNT: string; //
  /**
   * @description 总时间
   * @field BC_TOTAL_TIME
   */
  BC_TOTAL_TIME: number; //
  /**
   * @description 类型
   * @field BC_TYPE
   */
  BC_TYPE: string; //
  /**
   * @description 绑定帐号
   * @field BC_ACCOUNT
   */
  BC_ACCOUNT: string; //
  /**
   * @description 绑定密码
   * @field BC_PASSWORD
   */
  BC_PASSWORD: string; //
  /**
   * @description 状态
   * @field BC_STATE
   */
  BC_STATE: string; //
  /**
   * @description 最大指纹数
   * @field BC_BIGGEST_GENE_COUNT
   */
  BC_BIGGEST_GENE_COUNT: string; //
  /**
   * @description 创建日期
   * @field BC_CREATE_DATE
   */
  BC_CREATE_DATE: Date; //
}

/**
 * SYS_BILLING_CARD-计费卡信息表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysBillingCardColProps
 */
export interface SysBillingCardColProps {
  /**
   * @description bcId
   * @field bcId
   */
  bcId: string; //
  /**
   * @description bcSerialNumber
   * @field bcSerialNumber
   */
  bcSerialNumber: string; //
  /**
   * @description bcAmount
   * @field bcAmount
   */
  bcAmount: string; //
  /**
   * @description bcTotalTime
   * @field bcTotalTime
   */
  bcTotalTime: number; //
  /**
   * @description bcType
   * @field bcType
   */
  bcType: string; //
  /**
   * @description bcAccount
   * @field bcAccount
   */
  bcAccount: string; //
  /**
   * @description bcPassword
   * @field bcPassword
   */
  bcPassword: string; //
  /**
   * @description bcState
   * @field bcState
   */
  bcState: string; //
  /**
   * @description bcBiggestGeneCount
   * @field bcBiggestGeneCount
   */
  bcBiggestGeneCount: string; //
  /**
   * @description bcCreateDate
   * @field bcCreateDate
   */
  bcCreateDate: Date; //
}

/**
 * SYS_BILLING_CARD-计费卡信息表表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysBillingCardCols
 */
export enum SysBillingCardColNameEnum {
  /**
   * @description id
   * @field BC_ID
   */
  BC_ID = 'BC_ID', //
  /**
   * @description 序列号信息
   * @field BC_SERIAL_NUMBER
   */
  BC_SERIAL_NUMBER = 'BC_SERIAL_NUMBER', //
  /**
   * @description 总额
   * @field BC_AMOUNT
   */
  BC_AMOUNT = 'BC_AMOUNT', //
  /**
   * @description 总时间
   * @field BC_TOTAL_TIME
   */
  BC_TOTAL_TIME = 'BC_TOTAL_TIME', //
  /**
   * @description 类型
   * @field BC_TYPE
   */
  BC_TYPE = 'BC_TYPE', //
  /**
   * @description 绑定帐号
   * @field BC_ACCOUNT
   */
  BC_ACCOUNT = 'BC_ACCOUNT', //
  /**
   * @description 绑定密码
   * @field BC_PASSWORD
   */
  BC_PASSWORD = 'BC_PASSWORD', //
  /**
   * @description 状态
   * @field BC_STATE
   */
  BC_STATE = 'BC_STATE', //
  /**
   * @description 最大指纹数
   * @field BC_BIGGEST_GENE_COUNT
   */
  BC_BIGGEST_GENE_COUNT = 'BC_BIGGEST_GENE_COUNT', //
  /**
   * @description 创建日期
   * @field BC_CREATE_DATE
   */
  BC_CREATE_DATE = 'BC_CREATE_DATE', //
}

/**
 * SYS_BILLING_CARD-计费卡信息表表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysBillingCardCols
 */
export enum SysBillingCardColPropEnum {
  /**
   * @description bcId
   * @field bcId
   */
  bcId = 'bcId', //
  /**
   * @description bcSerialNumber
   * @field bcSerialNumber
   */
  bcSerialNumber = 'bcSerialNumber', //
  /**
   * @description bcAmount
   * @field bcAmount
   */
  bcAmount = 'bcAmount', //
  /**
   * @description bcTotalTime
   * @field bcTotalTime
   */
  bcTotalTime = 'bcTotalTime', //
  /**
   * @description bcType
   * @field bcType
   */
  bcType = 'bcType', //
  /**
   * @description bcAccount
   * @field bcAccount
   */
  bcAccount = 'bcAccount', //
  /**
   * @description bcPassword
   * @field bcPassword
   */
  bcPassword = 'bcPassword', //
  /**
   * @description bcState
   * @field bcState
   */
  bcState = 'bcState', //
  /**
   * @description bcBiggestGeneCount
   * @field bcBiggestGeneCount
   */
  bcBiggestGeneCount = 'bcBiggestGeneCount', //
  /**
   * @description bcCreateDate
   * @field bcCreateDate
   */
  bcCreateDate = 'bcCreateDate', //
}

/**
 * SYS_BILLING_CARD-计费卡信息表表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysBillingCardTable
 */
export enum SysBillingCardTable {
  /**
   * @description BC_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'BC_ID',
  /**
   * @description bcId
   * @field primerKey
   */
  primerKey = 'bcId',
  /**
   * @description SYS_BILLING_CARD
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_BILLING_CARD',
  /**
   * @description SysBillingCard
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysBillingCard',
}

/**
 * SYS_BILLING_CARD-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysBillingCardColNames = Object.keys(SysBillingCardColNameEnum);
/**
 * SYS_BILLING_CARD-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysBillingCardColProps = Object.keys(SysBillingCardColPropEnum);
/**
 * SYS_BILLING_CARD-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysBillingCardColMap = { names: SysBillingCardColNames, props: SysBillingCardColProps };
