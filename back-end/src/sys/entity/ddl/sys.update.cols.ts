/**
 * SYS_UPDATE-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUpdateColNames
 */
export interface SysUpdateColNames {
  /**
   * @description 系统升级日志主键ID
   * @field UPDATE_ID
   */
  UPDATE_ID: string; //
  /**
   * @description 升级版本号
   * @field UPDATE_VERSION
   */
  UPDATE_VERSION: string; //
  /**
   * @description 升级内容
   * @field UPDATE_CONTENT
   */
  UPDATE_CONTENT: string; //
  /**
   * @description 升级负责人
   * @field UPDATE_MANAGER
   */
  UPDATE_MANAGER: string; //
  /**
   * @description 创建日期
   * @field UPDATE_CREATE_TIME
   */
  UPDATE_CREATE_TIME: Date; //
}

/**
 * SYS_UPDATE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUpdateColProps
 */
export interface SysUpdateColProps {
  /**
   * @description updateId
   * @field updateId
   */
  updateId: string; //
  /**
   * @description updateVersion
   * @field updateVersion
   */
  updateVersion: string; //
  /**
   * @description updateContent
   * @field updateContent
   */
  updateContent: string; //
  /**
   * @description updateManager
   * @field updateManager
   */
  updateManager: string; //
  /**
   * @description updateCreateTime
   * @field updateCreateTime
   */
  updateCreateTime: Date; //
}

/**
 * SYS_UPDATE-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUpdateCols
 */
export enum SysUpdateColNameEnum {
  /**
   * @description 系统升级日志主键ID
   * @field UPDATE_ID
   */
  UPDATE_ID = 'UPDATE_ID', //
  /**
   * @description 升级版本号
   * @field UPDATE_VERSION
   */
  UPDATE_VERSION = 'UPDATE_VERSION', //
  /**
   * @description 升级内容
   * @field UPDATE_CONTENT
   */
  UPDATE_CONTENT = 'UPDATE_CONTENT', //
  /**
   * @description 升级负责人
   * @field UPDATE_MANAGER
   */
  UPDATE_MANAGER = 'UPDATE_MANAGER', //
  /**
   * @description 创建日期
   * @field UPDATE_CREATE_TIME
   */
  UPDATE_CREATE_TIME = 'UPDATE_CREATE_TIME', //
}

/**
 * SYS_UPDATE-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUpdateCols
 */
export enum SysUpdateColPropEnum {
  /**
   * @description updateId
   * @field updateId
   */
  updateId = 'updateId', //
  /**
   * @description updateVersion
   * @field updateVersion
   */
  updateVersion = 'updateVersion', //
  /**
   * @description updateContent
   * @field updateContent
   */
  updateContent = 'updateContent', //
  /**
   * @description updateManager
   * @field updateManager
   */
  updateManager = 'updateManager', //
  /**
   * @description updateCreateTime
   * @field updateCreateTime
   */
  updateCreateTime = 'updateCreateTime', //
}

/**
 * SYS_UPDATE-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysUpdateTable
 */
export enum SysUpdateTable {
  /**
   * @description UPDATE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'UPDATE_ID',
  /**
   * @description updateId
   * @field primerKey
   */
  primerKey = 'updateId',
  /**
   * @description SYS_UPDATE
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_UPDATE',
  /**
   * @description SysUpdate
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysUpdate',
}

/**
 * SYS_UPDATE-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUpdateColNames = Object.keys(SysUpdateColNameEnum);
/**
 * SYS_UPDATE-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUpdateColProps = Object.keys(SysUpdateColPropEnum);
/**
 * SYS_UPDATE-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUpdateColMap = { names: SysUpdateColNames, props: SysUpdateColProps };
