/**
 * SYS_ROLE_DICTIONARY-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleDictionaryColNames
 */
export interface SysRoleDictionaryColNames {
  /**
   * @description 角色字典ID
   * @field RD_ID
   */
  RD_ID: string; //
  /**
   * @description 角色ID
   * @field RD_ROLE_ID
   */
  RD_ROLE_ID: string; //
  /**
   * @description 字典ID
   * @field RD_DICTIONARY_ID
   */
  RD_DICTIONARY_ID: string; //
}

/**
 * SYS_ROLE_DICTIONARY-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleDictionaryColProps
 */
export interface SysRoleDictionaryColProps {
  /**
   * @description rdId
   * @field rdId
   */
  rdId: string; //
  /**
   * @description rdRoleId
   * @field rdRoleId
   */
  rdRoleId: string; //
  /**
   * @description rdDictionaryId
   * @field rdDictionaryId
   */
  rdDictionaryId: string; //
}

/**
 * SYS_ROLE_DICTIONARY-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleDictionaryCols
 */
export enum SysRoleDictionaryColNameEnum {
  /**
   * @description 角色字典ID
   * @field RD_ID
   */
  RD_ID = 'RD_ID', //
  /**
   * @description 角色ID
   * @field RD_ROLE_ID
   */
  RD_ROLE_ID = 'RD_ROLE_ID', //
  /**
   * @description 字典ID
   * @field RD_DICTIONARY_ID
   */
  RD_DICTIONARY_ID = 'RD_DICTIONARY_ID', //
}

/**
 * SYS_ROLE_DICTIONARY-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleDictionaryCols
 */
export enum SysRoleDictionaryColPropEnum {
  /**
   * @description rdId
   * @field rdId
   */
  rdId = 'rdId', //
  /**
   * @description rdRoleId
   * @field rdRoleId
   */
  rdRoleId = 'rdRoleId', //
  /**
   * @description rdDictionaryId
   * @field rdDictionaryId
   */
  rdDictionaryId = 'rdDictionaryId', //
}

/**
 * SYS_ROLE_DICTIONARY-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysRoleDictionaryTable
 */
export enum SysRoleDictionaryTable {
  /**
   * @description RD_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'RD_ID',
  /**
   * @description rdId
   * @field primerKey
   */
  primerKey = 'rdId',
  /**
   * @description SYS_ROLE_DICTIONARY
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_ROLE_DICTIONARY',
  /**
   * @description SysRoleDictionary
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysRoleDictionary',
}

/**
 * SYS_ROLE_DICTIONARY-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleDictionaryColNames = Object.keys(SysRoleDictionaryColNameEnum);
/**
 * SYS_ROLE_DICTIONARY-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleDictionaryColProps = Object.keys(SysRoleDictionaryColPropEnum);
/**
 * SYS_ROLE_DICTIONARY-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleDictionaryColMap = { names: SysRoleDictionaryColNames, props: SysRoleDictionaryColProps };
