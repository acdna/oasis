/**
 * SYS_TIP-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysTipColNames
 */
export interface SysTipColNames {
  /**
   * @description 提示信息ID
   * @field TIP_ID
   */
  TIP_ID: string; //
  /**
   * @description 提示次数
   * @field TIP_COUNT
   */
  TIP_COUNT: number; //
  /**
   * @description 提示信息内容
   * @field TIP_CONTENTS
   */
  TIP_CONTENTS: string; //
  /**
   * @description 所属者
   * @field TIP_MANAGER
   */
  TIP_MANAGER: string; //
  /**
   * @description 消息受众
   * @field TIP_AUDIENCE
   */
  TIP_AUDIENCE: string; //
  /**
   * @description 创建日期
   * @field TIP_CREATE_DATE
   */
  TIP_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field TIP_UPDATE_DATE
   */
  TIP_UPDATE_DATE: Date; //
}

/**
 * SYS_TIP-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysTipColProps
 */
export interface SysTipColProps {
  /**
   * @description tipId
   * @field tipId
   */
  tipId: string; //
  /**
   * @description tipCount
   * @field tipCount
   */
  tipCount: number; //
  /**
   * @description tipContents
   * @field tipContents
   */
  tipContents: string; //
  /**
   * @description tipManager
   * @field tipManager
   */
  tipManager: string; //
  /**
   * @description tipAudience
   * @field tipAudience
   */
  tipAudience: string; //
  /**
   * @description tipCreateDate
   * @field tipCreateDate
   */
  tipCreateDate: Date; //
  /**
   * @description tipUpdateDate
   * @field tipUpdateDate
   */
  tipUpdateDate: Date; //
}

/**
 * SYS_TIP-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysTipCols
 */
export enum SysTipColNameEnum {
  /**
   * @description 提示信息ID
   * @field TIP_ID
   */
  TIP_ID = 'TIP_ID', //
  /**
   * @description 提示次数
   * @field TIP_COUNT
   */
  TIP_COUNT = 'TIP_COUNT', //
  /**
   * @description 提示信息内容
   * @field TIP_CONTENTS
   */
  TIP_CONTENTS = 'TIP_CONTENTS', //
  /**
   * @description 所属者
   * @field TIP_MANAGER
   */
  TIP_MANAGER = 'TIP_MANAGER', //
  /**
   * @description 消息受众
   * @field TIP_AUDIENCE
   */
  TIP_AUDIENCE = 'TIP_AUDIENCE', //
  /**
   * @description 创建日期
   * @field TIP_CREATE_DATE
   */
  TIP_CREATE_DATE = 'TIP_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field TIP_UPDATE_DATE
   */
  TIP_UPDATE_DATE = 'TIP_UPDATE_DATE', //
}

/**
 * SYS_TIP-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysTipCols
 */
export enum SysTipColPropEnum {
  /**
   * @description tipId
   * @field tipId
   */
  tipId = 'tipId', //
  /**
   * @description tipCount
   * @field tipCount
   */
  tipCount = 'tipCount', //
  /**
   * @description tipContents
   * @field tipContents
   */
  tipContents = 'tipContents', //
  /**
   * @description tipManager
   * @field tipManager
   */
  tipManager = 'tipManager', //
  /**
   * @description tipAudience
   * @field tipAudience
   */
  tipAudience = 'tipAudience', //
  /**
   * @description tipCreateDate
   * @field tipCreateDate
   */
  tipCreateDate = 'tipCreateDate', //
  /**
   * @description tipUpdateDate
   * @field tipUpdateDate
   */
  tipUpdateDate = 'tipUpdateDate', //
}

/**
 * SYS_TIP-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysTipTable
 */
export enum SysTipTable {
  /**
   * @description TIP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'TIP_ID',
  /**
   * @description tipId
   * @field primerKey
   */
  primerKey = 'tipId',
  /**
   * @description SYS_TIP
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_TIP',
  /**
   * @description SysTip
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysTip',
}

/**
 * SYS_TIP-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysTipColNames = Object.keys(SysTipColNameEnum);
/**
 * SYS_TIP-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysTipColProps = Object.keys(SysTipColPropEnum);
/**
 * SYS_TIP-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysTipColMap = { names: SysTipColNames, props: SysTipColProps };
