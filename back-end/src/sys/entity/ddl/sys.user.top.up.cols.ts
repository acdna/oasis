/**
 * SYS_USER_TOP_UP-用户充值信息表表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserTopUpColNames
 */
export interface SysUserTopUpColNames {
  /**
   * @description id
   * @field UTU_ID
   */
  UTU_ID: string; //
  /**
   * @description 用户登录名
   * @field UTU_LOGIN_NAME
   */
  UTU_LOGIN_NAME: string; //
  /**
   * @description 序列号
   * @field UTU_SERIAL_NUMBER
   */
  UTU_SERIAL_NUMBER: string; //
  /**
   * @description 计费开始时间
   * @field UTU_BILLING_START_DATE
   */
  UTU_BILLING_START_DATE: Date; //
  /**
   * @description 计费结束时间
   * @field UTU_BILLING_END_DATE
   */
  UTU_BILLING_END_DATE: Date; //
  /**
   * @description 状态
   * @field UTU_STATE
   */
  UTU_STATE: string; //
  /**
   * @description 创建日期
   * @field UTU_CREATE_DATE
   */
  UTU_CREATE_DATE: Date; //
}

/**
 * SYS_USER_TOP_UP-用户充值信息表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserTopUpColProps
 */
export interface SysUserTopUpColProps {
  /**
   * @description utuId
   * @field utuId
   */
  utuId: string; //
  /**
   * @description utuLoginName
   * @field utuLoginName
   */
  utuLoginName: string; //
  /**
   * @description utuSerialNumber
   * @field utuSerialNumber
   */
  utuSerialNumber: string; //
  /**
   * @description utuBillingStartDate
   * @field utuBillingStartDate
   */
  utuBillingStartDate: Date; //
  /**
   * @description utuBillingEndDate
   * @field utuBillingEndDate
   */
  utuBillingEndDate: Date; //
  /**
   * @description utuState
   * @field utuState
   */
  utuState: string; //
  /**
   * @description utuCreateDate
   * @field utuCreateDate
   */
  utuCreateDate: Date; //
}

/**
 * SYS_USER_TOP_UP-用户充值信息表表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserTopUpCols
 */
export enum SysUserTopUpColNameEnum {
  /**
   * @description id
   * @field UTU_ID
   */
  UTU_ID = 'UTU_ID', //
  /**
   * @description 用户登录名
   * @field UTU_LOGIN_NAME
   */
  UTU_LOGIN_NAME = 'UTU_LOGIN_NAME', //
  /**
   * @description 序列号
   * @field UTU_SERIAL_NUMBER
   */
  UTU_SERIAL_NUMBER = 'UTU_SERIAL_NUMBER', //
  /**
   * @description 计费开始时间
   * @field UTU_BILLING_START_DATE
   */
  UTU_BILLING_START_DATE = 'UTU_BILLING_START_DATE', //
  /**
   * @description 计费结束时间
   * @field UTU_BILLING_END_DATE
   */
  UTU_BILLING_END_DATE = 'UTU_BILLING_END_DATE', //
  /**
   * @description 状态
   * @field UTU_STATE
   */
  UTU_STATE = 'UTU_STATE', //
  /**
   * @description 创建日期
   * @field UTU_CREATE_DATE
   */
  UTU_CREATE_DATE = 'UTU_CREATE_DATE', //
}

/**
 * SYS_USER_TOP_UP-用户充值信息表表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserTopUpCols
 */
export enum SysUserTopUpColPropEnum {
  /**
   * @description utuId
   * @field utuId
   */
  utuId = 'utuId', //
  /**
   * @description utuLoginName
   * @field utuLoginName
   */
  utuLoginName = 'utuLoginName', //
  /**
   * @description utuSerialNumber
   * @field utuSerialNumber
   */
  utuSerialNumber = 'utuSerialNumber', //
  /**
   * @description utuBillingStartDate
   * @field utuBillingStartDate
   */
  utuBillingStartDate = 'utuBillingStartDate', //
  /**
   * @description utuBillingEndDate
   * @field utuBillingEndDate
   */
  utuBillingEndDate = 'utuBillingEndDate', //
  /**
   * @description utuState
   * @field utuState
   */
  utuState = 'utuState', //
  /**
   * @description utuCreateDate
   * @field utuCreateDate
   */
  utuCreateDate = 'utuCreateDate', //
}

/**
 * SYS_USER_TOP_UP-用户充值信息表表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysUserTopUpTable
 */
export enum SysUserTopUpTable {
  /**
   * @description UTU_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'UTU_ID',
  /**
   * @description utuId
   * @field primerKey
   */
  primerKey = 'utuId',
  /**
   * @description SYS_USER_TOP_UP
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_USER_TOP_UP',
  /**
   * @description SysUserTopUp
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysUserTopUp',
}

/**
 * SYS_USER_TOP_UP-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUserTopUpColNames = Object.keys(SysUserTopUpColNameEnum);
/**
 * SYS_USER_TOP_UP-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUserTopUpColProps = Object.keys(SysUserTopUpColPropEnum);
/**
 * SYS_USER_TOP_UP-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUserTopUpColMap = { names: SysUserTopUpColNames, props: SysUserTopUpColProps };
