/**
 * SYS_HELP-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysHelpColNames
 */
export interface SysHelpColNames {
  /**
   * @description 帮助编号
   * @field HELP_ID
   */
  HELP_ID: string; //
  /**
   * @description 帮助问题
   * @field HELP_QUESTION
   */
  HELP_QUESTION: string; //
  /**
   * @description 帮助回答
   * @field HELP_ANSWER
   */
  HELP_ANSWER: string; //
  /**
   * @description 问题编号父级编号
   * @field HELP_PARENT_ID
   */
  HELP_PARENT_ID: string; //
  /**
   * @description 帮助类别
   * @field HELP_TYPE
   */
  HELP_TYPE: string; //
  /**
   * @description 帮助匹配路径
   * @field HELP_PATH
   */
  HELP_PATH: string; //
  /**
   * @description 排序
   * @field HELP_SORT
   */
  HELP_SORT: string; //
  /**
   * @description 创建日期
   * @field HELP_CREATE_DATE
   */
  HELP_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field HELP_UPDATE_DATE
   */
  HELP_UPDATE_DATE: Date; //
}

/**
 * SYS_HELP-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysHelpColProps
 */
export interface SysHelpColProps {
  /**
   * @description helpId
   * @field helpId
   */
  helpId: string; //
  /**
   * @description helpQuestion
   * @field helpQuestion
   */
  helpQuestion: string; //
  /**
   * @description helpAnswer
   * @field helpAnswer
   */
  helpAnswer: string; //
  /**
   * @description helpParentId
   * @field helpParentId
   */
  helpParentId: string; //
  /**
   * @description helpType
   * @field helpType
   */
  helpType: string; //
  /**
   * @description helpPath
   * @field helpPath
   */
  helpPath: string; //
  /**
   * @description helpSort
   * @field helpSort
   */
  helpSort: string; //
  /**
   * @description helpCreateDate
   * @field helpCreateDate
   */
  helpCreateDate: Date; //
  /**
   * @description helpUpdateDate
   * @field helpUpdateDate
   */
  helpUpdateDate: Date; //
}

/**
 * SYS_HELP-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysHelpCols
 */
export enum SysHelpColNameEnum {
  /**
   * @description 帮助编号
   * @field HELP_ID
   */
  HELP_ID = 'HELP_ID', //
  /**
   * @description 帮助问题
   * @field HELP_QUESTION
   */
  HELP_QUESTION = 'HELP_QUESTION', //
  /**
   * @description 帮助回答
   * @field HELP_ANSWER
   */
  HELP_ANSWER = 'HELP_ANSWER', //
  /**
   * @description 问题编号父级编号
   * @field HELP_PARENT_ID
   */
  HELP_PARENT_ID = 'HELP_PARENT_ID', //
  /**
   * @description 帮助类别
   * @field HELP_TYPE
   */
  HELP_TYPE = 'HELP_TYPE', //
  /**
   * @description 帮助匹配路径
   * @field HELP_PATH
   */
  HELP_PATH = 'HELP_PATH', //
  /**
   * @description 排序
   * @field HELP_SORT
   */
  HELP_SORT = 'HELP_SORT', //
  /**
   * @description 创建日期
   * @field HELP_CREATE_DATE
   */
  HELP_CREATE_DATE = 'HELP_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field HELP_UPDATE_DATE
   */
  HELP_UPDATE_DATE = 'HELP_UPDATE_DATE', //
}

/**
 * SYS_HELP-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysHelpCols
 */
export enum SysHelpColPropEnum {
  /**
   * @description helpId
   * @field helpId
   */
  helpId = 'helpId', //
  /**
   * @description helpQuestion
   * @field helpQuestion
   */
  helpQuestion = 'helpQuestion', //
  /**
   * @description helpAnswer
   * @field helpAnswer
   */
  helpAnswer = 'helpAnswer', //
  /**
   * @description helpParentId
   * @field helpParentId
   */
  helpParentId = 'helpParentId', //
  /**
   * @description helpType
   * @field helpType
   */
  helpType = 'helpType', //
  /**
   * @description helpPath
   * @field helpPath
   */
  helpPath = 'helpPath', //
  /**
   * @description helpSort
   * @field helpSort
   */
  helpSort = 'helpSort', //
  /**
   * @description helpCreateDate
   * @field helpCreateDate
   */
  helpCreateDate = 'helpCreateDate', //
  /**
   * @description helpUpdateDate
   * @field helpUpdateDate
   */
  helpUpdateDate = 'helpUpdateDate', //
}

/**
 * SYS_HELP-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysHelpTable
 */
export enum SysHelpTable {
  /**
   * @description HELP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'HELP_ID',
  /**
   * @description helpId
   * @field primerKey
   */
  primerKey = 'helpId',
  /**
   * @description SYS_HELP
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_HELP',
  /**
   * @description SysHelp
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysHelp',
}

/**
 * SYS_HELP-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysHelpColNames = Object.keys(SysHelpColNameEnum);
/**
 * SYS_HELP-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysHelpColProps = Object.keys(SysHelpColPropEnum);
/**
 * SYS_HELP-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysHelpColMap = { names: SysHelpColNames, props: SysHelpColProps };
