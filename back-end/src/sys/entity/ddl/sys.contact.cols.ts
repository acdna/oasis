/**
 * SYS_CONTACT-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysContactColNames
 */
export interface SysContactColNames {
  /**
   * @description 反馈编号
   * @field CONTACT_ID
   */
  CONTACT_ID: string; //
  /**
   * @description 反馈用户
   * @field CONTACT_AUTHOR
   */
  CONTACT_AUTHOR: string; //
  /**
   * @description 反馈邮箱
   * @field CONTACT_EMAIL
   */
  CONTACT_EMAIL: string; //
  /**
   * @description 反馈主题
   * @field CONTACT_SUBJECT
   */
  CONTACT_SUBJECT: string; //
  /**
   * @description 反馈内容
   * @field CONTACT_TEXT
   */
  CONTACT_TEXT: string; //
  /**
   * @description 创建日期
   * @field CONTACT_CREATE_DATE
   */
  CONTACT_CREATE_DATE: Date; //
}

/**
 * SYS_CONTACT-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysContactColProps
 */
export interface SysContactColProps {
  /**
   * @description contactId
   * @field contactId
   */
  contactId: string; //
  /**
   * @description contactAuthor
   * @field contactAuthor
   */
  contactAuthor: string; //
  /**
   * @description contactEmail
   * @field contactEmail
   */
  contactEmail: string; //
  /**
   * @description contactSubject
   * @field contactSubject
   */
  contactSubject: string; //
  /**
   * @description contactText
   * @field contactText
   */
  contactText: string; //
  /**
   * @description contactCreateDate
   * @field contactCreateDate
   */
  contactCreateDate: Date; //
}

/**
 * SYS_CONTACT-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysContactCols
 */
export enum SysContactColNameEnum {
  /**
   * @description 反馈编号
   * @field CONTACT_ID
   */
  CONTACT_ID = 'CONTACT_ID', //
  /**
   * @description 反馈用户
   * @field CONTACT_AUTHOR
   */
  CONTACT_AUTHOR = 'CONTACT_AUTHOR', //
  /**
   * @description 反馈邮箱
   * @field CONTACT_EMAIL
   */
  CONTACT_EMAIL = 'CONTACT_EMAIL', //
  /**
   * @description 反馈主题
   * @field CONTACT_SUBJECT
   */
  CONTACT_SUBJECT = 'CONTACT_SUBJECT', //
  /**
   * @description 反馈内容
   * @field CONTACT_TEXT
   */
  CONTACT_TEXT = 'CONTACT_TEXT', //
  /**
   * @description 创建日期
   * @field CONTACT_CREATE_DATE
   */
  CONTACT_CREATE_DATE = 'CONTACT_CREATE_DATE', //
}

/**
 * SYS_CONTACT-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysContactCols
 */
export enum SysContactColPropEnum {
  /**
   * @description contactId
   * @field contactId
   */
  contactId = 'contactId', //
  /**
   * @description contactAuthor
   * @field contactAuthor
   */
  contactAuthor = 'contactAuthor', //
  /**
   * @description contactEmail
   * @field contactEmail
   */
  contactEmail = 'contactEmail', //
  /**
   * @description contactSubject
   * @field contactSubject
   */
  contactSubject = 'contactSubject', //
  /**
   * @description contactText
   * @field contactText
   */
  contactText = 'contactText', //
  /**
   * @description contactCreateDate
   * @field contactCreateDate
   */
  contactCreateDate = 'contactCreateDate', //
}

/**
 * SYS_CONTACT-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysContactTable
 */
export enum SysContactTable {
  /**
   * @description CONTACT_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'CONTACT_ID',
  /**
   * @description contactId
   * @field primerKey
   */
  primerKey = 'contactId',
  /**
   * @description SYS_CONTACT
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_CONTACT',
  /**
   * @description SysContact
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysContact',
}

/**
 * SYS_CONTACT-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysContactColNames = Object.keys(SysContactColNameEnum);
/**
 * SYS_CONTACT-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysContactColProps = Object.keys(SysContactColPropEnum);
/**
 * SYS_CONTACT-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysContactColMap = { names: SysContactColNames, props: SysContactColProps };
