/**
 * SYS_COL_DETAIL-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysColDetailColNames
 */
export interface SysColDetailColNames {
  /**
   * @description 主键ID
   * @field CD_ID
   */
  CD_ID: string; //
  /**
   * @description 表名
   * @field CD_TABLE_NAME
   */
  CD_TABLE_NAME: string; //
  /**
   * @description 列名
   * @field CD_COL_NAME
   */
  CD_COL_NAME: string; //
  /**
   * @description 列中文名
   * @field CD_DESC_NAME
   */
  CD_DESC_NAME: string; //
  /**
   * @description 字典名
   * @field CD_DICT_NAME
   */
  CD_DICT_NAME: string; //
  /**
   * @description 模式
   * @field CD_MODE
   */
  CD_MODE: string; //
  /**
   * @description 创建日期
   * @field CD_CREATE_DATE
   */
  CD_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field CD_UPDATE_DATE
   */
  CD_UPDATE_DATE: Date; //
}

/**
 * SYS_COL_DETAIL-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysColDetailColProps
 */
export interface SysColDetailColProps {
  /**
   * @description cdId
   * @field cdId
   */
  cdId: string; //
  /**
   * @description cdTableName
   * @field cdTableName
   */
  cdTableName: string; //
  /**
   * @description cdColName
   * @field cdColName
   */
  cdColName: string; //
  /**
   * @description cdDescName
   * @field cdDescName
   */
  cdDescName: string; //
  /**
   * @description cdDictName
   * @field cdDictName
   */
  cdDictName: string; //
  /**
   * @description cdMode
   * @field cdMode
   */
  cdMode: string; //
  /**
   * @description cdCreateDate
   * @field cdCreateDate
   */
  cdCreateDate: Date; //
  /**
   * @description cdUpdateDate
   * @field cdUpdateDate
   */
  cdUpdateDate: Date; //
}

/**
 * SYS_COL_DETAIL-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysColDetailCols
 */
export enum SysColDetailColNameEnum {
  /**
   * @description 主键ID
   * @field CD_ID
   */
  CD_ID = 'CD_ID', //
  /**
   * @description 表名
   * @field CD_TABLE_NAME
   */
  CD_TABLE_NAME = 'CD_TABLE_NAME', //
  /**
   * @description 列名
   * @field CD_COL_NAME
   */
  CD_COL_NAME = 'CD_COL_NAME', //
  /**
   * @description 列中文名
   * @field CD_DESC_NAME
   */
  CD_DESC_NAME = 'CD_DESC_NAME', //
  /**
   * @description 字典名
   * @field CD_DICT_NAME
   */
  CD_DICT_NAME = 'CD_DICT_NAME', //
  /**
   * @description 模式
   * @field CD_MODE
   */
  CD_MODE = 'CD_MODE', //
  /**
   * @description 创建日期
   * @field CD_CREATE_DATE
   */
  CD_CREATE_DATE = 'CD_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field CD_UPDATE_DATE
   */
  CD_UPDATE_DATE = 'CD_UPDATE_DATE', //
}

/**
 * SYS_COL_DETAIL-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysColDetailCols
 */
export enum SysColDetailColPropEnum {
  /**
   * @description cdId
   * @field cdId
   */
  cdId = 'cdId', //
  /**
   * @description cdTableName
   * @field cdTableName
   */
  cdTableName = 'cdTableName', //
  /**
   * @description cdColName
   * @field cdColName
   */
  cdColName = 'cdColName', //
  /**
   * @description cdDescName
   * @field cdDescName
   */
  cdDescName = 'cdDescName', //
  /**
   * @description cdDictName
   * @field cdDictName
   */
  cdDictName = 'cdDictName', //
  /**
   * @description cdMode
   * @field cdMode
   */
  cdMode = 'cdMode', //
  /**
   * @description cdCreateDate
   * @field cdCreateDate
   */
  cdCreateDate = 'cdCreateDate', //
  /**
   * @description cdUpdateDate
   * @field cdUpdateDate
   */
  cdUpdateDate = 'cdUpdateDate', //
}

/**
 * SYS_COL_DETAIL-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysColDetailTable
 */
export enum SysColDetailTable {
  /**
   * @description CD_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'CD_ID',
  /**
   * @description cdId
   * @field primerKey
   */
  primerKey = 'cdId',
  /**
   * @description SYS_COL_DETAIL
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_COL_DETAIL',
  /**
   * @description SysColDetail
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysColDetail',
}

/**
 * SYS_COL_DETAIL-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysColDetailColNames = Object.keys(SysColDetailColNameEnum);
/**
 * SYS_COL_DETAIL-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysColDetailColProps = Object.keys(SysColDetailColPropEnum);
/**
 * SYS_COL_DETAIL-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysColDetailColMap = { names: SysColDetailColNames, props: SysColDetailColProps };
