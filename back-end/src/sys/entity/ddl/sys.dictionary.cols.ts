/**
 * SYS_DICTIONARY-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysDictionaryColNames
 */
export interface SysDictionaryColNames {
  /**
   * @description ID
   * @field DIC_ID
   */
  DIC_ID: string; //
  /**
   * @description 字典名称
   * @field DIC_NAME
   */
  DIC_NAME: string; //
  /**
   * @description
   * @field DIC_NAME_EN
   */
  DIC_NAME_EN: string; //
  /**
   * @description 字典值
   * @field DIC_VALUE
   */
  DIC_VALUE: string; //
  /**
   * @description 所属组
   * @field DIC_GROUP
   */
  DIC_GROUP: string; //
  /**
   * @description 模块名
   * @field DIC_MODULE
   */
  DIC_MODULE: string; //
  /**
   * @description 父级ID
   * @field DIC_PARENT_ID
   */
  DIC_PARENT_ID: string; //
  /**
   * @description 字典类型
   * @field DIC_TYPE
   */
  DIC_TYPE: string; //
  /**
   * @description 排序
   * @field DIC_ORDER
   */
  DIC_ORDER: number; //
  /**
   * @description 状态
   * @field DIC_STATE
   */
  DIC_STATE: string; //
  /**
   * @description
   * @field DIC_PARAMS
   */
  DIC_PARAMS: string; //
  /**
   * @description 关联种属
   * @field DIC_SPECIES
   */
  DIC_SPECIES: string; //
  /**
   * @description 创建日期
   * @field DIC_CREATE_DATE
   */
  DIC_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field DIC_UPDATE_DATE
   */
  DIC_UPDATE_DATE: Date; //
}

/**
 * SYS_DICTIONARY-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysDictionaryColProps
 */
export interface SysDictionaryColProps {
  /**
   * @description dicId
   * @field dicId
   */
  dicId: string; //
  /**
   * @description dicName
   * @field dicName
   */
  dicName: string; //
  /**
   * @description dicNameEn
   * @field dicNameEn
   */
  dicNameEn: string; //
  /**
   * @description dicValue
   * @field dicValue
   */
  dicValue: string; //
  /**
   * @description dicGroup
   * @field dicGroup
   */
  dicGroup: string; //
  /**
   * @description dicModule
   * @field dicModule
   */
  dicModule: string; //
  /**
   * @description dicParentId
   * @field dicParentId
   */
  dicParentId: string; //
  /**
   * @description dicType
   * @field dicType
   */
  dicType: string; //
  /**
   * @description dicOrder
   * @field dicOrder
   */
  dicOrder: number; //
  /**
   * @description dicState
   * @field dicState
   */
  dicState: string; //
  /**
   * @description dicParams
   * @field dicParams
   */
  dicParams: string; //
  /**
   * @description dicSpecies
   * @field dicSpecies
   */
  dicSpecies: string; //
  /**
   * @description dicCreateDate
   * @field dicCreateDate
   */
  dicCreateDate: Date; //
  /**
   * @description dicUpdateDate
   * @field dicUpdateDate
   */
  dicUpdateDate: Date; //
}

/**
 * SYS_DICTIONARY-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysDictionaryCols
 */
export enum SysDictionaryColNameEnum {
  /**
   * @description ID
   * @field DIC_ID
   */
  DIC_ID = 'DIC_ID', //
  /**
   * @description 字典名称
   * @field DIC_NAME
   */
  DIC_NAME = 'DIC_NAME', //
  /**
   * @description
   * @field DIC_NAME_EN
   */
  DIC_NAME_EN = 'DIC_NAME_EN', //
  /**
   * @description 字典值
   * @field DIC_VALUE
   */
  DIC_VALUE = 'DIC_VALUE', //
  /**
   * @description 所属组
   * @field DIC_GROUP
   */
  DIC_GROUP = 'DIC_GROUP', //
  /**
   * @description 模块名
   * @field DIC_MODULE
   */
  DIC_MODULE = 'DIC_MODULE', //
  /**
   * @description 父级ID
   * @field DIC_PARENT_ID
   */
  DIC_PARENT_ID = 'DIC_PARENT_ID', //
  /**
   * @description 字典类型
   * @field DIC_TYPE
   */
  DIC_TYPE = 'DIC_TYPE', //
  /**
   * @description 排序
   * @field DIC_ORDER
   */
  DIC_ORDER = 'DIC_ORDER', //
  /**
   * @description 状态
   * @field DIC_STATE
   */
  DIC_STATE = 'DIC_STATE', //
  /**
   * @description
   * @field DIC_PARAMS
   */
  DIC_PARAMS = 'DIC_PARAMS', //
  /**
   * @description 关联种属
   * @field DIC_SPECIES
   */
  DIC_SPECIES = 'DIC_SPECIES', //
  /**
   * @description 创建日期
   * @field DIC_CREATE_DATE
   */
  DIC_CREATE_DATE = 'DIC_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field DIC_UPDATE_DATE
   */
  DIC_UPDATE_DATE = 'DIC_UPDATE_DATE', //
}

/**
 * SYS_DICTIONARY-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysDictionaryCols
 */
export enum SysDictionaryColPropEnum {
  /**
   * @description dicId
   * @field dicId
   */
  dicId = 'dicId', //
  /**
   * @description dicName
   * @field dicName
   */
  dicName = 'dicName', //
  /**
   * @description dicNameEn
   * @field dicNameEn
   */
  dicNameEn = 'dicNameEn', //
  /**
   * @description dicValue
   * @field dicValue
   */
  dicValue = 'dicValue', //
  /**
   * @description dicGroup
   * @field dicGroup
   */
  dicGroup = 'dicGroup', //
  /**
   * @description dicModule
   * @field dicModule
   */
  dicModule = 'dicModule', //
  /**
   * @description dicParentId
   * @field dicParentId
   */
  dicParentId = 'dicParentId', //
  /**
   * @description dicType
   * @field dicType
   */
  dicType = 'dicType', //
  /**
   * @description dicOrder
   * @field dicOrder
   */
  dicOrder = 'dicOrder', //
  /**
   * @description dicState
   * @field dicState
   */
  dicState = 'dicState', //
  /**
   * @description dicParams
   * @field dicParams
   */
  dicParams = 'dicParams', //
  /**
   * @description dicSpecies
   * @field dicSpecies
   */
  dicSpecies = 'dicSpecies', //
  /**
   * @description dicCreateDate
   * @field dicCreateDate
   */
  dicCreateDate = 'dicCreateDate', //
  /**
   * @description dicUpdateDate
   * @field dicUpdateDate
   */
  dicUpdateDate = 'dicUpdateDate', //
}

/**
 * SYS_DICTIONARY-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysDictionaryTable
 */
export enum SysDictionaryTable {
  /**
   * @description DIC_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'DIC_ID',
  /**
   * @description dicId
   * @field primerKey
   */
  primerKey = 'dicId',
  /**
   * @description SYS_DICTIONARY
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_DICTIONARY',
  /**
   * @description SysDictionary
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysDictionary',
}

/**
 * SYS_DICTIONARY-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysDictionaryColNames = Object.keys(SysDictionaryColNameEnum);
/**
 * SYS_DICTIONARY-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysDictionaryColProps = Object.keys(SysDictionaryColPropEnum);
/**
 * SYS_DICTIONARY-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysDictionaryColMap = { names: SysDictionaryColNames, props: SysDictionaryColProps };
