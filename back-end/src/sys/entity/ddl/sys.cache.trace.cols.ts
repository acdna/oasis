/**
 * SYS_CACHE_TRACE-缓存信息表表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCacheTraceColNames
 */
export interface SysCacheTraceColNames {
  /**
   * @description 缓存记录ID
   * @field CT_ID
   */
  CT_ID: string; //
  /**
   * @description 缓存类型代码
   * @field CT_CODE
   */
  CT_CODE: string; //
  /**
   * @description 缓存操作类型代码
   * @field CT_OPERATE
   */
  CT_OPERATE: string; //
  /**
   * @description 缓存需要操作的目标记录ID
   * @field CT_TARGET_ID
   */
  CT_TARGET_ID: string; //
  /**
   * @description 创建日期
   * @field CT_CREATE_DATE
   */
  CT_CREATE_DATE: Date; //
  /**
   * @description 缓存备注信息
   * @field CT_COMMENT
   */
  CT_COMMENT: string; //
}

/**
 * SYS_CACHE_TRACE-缓存信息表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCacheTraceColProps
 */
export interface SysCacheTraceColProps {
  /**
   * @description ctId
   * @field ctId
   */
  ctId: string; //
  /**
   * @description ctCode
   * @field ctCode
   */
  ctCode: string; //
  /**
   * @description ctOperate
   * @field ctOperate
   */
  ctOperate: string; //
  /**
   * @description ctTargetId
   * @field ctTargetId
   */
  ctTargetId: string; //
  /**
   * @description ctCreateDate
   * @field ctCreateDate
   */
  ctCreateDate: Date; //
  /**
   * @description ctComment
   * @field ctComment
   */
  ctComment: string; //
}

/**
 * SYS_CACHE_TRACE-缓存信息表表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCacheTraceCols
 */
export enum SysCacheTraceColNameEnum {
  /**
   * @description 缓存记录ID
   * @field CT_ID
   */
  CT_ID = 'CT_ID', //
  /**
   * @description 缓存类型代码
   * @field CT_CODE
   */
  CT_CODE = 'CT_CODE', //
  /**
   * @description 缓存操作类型代码
   * @field CT_OPERATE
   */
  CT_OPERATE = 'CT_OPERATE', //
  /**
   * @description 缓存需要操作的目标记录ID
   * @field CT_TARGET_ID
   */
  CT_TARGET_ID = 'CT_TARGET_ID', //
  /**
   * @description 创建日期
   * @field CT_CREATE_DATE
   */
  CT_CREATE_DATE = 'CT_CREATE_DATE', //
  /**
   * @description 缓存备注信息
   * @field CT_COMMENT
   */
  CT_COMMENT = 'CT_COMMENT', //
}

/**
 * SYS_CACHE_TRACE-缓存信息表表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCacheTraceCols
 */
export enum SysCacheTraceColPropEnum {
  /**
   * @description ctId
   * @field ctId
   */
  ctId = 'ctId', //
  /**
   * @description ctCode
   * @field ctCode
   */
  ctCode = 'ctCode', //
  /**
   * @description ctOperate
   * @field ctOperate
   */
  ctOperate = 'ctOperate', //
  /**
   * @description ctTargetId
   * @field ctTargetId
   */
  ctTargetId = 'ctTargetId', //
  /**
   * @description ctCreateDate
   * @field ctCreateDate
   */
  ctCreateDate = 'ctCreateDate', //
  /**
   * @description ctComment
   * @field ctComment
   */
  ctComment = 'ctComment', //
}

/**
 * SYS_CACHE_TRACE-缓存信息表表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysCacheTraceTable
 */
export enum SysCacheTraceTable {
  /**
   * @description CT_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'CT_ID',
  /**
   * @description ctId
   * @field primerKey
   */
  primerKey = 'ctId',
  /**
   * @description SYS_CACHE_TRACE
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_CACHE_TRACE',
  /**
   * @description SysCacheTrace
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysCacheTrace',
}

/**
 * SYS_CACHE_TRACE-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCacheTraceColNames = Object.keys(SysCacheTraceColNameEnum);
/**
 * SYS_CACHE_TRACE-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCacheTraceColProps = Object.keys(SysCacheTraceColPropEnum);
/**
 * SYS_CACHE_TRACE-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCacheTraceColMap = { names: SysCacheTraceColNames, props: SysCacheTraceColProps };
