/**
 * SYS_FILES-文件管理表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysFilesColNames
 */
export interface SysFilesColNames {
  /**
   * @description 记录ID
   * @field FILE_ID
   */
  FILE_ID: string; //
  /**
   * @description 文件条码号
   * @field FILE_BARCODE
   */
  FILE_BARCODE: string; //
  /**
   * @description 关联的其它记录信息ID号
   * @field FILE_RELATE_ID
   */
  FILE_RELATE_ID: string; //
  /**
   * @description 文件名称
   * @field FILE_NAME
   */
  FILE_NAME: string; //
  /**
   * @description 文件名
   * @field FILE_NAME_EN
   */
  FILE_NAME_EN: string; //
  /**
   * @description 文件存储路径
   * @field FILE_PATH
   */
  FILE_PATH: string; //
  /**
   * @description 创建日期
   * @field FILE_CREATE_DATE
   */
  FILE_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field FILE_UPDATE_DATE
   */
  FILE_UPDATE_DATE: Date; //
  /**
   * @description 是否被启用
   * @field FILE_IS_USED
   */
  FILE_IS_USED: string; //
  /**
   * @description 所属用户
   * @field FILE_OWNER
   */
  FILE_OWNER: string; //
  /**
   * @description 文件大小
   * @field FILE_SIZE
   */
  FILE_SIZE: number; //
  /**
   * @description 用户权限表
   * @field FILE_GRANTS
   */
  FILE_GRANTS: string; //
  /**
   * @description 文件分类
   * @field FILE_CLASSES
   */
  FILE_CLASSES: string; //
  /**
   * @description 文件状态信息
   * @field FILE_STATE
   */
  FILE_STATE: string; //
  /**
   * @description 文件类型
   * @field FILE_TYPE
   */
  FILE_TYPE: string; //
  /**
   * @description 文件备注信息
   * @field FILE_COMMENTS
   */
  FILE_COMMENTS: string; //
  /**
   * @description 种属
   * @field FILE_SPECIES
   */
  FILE_SPECIES: string; //
}

/**
 * SYS_FILES-文件管理表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysFilesColProps
 */
export interface SysFilesColProps {
  /**
   * @description fileId
   * @field fileId
   */
  fileId: string; //
  /**
   * @description fileBarcode
   * @field fileBarcode
   */
  fileBarcode: string; //
  /**
   * @description fileRelateId
   * @field fileRelateId
   */
  fileRelateId: string; //
  /**
   * @description fileName
   * @field fileName
   */
  fileName: string; //
  /**
   * @description fileNameEn
   * @field fileNameEn
   */
  fileNameEn: string; //
  /**
   * @description filePath
   * @field filePath
   */
  filePath: string; //
  /**
   * @description fileCreateDate
   * @field fileCreateDate
   */
  fileCreateDate: Date; //
  /**
   * @description fileUpdateDate
   * @field fileUpdateDate
   */
  fileUpdateDate: Date; //
  /**
   * @description fileIsUsed
   * @field fileIsUsed
   */
  fileIsUsed: string; //
  /**
   * @description fileOwner
   * @field fileOwner
   */
  fileOwner: string; //
  /**
   * @description fileSize
   * @field fileSize
   */
  fileSize: number; //
  /**
   * @description fileGrants
   * @field fileGrants
   */
  fileGrants: string; //
  /**
   * @description fileClasses
   * @field fileClasses
   */
  fileClasses: string; //
  /**
   * @description fileState
   * @field fileState
   */
  fileState: string; //
  /**
   * @description fileType
   * @field fileType
   */
  fileType: string; //
  /**
   * @description fileComments
   * @field fileComments
   */
  fileComments: string; //
  /**
   * @description fileSpecies
   * @field fileSpecies
   */
  fileSpecies: string; //
}

/**
 * SYS_FILES-文件管理表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysFilesCols
 */
export enum SysFilesColNameEnum {
  /**
   * @description 记录ID
   * @field FILE_ID
   */
  FILE_ID = 'FILE_ID', //
  /**
   * @description 文件条码号
   * @field FILE_BARCODE
   */
  FILE_BARCODE = 'FILE_BARCODE', //
  /**
   * @description 关联的其它记录信息ID号
   * @field FILE_RELATE_ID
   */
  FILE_RELATE_ID = 'FILE_RELATE_ID', //
  /**
   * @description 文件名称
   * @field FILE_NAME
   */
  FILE_NAME = 'FILE_NAME', //
  /**
   * @description 文件名
   * @field FILE_NAME_EN
   */
  FILE_NAME_EN = 'FILE_NAME_EN', //
  /**
   * @description 文件存储路径
   * @field FILE_PATH
   */
  FILE_PATH = 'FILE_PATH', //
  /**
   * @description 创建日期
   * @field FILE_CREATE_DATE
   */
  FILE_CREATE_DATE = 'FILE_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field FILE_UPDATE_DATE
   */
  FILE_UPDATE_DATE = 'FILE_UPDATE_DATE', //
  /**
   * @description 是否被启用
   * @field FILE_IS_USED
   */
  FILE_IS_USED = 'FILE_IS_USED', //
  /**
   * @description 所属用户
   * @field FILE_OWNER
   */
  FILE_OWNER = 'FILE_OWNER', //
  /**
   * @description 文件大小
   * @field FILE_SIZE
   */
  FILE_SIZE = 'FILE_SIZE', //
  /**
   * @description 用户权限表
   * @field FILE_GRANTS
   */
  FILE_GRANTS = 'FILE_GRANTS', //
  /**
   * @description 文件分类
   * @field FILE_CLASSES
   */
  FILE_CLASSES = 'FILE_CLASSES', //
  /**
   * @description 文件状态信息
   * @field FILE_STATE
   */
  FILE_STATE = 'FILE_STATE', //
  /**
   * @description 文件类型
   * @field FILE_TYPE
   */
  FILE_TYPE = 'FILE_TYPE', //
  /**
   * @description 文件备注信息
   * @field FILE_COMMENTS
   */
  FILE_COMMENTS = 'FILE_COMMENTS', //
  /**
   * @description 种属
   * @field FILE_SPECIES
   */
  FILE_SPECIES = 'FILE_SPECIES', //
}

/**
 * SYS_FILES-文件管理表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysFilesCols
 */
export enum SysFilesColPropEnum {
  /**
   * @description fileId
   * @field fileId
   */
  fileId = 'fileId', //
  /**
   * @description fileBarcode
   * @field fileBarcode
   */
  fileBarcode = 'fileBarcode', //
  /**
   * @description fileRelateId
   * @field fileRelateId
   */
  fileRelateId = 'fileRelateId', //
  /**
   * @description fileName
   * @field fileName
   */
  fileName = 'fileName', //
  /**
   * @description fileNameEn
   * @field fileNameEn
   */
  fileNameEn = 'fileNameEn', //
  /**
   * @description filePath
   * @field filePath
   */
  filePath = 'filePath', //
  /**
   * @description fileCreateDate
   * @field fileCreateDate
   */
  fileCreateDate = 'fileCreateDate', //
  /**
   * @description fileUpdateDate
   * @field fileUpdateDate
   */
  fileUpdateDate = 'fileUpdateDate', //
  /**
   * @description fileIsUsed
   * @field fileIsUsed
   */
  fileIsUsed = 'fileIsUsed', //
  /**
   * @description fileOwner
   * @field fileOwner
   */
  fileOwner = 'fileOwner', //
  /**
   * @description fileSize
   * @field fileSize
   */
  fileSize = 'fileSize', //
  /**
   * @description fileGrants
   * @field fileGrants
   */
  fileGrants = 'fileGrants', //
  /**
   * @description fileClasses
   * @field fileClasses
   */
  fileClasses = 'fileClasses', //
  /**
   * @description fileState
   * @field fileState
   */
  fileState = 'fileState', //
  /**
   * @description fileType
   * @field fileType
   */
  fileType = 'fileType', //
  /**
   * @description fileComments
   * @field fileComments
   */
  fileComments = 'fileComments', //
  /**
   * @description fileSpecies
   * @field fileSpecies
   */
  fileSpecies = 'fileSpecies', //
}

/**
 * SYS_FILES-文件管理表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysFilesTable
 */
export enum SysFilesTable {
  /**
   * @description FILE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'FILE_ID',
  /**
   * @description fileId
   * @field primerKey
   */
  primerKey = 'fileId',
  /**
   * @description SYS_FILES
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_FILES',
  /**
   * @description SysFiles
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysFiles',
}

/**
 * SYS_FILES-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysFilesColNames = Object.keys(SysFilesColNameEnum);
/**
 * SYS_FILES-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysFilesColProps = Object.keys(SysFilesColPropEnum);
/**
 * SYS_FILES-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysFilesColMap = { names: SysFilesColNames, props: SysFilesColProps };
