import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_ROLE_PERMISSION CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysRolePermissionCDto
 * @class SysRolePermissionCDto
 */
@ApiTags('SYS_ROLE_PERMISSION表的CDTO对象')
export class SysRolePermissionCDto {
  /**
   * 角色权限表Id
   *
   * @type { string }
   * @memberof SysRolePermissionCDto
   */
  @ApiProperty({ name: 'rpId', type: 'string', description: '角色权限表Id' })
  rpId: string;
  /**
   * 角色ID号
   *
   * @type { string }
   * @memberof SysRolePermissionCDto
   */
  @ApiProperty({ name: 'rpRoleId', type: 'string', description: '角色ID号' })
  rpRoleId: string;
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysRolePermissionCDto
   */
  @ApiProperty({ name: 'rpPermissionId', type: 'string', description: '权限ID' })
  rpPermissionId: string;
}
