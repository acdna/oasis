import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 计费卡信息表 CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysBillingCardCDto
 * @class SysBillingCardCDto
 */
@ApiTags('SYS_BILLING_CARD表的CDTO对象')
export class SysBillingCardCDto {
  /**
   * id
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiProperty({ name: 'bcId', type: 'string', description: 'id' })
  bcId: string;
  /**
   * 序列号信息
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiProperty({ name: 'bcSerialNumber', type: 'string', description: '序列号信息' })
  bcSerialNumber: string;
  /**
   * 总额
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcAmount', type: 'string', description: '总额' })
  bcAmount?: string;
  /**
   * 总时间
   *
   * @type { number }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcTotalTime', type: 'number', description: '总时间' })
  bcTotalTime?: number;
  /**
   * 类型
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcType', type: 'string', description: '类型' })
  bcType?: string;
  /**
   * 绑定帐号
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcAccount', type: 'string', description: '绑定帐号' })
  bcAccount?: string;
  /**
   * 绑定密码
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcPassword', type: 'string', description: '绑定密码' })
  bcPassword?: string;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcState', type: 'string', description: '状态' })
  bcState?: string;
  /**
   * 最大指纹数
   *
   * @type { string }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcBiggestGeneCount', type: 'string', description: '最大指纹数' })
  bcBiggestGeneCount?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysBillingCardCDto
   */
  @ApiPropertyOptional({ name: 'bcCreateDate', type: 'Date', description: '创建日期' })
  bcCreateDate?: Date;
}
