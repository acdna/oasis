import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_COL_DETAIL CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysColDetailCDto
 * @class SysColDetailCDto
 */
@ApiTags('SYS_COL_DETAIL表的CDTO对象')
export class SysColDetailCDto {
  /**
   * 主键ID
   *
   * @type { string }
   * @memberof SysColDetailCDto
   */
  @ApiProperty({ name: 'cdId', type: 'string', description: '主键ID' })
  cdId: string;
  /**
   * 表名
   *
   * @type { string }
   * @memberof SysColDetailCDto
   */
  @ApiProperty({ name: 'cdTableName', type: 'string', description: '表名' })
  cdTableName: string;
  /**
   * 列名
   *
   * @type { string }
   * @memberof SysColDetailCDto
   */
  @ApiProperty({ name: 'cdColName', type: 'string', description: '列名' })
  cdColName: string;
  /**
   * 列中文名
   *
   * @type { string }
   * @memberof SysColDetailCDto
   */
  @ApiPropertyOptional({ name: 'cdDescName', type: 'string', description: '列中文名' })
  cdDescName?: string;
  /**
   * 字典名
   *
   * @type { string }
   * @memberof SysColDetailCDto
   */
  @ApiPropertyOptional({ name: 'cdDictName', type: 'string', description: '字典名' })
  cdDictName?: string;
  /**
   * 模式
   *
   * @type { string }
   * @memberof SysColDetailCDto
   */
  @ApiPropertyOptional({ name: 'cdMode', type: 'string', description: '模式' })
  cdMode?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysColDetailCDto
   */
  @ApiPropertyOptional({ name: 'cdCreateDate', type: 'Date', description: '创建日期' })
  cdCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysColDetailCDto
   */
  @ApiPropertyOptional({ name: 'cdUpdateDate', type: 'Date', description: '更新日期' })
  cdUpdateDate?: Date;
}
