import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 系统JVM内存表 CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysMemoryCDto
 * @class SysMemoryCDto
 */
@ApiTags('SYS_MEMORY表的CDTO对象')
export class SysMemoryCDto {
  /**
   * memId
   *
   * @type { string }
   * @memberof SysMemoryCDto
   */
  @ApiProperty({ name: 'memId', type: 'string', description: 'memId' })
  memId: string;
  /**
   * 总内存
   *
   * @type { number }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memTotal', type: 'number', description: '总内存' })
  memTotal?: number;
  /**
   * 可用内存
   *
   * @type { number }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memFree', type: 'number', description: '可用内存' })
  memFree?: number;
  /**
   * 已使用内存
   *
   * @type { number }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memUsed', type: 'number', description: '已使用内存' })
  memUsed?: number;
  /**
   * 内存用量是否预警
   *
   * @type { string }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memIsWarning', type: 'string', description: '内存用量是否预警' })
  memIsWarning?: string;
  /**
   * 最大内存
   *
   * @type { number }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memMax', type: 'number', description: '最大内存' })
  memMax?: number;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memCreateDate', type: 'Date', description: '创建日期' })
  memCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysMemoryCDto
   */
  @ApiPropertyOptional({ name: 'memUpdateDate', type: 'Date', description: '更新日期' })
  memUpdateDate?: Date;
}
