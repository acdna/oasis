import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_MENU CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysMenuCDto
 * @class SysMenuCDto
 */
@ApiTags('SYS_MENU表的CDTO对象')
export class SysMenuCDto {
  /**
   * 菜单ID号
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiProperty({ name: 'menuId', type: 'string', description: '菜单ID号' })
  menuId: string;
  /**
   * 菜单名称
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiProperty({ name: 'menuName', type: 'string', description: '菜单名称' })
  menuName: string;
  /**
   * 英文菜单名
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiPropertyOptional({ name: 'menuNameEn', type: 'string', description: '英文菜单名' })
  menuNameEn?: string;
  /**
   * 菜单连接地址
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiPropertyOptional({ name: 'menuUrl', type: 'string', description: '菜单连接地址' })
  menuUrl?: string;
  /**
   * 父菜单ID
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiPropertyOptional({ name: 'menuParentId', type: 'string', description: '父菜单ID' })
  menuParentId?: string;
  /**
   * 菜单顺序
   *
   * @type { number }
   * @memberof SysMenuCDto
   */
  @ApiProperty({ name: 'menuOrder', type: 'number', description: '菜单顺序' })
  menuOrder: number;
  /**
   * 菜单备注
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiPropertyOptional({ name: 'menuRemark', type: 'string', description: '菜单备注' })
  menuRemark?: string;
  /**
   * 菜单状态
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiProperty({ name: 'menuState', type: 'string', description: '菜单状态' })
  menuState: string;
  /**
   * 菜单类型
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiPropertyOptional({ name: 'menuType', type: 'string', description: '菜单类型' })
  menuType?: string;
  /**
   * 菜单图标CSS名称
   *
   * @type { string }
   * @memberof SysMenuCDto
   */
  @ApiPropertyOptional({ name: 'menuIconClass', type: 'string', description: '菜单图标CSS名称' })
  menuIconClass?: string;
}
