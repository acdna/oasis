import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_UPDATE CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysUpdateCDto
 * @class SysUpdateCDto
 */
@ApiTags('SYS_UPDATE表的CDTO对象')
export class SysUpdateCDto {
  /**
   * 系统升级日志主键ID
   *
   * @type { string }
   * @memberof SysUpdateCDto
   */
  @ApiProperty({ name: 'updateId', type: 'string', description: '系统升级日志主键ID' })
  updateId: string;
  /**
   * 升级版本号
   *
   * @type { string }
   * @memberof SysUpdateCDto
   */
  @ApiPropertyOptional({ name: 'updateVersion', type: 'string', description: '升级版本号' })
  updateVersion?: string;
  /**
   * 升级内容
   *
   * @type { string }
   * @memberof SysUpdateCDto
   */
  @ApiPropertyOptional({ name: 'updateContent', type: 'string', description: '升级内容' })
  updateContent?: string;
  /**
   * 升级负责人
   *
   * @type { string }
   * @memberof SysUpdateCDto
   */
  @ApiPropertyOptional({ name: 'updateManager', type: 'string', description: '升级负责人' })
  updateManager?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUpdateCDto
   */
  @ApiPropertyOptional({ name: 'updateCreateTime', type: 'Date', description: '创建日期' })
  updateCreateTime?: Date;
}
