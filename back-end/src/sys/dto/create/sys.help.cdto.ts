import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_HELP CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysHelpCDto
 * @class SysHelpCDto
 */
@ApiTags('SYS_HELP表的CDTO对象')
export class SysHelpCDto {
  /**
   * 帮助编号
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiProperty({ name: 'helpId', type: 'string', description: '帮助编号' })
  helpId: string;
  /**
   * 帮助问题
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpQuestion', type: 'string', description: '帮助问题' })
  helpQuestion?: string;
  /**
   * 帮助回答
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpAnswer', type: 'string', description: '帮助回答' })
  helpAnswer?: string;
  /**
   * 问题编号父级编号
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpParentId', type: 'string', description: '问题编号父级编号' })
  helpParentId?: string;
  /**
   * 帮助类别
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpType', type: 'string', description: '帮助类别' })
  helpType?: string;
  /**
   * 帮助匹配路径
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpPath', type: 'string', description: '帮助匹配路径' })
  helpPath?: string;
  /**
   * 排序
   *
   * @type { string }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpSort', type: 'string', description: '排序' })
  helpSort?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpCreateDate', type: 'Date', description: '创建日期' })
  helpCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysHelpCDto
   */
  @ApiPropertyOptional({ name: 'helpUpdateDate', type: 'Date', description: '更新日期' })
  helpUpdateDate?: Date;
}
