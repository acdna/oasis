import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_CONFIG CDTO对象
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @export SysConfigCDto
 * @class SysConfigCDto
 */
@ApiTags('SYS_CONFIG表的CDTO对象')
export class SysConfigCDto {
  /**
   * ID
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiProperty({ name: 'conId', type: 'string', description: 'ID' })
  conId: string;
  /**
   * 参数名称
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiProperty({ name: 'conName', type: 'string', description: '参数名称' })
  conName: string;
  /**
   * 参数名称
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conNameEn', type: 'string', description: '参数名称' })
  conNameEn?: string;
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conValue', type: 'string', description: '参数值' })
  conValue?: string;
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conGroup', type: 'string', description: '所属组' })
  conGroup?: string;
  /**
   * 参数类型
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conParamType', type: 'string', description: '参数类型' })
  conParamType?: string;
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conParentId', type: 'string', description: '父级ID' })
  conParentId?: string;
  /**
   * 参数类型
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiProperty({ name: 'conType', type: 'string', description: '参数类型' })
  conType: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysConfigCDto
   */
  @ApiProperty({ name: 'conCreateDate', type: 'Date', description: '创建日期' })
  conCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conUpdateDate', type: 'Date', description: '更新日期' })
  conUpdateDate?: Date;
  /**
   * 用户
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conManager', type: 'string', description: '用户' })
  conManager?: string;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conSpecies', type: 'string', description: '种属' })
  conSpecies?: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conOrder', type: 'number', description: '排序' })
  conOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiProperty({ name: 'conState', type: 'string', description: '状态' })
  conState: string;
  /**
   * 描述
   *
   * @type { string }
   * @memberof SysConfigCDto
   */
  @ApiPropertyOptional({ name: 'conRemark', type: 'string', description: '描述' })
  conRemark?: string;
}
