import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_INSTRUMENT CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysInstrumentCDto
 * @class SysInstrumentCDto
 */
@ApiTags('SYS_INSTRUMENT表的CDTO对象')
export class SysInstrumentCDto {
  /**
   * 仪器编号
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiProperty({ name: 'insId', type: 'string', description: '仪器编号' })
  insId: string;
  /**
   * 仪器条码号
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insBarcode', type: 'string', description: '仪器条码号' })
  insBarcode?: string;
  /**
   * 仪器名称
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insName', type: 'string', description: '仪器名称' })
  insName?: string;
  /**
   * 仪器型号
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insModel', type: 'string', description: '仪器型号' })
  insModel?: string;
  /**
   * 仪器负责人
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insManager', type: 'string', description: '仪器负责人' })
  insManager?: string;
  /**
   * 仪器类型
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insType', type: 'string', description: '仪器类型' })
  insType?: string;
  /**
   * 仪器描述
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insRemark', type: 'string', description: '仪器描述' })
  insRemark?: string;
  /**
   * 仪器购买日期
   *
   * @type { Date }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insBuyDate', type: 'Date', description: '仪器购买日期' })
  insBuyDate?: Date;
  /**
   * 仪器维护周期
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insMaintainPeriod', type: 'string', description: '仪器维护周期' })
  insMaintainPeriod?: string;
  /**
   * 仪器厂商
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insMaker', type: 'string', description: '仪器厂商' })
  insMaker?: string;
  /**
   * 仪器价格
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insPrice', type: 'string', description: '仪器价格' })
  insPrice?: string;
  /**
   * insExtraProvide
   *
   * @type { string }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insExtraProvide', type: 'string', description: 'insExtraProvide' })
  insExtraProvide?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insCreateDate', type: 'Date', description: '创建日期' })
  insCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysInstrumentCDto
   */
  @ApiPropertyOptional({ name: 'insUpdateDate', type: 'Date', description: '更新日期' })
  insUpdateDate?: Date;
}
