import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_TIP CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysTipCDto
 * @class SysTipCDto
 */
@ApiTags('SYS_TIP表的CDTO对象')
export class SysTipCDto {
  /**
   * 提示信息ID
   *
   * @type { string }
   * @memberof SysTipCDto
   */
  @ApiProperty({ name: 'tipId', type: 'string', description: '提示信息ID' })
  tipId: string;
  /**
   * 提示次数
   *
   * @type { number }
   * @memberof SysTipCDto
   */
  @ApiPropertyOptional({ name: 'tipCount', type: 'number', description: '提示次数' })
  tipCount?: number;
  /**
   * 提示信息内容
   *
   * @type { string }
   * @memberof SysTipCDto
   */
  @ApiPropertyOptional({ name: 'tipContents', type: 'string', description: '提示信息内容' })
  tipContents?: string;
  /**
   * 所属者
   *
   * @type { string }
   * @memberof SysTipCDto
   */
  @ApiPropertyOptional({ name: 'tipManager', type: 'string', description: '所属者' })
  tipManager?: string;
  /**
   * 消息受众
   *
   * @type { string }
   * @memberof SysTipCDto
   */
  @ApiPropertyOptional({ name: 'tipAudience', type: 'string', description: '消息受众' })
  tipAudience?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysTipCDto
   */
  @ApiPropertyOptional({ name: 'tipCreateDate', type: 'Date', description: '创建日期' })
  tipCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysTipCDto
   */
  @ApiPropertyOptional({ name: 'tipUpdateDate', type: 'Date', description: '更新日期' })
  tipUpdateDate?: Date;
}
