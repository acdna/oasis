import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_DICTIONARY CDTO对象
 * @date 12/28/2020, 6:29:16 PM
 * @author jiangbin
 * @export SysDictionaryCDto
 * @class SysDictionaryCDto
 */
@ApiTags('SYS_DICTIONARY表的CDTO对象')
export class SysDictionaryCDto {
  /**
   * ID
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiProperty({ name: 'dicId', type: 'string', description: 'ID' })
  dicId: string;
  /**
   * 字典名称
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiProperty({ name: 'dicName', type: 'string', description: '字典名称' })
  dicName: string;
  /**
   * dicNameEn
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicNameEn', type: 'string', description: 'dicNameEn' })
  dicNameEn?: string;
  /**
   * 字典值
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicValue', type: 'string', description: '字典值' })
  dicValue?: string;
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicGroup', type: 'string', description: '所属组' })
  dicGroup?: string;
  /**
   * 模块名
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicModule', type: 'string', description: '模块名' })
  dicModule?: string;
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicParentId', type: 'string', description: '父级ID' })
  dicParentId?: string;
  /**
   * 字典类型
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiProperty({ name: 'dicType', type: 'string', description: '字典类型' })
  dicType: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicOrder', type: 'number', description: '排序' })
  dicOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiProperty({ name: 'dicState', type: 'string', description: '状态' })
  dicState: string;
  /**
   * dicParams
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicParams', type: 'string', description: 'dicParams' })
  dicParams?: string;
  /**
   * 关联种属
   *
   * @type { string }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicSpecies', type: 'string', description: '关联种属' })
  dicSpecies?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicCreateDate', type: 'Date', description: '创建日期' })
  dicCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysDictionaryCDto
   */
  @ApiPropertyOptional({ name: 'dicUpdateDate', type: 'Date', description: '更新日期' })
  dicUpdateDate?: Date;
}
