import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 缓存信息表 CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysCacheTraceCDto
 * @class SysCacheTraceCDto
 */
@ApiTags('SYS_CACHE_TRACE表的CDTO对象')
export class SysCacheTraceCDto {
  /**
   * 缓存记录ID
   *
   * @type { string }
   * @memberof SysCacheTraceCDto
   */
  @ApiProperty({ name: 'ctId', type: 'string', description: '缓存记录ID' })
  ctId: string;
  /**
   * 缓存类型代码
   *
   * @type { string }
   * @memberof SysCacheTraceCDto
   */
  @ApiPropertyOptional({ name: 'ctCode', type: 'string', description: '缓存类型代码' })
  ctCode?: string;
  /**
   * 缓存操作类型代码
   *
   * @type { string }
   * @memberof SysCacheTraceCDto
   */
  @ApiPropertyOptional({ name: 'ctOperate', type: 'string', description: '缓存操作类型代码' })
  ctOperate?: string;
  /**
   * 缓存需要操作的目标记录ID
   *
   * @type { string }
   * @memberof SysCacheTraceCDto
   */
  @ApiPropertyOptional({ name: 'ctTargetId', type: 'string', description: '缓存需要操作的目标记录ID' })
  ctTargetId?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCacheTraceCDto
   */
  @ApiPropertyOptional({ name: 'ctCreateDate', type: 'Date', description: '创建日期' })
  ctCreateDate?: Date;
  /**
   * 缓存备注信息
   *
   * @type { string }
   * @memberof SysCacheTraceCDto
   */
  @ApiPropertyOptional({ name: 'ctComment', type: 'string', description: '缓存备注信息' })
  ctComment?: string;
}
