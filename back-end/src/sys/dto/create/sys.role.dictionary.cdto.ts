import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_ROLE_DICTIONARY CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysRoleDictionaryCDto
 * @class SysRoleDictionaryCDto
 */
@ApiTags('SYS_ROLE_DICTIONARY表的CDTO对象')
export class SysRoleDictionaryCDto {
  /**
   * 角色字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryCDto
   */
  @ApiProperty({ name: 'rdId', type: 'string', description: '角色字典ID' })
  rdId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryCDto
   */
  @ApiProperty({ name: 'rdRoleId', type: 'string', description: '角色ID' })
  rdRoleId: string;
  /**
   * 字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryCDto
   */
  @ApiProperty({ name: 'rdDictionaryId', type: 'string', description: '字典ID' })
  rdDictionaryId: string;
}
