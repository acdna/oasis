import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_LOG CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysLogCDto
 * @class SysLogCDto
 */
@ApiTags('SYS_LOG表的CDTO对象')
export class SysLogCDto {
  /**
   * 日志ID号
   *
   * @type { string }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logId', type: 'string', description: '日志ID号' })
  logId: string;
  /**
   * 操作人员名称
   *
   * @type { string }
   * @memberof SysLogCDto
   */
  @ApiPropertyOptional({ name: 'logUser', type: 'string', description: '操作人员名称' })
  logUser?: string;
  /**
   * 操作时间
   *
   * @type { Date }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logTime', type: 'Date', description: '操作时间' })
  logTime: Date;
  /**
   * IP地址
   *
   * @type { string }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logIp', type: 'string', description: 'IP地址' })
  logIp: string;
  /**
   * 操作URL
   *
   * @type { string }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logUrl', type: 'string', description: '操作URL' })
  logUrl: string;
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logTitle', type: 'string', description: '模块' })
  logTitle: string;
  /**
   * 内容
   *
   * @type { string }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logContent', type: 'string', description: '内容' })
  logContent: string;
  /**
   * 操作类型
   *
   * @type { number }
   * @memberof SysLogCDto
   */
  @ApiProperty({ name: 'logType', type: 'number', description: '操作类型' })
  logType: number;
}
