import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 用户充值信息表 CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysUserTopUpCDto
 * @class SysUserTopUpCDto
 */
@ApiTags('SYS_USER_TOP_UP表的CDTO对象')
export class SysUserTopUpCDto {
  /**
   * id
   *
   * @type { string }
   * @memberof SysUserTopUpCDto
   */
  @ApiProperty({ name: 'utuId', type: 'string', description: 'id' })
  utuId: string;
  /**
   * 用户登录名
   *
   * @type { string }
   * @memberof SysUserTopUpCDto
   */
  @ApiProperty({ name: 'utuLoginName', type: 'string', description: '用户登录名' })
  utuLoginName: string;
  /**
   * 序列号
   *
   * @type { string }
   * @memberof SysUserTopUpCDto
   */
  @ApiProperty({ name: 'utuSerialNumber', type: 'string', description: '序列号' })
  utuSerialNumber: string;
  /**
   * 计费开始时间
   *
   * @type { Date }
   * @memberof SysUserTopUpCDto
   */
  @ApiPropertyOptional({ name: 'utuBillingStartDate', type: 'Date', description: '计费开始时间' })
  utuBillingStartDate?: Date;
  /**
   * 计费结束时间
   *
   * @type { Date }
   * @memberof SysUserTopUpCDto
   */
  @ApiPropertyOptional({ name: 'utuBillingEndDate', type: 'Date', description: '计费结束时间' })
  utuBillingEndDate?: Date;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUserTopUpCDto
   */
  @ApiPropertyOptional({ name: 'utuState', type: 'string', description: '状态' })
  utuState?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserTopUpCDto
   */
  @ApiPropertyOptional({ name: 'utuCreateDate', type: 'Date', description: '创建日期' })
  utuCreateDate?: Date;
}
