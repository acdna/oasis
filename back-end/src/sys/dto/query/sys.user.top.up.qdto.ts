import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysUserTopUpColNames, SysUserTopUpColProps } from '../../entity/ddl/sys.user.top.up.cols';
/**
 * 用户充值信息表表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysUserTopUpQDto
 * @class SysUserTopUpQDto
 */
@ApiTags('SYS_USER_TOP_UP表的QDTO对象')
export class SysUserTopUpQDto {
  /**
   * id
   *
   * @type { string }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'utuId', type: 'string', description: 'id' })
  utuId?: string;
  @ApiProperty({ name: 'nutuId', type: 'string', description: 'id' })
  nutuId?: string;
  @ApiPropertyOptional({ name: 'utuIdList', type: 'string[]', description: 'id-列表条件' })
  utuIdList?: string[];
  @ApiPropertyOptional({ name: 'nutuIdList', type: 'string[]', description: 'id-列表条件' })
  nutuIdList?: string[];
  /**
   * 用户登录名
   *
   * @type { string }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'utuLoginName', type: 'string', description: '用户登录名' })
  utuLoginName?: string;
  @ApiProperty({ name: 'nutuLoginName', type: 'string', description: '用户登录名' })
  nutuLoginName?: string;
  @ApiPropertyOptional({ name: 'utuLoginNameLike', type: 'string', description: '用户登录名-模糊条件' })
  utuLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'utuLoginNameList', type: 'string[]', description: '用户登录名-列表条件' })
  utuLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'nutuLoginNameLike', type: 'string', description: '用户登录名-模糊条件' })
  nutuLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'nutuLoginNameList', type: 'string[]', description: '用户登录名-列表条件' })
  nutuLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'utuLoginNameLikeList', type: 'string[]', description: '用户登录名-列表模糊条件' })
  utuLoginNameLikeList?: string[];
  /**
   * 序列号
   *
   * @type { string }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'utuSerialNumber', type: 'string', description: '序列号' })
  utuSerialNumber?: string;
  @ApiProperty({ name: 'nutuSerialNumber', type: 'string', description: '序列号' })
  nutuSerialNumber?: string;
  @ApiPropertyOptional({ name: 'utuSerialNumberLike', type: 'string', description: '序列号-模糊条件' })
  utuSerialNumberLike?: string;
  @ApiPropertyOptional({ name: 'utuSerialNumberList', type: 'string[]', description: '序列号-列表条件' })
  utuSerialNumberList?: string[];
  @ApiPropertyOptional({ name: 'nutuSerialNumberLike', type: 'string', description: '序列号-模糊条件' })
  nutuSerialNumberLike?: string;
  @ApiPropertyOptional({ name: 'nutuSerialNumberList', type: 'string[]', description: '序列号-列表条件' })
  nutuSerialNumberList?: string[];
  @ApiPropertyOptional({ name: 'utuSerialNumberLikeList', type: 'string[]', description: '序列号-列表模糊条件' })
  utuSerialNumberLikeList?: string[];
  /**
   * 计费开始时间
   *
   * @type { Date }
   * @memberof SysUserTopUpQDto
   */
  @ApiPropertyOptional({ name: 'utuBillingStartDate', type: 'Date', description: '计费开始时间' })
  @Type(() => Date)
  utuBillingStartDate?: Date;
  @ApiPropertyOptional({ name: 'nutuBillingStartDate', type: 'Date', description: '计费开始时间' })
  @Type(() => Date)
  nutuBillingStartDate?: Date;
  @ApiPropertyOptional({ name: 'sUtuBillingStartDate', type: 'Date', description: '计费开始时间-起始日期' })
  @Type(() => Date)
  sUtuBillingStartDate?: Date;
  @ApiPropertyOptional({ name: 'eUtuBillingStartDate', type: 'Date', description: '计费开始时间-终止日期' })
  @Type(() => Date)
  eUtuBillingStartDate?: Date;
  /**
   * 计费结束时间
   *
   * @type { Date }
   * @memberof SysUserTopUpQDto
   */
  @ApiPropertyOptional({ name: 'utuBillingEndDate', type: 'Date', description: '计费结束时间' })
  @Type(() => Date)
  utuBillingEndDate?: Date;
  @ApiPropertyOptional({ name: 'nutuBillingEndDate', type: 'Date', description: '计费结束时间' })
  @Type(() => Date)
  nutuBillingEndDate?: Date;
  @ApiPropertyOptional({ name: 'sUtuBillingEndDate', type: 'Date', description: '计费结束时间-起始日期' })
  @Type(() => Date)
  sUtuBillingEndDate?: Date;
  @ApiPropertyOptional({ name: 'eUtuBillingEndDate', type: 'Date', description: '计费结束时间-终止日期' })
  @Type(() => Date)
  eUtuBillingEndDate?: Date;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUserTopUpQDto
   */
  @ApiPropertyOptional({ name: 'utuState', type: 'string', description: '状态' })
  utuState?: string;
  @ApiPropertyOptional({ name: 'nutuState', type: 'string', description: '状态' })
  nutuState?: string;
  @ApiPropertyOptional({ name: 'utuStateLike', type: 'string', description: '状态-模糊条件' })
  utuStateLike?: string;
  @ApiPropertyOptional({ name: 'utuStateList', type: 'string[]', description: '状态-列表条件' })
  utuStateList?: string[];
  @ApiPropertyOptional({ name: 'nutuStateLike', type: 'string', description: '状态-模糊条件' })
  nutuStateLike?: string;
  @ApiPropertyOptional({ name: 'nutuStateList', type: 'string[]', description: '状态-列表条件' })
  nutuStateList?: string[];
  @ApiPropertyOptional({ name: 'utuStateLikeList', type: 'string[]', description: '状态-列表模糊条件' })
  utuStateLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserTopUpQDto
   */
  @ApiPropertyOptional({ name: 'utuCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  utuCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nutuCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nutuCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sUtuCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sUtuCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eUtuCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eUtuCreateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserTopUpColNames)[]|(keyof SysUserTopUpColProps)[] }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserTopUpColNames)[] | (keyof SysUserTopUpColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysUserTopUpQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
