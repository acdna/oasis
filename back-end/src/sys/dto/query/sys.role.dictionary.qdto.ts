import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysRoleDictionaryColNames, SysRoleDictionaryColProps } from '../../entity/ddl/sys.role.dictionary.cols';
/**
 * SYS_ROLE_DICTIONARY表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysRoleDictionaryQDto
 * @class SysRoleDictionaryQDto
 */
@ApiTags('SYS_ROLE_DICTIONARY表的QDTO对象')
export class SysRoleDictionaryQDto {
  /**
   * 角色字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'rdId', type: 'string', description: '角色字典ID' })
  rdId?: string;
  @ApiProperty({ name: 'nrdId', type: 'string', description: '角色字典ID' })
  nrdId?: string;
  @ApiPropertyOptional({ name: 'rdIdList', type: 'string[]', description: '角色字典ID-列表条件' })
  rdIdList?: string[];
  @ApiPropertyOptional({ name: 'nrdIdList', type: 'string[]', description: '角色字典ID-列表条件' })
  nrdIdList?: string[];
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'rdRoleId', type: 'string', description: '角色ID' })
  rdRoleId?: string;
  @ApiProperty({ name: 'nrdRoleId', type: 'string', description: '角色ID' })
  nrdRoleId?: string;
  @ApiPropertyOptional({ name: 'rdRoleIdLike', type: 'string', description: '角色ID-模糊条件' })
  rdRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'rdRoleIdList', type: 'string[]', description: '角色ID-列表条件' })
  rdRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'nrdRoleIdLike', type: 'string', description: '角色ID-模糊条件' })
  nrdRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'nrdRoleIdList', type: 'string[]', description: '角色ID-列表条件' })
  nrdRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'rdRoleIdLikeList', type: 'string[]', description: '角色ID-列表模糊条件' })
  rdRoleIdLikeList?: string[];
  /**
   * 字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'rdDictionaryId', type: 'string', description: '字典ID' })
  rdDictionaryId?: string;
  @ApiProperty({ name: 'nrdDictionaryId', type: 'string', description: '字典ID' })
  nrdDictionaryId?: string;
  @ApiPropertyOptional({ name: 'rdDictionaryIdLike', type: 'string', description: '字典ID-模糊条件' })
  rdDictionaryIdLike?: string;
  @ApiPropertyOptional({ name: 'rdDictionaryIdList', type: 'string[]', description: '字典ID-列表条件' })
  rdDictionaryIdList?: string[];
  @ApiPropertyOptional({ name: 'nrdDictionaryIdLike', type: 'string', description: '字典ID-模糊条件' })
  nrdDictionaryIdLike?: string;
  @ApiPropertyOptional({ name: 'nrdDictionaryIdList', type: 'string[]', description: '字典ID-列表条件' })
  nrdDictionaryIdList?: string[];
  @ApiPropertyOptional({ name: 'rdDictionaryIdLikeList', type: 'string[]', description: '字典ID-列表模糊条件' })
  rdDictionaryIdLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysRoleDictionaryColNames)[]|(keyof SysRoleDictionaryColProps)[] }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRoleDictionaryColNames)[] | (keyof SysRoleDictionaryColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysRoleDictionaryQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
