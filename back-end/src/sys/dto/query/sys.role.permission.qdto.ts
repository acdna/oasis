import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysRolePermissionColNames, SysRolePermissionColProps } from '../../entity/ddl/sys.role.permission.cols';
/**
 * SYS_ROLE_PERMISSION表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysRolePermissionQDto
 * @class SysRolePermissionQDto
 */
@ApiTags('SYS_ROLE_PERMISSION表的QDTO对象')
export class SysRolePermissionQDto {
  /**
   * 角色权限表Id
   *
   * @type { string }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'rpId', type: 'string', description: '角色权限表Id' })
  rpId?: string;
  @ApiProperty({ name: 'nrpId', type: 'string', description: '角色权限表Id' })
  nrpId?: string;
  @ApiPropertyOptional({ name: 'rpIdList', type: 'string[]', description: '角色权限表Id-列表条件' })
  rpIdList?: string[];
  @ApiPropertyOptional({ name: 'nrpIdList', type: 'string[]', description: '角色权限表Id-列表条件' })
  nrpIdList?: string[];
  /**
   * 角色ID号
   *
   * @type { string }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'rpRoleId', type: 'string', description: '角色ID号' })
  rpRoleId?: string;
  @ApiProperty({ name: 'nrpRoleId', type: 'string', description: '角色ID号' })
  nrpRoleId?: string;
  @ApiPropertyOptional({ name: 'rpRoleIdLike', type: 'string', description: '角色ID号-模糊条件' })
  rpRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'rpRoleIdList', type: 'string[]', description: '角色ID号-列表条件' })
  rpRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'nrpRoleIdLike', type: 'string', description: '角色ID号-模糊条件' })
  nrpRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'nrpRoleIdList', type: 'string[]', description: '角色ID号-列表条件' })
  nrpRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'rpRoleIdLikeList', type: 'string[]', description: '角色ID号-列表模糊条件' })
  rpRoleIdLikeList?: string[];
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'rpPermissionId', type: 'string', description: '权限ID' })
  rpPermissionId?: string;
  @ApiProperty({ name: 'nrpPermissionId', type: 'string', description: '权限ID' })
  nrpPermissionId?: string;
  @ApiPropertyOptional({ name: 'rpPermissionIdLike', type: 'string', description: '权限ID-模糊条件' })
  rpPermissionIdLike?: string;
  @ApiPropertyOptional({ name: 'rpPermissionIdList', type: 'string[]', description: '权限ID-列表条件' })
  rpPermissionIdList?: string[];
  @ApiPropertyOptional({ name: 'nrpPermissionIdLike', type: 'string', description: '权限ID-模糊条件' })
  nrpPermissionIdLike?: string;
  @ApiPropertyOptional({ name: 'nrpPermissionIdList', type: 'string[]', description: '权限ID-列表条件' })
  nrpPermissionIdList?: string[];
  @ApiPropertyOptional({ name: 'rpPermissionIdLikeList', type: 'string[]', description: '权限ID-列表模糊条件' })
  rpPermissionIdLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysRolePermissionColNames)[]|(keyof SysRolePermissionColProps)[] }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRolePermissionColNames)[] | (keyof SysRolePermissionColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysRolePermissionQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
