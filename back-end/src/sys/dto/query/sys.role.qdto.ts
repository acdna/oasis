import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysRoleColNames, SysRoleColProps } from '../../entity/ddl/sys.role.cols';
/**
 * SYS_ROLE表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysRoleQDto
 * @class SysRoleQDto
 */
@ApiTags('SYS_ROLE表的QDTO对象')
export class SysRoleQDto {
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'roleId', type: 'string', description: '角色ID' })
  roleId?: string;
  @ApiProperty({ name: 'nroleId', type: 'string', description: '角色ID' })
  nroleId?: string;
  @ApiPropertyOptional({ name: 'roleIdList', type: 'string[]', description: '角色ID-列表条件' })
  roleIdList?: string[];
  @ApiPropertyOptional({ name: 'nroleIdList', type: 'string[]', description: '角色ID-列表条件' })
  nroleIdList?: string[];
  /**
   * 角色名称
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiPropertyOptional({ name: 'roleName', type: 'string', description: '角色名称' })
  roleName?: string;
  @ApiPropertyOptional({ name: 'nroleName', type: 'string', description: '角色名称' })
  nroleName?: string;
  @ApiPropertyOptional({ name: 'roleNameLike', type: 'string', description: '角色名称-模糊条件' })
  roleNameLike?: string;
  @ApiPropertyOptional({ name: 'roleNameList', type: 'string[]', description: '角色名称-列表条件' })
  roleNameList?: string[];
  @ApiPropertyOptional({ name: 'nroleNameLike', type: 'string', description: '角色名称-模糊条件' })
  nroleNameLike?: string;
  @ApiPropertyOptional({ name: 'nroleNameList', type: 'string[]', description: '角色名称-列表条件' })
  nroleNameList?: string[];
  @ApiPropertyOptional({ name: 'roleNameLikeList', type: 'string[]', description: '角色名称-列表模糊条件' })
  roleNameLikeList?: string[];
  /**
   * roleNameEn
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiPropertyOptional({ name: 'roleNameEn', type: 'string', description: 'roleNameEn' })
  roleNameEn?: string;
  @ApiPropertyOptional({ name: 'nroleNameEn', type: 'string', description: 'roleNameEn' })
  nroleNameEn?: string;
  @ApiPropertyOptional({ name: 'roleNameEnLike', type: 'string', description: 'roleNameEn-模糊条件' })
  roleNameEnLike?: string;
  @ApiPropertyOptional({ name: 'roleNameEnList', type: 'string[]', description: 'roleNameEn-列表条件' })
  roleNameEnList?: string[];
  @ApiPropertyOptional({ name: 'nroleNameEnLike', type: 'string', description: 'roleNameEn-模糊条件' })
  nroleNameEnLike?: string;
  @ApiPropertyOptional({ name: 'nroleNameEnList', type: 'string[]', description: 'roleNameEn-列表条件' })
  nroleNameEnList?: string[];
  @ApiPropertyOptional({ name: 'roleNameEnLikeList', type: 'string[]', description: 'roleNameEn-列表模糊条件' })
  roleNameEnLikeList?: string[];
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiPropertyOptional({ name: 'roleState', type: 'string', description: '状态' })
  roleState?: string;
  @ApiPropertyOptional({ name: 'nroleState', type: 'string', description: '状态' })
  nroleState?: string;
  @ApiPropertyOptional({ name: 'roleStateLike', type: 'string', description: '状态-模糊条件' })
  roleStateLike?: string;
  @ApiPropertyOptional({ name: 'roleStateList', type: 'string[]', description: '状态-列表条件' })
  roleStateList?: string[];
  @ApiPropertyOptional({ name: 'nroleStateLike', type: 'string', description: '状态-模糊条件' })
  nroleStateLike?: string;
  @ApiPropertyOptional({ name: 'nroleStateList', type: 'string[]', description: '状态-列表条件' })
  nroleStateList?: string[];
  @ApiPropertyOptional({ name: 'roleStateLikeList', type: 'string[]', description: '状态-列表模糊条件' })
  roleStateLikeList?: string[];
  /**
   * 角色所属系统
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiPropertyOptional({ name: 'roleSys', type: 'string', description: '角色所属系统' })
  roleSys?: string;
  @ApiPropertyOptional({ name: 'nroleSys', type: 'string', description: '角色所属系统' })
  nroleSys?: string;
  @ApiPropertyOptional({ name: 'roleSysLike', type: 'string', description: '角色所属系统-模糊条件' })
  roleSysLike?: string;
  @ApiPropertyOptional({ name: 'roleSysList', type: 'string[]', description: '角色所属系统-列表条件' })
  roleSysList?: string[];
  @ApiPropertyOptional({ name: 'nroleSysLike', type: 'string', description: '角色所属系统-模糊条件' })
  nroleSysLike?: string;
  @ApiPropertyOptional({ name: 'nroleSysList', type: 'string[]', description: '角色所属系统-列表条件' })
  nroleSysList?: string[];
  @ApiPropertyOptional({ name: 'roleSysLikeList', type: 'string[]', description: '角色所属系统-列表模糊条件' })
  roleSysLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysRoleColNames)[]|(keyof SysRoleColProps)[] }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRoleColNames)[] | (keyof SysRoleColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysRoleQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
