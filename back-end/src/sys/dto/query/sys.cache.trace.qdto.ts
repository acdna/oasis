import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysCacheTraceColNames, SysCacheTraceColProps } from '../../entity/ddl/sys.cache.trace.cols';
/**
 * 缓存信息表表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysCacheTraceQDto
 * @class SysCacheTraceQDto
 */
@ApiTags('SYS_CACHE_TRACE表的QDTO对象')
export class SysCacheTraceQDto {
  /**
   * 缓存记录ID
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'ctId', type: 'string', description: '缓存记录ID' })
  ctId?: string;
  @ApiProperty({ name: 'nctId', type: 'string', description: '缓存记录ID' })
  nctId?: string;
  @ApiPropertyOptional({ name: 'ctIdList', type: 'string[]', description: '缓存记录ID-列表条件' })
  ctIdList?: string[];
  @ApiPropertyOptional({ name: 'nctIdList', type: 'string[]', description: '缓存记录ID-列表条件' })
  nctIdList?: string[];
  /**
   * 缓存类型代码
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiPropertyOptional({ name: 'ctCode', type: 'string', description: '缓存类型代码' })
  ctCode?: string;
  @ApiPropertyOptional({ name: 'nctCode', type: 'string', description: '缓存类型代码' })
  nctCode?: string;
  @ApiPropertyOptional({ name: 'ctCodeLike', type: 'string', description: '缓存类型代码-模糊条件' })
  ctCodeLike?: string;
  @ApiPropertyOptional({ name: 'ctCodeList', type: 'string[]', description: '缓存类型代码-列表条件' })
  ctCodeList?: string[];
  @ApiPropertyOptional({ name: 'nctCodeLike', type: 'string', description: '缓存类型代码-模糊条件' })
  nctCodeLike?: string;
  @ApiPropertyOptional({ name: 'nctCodeList', type: 'string[]', description: '缓存类型代码-列表条件' })
  nctCodeList?: string[];
  @ApiPropertyOptional({ name: 'ctCodeLikeList', type: 'string[]', description: '缓存类型代码-列表模糊条件' })
  ctCodeLikeList?: string[];
  /**
   * 缓存操作类型代码
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiPropertyOptional({ name: 'ctOperate', type: 'string', description: '缓存操作类型代码' })
  ctOperate?: string;
  @ApiPropertyOptional({ name: 'nctOperate', type: 'string', description: '缓存操作类型代码' })
  nctOperate?: string;
  @ApiPropertyOptional({ name: 'ctOperateLike', type: 'string', description: '缓存操作类型代码-模糊条件' })
  ctOperateLike?: string;
  @ApiPropertyOptional({ name: 'ctOperateList', type: 'string[]', description: '缓存操作类型代码-列表条件' })
  ctOperateList?: string[];
  @ApiPropertyOptional({ name: 'nctOperateLike', type: 'string', description: '缓存操作类型代码-模糊条件' })
  nctOperateLike?: string;
  @ApiPropertyOptional({ name: 'nctOperateList', type: 'string[]', description: '缓存操作类型代码-列表条件' })
  nctOperateList?: string[];
  @ApiPropertyOptional({ name: 'ctOperateLikeList', type: 'string[]', description: '缓存操作类型代码-列表模糊条件' })
  ctOperateLikeList?: string[];
  /**
   * 缓存需要操作的目标记录ID
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiPropertyOptional({ name: 'ctTargetId', type: 'string', description: '缓存需要操作的目标记录ID' })
  ctTargetId?: string;
  @ApiPropertyOptional({ name: 'nctTargetId', type: 'string', description: '缓存需要操作的目标记录ID' })
  nctTargetId?: string;
  @ApiPropertyOptional({ name: 'ctTargetIdLike', type: 'string', description: '缓存需要操作的目标记录ID-模糊条件' })
  ctTargetIdLike?: string;
  @ApiPropertyOptional({ name: 'ctTargetIdList', type: 'string[]', description: '缓存需要操作的目标记录ID-列表条件' })
  ctTargetIdList?: string[];
  @ApiPropertyOptional({ name: 'nctTargetIdLike', type: 'string', description: '缓存需要操作的目标记录ID-模糊条件' })
  nctTargetIdLike?: string;
  @ApiPropertyOptional({ name: 'nctTargetIdList', type: 'string[]', description: '缓存需要操作的目标记录ID-列表条件' })
  nctTargetIdList?: string[];
  @ApiPropertyOptional({ name: 'ctTargetIdLikeList', type: 'string[]', description: '缓存需要操作的目标记录ID-列表模糊条件' })
  ctTargetIdLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCacheTraceQDto
   */
  @ApiPropertyOptional({ name: 'ctCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  ctCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nctCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nctCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sCtCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sCtCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eCtCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eCtCreateDate?: Date;
  /**
   * 缓存备注信息
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiPropertyOptional({ name: 'ctComment', type: 'string', description: '缓存备注信息' })
  ctComment?: string;
  @ApiPropertyOptional({ name: 'nctComment', type: 'string', description: '缓存备注信息' })
  nctComment?: string;
  @ApiPropertyOptional({ name: 'ctCommentLike', type: 'string', description: '缓存备注信息-模糊条件' })
  ctCommentLike?: string;
  @ApiPropertyOptional({ name: 'ctCommentList', type: 'string[]', description: '缓存备注信息-列表条件' })
  ctCommentList?: string[];
  @ApiPropertyOptional({ name: 'nctCommentLike', type: 'string', description: '缓存备注信息-模糊条件' })
  nctCommentLike?: string;
  @ApiPropertyOptional({ name: 'nctCommentList', type: 'string[]', description: '缓存备注信息-列表条件' })
  nctCommentList?: string[];
  @ApiPropertyOptional({ name: 'ctCommentLikeList', type: 'string[]', description: '缓存备注信息-列表模糊条件' })
  ctCommentLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysCacheTraceColNames)[]|(keyof SysCacheTraceColProps)[] }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysCacheTraceColNames)[] | (keyof SysCacheTraceColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysCacheTraceQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
