import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysMemoryColNames, SysMemoryColProps } from '../../entity/ddl/sys.memory.cols';
/**
 * 系统JVM内存表表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysMemoryQDto
 * @class SysMemoryQDto
 */
@ApiTags('SYS_MEMORY表的QDTO对象')
export class SysMemoryQDto {
  /**
   * memId
   *
   * @type { string }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'memId', type: 'string', description: 'memId' })
  memId?: string;
  @ApiProperty({ name: 'nmemId', type: 'string', description: 'memId' })
  nmemId?: string;
  @ApiPropertyOptional({ name: 'memIdList', type: 'string[]', description: 'memId-列表条件' })
  memIdList?: string[];
  @ApiPropertyOptional({ name: 'nmemIdList', type: 'string[]', description: 'memId-列表条件' })
  nmemIdList?: string[];
  /**
   * 总内存
   *
   * @type { number }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memTotal', type: 'number', description: '总内存' })
  memTotal?: number;
  @ApiPropertyOptional({ name: 'nmemTotal', type: 'number', description: '总内存' })
  nmemTotal?: number;
  @ApiPropertyOptional({ name: 'minMemTotal', type: 'number', description: '总内存-最小值' })
  minMemTotal?: number;
  @ApiPropertyOptional({ name: 'maxMemTotal', type: 'number', description: '总内存-最大值' })
  maxMemTotal?: number;
  /**
   * 可用内存
   *
   * @type { number }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memFree', type: 'number', description: '可用内存' })
  memFree?: number;
  @ApiPropertyOptional({ name: 'nmemFree', type: 'number', description: '可用内存' })
  nmemFree?: number;
  @ApiPropertyOptional({ name: 'minMemFree', type: 'number', description: '可用内存-最小值' })
  minMemFree?: number;
  @ApiPropertyOptional({ name: 'maxMemFree', type: 'number', description: '可用内存-最大值' })
  maxMemFree?: number;
  /**
   * 已使用内存
   *
   * @type { number }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memUsed', type: 'number', description: '已使用内存' })
  memUsed?: number;
  @ApiPropertyOptional({ name: 'nmemUsed', type: 'number', description: '已使用内存' })
  nmemUsed?: number;
  @ApiPropertyOptional({ name: 'minMemUsed', type: 'number', description: '已使用内存-最小值' })
  minMemUsed?: number;
  @ApiPropertyOptional({ name: 'maxMemUsed', type: 'number', description: '已使用内存-最大值' })
  maxMemUsed?: number;
  /**
   * 内存用量是否预警
   *
   * @type { string }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memIsWarning', type: 'string', description: '内存用量是否预警' })
  memIsWarning?: string;
  @ApiPropertyOptional({ name: 'nmemIsWarning', type: 'string', description: '内存用量是否预警' })
  nmemIsWarning?: string;
  @ApiPropertyOptional({ name: 'memIsWarningLike', type: 'string', description: '内存用量是否预警-模糊条件' })
  memIsWarningLike?: string;
  @ApiPropertyOptional({ name: 'memIsWarningList', type: 'string[]', description: '内存用量是否预警-列表条件' })
  memIsWarningList?: string[];
  @ApiPropertyOptional({ name: 'nmemIsWarningLike', type: 'string', description: '内存用量是否预警-模糊条件' })
  nmemIsWarningLike?: string;
  @ApiPropertyOptional({ name: 'nmemIsWarningList', type: 'string[]', description: '内存用量是否预警-列表条件' })
  nmemIsWarningList?: string[];
  @ApiPropertyOptional({ name: 'memIsWarningLikeList', type: 'string[]', description: '内存用量是否预警-列表模糊条件' })
  memIsWarningLikeList?: string[];
  /**
   * 最大内存
   *
   * @type { number }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memMax', type: 'number', description: '最大内存' })
  memMax?: number;
  @ApiPropertyOptional({ name: 'nmemMax', type: 'number', description: '最大内存' })
  nmemMax?: number;
  @ApiPropertyOptional({ name: 'minMemMax', type: 'number', description: '最大内存-最小值' })
  minMemMax?: number;
  @ApiPropertyOptional({ name: 'maxMemMax', type: 'number', description: '最大内存-最大值' })
  maxMemMax?: number;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  memCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nmemCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nmemCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sMemCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sMemCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eMemCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eMemCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysMemoryQDto
   */
  @ApiPropertyOptional({ name: 'memUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  memUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nmemUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nmemUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sMemUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sMemUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eMemUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eMemUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysMemoryColNames)[]|(keyof SysMemoryColProps)[] }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysMemoryColNames)[] | (keyof SysMemoryColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysMemoryQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
