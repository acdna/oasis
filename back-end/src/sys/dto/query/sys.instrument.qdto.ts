import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysInstrumentColNames, SysInstrumentColProps } from '../../entity/ddl/sys.instrument.cols';
/**
 * SYS_INSTRUMENT表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysInstrumentQDto
 * @class SysInstrumentQDto
 */
@ApiTags('SYS_INSTRUMENT表的QDTO对象')
export class SysInstrumentQDto {
  /**
   * 仪器编号
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'insId', type: 'string', description: '仪器编号' })
  insId?: string;
  @ApiProperty({ name: 'ninsId', type: 'string', description: '仪器编号' })
  ninsId?: string;
  @ApiPropertyOptional({ name: 'insIdList', type: 'string[]', description: '仪器编号-列表条件' })
  insIdList?: string[];
  @ApiPropertyOptional({ name: 'ninsIdList', type: 'string[]', description: '仪器编号-列表条件' })
  ninsIdList?: string[];
  /**
   * 仪器条码号
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insBarcode', type: 'string', description: '仪器条码号' })
  insBarcode?: string;
  @ApiPropertyOptional({ name: 'ninsBarcode', type: 'string', description: '仪器条码号' })
  ninsBarcode?: string;
  @ApiPropertyOptional({ name: 'insBarcodeLike', type: 'string', description: '仪器条码号-模糊条件' })
  insBarcodeLike?: string;
  @ApiPropertyOptional({ name: 'insBarcodeList', type: 'string[]', description: '仪器条码号-列表条件' })
  insBarcodeList?: string[];
  @ApiPropertyOptional({ name: 'ninsBarcodeLike', type: 'string', description: '仪器条码号-模糊条件' })
  ninsBarcodeLike?: string;
  @ApiPropertyOptional({ name: 'ninsBarcodeList', type: 'string[]', description: '仪器条码号-列表条件' })
  ninsBarcodeList?: string[];
  @ApiPropertyOptional({ name: 'insBarcodeLikeList', type: 'string[]', description: '仪器条码号-列表模糊条件' })
  insBarcodeLikeList?: string[];
  /**
   * 仪器名称
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insName', type: 'string', description: '仪器名称' })
  insName?: string;
  @ApiPropertyOptional({ name: 'ninsName', type: 'string', description: '仪器名称' })
  ninsName?: string;
  @ApiPropertyOptional({ name: 'insNameLike', type: 'string', description: '仪器名称-模糊条件' })
  insNameLike?: string;
  @ApiPropertyOptional({ name: 'insNameList', type: 'string[]', description: '仪器名称-列表条件' })
  insNameList?: string[];
  @ApiPropertyOptional({ name: 'ninsNameLike', type: 'string', description: '仪器名称-模糊条件' })
  ninsNameLike?: string;
  @ApiPropertyOptional({ name: 'ninsNameList', type: 'string[]', description: '仪器名称-列表条件' })
  ninsNameList?: string[];
  @ApiPropertyOptional({ name: 'insNameLikeList', type: 'string[]', description: '仪器名称-列表模糊条件' })
  insNameLikeList?: string[];
  /**
   * 仪器型号
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insModel', type: 'string', description: '仪器型号' })
  insModel?: string;
  @ApiPropertyOptional({ name: 'ninsModel', type: 'string', description: '仪器型号' })
  ninsModel?: string;
  @ApiPropertyOptional({ name: 'insModelLike', type: 'string', description: '仪器型号-模糊条件' })
  insModelLike?: string;
  @ApiPropertyOptional({ name: 'insModelList', type: 'string[]', description: '仪器型号-列表条件' })
  insModelList?: string[];
  @ApiPropertyOptional({ name: 'ninsModelLike', type: 'string', description: '仪器型号-模糊条件' })
  ninsModelLike?: string;
  @ApiPropertyOptional({ name: 'ninsModelList', type: 'string[]', description: '仪器型号-列表条件' })
  ninsModelList?: string[];
  @ApiPropertyOptional({ name: 'insModelLikeList', type: 'string[]', description: '仪器型号-列表模糊条件' })
  insModelLikeList?: string[];
  /**
   * 仪器负责人
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insManager', type: 'string', description: '仪器负责人' })
  insManager?: string;
  @ApiPropertyOptional({ name: 'ninsManager', type: 'string', description: '仪器负责人' })
  ninsManager?: string;
  @ApiPropertyOptional({ name: 'insManagerLike', type: 'string', description: '仪器负责人-模糊条件' })
  insManagerLike?: string;
  @ApiPropertyOptional({ name: 'insManagerList', type: 'string[]', description: '仪器负责人-列表条件' })
  insManagerList?: string[];
  @ApiPropertyOptional({ name: 'ninsManagerLike', type: 'string', description: '仪器负责人-模糊条件' })
  ninsManagerLike?: string;
  @ApiPropertyOptional({ name: 'ninsManagerList', type: 'string[]', description: '仪器负责人-列表条件' })
  ninsManagerList?: string[];
  @ApiPropertyOptional({ name: 'insManagerLikeList', type: 'string[]', description: '仪器负责人-列表模糊条件' })
  insManagerLikeList?: string[];
  /**
   * 仪器类型
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insType', type: 'string', description: '仪器类型' })
  insType?: string;
  @ApiPropertyOptional({ name: 'ninsType', type: 'string', description: '仪器类型' })
  ninsType?: string;
  @ApiPropertyOptional({ name: 'insTypeLike', type: 'string', description: '仪器类型-模糊条件' })
  insTypeLike?: string;
  @ApiPropertyOptional({ name: 'insTypeList', type: 'string[]', description: '仪器类型-列表条件' })
  insTypeList?: string[];
  @ApiPropertyOptional({ name: 'ninsTypeLike', type: 'string', description: '仪器类型-模糊条件' })
  ninsTypeLike?: string;
  @ApiPropertyOptional({ name: 'ninsTypeList', type: 'string[]', description: '仪器类型-列表条件' })
  ninsTypeList?: string[];
  @ApiPropertyOptional({ name: 'insTypeLikeList', type: 'string[]', description: '仪器类型-列表模糊条件' })
  insTypeLikeList?: string[];
  /**
   * 仪器描述
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insRemark', type: 'string', description: '仪器描述' })
  insRemark?: string;
  @ApiPropertyOptional({ name: 'ninsRemark', type: 'string', description: '仪器描述' })
  ninsRemark?: string;
  @ApiPropertyOptional({ name: 'insRemarkLike', type: 'string', description: '仪器描述-模糊条件' })
  insRemarkLike?: string;
  @ApiPropertyOptional({ name: 'insRemarkList', type: 'string[]', description: '仪器描述-列表条件' })
  insRemarkList?: string[];
  @ApiPropertyOptional({ name: 'ninsRemarkLike', type: 'string', description: '仪器描述-模糊条件' })
  ninsRemarkLike?: string;
  @ApiPropertyOptional({ name: 'ninsRemarkList', type: 'string[]', description: '仪器描述-列表条件' })
  ninsRemarkList?: string[];
  @ApiPropertyOptional({ name: 'insRemarkLikeList', type: 'string[]', description: '仪器描述-列表模糊条件' })
  insRemarkLikeList?: string[];
  /**
   * 仪器购买日期
   *
   * @type { Date }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insBuyDate', type: 'Date', description: '仪器购买日期' })
  @Type(() => Date)
  insBuyDate?: Date;
  @ApiPropertyOptional({ name: 'ninsBuyDate', type: 'Date', description: '仪器购买日期' })
  @Type(() => Date)
  ninsBuyDate?: Date;
  @ApiPropertyOptional({ name: 'sInsBuyDate', type: 'Date', description: '仪器购买日期-起始日期' })
  @Type(() => Date)
  sInsBuyDate?: Date;
  @ApiPropertyOptional({ name: 'eInsBuyDate', type: 'Date', description: '仪器购买日期-终止日期' })
  @Type(() => Date)
  eInsBuyDate?: Date;
  /**
   * 仪器维护周期
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insMaintainPeriod', type: 'string', description: '仪器维护周期' })
  insMaintainPeriod?: string;
  @ApiPropertyOptional({ name: 'ninsMaintainPeriod', type: 'string', description: '仪器维护周期' })
  ninsMaintainPeriod?: string;
  @ApiPropertyOptional({ name: 'insMaintainPeriodLike', type: 'string', description: '仪器维护周期-模糊条件' })
  insMaintainPeriodLike?: string;
  @ApiPropertyOptional({ name: 'insMaintainPeriodList', type: 'string[]', description: '仪器维护周期-列表条件' })
  insMaintainPeriodList?: string[];
  @ApiPropertyOptional({ name: 'ninsMaintainPeriodLike', type: 'string', description: '仪器维护周期-模糊条件' })
  ninsMaintainPeriodLike?: string;
  @ApiPropertyOptional({ name: 'ninsMaintainPeriodList', type: 'string[]', description: '仪器维护周期-列表条件' })
  ninsMaintainPeriodList?: string[];
  @ApiPropertyOptional({ name: 'insMaintainPeriodLikeList', type: 'string[]', description: '仪器维护周期-列表模糊条件' })
  insMaintainPeriodLikeList?: string[];
  /**
   * 仪器厂商
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insMaker', type: 'string', description: '仪器厂商' })
  insMaker?: string;
  @ApiPropertyOptional({ name: 'ninsMaker', type: 'string', description: '仪器厂商' })
  ninsMaker?: string;
  @ApiPropertyOptional({ name: 'insMakerLike', type: 'string', description: '仪器厂商-模糊条件' })
  insMakerLike?: string;
  @ApiPropertyOptional({ name: 'insMakerList', type: 'string[]', description: '仪器厂商-列表条件' })
  insMakerList?: string[];
  @ApiPropertyOptional({ name: 'ninsMakerLike', type: 'string', description: '仪器厂商-模糊条件' })
  ninsMakerLike?: string;
  @ApiPropertyOptional({ name: 'ninsMakerList', type: 'string[]', description: '仪器厂商-列表条件' })
  ninsMakerList?: string[];
  @ApiPropertyOptional({ name: 'insMakerLikeList', type: 'string[]', description: '仪器厂商-列表模糊条件' })
  insMakerLikeList?: string[];
  /**
   * 仪器价格
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insPrice', type: 'string', description: '仪器价格' })
  insPrice?: string;
  @ApiPropertyOptional({ name: 'ninsPrice', type: 'string', description: '仪器价格' })
  ninsPrice?: string;
  @ApiPropertyOptional({ name: 'insPriceLike', type: 'string', description: '仪器价格-模糊条件' })
  insPriceLike?: string;
  @ApiPropertyOptional({ name: 'insPriceList', type: 'string[]', description: '仪器价格-列表条件' })
  insPriceList?: string[];
  @ApiPropertyOptional({ name: 'ninsPriceLike', type: 'string', description: '仪器价格-模糊条件' })
  ninsPriceLike?: string;
  @ApiPropertyOptional({ name: 'ninsPriceList', type: 'string[]', description: '仪器价格-列表条件' })
  ninsPriceList?: string[];
  @ApiPropertyOptional({ name: 'insPriceLikeList', type: 'string[]', description: '仪器价格-列表模糊条件' })
  insPriceLikeList?: string[];
  /**
   * insExtraProvide
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insExtraProvide', type: 'string', description: 'insExtraProvide' })
  insExtraProvide?: string;
  @ApiPropertyOptional({ name: 'ninsExtraProvide', type: 'string', description: 'insExtraProvide' })
  ninsExtraProvide?: string;
  @ApiPropertyOptional({ name: 'insExtraProvideLike', type: 'string', description: 'insExtraProvide-模糊条件' })
  insExtraProvideLike?: string;
  @ApiPropertyOptional({ name: 'insExtraProvideList', type: 'string[]', description: 'insExtraProvide-列表条件' })
  insExtraProvideList?: string[];
  @ApiPropertyOptional({ name: 'ninsExtraProvideLike', type: 'string', description: 'insExtraProvide-模糊条件' })
  ninsExtraProvideLike?: string;
  @ApiPropertyOptional({ name: 'ninsExtraProvideList', type: 'string[]', description: 'insExtraProvide-列表条件' })
  ninsExtraProvideList?: string[];
  @ApiPropertyOptional({ name: 'insExtraProvideLikeList', type: 'string[]', description: 'insExtraProvide-列表模糊条件' })
  insExtraProvideLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  insCreateDate?: Date;
  @ApiPropertyOptional({ name: 'ninsCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  ninsCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sInsCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sInsCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eInsCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eInsCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysInstrumentQDto
   */
  @ApiPropertyOptional({ name: 'insUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  insUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'ninsUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  ninsUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sInsUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sInsUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eInsUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eInsUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysInstrumentColNames)[]|(keyof SysInstrumentColProps)[] }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysInstrumentColNames)[] | (keyof SysInstrumentColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysInstrumentQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
