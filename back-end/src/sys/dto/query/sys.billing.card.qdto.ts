import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysBillingCardColNames, SysBillingCardColProps } from '../../entity/ddl/sys.billing.card.cols';
/**
 * 计费卡信息表表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysBillingCardQDto
 * @class SysBillingCardQDto
 */
@ApiTags('SYS_BILLING_CARD表的QDTO对象')
export class SysBillingCardQDto {
  /**
   * id
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'bcId', type: 'string', description: 'id' })
  bcId?: string;
  @ApiProperty({ name: 'nbcId', type: 'string', description: 'id' })
  nbcId?: string;
  @ApiPropertyOptional({ name: 'bcIdList', type: 'string[]', description: 'id-列表条件' })
  bcIdList?: string[];
  @ApiPropertyOptional({ name: 'nbcIdList', type: 'string[]', description: 'id-列表条件' })
  nbcIdList?: string[];
  /**
   * 序列号信息
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'bcSerialNumber', type: 'string', description: '序列号信息' })
  bcSerialNumber?: string;
  @ApiProperty({ name: 'nbcSerialNumber', type: 'string', description: '序列号信息' })
  nbcSerialNumber?: string;
  @ApiPropertyOptional({ name: 'bcSerialNumberLike', type: 'string', description: '序列号信息-模糊条件' })
  bcSerialNumberLike?: string;
  @ApiPropertyOptional({ name: 'bcSerialNumberList', type: 'string[]', description: '序列号信息-列表条件' })
  bcSerialNumberList?: string[];
  @ApiPropertyOptional({ name: 'nbcSerialNumberLike', type: 'string', description: '序列号信息-模糊条件' })
  nbcSerialNumberLike?: string;
  @ApiPropertyOptional({ name: 'nbcSerialNumberList', type: 'string[]', description: '序列号信息-列表条件' })
  nbcSerialNumberList?: string[];
  @ApiPropertyOptional({ name: 'bcSerialNumberLikeList', type: 'string[]', description: '序列号信息-列表模糊条件' })
  bcSerialNumberLikeList?: string[];
  /**
   * 总额
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcAmount', type: 'string', description: '总额' })
  bcAmount?: string;
  @ApiPropertyOptional({ name: 'nbcAmount', type: 'string', description: '总额' })
  nbcAmount?: string;
  @ApiPropertyOptional({ name: 'bcAmountLike', type: 'string', description: '总额-模糊条件' })
  bcAmountLike?: string;
  @ApiPropertyOptional({ name: 'bcAmountList', type: 'string[]', description: '总额-列表条件' })
  bcAmountList?: string[];
  @ApiPropertyOptional({ name: 'nbcAmountLike', type: 'string', description: '总额-模糊条件' })
  nbcAmountLike?: string;
  @ApiPropertyOptional({ name: 'nbcAmountList', type: 'string[]', description: '总额-列表条件' })
  nbcAmountList?: string[];
  @ApiPropertyOptional({ name: 'bcAmountLikeList', type: 'string[]', description: '总额-列表模糊条件' })
  bcAmountLikeList?: string[];
  /**
   * 总时间
   *
   * @type { number }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcTotalTime', type: 'number', description: '总时间' })
  bcTotalTime?: number;
  @ApiPropertyOptional({ name: 'nbcTotalTime', type: 'number', description: '总时间' })
  nbcTotalTime?: number;
  @ApiPropertyOptional({ name: 'minBcTotalTime', type: 'number', description: '总时间-最小值' })
  minBcTotalTime?: number;
  @ApiPropertyOptional({ name: 'maxBcTotalTime', type: 'number', description: '总时间-最大值' })
  maxBcTotalTime?: number;
  /**
   * 类型
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcType', type: 'string', description: '类型' })
  bcType?: string;
  @ApiPropertyOptional({ name: 'nbcType', type: 'string', description: '类型' })
  nbcType?: string;
  @ApiPropertyOptional({ name: 'bcTypeLike', type: 'string', description: '类型-模糊条件' })
  bcTypeLike?: string;
  @ApiPropertyOptional({ name: 'bcTypeList', type: 'string[]', description: '类型-列表条件' })
  bcTypeList?: string[];
  @ApiPropertyOptional({ name: 'nbcTypeLike', type: 'string', description: '类型-模糊条件' })
  nbcTypeLike?: string;
  @ApiPropertyOptional({ name: 'nbcTypeList', type: 'string[]', description: '类型-列表条件' })
  nbcTypeList?: string[];
  @ApiPropertyOptional({ name: 'bcTypeLikeList', type: 'string[]', description: '类型-列表模糊条件' })
  bcTypeLikeList?: string[];
  /**
   * 绑定帐号
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcAccount', type: 'string', description: '绑定帐号' })
  bcAccount?: string;
  @ApiPropertyOptional({ name: 'nbcAccount', type: 'string', description: '绑定帐号' })
  nbcAccount?: string;
  @ApiPropertyOptional({ name: 'bcAccountLike', type: 'string', description: '绑定帐号-模糊条件' })
  bcAccountLike?: string;
  @ApiPropertyOptional({ name: 'bcAccountList', type: 'string[]', description: '绑定帐号-列表条件' })
  bcAccountList?: string[];
  @ApiPropertyOptional({ name: 'nbcAccountLike', type: 'string', description: '绑定帐号-模糊条件' })
  nbcAccountLike?: string;
  @ApiPropertyOptional({ name: 'nbcAccountList', type: 'string[]', description: '绑定帐号-列表条件' })
  nbcAccountList?: string[];
  @ApiPropertyOptional({ name: 'bcAccountLikeList', type: 'string[]', description: '绑定帐号-列表模糊条件' })
  bcAccountLikeList?: string[];
  /**
   * 绑定密码
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcPassword', type: 'string', description: '绑定密码' })
  bcPassword?: string;
  @ApiPropertyOptional({ name: 'nbcPassword', type: 'string', description: '绑定密码' })
  nbcPassword?: string;
  @ApiPropertyOptional({ name: 'bcPasswordLike', type: 'string', description: '绑定密码-模糊条件' })
  bcPasswordLike?: string;
  @ApiPropertyOptional({ name: 'bcPasswordList', type: 'string[]', description: '绑定密码-列表条件' })
  bcPasswordList?: string[];
  @ApiPropertyOptional({ name: 'nbcPasswordLike', type: 'string', description: '绑定密码-模糊条件' })
  nbcPasswordLike?: string;
  @ApiPropertyOptional({ name: 'nbcPasswordList', type: 'string[]', description: '绑定密码-列表条件' })
  nbcPasswordList?: string[];
  @ApiPropertyOptional({ name: 'bcPasswordLikeList', type: 'string[]', description: '绑定密码-列表模糊条件' })
  bcPasswordLikeList?: string[];
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcState', type: 'string', description: '状态' })
  bcState?: string;
  @ApiPropertyOptional({ name: 'nbcState', type: 'string', description: '状态' })
  nbcState?: string;
  @ApiPropertyOptional({ name: 'bcStateLike', type: 'string', description: '状态-模糊条件' })
  bcStateLike?: string;
  @ApiPropertyOptional({ name: 'bcStateList', type: 'string[]', description: '状态-列表条件' })
  bcStateList?: string[];
  @ApiPropertyOptional({ name: 'nbcStateLike', type: 'string', description: '状态-模糊条件' })
  nbcStateLike?: string;
  @ApiPropertyOptional({ name: 'nbcStateList', type: 'string[]', description: '状态-列表条件' })
  nbcStateList?: string[];
  @ApiPropertyOptional({ name: 'bcStateLikeList', type: 'string[]', description: '状态-列表模糊条件' })
  bcStateLikeList?: string[];
  /**
   * 最大指纹数
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcBiggestGeneCount', type: 'string', description: '最大指纹数' })
  bcBiggestGeneCount?: string;
  @ApiPropertyOptional({ name: 'nbcBiggestGeneCount', type: 'string', description: '最大指纹数' })
  nbcBiggestGeneCount?: string;
  @ApiPropertyOptional({ name: 'bcBiggestGeneCountLike', type: 'string', description: '最大指纹数-模糊条件' })
  bcBiggestGeneCountLike?: string;
  @ApiPropertyOptional({ name: 'bcBiggestGeneCountList', type: 'string[]', description: '最大指纹数-列表条件' })
  bcBiggestGeneCountList?: string[];
  @ApiPropertyOptional({ name: 'nbcBiggestGeneCountLike', type: 'string', description: '最大指纹数-模糊条件' })
  nbcBiggestGeneCountLike?: string;
  @ApiPropertyOptional({ name: 'nbcBiggestGeneCountList', type: 'string[]', description: '最大指纹数-列表条件' })
  nbcBiggestGeneCountList?: string[];
  @ApiPropertyOptional({ name: 'bcBiggestGeneCountLikeList', type: 'string[]', description: '最大指纹数-列表模糊条件' })
  bcBiggestGeneCountLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysBillingCardQDto
   */
  @ApiPropertyOptional({ name: 'bcCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  bcCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nbcCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nbcCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sBcCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sBcCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eBcCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eBcCreateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysBillingCardColNames)[]|(keyof SysBillingCardColProps)[] }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysBillingCardColNames)[] | (keyof SysBillingCardColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysBillingCardQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
