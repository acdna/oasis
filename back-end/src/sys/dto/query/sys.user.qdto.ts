import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysUserColNames, SysUserColProps } from '../../entity/ddl/sys.user.cols';
/**
 * SYS_USER表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysUserQDto
 * @class SysUserQDto
 */
@ApiTags('SYS_USER表的QDTO对象')
export class SysUserQDto {
  /**
   * 用户ID
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'userId', type: 'string', description: '用户ID' })
  userId?: string;
  @ApiProperty({ name: 'nuserId', type: 'string', description: '用户ID' })
  nuserId?: string;
  @ApiPropertyOptional({ name: 'userIdList', type: 'string[]', description: '用户ID-列表条件' })
  userIdList?: string[];
  @ApiPropertyOptional({ name: 'nuserIdList', type: 'string[]', description: '用户ID-列表条件' })
  nuserIdList?: string[];
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'userLoginName', type: 'string', description: '用户帐号' })
  userLoginName?: string;
  @ApiProperty({ name: 'nuserLoginName', type: 'string', description: '用户帐号' })
  nuserLoginName?: string;
  @ApiPropertyOptional({ name: 'userLoginNameLike', type: 'string', description: '用户帐号-模糊条件' })
  userLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'userLoginNameList', type: 'string[]', description: '用户帐号-列表条件' })
  userLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'nuserLoginNameLike', type: 'string', description: '用户帐号-模糊条件' })
  nuserLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'nuserLoginNameList', type: 'string[]', description: '用户帐号-列表条件' })
  nuserLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'userLoginNameLikeList', type: 'string[]', description: '用户帐号-列表模糊条件' })
  userLoginNameLikeList?: string[];
  /**
   * 用户密码
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'userPassword', type: 'string', description: '用户密码' })
  userPassword?: string;
  @ApiProperty({ name: 'nuserPassword', type: 'string', description: '用户密码' })
  nuserPassword?: string;
  @ApiPropertyOptional({ name: 'userPasswordLike', type: 'string', description: '用户密码-模糊条件' })
  userPasswordLike?: string;
  @ApiPropertyOptional({ name: 'userPasswordList', type: 'string[]', description: '用户密码-列表条件' })
  userPasswordList?: string[];
  @ApiPropertyOptional({ name: 'nuserPasswordLike', type: 'string', description: '用户密码-模糊条件' })
  nuserPasswordLike?: string;
  @ApiPropertyOptional({ name: 'nuserPasswordList', type: 'string[]', description: '用户密码-列表条件' })
  nuserPasswordList?: string[];
  @ApiPropertyOptional({ name: 'userPasswordLikeList', type: 'string[]', description: '用户密码-列表模糊条件' })
  userPasswordLikeList?: string[];
  /**
   * userPasswordPrivate
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userPasswordPrivate', type: 'string', description: 'userPasswordPrivate' })
  userPasswordPrivate?: string;
  @ApiPropertyOptional({ name: 'nuserPasswordPrivate', type: 'string', description: 'userPasswordPrivate' })
  nuserPasswordPrivate?: string;
  @ApiPropertyOptional({ name: 'userPasswordPrivateLike', type: 'string', description: 'userPasswordPrivate-模糊条件' })
  userPasswordPrivateLike?: string;
  @ApiPropertyOptional({ name: 'userPasswordPrivateList', type: 'string[]', description: 'userPasswordPrivate-列表条件' })
  userPasswordPrivateList?: string[];
  @ApiPropertyOptional({ name: 'nuserPasswordPrivateLike', type: 'string', description: 'userPasswordPrivate-模糊条件' })
  nuserPasswordPrivateLike?: string;
  @ApiPropertyOptional({ name: 'nuserPasswordPrivateList', type: 'string[]', description: 'userPasswordPrivate-列表条件' })
  nuserPasswordPrivateList?: string[];
  @ApiPropertyOptional({ name: 'userPasswordPrivateLikeList', type: 'string[]', description: 'userPasswordPrivate-列表模糊条件' })
  userPasswordPrivateLikeList?: string[];
  /**
   * 姓名缩写
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userNameAbbr', type: 'string', description: '姓名缩写' })
  userNameAbbr?: string;
  @ApiPropertyOptional({ name: 'nuserNameAbbr', type: 'string', description: '姓名缩写' })
  nuserNameAbbr?: string;
  @ApiPropertyOptional({ name: 'userNameAbbrLike', type: 'string', description: '姓名缩写-模糊条件' })
  userNameAbbrLike?: string;
  @ApiPropertyOptional({ name: 'userNameAbbrList', type: 'string[]', description: '姓名缩写-列表条件' })
  userNameAbbrList?: string[];
  @ApiPropertyOptional({ name: 'nuserNameAbbrLike', type: 'string', description: '姓名缩写-模糊条件' })
  nuserNameAbbrLike?: string;
  @ApiPropertyOptional({ name: 'nuserNameAbbrList', type: 'string[]', description: '姓名缩写-列表条件' })
  nuserNameAbbrList?: string[];
  @ApiPropertyOptional({ name: 'userNameAbbrLikeList', type: 'string[]', description: '姓名缩写-列表模糊条件' })
  userNameAbbrLikeList?: string[];
  /**
   * 用户名称
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userName', type: 'string', description: '用户名称' })
  userName?: string;
  @ApiPropertyOptional({ name: 'nuserName', type: 'string', description: '用户名称' })
  nuserName?: string;
  @ApiPropertyOptional({ name: 'userNameLike', type: 'string', description: '用户名称-模糊条件' })
  userNameLike?: string;
  @ApiPropertyOptional({ name: 'userNameList', type: 'string[]', description: '用户名称-列表条件' })
  userNameList?: string[];
  @ApiPropertyOptional({ name: 'nuserNameLike', type: 'string', description: '用户名称-模糊条件' })
  nuserNameLike?: string;
  @ApiPropertyOptional({ name: 'nuserNameList', type: 'string[]', description: '用户名称-列表条件' })
  nuserNameList?: string[];
  @ApiPropertyOptional({ name: 'userNameLikeList', type: 'string[]', description: '用户名称-列表模糊条件' })
  userNameLikeList?: string[];
  /**
   * 英文名
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userNameEn', type: 'string', description: '英文名' })
  userNameEn?: string;
  @ApiPropertyOptional({ name: 'nuserNameEn', type: 'string', description: '英文名' })
  nuserNameEn?: string;
  @ApiPropertyOptional({ name: 'userNameEnLike', type: 'string', description: '英文名-模糊条件' })
  userNameEnLike?: string;
  @ApiPropertyOptional({ name: 'userNameEnList', type: 'string[]', description: '英文名-列表条件' })
  userNameEnList?: string[];
  @ApiPropertyOptional({ name: 'nuserNameEnLike', type: 'string', description: '英文名-模糊条件' })
  nuserNameEnLike?: string;
  @ApiPropertyOptional({ name: 'nuserNameEnList', type: 'string[]', description: '英文名-列表条件' })
  nuserNameEnList?: string[];
  @ApiPropertyOptional({ name: 'userNameEnLikeList', type: 'string[]', description: '英文名-列表模糊条件' })
  userNameEnLikeList?: string[];
  /**
   * 性别
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userSex', type: 'string', description: '性别' })
  userSex?: string;
  @ApiPropertyOptional({ name: 'nuserSex', type: 'string', description: '性别' })
  nuserSex?: string;
  @ApiPropertyOptional({ name: 'userSexLike', type: 'string', description: '性别-模糊条件' })
  userSexLike?: string;
  @ApiPropertyOptional({ name: 'userSexList', type: 'string[]', description: '性别-列表条件' })
  userSexList?: string[];
  @ApiPropertyOptional({ name: 'nuserSexLike', type: 'string', description: '性别-模糊条件' })
  nuserSexLike?: string;
  @ApiPropertyOptional({ name: 'nuserSexList', type: 'string[]', description: '性别-列表条件' })
  nuserSexList?: string[];
  @ApiPropertyOptional({ name: 'userSexLikeList', type: 'string[]', description: '性别-列表模糊条件' })
  userSexLikeList?: string[];
  /**
   * 用户手机号
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userPhone', type: 'string', description: '用户手机号' })
  userPhone?: string;
  @ApiPropertyOptional({ name: 'nuserPhone', type: 'string', description: '用户手机号' })
  nuserPhone?: string;
  @ApiPropertyOptional({ name: 'userPhoneLike', type: 'string', description: '用户手机号-模糊条件' })
  userPhoneLike?: string;
  @ApiPropertyOptional({ name: 'userPhoneList', type: 'string[]', description: '用户手机号-列表条件' })
  userPhoneList?: string[];
  @ApiPropertyOptional({ name: 'nuserPhoneLike', type: 'string', description: '用户手机号-模糊条件' })
  nuserPhoneLike?: string;
  @ApiPropertyOptional({ name: 'nuserPhoneList', type: 'string[]', description: '用户手机号-列表条件' })
  nuserPhoneList?: string[];
  @ApiPropertyOptional({ name: 'userPhoneLikeList', type: 'string[]', description: '用户手机号-列表模糊条件' })
  userPhoneLikeList?: string[];
  /**
   * 用户的邮箱
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userEmail', type: 'string', description: '用户的邮箱' })
  userEmail?: string;
  @ApiPropertyOptional({ name: 'nuserEmail', type: 'string', description: '用户的邮箱' })
  nuserEmail?: string;
  @ApiPropertyOptional({ name: 'userEmailLike', type: 'string', description: '用户的邮箱-模糊条件' })
  userEmailLike?: string;
  @ApiPropertyOptional({ name: 'userEmailList', type: 'string[]', description: '用户的邮箱-列表条件' })
  userEmailList?: string[];
  @ApiPropertyOptional({ name: 'nuserEmailLike', type: 'string', description: '用户的邮箱-模糊条件' })
  nuserEmailLike?: string;
  @ApiPropertyOptional({ name: 'nuserEmailList', type: 'string[]', description: '用户的邮箱-列表条件' })
  nuserEmailList?: string[];
  @ApiPropertyOptional({ name: 'userEmailLikeList', type: 'string[]', description: '用户的邮箱-列表模糊条件' })
  userEmailLikeList?: string[];
  /**
   * 用户头像路径
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userImageUrl', type: 'string', description: '用户头像路径' })
  userImageUrl?: string;
  @ApiPropertyOptional({ name: 'nuserImageUrl', type: 'string', description: '用户头像路径' })
  nuserImageUrl?: string;
  @ApiPropertyOptional({ name: 'userImageUrlLike', type: 'string', description: '用户头像路径-模糊条件' })
  userImageUrlLike?: string;
  @ApiPropertyOptional({ name: 'userImageUrlList', type: 'string[]', description: '用户头像路径-列表条件' })
  userImageUrlList?: string[];
  @ApiPropertyOptional({ name: 'nuserImageUrlLike', type: 'string', description: '用户头像路径-模糊条件' })
  nuserImageUrlLike?: string;
  @ApiPropertyOptional({ name: 'nuserImageUrlList', type: 'string[]', description: '用户头像路径-列表条件' })
  nuserImageUrlList?: string[];
  @ApiPropertyOptional({ name: 'userImageUrlLikeList', type: 'string[]', description: '用户头像路径-列表模糊条件' })
  userImageUrlLikeList?: string[];
  /**
   * 用户排序
   *
   * @type { number }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userOrder', type: 'number', description: '用户排序' })
  userOrder?: number;
  @ApiPropertyOptional({ name: 'nuserOrder', type: 'number', description: '用户排序' })
  nuserOrder?: number;
  @ApiPropertyOptional({ name: 'minUserOrder', type: 'number', description: '用户排序-最小值' })
  minUserOrder?: number;
  @ApiPropertyOptional({ name: 'maxUserOrder', type: 'number', description: '用户排序-最大值' })
  maxUserOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'userState', type: 'string', description: '状态' })
  userState?: string;
  @ApiProperty({ name: 'nuserState', type: 'string', description: '状态' })
  nuserState?: string;
  @ApiPropertyOptional({ name: 'userStateLike', type: 'string', description: '状态-模糊条件' })
  userStateLike?: string;
  @ApiPropertyOptional({ name: 'userStateList', type: 'string[]', description: '状态-列表条件' })
  userStateList?: string[];
  @ApiPropertyOptional({ name: 'nuserStateLike', type: 'string', description: '状态-模糊条件' })
  nuserStateLike?: string;
  @ApiPropertyOptional({ name: 'nuserStateList', type: 'string[]', description: '状态-列表条件' })
  nuserStateList?: string[];
  @ApiPropertyOptional({ name: 'userStateLikeList', type: 'string[]', description: '状态-列表模糊条件' })
  userStateLikeList?: string[];
  /**
   * 用户代码
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'userCode', type: 'string', description: '用户代码' })
  userCode?: string;
  @ApiProperty({ name: 'nuserCode', type: 'string', description: '用户代码' })
  nuserCode?: string;
  @ApiPropertyOptional({ name: 'userCodeLike', type: 'string', description: '用户代码-模糊条件' })
  userCodeLike?: string;
  @ApiPropertyOptional({ name: 'userCodeList', type: 'string[]', description: '用户代码-列表条件' })
  userCodeList?: string[];
  @ApiPropertyOptional({ name: 'nuserCodeLike', type: 'string', description: '用户代码-模糊条件' })
  nuserCodeLike?: string;
  @ApiPropertyOptional({ name: 'nuserCodeList', type: 'string[]', description: '用户代码-列表条件' })
  nuserCodeList?: string[];
  @ApiPropertyOptional({ name: 'userCodeLikeList', type: 'string[]', description: '用户代码-列表模糊条件' })
  userCodeLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'userCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  userCreateDate?: Date;
  @ApiProperty({ name: 'nuserCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nuserCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sUserCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sUserCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eUserCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eUserCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  userUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nuserUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nuserUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sUserUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sUserUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eUserUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eUserUpdateDate?: Date;
  /**
   * 用户最后登录时间
   *
   * @type { Date }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userLastLoginTime', type: 'Date', description: '用户最后登录时间' })
  @Type(() => Date)
  userLastLoginTime?: Date;
  @ApiPropertyOptional({ name: 'nuserLastLoginTime', type: 'Date', description: '用户最后登录时间' })
  @Type(() => Date)
  nuserLastLoginTime?: Date;
  @ApiPropertyOptional({ name: 'sUserLastLoginTime', type: 'Date', description: '用户最后登录时间-起始日期' })
  @Type(() => Date)
  sUserLastLoginTime?: Date;
  @ApiPropertyOptional({ name: 'eUserLastLoginTime', type: 'Date', description: '用户最后登录时间-终止日期' })
  @Type(() => Date)
  eUserLastLoginTime?: Date;
  /**
   * 用户最后登录Ip
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userLastLoginIp', type: 'string', description: '用户最后登录Ip' })
  userLastLoginIp?: string;
  @ApiPropertyOptional({ name: 'nuserLastLoginIp', type: 'string', description: '用户最后登录Ip' })
  nuserLastLoginIp?: string;
  @ApiPropertyOptional({ name: 'userLastLoginIpLike', type: 'string', description: '用户最后登录Ip-模糊条件' })
  userLastLoginIpLike?: string;
  @ApiPropertyOptional({ name: 'userLastLoginIpList', type: 'string[]', description: '用户最后登录Ip-列表条件' })
  userLastLoginIpList?: string[];
  @ApiPropertyOptional({ name: 'nuserLastLoginIpLike', type: 'string', description: '用户最后登录Ip-模糊条件' })
  nuserLastLoginIpLike?: string;
  @ApiPropertyOptional({ name: 'nuserLastLoginIpList', type: 'string[]', description: '用户最后登录Ip-列表条件' })
  nuserLastLoginIpList?: string[];
  @ApiPropertyOptional({ name: 'userLastLoginIpLikeList', type: 'string[]', description: '用户最后登录Ip-列表模糊条件' })
  userLastLoginIpLikeList?: string[];
  /**
   * 用户所属单位
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userUnit', type: 'string', description: '用户所属单位' })
  userUnit?: string;
  @ApiPropertyOptional({ name: 'nuserUnit', type: 'string', description: '用户所属单位' })
  nuserUnit?: string;
  @ApiPropertyOptional({ name: 'userUnitLike', type: 'string', description: '用户所属单位-模糊条件' })
  userUnitLike?: string;
  @ApiPropertyOptional({ name: 'userUnitList', type: 'string[]', description: '用户所属单位-列表条件' })
  userUnitList?: string[];
  @ApiPropertyOptional({ name: 'nuserUnitLike', type: 'string', description: '用户所属单位-模糊条件' })
  nuserUnitLike?: string;
  @ApiPropertyOptional({ name: 'nuserUnitList', type: 'string[]', description: '用户所属单位-列表条件' })
  nuserUnitList?: string[];
  @ApiPropertyOptional({ name: 'userUnitLikeList', type: 'string[]', description: '用户所属单位-列表模糊条件' })
  userUnitLikeList?: string[];
  /**
   * 用户职务/职称
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userJobTitle', type: 'string', description: '用户职务/职称' })
  userJobTitle?: string;
  @ApiPropertyOptional({ name: 'nuserJobTitle', type: 'string', description: '用户职务/职称' })
  nuserJobTitle?: string;
  @ApiPropertyOptional({ name: 'userJobTitleLike', type: 'string', description: '用户职务/职称-模糊条件' })
  userJobTitleLike?: string;
  @ApiPropertyOptional({ name: 'userJobTitleList', type: 'string[]', description: '用户职务/职称-列表条件' })
  userJobTitleList?: string[];
  @ApiPropertyOptional({ name: 'nuserJobTitleLike', type: 'string', description: '用户职务/职称-模糊条件' })
  nuserJobTitleLike?: string;
  @ApiPropertyOptional({ name: 'nuserJobTitleList', type: 'string[]', description: '用户职务/职称-列表条件' })
  nuserJobTitleList?: string[];
  @ApiPropertyOptional({ name: 'userJobTitleLikeList', type: 'string[]', description: '用户职务/职称-列表模糊条件' })
  userJobTitleLikeList?: string[];
  /**
   * 用户通讯地址
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userAddr', type: 'string', description: '用户通讯地址' })
  userAddr?: string;
  @ApiPropertyOptional({ name: 'nuserAddr', type: 'string', description: '用户通讯地址' })
  nuserAddr?: string;
  @ApiPropertyOptional({ name: 'userAddrLike', type: 'string', description: '用户通讯地址-模糊条件' })
  userAddrLike?: string;
  @ApiPropertyOptional({ name: 'userAddrList', type: 'string[]', description: '用户通讯地址-列表条件' })
  userAddrList?: string[];
  @ApiPropertyOptional({ name: 'nuserAddrLike', type: 'string', description: '用户通讯地址-模糊条件' })
  nuserAddrLike?: string;
  @ApiPropertyOptional({ name: 'nuserAddrList', type: 'string[]', description: '用户通讯地址-列表条件' })
  nuserAddrList?: string[];
  @ApiPropertyOptional({ name: 'userAddrLikeList', type: 'string[]', description: '用户通讯地址-列表模糊条件' })
  userAddrLikeList?: string[];
  /**
   * 邮编
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userPostcode', type: 'string', description: '邮编' })
  userPostcode?: string;
  @ApiPropertyOptional({ name: 'nuserPostcode', type: 'string', description: '邮编' })
  nuserPostcode?: string;
  @ApiPropertyOptional({ name: 'userPostcodeLike', type: 'string', description: '邮编-模糊条件' })
  userPostcodeLike?: string;
  @ApiPropertyOptional({ name: 'userPostcodeList', type: 'string[]', description: '邮编-列表条件' })
  userPostcodeList?: string[];
  @ApiPropertyOptional({ name: 'nuserPostcodeLike', type: 'string', description: '邮编-模糊条件' })
  nuserPostcodeLike?: string;
  @ApiPropertyOptional({ name: 'nuserPostcodeList', type: 'string[]', description: '邮编-列表条件' })
  nuserPostcodeList?: string[];
  @ApiPropertyOptional({ name: 'userPostcodeLikeList', type: 'string[]', description: '邮编-列表模糊条件' })
  userPostcodeLikeList?: string[];
  /**
   * 用户绑定种属
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userSamSpecies', type: 'string', description: '用户绑定种属' })
  userSamSpecies?: string;
  @ApiPropertyOptional({ name: 'nuserSamSpecies', type: 'string', description: '用户绑定种属' })
  nuserSamSpecies?: string;
  @ApiPropertyOptional({ name: 'userSamSpeciesLike', type: 'string', description: '用户绑定种属-模糊条件' })
  userSamSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'userSamSpeciesList', type: 'string[]', description: '用户绑定种属-列表条件' })
  userSamSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'nuserSamSpeciesLike', type: 'string', description: '用户绑定种属-模糊条件' })
  nuserSamSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'nuserSamSpeciesList', type: 'string[]', description: '用户绑定种属-列表条件' })
  nuserSamSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'userSamSpeciesLikeList', type: 'string[]', description: '用户绑定种属-列表模糊条件' })

  /**
   * 当前种属
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiPropertyOptional({ name: 'userCurrSpecies', type: 'string', description: '当前种属' })
  userCurrSpecies?: string;
  @ApiPropertyOptional({ name: 'nuserCurrSpecies', type: 'string', description: '当前种属' })
  nuserCurrSpecies?: string;
  @ApiPropertyOptional({ name: 'userCurrSpeciesLike', type: 'string', description: '当前种属-模糊条件' })
  userCurrSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'userCurrSpeciesList', type: 'string[]', description: '当前种属-列表条件' })
  userCurrSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'nuserCurrSpeciesLike', type: 'string', description: '当前种属-模糊条件' })
  nuserCurrSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'nuserCurrSpeciesList', type: 'string[]', description: '当前种属-列表条件' })
  nuserCurrSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'userCurrSpeciesLikeList', type: 'string[]', description: '当前种属-列表模糊条件' })
  userCurrSpeciesLikeList?: string[];

  userSamSpeciesLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserColNames)[]|(keyof SysUserColProps)[] }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserColNames)[] | (keyof SysUserColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysUserQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
