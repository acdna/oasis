import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysUserRoleColNames, SysUserRoleColProps } from '../../entity/ddl/sys.user.role.cols';
/**
 * SYS_USER_ROLE表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysUserRoleQDto
 * @class SysUserRoleQDto
 */
@ApiTags('SYS_USER_ROLE表的QDTO对象')
export class SysUserRoleQDto {
  /**
   * 用户角色ID
   *
   * @type { string }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'urId', type: 'string', description: '用户角色ID' })
  urId?: string;
  @ApiProperty({ name: 'nurId', type: 'string', description: '用户角色ID' })
  nurId?: string;
  @ApiPropertyOptional({ name: 'urIdList', type: 'string[]', description: '用户角色ID-列表条件' })
  urIdList?: string[];
  @ApiPropertyOptional({ name: 'nurIdList', type: 'string[]', description: '用户角色ID-列表条件' })
  nurIdList?: string[];
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'urRoleId', type: 'string', description: '角色ID' })
  urRoleId?: string;
  @ApiProperty({ name: 'nurRoleId', type: 'string', description: '角色ID' })
  nurRoleId?: string;
  @ApiPropertyOptional({ name: 'urRoleIdLike', type: 'string', description: '角色ID-模糊条件' })
  urRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'urRoleIdList', type: 'string[]', description: '角色ID-列表条件' })
  urRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'nurRoleIdLike', type: 'string', description: '角色ID-模糊条件' })
  nurRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'nurRoleIdList', type: 'string[]', description: '角色ID-列表条件' })
  nurRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'urRoleIdLikeList', type: 'string[]', description: '角色ID-列表模糊条件' })
  urRoleIdLikeList?: string[];
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'urUserLoginName', type: 'string', description: '用户帐号' })
  urUserLoginName?: string;
  @ApiProperty({ name: 'nurUserLoginName', type: 'string', description: '用户帐号' })
  nurUserLoginName?: string;
  @ApiPropertyOptional({ name: 'urUserLoginNameLike', type: 'string', description: '用户帐号-模糊条件' })
  urUserLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'urUserLoginNameList', type: 'string[]', description: '用户帐号-列表条件' })
  urUserLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'nurUserLoginNameLike', type: 'string', description: '用户帐号-模糊条件' })
  nurUserLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'nurUserLoginNameList', type: 'string[]', description: '用户帐号-列表条件' })
  nurUserLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'urUserLoginNameLikeList', type: 'string[]', description: '用户帐号-列表模糊条件' })
  urUserLoginNameLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserRoleColNames)[]|(keyof SysUserRoleColProps)[] }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserRoleColNames)[] | (keyof SysUserRoleColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysUserRoleQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
