import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysColDetailColNames, SysColDetailColProps } from '../../entity/ddl/sys.col.detail.cols';
/**
 * SYS_COL_DETAIL表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysColDetailQDto
 * @class SysColDetailQDto
 */
@ApiTags('SYS_COL_DETAIL表的QDTO对象')
export class SysColDetailQDto {
  /**
   * 主键ID
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'cdId', type: 'string', description: '主键ID' })
  cdId?: string;
  @ApiProperty({ name: 'ncdId', type: 'string', description: '主键ID' })
  ncdId?: string;
  @ApiPropertyOptional({ name: 'cdIdList', type: 'string[]', description: '主键ID-列表条件' })
  cdIdList?: string[];
  @ApiPropertyOptional({ name: 'ncdIdList', type: 'string[]', description: '主键ID-列表条件' })
  ncdIdList?: string[];
  /**
   * 表名
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'cdTableName', type: 'string', description: '表名' })
  cdTableName?: string;
  @ApiProperty({ name: 'ncdTableName', type: 'string', description: '表名' })
  ncdTableName?: string;
  @ApiPropertyOptional({ name: 'cdTableNameLike', type: 'string', description: '表名-模糊条件' })
  cdTableNameLike?: string;
  @ApiPropertyOptional({ name: 'cdTableNameList', type: 'string[]', description: '表名-列表条件' })
  cdTableNameList?: string[];
  @ApiPropertyOptional({ name: 'ncdTableNameLike', type: 'string', description: '表名-模糊条件' })
  ncdTableNameLike?: string;
  @ApiPropertyOptional({ name: 'ncdTableNameList', type: 'string[]', description: '表名-列表条件' })
  ncdTableNameList?: string[];
  @ApiPropertyOptional({ name: 'cdTableNameLikeList', type: 'string[]', description: '表名-列表模糊条件' })
  cdTableNameLikeList?: string[];
  /**
   * 列名
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'cdColName', type: 'string', description: '列名' })
  cdColName?: string;
  @ApiProperty({ name: 'ncdColName', type: 'string', description: '列名' })
  ncdColName?: string;
  @ApiPropertyOptional({ name: 'cdColNameLike', type: 'string', description: '列名-模糊条件' })
  cdColNameLike?: string;
  @ApiPropertyOptional({ name: 'cdColNameList', type: 'string[]', description: '列名-列表条件' })
  cdColNameList?: string[];
  @ApiPropertyOptional({ name: 'ncdColNameLike', type: 'string', description: '列名-模糊条件' })
  ncdColNameLike?: string;
  @ApiPropertyOptional({ name: 'ncdColNameList', type: 'string[]', description: '列名-列表条件' })
  ncdColNameList?: string[];
  @ApiPropertyOptional({ name: 'cdColNameLikeList', type: 'string[]', description: '列名-列表模糊条件' })
  cdColNameLikeList?: string[];
  /**
   * 列中文名
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiPropertyOptional({ name: 'cdDescName', type: 'string', description: '列中文名' })
  cdDescName?: string;
  @ApiPropertyOptional({ name: 'ncdDescName', type: 'string', description: '列中文名' })
  ncdDescName?: string;
  @ApiPropertyOptional({ name: 'cdDescNameLike', type: 'string', description: '列中文名-模糊条件' })
  cdDescNameLike?: string;
  @ApiPropertyOptional({ name: 'cdDescNameList', type: 'string[]', description: '列中文名-列表条件' })
  cdDescNameList?: string[];
  @ApiPropertyOptional({ name: 'ncdDescNameLike', type: 'string', description: '列中文名-模糊条件' })
  ncdDescNameLike?: string;
  @ApiPropertyOptional({ name: 'ncdDescNameList', type: 'string[]', description: '列中文名-列表条件' })
  ncdDescNameList?: string[];
  @ApiPropertyOptional({ name: 'cdDescNameLikeList', type: 'string[]', description: '列中文名-列表模糊条件' })
  cdDescNameLikeList?: string[];
  /**
   * 字典名
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiPropertyOptional({ name: 'cdDictName', type: 'string', description: '字典名' })
  cdDictName?: string;
  @ApiPropertyOptional({ name: 'ncdDictName', type: 'string', description: '字典名' })
  ncdDictName?: string;
  @ApiPropertyOptional({ name: 'cdDictNameLike', type: 'string', description: '字典名-模糊条件' })
  cdDictNameLike?: string;
  @ApiPropertyOptional({ name: 'cdDictNameList', type: 'string[]', description: '字典名-列表条件' })
  cdDictNameList?: string[];
  @ApiPropertyOptional({ name: 'ncdDictNameLike', type: 'string', description: '字典名-模糊条件' })
  ncdDictNameLike?: string;
  @ApiPropertyOptional({ name: 'ncdDictNameList', type: 'string[]', description: '字典名-列表条件' })
  ncdDictNameList?: string[];
  @ApiPropertyOptional({ name: 'cdDictNameLikeList', type: 'string[]', description: '字典名-列表模糊条件' })
  cdDictNameLikeList?: string[];
  /**
   * 模式
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiPropertyOptional({ name: 'cdMode', type: 'string', description: '模式' })
  cdMode?: string;
  @ApiPropertyOptional({ name: 'ncdMode', type: 'string', description: '模式' })
  ncdMode?: string;
  @ApiPropertyOptional({ name: 'cdModeLike', type: 'string', description: '模式-模糊条件' })
  cdModeLike?: string;
  @ApiPropertyOptional({ name: 'cdModeList', type: 'string[]', description: '模式-列表条件' })
  cdModeList?: string[];
  @ApiPropertyOptional({ name: 'ncdModeLike', type: 'string', description: '模式-模糊条件' })
  ncdModeLike?: string;
  @ApiPropertyOptional({ name: 'ncdModeList', type: 'string[]', description: '模式-列表条件' })
  ncdModeList?: string[];
  @ApiPropertyOptional({ name: 'cdModeLikeList', type: 'string[]', description: '模式-列表模糊条件' })
  cdModeLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysColDetailQDto
   */
  @ApiPropertyOptional({ name: 'cdCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  cdCreateDate?: Date;
  @ApiPropertyOptional({ name: 'ncdCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  ncdCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sCdCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sCdCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eCdCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eCdCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysColDetailQDto
   */
  @ApiPropertyOptional({ name: 'cdUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  cdUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'ncdUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  ncdUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sCdUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sCdUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eCdUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eCdUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysColDetailColNames)[]|(keyof SysColDetailColProps)[] }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysColDetailColNames)[] | (keyof SysColDetailColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysColDetailQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
