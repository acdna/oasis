import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysHelpColNames, SysHelpColProps } from '../../entity/ddl/sys.help.cols';
/**
 * SYS_HELP表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysHelpQDto
 * @class SysHelpQDto
 */
@ApiTags('SYS_HELP表的QDTO对象')
export class SysHelpQDto {
  /**
   * 帮助编号
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'helpId', type: 'string', description: '帮助编号' })
  helpId?: string;
  @ApiProperty({ name: 'nhelpId', type: 'string', description: '帮助编号' })
  nhelpId?: string;
  @ApiPropertyOptional({ name: 'helpIdList', type: 'string[]', description: '帮助编号-列表条件' })
  helpIdList?: string[];
  @ApiPropertyOptional({ name: 'nhelpIdList', type: 'string[]', description: '帮助编号-列表条件' })
  nhelpIdList?: string[];
  /**
   * 帮助问题
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpQuestion', type: 'string', description: '帮助问题' })
  helpQuestion?: string;
  @ApiPropertyOptional({ name: 'nhelpQuestion', type: 'string', description: '帮助问题' })
  nhelpQuestion?: string;
  @ApiPropertyOptional({ name: 'helpQuestionLike', type: 'string', description: '帮助问题-模糊条件' })
  helpQuestionLike?: string;
  @ApiPropertyOptional({ name: 'helpQuestionList', type: 'string[]', description: '帮助问题-列表条件' })
  helpQuestionList?: string[];
  @ApiPropertyOptional({ name: 'nhelpQuestionLike', type: 'string', description: '帮助问题-模糊条件' })
  nhelpQuestionLike?: string;
  @ApiPropertyOptional({ name: 'nhelpQuestionList', type: 'string[]', description: '帮助问题-列表条件' })
  nhelpQuestionList?: string[];
  @ApiPropertyOptional({ name: 'helpQuestionLikeList', type: 'string[]', description: '帮助问题-列表模糊条件' })
  helpQuestionLikeList?: string[];
  /**
   * 帮助回答
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpAnswer', type: 'string', description: '帮助回答' })
  helpAnswer?: string;
  @ApiPropertyOptional({ name: 'nhelpAnswer', type: 'string', description: '帮助回答' })
  nhelpAnswer?: string;
  @ApiPropertyOptional({ name: 'helpAnswerLike', type: 'string', description: '帮助回答-模糊条件' })
  helpAnswerLike?: string;
  @ApiPropertyOptional({ name: 'helpAnswerList', type: 'string[]', description: '帮助回答-列表条件' })
  helpAnswerList?: string[];
  @ApiPropertyOptional({ name: 'nhelpAnswerLike', type: 'string', description: '帮助回答-模糊条件' })
  nhelpAnswerLike?: string;
  @ApiPropertyOptional({ name: 'nhelpAnswerList', type: 'string[]', description: '帮助回答-列表条件' })
  nhelpAnswerList?: string[];
  @ApiPropertyOptional({ name: 'helpAnswerLikeList', type: 'string[]', description: '帮助回答-列表模糊条件' })
  helpAnswerLikeList?: string[];
  /**
   * 问题编号父级编号
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpParentId', type: 'string', description: '问题编号父级编号' })
  helpParentId?: string;
  @ApiPropertyOptional({ name: 'nhelpParentId', type: 'string', description: '问题编号父级编号' })
  nhelpParentId?: string;
  @ApiPropertyOptional({ name: 'helpParentIdLike', type: 'string', description: '问题编号父级编号-模糊条件' })
  helpParentIdLike?: string;
  @ApiPropertyOptional({ name: 'helpParentIdList', type: 'string[]', description: '问题编号父级编号-列表条件' })
  helpParentIdList?: string[];
  @ApiPropertyOptional({ name: 'nhelpParentIdLike', type: 'string', description: '问题编号父级编号-模糊条件' })
  nhelpParentIdLike?: string;
  @ApiPropertyOptional({ name: 'nhelpParentIdList', type: 'string[]', description: '问题编号父级编号-列表条件' })
  nhelpParentIdList?: string[];
  @ApiPropertyOptional({ name: 'helpParentIdLikeList', type: 'string[]', description: '问题编号父级编号-列表模糊条件' })
  helpParentIdLikeList?: string[];
  /**
   * 帮助类别
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpType', type: 'string', description: '帮助类别' })
  helpType?: string;
  @ApiPropertyOptional({ name: 'nhelpType', type: 'string', description: '帮助类别' })
  nhelpType?: string;
  @ApiPropertyOptional({ name: 'helpTypeLike', type: 'string', description: '帮助类别-模糊条件' })
  helpTypeLike?: string;
  @ApiPropertyOptional({ name: 'helpTypeList', type: 'string[]', description: '帮助类别-列表条件' })
  helpTypeList?: string[];
  @ApiPropertyOptional({ name: 'nhelpTypeLike', type: 'string', description: '帮助类别-模糊条件' })
  nhelpTypeLike?: string;
  @ApiPropertyOptional({ name: 'nhelpTypeList', type: 'string[]', description: '帮助类别-列表条件' })
  nhelpTypeList?: string[];
  @ApiPropertyOptional({ name: 'helpTypeLikeList', type: 'string[]', description: '帮助类别-列表模糊条件' })
  helpTypeLikeList?: string[];
  /**
   * 帮助匹配路径
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpPath', type: 'string', description: '帮助匹配路径' })
  helpPath?: string;
  @ApiPropertyOptional({ name: 'nhelpPath', type: 'string', description: '帮助匹配路径' })
  nhelpPath?: string;
  @ApiPropertyOptional({ name: 'helpPathLike', type: 'string', description: '帮助匹配路径-模糊条件' })
  helpPathLike?: string;
  @ApiPropertyOptional({ name: 'helpPathList', type: 'string[]', description: '帮助匹配路径-列表条件' })
  helpPathList?: string[];
  @ApiPropertyOptional({ name: 'nhelpPathLike', type: 'string', description: '帮助匹配路径-模糊条件' })
  nhelpPathLike?: string;
  @ApiPropertyOptional({ name: 'nhelpPathList', type: 'string[]', description: '帮助匹配路径-列表条件' })
  nhelpPathList?: string[];
  @ApiPropertyOptional({ name: 'helpPathLikeList', type: 'string[]', description: '帮助匹配路径-列表模糊条件' })
  helpPathLikeList?: string[];
  /**
   * 排序
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpSort', type: 'string', description: '排序' })
  helpSort?: string;
  @ApiPropertyOptional({ name: 'nhelpSort', type: 'string', description: '排序' })
  nhelpSort?: string;
  @ApiPropertyOptional({ name: 'helpSortLike', type: 'string', description: '排序-模糊条件' })
  helpSortLike?: string;
  @ApiPropertyOptional({ name: 'helpSortList', type: 'string[]', description: '排序-列表条件' })
  helpSortList?: string[];
  @ApiPropertyOptional({ name: 'nhelpSortLike', type: 'string', description: '排序-模糊条件' })
  nhelpSortLike?: string;
  @ApiPropertyOptional({ name: 'nhelpSortList', type: 'string[]', description: '排序-列表条件' })
  nhelpSortList?: string[];
  @ApiPropertyOptional({ name: 'helpSortLikeList', type: 'string[]', description: '排序-列表模糊条件' })
  helpSortLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  helpCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nhelpCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nhelpCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sHelpCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sHelpCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eHelpCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eHelpCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysHelpQDto
   */
  @ApiPropertyOptional({ name: 'helpUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  helpUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nhelpUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nhelpUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sHelpUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sHelpUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eHelpUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eHelpUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysHelpColNames)[]|(keyof SysHelpColProps)[] }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysHelpColNames)[] | (keyof SysHelpColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysHelpQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
