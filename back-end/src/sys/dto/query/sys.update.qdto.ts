import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysUpdateColNames, SysUpdateColProps } from '../../entity/ddl/sys.update.cols';
/**
 * SYS_UPDATE表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysUpdateQDto
 * @class SysUpdateQDto
 */
@ApiTags('SYS_UPDATE表的QDTO对象')
export class SysUpdateQDto {
  /**
   * 系统升级日志主键ID
   *
   * @type { string }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'updateId', type: 'string', description: '系统升级日志主键ID' })
  updateId?: string;
  @ApiProperty({ name: 'nupdateId', type: 'string', description: '系统升级日志主键ID' })
  nupdateId?: string;
  @ApiPropertyOptional({ name: 'updateIdList', type: 'string[]', description: '系统升级日志主键ID-列表条件' })
  updateIdList?: string[];
  @ApiPropertyOptional({ name: 'nupdateIdList', type: 'string[]', description: '系统升级日志主键ID-列表条件' })
  nupdateIdList?: string[];
  /**
   * 升级版本号
   *
   * @type { string }
   * @memberof SysUpdateQDto
   */
  @ApiPropertyOptional({ name: 'updateVersion', type: 'string', description: '升级版本号' })
  updateVersion?: string;
  @ApiPropertyOptional({ name: 'nupdateVersion', type: 'string', description: '升级版本号' })
  nupdateVersion?: string;
  @ApiPropertyOptional({ name: 'updateVersionLike', type: 'string', description: '升级版本号-模糊条件' })
  updateVersionLike?: string;
  @ApiPropertyOptional({ name: 'updateVersionList', type: 'string[]', description: '升级版本号-列表条件' })
  updateVersionList?: string[];
  @ApiPropertyOptional({ name: 'nupdateVersionLike', type: 'string', description: '升级版本号-模糊条件' })
  nupdateVersionLike?: string;
  @ApiPropertyOptional({ name: 'nupdateVersionList', type: 'string[]', description: '升级版本号-列表条件' })
  nupdateVersionList?: string[];
  @ApiPropertyOptional({ name: 'updateVersionLikeList', type: 'string[]', description: '升级版本号-列表模糊条件' })
  updateVersionLikeList?: string[];
  /**
   * 升级内容
   *
   * @type { string }
   * @memberof SysUpdateQDto
   */
  @ApiPropertyOptional({ name: 'updateContent', type: 'string', description: '升级内容' })
  updateContent?: string;
  @ApiPropertyOptional({ name: 'nupdateContent', type: 'string', description: '升级内容' })
  nupdateContent?: string;
  @ApiPropertyOptional({ name: 'updateContentLike', type: 'string', description: '升级内容-模糊条件' })
  updateContentLike?: string;
  @ApiPropertyOptional({ name: 'updateContentList', type: 'string[]', description: '升级内容-列表条件' })
  updateContentList?: string[];
  @ApiPropertyOptional({ name: 'nupdateContentLike', type: 'string', description: '升级内容-模糊条件' })
  nupdateContentLike?: string;
  @ApiPropertyOptional({ name: 'nupdateContentList', type: 'string[]', description: '升级内容-列表条件' })
  nupdateContentList?: string[];
  @ApiPropertyOptional({ name: 'updateContentLikeList', type: 'string[]', description: '升级内容-列表模糊条件' })
  updateContentLikeList?: string[];
  /**
   * 升级负责人
   *
   * @type { string }
   * @memberof SysUpdateQDto
   */
  @ApiPropertyOptional({ name: 'updateManager', type: 'string', description: '升级负责人' })
  updateManager?: string;
  @ApiPropertyOptional({ name: 'nupdateManager', type: 'string', description: '升级负责人' })
  nupdateManager?: string;
  @ApiPropertyOptional({ name: 'updateManagerLike', type: 'string', description: '升级负责人-模糊条件' })
  updateManagerLike?: string;
  @ApiPropertyOptional({ name: 'updateManagerList', type: 'string[]', description: '升级负责人-列表条件' })
  updateManagerList?: string[];
  @ApiPropertyOptional({ name: 'nupdateManagerLike', type: 'string', description: '升级负责人-模糊条件' })
  nupdateManagerLike?: string;
  @ApiPropertyOptional({ name: 'nupdateManagerList', type: 'string[]', description: '升级负责人-列表条件' })
  nupdateManagerList?: string[];
  @ApiPropertyOptional({ name: 'updateManagerLikeList', type: 'string[]', description: '升级负责人-列表模糊条件' })
  updateManagerLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUpdateQDto
   */
  @ApiPropertyOptional({ name: 'updateCreateTime', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  updateCreateTime?: Date;
  @ApiPropertyOptional({ name: 'nupdateCreateTime', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nupdateCreateTime?: Date;
  @ApiPropertyOptional({ name: 'sUpdateCreateTime', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sUpdateCreateTime?: Date;
  @ApiPropertyOptional({ name: 'eUpdateCreateTime', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eUpdateCreateTime?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysUpdateColNames)[]|(keyof SysUpdateColProps)[] }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUpdateColNames)[] | (keyof SysUpdateColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysUpdateQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
