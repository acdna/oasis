import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysCmsNoticeColNames, SysCmsNoticeColProps } from '../../entity/ddl/sys.cms.notice.cols';
/**
 * SYS_CMS_NOTICE表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysCmsNoticeQDto
 * @class SysCmsNoticeQDto
 */
@ApiTags('SYS_CMS_NOTICE表的QDTO对象')
export class SysCmsNoticeQDto {
  /**
   * 公告编号
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'scNoticeId', type: 'string', description: '公告编号' })
  scNoticeId?: string;
  @ApiProperty({ name: 'nscNoticeId', type: 'string', description: '公告编号' })
  nscNoticeId?: string;
  @ApiPropertyOptional({ name: 'scNoticeIdList', type: 'string[]', description: '公告编号-列表条件' })
  scNoticeIdList?: string[];
  @ApiPropertyOptional({ name: 'nscNoticeIdList', type: 'string[]', description: '公告编号-列表条件' })
  nscNoticeIdList?: string[];
  /**
   * 公告标题
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiPropertyOptional({ name: 'scNoticeTitle', type: 'string', description: '公告标题' })
  scNoticeTitle?: string;
  @ApiPropertyOptional({ name: 'nscNoticeTitle', type: 'string', description: '公告标题' })
  nscNoticeTitle?: string;
  @ApiPropertyOptional({ name: 'scNoticeTitleLike', type: 'string', description: '公告标题-模糊条件' })
  scNoticeTitleLike?: string;
  @ApiPropertyOptional({ name: 'scNoticeTitleList', type: 'string[]', description: '公告标题-列表条件' })
  scNoticeTitleList?: string[];
  @ApiPropertyOptional({ name: 'nscNoticeTitleLike', type: 'string', description: '公告标题-模糊条件' })
  nscNoticeTitleLike?: string;
  @ApiPropertyOptional({ name: 'nscNoticeTitleList', type: 'string[]', description: '公告标题-列表条件' })
  nscNoticeTitleList?: string[];
  @ApiPropertyOptional({ name: 'scNoticeTitleLikeList', type: 'string[]', description: '公告标题-列表模糊条件' })
  scNoticeTitleLikeList?: string[];
  /**
   * 公告内容
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiPropertyOptional({ name: 'scNoticeContent', type: 'string', description: '公告内容' })
  scNoticeContent?: string;
  @ApiPropertyOptional({ name: 'nscNoticeContent', type: 'string', description: '公告内容' })
  nscNoticeContent?: string;
  @ApiPropertyOptional({ name: 'scNoticeContentLike', type: 'string', description: '公告内容-模糊条件' })
  scNoticeContentLike?: string;
  @ApiPropertyOptional({ name: 'scNoticeContentList', type: 'string[]', description: '公告内容-列表条件' })
  scNoticeContentList?: string[];
  @ApiPropertyOptional({ name: 'nscNoticeContentLike', type: 'string', description: '公告内容-模糊条件' })
  nscNoticeContentLike?: string;
  @ApiPropertyOptional({ name: 'nscNoticeContentList', type: 'string[]', description: '公告内容-列表条件' })
  nscNoticeContentList?: string[];
  @ApiPropertyOptional({ name: 'scNoticeContentLikeList', type: 'string[]', description: '公告内容-列表模糊条件' })
  scNoticeContentLikeList?: string[];
  /**
   * 是否是新文件
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiPropertyOptional({ name: 'scNoticeIsNew', type: 'string', description: '是否是新文件' })
  scNoticeIsNew?: string;
  @ApiPropertyOptional({ name: 'nscNoticeIsNew', type: 'string', description: '是否是新文件' })
  nscNoticeIsNew?: string;
  @ApiPropertyOptional({ name: 'scNoticeIsNewLike', type: 'string', description: '是否是新文件-模糊条件' })
  scNoticeIsNewLike?: string;
  @ApiPropertyOptional({ name: 'scNoticeIsNewList', type: 'string[]', description: '是否是新文件-列表条件' })
  scNoticeIsNewList?: string[];
  @ApiPropertyOptional({ name: 'nscNoticeIsNewLike', type: 'string', description: '是否是新文件-模糊条件' })
  nscNoticeIsNewLike?: string;
  @ApiPropertyOptional({ name: 'nscNoticeIsNewList', type: 'string[]', description: '是否是新文件-列表条件' })
  nscNoticeIsNewList?: string[];
  @ApiPropertyOptional({ name: 'scNoticeIsNewLikeList', type: 'string[]', description: '是否是新文件-列表模糊条件' })
  scNoticeIsNewLikeList?: string[];
  /**
   * 公告发布者
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiPropertyOptional({ name: 'scNoticeManager', type: 'string', description: '公告发布者' })
  scNoticeManager?: string;
  @ApiPropertyOptional({ name: 'nscNoticeManager', type: 'string', description: '公告发布者' })
  nscNoticeManager?: string;
  @ApiPropertyOptional({ name: 'scNoticeManagerLike', type: 'string', description: '公告发布者-模糊条件' })
  scNoticeManagerLike?: string;
  @ApiPropertyOptional({ name: 'scNoticeManagerList', type: 'string[]', description: '公告发布者-列表条件' })
  scNoticeManagerList?: string[];
  @ApiPropertyOptional({ name: 'nscNoticeManagerLike', type: 'string', description: '公告发布者-模糊条件' })
  nscNoticeManagerLike?: string;
  @ApiPropertyOptional({ name: 'nscNoticeManagerList', type: 'string[]', description: '公告发布者-列表条件' })
  nscNoticeManagerList?: string[];
  @ApiPropertyOptional({ name: 'scNoticeManagerLikeList', type: 'string[]', description: '公告发布者-列表模糊条件' })
  scNoticeManagerLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsNoticeQDto
   */
  @ApiPropertyOptional({ name: 'scNoticeCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  scNoticeCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nscNoticeCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nscNoticeCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sScNoticeCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sScNoticeCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eScNoticeCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eScNoticeCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsNoticeQDto
   */
  @ApiPropertyOptional({ name: 'scNoticeUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  scNoticeUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nscNoticeUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nscNoticeUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sScNoticeUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sScNoticeUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eScNoticeUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eScNoticeUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysCmsNoticeColNames)[]|(keyof SysCmsNoticeColProps)[] }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysCmsNoticeColNames)[] | (keyof SysCmsNoticeColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysCmsNoticeQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
