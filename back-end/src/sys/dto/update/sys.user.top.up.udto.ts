import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysUserTopUpColNames } from '../../entity/ddl/sys.user.top.up.cols';
/**
 * 用户充值信息表表的UDTO对象
 * @date 12/30/2020, 3:09:40 PM
 * @author jiangbin
 * @export SysUserTopUpUDto
 * @class SysUserTopUpUDto
 */
@ApiTags('SYS_USER_TOP_UP表的UDTO对象')
export class SysUserTopUpUDto {
  /**
   * id
   *
   * @type { string }
   * @memberof SysUserTopUpUDto
   */
  @ApiProperty({ name: 'utuId', type: 'string', description: 'id' })
  utuId: string;
  /**
   * 用户登录名
   *
   * @type { string }
   * @memberof SysUserTopUpUDto
   */
  @ApiProperty({ name: 'utuLoginName', type: 'string', description: '用户登录名' })
  utuLoginName: string;
  /**
   * 序列号
   *
   * @type { string }
   * @memberof SysUserTopUpUDto
   */
  @ApiProperty({ name: 'utuSerialNumber', type: 'string', description: '序列号' })
  utuSerialNumber: string;
  /**
   * 计费开始时间
   *
   * @type { Date }
   * @memberof SysUserTopUpUDto
   */
  @ApiPropertyOptional({ name: 'utuBillingStartDate', type: 'Date', description: '计费开始时间' })
  utuBillingStartDate?: Date;
  /**
   * 计费结束时间
   *
   * @type { Date }
   * @memberof SysUserTopUpUDto
   */
  @ApiPropertyOptional({ name: 'utuBillingEndDate', type: 'Date', description: '计费结束时间' })
  utuBillingEndDate?: Date;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUserTopUpUDto
   */
  @ApiPropertyOptional({ name: 'utuState', type: 'string', description: '状态' })
  utuState?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserTopUpUDto
   */
  @ApiPropertyOptional({ name: 'utuCreateDate', type: 'Date', description: '创建日期' })
  utuCreateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserTopUpColNames)[] }
   * @memberof SysUserTopUpUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserTopUpColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysUserTopUpUDto
   */
  @ApiProperty({ name: 'utuIdList', type: 'string[]', description: 'ID列表' })
  utuIdList?: string[];
}
