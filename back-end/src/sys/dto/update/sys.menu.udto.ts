import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysMenuColNames } from '../../entity/ddl/sys.menu.cols';
/**
 * SYS_MENU表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysMenuUDto
 * @class SysMenuUDto
 */
@ApiTags('SYS_MENU表的UDTO对象')
export class SysMenuUDto {
  /**
   * 菜单ID号
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiProperty({ name: 'menuId', type: 'string', description: '菜单ID号' })
  menuId: string;
  /**
   * 菜单名称
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiProperty({ name: 'menuName', type: 'string', description: '菜单名称' })
  menuName: string;
  /**
   * 英文菜单名
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiPropertyOptional({ name: 'menuNameEn', type: 'string', description: '英文菜单名' })
  menuNameEn?: string;
  /**
   * 菜单连接地址
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiPropertyOptional({ name: 'menuUrl', type: 'string', description: '菜单连接地址' })
  menuUrl?: string;
  /**
   * 父菜单ID
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiPropertyOptional({ name: 'menuParentId', type: 'string', description: '父菜单ID' })
  menuParentId?: string;
  /**
   * 菜单顺序
   *
   * @type { number }
   * @memberof SysMenuUDto
   */
  @ApiProperty({ name: 'menuOrder', type: 'number', description: '菜单顺序' })
  menuOrder: number;
  /**
   * 菜单备注
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiPropertyOptional({ name: 'menuRemark', type: 'string', description: '菜单备注' })
  menuRemark?: string;
  /**
   * 菜单状态
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiProperty({ name: 'menuState', type: 'string', description: '菜单状态' })
  menuState: string;
  /**
   * 菜单类型
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiPropertyOptional({ name: 'menuType', type: 'string', description: '菜单类型' })
  menuType?: string;
  /**
   * 菜单图标CSS名称
   *
   * @type { string }
   * @memberof SysMenuUDto
   */
  @ApiPropertyOptional({ name: 'menuIconClass', type: 'string', description: '菜单图标CSS名称' })
  menuIconClass?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysMenuColNames)[] }
   * @memberof SysMenuUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysMenuColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysMenuUDto
   */
  @ApiProperty({ name: 'menuIdList', type: 'string[]', description: 'ID列表' })
  menuIdList?: string[];
}
