import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysCmsNoticeColNames } from '../../entity/ddl/sys.cms.notice.cols';
/**
 * SYS_CMS_NOTICE表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysCmsNoticeUDto
 * @class SysCmsNoticeUDto
 */
@ApiTags('SYS_CMS_NOTICE表的UDTO对象')
export class SysCmsNoticeUDto {
  /**
   * 公告编号
   *
   * @type { string }
   * @memberof SysCmsNoticeUDto
   */
  @ApiProperty({ name: 'scNoticeId', type: 'string', description: '公告编号' })
  scNoticeId: string;
  /**
   * 公告标题
   *
   * @type { string }
   * @memberof SysCmsNoticeUDto
   */
  @ApiPropertyOptional({ name: 'scNoticeTitle', type: 'string', description: '公告标题' })
  scNoticeTitle?: string;
  /**
   * 公告内容
   *
   * @type { string }
   * @memberof SysCmsNoticeUDto
   */
  @ApiPropertyOptional({ name: 'scNoticeContent', type: 'string', description: '公告内容' })
  scNoticeContent?: string;
  /**
   * 是否是新文件
   *
   * @type { string }
   * @memberof SysCmsNoticeUDto
   */
  @ApiPropertyOptional({ name: 'scNoticeIsNew', type: 'string', description: '是否是新文件' })
  scNoticeIsNew?: string;
  /**
   * 公告发布者
   *
   * @type { string }
   * @memberof SysCmsNoticeUDto
   */
  @ApiPropertyOptional({ name: 'scNoticeManager', type: 'string', description: '公告发布者' })
  scNoticeManager?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsNoticeUDto
   */
  @ApiPropertyOptional({ name: 'scNoticeCreateDate', type: 'Date', description: '创建日期' })
  scNoticeCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsNoticeUDto
   */
  @ApiPropertyOptional({ name: 'scNoticeUpdateDate', type: 'Date', description: '更新日期' })
  scNoticeUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysCmsNoticeColNames)[] }
   * @memberof SysCmsNoticeUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysCmsNoticeColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysCmsNoticeUDto
   */
  @ApiProperty({ name: 'scNoticeIdList', type: 'string[]', description: 'ID列表' })
  scNoticeIdList?: string[];
}
