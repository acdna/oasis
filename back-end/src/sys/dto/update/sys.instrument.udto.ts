import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysInstrumentColNames } from '../../entity/ddl/sys.instrument.cols';
/**
 * SYS_INSTRUMENT表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysInstrumentUDto
 * @class SysInstrumentUDto
 */
@ApiTags('SYS_INSTRUMENT表的UDTO对象')
export class SysInstrumentUDto {
  /**
   * 仪器编号
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiProperty({ name: 'insId', type: 'string', description: '仪器编号' })
  insId: string;
  /**
   * 仪器条码号
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insBarcode', type: 'string', description: '仪器条码号' })
  insBarcode?: string;
  /**
   * 仪器名称
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insName', type: 'string', description: '仪器名称' })
  insName?: string;
  /**
   * 仪器型号
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insModel', type: 'string', description: '仪器型号' })
  insModel?: string;
  /**
   * 仪器负责人
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insManager', type: 'string', description: '仪器负责人' })
  insManager?: string;
  /**
   * 仪器类型
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insType', type: 'string', description: '仪器类型' })
  insType?: string;
  /**
   * 仪器描述
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insRemark', type: 'string', description: '仪器描述' })
  insRemark?: string;
  /**
   * 仪器购买日期
   *
   * @type { Date }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insBuyDate', type: 'Date', description: '仪器购买日期' })
  insBuyDate?: Date;
  /**
   * 仪器维护周期
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insMaintainPeriod', type: 'string', description: '仪器维护周期' })
  insMaintainPeriod?: string;
  /**
   * 仪器厂商
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insMaker', type: 'string', description: '仪器厂商' })
  insMaker?: string;
  /**
   * 仪器价格
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insPrice', type: 'string', description: '仪器价格' })
  insPrice?: string;
  /**
   * insExtraProvide
   *
   * @type { string }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insExtraProvide', type: 'string', description: 'insExtraProvide' })
  insExtraProvide?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insCreateDate', type: 'Date', description: '创建日期' })
  insCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysInstrumentUDto
   */
  @ApiPropertyOptional({ name: 'insUpdateDate', type: 'Date', description: '更新日期' })
  insUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysInstrumentColNames)[] }
   * @memberof SysInstrumentUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysInstrumentColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysInstrumentUDto
   */
  @ApiProperty({ name: 'insIdList', type: 'string[]', description: 'ID列表' })
  insIdList?: string[];
}
