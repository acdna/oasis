import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysBillingCardColNames } from '../../entity/ddl/sys.billing.card.cols';
/**
 * 计费卡信息表表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysBillingCardUDto
 * @class SysBillingCardUDto
 */
@ApiTags('SYS_BILLING_CARD表的UDTO对象')
export class SysBillingCardUDto {
  /**
   * id
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiProperty({ name: 'bcId', type: 'string', description: 'id' })
  bcId: string;
  /**
   * 序列号信息
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiProperty({ name: 'bcSerialNumber', type: 'string', description: '序列号信息' })
  bcSerialNumber: string;
  /**
   * 总额
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcAmount', type: 'string', description: '总额' })
  bcAmount?: string;
  /**
   * 总时间
   *
   * @type { number }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcTotalTime', type: 'number', description: '总时间' })
  bcTotalTime?: number;
  /**
   * 类型
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcType', type: 'string', description: '类型' })
  bcType?: string;
  /**
   * 绑定帐号
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcAccount', type: 'string', description: '绑定帐号' })
  bcAccount?: string;
  /**
   * 绑定密码
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcPassword', type: 'string', description: '绑定密码' })
  bcPassword?: string;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcState', type: 'string', description: '状态' })
  bcState?: string;
  /**
   * 最大指纹数
   *
   * @type { string }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcBiggestGeneCount', type: 'string', description: '最大指纹数' })
  bcBiggestGeneCount?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysBillingCardUDto
   */
  @ApiPropertyOptional({ name: 'bcCreateDate', type: 'Date', description: '创建日期' })
  bcCreateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysBillingCardColNames)[] }
   * @memberof SysBillingCardUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysBillingCardColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysBillingCardUDto
   */
  @ApiProperty({ name: 'bcIdList', type: 'string[]', description: 'ID列表' })
  bcIdList?: string[];
}
