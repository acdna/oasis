import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysRoleColNames } from '../../entity/ddl/sys.role.cols';
/**
 * SYS_ROLE表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysRoleUDto
 * @class SysRoleUDto
 */
@ApiTags('SYS_ROLE表的UDTO对象')
export class SysRoleUDto {
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleUDto
   */
  @ApiProperty({ name: 'roleId', type: 'string', description: '角色ID' })
  roleId: string;
  /**
   * 角色名称
   *
   * @type { string }
   * @memberof SysRoleUDto
   */
  @ApiPropertyOptional({ name: 'roleName', type: 'string', description: '角色名称' })
  roleName?: string;
  /**
   * roleNameEn
   *
   * @type { string }
   * @memberof SysRoleUDto
   */
  @ApiPropertyOptional({ name: 'roleNameEn', type: 'string', description: 'roleNameEn' })
  roleNameEn?: string;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysRoleUDto
   */
  @ApiPropertyOptional({ name: 'roleState', type: 'string', description: '状态' })
  roleState?: string;
  /**
   * 角色所属系统
   *
   * @type { string }
   * @memberof SysRoleUDto
   */
  @ApiPropertyOptional({ name: 'roleSys', type: 'string', description: '角色所属系统' })
  roleSys?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysRoleColNames)[] }
   * @memberof SysRoleUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRoleColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysRoleUDto
   */
  @ApiProperty({ name: 'roleIdList', type: 'string[]', description: 'ID列表' })
  roleIdList?: string[];
}
