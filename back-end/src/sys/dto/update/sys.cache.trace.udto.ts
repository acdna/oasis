import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysCacheTraceColNames } from '../../entity/ddl/sys.cache.trace.cols';
/**
 * 缓存信息表表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysCacheTraceUDto
 * @class SysCacheTraceUDto
 */
@ApiTags('SYS_CACHE_TRACE表的UDTO对象')
export class SysCacheTraceUDto {
  /**
   * 缓存记录ID
   *
   * @type { string }
   * @memberof SysCacheTraceUDto
   */
  @ApiProperty({ name: 'ctId', type: 'string', description: '缓存记录ID' })
  ctId: string;
  /**
   * 缓存类型代码
   *
   * @type { string }
   * @memberof SysCacheTraceUDto
   */
  @ApiPropertyOptional({ name: 'ctCode', type: 'string', description: '缓存类型代码' })
  ctCode?: string;
  /**
   * 缓存操作类型代码
   *
   * @type { string }
   * @memberof SysCacheTraceUDto
   */
  @ApiPropertyOptional({ name: 'ctOperate', type: 'string', description: '缓存操作类型代码' })
  ctOperate?: string;
  /**
   * 缓存需要操作的目标记录ID
   *
   * @type { string }
   * @memberof SysCacheTraceUDto
   */
  @ApiPropertyOptional({ name: 'ctTargetId', type: 'string', description: '缓存需要操作的目标记录ID' })
  ctTargetId?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCacheTraceUDto
   */
  @ApiPropertyOptional({ name: 'ctCreateDate', type: 'Date', description: '创建日期' })
  ctCreateDate?: Date;
  /**
   * 缓存备注信息
   *
   * @type { string }
   * @memberof SysCacheTraceUDto
   */
  @ApiPropertyOptional({ name: 'ctComment', type: 'string', description: '缓存备注信息' })
  ctComment?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysCacheTraceColNames)[] }
   * @memberof SysCacheTraceUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysCacheTraceColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysCacheTraceUDto
   */
  @ApiProperty({ name: 'ctIdList', type: 'string[]', description: 'ID列表' })
  ctIdList?: string[];
}
