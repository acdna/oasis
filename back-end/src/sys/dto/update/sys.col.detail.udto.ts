import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysColDetailColNames } from '../../entity/ddl/sys.col.detail.cols';
/**
 * SYS_COL_DETAIL表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysColDetailUDto
 * @class SysColDetailUDto
 */
@ApiTags('SYS_COL_DETAIL表的UDTO对象')
export class SysColDetailUDto {
  /**
   * 主键ID
   *
   * @type { string }
   * @memberof SysColDetailUDto
   */
  @ApiProperty({ name: 'cdId', type: 'string', description: '主键ID' })
  cdId: string;
  /**
   * 表名
   *
   * @type { string }
   * @memberof SysColDetailUDto
   */
  @ApiProperty({ name: 'cdTableName', type: 'string', description: '表名' })
  cdTableName: string;
  /**
   * 列名
   *
   * @type { string }
   * @memberof SysColDetailUDto
   */
  @ApiProperty({ name: 'cdColName', type: 'string', description: '列名' })
  cdColName: string;
  /**
   * 列中文名
   *
   * @type { string }
   * @memberof SysColDetailUDto
   */
  @ApiPropertyOptional({ name: 'cdDescName', type: 'string', description: '列中文名' })
  cdDescName?: string;
  /**
   * 字典名
   *
   * @type { string }
   * @memberof SysColDetailUDto
   */
  @ApiPropertyOptional({ name: 'cdDictName', type: 'string', description: '字典名' })
  cdDictName?: string;
  /**
   * 模式
   *
   * @type { string }
   * @memberof SysColDetailUDto
   */
  @ApiPropertyOptional({ name: 'cdMode', type: 'string', description: '模式' })
  cdMode?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysColDetailUDto
   */
  @ApiPropertyOptional({ name: 'cdCreateDate', type: 'Date', description: '创建日期' })
  cdCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysColDetailUDto
   */
  @ApiPropertyOptional({ name: 'cdUpdateDate', type: 'Date', description: '更新日期' })
  cdUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysColDetailColNames)[] }
   * @memberof SysColDetailUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysColDetailColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysColDetailUDto
   */
  @ApiProperty({ name: 'cdIdList', type: 'string[]', description: 'ID列表' })
  cdIdList?: string[];
}
