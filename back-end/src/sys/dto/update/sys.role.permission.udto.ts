import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysRolePermissionColNames } from '../../entity/ddl/sys.role.permission.cols';
/**
 * SYS_ROLE_PERMISSION表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysRolePermissionUDto
 * @class SysRolePermissionUDto
 */
@ApiTags('SYS_ROLE_PERMISSION表的UDTO对象')
export class SysRolePermissionUDto {
  /**
   * 角色权限表Id
   *
   * @type { string }
   * @memberof SysRolePermissionUDto
   */
  @ApiProperty({ name: 'rpId', type: 'string', description: '角色权限表Id' })
  rpId: string;
  /**
   * 角色ID号
   *
   * @type { string }
   * @memberof SysRolePermissionUDto
   */
  @ApiProperty({ name: 'rpRoleId', type: 'string', description: '角色ID号' })
  rpRoleId: string;
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysRolePermissionUDto
   */
  @ApiProperty({ name: 'rpPermissionId', type: 'string', description: '权限ID' })
  rpPermissionId: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysRolePermissionColNames)[] }
   * @memberof SysRolePermissionUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRolePermissionColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysRolePermissionUDto
   */
  @ApiProperty({ name: 'rpIdList', type: 'string[]', description: 'ID列表' })
  rpIdList?: string[];
}
