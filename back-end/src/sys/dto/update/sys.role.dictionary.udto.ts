import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysRoleDictionaryColNames } from '../../entity/ddl/sys.role.dictionary.cols';
/**
 * SYS_ROLE_DICTIONARY表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysRoleDictionaryUDto
 * @class SysRoleDictionaryUDto
 */
@ApiTags('SYS_ROLE_DICTIONARY表的UDTO对象')
export class SysRoleDictionaryUDto {
  /**
   * 角色字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryUDto
   */
  @ApiProperty({ name: 'rdId', type: 'string', description: '角色字典ID' })
  rdId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryUDto
   */
  @ApiProperty({ name: 'rdRoleId', type: 'string', description: '角色ID' })
  rdRoleId: string;
  /**
   * 字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionaryUDto
   */
  @ApiProperty({ name: 'rdDictionaryId', type: 'string', description: '字典ID' })
  rdDictionaryId: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysRoleDictionaryColNames)[] }
   * @memberof SysRoleDictionaryUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRoleDictionaryColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysRoleDictionaryUDto
   */
  @ApiProperty({ name: 'rdIdList', type: 'string[]', description: 'ID列表' })
  rdIdList?: string[];
}
