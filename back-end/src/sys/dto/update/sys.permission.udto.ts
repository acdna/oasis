import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysPermissionColNames } from '../../entity/ddl/sys.permission.cols';
/**
 * SYS_PERMISSION表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysPermissionUDto
 * @class SysPermissionUDto
 */
@ApiTags('SYS_PERMISSION表的UDTO对象')
export class SysPermissionUDto {
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'perId', type: 'string', description: '权限ID' })
  perId: string;
  /**
   * 权限名称
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'perName', type: 'string', description: '权限名称' })
  perName: string;
  /**
   * 权限英文名
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perNameEn', type: 'string', description: '权限英文名' })
  perNameEn?: string;
  /**
   * 权限类型
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'perType', type: 'string', description: '权限类型' })
  perType: string;
  /**
   * 权限URL
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perUrl', type: 'string', description: '权限URL' })
  perUrl?: string;
  /**
   * 权限访问方法
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perMethod', type: 'string', description: '权限访问方法' })
  perMethod?: string;
  /**
   * 父节点ID
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perParentId', type: 'string', description: '父节点ID' })
  perParentId?: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'perOrder', type: 'number', description: '排序' })
  perOrder: number;
  /**
   * 备注信息
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perRemark', type: 'string', description: '备注信息' })
  perRemark?: string;
  /**
   * 状态信息
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'perState', type: 'string', description: '状态信息' })
  perState: string;
  /**
   * 所属系统：前端
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perSystem', type: 'string', description: '所属系统：前端' })
  perSystem?: string;
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysPermissionUDto
   */
  @ApiPropertyOptional({ name: 'perModule', type: 'string', description: '模块' })
  perModule?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysPermissionColNames)[] }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysPermissionColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysPermissionUDto
   */
  @ApiProperty({ name: 'perIdList', type: 'string[]', description: 'ID列表' })
  perIdList?: string[];
}
