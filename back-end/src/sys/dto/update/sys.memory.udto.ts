import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysMemoryColNames } from '../../entity/ddl/sys.memory.cols';
/**
 * 系统JVM内存表表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysMemoryUDto
 * @class SysMemoryUDto
 */
@ApiTags('SYS_MEMORY表的UDTO对象')
export class SysMemoryUDto {
  /**
   * memId
   *
   * @type { string }
   * @memberof SysMemoryUDto
   */
  @ApiProperty({ name: 'memId', type: 'string', description: 'memId' })
  memId: string;
  /**
   * 总内存
   *
   * @type { number }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memTotal', type: 'number', description: '总内存' })
  memTotal?: number;
  /**
   * 可用内存
   *
   * @type { number }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memFree', type: 'number', description: '可用内存' })
  memFree?: number;
  /**
   * 已使用内存
   *
   * @type { number }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memUsed', type: 'number', description: '已使用内存' })
  memUsed?: number;
  /**
   * 内存用量是否预警
   *
   * @type { string }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memIsWarning', type: 'string', description: '内存用量是否预警' })
  memIsWarning?: string;
  /**
   * 最大内存
   *
   * @type { number }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memMax', type: 'number', description: '最大内存' })
  memMax?: number;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memCreateDate', type: 'Date', description: '创建日期' })
  memCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysMemoryUDto
   */
  @ApiPropertyOptional({ name: 'memUpdateDate', type: 'Date', description: '更新日期' })
  memUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysMemoryColNames)[] }
   * @memberof SysMemoryUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysMemoryColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysMemoryUDto
   */
  @ApiProperty({ name: 'memIdList', type: 'string[]', description: 'ID列表' })
  memIdList?: string[];
}
