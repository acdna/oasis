import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysUserColNames } from '../../entity/ddl/sys.user.cols';

/**
 * SYS_USER表的UDTO对象
 * @date 12/30/2020, 3:09:40 PM
 * @author jiangbin
 * @export SysUserUDto
 * @class SysUserUDto
 */
@ApiTags('SYS_USER表的UDTO对象')
export class SysUserUDto {
  /**
   * 用户ID
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userId', type: 'string', description: '用户ID' })
  userId: string;
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userLoginName', type: 'string', description: '用户帐号' })
  userLoginName: string;
  /**
   * 用户密码
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userPassword', type: 'string', description: '用户密码' })
  userPassword: string;
  /**
   * userPasswordPrivate
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userPasswordPrivate', type: 'string', description: 'userPasswordPrivate' })
  userPasswordPrivate?: string;
  /**
   * 姓名缩写
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userNameAbbr', type: 'string', description: '姓名缩写' })
  userNameAbbr?: string;
  /**
   * 用户名称
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userName', type: 'string', description: '用户名称' })
  userName?: string;
  /**
   * 英文名
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userNameEn', type: 'string', description: '英文名' })
  userNameEn?: string;
  /**
   * 性别
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userSex', type: 'string', description: '性别' })
  userSex?: string;
  /**
   * 用户手机号
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userPhone', type: 'string', description: '用户手机号' })
  userPhone?: string;
  /**
   * 用户的邮箱
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userEmail', type: 'string', description: '用户的邮箱' })
  userEmail?: string;
  /**
   * 用户头像路径
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userImageUrl', type: 'string', description: '用户头像路径' })
  userImageUrl?: string;
  /**
   * 用户排序
   *
   * @type { number }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userOrder', type: 'number', description: '用户排序' })
  userOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userState', type: 'string', description: '状态' })
  userState: string;
  /**
   * 用户代码
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userCode', type: 'string', description: '用户代码' })
  userCode: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userCreateDate', type: 'Date', description: '创建日期' })
  userCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userUpdateDate', type: 'Date', description: '更新日期' })
  userUpdateDate?: Date;
  /**
   * 用户最后登录时间
   *
   * @type { Date }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userLastLoginTime', type: 'Date', description: '用户最后登录时间' })
  userLastLoginTime?: Date;
  /**
   * 用户最后登录Ip
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userLastLoginIp', type: 'string', description: '用户最后登录Ip' })
  userLastLoginIp?: string;
  /**
   * 用户所属单位
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userUnit', type: 'string', description: '用户所属单位' })
  userUnit?: string;
  /**
   * 用户职务/职称
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userJobTitle', type: 'string', description: '用户职务/职称' })
  userJobTitle?: string;
  /**
   * 用户通讯地址
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userAddr', type: 'string', description: '用户通讯地址' })
  userAddr?: string;
  /**
   * 邮编
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userPostcode', type: 'string', description: '邮编' })
  userPostcode?: string;
  /**
   * 用户绑定种属
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userSamSpecies', type: 'string', description: '用户绑定种属' })
  userSamSpecies?: string;
  /**
   * 当前种属
   *
   * @type { string }
   * @memberof SysUserUDto
   */
  @ApiPropertyOptional({ name: 'userCurrSpecies', type: 'string', description: '当前种属' })
  userCurrSpecies?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserColNames)[] }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysUserUDto
   */
  @ApiProperty({ name: 'userIdList', type: 'string[]', description: 'ID列表' })
  userIdList?: string[];
}
