import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysRoleMenuColNames } from '../../entity/ddl/sys.role.menu.cols';
/**
 * SYS_ROLE_MENU表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysRoleMenuUDto
 * @class SysRoleMenuUDto
 */
@ApiTags('SYS_ROLE_MENU表的UDTO对象')
export class SysRoleMenuUDto {
  /**
   * 角色菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenuUDto
   */
  @ApiProperty({ name: 'rmId', type: 'string', description: '角色菜单ID' })
  rmId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleMenuUDto
   */
  @ApiProperty({ name: 'rmRoleId', type: 'string', description: '角色ID' })
  rmRoleId: string;
  /**
   * 菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenuUDto
   */
  @ApiProperty({ name: 'rmMenuId', type: 'string', description: '菜单ID' })
  rmMenuId: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysRoleMenuColNames)[] }
   * @memberof SysRoleMenuUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRoleMenuColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysRoleMenuUDto
   */
  @ApiProperty({ name: 'rmIdList', type: 'string[]', description: 'ID列表' })
  rmIdList?: string[];
}
