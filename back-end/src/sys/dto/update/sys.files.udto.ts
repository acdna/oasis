import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysFilesColNames } from '../../entity/ddl/sys.files.cols';
/**
 * 文件管理表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysFilesUDto
 * @class SysFilesUDto
 */
@ApiTags('SYS_FILES表的UDTO对象')
export class SysFilesUDto {
  /**
   * 记录ID
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileId', type: 'string', description: '记录ID' })
  fileId: string;
  /**
   * 文件条码号
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileBarcode', type: 'string', description: '文件条码号' })
  fileBarcode?: string;
  /**
   * 关联的其它记录信息ID号
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileRelateId', type: 'string', description: '关联的其它记录信息ID号' })
  fileRelateId?: string;
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileName', type: 'string', description: '文件名称' })
  fileName: string;
  /**
   * 文件名
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileNameEn', type: 'string', description: '文件名' })
  fileNameEn?: string;
  /**
   * 文件存储路径
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'filePath', type: 'string', description: '文件存储路径' })
  filePath: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileCreateDate', type: 'Date', description: '创建日期' })
  fileCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileUpdateDate', type: 'Date', description: '更新日期' })
  fileUpdateDate: Date;
  /**
   * 是否被启用
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileIsUsed', type: 'string', description: '是否被启用' })
  fileIsUsed: string;
  /**
   * 所属用户
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileOwner', type: 'string', description: '所属用户' })
  fileOwner: string;
  /**
   * 文件大小
   *
   * @type { number }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileSize', type: 'number', description: '文件大小' })
  fileSize: number;
  /**
   * 用户权限表
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileGrants', type: 'string', description: '用户权限表' })
  fileGrants?: string;
  /**
   * 文件分类
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileClasses', type: 'string', description: '文件分类' })
  fileClasses?: string;
  /**
   * 文件状态信息
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileState', type: 'string', description: '文件状态信息' })
  fileState?: string;
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileType', type: 'string', description: '文件类型' })
  fileType?: string;
  /**
   * 文件备注信息
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileComments', type: 'string', description: '文件备注信息' })
  fileComments?: string;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysFilesUDto
   */
  @ApiPropertyOptional({ name: 'fileSpecies', type: 'string', description: '种属' })
  fileSpecies?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysFilesColNames)[] }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysFilesColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysFilesUDto
   */
  @ApiProperty({ name: 'fileIdList', type: 'string[]', description: 'ID列表' })
  fileIdList?: string[];
}
