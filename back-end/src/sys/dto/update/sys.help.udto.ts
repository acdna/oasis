import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysHelpColNames } from '../../entity/ddl/sys.help.cols';
/**
 * SYS_HELP表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysHelpUDto
 * @class SysHelpUDto
 */
@ApiTags('SYS_HELP表的UDTO对象')
export class SysHelpUDto {
  /**
   * 帮助编号
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiProperty({ name: 'helpId', type: 'string', description: '帮助编号' })
  helpId: string;
  /**
   * 帮助问题
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpQuestion', type: 'string', description: '帮助问题' })
  helpQuestion?: string;
  /**
   * 帮助回答
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpAnswer', type: 'string', description: '帮助回答' })
  helpAnswer?: string;
  /**
   * 问题编号父级编号
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpParentId', type: 'string', description: '问题编号父级编号' })
  helpParentId?: string;
  /**
   * 帮助类别
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpType', type: 'string', description: '帮助类别' })
  helpType?: string;
  /**
   * 帮助匹配路径
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpPath', type: 'string', description: '帮助匹配路径' })
  helpPath?: string;
  /**
   * 排序
   *
   * @type { string }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpSort', type: 'string', description: '排序' })
  helpSort?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpCreateDate', type: 'Date', description: '创建日期' })
  helpCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysHelpUDto
   */
  @ApiPropertyOptional({ name: 'helpUpdateDate', type: 'Date', description: '更新日期' })
  helpUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysHelpColNames)[] }
   * @memberof SysHelpUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysHelpColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysHelpUDto
   */
  @ApiProperty({ name: 'helpIdList', type: 'string[]', description: 'ID列表' })
  helpIdList?: string[];
}
