import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysMenu } from '../entity/sys.menu.entity';
import { SysMenuUDto } from '../dto/update/sys.menu.udto';
import { SysMenuCDto } from '../dto/create/sys.menu.cdto';
import { SysMenuQDto } from '../dto/query/sys.menu.qdto';
import { SysMenuService } from '../service/sys.menu.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_MENU_PMS } from '../permission/sys.menu.pms';

/**
 * SYS_MENU表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysMenuController
 */
@Controller('sys/menu')
@ApiTags('访问SYS_MENU表的控制器类')
export class SysMenuController {
  constructor(@Inject(SysMenuService) private readonly sysMenuService: SysMenuService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_MENU_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysMenu[]> {
    return this.sysMenuService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_MENU_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysMenuQDto })
  async findList(@Query() dto: SysMenuQDto): Promise<SysMenu[]> {
    return this.sysMenuService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_MENU_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysMenu> {
    return this.sysMenuService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_MENU_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysMenuQDto })
  async findOne(@Query() dto: SysMenuQDto): Promise<SysMenu> {
    return this.sysMenuService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_MENU_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysMenu[]> {
    return this.sysMenuService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_MENU_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysMenuQDto })
  async findPage(@Query() query: SysMenuQDto): Promise<any> {
    return this.sysMenuService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_MENU_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysMenuQDto })
  async count(@Query() query: SysMenuQDto): Promise<number> {
    return this.sysMenuService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_MENU_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysMenuService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_MENU_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysMenuService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_MENU_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysMenuQDto })
  async delete(@Query() query: SysMenuQDto): Promise<any> {
    return this.sysMenuService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_MENU_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysMenuUDto })
  async update(@Body() dto: SysMenuUDto): Promise<any> {
    return this.sysMenuService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_MENU_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysMenuUDto })
  async updateByIds(@Body() dto: SysMenuUDto): Promise<any> {
    return this.sysMenuService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_MENU_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysMenuUDto] })
  async updateList(@Body() dtos: SysMenuUDto[]): Promise<any> {
    return this.sysMenuService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_MENU_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysMenuUDto })
  async updateSelective(@Body() dto: SysMenuUDto): Promise<any> {
    return this.sysMenuService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_MENU_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysMenuUDto] })
  async updateListSelective(@Body() dtos: SysMenuUDto[]): Promise<any> {
    return this.sysMenuService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_MENU_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysMenuCDto })
  async create(@Body() dto: SysMenuCDto): Promise<SysMenuCDto> {
    return this.sysMenuService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_MENU_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysMenuCDto] })
  async createList(@Body() dtos: SysMenuCDto[]): Promise<SysMenuCDto[]> {
    return this.sysMenuService.createList(dtos);
  }

  /*********以下为新增方法*********/
  @Permission(SYS_MENU_PMS.FIND_PAGE_DETAIL)
  @Get('find/page/detail')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysMenuQDto })
  async findPageDetail(@Query() dto: SysMenuQDto): Promise<any> {
    return this.sysMenuService.findPageDetail(dto);
  }
}
