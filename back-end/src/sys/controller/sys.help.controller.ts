import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysHelp } from '../entity/sys.help.entity';
import { SysHelpUDto } from '../dto/update/sys.help.udto';
import { SysHelpCDto } from '../dto/create/sys.help.cdto';
import { SysHelpQDto } from '../dto/query/sys.help.qdto';
import { SysHelpService } from '../service/sys.help.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_HELP_PMS } from '../permission/sys.help.pms';

/**
 * SYS_HELP表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysHelpController
 */
@Controller('sys/help')
@ApiTags('访问SYS_HELP表的控制器类')
export class SysHelpController {
  constructor(@Inject(SysHelpService) private readonly sysHelpService: SysHelpService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_HELP_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysHelp[]> {
    return this.sysHelpService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_HELP_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysHelpQDto })
  async findList(@Query() dto: SysHelpQDto): Promise<SysHelp[]> {
    return this.sysHelpService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_HELP_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysHelp> {
    return this.sysHelpService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_HELP_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysHelpQDto })
  async findOne(@Query() dto: SysHelpQDto): Promise<SysHelp> {
    return this.sysHelpService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_HELP_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysHelp[]> {
    return this.sysHelpService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_HELP_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysHelpQDto })
  async findPage(@Query() query: SysHelpQDto): Promise<any> {
    return this.sysHelpService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_HELP_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysHelpQDto })
  async count(@Query() query: SysHelpQDto): Promise<number> {
    return this.sysHelpService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_HELP_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysHelpService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_HELP_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysHelpService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_HELP_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysHelpQDto })
  async delete(@Query() query: SysHelpQDto): Promise<any> {
    return this.sysHelpService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_HELP_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysHelpUDto })
  async update(@Body() dto: SysHelpUDto): Promise<any> {
    return this.sysHelpService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_HELP_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysHelpUDto })
  async updateByIds(@Body() dto: SysHelpUDto): Promise<any> {
    return this.sysHelpService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_HELP_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysHelpUDto] })
  async updateList(@Body() dtos: SysHelpUDto[]): Promise<any> {
    return this.sysHelpService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_HELP_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysHelpUDto })
  async updateSelective(@Body() dto: SysHelpUDto): Promise<any> {
    return this.sysHelpService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_HELP_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysHelpUDto] })
  async updateListSelective(@Body() dtos: SysHelpUDto[]): Promise<any> {
    return this.sysHelpService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_HELP_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysHelpCDto })
  async create(@Body() dto: SysHelpCDto): Promise<SysHelpCDto> {
    return this.sysHelpService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_HELP_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysHelpCDto] })
  async createList(@Body() dtos: SysHelpCDto[]): Promise<SysHelpCDto[]> {
    return this.sysHelpService.createList(dtos);
  }
}
