import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysRoleDictionary } from '../entity/sys.role.dictionary.entity';
import { SysRoleDictionaryUDto } from '../dto/update/sys.role.dictionary.udto';
import { SysRoleDictionaryCDto } from '../dto/create/sys.role.dictionary.cdto';
import { SysRoleDictionaryQDto } from '../dto/query/sys.role.dictionary.qdto';
import { SysRoleDictionaryService } from '../service/sys.role.dictionary.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_ROLE_DICTIONARY_PMS } from '../permission/sys.role.dictionary.pms';

/**
 * SYS_ROLE_DICTIONARY表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysRoleDictionaryController
 */
@Controller('sys/role/dictionary')
@ApiTags('访问SYS_ROLE_DICTIONARY表的控制器类')
export class SysRoleDictionaryController {
  constructor(@Inject(SysRoleDictionaryService) private readonly sysRoleDictionaryService: SysRoleDictionaryService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysRoleDictionary[]> {
    return this.sysRoleDictionaryService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysRoleDictionaryQDto })
  async findList(@Query() dto: SysRoleDictionaryQDto): Promise<SysRoleDictionary[]> {
    return this.sysRoleDictionaryService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysRoleDictionary> {
    return this.sysRoleDictionaryService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysRoleDictionaryQDto })
  async findOne(@Query() dto: SysRoleDictionaryQDto): Promise<SysRoleDictionary> {
    return this.sysRoleDictionaryService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysRoleDictionary[]> {
    return this.sysRoleDictionaryService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRoleDictionaryQDto })
  async findPage(@Query() query: SysRoleDictionaryQDto): Promise<any> {
    return this.sysRoleDictionaryService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRoleDictionaryQDto })
  async count(@Query() query: SysRoleDictionaryQDto): Promise<number> {
    return this.sysRoleDictionaryService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysRoleDictionaryService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysRoleDictionaryService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysRoleDictionaryQDto })
  async delete(@Query() query: SysRoleDictionaryQDto): Promise<any> {
    return this.sysRoleDictionaryService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRoleDictionaryUDto })
  async update(@Body() dto: SysRoleDictionaryUDto): Promise<any> {
    return this.sysRoleDictionaryService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRoleDictionaryUDto })
  async updateByIds(@Body() dto: SysRoleDictionaryUDto): Promise<any> {
    return this.sysRoleDictionaryService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysRoleDictionaryUDto] })
  async updateList(@Body() dtos: SysRoleDictionaryUDto[]): Promise<any> {
    return this.sysRoleDictionaryService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysRoleDictionaryUDto })
  async updateSelective(@Body() dto: SysRoleDictionaryUDto): Promise<any> {
    return this.sysRoleDictionaryService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysRoleDictionaryUDto] })
  async updateListSelective(@Body() dtos: SysRoleDictionaryUDto[]): Promise<any> {
    return this.sysRoleDictionaryService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysRoleDictionaryCDto })
  async create(@Body() dto: SysRoleDictionaryCDto): Promise<SysRoleDictionaryCDto> {
    return this.sysRoleDictionaryService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_DICTIONARY_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysRoleDictionaryCDto] })
  async createList(@Body() dtos: SysRoleDictionaryCDto[]): Promise<SysRoleDictionaryCDto[]> {
    return this.sysRoleDictionaryService.createList(dtos);
  }
}
