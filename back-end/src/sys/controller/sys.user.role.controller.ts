import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysUserRole } from '../entity/sys.user.role.entity';
import { SysUserRoleUDto } from '../dto/update/sys.user.role.udto';
import { SysUserRoleCDto } from '../dto/create/sys.user.role.cdto';
import { SysUserRoleQDto } from '../dto/query/sys.user.role.qdto';
import { SysUserRoleService } from '../service/sys.user.role.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_USER_ROLE_PMS } from '../permission/sys.user.role.pms';

/**
 * SYS_USER_ROLE表对应控制器类
 * @date 12/30/2020, 2:44:22 PM
 * @author jiangbin
 * @export
 * @class SysUserRoleController
 */
@Controller('sys/user/role')
@ApiTags('访问SYS_USER_ROLE表的控制器类')
export class SysUserRoleController {
  constructor(@Inject(SysUserRoleService) private readonly sysUserRoleService: SysUserRoleService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_USER_ROLE_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysUserRole[]> {
    return this.sysUserRoleService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_USER_ROLE_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysUserRoleQDto })
  async findList(@Query() dto: SysUserRoleQDto): Promise<SysUserRole[]> {
    return this.sysUserRoleService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_USER_ROLE_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysUserRole> {
    return this.sysUserRoleService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_USER_ROLE_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysUserRoleQDto })
  async findOne(@Query() dto: SysUserRoleQDto): Promise<SysUserRole> {
    return this.sysUserRoleService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_USER_ROLE_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysUserRole[]> {
    return this.sysUserRoleService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_USER_ROLE_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserRoleQDto })
  async findPage(@Query() query: SysUserRoleQDto): Promise<any> {
    return this.sysUserRoleService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_USER_ROLE_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserRoleQDto })
  async count(@Query() query: SysUserRoleQDto): Promise<number> {
    return this.sysUserRoleService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_USER_ROLE_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysUserRoleService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_USER_ROLE_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysUserRoleService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_USER_ROLE_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysUserRoleQDto })
  async delete(@Query() query: SysUserRoleQDto): Promise<any> {
    return this.sysUserRoleService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_ROLE_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserRoleUDto })
  async update(@Body() dto: SysUserRoleUDto): Promise<any> {
    return this.sysUserRoleService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_ROLE_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserRoleUDto })
  async updateByIds(@Body() dto: SysUserRoleUDto): Promise<any> {
    return this.sysUserRoleService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_USER_ROLE_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysUserRoleUDto] })
  async updateList(@Body() dtos: SysUserRoleUDto[]): Promise<any> {
    return this.sysUserRoleService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_USER_ROLE_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysUserRoleUDto })
  async updateSelective(@Body() dto: SysUserRoleUDto): Promise<any> {
    return this.sysUserRoleService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_USER_ROLE_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserRoleUDto] })
  async updateListSelective(@Body() dtos: SysUserRoleUDto[]): Promise<any> {
    return this.sysUserRoleService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_USER_ROLE_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysUserRoleCDto })
  async create(@Body() dto: SysUserRoleCDto): Promise<SysUserRoleCDto> {
    return this.sysUserRoleService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_USER_ROLE_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserRoleCDto] })
  async createList(@Body() dtos: SysUserRoleCDto[]): Promise<SysUserRoleCDto[]> {
    return this.sysUserRoleService.createList(dtos);
  }
}
