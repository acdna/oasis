import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysNotice } from '../entity/sys.notice.entity';
import { SysNoticeUDto } from '../dto/update/sys.notice.udto';
import { SysNoticeCDto } from '../dto/create/sys.notice.cdto';
import { SysNoticeQDto } from '../dto/query/sys.notice.qdto';
import { SysNoticeService } from '../service/sys.notice.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_NOTICE_PMS } from '../permission/sys.notice.pms';

/**
 * SYS_NOTICE表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysNoticeController
 */
@Controller('sys/notice')
@ApiTags('访问SYS_NOTICE表的控制器类')
export class SysNoticeController {
  constructor(@Inject(SysNoticeService) private readonly sysNoticeService: SysNoticeService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_NOTICE_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysNotice[]> {
    return this.sysNoticeService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_NOTICE_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysNoticeQDto })
  async findList(@Query() dto: SysNoticeQDto): Promise<SysNotice[]> {
    return this.sysNoticeService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_NOTICE_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysNotice> {
    return this.sysNoticeService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_NOTICE_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysNoticeQDto })
  async findOne(@Query() dto: SysNoticeQDto): Promise<SysNotice> {
    return this.sysNoticeService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_NOTICE_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysNotice[]> {
    return this.sysNoticeService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_NOTICE_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysNoticeQDto })
  async findPage(@Query() query: SysNoticeQDto): Promise<any> {
    return this.sysNoticeService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_NOTICE_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysNoticeQDto })
  async count(@Query() query: SysNoticeQDto): Promise<number> {
    return this.sysNoticeService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_NOTICE_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysNoticeService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_NOTICE_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysNoticeService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_NOTICE_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysNoticeQDto })
  async delete(@Query() query: SysNoticeQDto): Promise<any> {
    return this.sysNoticeService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_NOTICE_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysNoticeUDto })
  async update(@Body() dto: SysNoticeUDto): Promise<any> {
    return this.sysNoticeService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_NOTICE_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysNoticeUDto })
  async updateByIds(@Body() dto: SysNoticeUDto): Promise<any> {
    return this.sysNoticeService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_NOTICE_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysNoticeUDto] })
  async updateList(@Body() dtos: SysNoticeUDto[]): Promise<any> {
    return this.sysNoticeService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_NOTICE_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysNoticeUDto })
  async updateSelective(@Body() dto: SysNoticeUDto): Promise<any> {
    return this.sysNoticeService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_NOTICE_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysNoticeUDto] })
  async updateListSelective(@Body() dtos: SysNoticeUDto[]): Promise<any> {
    return this.sysNoticeService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_NOTICE_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysNoticeCDto })
  async create(@Body() dto: SysNoticeCDto): Promise<SysNoticeCDto> {
    return this.sysNoticeService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_NOTICE_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysNoticeCDto] })
  async createList(@Body() dtos: SysNoticeCDto[]): Promise<SysNoticeCDto[]> {
    return this.sysNoticeService.createList(dtos);
  }
}
