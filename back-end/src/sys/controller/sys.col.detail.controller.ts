import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysColDetail } from '../entity/sys.col.detail.entity';
import { SysColDetailUDto } from '../dto/update/sys.col.detail.udto';
import { SysColDetailCDto } from '../dto/create/sys.col.detail.cdto';
import { SysColDetailQDto } from '../dto/query/sys.col.detail.qdto';
import { SysColDetailService } from '../service/sys.col.detail.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_COL_DETAIL_PMS } from '../permission/sys.col.detail.pms';

/**
 * SYS_COL_DETAIL表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysColDetailController
 */
@Controller('sys/col/detail')
@ApiTags('访问SYS_COL_DETAIL表的控制器类')
export class SysColDetailController {
  constructor(@Inject(SysColDetailService) private readonly sysColDetailService: SysColDetailService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_COL_DETAIL_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysColDetail[]> {
    return this.sysColDetailService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_COL_DETAIL_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysColDetailQDto })
  async findList(@Query() dto: SysColDetailQDto): Promise<SysColDetail[]> {
    return this.sysColDetailService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_COL_DETAIL_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysColDetail> {
    return this.sysColDetailService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_COL_DETAIL_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysColDetailQDto })
  async findOne(@Query() dto: SysColDetailQDto): Promise<SysColDetail> {
    return this.sysColDetailService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_COL_DETAIL_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysColDetail[]> {
    return this.sysColDetailService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_COL_DETAIL_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysColDetailQDto })
  async findPage(@Query() query: SysColDetailQDto): Promise<any> {
    return this.sysColDetailService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_COL_DETAIL_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysColDetailQDto })
  async count(@Query() query: SysColDetailQDto): Promise<number> {
    return this.sysColDetailService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_COL_DETAIL_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysColDetailService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_COL_DETAIL_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysColDetailService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_COL_DETAIL_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysColDetailQDto })
  async delete(@Query() query: SysColDetailQDto): Promise<any> {
    return this.sysColDetailService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_COL_DETAIL_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysColDetailUDto })
  async update(@Body() dto: SysColDetailUDto): Promise<any> {
    return this.sysColDetailService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_COL_DETAIL_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysColDetailUDto })
  async updateByIds(@Body() dto: SysColDetailUDto): Promise<any> {
    return this.sysColDetailService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_COL_DETAIL_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysColDetailUDto] })
  async updateList(@Body() dtos: SysColDetailUDto[]): Promise<any> {
    return this.sysColDetailService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_COL_DETAIL_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysColDetailUDto })
  async updateSelective(@Body() dto: SysColDetailUDto): Promise<any> {
    return this.sysColDetailService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_COL_DETAIL_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysColDetailUDto] })
  async updateListSelective(@Body() dtos: SysColDetailUDto[]): Promise<any> {
    return this.sysColDetailService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_COL_DETAIL_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysColDetailCDto })
  async create(@Body() dto: SysColDetailCDto): Promise<SysColDetailCDto> {
    return this.sysColDetailService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_COL_DETAIL_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysColDetailCDto] })
  async createList(@Body() dtos: SysColDetailCDto[]): Promise<SysColDetailCDto[]> {
    return this.sysColDetailService.createList(dtos);
  }
}
