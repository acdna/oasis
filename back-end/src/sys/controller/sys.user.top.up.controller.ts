import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysUserTopUp } from '../entity/sys.user.top.up.entity';
import { SysUserTopUpUDto } from '../dto/update/sys.user.top.up.udto';
import { SysUserTopUpCDto } from '../dto/create/sys.user.top.up.cdto';
import { SysUserTopUpQDto } from '../dto/query/sys.user.top.up.qdto';
import { SysUserTopUpService } from '../service/sys.user.top.up.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_USER_TOP_UP_PMS } from '../permission/sys.user.top.up.pms';

/**
 * SYS_USER_TOP_UP表对应控制器类
 * @date 12/30/2020, 2:44:22 PM
 * @author jiangbin
 * @export
 * @class SysUserTopUpController
 */
@Controller('sys/user/top/up')
@ApiTags('访问SYS_USER_TOP_UP表的控制器类')
export class SysUserTopUpController {
  constructor(@Inject(SysUserTopUpService) private readonly sysUserTopUpService: SysUserTopUpService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_USER_TOP_UP_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysUserTopUp[]> {
    return this.sysUserTopUpService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_USER_TOP_UP_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysUserTopUpQDto })
  async findList(@Query() dto: SysUserTopUpQDto): Promise<SysUserTopUp[]> {
    return this.sysUserTopUpService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_USER_TOP_UP_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysUserTopUp> {
    return this.sysUserTopUpService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_USER_TOP_UP_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysUserTopUpQDto })
  async findOne(@Query() dto: SysUserTopUpQDto): Promise<SysUserTopUp> {
    return this.sysUserTopUpService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_USER_TOP_UP_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysUserTopUp[]> {
    return this.sysUserTopUpService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_USER_TOP_UP_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserTopUpQDto })
  async findPage(@Query() query: SysUserTopUpQDto): Promise<any> {
    return this.sysUserTopUpService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_USER_TOP_UP_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserTopUpQDto })
  async count(@Query() query: SysUserTopUpQDto): Promise<number> {
    return this.sysUserTopUpService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_USER_TOP_UP_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysUserTopUpService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_USER_TOP_UP_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysUserTopUpService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_USER_TOP_UP_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysUserTopUpQDto })
  async delete(@Query() query: SysUserTopUpQDto): Promise<any> {
    return this.sysUserTopUpService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_TOP_UP_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserTopUpUDto })
  async update(@Body() dto: SysUserTopUpUDto): Promise<any> {
    return this.sysUserTopUpService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_TOP_UP_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserTopUpUDto })
  async updateByIds(@Body() dto: SysUserTopUpUDto): Promise<any> {
    return this.sysUserTopUpService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_USER_TOP_UP_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysUserTopUpUDto] })
  async updateList(@Body() dtos: SysUserTopUpUDto[]): Promise<any> {
    return this.sysUserTopUpService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_USER_TOP_UP_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysUserTopUpUDto })
  async updateSelective(@Body() dto: SysUserTopUpUDto): Promise<any> {
    return this.sysUserTopUpService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_USER_TOP_UP_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserTopUpUDto] })
  async updateListSelective(@Body() dtos: SysUserTopUpUDto[]): Promise<any> {
    return this.sysUserTopUpService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_USER_TOP_UP_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysUserTopUpCDto })
  async create(@Body() dto: SysUserTopUpCDto): Promise<SysUserTopUpCDto> {
    return this.sysUserTopUpService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_USER_TOP_UP_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserTopUpCDto] })
  async createList(@Body() dtos: SysUserTopUpCDto[]): Promise<SysUserTopUpCDto[]> {
    return this.sysUserTopUpService.createList(dtos);
  }
}
