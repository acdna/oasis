import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysMemory } from '../entity/sys.memory.entity';
import { SysMemoryUDto } from '../dto/update/sys.memory.udto';
import { SysMemoryCDto } from '../dto/create/sys.memory.cdto';
import { SysMemoryQDto } from '../dto/query/sys.memory.qdto';
import { SysMemoryService } from '../service/sys.memory.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_MEMORY_PMS } from '../permission/sys.memory.pms';

/**
 * SYS_MEMORY表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysMemoryController
 */
@Controller('sys/memory')
@ApiTags('访问SYS_MEMORY表的控制器类')
export class SysMemoryController {
  constructor(@Inject(SysMemoryService) private readonly sysMemoryService: SysMemoryService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_MEMORY_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysMemory[]> {
    return this.sysMemoryService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_MEMORY_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysMemoryQDto })
  async findList(@Query() dto: SysMemoryQDto): Promise<SysMemory[]> {
    return this.sysMemoryService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_MEMORY_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysMemory> {
    return this.sysMemoryService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_MEMORY_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysMemoryQDto })
  async findOne(@Query() dto: SysMemoryQDto): Promise<SysMemory> {
    return this.sysMemoryService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_MEMORY_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysMemory[]> {
    return this.sysMemoryService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_MEMORY_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysMemoryQDto })
  async findPage(@Query() query: SysMemoryQDto): Promise<any> {
    return this.sysMemoryService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_MEMORY_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysMemoryQDto })
  async count(@Query() query: SysMemoryQDto): Promise<number> {
    return this.sysMemoryService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_MEMORY_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysMemoryService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_MEMORY_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysMemoryService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_MEMORY_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysMemoryQDto })
  async delete(@Query() query: SysMemoryQDto): Promise<any> {
    return this.sysMemoryService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_MEMORY_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysMemoryUDto })
  async update(@Body() dto: SysMemoryUDto): Promise<any> {
    return this.sysMemoryService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_MEMORY_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysMemoryUDto })
  async updateByIds(@Body() dto: SysMemoryUDto): Promise<any> {
    return this.sysMemoryService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_MEMORY_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysMemoryUDto] })
  async updateList(@Body() dtos: SysMemoryUDto[]): Promise<any> {
    return this.sysMemoryService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_MEMORY_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysMemoryUDto })
  async updateSelective(@Body() dto: SysMemoryUDto): Promise<any> {
    return this.sysMemoryService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_MEMORY_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysMemoryUDto] })
  async updateListSelective(@Body() dtos: SysMemoryUDto[]): Promise<any> {
    return this.sysMemoryService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_MEMORY_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysMemoryCDto })
  async create(@Body() dto: SysMemoryCDto): Promise<SysMemoryCDto> {
    return this.sysMemoryService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_MEMORY_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysMemoryCDto] })
  async createList(@Body() dtos: SysMemoryCDto[]): Promise<SysMemoryCDto[]> {
    return this.sysMemoryService.createList(dtos);
  }
}
