import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysBillingCard } from '../entity/sys.billing.card.entity';
import { SysBillingCardUDto } from '../dto/update/sys.billing.card.udto';
import { SysBillingCardCDto } from '../dto/create/sys.billing.card.cdto';
import { SysBillingCardQDto } from '../dto/query/sys.billing.card.qdto';
import { SysBillingCardService } from '../service/sys.billing.card.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_BILLING_CARD_PMS } from '../permission/sys.billing.card.pms';

/**
 * SYS_BILLING_CARD表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysBillingCardController
 */
@Controller('sys/billing/card')
@ApiTags('访问SYS_BILLING_CARD表的控制器类')
export class SysBillingCardController {
  constructor(@Inject(SysBillingCardService) private readonly sysBillingCardService: SysBillingCardService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_BILLING_CARD_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysBillingCard[]> {
    return this.sysBillingCardService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_BILLING_CARD_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysBillingCardQDto })
  async findList(@Query() dto: SysBillingCardQDto): Promise<SysBillingCard[]> {
    return this.sysBillingCardService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_BILLING_CARD_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysBillingCard> {
    return this.sysBillingCardService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_BILLING_CARD_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysBillingCardQDto })
  async findOne(@Query() dto: SysBillingCardQDto): Promise<SysBillingCard> {
    return this.sysBillingCardService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_BILLING_CARD_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysBillingCard[]> {
    return this.sysBillingCardService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_BILLING_CARD_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysBillingCardQDto })
  async findPage(@Query() query: SysBillingCardQDto): Promise<any> {
    return this.sysBillingCardService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_BILLING_CARD_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysBillingCardQDto })
  async count(@Query() query: SysBillingCardQDto): Promise<number> {
    return this.sysBillingCardService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_BILLING_CARD_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysBillingCardService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_BILLING_CARD_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysBillingCardService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_BILLING_CARD_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysBillingCardQDto })
  async delete(@Query() query: SysBillingCardQDto): Promise<any> {
    return this.sysBillingCardService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_BILLING_CARD_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysBillingCardUDto })
  async update(@Body() dto: SysBillingCardUDto): Promise<any> {
    return this.sysBillingCardService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_BILLING_CARD_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysBillingCardUDto })
  async updateByIds(@Body() dto: SysBillingCardUDto): Promise<any> {
    return this.sysBillingCardService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_BILLING_CARD_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysBillingCardUDto] })
  async updateList(@Body() dtos: SysBillingCardUDto[]): Promise<any> {
    return this.sysBillingCardService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_BILLING_CARD_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysBillingCardUDto })
  async updateSelective(@Body() dto: SysBillingCardUDto): Promise<any> {
    return this.sysBillingCardService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_BILLING_CARD_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysBillingCardUDto] })
  async updateListSelective(@Body() dtos: SysBillingCardUDto[]): Promise<any> {
    return this.sysBillingCardService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_BILLING_CARD_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysBillingCardCDto })
  async create(@Body() dto: SysBillingCardCDto): Promise<SysBillingCardCDto> {
    return this.sysBillingCardService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_BILLING_CARD_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysBillingCardCDto] })
  async createList(@Body() dtos: SysBillingCardCDto[]): Promise<SysBillingCardCDto[]> {
    return this.sysBillingCardService.createList(dtos);
  }
}
