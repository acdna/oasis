import { Body, Controller, Delete, Get, Inject, Param, Post, Put, Query } from '@nestjs/common';
import { SysConfig } from '../entity/sys.config.entity';
import { SysConfigUDto } from '../dto/update/sys.config.udto';
import { SysConfigCDto } from '../dto/create/sys.config.cdto';
import { SysConfigQDto } from '../dto/query/sys.config.qdto';
import { SysConfigService } from '../service/sys.config.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_CONFIG_PMS } from '../permission/sys.config.pms';
import { AopConfigLoader } from '../../common/aop/aop.config.loader';
import { ConfigMapper } from '../config/config.mapper';
import { SYS_SYSTEM } from '../config/sys.config.defined';
import { SkipAuth } from '../../common/auth/guard/skip.auth';
import { LOCALES } from '../../common/locales/locale';

/**
 * SYS_CONFIG表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysConfigController
 */
@Controller('sys/config')
@ApiTags('访问SYS_CONFIG表的控制器类')
export class SysConfigController {
  constructor(@Inject(SysConfigService) private readonly sysConfigService: SysConfigService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_CONFIG_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysConfig[]> {
    return this.sysConfigService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysConfigQDto })
  async findList(@Query() dto: SysConfigQDto): Promise<SysConfig[]> {
    return this.sysConfigService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_CONFIG_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysConfig> {
    return this.sysConfigService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_CONFIG_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysConfigQDto })
  async findOne(@Query() dto: SysConfigQDto): Promise<SysConfig> {
    return this.sysConfigService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_CONFIG_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysConfig[]> {
    return this.sysConfigService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_CONFIG_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysConfigQDto })
  async findPage(@Query() query: SysConfigQDto): Promise<any> {
    return this.sysConfigService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_CONFIG_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysConfigQDto })
  async count(@Query() query: SysConfigQDto): Promise<number> {
    return this.sysConfigService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_CONFIG_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  @AopConfigLoader(false)
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysConfigService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_CONFIG_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @AopConfigLoader(false)
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysConfigService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_CONFIG_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysConfigQDto })
  @AopConfigLoader(false)
  async delete(@Query() query: SysConfigQDto): Promise<any> {
    return this.sysConfigService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysConfigUDto })
  @AopConfigLoader(false)
  async update(@Body() dto: SysConfigUDto): Promise<any> {
    return this.sysConfigService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysConfigUDto })
  @AopConfigLoader(false)
  async updateByIds(@Body() dto: SysConfigUDto): Promise<any> {
    return this.sysConfigService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_CONFIG_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysConfigUDto] })
  @AopConfigLoader(false)
  async updateList(@Body() dtos: SysConfigUDto[]): Promise<any> {
    return this.sysConfigService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysConfigUDto })
  @AopConfigLoader(false)
  async updateSelective(@Body() dto: SysConfigUDto): Promise<any> {
    return this.sysConfigService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_CONFIG_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysConfigUDto] })
  @AopConfigLoader(false)
  async updateListSelective(@Body() dtos: SysConfigUDto[]): Promise<any> {
    return this.sysConfigService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysConfigCDto })
  @AopConfigLoader(false)
  async create(@Body() dto: SysConfigCDto): Promise<SysConfigCDto> {
    return this.sysConfigService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysConfigCDto] })
  @AopConfigLoader(false)
  async createList(@Body() dtos: SysConfigCDto[]): Promise<SysConfigCDto[]> {
    return this.sysConfigService.createList(dtos);
  }

  /*********以下为新增方法*********/

  @Permission(SYS_CONFIG_PMS.FIND_PAGE_DETAIL)
  @Get('find/page/detail')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysConfigQDto })
  async findPageDetail(@Query() dto: SysConfigQDto): Promise<any> {
    return this.sysConfigService.findPageDetail(dto);
  }

  /**
   * 初始化字典
   * @param dto
   */
  @Permission(SYS_CONFIG_PMS.INIT)
  @Post('init')
  @ApiOperation({ summary: '初始化参数' })
  @AopConfigLoader(false)
  async init(): Promise<boolean> {
    return this.sysConfigService.init();
  }

  /**
   * 系统默认语言
   */
  @Permission(SYS_CONFIG_PMS.DEFAULT_LOCALE)
  @Get('default/locale')
  @ApiOperation({ summary: '系统默认语言' })
  @SkipAuth()
  async defaultLocale(): Promise<string> {
    return ConfigMapper.getValue(SYS_SYSTEM.SYS_LANGUAGE);
  }

  /**
   * 系统默认版权
   */
  @Permission(SYS_CONFIG_PMS.DEFAULT_COPYRIGHT)
  @Get('default/copyright/:locale')
  @ApiOperation({ summary: '系统默认版权' })
  @SkipAuth()
  async defaultCopyrightCn(@Param('locale') locale): Promise<string> {
    console.log(locale);
    let conf = locale === LOCALES.zh_cn ? SYS_SYSTEM.SYS_COPYRIGHT_CN : SYS_SYSTEM.SYS_COPYRIGHT_EN;
    return ConfigMapper.getValue(conf);
  }

  /**
   * 更新用户的当前语言参数
   * @param locale
   */
  @Permission(SYS_CONFIG_PMS.UPDATE_LOCALE)
  @Put('update/locale')
  @ApiOperation({ summary: '更新用户的当前语言参数' })
  @ApiQuery({ name: 'locale', type: 'string' })
  @AopConfigLoader(false)
  async updateLocale(@Body('locale') locale: string): Promise<any> {
    return this.sysConfigService.updateLocale(locale);
  }

  @Permission(SYS_CONFIG_PMS.VERSION)
  @Get('version')
  @ApiOperation({ summary: '系统版本' })
  async version(): Promise<any> {
    return ConfigMapper.getValue(SYS_SYSTEM.SYS_VERSION);
  }
}
