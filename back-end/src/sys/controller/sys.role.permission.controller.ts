import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysRolePermission } from '../entity/sys.role.permission.entity';
import { SysRolePermissionUDto } from '../dto/update/sys.role.permission.udto';
import { SysRolePermissionCDto } from '../dto/create/sys.role.permission.cdto';
import { SysRolePermissionQDto } from '../dto/query/sys.role.permission.qdto';
import { SysRolePermissionService } from '../service/sys.role.permission.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_ROLE_PERMISSION_PMS } from '../permission/sys.role.permission.pms';

/**
 * SYS_ROLE_PERMISSION表对应控制器类
 * @date 12/30/2020, 2:44:22 PM
 * @author jiangbin
 * @export
 * @class SysRolePermissionController
 */
@Controller('sys/role/permission')
@ApiTags('访问SYS_ROLE_PERMISSION表的控制器类')
export class SysRolePermissionController {
  constructor(@Inject(SysRolePermissionService) private readonly sysRolePermissionService: SysRolePermissionService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysRolePermission[]> {
    return this.sysRolePermissionService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysRolePermissionQDto })
  async findList(@Query() dto: SysRolePermissionQDto): Promise<SysRolePermission[]> {
    return this.sysRolePermissionService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysRolePermission> {
    return this.sysRolePermissionService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysRolePermissionQDto })
  async findOne(@Query() dto: SysRolePermissionQDto): Promise<SysRolePermission> {
    return this.sysRolePermissionService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysRolePermission[]> {
    return this.sysRolePermissionService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRolePermissionQDto })
  async findPage(@Query() query: SysRolePermissionQDto): Promise<any> {
    return this.sysRolePermissionService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRolePermissionQDto })
  async count(@Query() query: SysRolePermissionQDto): Promise<number> {
    return this.sysRolePermissionService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysRolePermissionService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysRolePermissionService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysRolePermissionQDto })
  async delete(@Query() query: SysRolePermissionQDto): Promise<any> {
    return this.sysRolePermissionService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRolePermissionUDto })
  async update(@Body() dto: SysRolePermissionUDto): Promise<any> {
    return this.sysRolePermissionService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRolePermissionUDto })
  async updateByIds(@Body() dto: SysRolePermissionUDto): Promise<any> {
    return this.sysRolePermissionService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysRolePermissionUDto] })
  async updateList(@Body() dtos: SysRolePermissionUDto[]): Promise<any> {
    return this.sysRolePermissionService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysRolePermissionUDto })
  async updateSelective(@Body() dto: SysRolePermissionUDto): Promise<any> {
    return this.sysRolePermissionService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysRolePermissionUDto] })
  async updateListSelective(@Body() dtos: SysRolePermissionUDto[]): Promise<any> {
    return this.sysRolePermissionService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysRolePermissionCDto })
  async create(@Body() dto: SysRolePermissionCDto): Promise<SysRolePermissionCDto> {
    return this.sysRolePermissionService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_PERMISSION_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysRolePermissionCDto] })
  async createList(@Body() dtos: SysRolePermissionCDto[]): Promise<SysRolePermissionCDto[]> {
    return this.sysRolePermissionService.createList(dtos);
  }
}
