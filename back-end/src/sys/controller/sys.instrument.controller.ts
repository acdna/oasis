import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysInstrument } from '../entity/sys.instrument.entity';
import { SysInstrumentUDto } from '../dto/update/sys.instrument.udto';
import { SysInstrumentCDto } from '../dto/create/sys.instrument.cdto';
import { SysInstrumentQDto } from '../dto/query/sys.instrument.qdto';
import { SysInstrumentService } from '../service/sys.instrument.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_INSTRUMENT_PMS } from '../permission/sys.instrument.pms';

/**
 * SYS_INSTRUMENT表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysInstrumentController
 */
@Controller('sys/instrument')
@ApiTags('访问SYS_INSTRUMENT表的控制器类')
export class SysInstrumentController {
  constructor(@Inject(SysInstrumentService) private readonly sysInstrumentService: SysInstrumentService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_INSTRUMENT_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysInstrument[]> {
    return this.sysInstrumentService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_INSTRUMENT_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysInstrumentQDto })
  async findList(@Query() dto: SysInstrumentQDto): Promise<SysInstrument[]> {
    return this.sysInstrumentService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_INSTRUMENT_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysInstrument> {
    return this.sysInstrumentService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_INSTRUMENT_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysInstrumentQDto })
  async findOne(@Query() dto: SysInstrumentQDto): Promise<SysInstrument> {
    return this.sysInstrumentService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_INSTRUMENT_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysInstrument[]> {
    return this.sysInstrumentService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_INSTRUMENT_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysInstrumentQDto })
  async findPage(@Query() query: SysInstrumentQDto): Promise<any> {
    return this.sysInstrumentService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_INSTRUMENT_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysInstrumentQDto })
  async count(@Query() query: SysInstrumentQDto): Promise<number> {
    return this.sysInstrumentService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_INSTRUMENT_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysInstrumentService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_INSTRUMENT_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysInstrumentService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_INSTRUMENT_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysInstrumentQDto })
  async delete(@Query() query: SysInstrumentQDto): Promise<any> {
    return this.sysInstrumentService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_INSTRUMENT_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysInstrumentUDto })
  async update(@Body() dto: SysInstrumentUDto): Promise<any> {
    return this.sysInstrumentService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_INSTRUMENT_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysInstrumentUDto })
  async updateByIds(@Body() dto: SysInstrumentUDto): Promise<any> {
    return this.sysInstrumentService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_INSTRUMENT_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysInstrumentUDto] })
  async updateList(@Body() dtos: SysInstrumentUDto[]): Promise<any> {
    return this.sysInstrumentService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_INSTRUMENT_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysInstrumentUDto })
  async updateSelective(@Body() dto: SysInstrumentUDto): Promise<any> {
    return this.sysInstrumentService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_INSTRUMENT_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysInstrumentUDto] })
  async updateListSelective(@Body() dtos: SysInstrumentUDto[]): Promise<any> {
    return this.sysInstrumentService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_INSTRUMENT_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysInstrumentCDto })
  async create(@Body() dto: SysInstrumentCDto): Promise<SysInstrumentCDto> {
    return this.sysInstrumentService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_INSTRUMENT_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysInstrumentCDto] })
  async createList(@Body() dtos: SysInstrumentCDto[]): Promise<SysInstrumentCDto[]> {
    return this.sysInstrumentService.createList(dtos);
  }
}
