import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysCacheTrace } from '../entity/sys.cache.trace.entity';
import { SysCacheTraceUDto } from '../dto/update/sys.cache.trace.udto';
import { SysCacheTraceCDto } from '../dto/create/sys.cache.trace.cdto';
import { SysCacheTraceQDto } from '../dto/query/sys.cache.trace.qdto';
import { SysCacheTraceService } from '../service/sys.cache.trace.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_CACHE_TRACE_PMS } from '../permission/sys.cache.trace.pms';

/**
 * SYS_CACHE_TRACE表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysCacheTraceController
 */
@Controller('sys/cache/trace')
@ApiTags('访问SYS_CACHE_TRACE表的控制器类')
export class SysCacheTraceController {
  constructor(@Inject(SysCacheTraceService) private readonly sysCacheTraceService: SysCacheTraceService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_CACHE_TRACE_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysCacheTrace[]> {
    return this.sysCacheTraceService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_CACHE_TRACE_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysCacheTraceQDto })
  async findList(@Query() dto: SysCacheTraceQDto): Promise<SysCacheTrace[]> {
    return this.sysCacheTraceService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_CACHE_TRACE_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysCacheTrace> {
    return this.sysCacheTraceService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_CACHE_TRACE_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysCacheTraceQDto })
  async findOne(@Query() dto: SysCacheTraceQDto): Promise<SysCacheTrace> {
    return this.sysCacheTraceService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_CACHE_TRACE_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysCacheTrace[]> {
    return this.sysCacheTraceService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_CACHE_TRACE_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysCacheTraceQDto })
  async findPage(@Query() query: SysCacheTraceQDto): Promise<any> {
    return this.sysCacheTraceService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_CACHE_TRACE_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysCacheTraceQDto })
  async count(@Query() query: SysCacheTraceQDto): Promise<number> {
    return this.sysCacheTraceService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_CACHE_TRACE_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysCacheTraceService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_CACHE_TRACE_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysCacheTraceService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_CACHE_TRACE_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysCacheTraceQDto })
  async delete(@Query() query: SysCacheTraceQDto): Promise<any> {
    return this.sysCacheTraceService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_CACHE_TRACE_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysCacheTraceUDto })
  async update(@Body() dto: SysCacheTraceUDto): Promise<any> {
    return this.sysCacheTraceService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_CACHE_TRACE_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysCacheTraceUDto })
  async updateByIds(@Body() dto: SysCacheTraceUDto): Promise<any> {
    return this.sysCacheTraceService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_CACHE_TRACE_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysCacheTraceUDto] })
  async updateList(@Body() dtos: SysCacheTraceUDto[]): Promise<any> {
    return this.sysCacheTraceService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_CACHE_TRACE_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysCacheTraceUDto })
  async updateSelective(@Body() dto: SysCacheTraceUDto): Promise<any> {
    return this.sysCacheTraceService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_CACHE_TRACE_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysCacheTraceUDto] })
  async updateListSelective(@Body() dtos: SysCacheTraceUDto[]): Promise<any> {
    return this.sysCacheTraceService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_CACHE_TRACE_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysCacheTraceCDto })
  async create(@Body() dto: SysCacheTraceCDto): Promise<SysCacheTraceCDto> {
    return this.sysCacheTraceService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_CACHE_TRACE_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysCacheTraceCDto] })
  async createList(@Body() dtos: SysCacheTraceCDto[]): Promise<SysCacheTraceCDto[]> {
    return this.sysCacheTraceService.createList(dtos);
  }
}
