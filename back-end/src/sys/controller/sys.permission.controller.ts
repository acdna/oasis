import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysPermission } from '../entity/sys.permission.entity';
import { SysPermissionUDto } from '../dto/update/sys.permission.udto';
import { SysPermissionCDto } from '../dto/create/sys.permission.cdto';
import { SysPermissionQDto } from '../dto/query/sys.permission.qdto';
import { SysPermissionService } from '../service/sys.permission.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_PERMISSION_PMS } from '../permission/sys.permission.pms';
import { PmsParser } from '../../common/pms/pms.parser';
import { AopPmsLoader } from '../../common/aop/aop.pms.loader';

/**
 * SYS_PERMISSION表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysPermissionController
 */
@Controller('sys/permission')
@ApiTags('访问SYS_PERMISSION表的控制器类')
export class SysPermissionController {
  constructor(@Inject(SysPermissionService) private readonly sysPermissionService: SysPermissionService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_PERMISSION_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysPermission[]> {
    return this.sysPermissionService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_PERMISSION_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysPermissionQDto })
  async findList(@Query() dto: SysPermissionQDto): Promise<SysPermission[]> {
    return this.sysPermissionService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_PERMISSION_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysPermission> {
    return this.sysPermissionService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_PERMISSION_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysPermissionQDto })
  async findOne(@Query() dto: SysPermissionQDto): Promise<SysPermission> {
    return this.sysPermissionService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_PERMISSION_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysPermission[]> {
    return this.sysPermissionService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_PERMISSION_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysPermissionQDto })
  async findPage(@Query() query: SysPermissionQDto): Promise<any> {
    return this.sysPermissionService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_PERMISSION_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysPermissionQDto })
  async count(@Query() query: SysPermissionQDto): Promise<number> {
    return this.sysPermissionService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_PERMISSION_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  @AopPmsLoader(false)
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysPermissionService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_PERMISSION_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @AopPmsLoader(false)
  async deleteByIdList(@Body('ids') ids: string[]): Promise<any> {
    return this.sysPermissionService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_PERMISSION_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysPermissionQDto })
  @AopPmsLoader(false)
  async delete(@Query() query: SysPermissionQDto): Promise<any> {
    return this.sysPermissionService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_PERMISSION_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysPermissionUDto })
  @AopPmsLoader(false)
  async update(@Body() dto: SysPermissionUDto): Promise<any> {
    return this.sysPermissionService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_PERMISSION_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysPermissionUDto })
  @AopPmsLoader(false)
  async updateByIds(@Body() dto: SysPermissionUDto): Promise<any> {
    return this.sysPermissionService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_PERMISSION_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysPermissionUDto] })
  @AopPmsLoader(false)
  async updateList(@Body() dtos: SysPermissionUDto[]): Promise<any> {
    return this.sysPermissionService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_PERMISSION_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysPermissionUDto })
  @AopPmsLoader(false)
  async updateSelective(@Body() dto: SysPermissionUDto): Promise<any> {
    return this.sysPermissionService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_PERMISSION_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysPermissionUDto] })
  @AopPmsLoader(false)
  async updateListSelective(@Body() dtos: SysPermissionUDto[]): Promise<any> {
    return this.sysPermissionService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_PERMISSION_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysPermissionCDto })
  @AopPmsLoader(false)
  async create(@Body() dto: SysPermissionCDto): Promise<SysPermissionCDto> {
    return this.sysPermissionService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_PERMISSION_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysPermissionCDto] })
  @AopPmsLoader(false)
  async createList(@Body() dtos: SysPermissionCDto[]): Promise<SysPermissionCDto[]> {
    return this.sysPermissionService.createList(dtos);
  }

  /*********以下为新增方法*********/
  @Permission(SYS_PERMISSION_PMS.FIND_PAGE_DETAIL)
  @Get('find/page/detail')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysPermissionQDto })
  async findPageDetail(@Query() dto: SysPermissionQDto): Promise<any> {
    return this.sysPermissionService.findPageDetail(dto);
  }

  @Permission(SYS_PERMISSION_PMS.INIT_FRONT)
  @Post('init/front')
  @ApiOperation({ summary: '初始化前端权限' })
  @ApiQuery({ name: 'dtos', type: [SysPermissionQDto] })
  @AopPmsLoader(false)
  async initFront(@Body() dtos: SysPermissionQDto[]): Promise<any> {
    return this.sysPermissionService.init('FRONT_END', dtos);
  }

  @Permission(SYS_PERMISSION_PMS.INIT_BACK)
  @Post('init/back')
  @ApiOperation({ summary: '初始化后端权限' })
  @AopPmsLoader(false)
  async initBack(): Promise<any> {
    let dtos = PmsParser.parserAll();
    return this.sysPermissionService.init('BACK_END', dtos);
  }
}
