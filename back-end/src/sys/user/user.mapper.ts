import { SysUser } from '../entity/sys.user.entity';
import { SysServices } from '../sys.services';
import { LogUtils } from '../../common/log4js/log4js.utils';

/**
 * 用户信息映射表
 *
 * @author jiang
 * @date 2021-01-06 20:15:40
 **/
export class UserMapper {
  private static userMap: Map<String, SysUser> = new Map<String, SysUser>();
  private static users: SysUser[] = [];

  /**
   * 重新加载全部用户
   * @author: jiangbin
   * @date: 2021-01-07 08:46:52
   **/
  static reload() {
    LogUtils.info(UserMapper.name, '开始加载全部用户信息...');
    SysServices.sysUserService
      .findAll()
      .then((users: SysUser[]) => {
        UserMapper.userMap = new Map<string, SysUser>();
        UserMapper.users = [];
        if (!users || users.length == 0) return;
        users.forEach((user) => {
          UserMapper.userMap.set(user.userLoginName, user);
        });
        UserMapper.users = users;
        LogUtils.info(UserMapper.name, '成功加载全部用户数据!');
      })
      .catch((err) => {
        LogUtils.error(UserMapper.name, '加载全部用户数据失败!', err);
      });
  }

  /**
   * 获取指定用户信息记录
   * @param loginName 参数对象
   * @return SysUser
   * @author: jiangbin
   * @date: 2021-01-07 12:00:54
   **/
  public static getUser(loginName: string): SysUser {
    return UserMapper.userMap.get(loginName);
  }

  /**
   * 获取所有用户信息列表
   * @author: jiangbin
   * @date: 2021-01-18 18:56:20
   **/
  public static getAll(): SysUser[] {
    return UserMapper.users;
  }
}
