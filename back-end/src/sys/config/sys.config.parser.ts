import { SysConfig } from '../entity/sys.config.entity';
import { SYS_CONFIG_LIST } from './sys.config.defined';
import { ConfigGroup, ConfigItem } from './sys.config.info';
import { uuid } from '../../common/utils/uuid.utils';
import { UserContext } from '../context/user.context';

/**
 * 解析参数信息为参数表对象列表
 * @author: jiangbin
 * @date: 2021-01-07 18:59:24
 **/
export const SysConfigParser = {
  /**
   * 解析参数定义信息文件
   * @author: jiangbin
   * @date: 2021-01-07 18:59:42
   **/
  parser: (): SysConfig[] => {
    let configs = [];
    //添加根结点
    configs.push(SysConfigParser.createRoot());
    //解析参数分组和叶子节点列表
    for (let i = 0; i < SYS_CONFIG_LIST.length; i++) {
      //分组包含的参数列表
      const groupItems = SYS_CONFIG_LIST[i];
      //参数名列表
      const keys = Object.keys(groupItems);
      let group = null;
      //分组中包含的叶子节点列表
      let leafs = [];
      //循环处理分组包含的每一个参数
      for (let j = 0; j < keys.length; j++) {
        const key = keys[j];
        //单个参数
        const item = groupItems[key];
        group = item.group;
        //创建记录
        const leaf = SysConfigParser.createLeaf(item);
        leaf.conOrder = j + 1;
        leafs.push(leaf);
      }
      //创建分组记录
      const node = SysConfigParser.createGroup(group);
      node.conOrder = i + 1;
      //填充叶子节点的上级ID
      leafs = leafs.map((leaf) => {
        leaf.conParentId = node.conId;
        return leaf;
      });
      configs.push(node);
      configs = configs.concat(leafs);
    }
    return configs;
  },
  /**
   * 创建根节点
   * @author: jiangbin
   * @date: 2021-01-07 22:00:57
   **/
  createRoot: (): SysConfig => {
    const record = new SysConfig();
    record.conId = '1';
    record.conName = 'ROOT';
    record.conNameEn = 'ROOT';
    record.conValue = null;
    record.conGroup = null;
    record.conParamType = null;
    record.conType = 'DIR';
    record.conManager = null;
    record.conSpecies = null;
    record.conState = 'ON';
    record.conRemark = null;
    return record;
  },
  /**
   * 创建叶子节点记录列表
   * @param item 参数定义信息
   * @author: jiangbin
   * @date: 2021-01-07 19:29:38
   **/
  createLeaf: (item: ConfigItem): SysConfig => {
    const record = new SysConfig();
    record.conId = uuid();
    record.conName = item.name;
    record.conNameEn = item.name;
    record.conValue = item.value;
    record.conGroup = item.group.group;
    record.conParamType = item.group.type;
    record.conType = 'DATA';
    record.conManager = UserContext.loginName;
    record.conSpecies = '';
    record.conState = 'ON';
    record.conRemark = item.remark;
    return record;
  },
  /**
   * 创建分组节点记录
   * @param group 分组定义信息
   * @author: jiangbin
   * @date: 2021-01-07 19:30:14
   **/
  createGroup: (group: ConfigGroup): SysConfig => {
    const record = new SysConfig();
    record.conId = uuid();
    record.conName = group.nameCn;
    record.conNameEn = group.nameEn;
    record.conGroup = group.group;
    record.conParamType = group.type;
    record.conParentId = '1';
    record.conType = 'DIR';
    record.conManager = UserContext.loginName;
    record.conSpecies = '';
    record.conState = 'ON';
    return record;
  },
};
