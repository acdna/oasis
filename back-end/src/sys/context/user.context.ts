import { SysUser } from '../entity/sys.user.entity';
import { SYS_SYSTEM, SYS_USER } from '../config/sys.config.defined';
import { ConfigMapper } from '../config/config.mapper';
import { SysRequestContext } from './sys.request.context';
import { LocaleContext } from '../../common/locales/locale';

/**
 * 封装了用户上下文信息
 * @author: jiangbin
 * @date: 2021-01-07 10:06:11
 **/
export const UserContext = {
  /**
   * 设置当前用户信息对象
   * @author: jiangbin
   * @date: 2021-01-18 22:18:49
   **/
  set user(user: SysUser) {
    this.context.user = user;
  },

  /**
   * 获取上下文对象
   * @author: jiangbin
   * @date: 2021-01-18 22:14:17
   **/
  get context() {
    return SysRequestContext.get();
  },

  /**
   * 获取用户信息
   * @author: jiangbin
   * @date: 2021-01-07 09:31:10
   **/
  get user(): SysUser {
    return this.context?.user;
  },

  /**
   * 获取用户登录帐号
   * @author: jiangbin
   * @date: 2021-01-07 09:32:19
   **/
  get loginName(): string {
    return this.user ? this.user.userLoginName : null;
  },

  /**
   * 获取用户姓名，支持国际化
   * @author: jiangbin
   * @date: 2021-02-05 08:43:07
   **/
  get userName(): string {
    return LocaleContext.message({ cn: this.user?.userName, en: this.user?.userNameEn });
  },

  /**
   * 获取请求对象
   * @author: jiangbin
   * @date: 2021-01-07 09:33:25
   **/
  get request() {
    return this.context.req;
  },

  /**
   * 获取响应对象
   * @author: jiangbin
   * @date: 2021-01-07 09:33:22
   **/
  get response() {
    return this.context.res;
  },

  /**
   * 获取用户当前种属
   * @author: jiangbin
   * @date: 2021-01-07 09:42:31
   **/
  get species(): string {
    return this.user?.userCurrSpecies || SYS_SYSTEM.SYS_SAM_SPECIES.value;
  },

  /**
   * 获取用户当前语言
   * @author: jiangbin
   * @date: 2021-01-07 09:42:31
   **/
  get language(): string {
    return ConfigMapper.userLanguage || SYS_USER.USER_LANGUAGE.value;
  },
};
