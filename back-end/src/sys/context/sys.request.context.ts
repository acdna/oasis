import { RequestContext } from '@medibloc/nestjs-request-context';
import { SysUser } from '../entity/sys.user.entity';

/**
 * 系统请求上下文对象
 * @author: jiangbin
 * @date: 2021-01-18 22:16:18
 **/
export class SysRequestContext extends RequestContext {
  user: SysUser;
}
