import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysCacheTrace } from '../entity/sys.cache.trace.entity';
import { SysCacheTraceUDto } from '../dto/update/sys.cache.trace.udto';
import { SysCacheTraceCDto } from '../dto/create/sys.cache.trace.cdto';
import { SysCacheTraceQDto } from '../dto/query/sys.cache.trace.qdto';
import { SysCacheTraceSQL } from '../entity/sql/sys.cache.trace.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_CACHE_TRACE表对应服务层类
 * @date 12/30/2020, 3:45:15 PM
 * @author jiangbin
 * @export
 * @class SysCacheTraceService
 */
@Injectable()
export class SysCacheTraceService {
  constructor(@InjectRepository(SysCacheTrace) private readonly entityRepo: Repository<SysCacheTrace>) {}

  /**
   * 获取实体类(SysCacheTrace)的Repository对象
   */
  get repository(): Repository<SysCacheTrace> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysCacheTrace[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): Promise<SysCacheTrace[]> {
    let querySql = SysCacheTraceSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysCacheTrace> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysCacheTrace[]> {
    let querySql = SysCacheTraceSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysCacheTraceSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): Promise<SysCacheTrace> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysCacheTraceSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): Promise<number> {
    let countSql = SysCacheTraceSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ ctId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysCacheTraceQDto | Partial<SysCacheTraceQDto>): Promise<any> {
    let deleteSql = SysCacheTraceSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysCacheTraceUDto | Partial<SysCacheTraceUDto>): Promise<any> {
    if (!dto.ctId || dto.ctId.length == 0) return {};
    let sql = SysCacheTraceSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysCacheTraceUDto[] | Partial<SysCacheTraceUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.ctId) dto.ctId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysCacheTraceUDto | Partial<SysCacheTraceUDto>): Promise<any> {
    if (!dto.ctId || dto.ctId.length == 0) return {};
    let sql = SysCacheTraceSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysCacheTraceUDto | Partial<SysCacheTraceUDto>): Promise<any> {
    if (!dto.ctIdList || dto.ctIdList.length == 0) return {};
    let sql = SysCacheTraceSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysCacheTraceUDto[] | Partial<SysCacheTraceUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.ctId) dto.ctId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysCacheTraceCDto | Partial<SysCacheTraceCDto>): Promise<SysCacheTraceCDto> {
    if (!dto.ctId) {
      dto.ctId = uuid();
    }
    let sql = SysCacheTraceSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysCacheTraceCDto[] | Partial<SysCacheTraceCDto>[]): Promise<SysCacheTraceCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.ctId) dto.ctId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysCacheTraceSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
