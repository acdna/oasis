import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysMenu } from '../entity/sys.menu.entity';
import { SysMenuUDto } from '../dto/update/sys.menu.udto';
import { SysMenuCDto } from '../dto/create/sys.menu.cdto';
import { SysMenuQDto } from '../dto/query/sys.menu.qdto';
import { SysMenuSQL } from '../entity/sql/sys.menu.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_MENU表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysMenuService
 */
@Injectable()
export class SysMenuService {
  constructor(@InjectRepository(SysMenu) private readonly entityRepo: Repository<SysMenu>) {}

  /**
   * 获取实体类(SysMenu)的Repository对象
   */
  get repository(): Repository<SysMenu> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysMenu[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysMenuQDto | Partial<SysMenuQDto>): Promise<SysMenu[]> {
    let querySql = SysMenuSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysMenu> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysMenu[]> {
    let querySql = SysMenuSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysMenuQDto | Partial<SysMenuQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysMenuSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysMenuQDto | Partial<SysMenuQDto>): Promise<SysMenu> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysMenuSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysMenuQDto | Partial<SysMenuQDto>): Promise<number> {
    let countSql = SysMenuSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ menuId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysMenuQDto | Partial<SysMenuQDto>): Promise<any> {
    let deleteSql = SysMenuSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysMenuUDto | Partial<SysMenuUDto>): Promise<any> {
    if (!dto.menuId || dto.menuId.length == 0) return {};
    let sql = SysMenuSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysMenuUDto[] | Partial<SysMenuUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.menuId) dto.menuId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysMenuUDto | Partial<SysMenuUDto>): Promise<any> {
    if (!dto.menuId || dto.menuId.length == 0) return {};
    let sql = SysMenuSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysMenuUDto | Partial<SysMenuUDto>): Promise<any> {
    if (!dto.menuIdList || dto.menuIdList.length == 0) return {};
    let sql = SysMenuSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysMenuUDto[] | Partial<SysMenuUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.menuId) dto.menuId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysMenuCDto | Partial<SysMenuCDto>): Promise<SysMenuCDto> {
    if (!dto.menuId) {
      dto.menuId = uuid();
    }
    let sql = SysMenuSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysMenuCDto[] | Partial<SysMenuCDto>[]): Promise<SysMenuCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.menuId) dto.menuId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysMenuSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }

  /***************新增的方法***************/

  /**
   * 分页查询记录
   * @param query
   */
  async findPageDetail(query: SysMenuCDto | Partial<SysMenuCDto>): Promise<any> {
    let page = await this.findPage(query);
    let data = page.data;
    if (!data || data.length == 0) {
      return page;
    }

    //查询字典的字典项列表
    let idList = data.map((row) => row.menuId);
    let subRows = await this.findList({ menuParentIdList: idList, order: 'MENU_ORDER ASC' });
    subRows?.length > 0 &&
      (data = data.map((row) => {
        row.children = subRows.filter((sub) => sub.menuParentId === row.menuId);
        return row;
      }));

    page.data = data;
    return page;
  }
}
