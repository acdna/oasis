import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysUserTopUp } from '../entity/sys.user.top.up.entity';
import { SysUserTopUpUDto } from '../dto/update/sys.user.top.up.udto';
import { SysUserTopUpCDto } from '../dto/create/sys.user.top.up.cdto';
import { SysUserTopUpQDto } from '../dto/query/sys.user.top.up.qdto';
import { SysUserTopUpSQL } from '../entity/sql/sys.user.top.up.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_USER_TOP_UP表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysUserTopUpService
 */
@Injectable()
export class SysUserTopUpService {
  constructor(@InjectRepository(SysUserTopUp) private readonly entityRepo: Repository<SysUserTopUp>) {}

  /**
   * 获取实体类(SysUserTopUp)的Repository对象
   */
  get repository(): Repository<SysUserTopUp> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysUserTopUp[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): Promise<SysUserTopUp[]> {
    let querySql = SysUserTopUpSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysUserTopUp> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysUserTopUp[]> {
    let querySql = SysUserTopUpSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysUserTopUpSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): Promise<SysUserTopUp> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysUserTopUpSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): Promise<number> {
    let countSql = SysUserTopUpSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ utuId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysUserTopUpQDto | Partial<SysUserTopUpQDto>): Promise<any> {
    let deleteSql = SysUserTopUpSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysUserTopUpUDto | Partial<SysUserTopUpUDto>): Promise<any> {
    if (!dto.utuId || dto.utuId.length == 0) return {};
    let sql = SysUserTopUpSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysUserTopUpUDto[] | Partial<SysUserTopUpUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.utuId) dto.utuId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysUserTopUpUDto | Partial<SysUserTopUpUDto>): Promise<any> {
    if (!dto.utuId || dto.utuId.length == 0) return {};
    let sql = SysUserTopUpSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysUserTopUpUDto | Partial<SysUserTopUpUDto>): Promise<any> {
    if (!dto.utuIdList || dto.utuIdList.length == 0) return {};
    let sql = SysUserTopUpSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysUserTopUpUDto[] | Partial<SysUserTopUpUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.utuId) dto.utuId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysUserTopUpCDto | Partial<SysUserTopUpCDto>): Promise<SysUserTopUpCDto> {
    if (!dto.utuId) {
      dto.utuId = uuid();
    }
    let sql = SysUserTopUpSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysUserTopUpCDto[] | Partial<SysUserTopUpCDto>[]): Promise<SysUserTopUpCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.utuId) dto.utuId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysUserTopUpSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
