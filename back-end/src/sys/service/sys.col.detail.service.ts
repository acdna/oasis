import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysColDetail } from '../entity/sys.col.detail.entity';
import { SysColDetailUDto } from '../dto/update/sys.col.detail.udto';
import { SysColDetailCDto } from '../dto/create/sys.col.detail.cdto';
import { SysColDetailQDto } from '../dto/query/sys.col.detail.qdto';
import { SysColDetailSQL } from '../entity/sql/sys.col.detail.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_COL_DETAIL表对应服务层类
 * @date 12/30/2020, 3:45:15 PM
 * @author jiangbin
 * @export
 * @class SysColDetailService
 */
@Injectable()
export class SysColDetailService {
  constructor(@InjectRepository(SysColDetail) private readonly entityRepo: Repository<SysColDetail>) {}

  /**
   * 获取实体类(SysColDetail)的Repository对象
   */
  get repository(): Repository<SysColDetail> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysColDetail[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysColDetailQDto | Partial<SysColDetailQDto>): Promise<SysColDetail[]> {
    let querySql = SysColDetailSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysColDetail> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysColDetail[]> {
    let querySql = SysColDetailSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysColDetailQDto | Partial<SysColDetailQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysColDetailSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysColDetailQDto | Partial<SysColDetailQDto>): Promise<SysColDetail> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysColDetailSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysColDetailQDto | Partial<SysColDetailQDto>): Promise<number> {
    let countSql = SysColDetailSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ cdId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysColDetailQDto | Partial<SysColDetailQDto>): Promise<any> {
    let deleteSql = SysColDetailSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysColDetailUDto | Partial<SysColDetailUDto>): Promise<any> {
    if (!dto.cdId || dto.cdId.length == 0) return {};
    let sql = SysColDetailSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysColDetailUDto[] | Partial<SysColDetailUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.cdId) dto.cdId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysColDetailUDto | Partial<SysColDetailUDto>): Promise<any> {
    if (!dto.cdId || dto.cdId.length == 0) return {};
    let sql = SysColDetailSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysColDetailUDto | Partial<SysColDetailUDto>): Promise<any> {
    if (!dto.cdIdList || dto.cdIdList.length == 0) return {};
    let sql = SysColDetailSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysColDetailUDto[] | Partial<SysColDetailUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.cdId) dto.cdId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysColDetailCDto | Partial<SysColDetailCDto>): Promise<SysColDetailCDto> {
    if (!dto.cdId) {
      dto.cdId = uuid();
    }
    let sql = SysColDetailSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysColDetailCDto[] | Partial<SysColDetailCDto>[]): Promise<SysColDetailCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.cdId) dto.cdId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysColDetailSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
