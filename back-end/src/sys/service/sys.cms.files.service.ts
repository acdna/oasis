import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysCmsFiles } from '../entity/sys.cms.files.entity';
import { SysCmsFilesUDto } from '../dto/update/sys.cms.files.udto';
import { SysCmsFilesCDto } from '../dto/create/sys.cms.files.cdto';
import { SysCmsFilesQDto } from '../dto/query/sys.cms.files.qdto';
import { SysCmsFilesSQL } from '../entity/sql/sys.cms.files.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_CMS_FILES表对应服务层类
 * @date 12/30/2020, 3:45:15 PM
 * @author jiangbin
 * @export
 * @class SysCmsFilesService
 */
@Injectable()
export class SysCmsFilesService {
  constructor(@InjectRepository(SysCmsFiles) private readonly entityRepo: Repository<SysCmsFiles>) {}

  /**
   * 获取实体类(SysCmsFiles)的Repository对象
   */
  get repository(): Repository<SysCmsFiles> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysCmsFiles[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): Promise<SysCmsFiles[]> {
    let querySql = SysCmsFilesSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysCmsFiles> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysCmsFiles[]> {
    let querySql = SysCmsFilesSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysCmsFilesSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): Promise<SysCmsFiles> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysCmsFilesSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): Promise<number> {
    let countSql = SysCmsFilesSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ scFileId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysCmsFilesQDto | Partial<SysCmsFilesQDto>): Promise<any> {
    let deleteSql = SysCmsFilesSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysCmsFilesUDto | Partial<SysCmsFilesUDto>): Promise<any> {
    if (!dto.scFileId || dto.scFileId.length == 0) return {};
    let sql = SysCmsFilesSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysCmsFilesUDto[] | Partial<SysCmsFilesUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.scFileId) dto.scFileId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysCmsFilesUDto | Partial<SysCmsFilesUDto>): Promise<any> {
    if (!dto.scFileId || dto.scFileId.length == 0) return {};
    let sql = SysCmsFilesSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysCmsFilesUDto | Partial<SysCmsFilesUDto>): Promise<any> {
    if (!dto.scFileIdList || dto.scFileIdList.length == 0) return {};
    let sql = SysCmsFilesSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysCmsFilesUDto[] | Partial<SysCmsFilesUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.scFileId) dto.scFileId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysCmsFilesCDto | Partial<SysCmsFilesCDto>): Promise<SysCmsFilesCDto> {
    if (!dto.scFileId) {
      dto.scFileId = uuid();
    }
    let sql = SysCmsFilesSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysCmsFilesCDto[] | Partial<SysCmsFilesCDto>[]): Promise<SysCmsFilesCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.scFileId) dto.scFileId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysCmsFilesSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
