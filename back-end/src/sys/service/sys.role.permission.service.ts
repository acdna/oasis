import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysRolePermission } from '../entity/sys.role.permission.entity';
import { SysRolePermissionUDto } from '../dto/update/sys.role.permission.udto';
import { SysRolePermissionCDto } from '../dto/create/sys.role.permission.cdto';
import { SysRolePermissionQDto } from '../dto/query/sys.role.permission.qdto';
import { SysRolePermissionSQL } from '../entity/sql/sys.role.permission.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_ROLE_PERMISSION表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysRolePermissionService
 */
@Injectable()
export class SysRolePermissionService {
  constructor(@InjectRepository(SysRolePermission) private readonly entityRepo: Repository<SysRolePermission>) {}

  /**
   * 获取实体类(SysRolePermission)的Repository对象
   */
  get repository(): Repository<SysRolePermission> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysRolePermission[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysRolePermissionQDto | Partial<SysRolePermissionQDto>): Promise<SysRolePermission[]> {
    let querySql = SysRolePermissionSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysRolePermission> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysRolePermission[]> {
    let querySql = SysRolePermissionSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysRolePermissionQDto | Partial<SysRolePermissionQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysRolePermissionSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysRolePermissionQDto | Partial<SysRolePermissionQDto>): Promise<SysRolePermission> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysRolePermissionSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysRolePermissionQDto | Partial<SysRolePermissionQDto>): Promise<number> {
    let countSql = SysRolePermissionSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ rpId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysRolePermissionQDto | Partial<SysRolePermissionQDto>): Promise<any> {
    let deleteSql = SysRolePermissionSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysRolePermissionUDto | Partial<SysRolePermissionUDto>): Promise<any> {
    if (!dto.rpId || dto.rpId.length == 0) return {};
    let sql = SysRolePermissionSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysRolePermissionUDto[] | Partial<SysRolePermissionUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.rpId) dto.rpId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysRolePermissionUDto | Partial<SysRolePermissionUDto>): Promise<any> {
    if (!dto.rpId || dto.rpId.length == 0) return {};
    let sql = SysRolePermissionSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysRolePermissionUDto | Partial<SysRolePermissionUDto>): Promise<any> {
    if (!dto.rpIdList || dto.rpIdList.length == 0) return {};
    let sql = SysRolePermissionSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysRolePermissionUDto[] | Partial<SysRolePermissionUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.rpId) dto.rpId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysRolePermissionCDto | Partial<SysRolePermissionCDto>): Promise<SysRolePermissionCDto> {
    if (!dto.rpId) {
      dto.rpId = uuid();
    }
    let sql = SysRolePermissionSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysRolePermissionCDto[] | Partial<SysRolePermissionCDto>[]): Promise<SysRolePermissionCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.rpId) dto.rpId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysRolePermissionSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
