import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysBillingCard } from '../entity/sys.billing.card.entity';
import { SysBillingCardUDto } from '../dto/update/sys.billing.card.udto';
import { SysBillingCardCDto } from '../dto/create/sys.billing.card.cdto';
import { SysBillingCardQDto } from '../dto/query/sys.billing.card.qdto';
import { SysBillingCardSQL } from '../entity/sql/sys.billing.card.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_BILLING_CARD表对应服务层类
 * @date 12/30/2020, 3:45:15 PM
 * @author jiangbin
 * @export
 * @class SysBillingCardService
 */
@Injectable()
export class SysBillingCardService {
  constructor(@InjectRepository(SysBillingCard) private readonly entityRepo: Repository<SysBillingCard>) {}

  /**
   * 获取实体类(SysBillingCard)的Repository对象
   */
  get repository(): Repository<SysBillingCard> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysBillingCard[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): Promise<SysBillingCard[]> {
    const querySql = SysBillingCardSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysBillingCard> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysBillingCard[]> {
    const querySql = SysBillingCardSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysBillingCardQDto | Partial<SysBillingCardQDto>): Promise<any> {
    const count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    const querySql = SysBillingCardSQL.SELECT_SQL(query);
    const data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysBillingCardQDto | Partial<SysBillingCardQDto>): Promise<SysBillingCard> {
    dto.offset = 0;
    dto.pageSize = 1;
    const sql = SysBillingCardSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysBillingCardQDto | Partial<SysBillingCardQDto>): Promise<number> {
    const countSql = SysBillingCardSQL.SELECT_COUNT_SQL(query);
    const result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ bcId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysBillingCardQDto | Partial<SysBillingCardQDto>): Promise<any> {
    const deleteSql = SysBillingCardSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysBillingCardUDto | Partial<SysBillingCardUDto>): Promise<any> {
    if (!dto.bcId || dto.bcId.length == 0) return {};
    const sql = SysBillingCardSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysBillingCardUDto[] | Partial<SysBillingCardUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.bcId) dto.bcId = uuid();
    });
    const results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (const dto of dtos) {
        const result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysBillingCardUDto | Partial<SysBillingCardUDto>): Promise<any> {
    if (!dto.bcId || dto.bcId.length == 0) return {};
    const sql = SysBillingCardSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysBillingCardUDto | Partial<SysBillingCardUDto>): Promise<any> {
    if (!dto.bcIdList || dto.bcIdList.length == 0) return {};
    const sql = SysBillingCardSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysBillingCardUDto[] | Partial<SysBillingCardUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.bcId) dto.bcId = uuid();
    });
    const results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (const dto of dtos) {
        const result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysBillingCardCDto | Partial<SysBillingCardCDto>): Promise<SysBillingCardCDto> {
    if (!dto.bcId) {
      dto.bcId = uuid();
    }
    const sql = SysBillingCardSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysBillingCardCDto[] | Partial<SysBillingCardCDto>[]): Promise<SysBillingCardCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.bcId) dto.bcId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      const sql = SysBillingCardSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
