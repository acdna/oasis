import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysUser } from '../entity/sys.user.entity';
import { SysUserUDto } from '../dto/update/sys.user.udto';
import { SysUserCDto } from '../dto/create/sys.user.cdto';
import { SysUserQDto } from '../dto/query/sys.user.qdto';
import { SysUserSQL } from '../entity/sql/sys.user.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import { SysUserRoleService } from './sys.user.role.service';

/**
 * SYS_USER表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysUserService
 */
@Injectable()
export class SysUserService {
  constructor(@InjectRepository(SysUser) private readonly entityRepo: Repository<SysUser>, @Inject(SysUserRoleService) private readonly userRoleService: SysUserRoleService) {}

  /**
   * 获取实体类(SysUser)的Repository对象
   */
  get repository(): Repository<SysUser> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysUser[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysUserQDto | Partial<SysUserQDto>): Promise<SysUser[]> {
    let querySql = SysUserSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysUser> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysUser[]> {
    let querySql = SysUserSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysUserQDto | Partial<SysUserQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysUserSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    //查询关联角色ID信息
    await this.findUserRoles(data);
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 填充用户角色ID列表
   * @param users 用户记录列表
   * @author: jiangbin
   * @date: 2021-01-02 10:32:25
   **/
  async findUserRoles(users: any[]) {
    if (!users || users.length == 0) return;
    let userLoginNames = users.map((record) => record['userLoginName']);
    if (!userLoginNames || userLoginNames.length == 0) return;
    let urRoles = await this.userRoleService.findList({ urUserLoginNameList: userLoginNames });
    urRoles?.length > 0 &&
      users.forEach((record) => {
        record.userRoles = urRoles.filter((urRole) => urRole.urUserLoginName == record.userLoginName).map((urRole) => urRole.urRoleId);
      });
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysUserQDto | Partial<SysUserQDto>): Promise<SysUser> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysUserSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysUserQDto | Partial<SysUserQDto>): Promise<number> {
    let countSql = SysUserSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ userId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysUserQDto | Partial<SysUserQDto>): Promise<any> {
    let deleteSql = SysUserSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysUserUDto | Partial<SysUserUDto>): Promise<any> {
    if (!dto.userId || dto.userId.length == 0) return {};
    let sql = SysUserSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysUserUDto[] | Partial<SysUserUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.userId) dto.userId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysUserUDto | Partial<SysUserUDto>): Promise<any> {
    if (!dto.userId || dto.userId.length == 0) return {};
    let sql = SysUserSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysUserUDto | Partial<SysUserUDto>): Promise<any> {
    if (!dto.userIdList || dto.userIdList.length == 0) return {};
    let sql = SysUserSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysUserUDto[] | Partial<SysUserUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.userId) dto.userId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysUserCDto | Partial<SysUserCDto>): Promise<SysUserCDto> {
    if (!dto.userId) {
      dto.userId = uuid();
    }
    let sql = SysUserSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysUserCDto[] | Partial<SysUserCDto>[]): Promise<SysUserCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.userId) dto.userId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysUserSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
