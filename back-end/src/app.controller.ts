import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { SkipAuth } from './common/auth/guard/skip.auth';

/**
 *
 */
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @SkipAuth()
  @Get('test')
  async test(): Promise<string> {
    return '系统提示：系统已启动，功能模块已成功加载，测试结束!';
  }
}
