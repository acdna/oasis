import {DocxContext} from "../../common/docx/templates/docx.context";
import {DOCX_TEMPLATES} from "../../common/docx/templates/docx.template.id";
import * as fs from "fs";
import {MailSender} from "../../common/mail/sender/mail.sender";
import {uuid} from "../../common/utils/uuid.utils";

export const OnpForecastSendMail = {
  async sendMail(list) {
    const filePath = DocxContext.getTemplatePath(DOCX_TEMPLATES['test_result']);
    const buffer = fs.readFileSync(filePath);
    /**
     * CC 英文全称是 Carbon Copy(抄送);
     * BCC英文全称是 Blind CarbonCopy(密送)。
     * 两者的区别在于在BCC栏中的收件人可以看到所有的收件人名(TO,CC,BCC)，而在TO 和CC栏中的收件人看不到BBC的收件人名。
     */
    return await MailSender.send({
      from: {name: '郑恩泽', address: 'zhengenze@maizedna.cn'},
      to: list.foColumn4,
      // to: '13485380225@163.com',
      cc: '754376957@qq.com', //抄送
      // bcc: '404093781@qq.com', //暗抄送
      text: '测试邮件',
      subject: '测试邮件',
      attachments: [
        {
          filename: '测试附件.docx',
          content: buffer,
          cid: uuid(),
        },
      ],
    });
  }
};