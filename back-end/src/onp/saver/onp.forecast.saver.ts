import { PathUtils } from '../../common/path/path.creator.utils';
import { OnpForecast } from '../entity/onp.forecast.entity';
import { OnpForecastCDto } from '../dto/create/onp.forecast.cdto';
import {OnpServices} from "../onp.services";

/**
 * 样品数据解析
 *
 * @author jiang
 * @date 2021-03-27 13:05:08
 **/
export const OnpForecastSaver = {
  /**
   * 解析样品信息
   * @param params.relativePath 样品数据文件相对路径
   * @param params.fileId 样品数据关联的指纹数据记录ID
   * @author: jiangbin
   * @date: 2021-01-14 15:53:54
   **/
  save: async (dto: OnpForecastCDto | Partial<OnpForecastCDto>): Promise<OnpForecastCDto> => {
    if (!dto || dto.foFilePath.length === 0) {
      return null;
    }
    return await OnpServices.onpForecastService.create(dto);
  },
};
