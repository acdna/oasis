import { OnpStatistics } from '../entity/onp.statistics.entity';
import { OnpServices } from '../onp.services';
import { ArrayUtils } from '../../common/utils/array.utils';
import { Mapper } from '../../common/mapper/mapper';
import { LogUtils } from '../../common/log4js/log4js.utils';

/**
 * 保存样品信息表
 * @author: jiangbin
 * @date: 2021-03-26 16:41:10
 **/
export const OnpStatisticsSaver = {
  /**
   * 保存样品信息列表
   * @param samples
   * @return
   * @author: jiangbin
   * @date: 2021-03-26 16:20:14
   **/
  async save(statistics: OnpStatistics[]) {
    if (ArrayUtils.isEmpty(statistics)) {
      return false;
    }

    //
    let stBinList = statistics.map((statistics) => statistics.stBin);

    //查询已有记录
    let records = await OnpServices.onpStatisticsService.findList({ stBinList });

    //构建样品条码号映射表
    let mapper = new Mapper<string, OnpStatistics>();
    mapper.addList(records, (record) => record.stBin);

    let updates = new Array<OnpStatistics>(); //更新记录列表
    let inserts = new Array<OnpStatistics>(); //新记录列表
    let noauths = new Array<OnpStatistics>(); //无权限记录列表

    statistics.forEach((statistics) => {
      let record = mapper.get(statistics.stBin);

      //新记录
      if (!record) {
        inserts.push(statistics);
        return;
      }

      //更新记录
      if (statistics.stBin === record.stBin) {
        updates.push(statistics);
        return;
      }

      //无权限记录
      noauths.push(record);
    });

    //更新和插入记录
    if (updates?.length > 0) {
      await OnpServices.onpStatisticsService.updateList(updates);
    }
    if (inserts?.length > 0) {
      await OnpServices.onpStatisticsService.createList(inserts);
    }

    LogUtils.info('SsrSampleSaver', `插入记录：${inserts?.length || 0},更新记录：${updates?.length || 0},无权限记录：${noauths?.length || 0}`);

    //操作结果
    return {
      updates,
      inserts,
      noauths,
      uCount: updates?.length || 0,
      iCount: inserts?.length || 0,
      nCount: noauths?.length || 0,
    };
  },
};
