import { NestExpressApplication } from '@nestjs/platform-express';
import { OnpForecastService } from './service/onp.forecast.service';
import { OnpStatisticsService } from './service/onp.statistics.service';
import { OnpVerificationService } from './service/onp.verification.service';
import {OnpChloroplastHaplotypeService} from "./service/onp.chloroplast.haplotype.service";
import {OnpChloroplastOverallService} from "./service/onp.chloroplast.overall.service";
import {OnpNucleusHaplotypeService} from "./service/onp.nucleus.haplotype.service";
/**
 * ONP模块服务类的静态引用
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 **/
export class OnpServices {
    public static onpChloroplastHaplotypeService: OnpChloroplastHaplotypeService;
    public static onpChloroplastOverallService: OnpChloroplastOverallService;
    public static onpNucleusHaplotypeService: OnpNucleusHaplotypeService;
    public static onpForecastService: OnpForecastService;
    public static onpStatisticsService: OnpStatisticsService;
    public static onpVerificationService: OnpVerificationService;
    /**
     * 初始化ONP模块服务类的静态引用
     * @date 4/6/2021, 10:21:58 AM
     * @author jiangbin
     **/
    static init(app: NestExpressApplication) {
        this.onpChloroplastHaplotypeService = app.get(OnpChloroplastHaplotypeService);
        this.onpChloroplastOverallService = app.get(OnpChloroplastOverallService);
        this.onpNucleusHaplotypeService = app.get(OnpNucleusHaplotypeService);
        this.onpForecastService = app.get(OnpForecastService);
        this.onpStatisticsService = app.get(OnpStatisticsService);
        this.onpVerificationService = app.get(OnpVerificationService);
    }
}