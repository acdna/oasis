import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * ONP统计数据表
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpStatistics
 */
@Entity({
  name: 'ONP_STATISTICS',
})
export class OnpStatistics {
  /**
   * id-主键
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_ID', type: 'varchar', length: '255', primary: true })
  @IsNotEmpty({ message: '【id】不能为空' })
  @Max(255, { message: '【id】长度不能超过255' })
  stId: string;
  /**
   * Bin
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_BIN', type: 'varchar', length: '255' })
  @Max(255, { message: '【Bin】长度不能超过255' })
  stBin: string;
  /**
   * Chr
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_CHR', type: 'varchar', length: '255' })
  @Max(255, { message: '【Chr】长度不能超过255' })
  stChr: string;
  /**
   * Start
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_START', type: 'varchar', length: '255' })
  @Max(255, { message: '【Start】长度不能超过255' })
  stStart: string;
  /**
   * Stop
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_STOP', type: 'varchar', length: '255' })
  @Max(255, { message: '【Stop】长度不能超过255' })
  stStop: string;
  /**
   * Bin
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_BIN_SIZE', type: 'varchar', length: '255' })
  @Max(255, { message: '【Bin】长度不能超过255' })
  stBinSize: string;
  /**
   * Desc
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_DESC', type: 'varchar', length: '255' })
  @Max(255, { message: '【Desc】长度不能超过255' })
  stDesc: string;
  /**
   * ALL
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_ALL_MARKERS', type: 'varchar', length: '255' })
  @Max(255, { message: '【ALL】长度不能超过255' })
  stAllMarkers: string;
  /**
   * SNP
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_SNP_MARKERS', type: 'varchar', length: '255' })
  @Max(255, { message: '【SNP】长度不能超过255' })
  stSnpMarkers: string;
  /**
   * INDEL
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_INDEL_MARKERS', type: 'varchar', length: '255' })
  @Max(255, { message: '【INDEL】长度不能超过255' })
  stIndelMarkers: string;
  /**
   * Block
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_BLOCK_MARKERS', type: 'varchar', length: '255' })
  @Max(255, { message: '【Block】长度不能超过255' })
  stBlockMarkers: string;
  /**
   * Tags
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_TAGS', type: 'varchar', length: '255' })
  @Max(255, { message: '【Tags】长度不能超过255' })
  stTags: string;
  /**
   * Genotypes
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_GENOTYPES', type: 'varchar', length: '255' })
  @Max(255, { message: '【Genotypes】长度不能超过255' })
  stGenotypes: string;
  /**
   * Max
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_MAX_GENOTYPES_FREQ', type: 'varchar', length: '255' })
  @Max(255, { message: '【Max】长度不能超过255' })
  stMaxGenotypesFreq: string;
  /**
   * Min
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_MIN_GENOTYPES_FREQ', type: 'varchar', length: '255' })
  @Max(255, { message: '【Min】长度不能超过255' })
  stMinGenotypesFreq: string;
  /**
   * PIC
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_PIC', type: 'varchar', length: '255' })
  @Max(255, { message: '【PIC】长度不能超过255' })
  stPic: string;
  /**
   * Genetic
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_GENETIC_MAP', type: 'varchar', length: '255' })
  @Max(255, { message: '【Genetic】长度不能超过255' })
  stGeneticMap: string;
  /**
   * ORDER
   *
   * @type { number }
   * @memberof OnpStatistics
   */
  @Type(() => Number)
  @Column({ name: 'ST_ORDER', type: 'int' })
  stOrder: number;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_COLUMN1', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column1】长度不能超过255' })
  stColumn1: string;
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_COLUMN2', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column2】长度不能超过255' })
  stColumn2: string;
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_COLUMN3', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column3】长度不能超过255' })
  stColumn3: string;
  /**
   * Species
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_SPECIES', type: 'varchar', length: '255' })
  @Max(255, { message: '【Species】长度不能超过255' })
  stSpecies: string;
  /**
   * Remark
   *
   * @type { string }
   * @memberof OnpStatistics
   */
  @Column({ name: 'ST_REMARK', type: 'varchar', length: '128' })
  @Max(128, { message: '【Remark】长度不能超过128' })
  stRemark: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof OnpStatistics
   */
  @Type(() => Date)
  @Column({ name: 'ST_CREATE_DATE', type: 'datetime' })
  stCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof OnpStatistics
   */
  @Type(() => Date)
  @Column({ name: 'ST_UPDATE_DATE', type: 'datetime' })
  stUpdateDate: Date;
}
//声明类型定义
export declare type OnpStatisticsType = OnpStatistics | Partial<OnpStatistics>;
