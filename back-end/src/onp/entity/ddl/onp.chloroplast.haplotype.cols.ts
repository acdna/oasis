/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotypeColNames
 */
export interface OnpChloroplastHaplotypeColNames {
    /**
     * @description id
     * @field OCH_ID
     */
    OCH_ID: string, //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_HAPLOTYPE
         */
        OCH_CHLOROPLAST_HAPLOTYPE: string, //
        /**
         * @description Chr
         * @field OCH_CHR
         */
        OCH_CHR: string, //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_ONP
         */
        OCH_CHLOROPLAST_ONP: string, //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE
         */
        OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE: string, //
        /**
         * @description Frequency
         * @field OCH_FREQUENCY
         */
        OCH_FREQUENCY: string, //
        /**
         * @description Column1
         * @field OCH_COLUMN1
         */
        OCH_COLUMN1: string, //
        /**
         * @description Column2
         * @field OCH_COLUMN2
         */
        OCH_COLUMN2: string, //
        /**
         * @description Column3
         * @field OCH_COLUMN3
         */
        OCH_COLUMN3: string, //
        /**
         * @description ID
         * @field OCH_ORDER
         */
        OCH_ORDER: number, //
        /**
         * @description Species
         * @field OCH_SPECIES
         */
        OCH_SPECIES: string, //
        /**
         * @description Remark
         * @field OCH_REMARK
         */
        OCH_REMARK: string, //
        /**
         * @description 创建日期
         * @field OCH_CREATE_DATE
         */
        OCH_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field OCH_UPDATE_DATE
         */
        OCH_UPDATE_DATE: Date, //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotypeColProps
 */
export interface OnpChloroplastHaplotypeColProps {
    /**
     * @description ochId
     * @field ochId
     */
    ochId: string, //
        /**
         * @description ochChloroplastHaplotype
         * @field ochChloroplastHaplotype
         */
        ochChloroplastHaplotype: string, //
        /**
         * @description ochChr
         * @field ochChr
         */
        ochChr: string, //
        /**
         * @description ochChloroplastOnp
         * @field ochChloroplastOnp
         */
        ochChloroplastOnp: string, //
        /**
         * @description ochChloroplastHaplotypeSequence
         * @field ochChloroplastHaplotypeSequence
         */
        ochChloroplastHaplotypeSequence: string, //
        /**
         * @description ochFrequency
         * @field ochFrequency
         */
        ochFrequency: string, //
        /**
         * @description ochColumn1
         * @field ochColumn1
         */
        ochColumn1: string, //
        /**
         * @description ochColumn2
         * @field ochColumn2
         */
        ochColumn2: string, //
        /**
         * @description ochColumn3
         * @field ochColumn3
         */
        ochColumn3: string, //
        /**
         * @description ochOrder
         * @field ochOrder
         */
        ochOrder: number, //
        /**
         * @description ochSpecies
         * @field ochSpecies
         */
        ochSpecies: string, //
        /**
         * @description ochRemark
         * @field ochRemark
         */
        ochRemark: string, //
        /**
         * @description ochCreateDate
         * @field ochCreateDate
         */
        ochCreateDate: Date, //
        /**
         * @description ochUpdateDate
         * @field ochUpdateDate
         */
        ochUpdateDate: Date, //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列名
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotypeCols
 */
export enum OnpChloroplastHaplotypeColNameEnum {
    /**
     * @description id
     * @field OCH_ID
     */
    OCH_ID = "OCH_ID", //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_HAPLOTYPE
         */
        OCH_CHLOROPLAST_HAPLOTYPE = "OCH_CHLOROPLAST_HAPLOTYPE", //
        /**
         * @description Chr
         * @field OCH_CHR
         */
        OCH_CHR = "OCH_CHR", //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_ONP
         */
        OCH_CHLOROPLAST_ONP = "OCH_CHLOROPLAST_ONP", //
        /**
         * @description Chloroplast
         * @field OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE
         */
        OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE = "OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE", //
        /**
         * @description Frequency
         * @field OCH_FREQUENCY
         */
        OCH_FREQUENCY = "OCH_FREQUENCY", //
        /**
         * @description Column1
         * @field OCH_COLUMN1
         */
        OCH_COLUMN1 = "OCH_COLUMN1", //
        /**
         * @description Column2
         * @field OCH_COLUMN2
         */
        OCH_COLUMN2 = "OCH_COLUMN2", //
        /**
         * @description Column3
         * @field OCH_COLUMN3
         */
        OCH_COLUMN3 = "OCH_COLUMN3", //
        /**
         * @description ID
         * @field OCH_ORDER
         */
        OCH_ORDER = "OCH_ORDER", //
        /**
         * @description Species
         * @field OCH_SPECIES
         */
        OCH_SPECIES = "OCH_SPECIES", //
        /**
         * @description Remark
         * @field OCH_REMARK
         */
        OCH_REMARK = "OCH_REMARK", //
        /**
         * @description 创建日期
         * @field OCH_CREATE_DATE
         */
        OCH_CREATE_DATE = "OCH_CREATE_DATE", //
        /**
         * @description 更新日期
         * @field OCH_UPDATE_DATE
         */
        OCH_UPDATE_DATE = "OCH_UPDATE_DATE", //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表列属性名
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastHaplotypeCols
 */
export enum OnpChloroplastHaplotypeColPropEnum {
    /**
     * @description ochId
     * @field ochId
     */
    ochId = "ochId", //
        /**
         * @description ochChloroplastHaplotype
         * @field ochChloroplastHaplotype
         */
        ochChloroplastHaplotype = "ochChloroplastHaplotype", //
        /**
         * @description ochChr
         * @field ochChr
         */
        ochChr = "ochChr", //
        /**
         * @description ochChloroplastOnp
         * @field ochChloroplastOnp
         */
        ochChloroplastOnp = "ochChloroplastOnp", //
        /**
         * @description ochChloroplastHaplotypeSequence
         * @field ochChloroplastHaplotypeSequence
         */
        ochChloroplastHaplotypeSequence = "ochChloroplastHaplotypeSequence", //
        /**
         * @description ochFrequency
         * @field ochFrequency
         */
        ochFrequency = "ochFrequency", //
        /**
         * @description ochColumn1
         * @field ochColumn1
         */
        ochColumn1 = "ochColumn1", //
        /**
         * @description ochColumn2
         * @field ochColumn2
         */
        ochColumn2 = "ochColumn2", //
        /**
         * @description ochColumn3
         * @field ochColumn3
         */
        ochColumn3 = "ochColumn3", //
        /**
         * @description ochOrder
         * @field ochOrder
         */
        ochOrder = "ochOrder", //
        /**
         * @description ochSpecies
         * @field ochSpecies
         */
        ochSpecies = "ochSpecies", //
        /**
         * @description ochRemark
         * @field ochRemark
         */
        ochRemark = "ochRemark", //
        /**
         * @description ochCreateDate
         * @field ochCreateDate
         */
        ochCreateDate = "ochCreateDate", //
        /**
         * @description ochUpdateDate
         * @field ochUpdateDate
         */
        ochUpdateDate = "ochUpdateDate", //
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-ONP_CHLOROPLAST_HAPLOTYPE表信息
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastHaplotypeTable
 */
export enum OnpChloroplastHaplotypeTable {
    /**
     * @description OCH_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY = 'OCH_ID',
        /**
         * @description ochId
         * @field primerKey
         */
        primerKey = 'ochId',
        /**
         * @description ONP_CHLOROPLAST_HAPLOTYPE
         * @field TABLE_NAME
         */
        TABLE_NAME = 'ONP_CHLOROPLAST_HAPLOTYPE',
        /**
         * @description OnpChloroplastHaplotype
         * @field ENTITY_NAME
         */
        ENTITY_NAME = 'OnpChloroplastHaplotype',
}
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-列名数组
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const OnpChloroplastHaplotypeColNames = Object.keys(OnpChloroplastHaplotypeColNameEnum);
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-列属性名数组
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const OnpChloroplastHaplotypeColProps = Object.keys(OnpChloroplastHaplotypeColPropEnum);
/**
 * ONP_CHLOROPLAST_HAPLOTYPE-列名和列属性名映射表
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const OnpChloroplastHaplotypeColMap = { names: OnpChloroplastHaplotypeColNames, props: OnpChloroplastHaplotypeColProps };