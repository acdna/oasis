/**
 * ONP_VERIFICATION-验证表列名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpVerificationColNames
 */
export interface OnpVerificationColNames {
    /**
     * @description id
     * @field VE_ID
     */
    VE_ID: string, //
        /**
         * @description ID
         * @field VE_ORDER
         */
        VE_ORDER: number, //
        /**
         * @description Column1
         * @field VE_COLUMN1
         */
        VE_COLUMN1: string, //
        /**
         * @description Column2
         * @field VE_COLUMN2
         */
        VE_COLUMN2: string, //
        /**
         * @description Column3
         * @field VE_COLUMN3
         */
        VE_COLUMN3: string, //
        /**
         * @description Column1
         * @field VE_COLUMN4
         */
        VE_COLUMN4: string, //
        /**
         * @description Column2
         * @field VE_COLUMN5
         */
        VE_COLUMN5: string, //
        /**
         * @description Column3
         * @field VE_COLUMN6
         */
        VE_COLUMN6: string, //
        /**
         * @description Column3
         * @field VE_COLUMN7
         */
        VE_COLUMN7: string, //
        /**
         * @description Column3
         * @field VE_COLUMN8
         */
        VE_COLUMN8: string, //
        /**
         * @description Column3
         * @field VE_COLUMN9
         */
        VE_COLUMN9: string, //
        /**
         * @description Species
         * @field VE_SPECIES
         */
        VE_SPECIES: string, //
        /**
         * @description Remark
         * @field VE_REMARK
         */
        VE_REMARK: string, //
        /**
         * @description 创建日期
         * @field VE_CREATE_DATE
         */
        VE_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field VE_UPDATE_DATE
         */
        VE_UPDATE_DATE: Date, //
}
/**
 * ONP_VERIFICATION-验证表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpVerificationColProps
 */
export interface OnpVerificationColProps {
    /**
     * @description veId
     * @field veId
     */
    veId: string, //
        /**
         * @description veOrder
         * @field veOrder
         */
        veOrder: number, //
        /**
         * @description veColumn1
         * @field veColumn1
         */
        veColumn1: string, //
        /**
         * @description veColumn2
         * @field veColumn2
         */
        veColumn2: string, //
        /**
         * @description veColumn3
         * @field veColumn3
         */
        veColumn3: string, //
        /**
         * @description veColumn4
         * @field veColumn4
         */
        veColumn4: string, //
        /**
         * @description veColumn5
         * @field veColumn5
         */
        veColumn5: string, //
        /**
         * @description veColumn6
         * @field veColumn6
         */
        veColumn6: string, //
        /**
         * @description veColumn7
         * @field veColumn7
         */
        veColumn7: string, //
        /**
         * @description veColumn8
         * @field veColumn8
         */
        veColumn8: string, //
        /**
         * @description veColumn9
         * @field veColumn9
         */
        veColumn9: string, //
        /**
         * @description veSpecies
         * @field veSpecies
         */
        veSpecies: string, //
        /**
         * @description veRemark
         * @field veRemark
         */
        veRemark: string, //
        /**
         * @description veCreateDate
         * @field veCreateDate
         */
        veCreateDate: Date, //
        /**
         * @description veUpdateDate
         * @field veUpdateDate
         */
        veUpdateDate: Date, //
}
/**
 * ONP_VERIFICATION-验证表列名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpVerificationCols
 */
export enum OnpVerificationColNameEnum {
    /**
     * @description id
     * @field VE_ID
     */
    VE_ID = "VE_ID", //
        /**
         * @description ID
         * @field VE_ORDER
         */
        VE_ORDER = "VE_ORDER", //
        /**
         * @description Column1
         * @field VE_COLUMN1
         */
        VE_COLUMN1 = "VE_COLUMN1", //
        /**
         * @description Column2
         * @field VE_COLUMN2
         */
        VE_COLUMN2 = "VE_COLUMN2", //
        /**
         * @description Column3
         * @field VE_COLUMN3
         */
        VE_COLUMN3 = "VE_COLUMN3", //
        /**
         * @description Column1
         * @field VE_COLUMN4
         */
        VE_COLUMN4 = "VE_COLUMN4", //
        /**
         * @description Column2
         * @field VE_COLUMN5
         */
        VE_COLUMN5 = "VE_COLUMN5", //
        /**
         * @description Column3
         * @field VE_COLUMN6
         */
        VE_COLUMN6 = "VE_COLUMN6", //
        /**
         * @description Column3
         * @field VE_COLUMN7
         */
        VE_COLUMN7 = "VE_COLUMN7", //
        /**
         * @description Column3
         * @field VE_COLUMN8
         */
        VE_COLUMN8 = "VE_COLUMN8", //
        /**
         * @description Column3
         * @field VE_COLUMN9
         */
        VE_COLUMN9 = "VE_COLUMN9", //
        /**
         * @description Species
         * @field VE_SPECIES
         */
        VE_SPECIES = "VE_SPECIES", //
        /**
         * @description Remark
         * @field VE_REMARK
         */
        VE_REMARK = "VE_REMARK", //
        /**
         * @description 创建日期
         * @field VE_CREATE_DATE
         */
        VE_CREATE_DATE = "VE_CREATE_DATE", //
        /**
         * @description 更新日期
         * @field VE_UPDATE_DATE
         */
        VE_UPDATE_DATE = "VE_UPDATE_DATE", //
}
/**
 * ONP_VERIFICATION-验证表列属性名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpVerificationCols
 */
export enum OnpVerificationColPropEnum {
    /**
     * @description veId
     * @field veId
     */
    veId = "veId", //
        /**
         * @description veOrder
         * @field veOrder
         */
        veOrder = "veOrder", //
        /**
         * @description veColumn1
         * @field veColumn1
         */
        veColumn1 = "veColumn1", //
        /**
         * @description veColumn2
         * @field veColumn2
         */
        veColumn2 = "veColumn2", //
        /**
         * @description veColumn3
         * @field veColumn3
         */
        veColumn3 = "veColumn3", //
        /**
         * @description veColumn4
         * @field veColumn4
         */
        veColumn4 = "veColumn4", //
        /**
         * @description veColumn5
         * @field veColumn5
         */
        veColumn5 = "veColumn5", //
        /**
         * @description veColumn6
         * @field veColumn6
         */
        veColumn6 = "veColumn6", //
        /**
         * @description veColumn7
         * @field veColumn7
         */
        veColumn7 = "veColumn7", //
        /**
         * @description veColumn8
         * @field veColumn8
         */
        veColumn8 = "veColumn8", //
        /**
         * @description veColumn9
         * @field veColumn9
         */
        veColumn9 = "veColumn9", //
        /**
         * @description veSpecies
         * @field veSpecies
         */
        veSpecies = "veSpecies", //
        /**
         * @description veRemark
         * @field veRemark
         */
        veRemark = "veRemark", //
        /**
         * @description veCreateDate
         * @field veCreateDate
         */
        veCreateDate = "veCreateDate", //
        /**
         * @description veUpdateDate
         * @field veUpdateDate
         */
        veUpdateDate = "veUpdateDate", //
}
/**
 * ONP_VERIFICATION-验证表信息
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpVerificationTable
 */
export enum OnpVerificationTable {
    /**
     * @description VE_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY = 'VE_ID',
        /**
         * @description veId
         * @field primerKey
         */
        primerKey = 'veId',
        /**
         * @description ONP_VERIFICATION
         * @field TABLE_NAME
         */
        TABLE_NAME = 'ONP_VERIFICATION',
        /**
         * @description OnpVerification
         * @field ENTITY_NAME
         */
        ENTITY_NAME = 'OnpVerification',
}
/**
 * ONP_VERIFICATION-列名数组
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpVerificationColNames = Object.keys(OnpVerificationColNameEnum);
/**
 * ONP_VERIFICATION-列属性名数组
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpVerificationColProps = Object.keys(OnpVerificationColPropEnum);
/**
 * ONP_VERIFICATION-列名和列属性名映射表
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpVerificationColMap = { names: OnpVerificationColNames, props: OnpVerificationColProps };