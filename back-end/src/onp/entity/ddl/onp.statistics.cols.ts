/**
 * ONP_STATISTICS-ONP统计数据表表列名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatisticsColNames
 */
export interface OnpStatisticsColNames {
    /**
     * @description id
     * @field ST_ID
     */
    ST_ID: string, //
        /**
         * @description Bin
         * @field ST_BIN
         */
        ST_BIN: string, //
        /**
         * @description Chr
         * @field ST_CHR
         */
        ST_CHR: string, //
        /**
         * @description Start
         * @field ST_START
         */
        ST_START: string, //
        /**
         * @description Stop
         * @field ST_STOP
         */
        ST_STOP: string, //
        /**
         * @description Bin
         * @field ST_BIN_SIZE
         */
        ST_BIN_SIZE: string, //
        /**
         * @description Desc
         * @field ST_DESC
         */
        ST_DESC: string, //
        /**
         * @description ALL
         * @field ST_ALL_MARKERS
         */
        ST_ALL_MARKERS: string, //
        /**
         * @description SNP
         * @field ST_SNP_MARKERS
         */
        ST_SNP_MARKERS: string, //
        /**
         * @description INDEL
         * @field ST_INDEL_MARKERS
         */
        ST_INDEL_MARKERS: string, //
        /**
         * @description Block
         * @field ST_BLOCK_MARKERS
         */
        ST_BLOCK_MARKERS: string, //
        /**
         * @description Tags
         * @field ST_TAGS
         */
        ST_TAGS: string, //
        /**
         * @description Genotypes
         * @field ST_GENOTYPES
         */
        ST_GENOTYPES: string, //
        /**
         * @description Max
         * @field ST_MAX_GENOTYPES_FREQ
         */
        ST_MAX_GENOTYPES_FREQ: string, //
        /**
         * @description Min
         * @field ST_MIN_GENOTYPES_FREQ
         */
        ST_MIN_GENOTYPES_FREQ: string, //
        /**
         * @description PIC
         * @field ST_PIC
         */
        ST_PIC: string, //
        /**
         * @description Genetic
         * @field ST_GENETIC_MAP
         */
        ST_GENETIC_MAP: string, //
        /**
         * @description ORDER
         * @field ST_ORDER
         */
        ST_ORDER: number, //
        /**
         * @description Column1
         * @field ST_COLUMN1
         */
        ST_COLUMN1: string, //
        /**
         * @description Column2
         * @field ST_COLUMN2
         */
        ST_COLUMN2: string, //
        /**
         * @description Column3
         * @field ST_COLUMN3
         */
        ST_COLUMN3: string, //
        /**
         * @description Species
         * @field ST_SPECIES
         */
        ST_SPECIES: string, //
        /**
         * @description Remark
         * @field ST_REMARK
         */
        ST_REMARK: string, //
        /**
         * @description 创建日期
         * @field ST_CREATE_DATE
         */
        ST_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field ST_UPDATE_DATE
         */
        ST_UPDATE_DATE: Date, //
}
/**
 * ONP_STATISTICS-ONP统计数据表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatisticsColProps
 */
export interface OnpStatisticsColProps {
    /**
     * @description stId
     * @field stId
     */
    stId: string, //
        /**
         * @description stBin
         * @field stBin
         */
        stBin: string, //
        /**
         * @description stChr
         * @field stChr
         */
        stChr: string, //
        /**
         * @description stStart
         * @field stStart
         */
        stStart: string, //
        /**
         * @description stStop
         * @field stStop
         */
        stStop: string, //
        /**
         * @description stBinSize
         * @field stBinSize
         */
        stBinSize: string, //
        /**
         * @description stDesc
         * @field stDesc
         */
        stDesc: string, //
        /**
         * @description stAllMarkers
         * @field stAllMarkers
         */
        stAllMarkers: string, //
        /**
         * @description stSnpMarkers
         * @field stSnpMarkers
         */
        stSnpMarkers: string, //
        /**
         * @description stIndelMarkers
         * @field stIndelMarkers
         */
        stIndelMarkers: string, //
        /**
         * @description stBlockMarkers
         * @field stBlockMarkers
         */
        stBlockMarkers: string, //
        /**
         * @description stTags
         * @field stTags
         */
        stTags: string, //
        /**
         * @description stGenotypes
         * @field stGenotypes
         */
        stGenotypes: string, //
        /**
         * @description stMaxGenotypesFreq
         * @field stMaxGenotypesFreq
         */
        stMaxGenotypesFreq: string, //
        /**
         * @description stMinGenotypesFreq
         * @field stMinGenotypesFreq
         */
        stMinGenotypesFreq: string, //
        /**
         * @description stPic
         * @field stPic
         */
        stPic: string, //
        /**
         * @description stGeneticMap
         * @field stGeneticMap
         */
        stGeneticMap: string, //
        /**
         * @description stOrder
         * @field stOrder
         */
        stOrder: number, //
        /**
         * @description stColumn1
         * @field stColumn1
         */
        stColumn1: string, //
        /**
         * @description stColumn2
         * @field stColumn2
         */
        stColumn2: string, //
        /**
         * @description stColumn3
         * @field stColumn3
         */
        stColumn3: string, //
        /**
         * @description stSpecies
         * @field stSpecies
         */
        stSpecies: string, //
        /**
         * @description stRemark
         * @field stRemark
         */
        stRemark: string, //
        /**
         * @description stCreateDate
         * @field stCreateDate
         */
        stCreateDate: Date, //
        /**
         * @description stUpdateDate
         * @field stUpdateDate
         */
        stUpdateDate: Date, //
}
/**
 * ONP_STATISTICS-ONP统计数据表表列名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatisticsCols
 */
export enum OnpStatisticsColNameEnum {
    /**
     * @description id
     * @field ST_ID
     */
    ST_ID = "ST_ID", //
        /**
         * @description Bin
         * @field ST_BIN
         */
        ST_BIN = "ST_BIN", //
        /**
         * @description Chr
         * @field ST_CHR
         */
        ST_CHR = "ST_CHR", //
        /**
         * @description Start
         * @field ST_START
         */
        ST_START = "ST_START", //
        /**
         * @description Stop
         * @field ST_STOP
         */
        ST_STOP = "ST_STOP", //
        /**
         * @description Bin
         * @field ST_BIN_SIZE
         */
        ST_BIN_SIZE = "ST_BIN_SIZE", //
        /**
         * @description Desc
         * @field ST_DESC
         */
        ST_DESC = "ST_DESC", //
        /**
         * @description ALL
         * @field ST_ALL_MARKERS
         */
        ST_ALL_MARKERS = "ST_ALL_MARKERS", //
        /**
         * @description SNP
         * @field ST_SNP_MARKERS
         */
        ST_SNP_MARKERS = "ST_SNP_MARKERS", //
        /**
         * @description INDEL
         * @field ST_INDEL_MARKERS
         */
        ST_INDEL_MARKERS = "ST_INDEL_MARKERS", //
        /**
         * @description Block
         * @field ST_BLOCK_MARKERS
         */
        ST_BLOCK_MARKERS = "ST_BLOCK_MARKERS", //
        /**
         * @description Tags
         * @field ST_TAGS
         */
        ST_TAGS = "ST_TAGS", //
        /**
         * @description Genotypes
         * @field ST_GENOTYPES
         */
        ST_GENOTYPES = "ST_GENOTYPES", //
        /**
         * @description Max
         * @field ST_MAX_GENOTYPES_FREQ
         */
        ST_MAX_GENOTYPES_FREQ = "ST_MAX_GENOTYPES_FREQ", //
        /**
         * @description Min
         * @field ST_MIN_GENOTYPES_FREQ
         */
        ST_MIN_GENOTYPES_FREQ = "ST_MIN_GENOTYPES_FREQ", //
        /**
         * @description PIC
         * @field ST_PIC
         */
        ST_PIC = "ST_PIC", //
        /**
         * @description Genetic
         * @field ST_GENETIC_MAP
         */
        ST_GENETIC_MAP = "ST_GENETIC_MAP", //
        /**
         * @description ORDER
         * @field ST_ORDER
         */
        ST_ORDER = "ST_ORDER", //
        /**
         * @description Column1
         * @field ST_COLUMN1
         */
        ST_COLUMN1 = "ST_COLUMN1", //
        /**
         * @description Column2
         * @field ST_COLUMN2
         */
        ST_COLUMN2 = "ST_COLUMN2", //
        /**
         * @description Column3
         * @field ST_COLUMN3
         */
        ST_COLUMN3 = "ST_COLUMN3", //
        /**
         * @description Species
         * @field ST_SPECIES
         */
        ST_SPECIES = "ST_SPECIES", //
        /**
         * @description Remark
         * @field ST_REMARK
         */
        ST_REMARK = "ST_REMARK", //
        /**
         * @description 创建日期
         * @field ST_CREATE_DATE
         */
        ST_CREATE_DATE = "ST_CREATE_DATE", //
        /**
         * @description 更新日期
         * @field ST_UPDATE_DATE
         */
        ST_UPDATE_DATE = "ST_UPDATE_DATE", //
}
/**
 * ONP_STATISTICS-ONP统计数据表表列属性名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpStatisticsCols
 */
export enum OnpStatisticsColPropEnum {
    /**
     * @description stId
     * @field stId
     */
    stId = "stId", //
        /**
         * @description stBin
         * @field stBin
         */
        stBin = "stBin", //
        /**
         * @description stChr
         * @field stChr
         */
        stChr = "stChr", //
        /**
         * @description stStart
         * @field stStart
         */
        stStart = "stStart", //
        /**
         * @description stStop
         * @field stStop
         */
        stStop = "stStop", //
        /**
         * @description stBinSize
         * @field stBinSize
         */
        stBinSize = "stBinSize", //
        /**
         * @description stDesc
         * @field stDesc
         */
        stDesc = "stDesc", //
        /**
         * @description stAllMarkers
         * @field stAllMarkers
         */
        stAllMarkers = "stAllMarkers", //
        /**
         * @description stSnpMarkers
         * @field stSnpMarkers
         */
        stSnpMarkers = "stSnpMarkers", //
        /**
         * @description stIndelMarkers
         * @field stIndelMarkers
         */
        stIndelMarkers = "stIndelMarkers", //
        /**
         * @description stBlockMarkers
         * @field stBlockMarkers
         */
        stBlockMarkers = "stBlockMarkers", //
        /**
         * @description stTags
         * @field stTags
         */
        stTags = "stTags", //
        /**
         * @description stGenotypes
         * @field stGenotypes
         */
        stGenotypes = "stGenotypes", //
        /**
         * @description stMaxGenotypesFreq
         * @field stMaxGenotypesFreq
         */
        stMaxGenotypesFreq = "stMaxGenotypesFreq", //
        /**
         * @description stMinGenotypesFreq
         * @field stMinGenotypesFreq
         */
        stMinGenotypesFreq = "stMinGenotypesFreq", //
        /**
         * @description stPic
         * @field stPic
         */
        stPic = "stPic", //
        /**
         * @description stGeneticMap
         * @field stGeneticMap
         */
        stGeneticMap = "stGeneticMap", //
        /**
         * @description stOrder
         * @field stOrder
         */
        stOrder = "stOrder", //
        /**
         * @description stColumn1
         * @field stColumn1
         */
        stColumn1 = "stColumn1", //
        /**
         * @description stColumn2
         * @field stColumn2
         */
        stColumn2 = "stColumn2", //
        /**
         * @description stColumn3
         * @field stColumn3
         */
        stColumn3 = "stColumn3", //
        /**
         * @description stSpecies
         * @field stSpecies
         */
        stSpecies = "stSpecies", //
        /**
         * @description stRemark
         * @field stRemark
         */
        stRemark = "stRemark", //
        /**
         * @description stCreateDate
         * @field stCreateDate
         */
        stCreateDate = "stCreateDate", //
        /**
         * @description stUpdateDate
         * @field stUpdateDate
         */
        stUpdateDate = "stUpdateDate", //
}
/**
 * ONP_STATISTICS-ONP统计数据表表信息
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpStatisticsTable
 */
export enum OnpStatisticsTable {
    /**
     * @description ST_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY = 'ST_ID',
        /**
         * @description stId
         * @field primerKey
         */
        primerKey = 'stId',
        /**
         * @description ONP_STATISTICS
         * @field TABLE_NAME
         */
        TABLE_NAME = 'ONP_STATISTICS',
        /**
         * @description OnpStatistics
         * @field ENTITY_NAME
         */
        ENTITY_NAME = 'OnpStatistics',
}
/**
 * ONP_STATISTICS-列名数组
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpStatisticsColNames = Object.keys(OnpStatisticsColNameEnum);
/**
 * ONP_STATISTICS-列属性名数组
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpStatisticsColProps = Object.keys(OnpStatisticsColPropEnum);
/**
 * ONP_STATISTICS-列名和列属性名映射表
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpStatisticsColMap = { names: OnpStatisticsColNames, props: OnpStatisticsColProps };