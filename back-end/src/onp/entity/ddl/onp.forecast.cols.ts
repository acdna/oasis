/**
 * ONP_FORECAST-预测表列名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecastColNames
 */
export interface OnpForecastColNames {
    /**
     * @description id
     * @field FO_ID
     */
    FO_ID: string, //
        /**
         * @description ID
         * @field FO_ORDER
         */
        FO_ORDER: number, //
        /**
         * @description Column1
         * @field FO_COLUMN1
         */
        FO_COLUMN1: string, //
        /**
         * @description Column2
         * @field FO_COLUMN2
         */
        FO_COLUMN2: string, //
        /**
         * @description Column3
         * @field FO_COLUMN3
         */
        FO_COLUMN3: string, //
        /**
         * @description Column1
         * @field FO_COLUMN4
         */
        FO_COLUMN4: string, //
        /**
         * @description Column2
         * @field FO_COLUMN5
         */
        FO_COLUMN5: string, //
        /**
         * @description Column3
         * @field FO_COLUMN6
         */
        FO_COLUMN6: string, //
        /**
         * @description Species
         * @field FO_SPECIES
         */
        FO_SPECIES: string, //
        /**
         * @description Remark
         * @field FO_REMARK
         */
        FO_REMARK: string, //
        /**
         * @description 创建日期
         * @field FO_CREATE_DATE
         */
        FO_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field FO_UPDATE_DATE
         */
        FO_UPDATE_DATE: Date, //
}
/**
 * ONP_FORECAST-预测表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecastColProps
 */
export interface OnpForecastColProps {
    /**
     * @description foId
     * @field foId
     */
    foId: string, //
        /**
         * @description foOrder
         * @field foOrder
         */
        foOrder: number, //
        /**
         * @description foColumn1
         * @field foColumn1
         */
        foColumn1: string, //
        /**
         * @description foColumn2
         * @field foColumn2
         */
        foColumn2: string, //
        /**
         * @description foColumn3
         * @field foColumn3
         */
        foColumn3: string, //
        /**
         * @description foColumn4
         * @field foColumn4
         */
        foColumn4: string, //
        /**
         * @description foColumn5
         * @field foColumn5
         */
        foColumn5: string, //
        /**
         * @description foColumn6
         * @field foColumn6
         */
        foColumn6: string, //
        /**
         * @description foSpecies
         * @field foSpecies
         */
        foSpecies: string, //
        /**
         * @description foRemark
         * @field foRemark
         */
        foRemark: string, //
        /**
         * @description foCreateDate
         * @field foCreateDate
         */
        foCreateDate: Date, //
        /**
         * @description foUpdateDate
         * @field foUpdateDate
         */
        foUpdateDate: Date, //
}
/**
 * ONP_FORECAST-预测表列名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecastCols
 */
export enum OnpForecastColNameEnum {
    /**
     * @description id
     * @field FO_ID
     */
    FO_ID = "FO_ID", //
        /**
         * @description ID
         * @field FO_ORDER
         */
        FO_ORDER = "FO_ORDER", //
        /**
         * @description Column1
         * @field FO_COLUMN1
         */
        FO_COLUMN1 = "FO_COLUMN1", //
        /**
         * @description Column2
         * @field FO_COLUMN2
         */
        FO_COLUMN2 = "FO_COLUMN2", //
        /**
         * @description Column3
         * @field FO_COLUMN3
         */
        FO_COLUMN3 = "FO_COLUMN3", //
        /**
         * @description Column1
         * @field FO_COLUMN4
         */
        FO_COLUMN4 = "FO_COLUMN4", //
        /**
         * @description Column2
         * @field FO_COLUMN5
         */
        FO_COLUMN5 = "FO_COLUMN5", //
        /**
         * @description Column3
         * @field FO_COLUMN6
         */
        FO_COLUMN6 = "FO_COLUMN6", //
        /**
         * @description Species
         * @field FO_SPECIES
         */
        FO_SPECIES = "FO_SPECIES", //
        /**
         * @description Remark
         * @field FO_REMARK
         */
        FO_REMARK = "FO_REMARK", //
        /**
         * @description 创建日期
         * @field FO_CREATE_DATE
         */
        FO_CREATE_DATE = "FO_CREATE_DATE", //
        /**
         * @description 更新日期
         * @field FO_UPDATE_DATE
         */
        FO_UPDATE_DATE = "FO_UPDATE_DATE", //
}
/**
 * ONP_FORECAST-预测表列属性名
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @class OnpForecastCols
 */
export enum OnpForecastColPropEnum {
    /**
     * @description foId
     * @field foId
     */
    foId = "foId", //
        /**
         * @description foOrder
         * @field foOrder
         */
        foOrder = "foOrder", //
        /**
         * @description foColumn1
         * @field foColumn1
         */
        foColumn1 = "foColumn1", //
        /**
         * @description foColumn2
         * @field foColumn2
         */
        foColumn2 = "foColumn2", //
        /**
         * @description foColumn3
         * @field foColumn3
         */
        foColumn3 = "foColumn3", //
        /**
         * @description foColumn4
         * @field foColumn4
         */
        foColumn4 = "foColumn4", //
        /**
         * @description foColumn5
         * @field foColumn5
         */
        foColumn5 = "foColumn5", //
        /**
         * @description foColumn6
         * @field foColumn6
         */
        foColumn6 = "foColumn6", //
        /**
         * @description foSpecies
         * @field foSpecies
         */
        foSpecies = "foSpecies", //
        /**
         * @description foRemark
         * @field foRemark
         */
        foRemark = "foRemark", //
        /**
         * @description foCreateDate
         * @field foCreateDate
         */
        foCreateDate = "foCreateDate", //
        /**
         * @description foUpdateDate
         * @field foUpdateDate
         */
        foUpdateDate = "foUpdateDate", //
}
/**
 * ONP_FORECAST-预测表信息
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpForecastTable
 */
export enum OnpForecastTable {
    /**
     * @description FO_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY = 'FO_ID',
        /**
         * @description foId
         * @field primerKey
         */
        primerKey = 'foId',
        /**
         * @description ONP_FORECAST
         * @field TABLE_NAME
         */
        TABLE_NAME = 'ONP_FORECAST',
        /**
         * @description OnpForecast
         * @field ENTITY_NAME
         */
        ENTITY_NAME = 'OnpForecast',
}
/**
 * ONP_FORECAST-列名数组
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpForecastColNames = Object.keys(OnpForecastColNameEnum);
/**
 * ONP_FORECAST-列属性名数组
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpForecastColProps = Object.keys(OnpForecastColPropEnum);
/**
 * ONP_FORECAST-列名和列属性名映射表
 * @author jiangbin
 * @date 4/6/2021, 10:21:58 AM
 **/
export const OnpForecastColMap = { names: OnpForecastColNames, props: OnpForecastColProps };