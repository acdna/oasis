/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastOverallColNames
 */
export interface OnpChloroplastOverallColNames {
    /**
     * @description ID
     * @field OCO_ID
     */
    OCO_ID: string, //
        /**
         * @description ONP ID
         * @field OCO_ONP_ID
         */
        OCO_ONP_ID: string, //
        /**
         * @description Chr
         * @field OCO_CHR
         */
        OCO_CHR: string, //
        /**
         * @description Start
         * @field OCO_START
         */
        OCO_START: string, //
        /**
         * @description Stop
         * @field OCO_STOP
         */
        OCO_STOP: string, //
        /**
         * @description ONP
         * @field OCO_ONP_SIZE
         */
        OCO_ONP_SIZE: string, //
        /**
         * @description Location
         * @field OCO_LOCATION
         */
        OCO_LOCATION: string, //
        /**
         * @description ALL
         * @field OCO_ALL_MARKERS
         */
        OCO_ALL_MARKERS: string, //
        /**
         * @description SNP
         * @field OCO_SNP_MARKERS
         */
        OCO_SNP_MARKERS: string, //
        /**
         * @description INDEL
         * @field OCO_INDEL_MARKERS
         */
        OCO_INDEL_MARKERS: string, //
        /**
         * @description Block
         * @field OCO_BLOCK_MARKERS
         */
        OCO_BLOCK_MARKERS: string, //
        /**
         * @description Tags
         * @field OCO_TAGS
         */
        OCO_TAGS: string, //
        /**
         * @description Genotypes
         * @field OCO_GENOTYPES
         */
        OCO_GENOTYPES: string, //
        /**
         * @description Max
         * @field OCO_MAX_GENOTYPES_FREQ
         */
        OCO_MAX_GENOTYPES_FREQ: string, //
        /**
         * @description Min
         * @field OCO_MIN_GENOTYPES_FREQ
         */
        OCO_MIN_GENOTYPES_FREQ: string, //
        /**
         * @description PIC
         * @field OCO_PIC
         */
        OCO_PIC: string, //
        /**
         * @description Column1
         * @field OCO_COLUMN1
         */
        OCO_COLUMN1: string, //
        /**
         * @description Column2
         * @field OCO_COLUMN2
         */
        OCO_COLUMN2: string, //
        /**
         * @description Column3
         * @field OCO_COLUMN3
         */
        OCO_COLUMN3: string, //
        /**
         * @description ID
         * @field OCO_ORDER
         */
        OCO_ORDER: number, //
        /**
         * @description Species
         * @field OCO_SPECIES
         */
        OCO_SPECIES: string, //
        /**
         * @description Remark
         * @field OCO_REMARK
         */
        OCO_REMARK: string, //
        /**
         * @description 创建日期
         * @field OCO_CREATE_DATE
         */
        OCO_CREATE_DATE: Date, //
        /**
         * @description 更新日期
         * @field OCO_UPDATE_DATE
         */
        OCO_UPDATE_DATE: Date, //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastOverallColProps
 */
export interface OnpChloroplastOverallColProps {
    /**
     * @description ocoId
     * @field ocoId
     */
    ocoId: string, //
        /**
         * @description ocoOnpId
         * @field ocoOnpId
         */
        ocoOnpId: string, //
        /**
         * @description ocoChr
         * @field ocoChr
         */
        ocoChr: string, //
        /**
         * @description ocoStart
         * @field ocoStart
         */
        ocoStart: string, //
        /**
         * @description ocoStop
         * @field ocoStop
         */
        ocoStop: string, //
        /**
         * @description ocoOnpSize
         * @field ocoOnpSize
         */
        ocoOnpSize: string, //
        /**
         * @description ocoLocation
         * @field ocoLocation
         */
        ocoLocation: string, //
        /**
         * @description ocoAllMarkers
         * @field ocoAllMarkers
         */
        ocoAllMarkers: string, //
        /**
         * @description ocoSnpMarkers
         * @field ocoSnpMarkers
         */
        ocoSnpMarkers: string, //
        /**
         * @description ocoIndelMarkers
         * @field ocoIndelMarkers
         */
        ocoIndelMarkers: string, //
        /**
         * @description ocoBlockMarkers
         * @field ocoBlockMarkers
         */
        ocoBlockMarkers: string, //
        /**
         * @description ocoTags
         * @field ocoTags
         */
        ocoTags: string, //
        /**
         * @description ocoGenotypes
         * @field ocoGenotypes
         */
        ocoGenotypes: string, //
        /**
         * @description ocoMaxGenotypesFreq
         * @field ocoMaxGenotypesFreq
         */
        ocoMaxGenotypesFreq: string, //
        /**
         * @description ocoMinGenotypesFreq
         * @field ocoMinGenotypesFreq
         */
        ocoMinGenotypesFreq: string, //
        /**
         * @description ocoPic
         * @field ocoPic
         */
        ocoPic: string, //
        /**
         * @description ocoColumn1
         * @field ocoColumn1
         */
        ocoColumn1: string, //
        /**
         * @description ocoColumn2
         * @field ocoColumn2
         */
        ocoColumn2: string, //
        /**
         * @description ocoColumn3
         * @field ocoColumn3
         */
        ocoColumn3: string, //
        /**
         * @description ocoOrder
         * @field ocoOrder
         */
        ocoOrder: number, //
        /**
         * @description ocoSpecies
         * @field ocoSpecies
         */
        ocoSpecies: string, //
        /**
         * @description ocoRemark
         * @field ocoRemark
         */
        ocoRemark: string, //
        /**
         * @description ocoCreateDate
         * @field ocoCreateDate
         */
        ocoCreateDate: Date, //
        /**
         * @description ocoUpdateDate
         * @field ocoUpdateDate
         */
        ocoUpdateDate: Date, //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列名
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastOverallCols
 */
export enum OnpChloroplastOverallColNameEnum {
    /**
     * @description ID
     * @field OCO_ID
     */
    OCO_ID = "OCO_ID", //
        /**
         * @description ONP ID
         * @field OCO_ONP_ID
         */
        OCO_ONP_ID = "OCO_ONP_ID", //
        /**
         * @description Chr
         * @field OCO_CHR
         */
        OCO_CHR = "OCO_CHR", //
        /**
         * @description Start
         * @field OCO_START
         */
        OCO_START = "OCO_START", //
        /**
         * @description Stop
         * @field OCO_STOP
         */
        OCO_STOP = "OCO_STOP", //
        /**
         * @description ONP
         * @field OCO_ONP_SIZE
         */
        OCO_ONP_SIZE = "OCO_ONP_SIZE", //
        /**
         * @description Location
         * @field OCO_LOCATION
         */
        OCO_LOCATION = "OCO_LOCATION", //
        /**
         * @description ALL
         * @field OCO_ALL_MARKERS
         */
        OCO_ALL_MARKERS = "OCO_ALL_MARKERS", //
        /**
         * @description SNP
         * @field OCO_SNP_MARKERS
         */
        OCO_SNP_MARKERS = "OCO_SNP_MARKERS", //
        /**
         * @description INDEL
         * @field OCO_INDEL_MARKERS
         */
        OCO_INDEL_MARKERS = "OCO_INDEL_MARKERS", //
        /**
         * @description Block
         * @field OCO_BLOCK_MARKERS
         */
        OCO_BLOCK_MARKERS = "OCO_BLOCK_MARKERS", //
        /**
         * @description Tags
         * @field OCO_TAGS
         */
        OCO_TAGS = "OCO_TAGS", //
        /**
         * @description Genotypes
         * @field OCO_GENOTYPES
         */
        OCO_GENOTYPES = "OCO_GENOTYPES", //
        /**
         * @description Max
         * @field OCO_MAX_GENOTYPES_FREQ
         */
        OCO_MAX_GENOTYPES_FREQ = "OCO_MAX_GENOTYPES_FREQ", //
        /**
         * @description Min
         * @field OCO_MIN_GENOTYPES_FREQ
         */
        OCO_MIN_GENOTYPES_FREQ = "OCO_MIN_GENOTYPES_FREQ", //
        /**
         * @description PIC
         * @field OCO_PIC
         */
        OCO_PIC = "OCO_PIC", //
        /**
         * @description Column1
         * @field OCO_COLUMN1
         */
        OCO_COLUMN1 = "OCO_COLUMN1", //
        /**
         * @description Column2
         * @field OCO_COLUMN2
         */
        OCO_COLUMN2 = "OCO_COLUMN2", //
        /**
         * @description Column3
         * @field OCO_COLUMN3
         */
        OCO_COLUMN3 = "OCO_COLUMN3", //
        /**
         * @description ID
         * @field OCO_ORDER
         */
        OCO_ORDER = "OCO_ORDER", //
        /**
         * @description Species
         * @field OCO_SPECIES
         */
        OCO_SPECIES = "OCO_SPECIES", //
        /**
         * @description Remark
         * @field OCO_REMARK
         */
        OCO_REMARK = "OCO_REMARK", //
        /**
         * @description 创建日期
         * @field OCO_CREATE_DATE
         */
        OCO_CREATE_DATE = "OCO_CREATE_DATE", //
        /**
         * @description 更新日期
         * @field OCO_UPDATE_DATE
         */
        OCO_UPDATE_DATE = "OCO_UPDATE_DATE", //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表列属性名
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @class OnpChloroplastOverallCols
 */
export enum OnpChloroplastOverallColPropEnum {
    /**
     * @description ocoId
     * @field ocoId
     */
    ocoId = "ocoId", //
        /**
         * @description ocoOnpId
         * @field ocoOnpId
         */
        ocoOnpId = "ocoOnpId", //
        /**
         * @description ocoChr
         * @field ocoChr
         */
        ocoChr = "ocoChr", //
        /**
         * @description ocoStart
         * @field ocoStart
         */
        ocoStart = "ocoStart", //
        /**
         * @description ocoStop
         * @field ocoStop
         */
        ocoStop = "ocoStop", //
        /**
         * @description ocoOnpSize
         * @field ocoOnpSize
         */
        ocoOnpSize = "ocoOnpSize", //
        /**
         * @description ocoLocation
         * @field ocoLocation
         */
        ocoLocation = "ocoLocation", //
        /**
         * @description ocoAllMarkers
         * @field ocoAllMarkers
         */
        ocoAllMarkers = "ocoAllMarkers", //
        /**
         * @description ocoSnpMarkers
         * @field ocoSnpMarkers
         */
        ocoSnpMarkers = "ocoSnpMarkers", //
        /**
         * @description ocoIndelMarkers
         * @field ocoIndelMarkers
         */
        ocoIndelMarkers = "ocoIndelMarkers", //
        /**
         * @description ocoBlockMarkers
         * @field ocoBlockMarkers
         */
        ocoBlockMarkers = "ocoBlockMarkers", //
        /**
         * @description ocoTags
         * @field ocoTags
         */
        ocoTags = "ocoTags", //
        /**
         * @description ocoGenotypes
         * @field ocoGenotypes
         */
        ocoGenotypes = "ocoGenotypes", //
        /**
         * @description ocoMaxGenotypesFreq
         * @field ocoMaxGenotypesFreq
         */
        ocoMaxGenotypesFreq = "ocoMaxGenotypesFreq", //
        /**
         * @description ocoMinGenotypesFreq
         * @field ocoMinGenotypesFreq
         */
        ocoMinGenotypesFreq = "ocoMinGenotypesFreq", //
        /**
         * @description ocoPic
         * @field ocoPic
         */
        ocoPic = "ocoPic", //
        /**
         * @description ocoColumn1
         * @field ocoColumn1
         */
        ocoColumn1 = "ocoColumn1", //
        /**
         * @description ocoColumn2
         * @field ocoColumn2
         */
        ocoColumn2 = "ocoColumn2", //
        /**
         * @description ocoColumn3
         * @field ocoColumn3
         */
        ocoColumn3 = "ocoColumn3", //
        /**
         * @description ocoOrder
         * @field ocoOrder
         */
        ocoOrder = "ocoOrder", //
        /**
         * @description ocoSpecies
         * @field ocoSpecies
         */
        ocoSpecies = "ocoSpecies", //
        /**
         * @description ocoRemark
         * @field ocoRemark
         */
        ocoRemark = "ocoRemark", //
        /**
         * @description ocoCreateDate
         * @field ocoCreateDate
         */
        ocoCreateDate = "ocoCreateDate", //
        /**
         * @description ocoUpdateDate
         * @field ocoUpdateDate
         */
        ocoUpdateDate = "ocoUpdateDate", //
}
/**
 * ONP_CHLOROPLAST_OVERALL-ONP_CHLOROPLAST_OVERALL表信息
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastOverallTable
 */
export enum OnpChloroplastOverallTable {
    /**
     * @description OCO_ID
     * @field PRIMER_KEY
     */
    PRIMER_KEY = 'OCO_ID',
        /**
         * @description ocoId
         * @field primerKey
         */
        primerKey = 'ocoId',
        /**
         * @description ONP_CHLOROPLAST_OVERALL
         * @field TABLE_NAME
         */
        TABLE_NAME = 'ONP_CHLOROPLAST_OVERALL',
        /**
         * @description OnpChloroplastOverall
         * @field ENTITY_NAME
         */
        ENTITY_NAME = 'OnpChloroplastOverall',
}
/**
 * ONP_CHLOROPLAST_OVERALL-列名数组
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const OnpChloroplastOverallColNames = Object.keys(OnpChloroplastOverallColNameEnum);
/**
 * ONP_CHLOROPLAST_OVERALL-列属性名数组
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const OnpChloroplastOverallColProps = Object.keys(OnpChloroplastOverallColPropEnum);
/**
 * ONP_CHLOROPLAST_OVERALL-列名和列属性名映射表
 * @author jiangbin
 * @date 4/29/2021, 1:57:37 PM
 **/
export const OnpChloroplastOverallColMap = { names: OnpChloroplastOverallColNames, props: OnpChloroplastOverallColProps };