import { Entity, Column } from "typeorm";
import { Max, IsNotEmpty } from "class-validator";
import { Type } from 'class-transformer';
/**
 * 验证
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpVerification
 */
@Entity({
    name: 'ONP_VERIFICATION'
})
export class OnpVerification {
    /**
     * id-主键
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_ID', type: 'varchar', length: '255', primary: true })
    @IsNotEmpty({ message: '【id】不能为空' })
    @Max(255, { message: '【id】长度不能超过255' })
    veId: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpVerification
     */
    @Type(() => Number)
    @Column({ name: 'VE_ORDER', type: 'int' })
    veOrder: number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN1', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column1】长度不能超过255' })
    veColumn1: string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN2', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column2】长度不能超过255' })
    veColumn2: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN3', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    veColumn3: string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN4', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column1】长度不能超过255' })
    veColumn4: string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN5', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column2】长度不能超过255' })
    veColumn5: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN6', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    veColumn6: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN7', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    veColumn7: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN8', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    veColumn8: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_COLUMN9', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    veColumn9: string;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_SPECIES', type: 'varchar', length: '255' })
    @Max(255, { message: '【Species】长度不能超过255' })
    veSpecies: string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpVerification
     */
    @Column({ name: 'VE_REMARK', type: 'varchar', length: '128' })
    @Max(128, { message: '【Remark】长度不能超过128' })
    veRemark: string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpVerification
     */
    @Type(() => Date)
    @Column({ name: 'VE_CREATE_DATE', type: 'datetime' })
    veCreateDate: Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpVerification
     */
    @Type(() => Date)
    @Column({ name: 'VE_UPDATE_DATE', type: 'datetime' })
    veUpdateDate: Date;
}
//声明类型定义
export declare type OnpVerificationType = OnpVerification | Partial < OnpVerification > ;