import { OnpChloroplastHaplotypeQDto } from '../../dto/query/onp.chloroplast.haplotype.qdto';
import { OnpChloroplastHaplotypeCDto } from '../../dto/create/onp.chloroplast.haplotype.cdto';
import { OnpChloroplastHaplotypeUDto } from '../../dto/update/onp.chloroplast.haplotype.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { OnpChloroplastHaplotypeColNameEnum, OnpChloroplastHaplotypeColPropEnum, OnpChloroplastHaplotypeTable, OnpChloroplastHaplotypeColNames, OnpChloroplastHaplotypeColProps } from '../ddl/onp.chloroplast.haplotype.cols';
/**
 * ONP_CHLOROPLAST_HAPLOTYPE--ONP_CHLOROPLAST_HAPLOTYPE表相关SQL定义
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastHaplotypeSQL
 */
export const OnpChloroplastHaplotypeSQL = {
    /**
     * 构建根据有值属性查询SQL
     * @param dto
     * @return {string}
     */
    SELECT_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let cols = OnpChloroplastHaplotypeSQL.COL_MAPPER_SQL(dto);
        let where = OnpChloroplastHaplotypeSQL.WHERE_SQL(dto);
        let group = OnpChloroplastHaplotypeSQL.GROUP_BY_SQL(dto);
        let order = OnpChloroplastHaplotypeSQL.ORDER_BY_SQL(dto);
        let limit = OnpChloroplastHaplotypeSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询记录数SQL
     * @param dto
     * @return {string}
     */
    SELECT_COUNT_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let where = OnpChloroplastHaplotypeSQL.WHERE_SQL(dto);
        let group = OnpChloroplastHaplotypeSQL.GROUP_BY_SQL(dto);
        return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
    },
    /**
     * 根据ID查询记录SQL
     * @param dto
     * @return {string}
     */
    SELECT_BY_ID_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let cols = OnpChloroplastHaplotypeSQL.COL_MAPPER_SQL(dto);
        let group = OnpChloroplastHaplotypeSQL.GROUP_BY_SQL(dto);
        let order = OnpChloroplastHaplotypeSQL.ORDER_BY_SQL(dto);
        let limit = OnpChloroplastHaplotypeSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
    },
    /**
     * 根据ID列表查询记录SQL
     * @param idList
     * @return {string}
     */
    SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
        if (!idList || idList.length == 0) return '';
        let sql = OnpChloroplastHaplotypeSQL.COL_MAPPER_SQL({ cols });
        return `SELECT ${sql} FROM ${TABLE} WHERE ${ OnpChloroplastHaplotypeSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询数据库中已存在ID的SQL语句
     * @param idList
     * @return {string}
     */
    SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${ OnpChloroplastHaplotypeSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询所有ID列表SQL语句
     * @return {string}
     */
    SELECT_ALL_ID_LIST_SQL: (): string => {
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
    },
    /**
     * 条件查询ID列表SQL语句
     * @param dto
     * @return {string}
     */
    SELECT_ID_LIST_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let where = OnpChloroplastHaplotypeSQL.WHERE_SQL(dto);
        let group = OnpChloroplastHaplotypeSQL.GROUP_BY_SQL(dto);
        let order = OnpChloroplastHaplotypeSQL.ORDER_BY_SQL(dto);
        let limit = OnpChloroplastHaplotypeSQL.LIMIT_SQL(dto);
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询结果列映射SQL
     * @param dto
     * @return {string}
     */
    COL_MAPPER_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let cols = StringUtils.toColName(dto.cols, OnpChloroplastHaplotypeColNames, OnpChloroplastHaplotypeColProps);
        if (!cols || cols.length == 0) cols = OnpChloroplastHaplotypeColNames;
        let sql = cols.map(col => `${ col } as ${StringUtils.camelCase(col)}`).join(',');
        return dto.distinct ? `DISTINCT ${sql}` : sql;
    },
    /**
     * Where语句，根据有值属性查询
     * @param dto
     * @return {string}
     */
    WHERE_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[ochId]?.length > 0) items.push(` ${ OCH_ID } = '${dto[ochId]}'`);
        if (dto['nochId']?.length > 0) items.push(` ${ OCH_ID } != '${dto['nochId']}'`);
        if (dto[ochChloroplastHaplotype]?.length > 0) items.push(` ${ OCH_CHLOROPLAST_HAPLOTYPE } = '${dto[ochChloroplastHaplotype]}'`);
        if (dto['nochChloroplastHaplotype']?.length > 0) items.push(` ${ OCH_CHLOROPLAST_HAPLOTYPE } != '${dto['nochChloroplastHaplotype']}'`);
        if (dto[ochChr]?.length > 0) items.push(` ${ OCH_CHR } = '${dto[ochChr]}'`);
        if (dto['nochChr']?.length > 0) items.push(` ${ OCH_CHR } != '${dto['nochChr']}'`);
        if (dto[ochChloroplastOnp]?.length > 0) items.push(` ${ OCH_CHLOROPLAST_ONP } = '${dto[ochChloroplastOnp]}'`);
        if (dto['nochChloroplastOnp']?.length > 0) items.push(` ${ OCH_CHLOROPLAST_ONP } != '${dto['nochChloroplastOnp']}'`);
        if (dto[ochChloroplastHaplotypeSequence]?.length > 0) items.push(` ${ OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE } = '${dto[ochChloroplastHaplotypeSequence]}'`);
        if (dto['nochChloroplastHaplotypeSequence']?.length > 0) items.push(` ${ OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE } != '${dto['nochChloroplastHaplotypeSequence']}'`);
        if (dto[ochFrequency]?.length > 0) items.push(` ${ OCH_FREQUENCY } = '${dto[ochFrequency]}'`);
        if (dto['nochFrequency']?.length > 0) items.push(` ${ OCH_FREQUENCY } != '${dto['nochFrequency']}'`);
        if (dto[ochColumn1]?.length > 0) items.push(` ${ OCH_COLUMN1 } = '${dto[ochColumn1]}'`);
        if (dto['nochColumn1']?.length > 0) items.push(` ${ OCH_COLUMN1 } != '${dto['nochColumn1']}'`);
        if (dto[ochColumn2]?.length > 0) items.push(` ${ OCH_COLUMN2 } = '${dto[ochColumn2]}'`);
        if (dto['nochColumn2']?.length > 0) items.push(` ${ OCH_COLUMN2 } != '${dto['nochColumn2']}'`);
        if (dto[ochColumn3]?.length > 0) items.push(` ${ OCH_COLUMN3 } = '${dto[ochColumn3]}'`);
        if (dto['nochColumn3']?.length > 0) items.push(` ${ OCH_COLUMN3 } != '${dto['nochColumn3']}'`);
        if (dto[ochOrder]) items.push(` ${ OCH_ORDER } = '${dto[ochOrder]}'`);
        if (dto['nochOrder']) items.push(` ${ OCH_ORDER } != '${dto['nochOrder']}'`);
        if (dto[ochSpecies]?.length > 0) items.push(` ${ OCH_SPECIES } = '${dto[ochSpecies]}'`);
        if (dto['nochSpecies']?.length > 0) items.push(` ${ OCH_SPECIES } != '${dto['nochSpecies']}'`);
        if (dto[ochRemark]?.length > 0) items.push(` ${ OCH_REMARK } = '${dto[ochRemark]}'`);
        if (dto['nochRemark']?.length > 0) items.push(` ${ OCH_REMARK } != '${dto['nochRemark']}'`);
        if (dto[ochCreateDate]) items.push(`${ OCH_CREATE_DATE } is not null and date_format(${ OCH_CREATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[ochCreateDate])}`);
        if (dto['nochCreateDate']) items.push(`${ OCH_CREATE_DATE } is not null and date_format(${ OCH_CREATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nochCreateDate'])}`);
        if (dto[ochUpdateDate]) items.push(`${ OCH_UPDATE_DATE } is not null and date_format(${ OCH_UPDATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[ochUpdateDate])}`);
        if (dto['nochUpdateDate']) items.push(`${ OCH_UPDATE_DATE } is not null and date_format(${ OCH_UPDATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nochUpdateDate'])}`);
        //数组、模糊条件、年份日期范围等额外查询条件
        //OCH_ID--字符串字段
        if (dto['ochIdList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_ID, < string[] > dto['ochIdList']));
        if (dto['nochIdList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_ID, < string[] > dto['nochIdList'], true));
        if (dto['ochIdLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_ID, dto['ochIdLike'])); //For MysSQL
        if (dto['nochIdLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_ID, dto['nochIdLike'], true)); //For MysSQL
        if (dto['ochIdLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_ID, < string[] > dto['ochIdLikeList'])); //For MysSQL
        //OCH_CHLOROPLAST_HAPLOTYPE--字符串字段
        if (dto['ochChloroplastHaplotypeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHLOROPLAST_HAPLOTYPE, < string[] > dto['ochChloroplastHaplotypeList']));
        if (dto['nochChloroplastHaplotypeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHLOROPLAST_HAPLOTYPE, < string[] > dto['nochChloroplastHaplotypeList'], true));
        if (dto['ochChloroplastHaplotypeLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHLOROPLAST_HAPLOTYPE, dto['ochChloroplastHaplotypeLike'])); //For MysSQL
        if (dto['nochChloroplastHaplotypeLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHLOROPLAST_HAPLOTYPE, dto['nochChloroplastHaplotypeLike'], true)); //For MysSQL
        if (dto['ochChloroplastHaplotypeLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_CHLOROPLAST_HAPLOTYPE, < string[] > dto['ochChloroplastHaplotypeLikeList'])); //For MysSQL
        //OCH_CHR--字符串字段
        if (dto['ochChrList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHR, < string[] > dto['ochChrList']));
        if (dto['nochChrList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHR, < string[] > dto['nochChrList'], true));
        if (dto['ochChrLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHR, dto['ochChrLike'])); //For MysSQL
        if (dto['nochChrLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHR, dto['nochChrLike'], true)); //For MysSQL
        if (dto['ochChrLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_CHR, < string[] > dto['ochChrLikeList'])); //For MysSQL
        //OCH_CHLOROPLAST_ONP--字符串字段
        if (dto['ochChloroplastOnpList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHLOROPLAST_ONP, < string[] > dto['ochChloroplastOnpList']));
        if (dto['nochChloroplastOnpList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHLOROPLAST_ONP, < string[] > dto['nochChloroplastOnpList'], true));
        if (dto['ochChloroplastOnpLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHLOROPLAST_ONP, dto['ochChloroplastOnpLike'])); //For MysSQL
        if (dto['nochChloroplastOnpLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHLOROPLAST_ONP, dto['nochChloroplastOnpLike'], true)); //For MysSQL
        if (dto['ochChloroplastOnpLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_CHLOROPLAST_ONP, < string[] > dto['ochChloroplastOnpLikeList'])); //For MysSQL
        //OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE--字符串字段
        if (dto['ochChloroplastHaplotypeSequenceList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE, < string[] > dto['ochChloroplastHaplotypeSequenceList']));
        if (dto['nochChloroplastHaplotypeSequenceList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE, < string[] > dto['nochChloroplastHaplotypeSequenceList'], true));
        if (dto['ochChloroplastHaplotypeSequenceLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE, dto['ochChloroplastHaplotypeSequenceLike'])); //For MysSQL
        if (dto['nochChloroplastHaplotypeSequenceLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE, dto['nochChloroplastHaplotypeSequenceLike'], true)); //For MysSQL
        if (dto['ochChloroplastHaplotypeSequenceLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE, < string[] > dto['ochChloroplastHaplotypeSequenceLikeList'])); //For MysSQL
        //OCH_FREQUENCY--字符串字段
        if (dto['ochFrequencyList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_FREQUENCY, < string[] > dto['ochFrequencyList']));
        if (dto['nochFrequencyList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_FREQUENCY, < string[] > dto['nochFrequencyList'], true));
        if (dto['ochFrequencyLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_FREQUENCY, dto['ochFrequencyLike'])); //For MysSQL
        if (dto['nochFrequencyLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_FREQUENCY, dto['nochFrequencyLike'], true)); //For MysSQL
        if (dto['ochFrequencyLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_FREQUENCY, < string[] > dto['ochFrequencyLikeList'])); //For MysSQL
        //OCH_COLUMN1--字符串字段
        if (dto['ochColumn1List']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_COLUMN1, < string[] > dto['ochColumn1List']));
        if (dto['nochColumn1List']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_COLUMN1, < string[] > dto['nochColumn1List'], true));
        if (dto['ochColumn1Like']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_COLUMN1, dto['ochColumn1Like'])); //For MysSQL
        if (dto['nochColumn1Like']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_COLUMN1, dto['nochColumn1Like'], true)); //For MysSQL
        if (dto['ochColumn1LikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_COLUMN1, < string[] > dto['ochColumn1LikeList'])); //For MysSQL
        //OCH_COLUMN2--字符串字段
        if (dto['ochColumn2List']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_COLUMN2, < string[] > dto['ochColumn2List']));
        if (dto['nochColumn2List']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_COLUMN2, < string[] > dto['nochColumn2List'], true));
        if (dto['ochColumn2Like']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_COLUMN2, dto['ochColumn2Like'])); //For MysSQL
        if (dto['nochColumn2Like']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_COLUMN2, dto['nochColumn2Like'], true)); //For MysSQL
        if (dto['ochColumn2LikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_COLUMN2, < string[] > dto['ochColumn2LikeList'])); //For MysSQL
        //OCH_COLUMN3--字符串字段
        if (dto['ochColumn3List']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_COLUMN3, < string[] > dto['ochColumn3List']));
        if (dto['nochColumn3List']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_COLUMN3, < string[] > dto['nochColumn3List'], true));
        if (dto['ochColumn3Like']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_COLUMN3, dto['ochColumn3Like'])); //For MysSQL
        if (dto['nochColumn3Like']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_COLUMN3, dto['nochColumn3Like'], true)); //For MysSQL
        if (dto['ochColumn3LikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_COLUMN3, < string[] > dto['ochColumn3LikeList'])); //For MysSQL
        //OCH_ORDER--数字字段
        if (dto['ochOrderList']) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_ORDER, dto['ochOrderList']));
        if (dto['nochOrderList']) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_ORDER, dto['nochOrderList'], true));
        //OCH_SPECIES--字符串字段
        if (dto['ochSpeciesList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_SPECIES, < string[] > dto['ochSpeciesList']));
        if (dto['nochSpeciesList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_SPECIES, < string[] > dto['nochSpeciesList'], true));
        if (dto['ochSpeciesLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_SPECIES, dto['ochSpeciesLike'])); //For MysSQL
        if (dto['nochSpeciesLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_SPECIES, dto['nochSpeciesLike'], true)); //For MysSQL
        if (dto['ochSpeciesLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_SPECIES, < string[] > dto['ochSpeciesLikeList'])); //For MysSQL
        //OCH_REMARK--字符串字段
        if (dto['ochRemarkList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_REMARK, < string[] > dto['ochRemarkList']));
        if (dto['nochRemarkList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.IN_SQL(OCH_REMARK, < string[] > dto['nochRemarkList'], true));
        if (dto['ochRemarkLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_REMARK, dto['ochRemarkLike'])); //For MysSQL
        if (dto['nochRemarkLike']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_SQL(OCH_REMARK, dto['nochRemarkLike'], true)); //For MysSQL
        if (dto['ochRemarkLikeList']?.length > 0) items.push(OnpChloroplastHaplotypeSQL.LIKE_LIST_SQL(OCH_REMARK, < string[] > dto['ochRemarkLikeList'])); //For MysSQL
        //OCH_CREATE_DATE--日期字段
        if (dto[ochCreateDate]) items.push(`${ OCH_CREATE_DATE } is not null and date_format(${ OCH_CREATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sOchCreateDate'])}`);
        if (dto[ochCreateDate]) items.push(`${ OCH_CREATE_DATE } is not null and date_format(${ OCH_CREATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eOchCreateDate'])}`);
        //OCH_UPDATE_DATE--日期字段
        if (dto[ochUpdateDate]) items.push(`${ OCH_UPDATE_DATE } is not null and date_format(${ OCH_UPDATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sOchUpdateDate'])}`);
        if (dto[ochUpdateDate]) items.push(`${ OCH_UPDATE_DATE } is not null and date_format(${ OCH_UPDATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eOchUpdateDate'])}`);
        return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
    },
    /**
     * 字符串的Like语句，采用前后模糊查询
     * @param colName 列名
     * @param values 值
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
        if (!colName || value.length == 0 || !value || value.length == 0) return '';
        if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
        else return ` ${colName} like CONCAT('%','${value}','%')`;
    },
    /**
     * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
     * @param colName 列名
     * @param values 值列表
     * @param andOr 采用and或or连接多个条件，默认为OR语句
     * @return {string}
     */
    LIKE_LIST_SQL: (colName: string, values: string[], andOr ? : string): string => {
        if (!colName || !values || values.length == 0) return '';
        if (!andOr) andOr = 'OR';
        let items = values.map(value => OnpChloroplastHaplotypeSQL.LIKE_SQL(colName, value));
        return `( ${items.join(' ' + andOr + ' ')} )`;
    },
    /**
     * 排序查询SQL语句
     * @param dto
     * @return {string}
     */
    ORDER_BY_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        if (dto['order']?.length > 0) {
            return `order by ${dto['order']}`;
        }
        return '';
    },
    /**
     * 分组查询SQL语句
     * @param dto
     * @return {string}
     */
    GROUP_BY_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        if (dto['group']?.length > 0) {
            return `group by ${dto['group']}`;
        }
        return '';
    },
    /**
     * 分页查询条件SQL语句
     * @param dto
     * @return {string}
     */
    LIMIT_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
            return `limit ${dto['offset']},${dto['pageSize']}`;
        }
        return '';
    },
    /**
     * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
     * @param colName 列名，如:SAM_NAME
     * @param values 数组
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
        if (!values || values.length == 0) return '';
        let items = [];
        Array.isArray(values) ? values.forEach(value => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
        return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
    },
    /**
     * 插入记录SQL语句
     * @param dto
     * @return {string}
     */
    INSERT_SQL: (dto: OnpChloroplastHaplotypeCDto | Partial < OnpChloroplastHaplotypeCDto > ): string => {
        if (!dto) return '';
        if (!dto[primerKey] || dto[primerKey] == undefined) return '';
        let cols = [];
        let values = [];
        cols.push(OCH_ID);
        cols.push(OCH_CHLOROPLAST_HAPLOTYPE);
        cols.push(OCH_CHR);
        cols.push(OCH_CHLOROPLAST_ONP);
        cols.push(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE);
        cols.push(OCH_FREQUENCY);
        cols.push(OCH_COLUMN1);
        cols.push(OCH_COLUMN2);
        cols.push(OCH_COLUMN3);
        cols.push(OCH_ORDER);
        cols.push(OCH_SPECIES);
        cols.push(OCH_REMARK);
        cols.push(OCH_CREATE_DATE);
        cols.push(OCH_UPDATE_DATE);
        values.push(toSqlValue(dto[ochId]));
        values.push(toSqlValue(dto[ochChloroplastHaplotype]));
        values.push(toSqlValue(dto[ochChr]));
        values.push(toSqlValue(dto[ochChloroplastOnp]));
        values.push(toSqlValue(dto[ochChloroplastHaplotypeSequence]));
        values.push(toSqlValue(dto[ochFrequency]));
        values.push(toSqlValue(dto[ochColumn1]));
        values.push(toSqlValue(dto[ochColumn2]));
        values.push(toSqlValue(dto[ochColumn3]));
        values.push(toSqlValue(dto[ochOrder]));
        values.push(toSqlValue(dto[ochSpecies]));
        values.push(toSqlValue(dto[ochRemark]));
        values.push('NOW()');
        values.push('NOW()');
        let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
        return sql;
    },
    /**
     * 批量插入SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_INSERT_SQL: (dtos: OnpChloroplastHaplotypeCDto[] | Partial < OnpChloroplastHaplotypeCDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let cols = [];
        cols.push(OCH_ID);
        cols.push(OCH_CHLOROPLAST_HAPLOTYPE);
        cols.push(OCH_CHR);
        cols.push(OCH_CHLOROPLAST_ONP);
        cols.push(OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE);
        cols.push(OCH_FREQUENCY);
        cols.push(OCH_COLUMN1);
        cols.push(OCH_COLUMN2);
        cols.push(OCH_COLUMN3);
        cols.push(OCH_ORDER);
        cols.push(OCH_SPECIES);
        cols.push(OCH_REMARK);
        cols.push(OCH_CREATE_DATE);
        cols.push(OCH_UPDATE_DATE);
        let items = [];
        dtos.forEach(dto => {
            let values = [];
            values.push(toSqlValue(dto[ochId]));
            values.push(toSqlValue(dto[ochChloroplastHaplotype]));
            values.push(toSqlValue(dto[ochChr]));
            values.push(toSqlValue(dto[ochChloroplastOnp]));
            values.push(toSqlValue(dto[ochChloroplastHaplotypeSequence]));
            values.push(toSqlValue(dto[ochFrequency]));
            values.push(toSqlValue(dto[ochColumn1]));
            values.push(toSqlValue(dto[ochColumn2]));
            values.push(toSqlValue(dto[ochColumn3]));
            values.push(toSqlValue(dto[ochOrder]));
            values.push(toSqlValue(dto[ochSpecies]));
            values.push(toSqlValue(dto[ochRemark]));
            values.push('NOW()');
            values.push('NOW()');
            items.push(`(${values.join(',')})`);
        });
        return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
    },
    /**
     * 根据ID更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SQL: (dto: OnpChloroplastHaplotypeUDto | Partial < OnpChloroplastHaplotypeUDto > ): string => {
        if (!dto) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpChloroplastHaplotypeColNames, OnpChloroplastHaplotypeColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ OCH_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ OCH_CHLOROPLAST_HAPLOTYPE } = ${toSqlValue(dto[ochChloroplastHaplotype])}`);
        items.push(`${ OCH_CHR } = ${toSqlValue(dto[ochChr])}`);
        items.push(`${ OCH_CHLOROPLAST_ONP } = ${toSqlValue(dto[ochChloroplastOnp])}`);
        items.push(`${ OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE } = ${toSqlValue(dto[ochChloroplastHaplotypeSequence])}`);
        items.push(`${ OCH_FREQUENCY } = ${toSqlValue(dto[ochFrequency])}`);
        items.push(`${ OCH_COLUMN1 } = ${toSqlValue(dto[ochColumn1])}`);
        items.push(`${ OCH_COLUMN2 } = ${toSqlValue(dto[ochColumn2])}`);
        items.push(`${ OCH_COLUMN3 } = ${toSqlValue(dto[ochColumn3])}`);
        items.push(`${ OCH_ORDER } = ${toSqlValue(dto[ochOrder])}`);
        items.push(`${ OCH_SPECIES } = ${toSqlValue(dto[ochSpecies])}`);
        items.push(`${ OCH_REMARK } = ${toSqlValue(dto[ochRemark])}`);
        items.push(`${ OCH_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID列表批量更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_IDS_SQL: (dto: OnpChloroplastHaplotypeUDto | Partial < OnpChloroplastHaplotypeUDto > ): string => {
        if (!dto) return '';
        //获取ID列表
        let ids = < string[] > dto[`${primerKey}List`];
        if (!ids || ids.length == 0) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpChloroplastHaplotypeColNames, OnpChloroplastHaplotypeColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ OCH_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpChloroplastHaplotypeSQL.IN_SQL(PRIMER_KEY, ids)}`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ OCH_CHLOROPLAST_HAPLOTYPE } = ${toSqlValue(dto[ochChloroplastHaplotype])}`);
        items.push(`${ OCH_CHR } = ${toSqlValue(dto[ochChr])}`);
        items.push(`${ OCH_CHLOROPLAST_ONP } = ${toSqlValue(dto[ochChloroplastOnp])}`);
        items.push(`${ OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE } = ${toSqlValue(dto[ochChloroplastHaplotypeSequence])}`);
        items.push(`${ OCH_FREQUENCY } = ${toSqlValue(dto[ochFrequency])}`);
        items.push(`${ OCH_COLUMN1 } = ${toSqlValue(dto[ochColumn1])}`);
        items.push(`${ OCH_COLUMN2 } = ${toSqlValue(dto[ochColumn2])}`);
        items.push(`${ OCH_COLUMN3 } = ${toSqlValue(dto[ochColumn3])}`);
        items.push(`${ OCH_ORDER } = ${toSqlValue(dto[ochOrder])}`);
        items.push(`${ OCH_SPECIES } = ${toSqlValue(dto[ochSpecies])}`);
        items.push(`${ OCH_REMARK } = ${toSqlValue(dto[ochRemark])}`);
        items.push(`${ OCH_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpChloroplastHaplotypeSQL.IN_SQL(PRIMER_KEY, ids)}`;
    },
    /**
     * 根据ID更新所有属性值的批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SQL: (dtos: OnpChloroplastHaplotypeUDto[] | Partial < OnpChloroplastHaplotypeUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let sqls = [];
        dtos.forEach(value => {
            sqls.push(OnpChloroplastHaplotypeSQL.UPDATE_BY_ID_SQL(value));
        });
        return sqls.join(';');
    },
    /**
     * 根据ID更新有值属性值的条件更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SELECTIVE_SQL: (dto: OnpChloroplastHaplotypeUDto | Partial < OnpChloroplastHaplotypeUDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[ochChloroplastHaplotype]?.length > 0) items.push(`${ OCH_CHLOROPLAST_HAPLOTYPE } = ${toSqlValue(dto[ochChloroplastHaplotype])}`);
        if (dto[ochChr]?.length > 0) items.push(`${ OCH_CHR } = ${toSqlValue(dto[ochChr])}`);
        if (dto[ochChloroplastOnp]?.length > 0) items.push(`${ OCH_CHLOROPLAST_ONP } = ${toSqlValue(dto[ochChloroplastOnp])}`);
        if (dto[ochChloroplastHaplotypeSequence]?.length > 0) items.push(`${ OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE } = ${toSqlValue(dto[ochChloroplastHaplotypeSequence])}`);
        if (dto[ochFrequency]?.length > 0) items.push(`${ OCH_FREQUENCY } = ${toSqlValue(dto[ochFrequency])}`);
        if (dto[ochColumn1]?.length > 0) items.push(`${ OCH_COLUMN1 } = ${toSqlValue(dto[ochColumn1])}`);
        if (dto[ochColumn2]?.length > 0) items.push(`${ OCH_COLUMN2 } = ${toSqlValue(dto[ochColumn2])}`);
        if (dto[ochColumn3]?.length > 0) items.push(`${ OCH_COLUMN3 } = ${toSqlValue(dto[ochColumn3])}`);
        if (dto[ochOrder]) items.push(`${ OCH_ORDER } = ${toSqlValue(dto[ochOrder])}`);
        if (dto[ochSpecies]?.length > 0) items.push(`${ OCH_SPECIES } = ${toSqlValue(dto[ochSpecies])}`);
        if (dto[ochRemark]?.length > 0) items.push(`${ OCH_REMARK } = ${toSqlValue(dto[ochRemark])}`);
        if (dto[ochUpdateDate]) items.push(`${  OCH_UPDATE_DATE }= NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID更新有值属性值的条件批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: OnpChloroplastHaplotypeUDto[] | Partial < OnpChloroplastHaplotypeUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let items = [];
        dtos.forEach(value => items.push(OnpChloroplastHaplotypeSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
        return items.join(';');
    },
    /**
     * 条件删除SQL语句
     * @param dtos
     * @return {string}
     */
    DELETE_SELECTIVE_SQL: (dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): string => {
        if (!dto) return '';
        let where = OnpChloroplastHaplotypeSQL.WHERE_SQL(dto);
        return `DELETE FROM ${TABLE} ${where}`;
    },
    /**
     * 删除
     * @param dto
     * @return {string}
     */
    DELETE_BY_ID_SQL: (id: string): string => {
        if (!id) return '';
        return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
    },
    /**
     *
     * @param dto
     * @return {string}
     */
    DELETE_BY_IDS_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        let inSql = OnpChloroplastHaplotypeSQL.IN_SQL(PRIMER_KEY, idList);
        return `DELETE FROM ${TABLE} WHERE ${inSql}`;
    },
    /**
     * 删除所有记录的SQL语句
     */
    DELETE_ALL_SQL: (): string => {
        return `DELETE FROM ${TABLE}`;
    },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'ONP_CHLOROPLAST_HAPLOTYPE';
/**
 * 列名常量字段
 */
const OCH_ID = OnpChloroplastHaplotypeColNameEnum.OCH_ID;
const OCH_CHLOROPLAST_HAPLOTYPE = OnpChloroplastHaplotypeColNameEnum.OCH_CHLOROPLAST_HAPLOTYPE;
const OCH_CHR = OnpChloroplastHaplotypeColNameEnum.OCH_CHR;
const OCH_CHLOROPLAST_ONP = OnpChloroplastHaplotypeColNameEnum.OCH_CHLOROPLAST_ONP;
const OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE = OnpChloroplastHaplotypeColNameEnum.OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE;
const OCH_FREQUENCY = OnpChloroplastHaplotypeColNameEnum.OCH_FREQUENCY;
const OCH_COLUMN1 = OnpChloroplastHaplotypeColNameEnum.OCH_COLUMN1;
const OCH_COLUMN2 = OnpChloroplastHaplotypeColNameEnum.OCH_COLUMN2;
const OCH_COLUMN3 = OnpChloroplastHaplotypeColNameEnum.OCH_COLUMN3;
const OCH_ORDER = OnpChloroplastHaplotypeColNameEnum.OCH_ORDER;
const OCH_SPECIES = OnpChloroplastHaplotypeColNameEnum.OCH_SPECIES;
const OCH_REMARK = OnpChloroplastHaplotypeColNameEnum.OCH_REMARK;
const OCH_CREATE_DATE = OnpChloroplastHaplotypeColNameEnum.OCH_CREATE_DATE;
const OCH_UPDATE_DATE = OnpChloroplastHaplotypeColNameEnum.OCH_UPDATE_DATE;
/**
 * 实体类属性名
 */
const ochId = OnpChloroplastHaplotypeColPropEnum.ochId;
const ochChloroplastHaplotype = OnpChloroplastHaplotypeColPropEnum.ochChloroplastHaplotype;
const ochChr = OnpChloroplastHaplotypeColPropEnum.ochChr;
const ochChloroplastOnp = OnpChloroplastHaplotypeColPropEnum.ochChloroplastOnp;
const ochChloroplastHaplotypeSequence = OnpChloroplastHaplotypeColPropEnum.ochChloroplastHaplotypeSequence;
const ochFrequency = OnpChloroplastHaplotypeColPropEnum.ochFrequency;
const ochColumn1 = OnpChloroplastHaplotypeColPropEnum.ochColumn1;
const ochColumn2 = OnpChloroplastHaplotypeColPropEnum.ochColumn2;
const ochColumn3 = OnpChloroplastHaplotypeColPropEnum.ochColumn3;
const ochOrder = OnpChloroplastHaplotypeColPropEnum.ochOrder;
const ochSpecies = OnpChloroplastHaplotypeColPropEnum.ochSpecies;
const ochRemark = OnpChloroplastHaplotypeColPropEnum.ochRemark;
const ochCreateDate = OnpChloroplastHaplotypeColPropEnum.ochCreateDate;
const ochUpdateDate = OnpChloroplastHaplotypeColPropEnum.ochUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = OnpChloroplastHaplotypeTable.PRIMER_KEY;
const primerKey = OnpChloroplastHaplotypeTable.primerKey;