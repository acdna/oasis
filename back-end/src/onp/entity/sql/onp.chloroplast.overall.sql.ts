import { OnpChloroplastOverallQDto } from '../../dto/query/onp.chloroplast.overall.qdto';
import { OnpChloroplastOverallCDto } from '../../dto/create/onp.chloroplast.overall.cdto';
import { OnpChloroplastOverallUDto } from '../../dto/update/onp.chloroplast.overall.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { OnpChloroplastOverallColNameEnum, OnpChloroplastOverallColPropEnum, OnpChloroplastOverallTable, OnpChloroplastOverallColNames, OnpChloroplastOverallColProps } from '../ddl/onp.chloroplast.overall.cols';
/**
 * ONP_CHLOROPLAST_OVERALL--ONP_CHLOROPLAST_OVERALL表相关SQL定义
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastOverallSQL
 */
export const OnpChloroplastOverallSQL = {
    /**
     * 构建根据有值属性查询SQL
     * @param dto
     * @return {string}
     */
    SELECT_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let cols = OnpChloroplastOverallSQL.COL_MAPPER_SQL(dto);
        let where = OnpChloroplastOverallSQL.WHERE_SQL(dto);
        let group = OnpChloroplastOverallSQL.GROUP_BY_SQL(dto);
        let order = OnpChloroplastOverallSQL.ORDER_BY_SQL(dto);
        let limit = OnpChloroplastOverallSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询记录数SQL
     * @param dto
     * @return {string}
     */
    SELECT_COUNT_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let where = OnpChloroplastOverallSQL.WHERE_SQL(dto);
        let group = OnpChloroplastOverallSQL.GROUP_BY_SQL(dto);
        return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
    },
    /**
     * 根据ID查询记录SQL
     * @param dto
     * @return {string}
     */
    SELECT_BY_ID_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let cols = OnpChloroplastOverallSQL.COL_MAPPER_SQL(dto);
        let group = OnpChloroplastOverallSQL.GROUP_BY_SQL(dto);
        let order = OnpChloroplastOverallSQL.ORDER_BY_SQL(dto);
        let limit = OnpChloroplastOverallSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
    },
    /**
     * 根据ID列表查询记录SQL
     * @param idList
     * @return {string}
     */
    SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
        if (!idList || idList.length == 0) return '';
        let sql = OnpChloroplastOverallSQL.COL_MAPPER_SQL({ cols });
        return `SELECT ${sql} FROM ${TABLE} WHERE ${ OnpChloroplastOverallSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询数据库中已存在ID的SQL语句
     * @param idList
     * @return {string}
     */
    SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${ OnpChloroplastOverallSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询所有ID列表SQL语句
     * @return {string}
     */
    SELECT_ALL_ID_LIST_SQL: (): string => {
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
    },
    /**
     * 条件查询ID列表SQL语句
     * @param dto
     * @return {string}
     */
    SELECT_ID_LIST_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let where = OnpChloroplastOverallSQL.WHERE_SQL(dto);
        let group = OnpChloroplastOverallSQL.GROUP_BY_SQL(dto);
        let order = OnpChloroplastOverallSQL.ORDER_BY_SQL(dto);
        let limit = OnpChloroplastOverallSQL.LIMIT_SQL(dto);
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询结果列映射SQL
     * @param dto
     * @return {string}
     */
    COL_MAPPER_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let cols = StringUtils.toColName(dto.cols, OnpChloroplastOverallColNames, OnpChloroplastOverallColProps);
        if (!cols || cols.length == 0) cols = OnpChloroplastOverallColNames;
        let sql = cols.map(col => `${ col } as ${StringUtils.camelCase(col)}`).join(',');
        return dto.distinct ? `DISTINCT ${sql}` : sql;
    },
    /**
     * Where语句，根据有值属性查询
     * @param dto
     * @return {string}
     */
    WHERE_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[ocoId]?.length > 0) items.push(` ${ OCO_ID } = '${dto[ocoId]}'`);
        if (dto['nocoId']?.length > 0) items.push(` ${ OCO_ID } != '${dto['nocoId']}'`);
        if (dto[ocoOnpId]?.length > 0) items.push(` ${ OCO_ONP_ID } = '${dto[ocoOnpId]}'`);
        if (dto['nocoOnpId']?.length > 0) items.push(` ${ OCO_ONP_ID } != '${dto['nocoOnpId']}'`);
        if (dto[ocoChr]?.length > 0) items.push(` ${ OCO_CHR } = '${dto[ocoChr]}'`);
        if (dto['nocoChr']?.length > 0) items.push(` ${ OCO_CHR } != '${dto['nocoChr']}'`);
        if (dto[ocoStart]?.length > 0) items.push(` ${ OCO_START } = '${dto[ocoStart]}'`);
        if (dto['nocoStart']?.length > 0) items.push(` ${ OCO_START } != '${dto['nocoStart']}'`);
        if (dto[ocoStop]?.length > 0) items.push(` ${ OCO_STOP } = '${dto[ocoStop]}'`);
        if (dto['nocoStop']?.length > 0) items.push(` ${ OCO_STOP } != '${dto['nocoStop']}'`);
        if (dto[ocoOnpSize]?.length > 0) items.push(` ${ OCO_ONP_SIZE } = '${dto[ocoOnpSize]}'`);
        if (dto['nocoOnpSize']?.length > 0) items.push(` ${ OCO_ONP_SIZE } != '${dto['nocoOnpSize']}'`);
        if (dto[ocoLocation]?.length > 0) items.push(` ${ OCO_LOCATION } = '${dto[ocoLocation]}'`);
        if (dto['nocoLocation']?.length > 0) items.push(` ${ OCO_LOCATION } != '${dto['nocoLocation']}'`);
        if (dto[ocoAllMarkers]?.length > 0) items.push(` ${ OCO_ALL_MARKERS } = '${dto[ocoAllMarkers]}'`);
        if (dto['nocoAllMarkers']?.length > 0) items.push(` ${ OCO_ALL_MARKERS } != '${dto['nocoAllMarkers']}'`);
        if (dto[ocoSnpMarkers]?.length > 0) items.push(` ${ OCO_SNP_MARKERS } = '${dto[ocoSnpMarkers]}'`);
        if (dto['nocoSnpMarkers']?.length > 0) items.push(` ${ OCO_SNP_MARKERS } != '${dto['nocoSnpMarkers']}'`);
        if (dto[ocoIndelMarkers]?.length > 0) items.push(` ${ OCO_INDEL_MARKERS } = '${dto[ocoIndelMarkers]}'`);
        if (dto['nocoIndelMarkers']?.length > 0) items.push(` ${ OCO_INDEL_MARKERS } != '${dto['nocoIndelMarkers']}'`);
        if (dto[ocoBlockMarkers]?.length > 0) items.push(` ${ OCO_BLOCK_MARKERS } = '${dto[ocoBlockMarkers]}'`);
        if (dto['nocoBlockMarkers']?.length > 0) items.push(` ${ OCO_BLOCK_MARKERS } != '${dto['nocoBlockMarkers']}'`);
        if (dto[ocoTags]?.length > 0) items.push(` ${ OCO_TAGS } = '${dto[ocoTags]}'`);
        if (dto['nocoTags']?.length > 0) items.push(` ${ OCO_TAGS } != '${dto['nocoTags']}'`);
        if (dto[ocoGenotypes]?.length > 0) items.push(` ${ OCO_GENOTYPES } = '${dto[ocoGenotypes]}'`);
        if (dto['nocoGenotypes']?.length > 0) items.push(` ${ OCO_GENOTYPES } != '${dto['nocoGenotypes']}'`);
        if (dto[ocoMaxGenotypesFreq]?.length > 0) items.push(` ${ OCO_MAX_GENOTYPES_FREQ } = '${dto[ocoMaxGenotypesFreq]}'`);
        if (dto['nocoMaxGenotypesFreq']?.length > 0) items.push(` ${ OCO_MAX_GENOTYPES_FREQ } != '${dto['nocoMaxGenotypesFreq']}'`);
        if (dto[ocoMinGenotypesFreq]?.length > 0) items.push(` ${ OCO_MIN_GENOTYPES_FREQ } = '${dto[ocoMinGenotypesFreq]}'`);
        if (dto['nocoMinGenotypesFreq']?.length > 0) items.push(` ${ OCO_MIN_GENOTYPES_FREQ } != '${dto['nocoMinGenotypesFreq']}'`);
        if (dto[ocoPic]?.length > 0) items.push(` ${ OCO_PIC } = '${dto[ocoPic]}'`);
        if (dto['nocoPic']?.length > 0) items.push(` ${ OCO_PIC } != '${dto['nocoPic']}'`);
        if (dto[ocoColumn1]?.length > 0) items.push(` ${ OCO_COLUMN1 } = '${dto[ocoColumn1]}'`);
        if (dto['nocoColumn1']?.length > 0) items.push(` ${ OCO_COLUMN1 } != '${dto['nocoColumn1']}'`);
        if (dto[ocoColumn2]?.length > 0) items.push(` ${ OCO_COLUMN2 } = '${dto[ocoColumn2]}'`);
        if (dto['nocoColumn2']?.length > 0) items.push(` ${ OCO_COLUMN2 } != '${dto['nocoColumn2']}'`);
        if (dto[ocoColumn3]?.length > 0) items.push(` ${ OCO_COLUMN3 } = '${dto[ocoColumn3]}'`);
        if (dto['nocoColumn3']?.length > 0) items.push(` ${ OCO_COLUMN3 } != '${dto['nocoColumn3']}'`);
        if (dto[ocoOrder]) items.push(` ${ OCO_ORDER } = '${dto[ocoOrder]}'`);
        if (dto['nocoOrder']) items.push(` ${ OCO_ORDER } != '${dto['nocoOrder']}'`);
        if (dto[ocoSpecies]?.length > 0) items.push(` ${ OCO_SPECIES } = '${dto[ocoSpecies]}'`);
        if (dto['nocoSpecies']?.length > 0) items.push(` ${ OCO_SPECIES } != '${dto['nocoSpecies']}'`);
        if (dto[ocoRemark]?.length > 0) items.push(` ${ OCO_REMARK } = '${dto[ocoRemark]}'`);
        if (dto['nocoRemark']?.length > 0) items.push(` ${ OCO_REMARK } != '${dto['nocoRemark']}'`);
        if (dto[ocoCreateDate]) items.push(`${ OCO_CREATE_DATE } is not null and date_format(${ OCO_CREATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[ocoCreateDate])}`);
        if (dto['nocoCreateDate']) items.push(`${ OCO_CREATE_DATE } is not null and date_format(${ OCO_CREATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nocoCreateDate'])}`);
        if (dto[ocoUpdateDate]) items.push(`${ OCO_UPDATE_DATE } is not null and date_format(${ OCO_UPDATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[ocoUpdateDate])}`);
        if (dto['nocoUpdateDate']) items.push(`${ OCO_UPDATE_DATE } is not null and date_format(${ OCO_UPDATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nocoUpdateDate'])}`);
        //数组、模糊条件、年份日期范围等额外查询条件
        //OCO_ID--字符串字段
        if (dto['ocoIdList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ID, < string[] > dto['ocoIdList']));
        if (dto['nocoIdList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ID, < string[] > dto['nocoIdList'], true));
        if (dto['ocoIdLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ID, dto['ocoIdLike'])); //For MysSQL
        if (dto['nocoIdLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ID, dto['nocoIdLike'], true)); //For MysSQL
        if (dto['ocoIdLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_ID, < string[] > dto['ocoIdLikeList'])); //For MysSQL
        //OCO_ONP_ID--字符串字段
        if (dto['ocoOnpIdList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ONP_ID, < string[] > dto['ocoOnpIdList']));
        if (dto['nocoOnpIdList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ONP_ID, < string[] > dto['nocoOnpIdList'], true));
        if (dto['ocoOnpIdLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ONP_ID, dto['ocoOnpIdLike'])); //For MysSQL
        if (dto['nocoOnpIdLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ONP_ID, dto['nocoOnpIdLike'], true)); //For MysSQL
        if (dto['ocoOnpIdLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_ONP_ID, < string[] > dto['ocoOnpIdLikeList'])); //For MysSQL
        //OCO_CHR--字符串字段
        if (dto['ocoChrList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_CHR, < string[] > dto['ocoChrList']));
        if (dto['nocoChrList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_CHR, < string[] > dto['nocoChrList'], true));
        if (dto['ocoChrLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_CHR, dto['ocoChrLike'])); //For MysSQL
        if (dto['nocoChrLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_CHR, dto['nocoChrLike'], true)); //For MysSQL
        if (dto['ocoChrLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_CHR, < string[] > dto['ocoChrLikeList'])); //For MysSQL
        //OCO_START--字符串字段
        if (dto['ocoStartList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_START, < string[] > dto['ocoStartList']));
        if (dto['nocoStartList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_START, < string[] > dto['nocoStartList'], true));
        if (dto['ocoStartLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_START, dto['ocoStartLike'])); //For MysSQL
        if (dto['nocoStartLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_START, dto['nocoStartLike'], true)); //For MysSQL
        if (dto['ocoStartLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_START, < string[] > dto['ocoStartLikeList'])); //For MysSQL
        //OCO_STOP--字符串字段
        if (dto['ocoStopList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_STOP, < string[] > dto['ocoStopList']));
        if (dto['nocoStopList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_STOP, < string[] > dto['nocoStopList'], true));
        if (dto['ocoStopLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_STOP, dto['ocoStopLike'])); //For MysSQL
        if (dto['nocoStopLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_STOP, dto['nocoStopLike'], true)); //For MysSQL
        if (dto['ocoStopLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_STOP, < string[] > dto['ocoStopLikeList'])); //For MysSQL
        //OCO_ONP_SIZE--字符串字段
        if (dto['ocoOnpSizeList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ONP_SIZE, < string[] > dto['ocoOnpSizeList']));
        if (dto['nocoOnpSizeList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ONP_SIZE, < string[] > dto['nocoOnpSizeList'], true));
        if (dto['ocoOnpSizeLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ONP_SIZE, dto['ocoOnpSizeLike'])); //For MysSQL
        if (dto['nocoOnpSizeLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ONP_SIZE, dto['nocoOnpSizeLike'], true)); //For MysSQL
        if (dto['ocoOnpSizeLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_ONP_SIZE, < string[] > dto['ocoOnpSizeLikeList'])); //For MysSQL
        //OCO_LOCATION--字符串字段
        if (dto['ocoLocationList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_LOCATION, < string[] > dto['ocoLocationList']));
        if (dto['nocoLocationList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_LOCATION, < string[] > dto['nocoLocationList'], true));
        if (dto['ocoLocationLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_LOCATION, dto['ocoLocationLike'])); //For MysSQL
        if (dto['nocoLocationLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_LOCATION, dto['nocoLocationLike'], true)); //For MysSQL
        if (dto['ocoLocationLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_LOCATION, < string[] > dto['ocoLocationLikeList'])); //For MysSQL
        //OCO_ALL_MARKERS--字符串字段
        if (dto['ocoAllMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ALL_MARKERS, < string[] > dto['ocoAllMarkersList']));
        if (dto['nocoAllMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ALL_MARKERS, < string[] > dto['nocoAllMarkersList'], true));
        if (dto['ocoAllMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ALL_MARKERS, dto['ocoAllMarkersLike'])); //For MysSQL
        if (dto['nocoAllMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_ALL_MARKERS, dto['nocoAllMarkersLike'], true)); //For MysSQL
        if (dto['ocoAllMarkersLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_ALL_MARKERS, < string[] > dto['ocoAllMarkersLikeList'])); //For MysSQL
        //OCO_SNP_MARKERS--字符串字段
        if (dto['ocoSnpMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_SNP_MARKERS, < string[] > dto['ocoSnpMarkersList']));
        if (dto['nocoSnpMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_SNP_MARKERS, < string[] > dto['nocoSnpMarkersList'], true));
        if (dto['ocoSnpMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_SNP_MARKERS, dto['ocoSnpMarkersLike'])); //For MysSQL
        if (dto['nocoSnpMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_SNP_MARKERS, dto['nocoSnpMarkersLike'], true)); //For MysSQL
        if (dto['ocoSnpMarkersLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_SNP_MARKERS, < string[] > dto['ocoSnpMarkersLikeList'])); //For MysSQL
        //OCO_INDEL_MARKERS--字符串字段
        if (dto['ocoIndelMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_INDEL_MARKERS, < string[] > dto['ocoIndelMarkersList']));
        if (dto['nocoIndelMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_INDEL_MARKERS, < string[] > dto['nocoIndelMarkersList'], true));
        if (dto['ocoIndelMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_INDEL_MARKERS, dto['ocoIndelMarkersLike'])); //For MysSQL
        if (dto['nocoIndelMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_INDEL_MARKERS, dto['nocoIndelMarkersLike'], true)); //For MysSQL
        if (dto['ocoIndelMarkersLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_INDEL_MARKERS, < string[] > dto['ocoIndelMarkersLikeList'])); //For MysSQL
        //OCO_BLOCK_MARKERS--字符串字段
        if (dto['ocoBlockMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_BLOCK_MARKERS, < string[] > dto['ocoBlockMarkersList']));
        if (dto['nocoBlockMarkersList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_BLOCK_MARKERS, < string[] > dto['nocoBlockMarkersList'], true));
        if (dto['ocoBlockMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_BLOCK_MARKERS, dto['ocoBlockMarkersLike'])); //For MysSQL
        if (dto['nocoBlockMarkersLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_BLOCK_MARKERS, dto['nocoBlockMarkersLike'], true)); //For MysSQL
        if (dto['ocoBlockMarkersLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_BLOCK_MARKERS, < string[] > dto['ocoBlockMarkersLikeList'])); //For MysSQL
        //OCO_TAGS--字符串字段
        if (dto['ocoTagsList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_TAGS, < string[] > dto['ocoTagsList']));
        if (dto['nocoTagsList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_TAGS, < string[] > dto['nocoTagsList'], true));
        if (dto['ocoTagsLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_TAGS, dto['ocoTagsLike'])); //For MysSQL
        if (dto['nocoTagsLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_TAGS, dto['nocoTagsLike'], true)); //For MysSQL
        if (dto['ocoTagsLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_TAGS, < string[] > dto['ocoTagsLikeList'])); //For MysSQL
        //OCO_GENOTYPES--字符串字段
        if (dto['ocoGenotypesList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_GENOTYPES, < string[] > dto['ocoGenotypesList']));
        if (dto['nocoGenotypesList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_GENOTYPES, < string[] > dto['nocoGenotypesList'], true));
        if (dto['ocoGenotypesLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_GENOTYPES, dto['ocoGenotypesLike'])); //For MysSQL
        if (dto['nocoGenotypesLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_GENOTYPES, dto['nocoGenotypesLike'], true)); //For MysSQL
        if (dto['ocoGenotypesLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_GENOTYPES, < string[] > dto['ocoGenotypesLikeList'])); //For MysSQL
        //OCO_MAX_GENOTYPES_FREQ--字符串字段
        if (dto['ocoMaxGenotypesFreqList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_MAX_GENOTYPES_FREQ, < string[] > dto['ocoMaxGenotypesFreqList']));
        if (dto['nocoMaxGenotypesFreqList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_MAX_GENOTYPES_FREQ, < string[] > dto['nocoMaxGenotypesFreqList'], true));
        if (dto['ocoMaxGenotypesFreqLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_MAX_GENOTYPES_FREQ, dto['ocoMaxGenotypesFreqLike'])); //For MysSQL
        if (dto['nocoMaxGenotypesFreqLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_MAX_GENOTYPES_FREQ, dto['nocoMaxGenotypesFreqLike'], true)); //For MysSQL
        if (dto['ocoMaxGenotypesFreqLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_MAX_GENOTYPES_FREQ, < string[] > dto['ocoMaxGenotypesFreqLikeList'])); //For MysSQL
        //OCO_MIN_GENOTYPES_FREQ--字符串字段
        if (dto['ocoMinGenotypesFreqList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_MIN_GENOTYPES_FREQ, < string[] > dto['ocoMinGenotypesFreqList']));
        if (dto['nocoMinGenotypesFreqList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_MIN_GENOTYPES_FREQ, < string[] > dto['nocoMinGenotypesFreqList'], true));
        if (dto['ocoMinGenotypesFreqLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_MIN_GENOTYPES_FREQ, dto['ocoMinGenotypesFreqLike'])); //For MysSQL
        if (dto['nocoMinGenotypesFreqLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_MIN_GENOTYPES_FREQ, dto['nocoMinGenotypesFreqLike'], true)); //For MysSQL
        if (dto['ocoMinGenotypesFreqLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_MIN_GENOTYPES_FREQ, < string[] > dto['ocoMinGenotypesFreqLikeList'])); //For MysSQL
        //OCO_PIC--字符串字段
        if (dto['ocoPicList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_PIC, < string[] > dto['ocoPicList']));
        if (dto['nocoPicList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_PIC, < string[] > dto['nocoPicList'], true));
        if (dto['ocoPicLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_PIC, dto['ocoPicLike'])); //For MysSQL
        if (dto['nocoPicLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_PIC, dto['nocoPicLike'], true)); //For MysSQL
        if (dto['ocoPicLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_PIC, < string[] > dto['ocoPicLikeList'])); //For MysSQL
        //OCO_COLUMN1--字符串字段
        if (dto['ocoColumn1List']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_COLUMN1, < string[] > dto['ocoColumn1List']));
        if (dto['nocoColumn1List']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_COLUMN1, < string[] > dto['nocoColumn1List'], true));
        if (dto['ocoColumn1Like']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_COLUMN1, dto['ocoColumn1Like'])); //For MysSQL
        if (dto['nocoColumn1Like']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_COLUMN1, dto['nocoColumn1Like'], true)); //For MysSQL
        if (dto['ocoColumn1LikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_COLUMN1, < string[] > dto['ocoColumn1LikeList'])); //For MysSQL
        //OCO_COLUMN2--字符串字段
        if (dto['ocoColumn2List']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_COLUMN2, < string[] > dto['ocoColumn2List']));
        if (dto['nocoColumn2List']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_COLUMN2, < string[] > dto['nocoColumn2List'], true));
        if (dto['ocoColumn2Like']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_COLUMN2, dto['ocoColumn2Like'])); //For MysSQL
        if (dto['nocoColumn2Like']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_COLUMN2, dto['nocoColumn2Like'], true)); //For MysSQL
        if (dto['ocoColumn2LikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_COLUMN2, < string[] > dto['ocoColumn2LikeList'])); //For MysSQL
        //OCO_COLUMN3--字符串字段
        if (dto['ocoColumn3List']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_COLUMN3, < string[] > dto['ocoColumn3List']));
        if (dto['nocoColumn3List']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_COLUMN3, < string[] > dto['nocoColumn3List'], true));
        if (dto['ocoColumn3Like']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_COLUMN3, dto['ocoColumn3Like'])); //For MysSQL
        if (dto['nocoColumn3Like']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_COLUMN3, dto['nocoColumn3Like'], true)); //For MysSQL
        if (dto['ocoColumn3LikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_COLUMN3, < string[] > dto['ocoColumn3LikeList'])); //For MysSQL
        //OCO_ORDER--数字字段
        if (dto['ocoOrderList']) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ORDER, dto['ocoOrderList']));
        if (dto['nocoOrderList']) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_ORDER, dto['nocoOrderList'], true));
        //OCO_SPECIES--字符串字段
        if (dto['ocoSpeciesList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_SPECIES, < string[] > dto['ocoSpeciesList']));
        if (dto['nocoSpeciesList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_SPECIES, < string[] > dto['nocoSpeciesList'], true));
        if (dto['ocoSpeciesLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_SPECIES, dto['ocoSpeciesLike'])); //For MysSQL
        if (dto['nocoSpeciesLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_SPECIES, dto['nocoSpeciesLike'], true)); //For MysSQL
        if (dto['ocoSpeciesLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_SPECIES, < string[] > dto['ocoSpeciesLikeList'])); //For MysSQL
        //OCO_REMARK--字符串字段
        if (dto['ocoRemarkList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_REMARK, < string[] > dto['ocoRemarkList']));
        if (dto['nocoRemarkList']?.length > 0) items.push(OnpChloroplastOverallSQL.IN_SQL(OCO_REMARK, < string[] > dto['nocoRemarkList'], true));
        if (dto['ocoRemarkLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_REMARK, dto['ocoRemarkLike'])); //For MysSQL
        if (dto['nocoRemarkLike']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_SQL(OCO_REMARK, dto['nocoRemarkLike'], true)); //For MysSQL
        if (dto['ocoRemarkLikeList']?.length > 0) items.push(OnpChloroplastOverallSQL.LIKE_LIST_SQL(OCO_REMARK, < string[] > dto['ocoRemarkLikeList'])); //For MysSQL
        //OCO_CREATE_DATE--日期字段
        if (dto[ocoCreateDate]) items.push(`${ OCO_CREATE_DATE } is not null and date_format(${ OCO_CREATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sOcoCreateDate'])}`);
        if (dto[ocoCreateDate]) items.push(`${ OCO_CREATE_DATE } is not null and date_format(${ OCO_CREATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eOcoCreateDate'])}`);
        //OCO_UPDATE_DATE--日期字段
        if (dto[ocoUpdateDate]) items.push(`${ OCO_UPDATE_DATE } is not null and date_format(${ OCO_UPDATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sOcoUpdateDate'])}`);
        if (dto[ocoUpdateDate]) items.push(`${ OCO_UPDATE_DATE } is not null and date_format(${ OCO_UPDATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eOcoUpdateDate'])}`);
        return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
    },
    /**
     * 字符串的Like语句，采用前后模糊查询
     * @param colName 列名
     * @param values 值
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
        if (!colName || value.length == 0 || !value || value.length == 0) return '';
        if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
        else return ` ${colName} like CONCAT('%','${value}','%')`;
    },
    /**
     * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
     * @param colName 列名
     * @param values 值列表
     * @param andOr 采用and或or连接多个条件，默认为OR语句
     * @return {string}
     */
    LIKE_LIST_SQL: (colName: string, values: string[], andOr ? : string): string => {
        if (!colName || !values || values.length == 0) return '';
        if (!andOr) andOr = 'OR';
        let items = values.map(value => OnpChloroplastOverallSQL.LIKE_SQL(colName, value));
        return `( ${items.join(' ' + andOr + ' ')} )`;
    },
    /**
     * 排序查询SQL语句
     * @param dto
     * @return {string}
     */
    ORDER_BY_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        if (dto['order']?.length > 0) {
            return `order by ${dto['order']}`;
        }
        return '';
    },
    /**
     * 分组查询SQL语句
     * @param dto
     * @return {string}
     */
    GROUP_BY_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        if (dto['group']?.length > 0) {
            return `group by ${dto['group']}`;
        }
        return '';
    },
    /**
     * 分页查询条件SQL语句
     * @param dto
     * @return {string}
     */
    LIMIT_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
            return `limit ${dto['offset']},${dto['pageSize']}`;
        }
        return '';
    },
    /**
     * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
     * @param colName 列名，如:SAM_NAME
     * @param values 数组
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
        if (!values || values.length == 0) return '';
        let items = [];
        Array.isArray(values) ? values.forEach(value => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
        return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
    },
    /**
     * 插入记录SQL语句
     * @param dto
     * @return {string}
     */
    INSERT_SQL: (dto: OnpChloroplastOverallCDto | Partial < OnpChloroplastOverallCDto > ): string => {
        if (!dto) return '';
        if (!dto[primerKey] || dto[primerKey] == undefined) return '';
        let cols = [];
        let values = [];
        cols.push(OCO_ID);
        cols.push(OCO_ONP_ID);
        cols.push(OCO_CHR);
        cols.push(OCO_START);
        cols.push(OCO_STOP);
        cols.push(OCO_ONP_SIZE);
        cols.push(OCO_LOCATION);
        cols.push(OCO_ALL_MARKERS);
        cols.push(OCO_SNP_MARKERS);
        cols.push(OCO_INDEL_MARKERS);
        cols.push(OCO_BLOCK_MARKERS);
        cols.push(OCO_TAGS);
        cols.push(OCO_GENOTYPES);
        cols.push(OCO_MAX_GENOTYPES_FREQ);
        cols.push(OCO_MIN_GENOTYPES_FREQ);
        cols.push(OCO_PIC);
        cols.push(OCO_COLUMN1);
        cols.push(OCO_COLUMN2);
        cols.push(OCO_COLUMN3);
        cols.push(OCO_ORDER);
        cols.push(OCO_SPECIES);
        cols.push(OCO_REMARK);
        cols.push(OCO_CREATE_DATE);
        cols.push(OCO_UPDATE_DATE);
        values.push(toSqlValue(dto[ocoId]));
        values.push(toSqlValue(dto[ocoOnpId]));
        values.push(toSqlValue(dto[ocoChr]));
        values.push(toSqlValue(dto[ocoStart]));
        values.push(toSqlValue(dto[ocoStop]));
        values.push(toSqlValue(dto[ocoOnpSize]));
        values.push(toSqlValue(dto[ocoLocation]));
        values.push(toSqlValue(dto[ocoAllMarkers]));
        values.push(toSqlValue(dto[ocoSnpMarkers]));
        values.push(toSqlValue(dto[ocoIndelMarkers]));
        values.push(toSqlValue(dto[ocoBlockMarkers]));
        values.push(toSqlValue(dto[ocoTags]));
        values.push(toSqlValue(dto[ocoGenotypes]));
        values.push(toSqlValue(dto[ocoMaxGenotypesFreq]));
        values.push(toSqlValue(dto[ocoMinGenotypesFreq]));
        values.push(toSqlValue(dto[ocoPic]));
        values.push(toSqlValue(dto[ocoColumn1]));
        values.push(toSqlValue(dto[ocoColumn2]));
        values.push(toSqlValue(dto[ocoColumn3]));
        values.push(toSqlValue(dto[ocoOrder]));
        values.push(toSqlValue(dto[ocoSpecies]));
        values.push(toSqlValue(dto[ocoRemark]));
        values.push('NOW()');
        values.push('NOW()');
        let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
        return sql;
    },
    /**
     * 批量插入SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_INSERT_SQL: (dtos: OnpChloroplastOverallCDto[] | Partial < OnpChloroplastOverallCDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let cols = [];
        cols.push(OCO_ID);
        cols.push(OCO_ONP_ID);
        cols.push(OCO_CHR);
        cols.push(OCO_START);
        cols.push(OCO_STOP);
        cols.push(OCO_ONP_SIZE);
        cols.push(OCO_LOCATION);
        cols.push(OCO_ALL_MARKERS);
        cols.push(OCO_SNP_MARKERS);
        cols.push(OCO_INDEL_MARKERS);
        cols.push(OCO_BLOCK_MARKERS);
        cols.push(OCO_TAGS);
        cols.push(OCO_GENOTYPES);
        cols.push(OCO_MAX_GENOTYPES_FREQ);
        cols.push(OCO_MIN_GENOTYPES_FREQ);
        cols.push(OCO_PIC);
        cols.push(OCO_COLUMN1);
        cols.push(OCO_COLUMN2);
        cols.push(OCO_COLUMN3);
        cols.push(OCO_ORDER);
        cols.push(OCO_SPECIES);
        cols.push(OCO_REMARK);
        cols.push(OCO_CREATE_DATE);
        cols.push(OCO_UPDATE_DATE);
        let items = [];
        dtos.forEach(dto => {
            let values = [];
            values.push(toSqlValue(dto[ocoId]));
            values.push(toSqlValue(dto[ocoOnpId]));
            values.push(toSqlValue(dto[ocoChr]));
            values.push(toSqlValue(dto[ocoStart]));
            values.push(toSqlValue(dto[ocoStop]));
            values.push(toSqlValue(dto[ocoOnpSize]));
            values.push(toSqlValue(dto[ocoLocation]));
            values.push(toSqlValue(dto[ocoAllMarkers]));
            values.push(toSqlValue(dto[ocoSnpMarkers]));
            values.push(toSqlValue(dto[ocoIndelMarkers]));
            values.push(toSqlValue(dto[ocoBlockMarkers]));
            values.push(toSqlValue(dto[ocoTags]));
            values.push(toSqlValue(dto[ocoGenotypes]));
            values.push(toSqlValue(dto[ocoMaxGenotypesFreq]));
            values.push(toSqlValue(dto[ocoMinGenotypesFreq]));
            values.push(toSqlValue(dto[ocoPic]));
            values.push(toSqlValue(dto[ocoColumn1]));
            values.push(toSqlValue(dto[ocoColumn2]));
            values.push(toSqlValue(dto[ocoColumn3]));
            values.push(toSqlValue(dto[ocoOrder]));
            values.push(toSqlValue(dto[ocoSpecies]));
            values.push(toSqlValue(dto[ocoRemark]));
            values.push('NOW()');
            values.push('NOW()');
            items.push(`(${values.join(',')})`);
        });
        return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
    },
    /**
     * 根据ID更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SQL: (dto: OnpChloroplastOverallUDto | Partial < OnpChloroplastOverallUDto > ): string => {
        if (!dto) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpChloroplastOverallColNames, OnpChloroplastOverallColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ OCO_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ OCO_ONP_ID } = ${toSqlValue(dto[ocoOnpId])}`);
        items.push(`${ OCO_CHR } = ${toSqlValue(dto[ocoChr])}`);
        items.push(`${ OCO_START } = ${toSqlValue(dto[ocoStart])}`);
        items.push(`${ OCO_STOP } = ${toSqlValue(dto[ocoStop])}`);
        items.push(`${ OCO_ONP_SIZE } = ${toSqlValue(dto[ocoOnpSize])}`);
        items.push(`${ OCO_LOCATION } = ${toSqlValue(dto[ocoLocation])}`);
        items.push(`${ OCO_ALL_MARKERS } = ${toSqlValue(dto[ocoAllMarkers])}`);
        items.push(`${ OCO_SNP_MARKERS } = ${toSqlValue(dto[ocoSnpMarkers])}`);
        items.push(`${ OCO_INDEL_MARKERS } = ${toSqlValue(dto[ocoIndelMarkers])}`);
        items.push(`${ OCO_BLOCK_MARKERS } = ${toSqlValue(dto[ocoBlockMarkers])}`);
        items.push(`${ OCO_TAGS } = ${toSqlValue(dto[ocoTags])}`);
        items.push(`${ OCO_GENOTYPES } = ${toSqlValue(dto[ocoGenotypes])}`);
        items.push(`${ OCO_MAX_GENOTYPES_FREQ } = ${toSqlValue(dto[ocoMaxGenotypesFreq])}`);
        items.push(`${ OCO_MIN_GENOTYPES_FREQ } = ${toSqlValue(dto[ocoMinGenotypesFreq])}`);
        items.push(`${ OCO_PIC } = ${toSqlValue(dto[ocoPic])}`);
        items.push(`${ OCO_COLUMN1 } = ${toSqlValue(dto[ocoColumn1])}`);
        items.push(`${ OCO_COLUMN2 } = ${toSqlValue(dto[ocoColumn2])}`);
        items.push(`${ OCO_COLUMN3 } = ${toSqlValue(dto[ocoColumn3])}`);
        items.push(`${ OCO_ORDER } = ${toSqlValue(dto[ocoOrder])}`);
        items.push(`${ OCO_SPECIES } = ${toSqlValue(dto[ocoSpecies])}`);
        items.push(`${ OCO_REMARK } = ${toSqlValue(dto[ocoRemark])}`);
        items.push(`${ OCO_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID列表批量更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_IDS_SQL: (dto: OnpChloroplastOverallUDto | Partial < OnpChloroplastOverallUDto > ): string => {
        if (!dto) return '';
        //获取ID列表
        let ids = < string[] > dto[`${primerKey}List`];
        if (!ids || ids.length == 0) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpChloroplastOverallColNames, OnpChloroplastOverallColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ OCO_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpChloroplastOverallSQL.IN_SQL(PRIMER_KEY, ids)}`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ OCO_ONP_ID } = ${toSqlValue(dto[ocoOnpId])}`);
        items.push(`${ OCO_CHR } = ${toSqlValue(dto[ocoChr])}`);
        items.push(`${ OCO_START } = ${toSqlValue(dto[ocoStart])}`);
        items.push(`${ OCO_STOP } = ${toSqlValue(dto[ocoStop])}`);
        items.push(`${ OCO_ONP_SIZE } = ${toSqlValue(dto[ocoOnpSize])}`);
        items.push(`${ OCO_LOCATION } = ${toSqlValue(dto[ocoLocation])}`);
        items.push(`${ OCO_ALL_MARKERS } = ${toSqlValue(dto[ocoAllMarkers])}`);
        items.push(`${ OCO_SNP_MARKERS } = ${toSqlValue(dto[ocoSnpMarkers])}`);
        items.push(`${ OCO_INDEL_MARKERS } = ${toSqlValue(dto[ocoIndelMarkers])}`);
        items.push(`${ OCO_BLOCK_MARKERS } = ${toSqlValue(dto[ocoBlockMarkers])}`);
        items.push(`${ OCO_TAGS } = ${toSqlValue(dto[ocoTags])}`);
        items.push(`${ OCO_GENOTYPES } = ${toSqlValue(dto[ocoGenotypes])}`);
        items.push(`${ OCO_MAX_GENOTYPES_FREQ } = ${toSqlValue(dto[ocoMaxGenotypesFreq])}`);
        items.push(`${ OCO_MIN_GENOTYPES_FREQ } = ${toSqlValue(dto[ocoMinGenotypesFreq])}`);
        items.push(`${ OCO_PIC } = ${toSqlValue(dto[ocoPic])}`);
        items.push(`${ OCO_COLUMN1 } = ${toSqlValue(dto[ocoColumn1])}`);
        items.push(`${ OCO_COLUMN2 } = ${toSqlValue(dto[ocoColumn2])}`);
        items.push(`${ OCO_COLUMN3 } = ${toSqlValue(dto[ocoColumn3])}`);
        items.push(`${ OCO_ORDER } = ${toSqlValue(dto[ocoOrder])}`);
        items.push(`${ OCO_SPECIES } = ${toSqlValue(dto[ocoSpecies])}`);
        items.push(`${ OCO_REMARK } = ${toSqlValue(dto[ocoRemark])}`);
        items.push(`${ OCO_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpChloroplastOverallSQL.IN_SQL(PRIMER_KEY, ids)}`;
    },
    /**
     * 根据ID更新所有属性值的批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SQL: (dtos: OnpChloroplastOverallUDto[] | Partial < OnpChloroplastOverallUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let sqls = [];
        dtos.forEach(value => {
            sqls.push(OnpChloroplastOverallSQL.UPDATE_BY_ID_SQL(value));
        });
        return sqls.join(';');
    },
    /**
     * 根据ID更新有值属性值的条件更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SELECTIVE_SQL: (dto: OnpChloroplastOverallUDto | Partial < OnpChloroplastOverallUDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[ocoOnpId]?.length > 0) items.push(`${ OCO_ONP_ID } = ${toSqlValue(dto[ocoOnpId])}`);
        if (dto[ocoChr]?.length > 0) items.push(`${ OCO_CHR } = ${toSqlValue(dto[ocoChr])}`);
        if (dto[ocoStart]?.length > 0) items.push(`${ OCO_START } = ${toSqlValue(dto[ocoStart])}`);
        if (dto[ocoStop]?.length > 0) items.push(`${ OCO_STOP } = ${toSqlValue(dto[ocoStop])}`);
        if (dto[ocoOnpSize]?.length > 0) items.push(`${ OCO_ONP_SIZE } = ${toSqlValue(dto[ocoOnpSize])}`);
        if (dto[ocoLocation]?.length > 0) items.push(`${ OCO_LOCATION } = ${toSqlValue(dto[ocoLocation])}`);
        if (dto[ocoAllMarkers]?.length > 0) items.push(`${ OCO_ALL_MARKERS } = ${toSqlValue(dto[ocoAllMarkers])}`);
        if (dto[ocoSnpMarkers]?.length > 0) items.push(`${ OCO_SNP_MARKERS } = ${toSqlValue(dto[ocoSnpMarkers])}`);
        if (dto[ocoIndelMarkers]?.length > 0) items.push(`${ OCO_INDEL_MARKERS } = ${toSqlValue(dto[ocoIndelMarkers])}`);
        if (dto[ocoBlockMarkers]?.length > 0) items.push(`${ OCO_BLOCK_MARKERS } = ${toSqlValue(dto[ocoBlockMarkers])}`);
        if (dto[ocoTags]?.length > 0) items.push(`${ OCO_TAGS } = ${toSqlValue(dto[ocoTags])}`);
        if (dto[ocoGenotypes]?.length > 0) items.push(`${ OCO_GENOTYPES } = ${toSqlValue(dto[ocoGenotypes])}`);
        if (dto[ocoMaxGenotypesFreq]?.length > 0) items.push(`${ OCO_MAX_GENOTYPES_FREQ } = ${toSqlValue(dto[ocoMaxGenotypesFreq])}`);
        if (dto[ocoMinGenotypesFreq]?.length > 0) items.push(`${ OCO_MIN_GENOTYPES_FREQ } = ${toSqlValue(dto[ocoMinGenotypesFreq])}`);
        if (dto[ocoPic]?.length > 0) items.push(`${ OCO_PIC } = ${toSqlValue(dto[ocoPic])}`);
        if (dto[ocoColumn1]?.length > 0) items.push(`${ OCO_COLUMN1 } = ${toSqlValue(dto[ocoColumn1])}`);
        if (dto[ocoColumn2]?.length > 0) items.push(`${ OCO_COLUMN2 } = ${toSqlValue(dto[ocoColumn2])}`);
        if (dto[ocoColumn3]?.length > 0) items.push(`${ OCO_COLUMN3 } = ${toSqlValue(dto[ocoColumn3])}`);
        if (dto[ocoOrder]) items.push(`${ OCO_ORDER } = ${toSqlValue(dto[ocoOrder])}`);
        if (dto[ocoSpecies]?.length > 0) items.push(`${ OCO_SPECIES } = ${toSqlValue(dto[ocoSpecies])}`);
        if (dto[ocoRemark]?.length > 0) items.push(`${ OCO_REMARK } = ${toSqlValue(dto[ocoRemark])}`);
        if (dto[ocoUpdateDate]) items.push(`${  OCO_UPDATE_DATE }= NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID更新有值属性值的条件批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: OnpChloroplastOverallUDto[] | Partial < OnpChloroplastOverallUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let items = [];
        dtos.forEach(value => items.push(OnpChloroplastOverallSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
        return items.join(';');
    },
    /**
     * 条件删除SQL语句
     * @param dtos
     * @return {string}
     */
    DELETE_SELECTIVE_SQL: (dto: OnpChloroplastOverallQDto | Partial < OnpChloroplastOverallQDto > ): string => {
        if (!dto) return '';
        let where = OnpChloroplastOverallSQL.WHERE_SQL(dto);
        return `DELETE FROM ${TABLE} ${where}`;
    },
    /**
     * 删除
     * @param dto
     * @return {string}
     */
    DELETE_BY_ID_SQL: (id: string): string => {
        if (!id) return '';
        return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
    },
    /**
     *
     * @param dto
     * @return {string}
     */
    DELETE_BY_IDS_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        let inSql = OnpChloroplastOverallSQL.IN_SQL(PRIMER_KEY, idList);
        return `DELETE FROM ${TABLE} WHERE ${inSql}`;
    },
    /**
     * 删除所有记录的SQL语句
     */
    DELETE_ALL_SQL: (): string => {
        return `DELETE FROM ${TABLE}`;
    },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'ONP_CHLOROPLAST_OVERALL';
/**
 * 列名常量字段
 */
const OCO_ID = OnpChloroplastOverallColNameEnum.OCO_ID;
const OCO_ONP_ID = OnpChloroplastOverallColNameEnum.OCO_ONP_ID;
const OCO_CHR = OnpChloroplastOverallColNameEnum.OCO_CHR;
const OCO_START = OnpChloroplastOverallColNameEnum.OCO_START;
const OCO_STOP = OnpChloroplastOverallColNameEnum.OCO_STOP;
const OCO_ONP_SIZE = OnpChloroplastOverallColNameEnum.OCO_ONP_SIZE;
const OCO_LOCATION = OnpChloroplastOverallColNameEnum.OCO_LOCATION;
const OCO_ALL_MARKERS = OnpChloroplastOverallColNameEnum.OCO_ALL_MARKERS;
const OCO_SNP_MARKERS = OnpChloroplastOverallColNameEnum.OCO_SNP_MARKERS;
const OCO_INDEL_MARKERS = OnpChloroplastOverallColNameEnum.OCO_INDEL_MARKERS;
const OCO_BLOCK_MARKERS = OnpChloroplastOverallColNameEnum.OCO_BLOCK_MARKERS;
const OCO_TAGS = OnpChloroplastOverallColNameEnum.OCO_TAGS;
const OCO_GENOTYPES = OnpChloroplastOverallColNameEnum.OCO_GENOTYPES;
const OCO_MAX_GENOTYPES_FREQ = OnpChloroplastOverallColNameEnum.OCO_MAX_GENOTYPES_FREQ;
const OCO_MIN_GENOTYPES_FREQ = OnpChloroplastOverallColNameEnum.OCO_MIN_GENOTYPES_FREQ;
const OCO_PIC = OnpChloroplastOverallColNameEnum.OCO_PIC;
const OCO_COLUMN1 = OnpChloroplastOverallColNameEnum.OCO_COLUMN1;
const OCO_COLUMN2 = OnpChloroplastOverallColNameEnum.OCO_COLUMN2;
const OCO_COLUMN3 = OnpChloroplastOverallColNameEnum.OCO_COLUMN3;
const OCO_ORDER = OnpChloroplastOverallColNameEnum.OCO_ORDER;
const OCO_SPECIES = OnpChloroplastOverallColNameEnum.OCO_SPECIES;
const OCO_REMARK = OnpChloroplastOverallColNameEnum.OCO_REMARK;
const OCO_CREATE_DATE = OnpChloroplastOverallColNameEnum.OCO_CREATE_DATE;
const OCO_UPDATE_DATE = OnpChloroplastOverallColNameEnum.OCO_UPDATE_DATE;
/**
 * 实体类属性名
 */
const ocoId = OnpChloroplastOverallColPropEnum.ocoId;
const ocoOnpId = OnpChloroplastOverallColPropEnum.ocoOnpId;
const ocoChr = OnpChloroplastOverallColPropEnum.ocoChr;
const ocoStart = OnpChloroplastOverallColPropEnum.ocoStart;
const ocoStop = OnpChloroplastOverallColPropEnum.ocoStop;
const ocoOnpSize = OnpChloroplastOverallColPropEnum.ocoOnpSize;
const ocoLocation = OnpChloroplastOverallColPropEnum.ocoLocation;
const ocoAllMarkers = OnpChloroplastOverallColPropEnum.ocoAllMarkers;
const ocoSnpMarkers = OnpChloroplastOverallColPropEnum.ocoSnpMarkers;
const ocoIndelMarkers = OnpChloroplastOverallColPropEnum.ocoIndelMarkers;
const ocoBlockMarkers = OnpChloroplastOverallColPropEnum.ocoBlockMarkers;
const ocoTags = OnpChloroplastOverallColPropEnum.ocoTags;
const ocoGenotypes = OnpChloroplastOverallColPropEnum.ocoGenotypes;
const ocoMaxGenotypesFreq = OnpChloroplastOverallColPropEnum.ocoMaxGenotypesFreq;
const ocoMinGenotypesFreq = OnpChloroplastOverallColPropEnum.ocoMinGenotypesFreq;
const ocoPic = OnpChloroplastOverallColPropEnum.ocoPic;
const ocoColumn1 = OnpChloroplastOverallColPropEnum.ocoColumn1;
const ocoColumn2 = OnpChloroplastOverallColPropEnum.ocoColumn2;
const ocoColumn3 = OnpChloroplastOverallColPropEnum.ocoColumn3;
const ocoOrder = OnpChloroplastOverallColPropEnum.ocoOrder;
const ocoSpecies = OnpChloroplastOverallColPropEnum.ocoSpecies;
const ocoRemark = OnpChloroplastOverallColPropEnum.ocoRemark;
const ocoCreateDate = OnpChloroplastOverallColPropEnum.ocoCreateDate;
const ocoUpdateDate = OnpChloroplastOverallColPropEnum.ocoUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = OnpChloroplastOverallTable.PRIMER_KEY;
const primerKey = OnpChloroplastOverallTable.primerKey;