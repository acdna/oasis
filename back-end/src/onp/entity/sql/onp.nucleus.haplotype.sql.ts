import { OnpNucleusHaplotypeQDto } from '../../dto/query/onp.nucleus.haplotype.qdto';
import { OnpNucleusHaplotypeCDto } from '../../dto/create/onp.nucleus.haplotype.cdto';
import { OnpNucleusHaplotypeUDto } from '../../dto/update/onp.nucleus.haplotype.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { OnpNucleusHaplotypeColNameEnum, OnpNucleusHaplotypeColPropEnum, OnpNucleusHaplotypeTable, OnpNucleusHaplotypeColNames, OnpNucleusHaplotypeColProps } from '../ddl/onp.nucleus.haplotype.cols';
/**
 * ONP_NUCLEUS_HAPLOTYPE--ONP_NUCLEUS_HAPLOTYPE表相关SQL定义
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export
 * @class OnpNucleusHaplotypeSQL
 */
export const OnpNucleusHaplotypeSQL = {
    /**
     * 构建根据有值属性查询SQL
     * @param dto
     * @return {string}
     */
    SELECT_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let cols = OnpNucleusHaplotypeSQL.COL_MAPPER_SQL(dto);
        let where = OnpNucleusHaplotypeSQL.WHERE_SQL(dto);
        let group = OnpNucleusHaplotypeSQL.GROUP_BY_SQL(dto);
        let order = OnpNucleusHaplotypeSQL.ORDER_BY_SQL(dto);
        let limit = OnpNucleusHaplotypeSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询记录数SQL
     * @param dto
     * @return {string}
     */
    SELECT_COUNT_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let where = OnpNucleusHaplotypeSQL.WHERE_SQL(dto);
        let group = OnpNucleusHaplotypeSQL.GROUP_BY_SQL(dto);
        return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
    },
    /**
     * 根据ID查询记录SQL
     * @param dto
     * @return {string}
     */
    SELECT_BY_ID_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let cols = OnpNucleusHaplotypeSQL.COL_MAPPER_SQL(dto);
        let group = OnpNucleusHaplotypeSQL.GROUP_BY_SQL(dto);
        let order = OnpNucleusHaplotypeSQL.ORDER_BY_SQL(dto);
        let limit = OnpNucleusHaplotypeSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
    },
    /**
     * 根据ID列表查询记录SQL
     * @param idList
     * @return {string}
     */
    SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
        if (!idList || idList.length == 0) return '';
        let sql = OnpNucleusHaplotypeSQL.COL_MAPPER_SQL({ cols });
        return `SELECT ${sql} FROM ${TABLE} WHERE ${ OnpNucleusHaplotypeSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询数据库中已存在ID的SQL语句
     * @param idList
     * @return {string}
     */
    SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${ OnpNucleusHaplotypeSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询所有ID列表SQL语句
     * @return {string}
     */
    SELECT_ALL_ID_LIST_SQL: (): string => {
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
    },
    /**
     * 条件查询ID列表SQL语句
     * @param dto
     * @return {string}
     */
    SELECT_ID_LIST_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let where = OnpNucleusHaplotypeSQL.WHERE_SQL(dto);
        let group = OnpNucleusHaplotypeSQL.GROUP_BY_SQL(dto);
        let order = OnpNucleusHaplotypeSQL.ORDER_BY_SQL(dto);
        let limit = OnpNucleusHaplotypeSQL.LIMIT_SQL(dto);
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询结果列映射SQL
     * @param dto
     * @return {string}
     */
    COL_MAPPER_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let cols = StringUtils.toColName(dto.cols, OnpNucleusHaplotypeColNames, OnpNucleusHaplotypeColProps);
        if (!cols || cols.length == 0) cols = OnpNucleusHaplotypeColNames;
        let sql = cols.map(col => `${ col } as ${StringUtils.camelCase(col)}`).join(',');
        return dto.distinct ? `DISTINCT ${sql}` : sql;
    },
    /**
     * Where语句，根据有值属性查询
     * @param dto
     * @return {string}
     */
    WHERE_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[onhId]?.length > 0) items.push(` ${ ONH_ID } = '${dto[onhId]}'`);
        if (dto['nonhId']?.length > 0) items.push(` ${ ONH_ID } != '${dto['nonhId']}'`);
        if (dto[onhHaplotypeIndex]?.length > 0) items.push(` ${ ONH_HAPLOTYPE_INDEX } = '${dto[onhHaplotypeIndex]}'`);
        if (dto['nonhHaplotypeIndex']?.length > 0) items.push(` ${ ONH_HAPLOTYPE_INDEX } != '${dto['nonhHaplotypeIndex']}'`);
        if (dto[onhChr]?.length > 0) items.push(` ${ ONH_CHR } = '${dto[onhChr]}'`);
        if (dto['nonhChr']?.length > 0) items.push(` ${ ONH_CHR } != '${dto['nonhChr']}'`);
        if (dto[onhOnpId]?.length > 0) items.push(` ${ ONH_ONP_ID } = '${dto[onhOnpId]}'`);
        if (dto['nonhOnpId']?.length > 0) items.push(` ${ ONH_ONP_ID } != '${dto['nonhOnpId']}'`);
        if (dto[onhHaplotypeSequence]?.length > 0) items.push(` ${ ONH_HAPLOTYPE_SEQUENCE } = '${dto[onhHaplotypeSequence]}'`);
        if (dto['nonhHaplotypeSequence']?.length > 0) items.push(` ${ ONH_HAPLOTYPE_SEQUENCE } != '${dto['nonhHaplotypeSequence']}'`);
        if (dto[onhHaplotypeTagSequence]?.length > 0) items.push(` ${ ONH_HAPLOTYPE_TAG_SEQUENCE } = '${dto[onhHaplotypeTagSequence]}'`);
        if (dto['nonhHaplotypeTagSequence']?.length > 0) items.push(` ${ ONH_HAPLOTYPE_TAG_SEQUENCE } != '${dto['nonhHaplotypeTagSequence']}'`);
        if (dto[onhFrequency]?.length > 0) items.push(` ${ ONH_FREQUENCY } = '${dto[onhFrequency]}'`);
        if (dto['nonhFrequency']?.length > 0) items.push(` ${ ONH_FREQUENCY } != '${dto['nonhFrequency']}'`);
        if (dto[onhColumn1]?.length > 0) items.push(` ${ ONH_COLUMN1 } = '${dto[onhColumn1]}'`);
        if (dto['nonhColumn1']?.length > 0) items.push(` ${ ONH_COLUMN1 } != '${dto['nonhColumn1']}'`);
        if (dto[onhColumn2]?.length > 0) items.push(` ${ ONH_COLUMN2 } = '${dto[onhColumn2]}'`);
        if (dto['nonhColumn2']?.length > 0) items.push(` ${ ONH_COLUMN2 } != '${dto['nonhColumn2']}'`);
        if (dto[onhColumn3]?.length > 0) items.push(` ${ ONH_COLUMN3 } = '${dto[onhColumn3]}'`);
        if (dto['nonhColumn3']?.length > 0) items.push(` ${ ONH_COLUMN3 } != '${dto['nonhColumn3']}'`);
        if (dto[onhOrder]) items.push(` ${ ONH_ORDER } = '${dto[onhOrder]}'`);
        if (dto['nonhOrder']) items.push(` ${ ONH_ORDER } != '${dto['nonhOrder']}'`);
        if (dto[onhSpecies]?.length > 0) items.push(` ${ ONH_SPECIES } = '${dto[onhSpecies]}'`);
        if (dto['nonhSpecies']?.length > 0) items.push(` ${ ONH_SPECIES } != '${dto['nonhSpecies']}'`);
        if (dto[onhRemark]?.length > 0) items.push(` ${ ONH_REMARK } = '${dto[onhRemark]}'`);
        if (dto['nonhRemark']?.length > 0) items.push(` ${ ONH_REMARK } != '${dto['nonhRemark']}'`);
        if (dto[onhCreateDate]) items.push(`${ ONH_CREATE_DATE } is not null and date_format(${ ONH_CREATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[onhCreateDate])}`);
        if (dto['nonhCreateDate']) items.push(`${ ONH_CREATE_DATE } is not null and date_format(${ ONH_CREATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nonhCreateDate'])}`);
        if (dto[onhUpdateDate]) items.push(`${ ONH_UPDATE_DATE } is not null and date_format(${ ONH_UPDATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[onhUpdateDate])}`);
        if (dto['nonhUpdateDate']) items.push(`${ ONH_UPDATE_DATE } is not null and date_format(${ ONH_UPDATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nonhUpdateDate'])}`);
        //数组、模糊条件、年份日期范围等额外查询条件
        //ONH_ID--字符串字段
        if (dto['onhIdList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_ID, < string[] > dto['onhIdList']));
        if (dto['nonhIdList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_ID, < string[] > dto['nonhIdList'], true));
        if (dto['onhIdLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_ID, dto['onhIdLike'])); //For MysSQL
        if (dto['nonhIdLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_ID, dto['nonhIdLike'], true)); //For MysSQL
        if (dto['onhIdLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_ID, < string[] > dto['onhIdLikeList'])); //For MysSQL
        //ONH_HAPLOTYPE_INDEX--字符串字段
        if (dto['onhHaplotypeIndexList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_HAPLOTYPE_INDEX, < string[] > dto['onhHaplotypeIndexList']));
        if (dto['nonhHaplotypeIndexList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_HAPLOTYPE_INDEX, < string[] > dto['nonhHaplotypeIndexList'], true));
        if (dto['onhHaplotypeIndexLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_HAPLOTYPE_INDEX, dto['onhHaplotypeIndexLike'])); //For MysSQL
        if (dto['nonhHaplotypeIndexLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_HAPLOTYPE_INDEX, dto['nonhHaplotypeIndexLike'], true)); //For MysSQL
        if (dto['onhHaplotypeIndexLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_HAPLOTYPE_INDEX, < string[] > dto['onhHaplotypeIndexLikeList'])); //For MysSQL
        //ONH_CHR--字符串字段
        if (dto['onhChrList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_CHR, < string[] > dto['onhChrList']));
        if (dto['nonhChrList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_CHR, < string[] > dto['nonhChrList'], true));
        if (dto['onhChrLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_CHR, dto['onhChrLike'])); //For MysSQL
        if (dto['nonhChrLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_CHR, dto['nonhChrLike'], true)); //For MysSQL
        if (dto['onhChrLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_CHR, < string[] > dto['onhChrLikeList'])); //For MysSQL
        //ONH_ONP_ID--字符串字段
        if (dto['onhOnpIdList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_ONP_ID, < string[] > dto['onhOnpIdList']));
        if (dto['nonhOnpIdList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_ONP_ID, < string[] > dto['nonhOnpIdList'], true));
        if (dto['onhOnpIdLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_ONP_ID, dto['onhOnpIdLike'])); //For MysSQL
        if (dto['nonhOnpIdLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_ONP_ID, dto['nonhOnpIdLike'], true)); //For MysSQL
        if (dto['onhOnpIdLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_ONP_ID, < string[] > dto['onhOnpIdLikeList'])); //For MysSQL
        //ONH_HAPLOTYPE_SEQUENCE--字符串字段
        if (dto['onhHaplotypeSequenceList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_HAPLOTYPE_SEQUENCE, < string[] > dto['onhHaplotypeSequenceList']));
        if (dto['nonhHaplotypeSequenceList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_HAPLOTYPE_SEQUENCE, < string[] > dto['nonhHaplotypeSequenceList'], true));
        if (dto['onhHaplotypeSequenceLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_HAPLOTYPE_SEQUENCE, dto['onhHaplotypeSequenceLike'])); //For MysSQL
        if (dto['nonhHaplotypeSequenceLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_HAPLOTYPE_SEQUENCE, dto['nonhHaplotypeSequenceLike'], true)); //For MysSQL
        if (dto['onhHaplotypeSequenceLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_HAPLOTYPE_SEQUENCE, < string[] > dto['onhHaplotypeSequenceLikeList'])); //For MysSQL
        //ONH_HAPLOTYPE_TAG_SEQUENCE--字符串字段
        if (dto['onhHaplotypeTagSequenceList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_HAPLOTYPE_TAG_SEQUENCE, < string[] > dto['onhHaplotypeTagSequenceList']));
        if (dto['nonhHaplotypeTagSequenceList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_HAPLOTYPE_TAG_SEQUENCE, < string[] > dto['nonhHaplotypeTagSequenceList'], true));
        if (dto['onhHaplotypeTagSequenceLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_HAPLOTYPE_TAG_SEQUENCE, dto['onhHaplotypeTagSequenceLike'])); //For MysSQL
        if (dto['nonhHaplotypeTagSequenceLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_HAPLOTYPE_TAG_SEQUENCE, dto['nonhHaplotypeTagSequenceLike'], true)); //For MysSQL
        if (dto['onhHaplotypeTagSequenceLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_HAPLOTYPE_TAG_SEQUENCE, < string[] > dto['onhHaplotypeTagSequenceLikeList'])); //For MysSQL
        //ONH_FREQUENCY--字符串字段
        if (dto['onhFrequencyList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_FREQUENCY, < string[] > dto['onhFrequencyList']));
        if (dto['nonhFrequencyList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_FREQUENCY, < string[] > dto['nonhFrequencyList'], true));
        if (dto['onhFrequencyLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_FREQUENCY, dto['onhFrequencyLike'])); //For MysSQL
        if (dto['nonhFrequencyLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_FREQUENCY, dto['nonhFrequencyLike'], true)); //For MysSQL
        if (dto['onhFrequencyLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_FREQUENCY, < string[] > dto['onhFrequencyLikeList'])); //For MysSQL
        //ONH_COLUMN1--字符串字段
        if (dto['onhColumn1List']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_COLUMN1, < string[] > dto['onhColumn1List']));
        if (dto['nonhColumn1List']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_COLUMN1, < string[] > dto['nonhColumn1List'], true));
        if (dto['onhColumn1Like']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_COLUMN1, dto['onhColumn1Like'])); //For MysSQL
        if (dto['nonhColumn1Like']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_COLUMN1, dto['nonhColumn1Like'], true)); //For MysSQL
        if (dto['onhColumn1LikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_COLUMN1, < string[] > dto['onhColumn1LikeList'])); //For MysSQL
        //ONH_COLUMN2--字符串字段
        if (dto['onhColumn2List']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_COLUMN2, < string[] > dto['onhColumn2List']));
        if (dto['nonhColumn2List']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_COLUMN2, < string[] > dto['nonhColumn2List'], true));
        if (dto['onhColumn2Like']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_COLUMN2, dto['onhColumn2Like'])); //For MysSQL
        if (dto['nonhColumn2Like']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_COLUMN2, dto['nonhColumn2Like'], true)); //For MysSQL
        if (dto['onhColumn2LikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_COLUMN2, < string[] > dto['onhColumn2LikeList'])); //For MysSQL
        //ONH_COLUMN3--字符串字段
        if (dto['onhColumn3List']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_COLUMN3, < string[] > dto['onhColumn3List']));
        if (dto['nonhColumn3List']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_COLUMN3, < string[] > dto['nonhColumn3List'], true));
        if (dto['onhColumn3Like']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_COLUMN3, dto['onhColumn3Like'])); //For MysSQL
        if (dto['nonhColumn3Like']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_COLUMN3, dto['nonhColumn3Like'], true)); //For MysSQL
        if (dto['onhColumn3LikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_COLUMN3, < string[] > dto['onhColumn3LikeList'])); //For MysSQL
        //ONH_ORDER--数字字段
        if (dto['onhOrderList']) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_ORDER, dto['onhOrderList']));
        if (dto['nonhOrderList']) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_ORDER, dto['nonhOrderList'], true));
        //ONH_SPECIES--字符串字段
        if (dto['onhSpeciesList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_SPECIES, < string[] > dto['onhSpeciesList']));
        if (dto['nonhSpeciesList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_SPECIES, < string[] > dto['nonhSpeciesList'], true));
        if (dto['onhSpeciesLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_SPECIES, dto['onhSpeciesLike'])); //For MysSQL
        if (dto['nonhSpeciesLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_SPECIES, dto['nonhSpeciesLike'], true)); //For MysSQL
        if (dto['onhSpeciesLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_SPECIES, < string[] > dto['onhSpeciesLikeList'])); //For MysSQL
        //ONH_REMARK--字符串字段
        if (dto['onhRemarkList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_REMARK, < string[] > dto['onhRemarkList']));
        if (dto['nonhRemarkList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.IN_SQL(ONH_REMARK, < string[] > dto['nonhRemarkList'], true));
        if (dto['onhRemarkLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_REMARK, dto['onhRemarkLike'])); //For MysSQL
        if (dto['nonhRemarkLike']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_SQL(ONH_REMARK, dto['nonhRemarkLike'], true)); //For MysSQL
        if (dto['onhRemarkLikeList']?.length > 0) items.push(OnpNucleusHaplotypeSQL.LIKE_LIST_SQL(ONH_REMARK, < string[] > dto['onhRemarkLikeList'])); //For MysSQL
        //ONH_CREATE_DATE--日期字段
        if (dto[onhCreateDate]) items.push(`${ ONH_CREATE_DATE } is not null and date_format(${ ONH_CREATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sOnhCreateDate'])}`);
        if (dto[onhCreateDate]) items.push(`${ ONH_CREATE_DATE } is not null and date_format(${ ONH_CREATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eOnhCreateDate'])}`);
        //ONH_UPDATE_DATE--日期字段
        if (dto[onhUpdateDate]) items.push(`${ ONH_UPDATE_DATE } is not null and date_format(${ ONH_UPDATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sOnhUpdateDate'])}`);
        if (dto[onhUpdateDate]) items.push(`${ ONH_UPDATE_DATE } is not null and date_format(${ ONH_UPDATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eOnhUpdateDate'])}`);
        return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
    },
    /**
     * 字符串的Like语句，采用前后模糊查询
     * @param colName 列名
     * @param values 值
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
        if (!colName || value.length == 0 || !value || value.length == 0) return '';
        if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
        else return ` ${colName} like CONCAT('%','${value}','%')`;
    },
    /**
     * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
     * @param colName 列名
     * @param values 值列表
     * @param andOr 采用and或or连接多个条件，默认为OR语句
     * @return {string}
     */
    LIKE_LIST_SQL: (colName: string, values: string[], andOr ? : string): string => {
        if (!colName || !values || values.length == 0) return '';
        if (!andOr) andOr = 'OR';
        let items = values.map(value => OnpNucleusHaplotypeSQL.LIKE_SQL(colName, value));
        return `( ${items.join(' ' + andOr + ' ')} )`;
    },
    /**
     * 排序查询SQL语句
     * @param dto
     * @return {string}
     */
    ORDER_BY_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        if (dto['order']?.length > 0) {
            return `order by ${dto['order']}`;
        }
        return '';
    },
    /**
     * 分组查询SQL语句
     * @param dto
     * @return {string}
     */
    GROUP_BY_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        if (dto['group']?.length > 0) {
            return `group by ${dto['group']}`;
        }
        return '';
    },
    /**
     * 分页查询条件SQL语句
     * @param dto
     * @return {string}
     */
    LIMIT_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
            return `limit ${dto['offset']},${dto['pageSize']}`;
        }
        return '';
    },
    /**
     * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
     * @param colName 列名，如:SAM_NAME
     * @param values 数组
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
        if (!values || values.length == 0) return '';
        let items = [];
        Array.isArray(values) ? values.forEach(value => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
        return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
    },
    /**
     * 插入记录SQL语句
     * @param dto
     * @return {string}
     */
    INSERT_SQL: (dto: OnpNucleusHaplotypeCDto | Partial < OnpNucleusHaplotypeCDto > ): string => {
        if (!dto) return '';
        if (!dto[primerKey] || dto[primerKey] == undefined) return '';
        let cols = [];
        let values = [];
        cols.push(ONH_ID);
        cols.push(ONH_HAPLOTYPE_INDEX);
        cols.push(ONH_CHR);
        cols.push(ONH_ONP_ID);
        cols.push(ONH_HAPLOTYPE_SEQUENCE);
        cols.push(ONH_HAPLOTYPE_TAG_SEQUENCE);
        cols.push(ONH_FREQUENCY);
        cols.push(ONH_COLUMN1);
        cols.push(ONH_COLUMN2);
        cols.push(ONH_COLUMN3);
        cols.push(ONH_ORDER);
        cols.push(ONH_SPECIES);
        cols.push(ONH_REMARK);
        cols.push(ONH_CREATE_DATE);
        cols.push(ONH_UPDATE_DATE);
        values.push(toSqlValue(dto[onhId]));
        values.push(toSqlValue(dto[onhHaplotypeIndex]));
        values.push(toSqlValue(dto[onhChr]));
        values.push(toSqlValue(dto[onhOnpId]));
        values.push(toSqlValue(dto[onhHaplotypeSequence]));
        values.push(toSqlValue(dto[onhHaplotypeTagSequence]));
        values.push(toSqlValue(dto[onhFrequency]));
        values.push(toSqlValue(dto[onhColumn1]));
        values.push(toSqlValue(dto[onhColumn2]));
        values.push(toSqlValue(dto[onhColumn3]));
        values.push(toSqlValue(dto[onhOrder]));
        values.push(toSqlValue(dto[onhSpecies]));
        values.push(toSqlValue(dto[onhRemark]));
        values.push('NOW()');
        values.push('NOW()');
        let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
        return sql;
    },
    /**
     * 批量插入SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_INSERT_SQL: (dtos: OnpNucleusHaplotypeCDto[] | Partial < OnpNucleusHaplotypeCDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let cols = [];
        cols.push(ONH_ID);
        cols.push(ONH_HAPLOTYPE_INDEX);
        cols.push(ONH_CHR);
        cols.push(ONH_ONP_ID);
        cols.push(ONH_HAPLOTYPE_SEQUENCE);
        cols.push(ONH_HAPLOTYPE_TAG_SEQUENCE);
        cols.push(ONH_FREQUENCY);
        cols.push(ONH_COLUMN1);
        cols.push(ONH_COLUMN2);
        cols.push(ONH_COLUMN3);
        cols.push(ONH_ORDER);
        cols.push(ONH_SPECIES);
        cols.push(ONH_REMARK);
        cols.push(ONH_CREATE_DATE);
        cols.push(ONH_UPDATE_DATE);
        let items = [];
        dtos.forEach(dto => {
            let values = [];
            values.push(toSqlValue(dto[onhId]));
            values.push(toSqlValue(dto[onhHaplotypeIndex]));
            values.push(toSqlValue(dto[onhChr]));
            values.push(toSqlValue(dto[onhOnpId]));
            values.push(toSqlValue(dto[onhHaplotypeSequence]));
            values.push(toSqlValue(dto[onhHaplotypeTagSequence]));
            values.push(toSqlValue(dto[onhFrequency]));
            values.push(toSqlValue(dto[onhColumn1]));
            values.push(toSqlValue(dto[onhColumn2]));
            values.push(toSqlValue(dto[onhColumn3]));
            values.push(toSqlValue(dto[onhOrder]));
            values.push(toSqlValue(dto[onhSpecies]));
            values.push(toSqlValue(dto[onhRemark]));
            values.push('NOW()');
            values.push('NOW()');
            items.push(`(${values.join(',')})`);
        });
        return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
    },
    /**
     * 根据ID更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SQL: (dto: OnpNucleusHaplotypeUDto | Partial < OnpNucleusHaplotypeUDto > ): string => {
        if (!dto) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpNucleusHaplotypeColNames, OnpNucleusHaplotypeColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ ONH_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ ONH_HAPLOTYPE_INDEX } = ${toSqlValue(dto[onhHaplotypeIndex])}`);
        items.push(`${ ONH_CHR } = ${toSqlValue(dto[onhChr])}`);
        items.push(`${ ONH_ONP_ID } = ${toSqlValue(dto[onhOnpId])}`);
        items.push(`${ ONH_HAPLOTYPE_SEQUENCE } = ${toSqlValue(dto[onhHaplotypeSequence])}`);
        items.push(`${ ONH_HAPLOTYPE_TAG_SEQUENCE } = ${toSqlValue(dto[onhHaplotypeTagSequence])}`);
        items.push(`${ ONH_FREQUENCY } = ${toSqlValue(dto[onhFrequency])}`);
        items.push(`${ ONH_COLUMN1 } = ${toSqlValue(dto[onhColumn1])}`);
        items.push(`${ ONH_COLUMN2 } = ${toSqlValue(dto[onhColumn2])}`);
        items.push(`${ ONH_COLUMN3 } = ${toSqlValue(dto[onhColumn3])}`);
        items.push(`${ ONH_ORDER } = ${toSqlValue(dto[onhOrder])}`);
        items.push(`${ ONH_SPECIES } = ${toSqlValue(dto[onhSpecies])}`);
        items.push(`${ ONH_REMARK } = ${toSqlValue(dto[onhRemark])}`);
        items.push(`${ ONH_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID列表批量更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_IDS_SQL: (dto: OnpNucleusHaplotypeUDto | Partial < OnpNucleusHaplotypeUDto > ): string => {
        if (!dto) return '';
        //获取ID列表
        let ids = < string[] > dto[`${primerKey}List`];
        if (!ids || ids.length == 0) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpNucleusHaplotypeColNames, OnpNucleusHaplotypeColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ ONH_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpNucleusHaplotypeSQL.IN_SQL(PRIMER_KEY, ids)}`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ ONH_HAPLOTYPE_INDEX } = ${toSqlValue(dto[onhHaplotypeIndex])}`);
        items.push(`${ ONH_CHR } = ${toSqlValue(dto[onhChr])}`);
        items.push(`${ ONH_ONP_ID } = ${toSqlValue(dto[onhOnpId])}`);
        items.push(`${ ONH_HAPLOTYPE_SEQUENCE } = ${toSqlValue(dto[onhHaplotypeSequence])}`);
        items.push(`${ ONH_HAPLOTYPE_TAG_SEQUENCE } = ${toSqlValue(dto[onhHaplotypeTagSequence])}`);
        items.push(`${ ONH_FREQUENCY } = ${toSqlValue(dto[onhFrequency])}`);
        items.push(`${ ONH_COLUMN1 } = ${toSqlValue(dto[onhColumn1])}`);
        items.push(`${ ONH_COLUMN2 } = ${toSqlValue(dto[onhColumn2])}`);
        items.push(`${ ONH_COLUMN3 } = ${toSqlValue(dto[onhColumn3])}`);
        items.push(`${ ONH_ORDER } = ${toSqlValue(dto[onhOrder])}`);
        items.push(`${ ONH_SPECIES } = ${toSqlValue(dto[onhSpecies])}`);
        items.push(`${ ONH_REMARK } = ${toSqlValue(dto[onhRemark])}`);
        items.push(`${ ONH_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpNucleusHaplotypeSQL.IN_SQL(PRIMER_KEY, ids)}`;
    },
    /**
     * 根据ID更新所有属性值的批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SQL: (dtos: OnpNucleusHaplotypeUDto[] | Partial < OnpNucleusHaplotypeUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let sqls = [];
        dtos.forEach(value => {
            sqls.push(OnpNucleusHaplotypeSQL.UPDATE_BY_ID_SQL(value));
        });
        return sqls.join(';');
    },
    /**
     * 根据ID更新有值属性值的条件更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SELECTIVE_SQL: (dto: OnpNucleusHaplotypeUDto | Partial < OnpNucleusHaplotypeUDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[onhHaplotypeIndex]?.length > 0) items.push(`${ ONH_HAPLOTYPE_INDEX } = ${toSqlValue(dto[onhHaplotypeIndex])}`);
        if (dto[onhChr]?.length > 0) items.push(`${ ONH_CHR } = ${toSqlValue(dto[onhChr])}`);
        if (dto[onhOnpId]?.length > 0) items.push(`${ ONH_ONP_ID } = ${toSqlValue(dto[onhOnpId])}`);
        if (dto[onhHaplotypeSequence]?.length > 0) items.push(`${ ONH_HAPLOTYPE_SEQUENCE } = ${toSqlValue(dto[onhHaplotypeSequence])}`);
        if (dto[onhHaplotypeTagSequence]?.length > 0) items.push(`${ ONH_HAPLOTYPE_TAG_SEQUENCE } = ${toSqlValue(dto[onhHaplotypeTagSequence])}`);
        if (dto[onhFrequency]?.length > 0) items.push(`${ ONH_FREQUENCY } = ${toSqlValue(dto[onhFrequency])}`);
        if (dto[onhColumn1]?.length > 0) items.push(`${ ONH_COLUMN1 } = ${toSqlValue(dto[onhColumn1])}`);
        if (dto[onhColumn2]?.length > 0) items.push(`${ ONH_COLUMN2 } = ${toSqlValue(dto[onhColumn2])}`);
        if (dto[onhColumn3]?.length > 0) items.push(`${ ONH_COLUMN3 } = ${toSqlValue(dto[onhColumn3])}`);
        if (dto[onhOrder]) items.push(`${ ONH_ORDER } = ${toSqlValue(dto[onhOrder])}`);
        if (dto[onhSpecies]?.length > 0) items.push(`${ ONH_SPECIES } = ${toSqlValue(dto[onhSpecies])}`);
        if (dto[onhRemark]?.length > 0) items.push(`${ ONH_REMARK } = ${toSqlValue(dto[onhRemark])}`);
        if (dto[onhUpdateDate]) items.push(`${  ONH_UPDATE_DATE }= NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID更新有值属性值的条件批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: OnpNucleusHaplotypeUDto[] | Partial < OnpNucleusHaplotypeUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let items = [];
        dtos.forEach(value => items.push(OnpNucleusHaplotypeSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
        return items.join(';');
    },
    /**
     * 条件删除SQL语句
     * @param dtos
     * @return {string}
     */
    DELETE_SELECTIVE_SQL: (dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): string => {
        if (!dto) return '';
        let where = OnpNucleusHaplotypeSQL.WHERE_SQL(dto);
        return `DELETE FROM ${TABLE} ${where}`;
    },
    /**
     * 删除
     * @param dto
     * @return {string}
     */
    DELETE_BY_ID_SQL: (id: string): string => {
        if (!id) return '';
        return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
    },
    /**
     *
     * @param dto
     * @return {string}
     */
    DELETE_BY_IDS_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        let inSql = OnpNucleusHaplotypeSQL.IN_SQL(PRIMER_KEY, idList);
        return `DELETE FROM ${TABLE} WHERE ${inSql}`;
    },
    /**
     * 删除所有记录的SQL语句
     */
    DELETE_ALL_SQL: (): string => {
        return `DELETE FROM ${TABLE}`;
    },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'ONP_NUCLEUS_HAPLOTYPE';
/**
 * 列名常量字段
 */
const ONH_ID = OnpNucleusHaplotypeColNameEnum.ONH_ID;
const ONH_HAPLOTYPE_INDEX = OnpNucleusHaplotypeColNameEnum.ONH_HAPLOTYPE_INDEX;
const ONH_CHR = OnpNucleusHaplotypeColNameEnum.ONH_CHR;
const ONH_ONP_ID = OnpNucleusHaplotypeColNameEnum.ONH_ONP_ID;
const ONH_HAPLOTYPE_SEQUENCE = OnpNucleusHaplotypeColNameEnum.ONH_HAPLOTYPE_SEQUENCE;
const ONH_HAPLOTYPE_TAG_SEQUENCE = OnpNucleusHaplotypeColNameEnum.ONH_HAPLOTYPE_TAG_SEQUENCE;
const ONH_FREQUENCY = OnpNucleusHaplotypeColNameEnum.ONH_FREQUENCY;
const ONH_COLUMN1 = OnpNucleusHaplotypeColNameEnum.ONH_COLUMN1;
const ONH_COLUMN2 = OnpNucleusHaplotypeColNameEnum.ONH_COLUMN2;
const ONH_COLUMN3 = OnpNucleusHaplotypeColNameEnum.ONH_COLUMN3;
const ONH_ORDER = OnpNucleusHaplotypeColNameEnum.ONH_ORDER;
const ONH_SPECIES = OnpNucleusHaplotypeColNameEnum.ONH_SPECIES;
const ONH_REMARK = OnpNucleusHaplotypeColNameEnum.ONH_REMARK;
const ONH_CREATE_DATE = OnpNucleusHaplotypeColNameEnum.ONH_CREATE_DATE;
const ONH_UPDATE_DATE = OnpNucleusHaplotypeColNameEnum.ONH_UPDATE_DATE;
/**
 * 实体类属性名
 */
const onhId = OnpNucleusHaplotypeColPropEnum.onhId;
const onhHaplotypeIndex = OnpNucleusHaplotypeColPropEnum.onhHaplotypeIndex;
const onhChr = OnpNucleusHaplotypeColPropEnum.onhChr;
const onhOnpId = OnpNucleusHaplotypeColPropEnum.onhOnpId;
const onhHaplotypeSequence = OnpNucleusHaplotypeColPropEnum.onhHaplotypeSequence;
const onhHaplotypeTagSequence = OnpNucleusHaplotypeColPropEnum.onhHaplotypeTagSequence;
const onhFrequency = OnpNucleusHaplotypeColPropEnum.onhFrequency;
const onhColumn1 = OnpNucleusHaplotypeColPropEnum.onhColumn1;
const onhColumn2 = OnpNucleusHaplotypeColPropEnum.onhColumn2;
const onhColumn3 = OnpNucleusHaplotypeColPropEnum.onhColumn3;
const onhOrder = OnpNucleusHaplotypeColPropEnum.onhOrder;
const onhSpecies = OnpNucleusHaplotypeColPropEnum.onhSpecies;
const onhRemark = OnpNucleusHaplotypeColPropEnum.onhRemark;
const onhCreateDate = OnpNucleusHaplotypeColPropEnum.onhCreateDate;
const onhUpdateDate = OnpNucleusHaplotypeColPropEnum.onhUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = OnpNucleusHaplotypeTable.PRIMER_KEY;
const primerKey = OnpNucleusHaplotypeTable.primerKey;