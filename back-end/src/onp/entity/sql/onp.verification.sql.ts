import { OnpVerificationQDto } from '../../dto/query/onp.verification.qdto';
import { OnpVerificationCDto } from '../../dto/create/onp.verification.cdto';
import { OnpVerificationUDto } from '../../dto/update/onp.verification.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { OnpVerificationColNameEnum, OnpVerificationColPropEnum, OnpVerificationTable, OnpVerificationColNames, OnpVerificationColProps } from '../ddl/onp.verification.cols';
/**
 * ONP_VERIFICATION--验证表相关SQL定义
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpVerificationSQL
 */
export const OnpVerificationSQL = {
    /**
     * 构建根据有值属性查询SQL
     * @param dto
     * @return {string}
     */
    SELECT_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let cols = OnpVerificationSQL.COL_MAPPER_SQL(dto);
        let where = OnpVerificationSQL.WHERE_SQL(dto);
        let group = OnpVerificationSQL.GROUP_BY_SQL(dto);
        let order = OnpVerificationSQL.ORDER_BY_SQL(dto);
        let limit = OnpVerificationSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询记录数SQL
     * @param dto
     * @return {string}
     */
    SELECT_COUNT_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let where = OnpVerificationSQL.WHERE_SQL(dto);
        let group = OnpVerificationSQL.GROUP_BY_SQL(dto);
        return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
    },
    /**
     * 根据ID查询记录SQL
     * @param dto
     * @return {string}
     */
    SELECT_BY_ID_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let cols = OnpVerificationSQL.COL_MAPPER_SQL(dto);
        let group = OnpVerificationSQL.GROUP_BY_SQL(dto);
        let order = OnpVerificationSQL.ORDER_BY_SQL(dto);
        let limit = OnpVerificationSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
    },
    /**
     * 根据ID列表查询记录SQL
     * @param idList
     * @return {string}
     */
    SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
        if (!idList || idList.length == 0) return '';
        let sql = OnpVerificationSQL.COL_MAPPER_SQL({ cols });
        return `SELECT ${sql} FROM ${TABLE} WHERE ${ OnpVerificationSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询数据库中已存在ID的SQL语句
     * @param idList
     * @return {string}
     */
    SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${ OnpVerificationSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询所有ID列表SQL语句
     * @return {string}
     */
    SELECT_ALL_ID_LIST_SQL: (): string => {
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
    },
    /**
     * 条件查询ID列表SQL语句
     * @param dto
     * @return {string}
     */
    SELECT_ID_LIST_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let where = OnpVerificationSQL.WHERE_SQL(dto);
        let group = OnpVerificationSQL.GROUP_BY_SQL(dto);
        let order = OnpVerificationSQL.ORDER_BY_SQL(dto);
        let limit = OnpVerificationSQL.LIMIT_SQL(dto);
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询结果列映射SQL
     * @param dto
     * @return {string}
     */
    COL_MAPPER_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let cols = StringUtils.toColName(dto.cols, OnpVerificationColNames, OnpVerificationColProps);
        if (!cols || cols.length == 0) cols = OnpVerificationColNames;
        let sql = cols.map(col => `${ col } as ${StringUtils.camelCase(col)}`).join(',');
        return dto.distinct ? `DISTINCT ${sql}` : sql;
    },
    /**
     * Where语句，根据有值属性查询
     * @param dto
     * @return {string}
     */
    WHERE_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[veId]?.length > 0) items.push(` ${ VE_ID } = '${dto[veId]}'`);
        if (dto['nveId']?.length > 0) items.push(` ${ VE_ID } != '${dto['nveId']}'`);
        if (dto[veOrder]) items.push(` ${ VE_ORDER } = '${dto[veOrder]}'`);
        if (dto['nveOrder']) items.push(` ${ VE_ORDER } != '${dto['nveOrder']}'`);
        if (dto[veColumn1]?.length > 0) items.push(` ${ VE_COLUMN1 } = '${dto[veColumn1]}'`);
        if (dto['nveColumn1']?.length > 0) items.push(` ${ VE_COLUMN1 } != '${dto['nveColumn1']}'`);
        if (dto[veColumn2]?.length > 0) items.push(` ${ VE_COLUMN2 } = '${dto[veColumn2]}'`);
        if (dto['nveColumn2']?.length > 0) items.push(` ${ VE_COLUMN2 } != '${dto['nveColumn2']}'`);
        if (dto[veColumn3]?.length > 0) items.push(` ${ VE_COLUMN3 } = '${dto[veColumn3]}'`);
        if (dto['nveColumn3']?.length > 0) items.push(` ${ VE_COLUMN3 } != '${dto['nveColumn3']}'`);
        if (dto[veColumn4]?.length > 0) items.push(` ${ VE_COLUMN4 } = '${dto[veColumn4]}'`);
        if (dto['nveColumn4']?.length > 0) items.push(` ${ VE_COLUMN4 } != '${dto['nveColumn4']}'`);
        if (dto[veColumn5]?.length > 0) items.push(` ${ VE_COLUMN5 } = '${dto[veColumn5]}'`);
        if (dto['nveColumn5']?.length > 0) items.push(` ${ VE_COLUMN5 } != '${dto['nveColumn5']}'`);
        if (dto[veColumn6]?.length > 0) items.push(` ${ VE_COLUMN6 } = '${dto[veColumn6]}'`);
        if (dto['nveColumn6']?.length > 0) items.push(` ${ VE_COLUMN6 } != '${dto['nveColumn6']}'`);
        if (dto[veColumn7]?.length > 0) items.push(` ${ VE_COLUMN7 } = '${dto[veColumn7]}'`);
        if (dto['nveColumn7']?.length > 0) items.push(` ${ VE_COLUMN7 } != '${dto['nveColumn7']}'`);
        if (dto[veColumn8]?.length > 0) items.push(` ${ VE_COLUMN8 } = '${dto[veColumn8]}'`);
        if (dto['nveColumn8']?.length > 0) items.push(` ${ VE_COLUMN8 } != '${dto['nveColumn8']}'`);
        if (dto[veColumn9]?.length > 0) items.push(` ${ VE_COLUMN9 } = '${dto[veColumn9]}'`);
        if (dto['nveColumn9']?.length > 0) items.push(` ${ VE_COLUMN9 } != '${dto['nveColumn9']}'`);
        if (dto[veSpecies]?.length > 0) items.push(` ${ VE_SPECIES } = '${dto[veSpecies]}'`);
        if (dto['nveSpecies']?.length > 0) items.push(` ${ VE_SPECIES } != '${dto['nveSpecies']}'`);
        if (dto[veRemark]?.length > 0) items.push(` ${ VE_REMARK } = '${dto[veRemark]}'`);
        if (dto['nveRemark']?.length > 0) items.push(` ${ VE_REMARK } != '${dto['nveRemark']}'`);
        if (dto[veCreateDate]) items.push(`${ VE_CREATE_DATE } is not null and date_format(${ VE_CREATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[veCreateDate])}`);
        if (dto['nveCreateDate']) items.push(`${ VE_CREATE_DATE } is not null and date_format(${ VE_CREATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nveCreateDate'])}`);
        if (dto[veUpdateDate]) items.push(`${ VE_UPDATE_DATE } is not null and date_format(${ VE_UPDATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[veUpdateDate])}`);
        if (dto['nveUpdateDate']) items.push(`${ VE_UPDATE_DATE } is not null and date_format(${ VE_UPDATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nveUpdateDate'])}`);
        //数组、模糊条件、年份日期范围等额外查询条件
        //VE_ID--字符串字段
        if (dto['veIdList']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_ID, < string[] > dto['veIdList']));
        if (dto['nveIdList']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_ID, < string[] > dto['nveIdList'], true));
        if (dto['veIdLike']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_ID, dto['veIdLike'])); //For MysSQL
        if (dto['nveIdLike']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_ID, dto['nveIdLike'], true)); //For MysSQL
        if (dto['veIdLikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_ID, < string[] > dto['veIdLikeList'])); //For MysSQL
        //VE_ORDER--数字字段
        if (dto['veOrderList']) items.push(OnpVerificationSQL.IN_SQL(VE_ORDER, dto['veOrderList']));
        if (dto['nveOrderList']) items.push(OnpVerificationSQL.IN_SQL(VE_ORDER, dto['nveOrderList'], true));
        //VE_COLUMN1--字符串字段
        if (dto['veColumn1List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN1, < string[] > dto['veColumn1List']));
        if (dto['nveColumn1List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN1, < string[] > dto['nveColumn1List'], true));
        if (dto['veColumn1Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN1, dto['veColumn1Like'])); //For MysSQL
        if (dto['nveColumn1Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN1, dto['nveColumn1Like'], true)); //For MysSQL
        if (dto['veColumn1LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN1, < string[] > dto['veColumn1LikeList'])); //For MysSQL
        //VE_COLUMN2--字符串字段
        if (dto['veColumn2List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN2, < string[] > dto['veColumn2List']));
        if (dto['nveColumn2List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN2, < string[] > dto['nveColumn2List'], true));
        if (dto['veColumn2Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN2, dto['veColumn2Like'])); //For MysSQL
        if (dto['nveColumn2Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN2, dto['nveColumn2Like'], true)); //For MysSQL
        if (dto['veColumn2LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN2, < string[] > dto['veColumn2LikeList'])); //For MysSQL
        //VE_COLUMN3--字符串字段
        if (dto['veColumn3List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN3, < string[] > dto['veColumn3List']));
        if (dto['nveColumn3List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN3, < string[] > dto['nveColumn3List'], true));
        if (dto['veColumn3Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN3, dto['veColumn3Like'])); //For MysSQL
        if (dto['nveColumn3Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN3, dto['nveColumn3Like'], true)); //For MysSQL
        if (dto['veColumn3LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN3, < string[] > dto['veColumn3LikeList'])); //For MysSQL
        //VE_COLUMN4--字符串字段
        if (dto['veColumn4List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN4, < string[] > dto['veColumn4List']));
        if (dto['nveColumn4List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN4, < string[] > dto['nveColumn4List'], true));
        if (dto['veColumn4Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN4, dto['veColumn4Like'])); //For MysSQL
        if (dto['nveColumn4Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN4, dto['nveColumn4Like'], true)); //For MysSQL
        if (dto['veColumn4LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN4, < string[] > dto['veColumn4LikeList'])); //For MysSQL
        //VE_COLUMN5--字符串字段
        if (dto['veColumn5List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN5, < string[] > dto['veColumn5List']));
        if (dto['nveColumn5List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN5, < string[] > dto['nveColumn5List'], true));
        if (dto['veColumn5Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN5, dto['veColumn5Like'])); //For MysSQL
        if (dto['nveColumn5Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN5, dto['nveColumn5Like'], true)); //For MysSQL
        if (dto['veColumn5LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN5, < string[] > dto['veColumn5LikeList'])); //For MysSQL
        //VE_COLUMN6--字符串字段
        if (dto['veColumn6List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN6, < string[] > dto['veColumn6List']));
        if (dto['nveColumn6List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN6, < string[] > dto['nveColumn6List'], true));
        if (dto['veColumn6Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN6, dto['veColumn6Like'])); //For MysSQL
        if (dto['nveColumn6Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN6, dto['nveColumn6Like'], true)); //For MysSQL
        if (dto['veColumn6LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN6, < string[] > dto['veColumn6LikeList'])); //For MysSQL
        //VE_COLUMN7--字符串字段
        if (dto['veColumn7List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN7, < string[] > dto['veColumn7List']));
        if (dto['nveColumn7List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN7, < string[] > dto['nveColumn7List'], true));
        if (dto['veColumn7Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN7, dto['veColumn7Like'])); //For MysSQL
        if (dto['nveColumn7Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN7, dto['nveColumn7Like'], true)); //For MysSQL
        if (dto['veColumn7LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN7, < string[] > dto['veColumn7LikeList'])); //For MysSQL
        //VE_COLUMN8--字符串字段
        if (dto['veColumn8List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN8, < string[] > dto['veColumn8List']));
        if (dto['nveColumn8List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN8, < string[] > dto['nveColumn8List'], true));
        if (dto['veColumn8Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN8, dto['veColumn8Like'])); //For MysSQL
        if (dto['nveColumn8Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN8, dto['nveColumn8Like'], true)); //For MysSQL
        if (dto['veColumn8LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN8, < string[] > dto['veColumn8LikeList'])); //For MysSQL
        //VE_COLUMN9--字符串字段
        if (dto['veColumn9List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN9, < string[] > dto['veColumn9List']));
        if (dto['nveColumn9List']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_COLUMN9, < string[] > dto['nveColumn9List'], true));
        if (dto['veColumn9Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN9, dto['veColumn9Like'])); //For MysSQL
        if (dto['nveColumn9Like']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_COLUMN9, dto['nveColumn9Like'], true)); //For MysSQL
        if (dto['veColumn9LikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_COLUMN9, < string[] > dto['veColumn9LikeList'])); //For MysSQL
        //VE_SPECIES--字符串字段
        if (dto['veSpeciesList']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_SPECIES, < string[] > dto['veSpeciesList']));
        if (dto['nveSpeciesList']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_SPECIES, < string[] > dto['nveSpeciesList'], true));
        if (dto['veSpeciesLike']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_SPECIES, dto['veSpeciesLike'])); //For MysSQL
        if (dto['nveSpeciesLike']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_SPECIES, dto['nveSpeciesLike'], true)); //For MysSQL
        if (dto['veSpeciesLikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_SPECIES, < string[] > dto['veSpeciesLikeList'])); //For MysSQL
        //VE_REMARK--字符串字段
        if (dto['veRemarkList']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_REMARK, < string[] > dto['veRemarkList']));
        if (dto['nveRemarkList']?.length > 0) items.push(OnpVerificationSQL.IN_SQL(VE_REMARK, < string[] > dto['nveRemarkList'], true));
        if (dto['veRemarkLike']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_REMARK, dto['veRemarkLike'])); //For MysSQL
        if (dto['nveRemarkLike']?.length > 0) items.push(OnpVerificationSQL.LIKE_SQL(VE_REMARK, dto['nveRemarkLike'], true)); //For MysSQL
        if (dto['veRemarkLikeList']?.length > 0) items.push(OnpVerificationSQL.LIKE_LIST_SQL(VE_REMARK, < string[] > dto['veRemarkLikeList'])); //For MysSQL
        //VE_CREATE_DATE--日期字段
        if (dto[veCreateDate]) items.push(`${ VE_CREATE_DATE } is not null and date_format(${ VE_CREATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sVeCreateDate'])}`);
        if (dto[veCreateDate]) items.push(`${ VE_CREATE_DATE } is not null and date_format(${ VE_CREATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eVeCreateDate'])}`);
        //VE_UPDATE_DATE--日期字段
        if (dto[veUpdateDate]) items.push(`${ VE_UPDATE_DATE } is not null and date_format(${ VE_UPDATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sVeUpdateDate'])}`);
        if (dto[veUpdateDate]) items.push(`${ VE_UPDATE_DATE } is not null and date_format(${ VE_UPDATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eVeUpdateDate'])}`);
        return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
    },
    /**
     * 字符串的Like语句，采用前后模糊查询
     * @param colName 列名
     * @param values 值
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
        if (!colName || value.length == 0 || !value || value.length == 0) return '';
        if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
        else return ` ${colName} like CONCAT('%','${value}','%')`;
    },
    /**
     * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
     * @param colName 列名
     * @param values 值列表
     * @param andOr 采用and或or连接多个条件，默认为OR语句
     * @return {string}
     */
    LIKE_LIST_SQL: (colName: string, values: string[], andOr ? : string): string => {
        if (!colName || !values || values.length == 0) return '';
        if (!andOr) andOr = 'OR';
        let items = values.map(value => OnpVerificationSQL.LIKE_SQL(colName, value));
        return `( ${items.join(' ' + andOr + ' ')} )`;
    },
    /**
     * 排序查询SQL语句
     * @param dto
     * @return {string}
     */
    ORDER_BY_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        if (dto['order']?.length > 0) {
            return `order by ${dto['order']}`;
        }
        return '';
    },
    /**
     * 分组查询SQL语句
     * @param dto
     * @return {string}
     */
    GROUP_BY_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        if (dto['group']?.length > 0) {
            return `group by ${dto['group']}`;
        }
        return '';
    },
    /**
     * 分页查询条件SQL语句
     * @param dto
     * @return {string}
     */
    LIMIT_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
            return `limit ${dto['offset']},${dto['pageSize']}`;
        }
        return '';
    },
    /**
     * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
     * @param colName 列名，如:SAM_NAME
     * @param values 数组
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
        if (!values || values.length == 0) return '';
        let items = [];
        Array.isArray(values) ? values.forEach(value => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
        return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
    },
    /**
     * 插入记录SQL语句
     * @param dto
     * @return {string}
     */
    INSERT_SQL: (dto: OnpVerificationCDto | Partial < OnpVerificationCDto > ): string => {
        if (!dto) return '';
        if (!dto[primerKey] || dto[primerKey] == undefined) return '';
        let cols = [];
        let values = [];
        cols.push(VE_ID);
        cols.push(VE_ORDER);
        cols.push(VE_COLUMN1);
        cols.push(VE_COLUMN2);
        cols.push(VE_COLUMN3);
        cols.push(VE_COLUMN4);
        cols.push(VE_COLUMN5);
        cols.push(VE_COLUMN6);
        cols.push(VE_COLUMN7);
        cols.push(VE_COLUMN8);
        cols.push(VE_COLUMN9);
        cols.push(VE_SPECIES);
        cols.push(VE_REMARK);
        cols.push(VE_CREATE_DATE);
        cols.push(VE_UPDATE_DATE);
        values.push(toSqlValue(dto[veId]));
        values.push(toSqlValue(dto[veOrder]));
        values.push(toSqlValue(dto[veColumn1]));
        values.push(toSqlValue(dto[veColumn2]));
        values.push(toSqlValue(dto[veColumn3]));
        values.push(toSqlValue(dto[veColumn4]));
        values.push(toSqlValue(dto[veColumn5]));
        values.push(toSqlValue(dto[veColumn6]));
        values.push(toSqlValue(dto[veColumn7]));
        values.push(toSqlValue(dto[veColumn8]));
        values.push(toSqlValue(dto[veColumn9]));
        values.push(toSqlValue(dto[veSpecies]));
        values.push(toSqlValue(dto[veRemark]));
        values.push('NOW()');
        values.push('NOW()');
        let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
        return sql;
    },
    /**
     * 批量插入SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_INSERT_SQL: (dtos: OnpVerificationCDto[] | Partial < OnpVerificationCDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let cols = [];
        cols.push(VE_ID);
        cols.push(VE_ORDER);
        cols.push(VE_COLUMN1);
        cols.push(VE_COLUMN2);
        cols.push(VE_COLUMN3);
        cols.push(VE_COLUMN4);
        cols.push(VE_COLUMN5);
        cols.push(VE_COLUMN6);
        cols.push(VE_COLUMN7);
        cols.push(VE_COLUMN8);
        cols.push(VE_COLUMN9);
        cols.push(VE_SPECIES);
        cols.push(VE_REMARK);
        cols.push(VE_CREATE_DATE);
        cols.push(VE_UPDATE_DATE);
        let items = [];
        dtos.forEach(dto => {
            let values = [];
            values.push(toSqlValue(dto[veId]));
            values.push(toSqlValue(dto[veOrder]));
            values.push(toSqlValue(dto[veColumn1]));
            values.push(toSqlValue(dto[veColumn2]));
            values.push(toSqlValue(dto[veColumn3]));
            values.push(toSqlValue(dto[veColumn4]));
            values.push(toSqlValue(dto[veColumn5]));
            values.push(toSqlValue(dto[veColumn6]));
            values.push(toSqlValue(dto[veColumn7]));
            values.push(toSqlValue(dto[veColumn8]));
            values.push(toSqlValue(dto[veColumn9]));
            values.push(toSqlValue(dto[veSpecies]));
            values.push(toSqlValue(dto[veRemark]));
            values.push('NOW()');
            values.push('NOW()');
            items.push(`(${values.join(',')})`);
        });
        return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
    },
    /**
     * 根据ID更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SQL: (dto: OnpVerificationUDto | Partial < OnpVerificationUDto > ): string => {
        if (!dto) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpVerificationColNames, OnpVerificationColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ VE_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ VE_ORDER } = ${toSqlValue(dto[veOrder])}`);
        items.push(`${ VE_COLUMN1 } = ${toSqlValue(dto[veColumn1])}`);
        items.push(`${ VE_COLUMN2 } = ${toSqlValue(dto[veColumn2])}`);
        items.push(`${ VE_COLUMN3 } = ${toSqlValue(dto[veColumn3])}`);
        items.push(`${ VE_COLUMN4 } = ${toSqlValue(dto[veColumn4])}`);
        items.push(`${ VE_COLUMN5 } = ${toSqlValue(dto[veColumn5])}`);
        items.push(`${ VE_COLUMN6 } = ${toSqlValue(dto[veColumn6])}`);
        items.push(`${ VE_COLUMN7 } = ${toSqlValue(dto[veColumn7])}`);
        items.push(`${ VE_COLUMN8 } = ${toSqlValue(dto[veColumn8])}`);
        items.push(`${ VE_COLUMN9 } = ${toSqlValue(dto[veColumn9])}`);
        items.push(`${ VE_SPECIES } = ${toSqlValue(dto[veSpecies])}`);
        items.push(`${ VE_REMARK } = ${toSqlValue(dto[veRemark])}`);
        items.push(`${ VE_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID列表批量更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_IDS_SQL: (dto: OnpVerificationUDto | Partial < OnpVerificationUDto > ): string => {
        if (!dto) return '';
        //获取ID列表
        let ids = < string[] > dto[`${primerKey}List`];
        if (!ids || ids.length == 0) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpVerificationColNames, OnpVerificationColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ VE_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpVerificationSQL.IN_SQL(PRIMER_KEY, ids)}`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ VE_ORDER } = ${toSqlValue(dto[veOrder])}`);
        items.push(`${ VE_COLUMN1 } = ${toSqlValue(dto[veColumn1])}`);
        items.push(`${ VE_COLUMN2 } = ${toSqlValue(dto[veColumn2])}`);
        items.push(`${ VE_COLUMN3 } = ${toSqlValue(dto[veColumn3])}`);
        items.push(`${ VE_COLUMN4 } = ${toSqlValue(dto[veColumn4])}`);
        items.push(`${ VE_COLUMN5 } = ${toSqlValue(dto[veColumn5])}`);
        items.push(`${ VE_COLUMN6 } = ${toSqlValue(dto[veColumn6])}`);
        items.push(`${ VE_COLUMN7 } = ${toSqlValue(dto[veColumn7])}`);
        items.push(`${ VE_COLUMN8 } = ${toSqlValue(dto[veColumn8])}`);
        items.push(`${ VE_COLUMN9 } = ${toSqlValue(dto[veColumn9])}`);
        items.push(`${ VE_SPECIES } = ${toSqlValue(dto[veSpecies])}`);
        items.push(`${ VE_REMARK } = ${toSqlValue(dto[veRemark])}`);
        items.push(`${ VE_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpVerificationSQL.IN_SQL(PRIMER_KEY, ids)}`;
    },
    /**
     * 根据ID更新所有属性值的批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SQL: (dtos: OnpVerificationUDto[] | Partial < OnpVerificationUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let sqls = [];
        dtos.forEach(value => {
            sqls.push(OnpVerificationSQL.UPDATE_BY_ID_SQL(value));
        });
        return sqls.join(';');
    },
    /**
     * 根据ID更新有值属性值的条件更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SELECTIVE_SQL: (dto: OnpVerificationUDto | Partial < OnpVerificationUDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[veOrder]) items.push(`${ VE_ORDER } = ${toSqlValue(dto[veOrder])}`);
        if (dto[veColumn1]?.length > 0) items.push(`${ VE_COLUMN1 } = ${toSqlValue(dto[veColumn1])}`);
        if (dto[veColumn2]?.length > 0) items.push(`${ VE_COLUMN2 } = ${toSqlValue(dto[veColumn2])}`);
        if (dto[veColumn3]?.length > 0) items.push(`${ VE_COLUMN3 } = ${toSqlValue(dto[veColumn3])}`);
        if (dto[veColumn4]?.length > 0) items.push(`${ VE_COLUMN4 } = ${toSqlValue(dto[veColumn4])}`);
        if (dto[veColumn5]?.length > 0) items.push(`${ VE_COLUMN5 } = ${toSqlValue(dto[veColumn5])}`);
        if (dto[veColumn6]?.length > 0) items.push(`${ VE_COLUMN6 } = ${toSqlValue(dto[veColumn6])}`);
        if (dto[veColumn7]?.length > 0) items.push(`${ VE_COLUMN7 } = ${toSqlValue(dto[veColumn7])}`);
        if (dto[veColumn8]?.length > 0) items.push(`${ VE_COLUMN8 } = ${toSqlValue(dto[veColumn8])}`);
        if (dto[veColumn9]?.length > 0) items.push(`${ VE_COLUMN9 } = ${toSqlValue(dto[veColumn9])}`);
        if (dto[veSpecies]?.length > 0) items.push(`${ VE_SPECIES } = ${toSqlValue(dto[veSpecies])}`);
        if (dto[veRemark]?.length > 0) items.push(`${ VE_REMARK } = ${toSqlValue(dto[veRemark])}`);
        if (dto[veUpdateDate]) items.push(`${  VE_UPDATE_DATE }= NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID更新有值属性值的条件批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: OnpVerificationUDto[] | Partial < OnpVerificationUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let items = [];
        dtos.forEach(value => items.push(OnpVerificationSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
        return items.join(';');
    },
    /**
     * 条件删除SQL语句
     * @param dtos
     * @return {string}
     */
    DELETE_SELECTIVE_SQL: (dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): string => {
        if (!dto) return '';
        let where = OnpVerificationSQL.WHERE_SQL(dto);
        return `DELETE FROM ${TABLE} ${where}`;
    },
    /**
     * 删除
     * @param dto
     * @return {string}
     */
    DELETE_BY_ID_SQL: (id: string): string => {
        if (!id) return '';
        return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
    },
    /**
     *
     * @param dto
     * @return {string}
     */
    DELETE_BY_IDS_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        let inSql = OnpVerificationSQL.IN_SQL(PRIMER_KEY, idList);
        return `DELETE FROM ${TABLE} WHERE ${inSql}`;
    },
    /**
     * 删除所有记录的SQL语句
     */
    DELETE_ALL_SQL: (): string => {
        return `DELETE FROM ${TABLE}`;
    },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'ONP_VERIFICATION';
/**
 * 列名常量字段
 */
const VE_ID = OnpVerificationColNameEnum.VE_ID;
const VE_ORDER = OnpVerificationColNameEnum.VE_ORDER;
const VE_COLUMN1 = OnpVerificationColNameEnum.VE_COLUMN1;
const VE_COLUMN2 = OnpVerificationColNameEnum.VE_COLUMN2;
const VE_COLUMN3 = OnpVerificationColNameEnum.VE_COLUMN3;
const VE_COLUMN4 = OnpVerificationColNameEnum.VE_COLUMN4;
const VE_COLUMN5 = OnpVerificationColNameEnum.VE_COLUMN5;
const VE_COLUMN6 = OnpVerificationColNameEnum.VE_COLUMN6;
const VE_COLUMN7 = OnpVerificationColNameEnum.VE_COLUMN7;
const VE_COLUMN8 = OnpVerificationColNameEnum.VE_COLUMN8;
const VE_COLUMN9 = OnpVerificationColNameEnum.VE_COLUMN9;
const VE_SPECIES = OnpVerificationColNameEnum.VE_SPECIES;
const VE_REMARK = OnpVerificationColNameEnum.VE_REMARK;
const VE_CREATE_DATE = OnpVerificationColNameEnum.VE_CREATE_DATE;
const VE_UPDATE_DATE = OnpVerificationColNameEnum.VE_UPDATE_DATE;
/**
 * 实体类属性名
 */
const veId = OnpVerificationColPropEnum.veId;
const veOrder = OnpVerificationColPropEnum.veOrder;
const veColumn1 = OnpVerificationColPropEnum.veColumn1;
const veColumn2 = OnpVerificationColPropEnum.veColumn2;
const veColumn3 = OnpVerificationColPropEnum.veColumn3;
const veColumn4 = OnpVerificationColPropEnum.veColumn4;
const veColumn5 = OnpVerificationColPropEnum.veColumn5;
const veColumn6 = OnpVerificationColPropEnum.veColumn6;
const veColumn7 = OnpVerificationColPropEnum.veColumn7;
const veColumn8 = OnpVerificationColPropEnum.veColumn8;
const veColumn9 = OnpVerificationColPropEnum.veColumn9;
const veSpecies = OnpVerificationColPropEnum.veSpecies;
const veRemark = OnpVerificationColPropEnum.veRemark;
const veCreateDate = OnpVerificationColPropEnum.veCreateDate;
const veUpdateDate = OnpVerificationColPropEnum.veUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = OnpVerificationTable.PRIMER_KEY;
const primerKey = OnpVerificationTable.primerKey;