import { OnpForecastQDto } from '../../dto/query/onp.forecast.qdto';
import { OnpForecastCDto } from '../../dto/create/onp.forecast.cdto';
import { OnpForecastUDto } from '../../dto/update/onp.forecast.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { OnpForecastColNameEnum, OnpForecastColPropEnum, OnpForecastTable, OnpForecastColNames, OnpForecastColProps } from '../ddl/onp.forecast.cols';
/**
 * ONP_FORECAST--预测表相关SQL定义
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpForecastSQL
 */
export const OnpForecastSQL = {
    /**
     * 构建根据有值属性查询SQL
     * @param dto
     * @return {string}
     */
    SELECT_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let cols = OnpForecastSQL.COL_MAPPER_SQL(dto);
        let where = OnpForecastSQL.WHERE_SQL(dto);
        let group = OnpForecastSQL.GROUP_BY_SQL(dto);
        let order = OnpForecastSQL.ORDER_BY_SQL(dto);
        let limit = OnpForecastSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询记录数SQL
     * @param dto
     * @return {string}
     */
    SELECT_COUNT_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let where = OnpForecastSQL.WHERE_SQL(dto);
        let group = OnpForecastSQL.GROUP_BY_SQL(dto);
        return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
    },
    /**
     * 根据ID查询记录SQL
     * @param dto
     * @return {string}
     */
    SELECT_BY_ID_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let cols = OnpForecastSQL.COL_MAPPER_SQL(dto);
        let group = OnpForecastSQL.GROUP_BY_SQL(dto);
        let order = OnpForecastSQL.ORDER_BY_SQL(dto);
        let limit = OnpForecastSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
    },
    /**
     * 根据ID列表查询记录SQL
     * @param idList
     * @return {string}
     */
    SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
        if (!idList || idList.length == 0) return '';
        let sql = OnpForecastSQL.COL_MAPPER_SQL({ cols });
        return `SELECT ${sql} FROM ${TABLE} WHERE ${ OnpForecastSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询数据库中已存在ID的SQL语句
     * @param idList
     * @return {string}
     */
    SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${ OnpForecastSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询所有ID列表SQL语句
     * @return {string}
     */
    SELECT_ALL_ID_LIST_SQL: (): string => {
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
    },
    /**
     * 条件查询ID列表SQL语句
     * @param dto
     * @return {string}
     */
    SELECT_ID_LIST_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let where = OnpForecastSQL.WHERE_SQL(dto);
        let group = OnpForecastSQL.GROUP_BY_SQL(dto);
        let order = OnpForecastSQL.ORDER_BY_SQL(dto);
        let limit = OnpForecastSQL.LIMIT_SQL(dto);
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询结果列映射SQL
     * @param dto
     * @return {string}
     */
    COL_MAPPER_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let cols = StringUtils.toColName(dto.cols, OnpForecastColNames, OnpForecastColProps);
        if (!cols || cols.length == 0) cols = OnpForecastColNames;
        let sql = cols.map(col => `${ col } as ${StringUtils.camelCase(col)}`).join(',');
        return dto.distinct ? `DISTINCT ${sql}` : sql;
    },
    /**
     * Where语句，根据有值属性查询
     * @param dto
     * @return {string}
     */
    WHERE_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[foId]?.length > 0) items.push(` ${ FO_ID } = '${dto[foId]}'`);
        if (dto['nfoId']?.length > 0) items.push(` ${ FO_ID } != '${dto['nfoId']}'`);
        if (dto[foOrder]) items.push(` ${ FO_ORDER } = '${dto[foOrder]}'`);
        if (dto['nfoOrder']) items.push(` ${ FO_ORDER } != '${dto['nfoOrder']}'`);
        if (dto[foColumn1]?.length > 0) items.push(` ${ FO_COLUMN1 } = '${dto[foColumn1]}'`);
        if (dto['nfoColumn1']?.length > 0) items.push(` ${ FO_COLUMN1 } != '${dto['nfoColumn1']}'`);
        if (dto[foColumn2]?.length > 0) items.push(` ${ FO_COLUMN2 } = '${dto[foColumn2]}'`);
        if (dto['nfoColumn2']?.length > 0) items.push(` ${ FO_COLUMN2 } != '${dto['nfoColumn2']}'`);
        if (dto[foColumn3]?.length > 0) items.push(` ${ FO_COLUMN3 } = '${dto[foColumn3]}'`);
        if (dto['nfoColumn3']?.length > 0) items.push(` ${ FO_COLUMN3 } != '${dto['nfoColumn3']}'`);
        if (dto[foColumn4]?.length > 0) items.push(` ${ FO_COLUMN4 } = '${dto[foColumn4]}'`);
        if (dto['nfoColumn4']?.length > 0) items.push(` ${ FO_COLUMN4 } != '${dto['nfoColumn4']}'`);
        if (dto[foColumn5]?.length > 0) items.push(` ${ FO_COLUMN5 } = '${dto[foColumn5]}'`);
        if (dto['nfoColumn5']?.length > 0) items.push(` ${ FO_COLUMN5 } != '${dto['nfoColumn5']}'`);
        if (dto[foColumn6]?.length > 0) items.push(` ${ FO_COLUMN6 } = '${dto[foColumn6]}'`);
        if (dto['nfoColumn6']?.length > 0) items.push(` ${ FO_COLUMN6 } != '${dto['nfoColumn6']}'`);
        if (dto[foSpecies]?.length > 0) items.push(` ${ FO_SPECIES } = '${dto[foSpecies]}'`);
        if (dto['nfoSpecies']?.length > 0) items.push(` ${ FO_SPECIES } != '${dto['nfoSpecies']}'`);
        if (dto[foRemark]?.length > 0) items.push(` ${ FO_REMARK } = '${dto[foRemark]}'`);
        if (dto['nfoRemark']?.length > 0) items.push(` ${ FO_REMARK } != '${dto['nfoRemark']}'`);
        if (dto[foCreateDate]) items.push(`${ FO_CREATE_DATE } is not null and date_format(${ FO_CREATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[foCreateDate])}`);
        if (dto['nfoCreateDate']) items.push(`${ FO_CREATE_DATE } is not null and date_format(${ FO_CREATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nfoCreateDate'])}`);
        if (dto[foUpdateDate]) items.push(`${ FO_UPDATE_DATE } is not null and date_format(${ FO_UPDATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[foUpdateDate])}`);
        if (dto['nfoUpdateDate']) items.push(`${ FO_UPDATE_DATE } is not null and date_format(${ FO_UPDATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nfoUpdateDate'])}`);
        //数组、模糊条件、年份日期范围等额外查询条件
        //FO_ID--字符串字段
        if (dto['foIdList']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_ID, < string[] > dto['foIdList']));
        if (dto['nfoIdList']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_ID, < string[] > dto['nfoIdList'], true));
        if (dto['foIdLike']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_ID, dto['foIdLike'])); //For MysSQL
        if (dto['nfoIdLike']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_ID, dto['nfoIdLike'], true)); //For MysSQL
        if (dto['foIdLikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_ID, < string[] > dto['foIdLikeList'])); //For MysSQL
        //FO_ORDER--数字字段
        if (dto['foOrderList']) items.push(OnpForecastSQL.IN_SQL(FO_ORDER, dto['foOrderList']));
        if (dto['nfoOrderList']) items.push(OnpForecastSQL.IN_SQL(FO_ORDER, dto['nfoOrderList'], true));
        //FO_COLUMN1--字符串字段
        if (dto['foColumn1List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN1, < string[] > dto['foColumn1List']));
        if (dto['nfoColumn1List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN1, < string[] > dto['nfoColumn1List'], true));
        if (dto['foColumn1Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN1, dto['foColumn1Like'])); //For MysSQL
        if (dto['nfoColumn1Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN1, dto['nfoColumn1Like'], true)); //For MysSQL
        if (dto['foColumn1LikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_COLUMN1, < string[] > dto['foColumn1LikeList'])); //For MysSQL
        //FO_COLUMN2--字符串字段
        if (dto['foColumn2List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN2, < string[] > dto['foColumn2List']));
        if (dto['nfoColumn2List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN2, < string[] > dto['nfoColumn2List'], true));
        if (dto['foColumn2Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN2, dto['foColumn2Like'])); //For MysSQL
        if (dto['nfoColumn2Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN2, dto['nfoColumn2Like'], true)); //For MysSQL
        if (dto['foColumn2LikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_COLUMN2, < string[] > dto['foColumn2LikeList'])); //For MysSQL
        //FO_COLUMN3--字符串字段
        if (dto['foColumn3List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN3, < string[] > dto['foColumn3List']));
        if (dto['nfoColumn3List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN3, < string[] > dto['nfoColumn3List'], true));
        if (dto['foColumn3Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN3, dto['foColumn3Like'])); //For MysSQL
        if (dto['nfoColumn3Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN3, dto['nfoColumn3Like'], true)); //For MysSQL
        if (dto['foColumn3LikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_COLUMN3, < string[] > dto['foColumn3LikeList'])); //For MysSQL
        //FO_COLUMN4--字符串字段
        if (dto['foColumn4List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN4, < string[] > dto['foColumn4List']));
        if (dto['nfoColumn4List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN4, < string[] > dto['nfoColumn4List'], true));
        if (dto['foColumn4Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN4, dto['foColumn4Like'])); //For MysSQL
        if (dto['nfoColumn4Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN4, dto['nfoColumn4Like'], true)); //For MysSQL
        if (dto['foColumn4LikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_COLUMN4, < string[] > dto['foColumn4LikeList'])); //For MysSQL
        //FO_COLUMN5--字符串字段
        if (dto['foColumn5List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN5, < string[] > dto['foColumn5List']));
        if (dto['nfoColumn5List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN5, < string[] > dto['nfoColumn5List'], true));
        if (dto['foColumn5Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN5, dto['foColumn5Like'])); //For MysSQL
        if (dto['nfoColumn5Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN5, dto['nfoColumn5Like'], true)); //For MysSQL
        if (dto['foColumn5LikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_COLUMN5, < string[] > dto['foColumn5LikeList'])); //For MysSQL
        //FO_COLUMN6--字符串字段
        if (dto['foColumn6List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN6, < string[] > dto['foColumn6List']));
        if (dto['nfoColumn6List']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_COLUMN6, < string[] > dto['nfoColumn6List'], true));
        if (dto['foColumn6Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN6, dto['foColumn6Like'])); //For MysSQL
        if (dto['nfoColumn6Like']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_COLUMN6, dto['nfoColumn6Like'], true)); //For MysSQL
        if (dto['foColumn6LikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_COLUMN6, < string[] > dto['foColumn6LikeList'])); //For MysSQL
        //FO_SPECIES--字符串字段
        if (dto['foSpeciesList']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_SPECIES, < string[] > dto['foSpeciesList']));
        if (dto['nfoSpeciesList']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_SPECIES, < string[] > dto['nfoSpeciesList'], true));
        if (dto['foSpeciesLike']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_SPECIES, dto['foSpeciesLike'])); //For MysSQL
        if (dto['nfoSpeciesLike']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_SPECIES, dto['nfoSpeciesLike'], true)); //For MysSQL
        if (dto['foSpeciesLikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_SPECIES, < string[] > dto['foSpeciesLikeList'])); //For MysSQL
        //FO_REMARK--字符串字段
        if (dto['foRemarkList']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_REMARK, < string[] > dto['foRemarkList']));
        if (dto['nfoRemarkList']?.length > 0) items.push(OnpForecastSQL.IN_SQL(FO_REMARK, < string[] > dto['nfoRemarkList'], true));
        if (dto['foRemarkLike']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_REMARK, dto['foRemarkLike'])); //For MysSQL
        if (dto['nfoRemarkLike']?.length > 0) items.push(OnpForecastSQL.LIKE_SQL(FO_REMARK, dto['nfoRemarkLike'], true)); //For MysSQL
        if (dto['foRemarkLikeList']?.length > 0) items.push(OnpForecastSQL.LIKE_LIST_SQL(FO_REMARK, < string[] > dto['foRemarkLikeList'])); //For MysSQL
        //FO_CREATE_DATE--日期字段
        if (dto[foCreateDate]) items.push(`${ FO_CREATE_DATE } is not null and date_format(${ FO_CREATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sFoCreateDate'])}`);
        if (dto[foCreateDate]) items.push(`${ FO_CREATE_DATE } is not null and date_format(${ FO_CREATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eFoCreateDate'])}`);
        //FO_UPDATE_DATE--日期字段
        if (dto[foUpdateDate]) items.push(`${ FO_UPDATE_DATE } is not null and date_format(${ FO_UPDATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sFoUpdateDate'])}`);
        if (dto[foUpdateDate]) items.push(`${ FO_UPDATE_DATE } is not null and date_format(${ FO_UPDATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eFoUpdateDate'])}`);
        return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
    },
    /**
     * 字符串的Like语句，采用前后模糊查询
     * @param colName 列名
     * @param values 值
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
        if (!colName || value.length == 0 || !value || value.length == 0) return '';
        if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
        else return ` ${colName} like CONCAT('%','${value}','%')`;
    },
    /**
     * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
     * @param colName 列名
     * @param values 值列表
     * @param andOr 采用and或or连接多个条件，默认为OR语句
     * @return {string}
     */
    LIKE_LIST_SQL: (colName: string, values: string[], andOr ? : string): string => {
        if (!colName || !values || values.length == 0) return '';
        if (!andOr) andOr = 'OR';
        let items = values.map(value => OnpForecastSQL.LIKE_SQL(colName, value));
        return `( ${items.join(' ' + andOr + ' ')} )`;
    },
    /**
     * 排序查询SQL语句
     * @param dto
     * @return {string}
     */
    ORDER_BY_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        if (dto['order']?.length > 0) {
            return `order by ${dto['order']}`;
        }
        return '';
    },
    /**
     * 分组查询SQL语句
     * @param dto
     * @return {string}
     */
    GROUP_BY_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        if (dto['group']?.length > 0) {
            return `group by ${dto['group']}`;
        }
        return '';
    },
    /**
     * 分页查询条件SQL语句
     * @param dto
     * @return {string}
     */
    LIMIT_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
            return `limit ${dto['offset']},${dto['pageSize']}`;
        }
        return '';
    },
    /**
     * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
     * @param colName 列名，如:SAM_NAME
     * @param values 数组
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
        if (!values || values.length == 0) return '';
        let items = [];
        Array.isArray(values) ? values.forEach(value => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
        return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
    },
    /**
     * 插入记录SQL语句
     * @param dto
     * @return {string}
     */
    INSERT_SQL: (dto: OnpForecastCDto | Partial < OnpForecastCDto > ): string => {
        if (!dto) return '';
        if (!dto[primerKey] || dto[primerKey] == undefined) return '';
        let cols = [];
        let values = [];
        cols.push(FO_ID);
        cols.push(FO_ORDER);
        cols.push(FO_COLUMN1);
        cols.push(FO_COLUMN2);
        cols.push(FO_COLUMN3);
        cols.push(FO_COLUMN4);
        cols.push(FO_COLUMN5);
        cols.push(FO_COLUMN6);
        cols.push(FO_SPECIES);
        cols.push(FO_REMARK);
        cols.push(FO_CREATE_DATE);
        cols.push(FO_UPDATE_DATE);
        values.push(toSqlValue(dto[foId]));
        values.push(toSqlValue(dto[foOrder]));
        values.push(toSqlValue(dto[foColumn1]));
        values.push(toSqlValue(dto[foColumn2]));
        values.push(toSqlValue(dto[foColumn3]));
        values.push(toSqlValue(dto[foColumn4]));
        values.push(toSqlValue(dto[foColumn5]));
        values.push(toSqlValue(dto[foColumn6]));
        values.push(toSqlValue(dto[foSpecies]));
        values.push(toSqlValue(dto[foRemark]));
        values.push('NOW()');
        values.push('NOW()');
        let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
        return sql;
    },
    /**
     * 批量插入SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_INSERT_SQL: (dtos: OnpForecastCDto[] | Partial < OnpForecastCDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let cols = [];
        cols.push(FO_ID);
        cols.push(FO_ORDER);
        cols.push(FO_COLUMN1);
        cols.push(FO_COLUMN2);
        cols.push(FO_COLUMN3);
        cols.push(FO_COLUMN4);
        cols.push(FO_COLUMN5);
        cols.push(FO_COLUMN6);
        cols.push(FO_SPECIES);
        cols.push(FO_REMARK);
        cols.push(FO_CREATE_DATE);
        cols.push(FO_UPDATE_DATE);
        let items = [];
        dtos.forEach(dto => {
            let values = [];
            values.push(toSqlValue(dto[foId]));
            values.push(toSqlValue(dto[foOrder]));
            values.push(toSqlValue(dto[foColumn1]));
            values.push(toSqlValue(dto[foColumn2]));
            values.push(toSqlValue(dto[foColumn3]));
            values.push(toSqlValue(dto[foColumn4]));
            values.push(toSqlValue(dto[foColumn5]));
            values.push(toSqlValue(dto[foColumn6]));
            values.push(toSqlValue(dto[foSpecies]));
            values.push(toSqlValue(dto[foRemark]));
            values.push('NOW()');
            values.push('NOW()');
            items.push(`(${values.join(',')})`);
        });
        return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
    },
    /**
     * 根据ID更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SQL: (dto: OnpForecastUDto | Partial < OnpForecastUDto > ): string => {
        if (!dto) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpForecastColNames, OnpForecastColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ FO_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ FO_ORDER } = ${toSqlValue(dto[foOrder])}`);
        items.push(`${ FO_COLUMN1 } = ${toSqlValue(dto[foColumn1])}`);
        items.push(`${ FO_COLUMN2 } = ${toSqlValue(dto[foColumn2])}`);
        items.push(`${ FO_COLUMN3 } = ${toSqlValue(dto[foColumn3])}`);
        items.push(`${ FO_COLUMN4 } = ${toSqlValue(dto[foColumn4])}`);
        items.push(`${ FO_COLUMN5 } = ${toSqlValue(dto[foColumn5])}`);
        items.push(`${ FO_COLUMN6 } = ${toSqlValue(dto[foColumn6])}`);
        items.push(`${ FO_SPECIES } = ${toSqlValue(dto[foSpecies])}`);
        items.push(`${ FO_REMARK } = ${toSqlValue(dto[foRemark])}`);
        items.push(`${ FO_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID列表批量更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_IDS_SQL: (dto: OnpForecastUDto | Partial < OnpForecastUDto > ): string => {
        if (!dto) return '';
        //获取ID列表
        let ids = < string[] > dto[`${primerKey}List`];
        if (!ids || ids.length == 0) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpForecastColNames, OnpForecastColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ FO_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpForecastSQL.IN_SQL(PRIMER_KEY, ids)}`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ FO_ORDER } = ${toSqlValue(dto[foOrder])}`);
        items.push(`${ FO_COLUMN1 } = ${toSqlValue(dto[foColumn1])}`);
        items.push(`${ FO_COLUMN2 } = ${toSqlValue(dto[foColumn2])}`);
        items.push(`${ FO_COLUMN3 } = ${toSqlValue(dto[foColumn3])}`);
        items.push(`${ FO_COLUMN4 } = ${toSqlValue(dto[foColumn4])}`);
        items.push(`${ FO_COLUMN5 } = ${toSqlValue(dto[foColumn5])}`);
        items.push(`${ FO_COLUMN6 } = ${toSqlValue(dto[foColumn6])}`);
        items.push(`${ FO_SPECIES } = ${toSqlValue(dto[foSpecies])}`);
        items.push(`${ FO_REMARK } = ${toSqlValue(dto[foRemark])}`);
        items.push(`${ FO_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpForecastSQL.IN_SQL(PRIMER_KEY, ids)}`;
    },
    /**
     * 根据ID更新所有属性值的批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SQL: (dtos: OnpForecastUDto[] | Partial < OnpForecastUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let sqls = [];
        dtos.forEach(value => {
            sqls.push(OnpForecastSQL.UPDATE_BY_ID_SQL(value));
        });
        return sqls.join(';');
    },
    /**
     * 根据ID更新有值属性值的条件更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SELECTIVE_SQL: (dto: OnpForecastUDto | Partial < OnpForecastUDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[foOrder]) items.push(`${ FO_ORDER } = ${toSqlValue(dto[foOrder])}`);
        if (dto[foColumn1]?.length > 0) items.push(`${ FO_COLUMN1 } = ${toSqlValue(dto[foColumn1])}`);
        if (dto[foColumn2]?.length > 0) items.push(`${ FO_COLUMN2 } = ${toSqlValue(dto[foColumn2])}`);
        if (dto[foColumn3]?.length > 0) items.push(`${ FO_COLUMN3 } = ${toSqlValue(dto[foColumn3])}`);
        if (dto[foColumn4]?.length > 0) items.push(`${ FO_COLUMN4 } = ${toSqlValue(dto[foColumn4])}`);
        if (dto[foColumn5]?.length > 0) items.push(`${ FO_COLUMN5 } = ${toSqlValue(dto[foColumn5])}`);
        if (dto[foColumn6]?.length > 0) items.push(`${ FO_COLUMN6 } = ${toSqlValue(dto[foColumn6])}`);
        if (dto[foSpecies]?.length > 0) items.push(`${ FO_SPECIES } = ${toSqlValue(dto[foSpecies])}`);
        if (dto[foRemark]?.length > 0) items.push(`${ FO_REMARK } = ${toSqlValue(dto[foRemark])}`);
        if (dto[foUpdateDate]) items.push(`${  FO_UPDATE_DATE }= NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID更新有值属性值的条件批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: OnpForecastUDto[] | Partial < OnpForecastUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let items = [];
        dtos.forEach(value => items.push(OnpForecastSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
        return items.join(';');
    },
    /**
     * 条件删除SQL语句
     * @param dtos
     * @return {string}
     */
    DELETE_SELECTIVE_SQL: (dto: OnpForecastQDto | Partial < OnpForecastQDto > ): string => {
        if (!dto) return '';
        let where = OnpForecastSQL.WHERE_SQL(dto);
        return `DELETE FROM ${TABLE} ${where}`;
    },
    /**
     * 删除
     * @param dto
     * @return {string}
     */
    DELETE_BY_ID_SQL: (id: string): string => {
        if (!id) return '';
        return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
    },
    /**
     *
     * @param dto
     * @return {string}
     */
    DELETE_BY_IDS_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        let inSql = OnpForecastSQL.IN_SQL(PRIMER_KEY, idList);
        return `DELETE FROM ${TABLE} WHERE ${inSql}`;
    },
    /**
     * 删除所有记录的SQL语句
     */
    DELETE_ALL_SQL: (): string => {
        return `DELETE FROM ${TABLE}`;
    },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'ONP_FORECAST';
/**
 * 列名常量字段
 */
const FO_ID = OnpForecastColNameEnum.FO_ID;
const FO_ORDER = OnpForecastColNameEnum.FO_ORDER;
const FO_COLUMN1 = OnpForecastColNameEnum.FO_COLUMN1;
const FO_COLUMN2 = OnpForecastColNameEnum.FO_COLUMN2;
const FO_COLUMN3 = OnpForecastColNameEnum.FO_COLUMN3;
const FO_COLUMN4 = OnpForecastColNameEnum.FO_COLUMN4;
const FO_COLUMN5 = OnpForecastColNameEnum.FO_COLUMN5;
const FO_COLUMN6 = OnpForecastColNameEnum.FO_COLUMN6;
const FO_SPECIES = OnpForecastColNameEnum.FO_SPECIES;
const FO_REMARK = OnpForecastColNameEnum.FO_REMARK;
const FO_CREATE_DATE = OnpForecastColNameEnum.FO_CREATE_DATE;
const FO_UPDATE_DATE = OnpForecastColNameEnum.FO_UPDATE_DATE;
/**
 * 实体类属性名
 */
const foId = OnpForecastColPropEnum.foId;
const foOrder = OnpForecastColPropEnum.foOrder;
const foColumn1 = OnpForecastColPropEnum.foColumn1;
const foColumn2 = OnpForecastColPropEnum.foColumn2;
const foColumn3 = OnpForecastColPropEnum.foColumn3;
const foColumn4 = OnpForecastColPropEnum.foColumn4;
const foColumn5 = OnpForecastColPropEnum.foColumn5;
const foColumn6 = OnpForecastColPropEnum.foColumn6;
const foSpecies = OnpForecastColPropEnum.foSpecies;
const foRemark = OnpForecastColPropEnum.foRemark;
const foCreateDate = OnpForecastColPropEnum.foCreateDate;
const foUpdateDate = OnpForecastColPropEnum.foUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = OnpForecastTable.PRIMER_KEY;
const primerKey = OnpForecastTable.primerKey;