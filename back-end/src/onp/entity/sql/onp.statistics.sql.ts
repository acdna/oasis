import { OnpStatisticsQDto } from '../../dto/query/onp.statistics.qdto';
import { OnpStatisticsCDto } from '../../dto/create/onp.statistics.cdto';
import { OnpStatisticsUDto } from '../../dto/update/onp.statistics.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { OnpStatisticsColNameEnum, OnpStatisticsColPropEnum, OnpStatisticsTable, OnpStatisticsColNames, OnpStatisticsColProps } from '../ddl/onp.statistics.cols';
/**
 * ONP_STATISTICS--ONP统计数据表表相关SQL定义
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpStatisticsSQL
 */
export const OnpStatisticsSQL = {
    /**
     * 构建根据有值属性查询SQL
     * @param dto
     * @return {string}
     */
    SELECT_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let cols = OnpStatisticsSQL.COL_MAPPER_SQL(dto);
        let where = OnpStatisticsSQL.WHERE_SQL(dto);
        let group = OnpStatisticsSQL.GROUP_BY_SQL(dto);
        let order = OnpStatisticsSQL.ORDER_BY_SQL(dto);
        let limit = OnpStatisticsSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询记录数SQL
     * @param dto
     * @return {string}
     */
    SELECT_COUNT_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let where = OnpStatisticsSQL.WHERE_SQL(dto);
        let group = OnpStatisticsSQL.GROUP_BY_SQL(dto);
        return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
    },
    /**
     * 根据ID查询记录SQL
     * @param dto
     * @return {string}
     */
    SELECT_BY_ID_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let cols = OnpStatisticsSQL.COL_MAPPER_SQL(dto);
        let group = OnpStatisticsSQL.GROUP_BY_SQL(dto);
        let order = OnpStatisticsSQL.ORDER_BY_SQL(dto);
        let limit = OnpStatisticsSQL.LIMIT_SQL(dto);
        return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
    },
    /**
     * 根据ID列表查询记录SQL
     * @param idList
     * @return {string}
     */
    SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
        if (!idList || idList.length == 0) return '';
        let sql = OnpStatisticsSQL.COL_MAPPER_SQL({ cols });
        return `SELECT ${sql} FROM ${TABLE} WHERE ${ OnpStatisticsSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询数据库中已存在ID的SQL语句
     * @param idList
     * @return {string}
     */
    SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${ OnpStatisticsSQL.IN_SQL(PRIMER_KEY, idList)}`;
    },
    /**
     * 查询所有ID列表SQL语句
     * @return {string}
     */
    SELECT_ALL_ID_LIST_SQL: (): string => {
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
    },
    /**
     * 条件查询ID列表SQL语句
     * @param dto
     * @return {string}
     */
    SELECT_ID_LIST_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let where = OnpStatisticsSQL.WHERE_SQL(dto);
        let group = OnpStatisticsSQL.GROUP_BY_SQL(dto);
        let order = OnpStatisticsSQL.ORDER_BY_SQL(dto);
        let limit = OnpStatisticsSQL.LIMIT_SQL(dto);
        return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
    },
    /**
     * 查询结果列映射SQL
     * @param dto
     * @return {string}
     */
    COL_MAPPER_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let cols = StringUtils.toColName(dto.cols, OnpStatisticsColNames, OnpStatisticsColProps);
        if (!cols || cols.length == 0) cols = OnpStatisticsColNames;
        let sql = cols.map(col => `${ col } as ${StringUtils.camelCase(col)}`).join(',');
        return dto.distinct ? `DISTINCT ${sql}` : sql;
    },
    /**
     * Where语句，根据有值属性查询
     * @param dto
     * @return {string}
     */
    WHERE_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[stId]?.length > 0) items.push(` ${ ST_ID } = '${dto[stId]}'`);
        if (dto['nstId']?.length > 0) items.push(` ${ ST_ID } != '${dto['nstId']}'`);
        if (dto[stBin]?.length > 0) items.push(` ${ ST_BIN } = '${dto[stBin]}'`);
        if (dto['nstBin']?.length > 0) items.push(` ${ ST_BIN } != '${dto['nstBin']}'`);
        if (dto[stChr]?.length > 0) items.push(` ${ ST_CHR } = '${dto[stChr]}'`);
        if (dto['nstChr']?.length > 0) items.push(` ${ ST_CHR } != '${dto['nstChr']}'`);
        if (dto[stStart]?.length > 0) items.push(` ${ ST_START } = '${dto[stStart]}'`);
        if (dto['nstStart']?.length > 0) items.push(` ${ ST_START } != '${dto['nstStart']}'`);
        if (dto[stStop]?.length > 0) items.push(` ${ ST_STOP } = '${dto[stStop]}'`);
        if (dto['nstStop']?.length > 0) items.push(` ${ ST_STOP } != '${dto['nstStop']}'`);
        if (dto[stBinSize]?.length > 0) items.push(` ${ ST_BIN_SIZE } = '${dto[stBinSize]}'`);
        if (dto['nstBinSize']?.length > 0) items.push(` ${ ST_BIN_SIZE } != '${dto['nstBinSize']}'`);
        if (dto[stDesc]?.length > 0) items.push(` ${ ST_DESC } = '${dto[stDesc]}'`);
        if (dto['nstDesc']?.length > 0) items.push(` ${ ST_DESC } != '${dto['nstDesc']}'`);
        if (dto[stAllMarkers]?.length > 0) items.push(` ${ ST_ALL_MARKERS } = '${dto[stAllMarkers]}'`);
        if (dto['nstAllMarkers']?.length > 0) items.push(` ${ ST_ALL_MARKERS } != '${dto['nstAllMarkers']}'`);
        if (dto[stSnpMarkers]?.length > 0) items.push(` ${ ST_SNP_MARKERS } = '${dto[stSnpMarkers]}'`);
        if (dto['nstSnpMarkers']?.length > 0) items.push(` ${ ST_SNP_MARKERS } != '${dto['nstSnpMarkers']}'`);
        if (dto[stIndelMarkers]?.length > 0) items.push(` ${ ST_INDEL_MARKERS } = '${dto[stIndelMarkers]}'`);
        if (dto['nstIndelMarkers']?.length > 0) items.push(` ${ ST_INDEL_MARKERS } != '${dto['nstIndelMarkers']}'`);
        if (dto[stBlockMarkers]?.length > 0) items.push(` ${ ST_BLOCK_MARKERS } = '${dto[stBlockMarkers]}'`);
        if (dto['nstBlockMarkers']?.length > 0) items.push(` ${ ST_BLOCK_MARKERS } != '${dto['nstBlockMarkers']}'`);
        if (dto[stTags]?.length > 0) items.push(` ${ ST_TAGS } = '${dto[stTags]}'`);
        if (dto['nstTags']?.length > 0) items.push(` ${ ST_TAGS } != '${dto['nstTags']}'`);
        if (dto[stGenotypes]?.length > 0) items.push(` ${ ST_GENOTYPES } = '${dto[stGenotypes]}'`);
        if (dto['nstGenotypes']?.length > 0) items.push(` ${ ST_GENOTYPES } != '${dto['nstGenotypes']}'`);
        if (dto[stMaxGenotypesFreq]?.length > 0) items.push(` ${ ST_MAX_GENOTYPES_FREQ } = '${dto[stMaxGenotypesFreq]}'`);
        if (dto['nstMaxGenotypesFreq']?.length > 0) items.push(` ${ ST_MAX_GENOTYPES_FREQ } != '${dto['nstMaxGenotypesFreq']}'`);
        if (dto[stMinGenotypesFreq]?.length > 0) items.push(` ${ ST_MIN_GENOTYPES_FREQ } = '${dto[stMinGenotypesFreq]}'`);
        if (dto['nstMinGenotypesFreq']?.length > 0) items.push(` ${ ST_MIN_GENOTYPES_FREQ } != '${dto['nstMinGenotypesFreq']}'`);
        if (dto[stPic]?.length > 0) items.push(` ${ ST_PIC } = '${dto[stPic]}'`);
        if (dto['nstPic']?.length > 0) items.push(` ${ ST_PIC } != '${dto['nstPic']}'`);
        if (dto[stGeneticMap]?.length > 0) items.push(` ${ ST_GENETIC_MAP } = '${dto[stGeneticMap]}'`);
        if (dto['nstGeneticMap']?.length > 0) items.push(` ${ ST_GENETIC_MAP } != '${dto['nstGeneticMap']}'`);
        if (dto[stOrder]) items.push(` ${ ST_ORDER } = '${dto[stOrder]}'`);
        if (dto['nstOrder']) items.push(` ${ ST_ORDER } != '${dto['nstOrder']}'`);
        if (dto[stColumn1]?.length > 0) items.push(` ${ ST_COLUMN1 } = '${dto[stColumn1]}'`);
        if (dto['nstColumn1']?.length > 0) items.push(` ${ ST_COLUMN1 } != '${dto['nstColumn1']}'`);
        if (dto[stColumn2]?.length > 0) items.push(` ${ ST_COLUMN2 } = '${dto[stColumn2]}'`);
        if (dto['nstColumn2']?.length > 0) items.push(` ${ ST_COLUMN2 } != '${dto['nstColumn2']}'`);
        if (dto[stColumn3]?.length > 0) items.push(` ${ ST_COLUMN3 } = '${dto[stColumn3]}'`);
        if (dto['nstColumn3']?.length > 0) items.push(` ${ ST_COLUMN3 } != '${dto['nstColumn3']}'`);
        if (dto[stSpecies]?.length > 0) items.push(` ${ ST_SPECIES } = '${dto[stSpecies]}'`);
        if (dto['nstSpecies']?.length > 0) items.push(` ${ ST_SPECIES } != '${dto['nstSpecies']}'`);
        if (dto[stRemark]?.length > 0) items.push(` ${ ST_REMARK } = '${dto[stRemark]}'`);
        if (dto['nstRemark']?.length > 0) items.push(` ${ ST_REMARK } != '${dto['nstRemark']}'`);
        if (dto[stCreateDate]) items.push(`${ ST_CREATE_DATE } is not null and date_format(${ ST_CREATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[stCreateDate])}`);
        if (dto['nstCreateDate']) items.push(`${ ST_CREATE_DATE } is not null and date_format(${ ST_CREATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nstCreateDate'])}`);
        if (dto[stUpdateDate]) items.push(`${ ST_UPDATE_DATE } is not null and date_format(${ ST_UPDATE_DATE },'%Y-%m-%d') = ${toSqlValue(dto[stUpdateDate])}`);
        if (dto['nstUpdateDate']) items.push(`${ ST_UPDATE_DATE } is not null and date_format(${ ST_UPDATE_DATE },'%Y-%m-%d') != ${toSqlValue(dto['nstUpdateDate'])}`);
        //数组、模糊条件、年份日期范围等额外查询条件
        //ST_ID--字符串字段
        if (dto['stIdList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_ID, < string[] > dto['stIdList']));
        if (dto['nstIdList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_ID, < string[] > dto['nstIdList'], true));
        if (dto['stIdLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_ID, dto['stIdLike'])); //For MysSQL
        if (dto['nstIdLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_ID, dto['nstIdLike'], true)); //For MysSQL
        if (dto['stIdLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_ID, < string[] > dto['stIdLikeList'])); //For MysSQL
        //ST_BIN--字符串字段
        if (dto['stBinList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_BIN, < string[] > dto['stBinList']));
        if (dto['nstBinList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_BIN, < string[] > dto['nstBinList'], true));
        if (dto['stBinLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_BIN, dto['stBinLike'])); //For MysSQL
        if (dto['nstBinLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_BIN, dto['nstBinLike'], true)); //For MysSQL
        if (dto['stBinLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_BIN, < string[] > dto['stBinLikeList'])); //For MysSQL
        //ST_CHR--字符串字段
        if (dto['stChrList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_CHR, < string[] > dto['stChrList']));
        if (dto['nstChrList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_CHR, < string[] > dto['nstChrList'], true));
        if (dto['stChrLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_CHR, dto['stChrLike'])); //For MysSQL
        if (dto['nstChrLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_CHR, dto['nstChrLike'], true)); //For MysSQL
        if (dto['stChrLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_CHR, < string[] > dto['stChrLikeList'])); //For MysSQL
        //ST_START--字符串字段
        if (dto['stStartList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_START, < string[] > dto['stStartList']));
        if (dto['nstStartList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_START, < string[] > dto['nstStartList'], true));
        if (dto['stStartLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_START, dto['stStartLike'])); //For MysSQL
        if (dto['nstStartLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_START, dto['nstStartLike'], true)); //For MysSQL
        if (dto['stStartLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_START, < string[] > dto['stStartLikeList'])); //For MysSQL
        //ST_STOP--字符串字段
        if (dto['stStopList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_STOP, < string[] > dto['stStopList']));
        if (dto['nstStopList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_STOP, < string[] > dto['nstStopList'], true));
        if (dto['stStopLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_STOP, dto['stStopLike'])); //For MysSQL
        if (dto['nstStopLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_STOP, dto['nstStopLike'], true)); //For MysSQL
        if (dto['stStopLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_STOP, < string[] > dto['stStopLikeList'])); //For MysSQL
        //ST_BIN_SIZE--字符串字段
        if (dto['stBinSizeList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_BIN_SIZE, < string[] > dto['stBinSizeList']));
        if (dto['nstBinSizeList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_BIN_SIZE, < string[] > dto['nstBinSizeList'], true));
        if (dto['stBinSizeLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_BIN_SIZE, dto['stBinSizeLike'])); //For MysSQL
        if (dto['nstBinSizeLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_BIN_SIZE, dto['nstBinSizeLike'], true)); //For MysSQL
        if (dto['stBinSizeLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_BIN_SIZE, < string[] > dto['stBinSizeLikeList'])); //For MysSQL
        //ST_DESC--字符串字段
        if (dto['stDescList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_DESC, < string[] > dto['stDescList']));
        if (dto['nstDescList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_DESC, < string[] > dto['nstDescList'], true));
        if (dto['stDescLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_DESC, dto['stDescLike'])); //For MysSQL
        if (dto['nstDescLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_DESC, dto['nstDescLike'], true)); //For MysSQL
        if (dto['stDescLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_DESC, < string[] > dto['stDescLikeList'])); //For MysSQL
        //ST_ALL_MARKERS--字符串字段
        if (dto['stAllMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_ALL_MARKERS, < string[] > dto['stAllMarkersList']));
        if (dto['nstAllMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_ALL_MARKERS, < string[] > dto['nstAllMarkersList'], true));
        if (dto['stAllMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_ALL_MARKERS, dto['stAllMarkersLike'])); //For MysSQL
        if (dto['nstAllMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_ALL_MARKERS, dto['nstAllMarkersLike'], true)); //For MysSQL
        if (dto['stAllMarkersLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_ALL_MARKERS, < string[] > dto['stAllMarkersLikeList'])); //For MysSQL
        //ST_SNP_MARKERS--字符串字段
        if (dto['stSnpMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_SNP_MARKERS, < string[] > dto['stSnpMarkersList']));
        if (dto['nstSnpMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_SNP_MARKERS, < string[] > dto['nstSnpMarkersList'], true));
        if (dto['stSnpMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_SNP_MARKERS, dto['stSnpMarkersLike'])); //For MysSQL
        if (dto['nstSnpMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_SNP_MARKERS, dto['nstSnpMarkersLike'], true)); //For MysSQL
        if (dto['stSnpMarkersLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_SNP_MARKERS, < string[] > dto['stSnpMarkersLikeList'])); //For MysSQL
        //ST_INDEL_MARKERS--字符串字段
        if (dto['stIndelMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_INDEL_MARKERS, < string[] > dto['stIndelMarkersList']));
        if (dto['nstIndelMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_INDEL_MARKERS, < string[] > dto['nstIndelMarkersList'], true));
        if (dto['stIndelMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_INDEL_MARKERS, dto['stIndelMarkersLike'])); //For MysSQL
        if (dto['nstIndelMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_INDEL_MARKERS, dto['nstIndelMarkersLike'], true)); //For MysSQL
        if (dto['stIndelMarkersLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_INDEL_MARKERS, < string[] > dto['stIndelMarkersLikeList'])); //For MysSQL
        //ST_BLOCK_MARKERS--字符串字段
        if (dto['stBlockMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_BLOCK_MARKERS, < string[] > dto['stBlockMarkersList']));
        if (dto['nstBlockMarkersList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_BLOCK_MARKERS, < string[] > dto['nstBlockMarkersList'], true));
        if (dto['stBlockMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_BLOCK_MARKERS, dto['stBlockMarkersLike'])); //For MysSQL
        if (dto['nstBlockMarkersLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_BLOCK_MARKERS, dto['nstBlockMarkersLike'], true)); //For MysSQL
        if (dto['stBlockMarkersLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_BLOCK_MARKERS, < string[] > dto['stBlockMarkersLikeList'])); //For MysSQL
        //ST_TAGS--字符串字段
        if (dto['stTagsList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_TAGS, < string[] > dto['stTagsList']));
        if (dto['nstTagsList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_TAGS, < string[] > dto['nstTagsList'], true));
        if (dto['stTagsLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_TAGS, dto['stTagsLike'])); //For MysSQL
        if (dto['nstTagsLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_TAGS, dto['nstTagsLike'], true)); //For MysSQL
        if (dto['stTagsLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_TAGS, < string[] > dto['stTagsLikeList'])); //For MysSQL
        //ST_GENOTYPES--字符串字段
        if (dto['stGenotypesList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_GENOTYPES, < string[] > dto['stGenotypesList']));
        if (dto['nstGenotypesList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_GENOTYPES, < string[] > dto['nstGenotypesList'], true));
        if (dto['stGenotypesLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_GENOTYPES, dto['stGenotypesLike'])); //For MysSQL
        if (dto['nstGenotypesLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_GENOTYPES, dto['nstGenotypesLike'], true)); //For MysSQL
        if (dto['stGenotypesLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_GENOTYPES, < string[] > dto['stGenotypesLikeList'])); //For MysSQL
        //ST_MAX_GENOTYPES_FREQ--字符串字段
        if (dto['stMaxGenotypesFreqList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_MAX_GENOTYPES_FREQ, < string[] > dto['stMaxGenotypesFreqList']));
        if (dto['nstMaxGenotypesFreqList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_MAX_GENOTYPES_FREQ, < string[] > dto['nstMaxGenotypesFreqList'], true));
        if (dto['stMaxGenotypesFreqLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_MAX_GENOTYPES_FREQ, dto['stMaxGenotypesFreqLike'])); //For MysSQL
        if (dto['nstMaxGenotypesFreqLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_MAX_GENOTYPES_FREQ, dto['nstMaxGenotypesFreqLike'], true)); //For MysSQL
        if (dto['stMaxGenotypesFreqLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_MAX_GENOTYPES_FREQ, < string[] > dto['stMaxGenotypesFreqLikeList'])); //For MysSQL
        //ST_MIN_GENOTYPES_FREQ--字符串字段
        if (dto['stMinGenotypesFreqList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_MIN_GENOTYPES_FREQ, < string[] > dto['stMinGenotypesFreqList']));
        if (dto['nstMinGenotypesFreqList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_MIN_GENOTYPES_FREQ, < string[] > dto['nstMinGenotypesFreqList'], true));
        if (dto['stMinGenotypesFreqLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_MIN_GENOTYPES_FREQ, dto['stMinGenotypesFreqLike'])); //For MysSQL
        if (dto['nstMinGenotypesFreqLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_MIN_GENOTYPES_FREQ, dto['nstMinGenotypesFreqLike'], true)); //For MysSQL
        if (dto['stMinGenotypesFreqLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_MIN_GENOTYPES_FREQ, < string[] > dto['stMinGenotypesFreqLikeList'])); //For MysSQL
        //ST_PIC--字符串字段
        if (dto['stPicList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_PIC, < string[] > dto['stPicList']));
        if (dto['nstPicList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_PIC, < string[] > dto['nstPicList'], true));
        if (dto['stPicLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_PIC, dto['stPicLike'])); //For MysSQL
        if (dto['nstPicLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_PIC, dto['nstPicLike'], true)); //For MysSQL
        if (dto['stPicLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_PIC, < string[] > dto['stPicLikeList'])); //For MysSQL
        //ST_GENETIC_MAP--字符串字段
        if (dto['stGeneticMapList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_GENETIC_MAP, < string[] > dto['stGeneticMapList']));
        if (dto['nstGeneticMapList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_GENETIC_MAP, < string[] > dto['nstGeneticMapList'], true));
        if (dto['stGeneticMapLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_GENETIC_MAP, dto['stGeneticMapLike'])); //For MysSQL
        if (dto['nstGeneticMapLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_GENETIC_MAP, dto['nstGeneticMapLike'], true)); //For MysSQL
        if (dto['stGeneticMapLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_GENETIC_MAP, < string[] > dto['stGeneticMapLikeList'])); //For MysSQL
        //ST_ORDER--数字字段
        if (dto['stOrderList']) items.push(OnpStatisticsSQL.IN_SQL(ST_ORDER, dto['stOrderList']));
        if (dto['nstOrderList']) items.push(OnpStatisticsSQL.IN_SQL(ST_ORDER, dto['nstOrderList'], true));
        //ST_COLUMN1--字符串字段
        if (dto['stColumn1List']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_COLUMN1, < string[] > dto['stColumn1List']));
        if (dto['nstColumn1List']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_COLUMN1, < string[] > dto['nstColumn1List'], true));
        if (dto['stColumn1Like']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_COLUMN1, dto['stColumn1Like'])); //For MysSQL
        if (dto['nstColumn1Like']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_COLUMN1, dto['nstColumn1Like'], true)); //For MysSQL
        if (dto['stColumn1LikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_COLUMN1, < string[] > dto['stColumn1LikeList'])); //For MysSQL
        //ST_COLUMN2--字符串字段
        if (dto['stColumn2List']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_COLUMN2, < string[] > dto['stColumn2List']));
        if (dto['nstColumn2List']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_COLUMN2, < string[] > dto['nstColumn2List'], true));
        if (dto['stColumn2Like']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_COLUMN2, dto['stColumn2Like'])); //For MysSQL
        if (dto['nstColumn2Like']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_COLUMN2, dto['nstColumn2Like'], true)); //For MysSQL
        if (dto['stColumn2LikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_COLUMN2, < string[] > dto['stColumn2LikeList'])); //For MysSQL
        //ST_COLUMN3--字符串字段
        if (dto['stColumn3List']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_COLUMN3, < string[] > dto['stColumn3List']));
        if (dto['nstColumn3List']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_COLUMN3, < string[] > dto['nstColumn3List'], true));
        if (dto['stColumn3Like']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_COLUMN3, dto['stColumn3Like'])); //For MysSQL
        if (dto['nstColumn3Like']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_COLUMN3, dto['nstColumn3Like'], true)); //For MysSQL
        if (dto['stColumn3LikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_COLUMN3, < string[] > dto['stColumn3LikeList'])); //For MysSQL
        //ST_SPECIES--字符串字段
        if (dto['stSpeciesList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_SPECIES, < string[] > dto['stSpeciesList']));
        if (dto['nstSpeciesList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_SPECIES, < string[] > dto['nstSpeciesList'], true));
        if (dto['stSpeciesLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_SPECIES, dto['stSpeciesLike'])); //For MysSQL
        if (dto['nstSpeciesLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_SPECIES, dto['nstSpeciesLike'], true)); //For MysSQL
        if (dto['stSpeciesLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_SPECIES, < string[] > dto['stSpeciesLikeList'])); //For MysSQL
        //ST_REMARK--字符串字段
        if (dto['stRemarkList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_REMARK, < string[] > dto['stRemarkList']));
        if (dto['nstRemarkList']?.length > 0) items.push(OnpStatisticsSQL.IN_SQL(ST_REMARK, < string[] > dto['nstRemarkList'], true));
        if (dto['stRemarkLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_REMARK, dto['stRemarkLike'])); //For MysSQL
        if (dto['nstRemarkLike']?.length > 0) items.push(OnpStatisticsSQL.LIKE_SQL(ST_REMARK, dto['nstRemarkLike'], true)); //For MysSQL
        if (dto['stRemarkLikeList']?.length > 0) items.push(OnpStatisticsSQL.LIKE_LIST_SQL(ST_REMARK, < string[] > dto['stRemarkLikeList'])); //For MysSQL
        //ST_CREATE_DATE--日期字段
        if (dto[stCreateDate]) items.push(`${ ST_CREATE_DATE } is not null and date_format(${ ST_CREATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sStCreateDate'])}`);
        if (dto[stCreateDate]) items.push(`${ ST_CREATE_DATE } is not null and date_format(${ ST_CREATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eStCreateDate'])}`);
        //ST_UPDATE_DATE--日期字段
        if (dto[stUpdateDate]) items.push(`${ ST_UPDATE_DATE } is not null and date_format(${ ST_UPDATE_DATE },'%Y-%m-%d %T') >= ${toSqlValue(dto['sStUpdateDate'])}`);
        if (dto[stUpdateDate]) items.push(`${ ST_UPDATE_DATE } is not null and date_format(${ ST_UPDATE_DATE },'%Y-%m-%d %T') <= ${toSqlValue(dto['eStUpdateDate'])}`);
        return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
    },
    /**
     * 字符串的Like语句，采用前后模糊查询
     * @param colName 列名
     * @param values 值
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
        if (!colName || value.length == 0 || !value || value.length == 0) return '';
        if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
        else return ` ${colName} like CONCAT('%','${value}','%')`;
    },
    /**
     * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
     * @param colName 列名
     * @param values 值列表
     * @param andOr 采用and或or连接多个条件，默认为OR语句
     * @return {string}
     */
    LIKE_LIST_SQL: (colName: string, values: string[], andOr ? : string): string => {
        if (!colName || !values || values.length == 0) return '';
        if (!andOr) andOr = 'OR';
        let items = values.map(value => OnpStatisticsSQL.LIKE_SQL(colName, value));
        return `( ${items.join(' ' + andOr + ' ')} )`;
    },
    /**
     * 排序查询SQL语句
     * @param dto
     * @return {string}
     */
    ORDER_BY_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        if (dto['order']?.length > 0) {
            return `order by ${dto['order']}`;
        }
        return '';
    },
    /**
     * 分组查询SQL语句
     * @param dto
     * @return {string}
     */
    GROUP_BY_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        if (dto['group']?.length > 0) {
            return `group by ${dto['group']}`;
        }
        return '';
    },
    /**
     * 分页查询条件SQL语句
     * @param dto
     * @return {string}
     */
    LIMIT_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
            return `limit ${dto['offset']},${dto['pageSize']}`;
        }
        return '';
    },
    /**
     * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
     * @param colName 列名，如:SAM_NAME
     * @param values 数组
     * @param not true/false--不包含于/包含于
     * @return {string}
     */
    IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
        if (!values || values.length == 0) return '';
        let items = [];
        Array.isArray(values) ? values.forEach(value => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
        return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
    },
    /**
     * 插入记录SQL语句
     * @param dto
     * @return {string}
     */
    INSERT_SQL: (dto: OnpStatisticsCDto | Partial < OnpStatisticsCDto > ): string => {
        if (!dto) return '';
        if (!dto[primerKey] || dto[primerKey] == undefined) return '';
        let cols = [];
        let values = [];
        cols.push(ST_ID);
        cols.push(ST_BIN);
        cols.push(ST_CHR);
        cols.push(ST_START);
        cols.push(ST_STOP);
        cols.push(ST_BIN_SIZE);
        cols.push(ST_DESC);
        cols.push(ST_ALL_MARKERS);
        cols.push(ST_SNP_MARKERS);
        cols.push(ST_INDEL_MARKERS);
        cols.push(ST_BLOCK_MARKERS);
        cols.push(ST_TAGS);
        cols.push(ST_GENOTYPES);
        cols.push(ST_MAX_GENOTYPES_FREQ);
        cols.push(ST_MIN_GENOTYPES_FREQ);
        cols.push(ST_PIC);
        cols.push(ST_GENETIC_MAP);
        cols.push(ST_ORDER);
        cols.push(ST_COLUMN1);
        cols.push(ST_COLUMN2);
        cols.push(ST_COLUMN3);
        cols.push(ST_SPECIES);
        cols.push(ST_REMARK);
        cols.push(ST_CREATE_DATE);
        cols.push(ST_UPDATE_DATE);
        values.push(toSqlValue(dto[stId]));
        values.push(toSqlValue(dto[stBin]));
        values.push(toSqlValue(dto[stChr]));
        values.push(toSqlValue(dto[stStart]));
        values.push(toSqlValue(dto[stStop]));
        values.push(toSqlValue(dto[stBinSize]));
        values.push(toSqlValue(dto[stDesc]));
        values.push(toSqlValue(dto[stAllMarkers]));
        values.push(toSqlValue(dto[stSnpMarkers]));
        values.push(toSqlValue(dto[stIndelMarkers]));
        values.push(toSqlValue(dto[stBlockMarkers]));
        values.push(toSqlValue(dto[stTags]));
        values.push(toSqlValue(dto[stGenotypes]));
        values.push(toSqlValue(dto[stMaxGenotypesFreq]));
        values.push(toSqlValue(dto[stMinGenotypesFreq]));
        values.push(toSqlValue(dto[stPic]));
        values.push(toSqlValue(dto[stGeneticMap]));
        values.push(toSqlValue(dto[stOrder]));
        values.push(toSqlValue(dto[stColumn1]));
        values.push(toSqlValue(dto[stColumn2]));
        values.push(toSqlValue(dto[stColumn3]));
        values.push(toSqlValue(dto[stSpecies]));
        values.push(toSqlValue(dto[stRemark]));
        values.push('NOW()');
        values.push('NOW()');
        let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
        return sql;
    },
    /**
     * 批量插入SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_INSERT_SQL: (dtos: OnpStatisticsCDto[] | Partial < OnpStatisticsCDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let cols = [];
        cols.push(ST_ID);
        cols.push(ST_BIN);
        cols.push(ST_CHR);
        cols.push(ST_START);
        cols.push(ST_STOP);
        cols.push(ST_BIN_SIZE);
        cols.push(ST_DESC);
        cols.push(ST_ALL_MARKERS);
        cols.push(ST_SNP_MARKERS);
        cols.push(ST_INDEL_MARKERS);
        cols.push(ST_BLOCK_MARKERS);
        cols.push(ST_TAGS);
        cols.push(ST_GENOTYPES);
        cols.push(ST_MAX_GENOTYPES_FREQ);
        cols.push(ST_MIN_GENOTYPES_FREQ);
        cols.push(ST_PIC);
        cols.push(ST_GENETIC_MAP);
        cols.push(ST_ORDER);
        cols.push(ST_COLUMN1);
        cols.push(ST_COLUMN2);
        cols.push(ST_COLUMN3);
        cols.push(ST_SPECIES);
        cols.push(ST_REMARK);
        cols.push(ST_CREATE_DATE);
        cols.push(ST_UPDATE_DATE);
        let items = [];
        dtos.forEach(dto => {
            let values = [];
            values.push(toSqlValue(dto[stId]));
            values.push(toSqlValue(dto[stBin]));
            values.push(toSqlValue(dto[stChr]));
            values.push(toSqlValue(dto[stStart]));
            values.push(toSqlValue(dto[stStop]));
            values.push(toSqlValue(dto[stBinSize]));
            values.push(toSqlValue(dto[stDesc]));
            values.push(toSqlValue(dto[stAllMarkers]));
            values.push(toSqlValue(dto[stSnpMarkers]));
            values.push(toSqlValue(dto[stIndelMarkers]));
            values.push(toSqlValue(dto[stBlockMarkers]));
            values.push(toSqlValue(dto[stTags]));
            values.push(toSqlValue(dto[stGenotypes]));
            values.push(toSqlValue(dto[stMaxGenotypesFreq]));
            values.push(toSqlValue(dto[stMinGenotypesFreq]));
            values.push(toSqlValue(dto[stPic]));
            values.push(toSqlValue(dto[stGeneticMap]));
            values.push(toSqlValue(dto[stOrder]));
            values.push(toSqlValue(dto[stColumn1]));
            values.push(toSqlValue(dto[stColumn2]));
            values.push(toSqlValue(dto[stColumn3]));
            values.push(toSqlValue(dto[stSpecies]));
            values.push(toSqlValue(dto[stRemark]));
            values.push('NOW()');
            values.push('NOW()');
            items.push(`(${values.join(',')})`);
        });
        return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
    },
    /**
     * 根据ID更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SQL: (dto: OnpStatisticsUDto | Partial < OnpStatisticsUDto > ): string => {
        if (!dto) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpStatisticsColNames, OnpStatisticsColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ ST_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ ST_BIN } = ${toSqlValue(dto[stBin])}`);
        items.push(`${ ST_CHR } = ${toSqlValue(dto[stChr])}`);
        items.push(`${ ST_START } = ${toSqlValue(dto[stStart])}`);
        items.push(`${ ST_STOP } = ${toSqlValue(dto[stStop])}`);
        items.push(`${ ST_BIN_SIZE } = ${toSqlValue(dto[stBinSize])}`);
        items.push(`${ ST_DESC } = ${toSqlValue(dto[stDesc])}`);
        items.push(`${ ST_ALL_MARKERS } = ${toSqlValue(dto[stAllMarkers])}`);
        items.push(`${ ST_SNP_MARKERS } = ${toSqlValue(dto[stSnpMarkers])}`);
        items.push(`${ ST_INDEL_MARKERS } = ${toSqlValue(dto[stIndelMarkers])}`);
        items.push(`${ ST_BLOCK_MARKERS } = ${toSqlValue(dto[stBlockMarkers])}`);
        items.push(`${ ST_TAGS } = ${toSqlValue(dto[stTags])}`);
        items.push(`${ ST_GENOTYPES } = ${toSqlValue(dto[stGenotypes])}`);
        items.push(`${ ST_MAX_GENOTYPES_FREQ } = ${toSqlValue(dto[stMaxGenotypesFreq])}`);
        items.push(`${ ST_MIN_GENOTYPES_FREQ } = ${toSqlValue(dto[stMinGenotypesFreq])}`);
        items.push(`${ ST_PIC } = ${toSqlValue(dto[stPic])}`);
        items.push(`${ ST_GENETIC_MAP } = ${toSqlValue(dto[stGeneticMap])}`);
        items.push(`${ ST_ORDER } = ${toSqlValue(dto[stOrder])}`);
        items.push(`${ ST_COLUMN1 } = ${toSqlValue(dto[stColumn1])}`);
        items.push(`${ ST_COLUMN2 } = ${toSqlValue(dto[stColumn2])}`);
        items.push(`${ ST_COLUMN3 } = ${toSqlValue(dto[stColumn3])}`);
        items.push(`${ ST_SPECIES } = ${toSqlValue(dto[stSpecies])}`);
        items.push(`${ ST_REMARK } = ${toSqlValue(dto[stRemark])}`);
        items.push(`${ ST_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID列表批量更新所有属性值的更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_IDS_SQL: (dto: OnpStatisticsUDto | Partial < OnpStatisticsUDto > ): string => {
        if (!dto) return '';
        //获取ID列表
        let ids = < string[] > dto[`${primerKey}List`];
        if (!ids || ids.length == 0) return '';
        //校正列名
        let cols = StringUtils.toColName(dto.cols, OnpStatisticsColNames, OnpStatisticsColProps);
        //移除日期列名
        cols = StringUtils.delDateCols(cols);
        //更新指定的列数据
        if (cols?.length > 0) {
            let items = cols.map(col => (`${ col } = ${toSqlValue(dto[StringUtils.camelCase(col)])}`));
            items.push(`${ ST_UPDATE_DATE } = NOW()`);
            return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpStatisticsSQL.IN_SQL(PRIMER_KEY, ids)}`;
        }
        //更新所有列数据
        let items = [];
        items.push(`${ ST_BIN } = ${toSqlValue(dto[stBin])}`);
        items.push(`${ ST_CHR } = ${toSqlValue(dto[stChr])}`);
        items.push(`${ ST_START } = ${toSqlValue(dto[stStart])}`);
        items.push(`${ ST_STOP } = ${toSqlValue(dto[stStop])}`);
        items.push(`${ ST_BIN_SIZE } = ${toSqlValue(dto[stBinSize])}`);
        items.push(`${ ST_DESC } = ${toSqlValue(dto[stDesc])}`);
        items.push(`${ ST_ALL_MARKERS } = ${toSqlValue(dto[stAllMarkers])}`);
        items.push(`${ ST_SNP_MARKERS } = ${toSqlValue(dto[stSnpMarkers])}`);
        items.push(`${ ST_INDEL_MARKERS } = ${toSqlValue(dto[stIndelMarkers])}`);
        items.push(`${ ST_BLOCK_MARKERS } = ${toSqlValue(dto[stBlockMarkers])}`);
        items.push(`${ ST_TAGS } = ${toSqlValue(dto[stTags])}`);
        items.push(`${ ST_GENOTYPES } = ${toSqlValue(dto[stGenotypes])}`);
        items.push(`${ ST_MAX_GENOTYPES_FREQ } = ${toSqlValue(dto[stMaxGenotypesFreq])}`);
        items.push(`${ ST_MIN_GENOTYPES_FREQ } = ${toSqlValue(dto[stMinGenotypesFreq])}`);
        items.push(`${ ST_PIC } = ${toSqlValue(dto[stPic])}`);
        items.push(`${ ST_GENETIC_MAP } = ${toSqlValue(dto[stGeneticMap])}`);
        items.push(`${ ST_ORDER } = ${toSqlValue(dto[stOrder])}`);
        items.push(`${ ST_COLUMN1 } = ${toSqlValue(dto[stColumn1])}`);
        items.push(`${ ST_COLUMN2 } = ${toSqlValue(dto[stColumn2])}`);
        items.push(`${ ST_COLUMN3 } = ${toSqlValue(dto[stColumn3])}`);
        items.push(`${ ST_SPECIES } = ${toSqlValue(dto[stSpecies])}`);
        items.push(`${ ST_REMARK } = ${toSqlValue(dto[stRemark])}`);
        items.push(`${ ST_UPDATE_DATE } = NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${ OnpStatisticsSQL.IN_SQL(PRIMER_KEY, ids)}`;
    },
    /**
     * 根据ID更新所有属性值的批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SQL: (dtos: OnpStatisticsUDto[] | Partial < OnpStatisticsUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let sqls = [];
        dtos.forEach(value => {
            sqls.push(OnpStatisticsSQL.UPDATE_BY_ID_SQL(value));
        });
        return sqls.join(';');
    },
    /**
     * 根据ID更新有值属性值的条件更新SQL语句
     * @param dto
     * @return {string}
     */
    UPDATE_BY_ID_SELECTIVE_SQL: (dto: OnpStatisticsUDto | Partial < OnpStatisticsUDto > ): string => {
        if (!dto) return '';
        let items = [];
        if (dto[stBin]?.length > 0) items.push(`${ ST_BIN } = ${toSqlValue(dto[stBin])}`);
        if (dto[stChr]?.length > 0) items.push(`${ ST_CHR } = ${toSqlValue(dto[stChr])}`);
        if (dto[stStart]?.length > 0) items.push(`${ ST_START } = ${toSqlValue(dto[stStart])}`);
        if (dto[stStop]?.length > 0) items.push(`${ ST_STOP } = ${toSqlValue(dto[stStop])}`);
        if (dto[stBinSize]?.length > 0) items.push(`${ ST_BIN_SIZE } = ${toSqlValue(dto[stBinSize])}`);
        if (dto[stDesc]?.length > 0) items.push(`${ ST_DESC } = ${toSqlValue(dto[stDesc])}`);
        if (dto[stAllMarkers]?.length > 0) items.push(`${ ST_ALL_MARKERS } = ${toSqlValue(dto[stAllMarkers])}`);
        if (dto[stSnpMarkers]?.length > 0) items.push(`${ ST_SNP_MARKERS } = ${toSqlValue(dto[stSnpMarkers])}`);
        if (dto[stIndelMarkers]?.length > 0) items.push(`${ ST_INDEL_MARKERS } = ${toSqlValue(dto[stIndelMarkers])}`);
        if (dto[stBlockMarkers]?.length > 0) items.push(`${ ST_BLOCK_MARKERS } = ${toSqlValue(dto[stBlockMarkers])}`);
        if (dto[stTags]?.length > 0) items.push(`${ ST_TAGS } = ${toSqlValue(dto[stTags])}`);
        if (dto[stGenotypes]?.length > 0) items.push(`${ ST_GENOTYPES } = ${toSqlValue(dto[stGenotypes])}`);
        if (dto[stMaxGenotypesFreq]?.length > 0) items.push(`${ ST_MAX_GENOTYPES_FREQ } = ${toSqlValue(dto[stMaxGenotypesFreq])}`);
        if (dto[stMinGenotypesFreq]?.length > 0) items.push(`${ ST_MIN_GENOTYPES_FREQ } = ${toSqlValue(dto[stMinGenotypesFreq])}`);
        if (dto[stPic]?.length > 0) items.push(`${ ST_PIC } = ${toSqlValue(dto[stPic])}`);
        if (dto[stGeneticMap]?.length > 0) items.push(`${ ST_GENETIC_MAP } = ${toSqlValue(dto[stGeneticMap])}`);
        if (dto[stOrder]) items.push(`${ ST_ORDER } = ${toSqlValue(dto[stOrder])}`);
        if (dto[stColumn1]?.length > 0) items.push(`${ ST_COLUMN1 } = ${toSqlValue(dto[stColumn1])}`);
        if (dto[stColumn2]?.length > 0) items.push(`${ ST_COLUMN2 } = ${toSqlValue(dto[stColumn2])}`);
        if (dto[stColumn3]?.length > 0) items.push(`${ ST_COLUMN3 } = ${toSqlValue(dto[stColumn3])}`);
        if (dto[stSpecies]?.length > 0) items.push(`${ ST_SPECIES } = ${toSqlValue(dto[stSpecies])}`);
        if (dto[stRemark]?.length > 0) items.push(`${ ST_REMARK } = ${toSqlValue(dto[stRemark])}`);
        if (dto[stUpdateDate]) items.push(`${  ST_UPDATE_DATE }= NOW()`);
        return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    },
    /**
     * 根据ID更新有值属性值的条件批量更新SQL语句
     * @param entitys
     * @return {string}
     */
    BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: OnpStatisticsUDto[] | Partial < OnpStatisticsUDto > []): string => {
        if (!dtos || dtos.length == 0) return '';
        let items = [];
        dtos.forEach(value => items.push(OnpStatisticsSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
        return items.join(';');
    },
    /**
     * 条件删除SQL语句
     * @param dtos
     * @return {string}
     */
    DELETE_SELECTIVE_SQL: (dto: OnpStatisticsQDto | Partial < OnpStatisticsQDto > ): string => {
        if (!dto) return '';
        let where = OnpStatisticsSQL.WHERE_SQL(dto);
        return `DELETE FROM ${TABLE} ${where}`;
    },
    /**
     * 删除
     * @param dto
     * @return {string}
     */
    DELETE_BY_ID_SQL: (id: string): string => {
        if (!id) return '';
        return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
    },
    /**
     *
     * @param dto
     * @return {string}
     */
    DELETE_BY_IDS_SQL: (idList: string[]): string => {
        if (!idList || idList.length == 0) return '';
        let inSql = OnpStatisticsSQL.IN_SQL(PRIMER_KEY, idList);
        return `DELETE FROM ${TABLE} WHERE ${inSql}`;
    },
    /**
     * 删除所有记录的SQL语句
     */
    DELETE_ALL_SQL: (): string => {
        return `DELETE FROM ${TABLE}`;
    },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'ONP_STATISTICS';
/**
 * 列名常量字段
 */
const ST_ID = OnpStatisticsColNameEnum.ST_ID;
const ST_BIN = OnpStatisticsColNameEnum.ST_BIN;
const ST_CHR = OnpStatisticsColNameEnum.ST_CHR;
const ST_START = OnpStatisticsColNameEnum.ST_START;
const ST_STOP = OnpStatisticsColNameEnum.ST_STOP;
const ST_BIN_SIZE = OnpStatisticsColNameEnum.ST_BIN_SIZE;
const ST_DESC = OnpStatisticsColNameEnum.ST_DESC;
const ST_ALL_MARKERS = OnpStatisticsColNameEnum.ST_ALL_MARKERS;
const ST_SNP_MARKERS = OnpStatisticsColNameEnum.ST_SNP_MARKERS;
const ST_INDEL_MARKERS = OnpStatisticsColNameEnum.ST_INDEL_MARKERS;
const ST_BLOCK_MARKERS = OnpStatisticsColNameEnum.ST_BLOCK_MARKERS;
const ST_TAGS = OnpStatisticsColNameEnum.ST_TAGS;
const ST_GENOTYPES = OnpStatisticsColNameEnum.ST_GENOTYPES;
const ST_MAX_GENOTYPES_FREQ = OnpStatisticsColNameEnum.ST_MAX_GENOTYPES_FREQ;
const ST_MIN_GENOTYPES_FREQ = OnpStatisticsColNameEnum.ST_MIN_GENOTYPES_FREQ;
const ST_PIC = OnpStatisticsColNameEnum.ST_PIC;
const ST_GENETIC_MAP = OnpStatisticsColNameEnum.ST_GENETIC_MAP;
const ST_ORDER = OnpStatisticsColNameEnum.ST_ORDER;
const ST_COLUMN1 = OnpStatisticsColNameEnum.ST_COLUMN1;
const ST_COLUMN2 = OnpStatisticsColNameEnum.ST_COLUMN2;
const ST_COLUMN3 = OnpStatisticsColNameEnum.ST_COLUMN3;
const ST_SPECIES = OnpStatisticsColNameEnum.ST_SPECIES;
const ST_REMARK = OnpStatisticsColNameEnum.ST_REMARK;
const ST_CREATE_DATE = OnpStatisticsColNameEnum.ST_CREATE_DATE;
const ST_UPDATE_DATE = OnpStatisticsColNameEnum.ST_UPDATE_DATE;
/**
 * 实体类属性名
 */
const stId = OnpStatisticsColPropEnum.stId;
const stBin = OnpStatisticsColPropEnum.stBin;
const stChr = OnpStatisticsColPropEnum.stChr;
const stStart = OnpStatisticsColPropEnum.stStart;
const stStop = OnpStatisticsColPropEnum.stStop;
const stBinSize = OnpStatisticsColPropEnum.stBinSize;
const stDesc = OnpStatisticsColPropEnum.stDesc;
const stAllMarkers = OnpStatisticsColPropEnum.stAllMarkers;
const stSnpMarkers = OnpStatisticsColPropEnum.stSnpMarkers;
const stIndelMarkers = OnpStatisticsColPropEnum.stIndelMarkers;
const stBlockMarkers = OnpStatisticsColPropEnum.stBlockMarkers;
const stTags = OnpStatisticsColPropEnum.stTags;
const stGenotypes = OnpStatisticsColPropEnum.stGenotypes;
const stMaxGenotypesFreq = OnpStatisticsColPropEnum.stMaxGenotypesFreq;
const stMinGenotypesFreq = OnpStatisticsColPropEnum.stMinGenotypesFreq;
const stPic = OnpStatisticsColPropEnum.stPic;
const stGeneticMap = OnpStatisticsColPropEnum.stGeneticMap;
const stOrder = OnpStatisticsColPropEnum.stOrder;
const stColumn1 = OnpStatisticsColPropEnum.stColumn1;
const stColumn2 = OnpStatisticsColPropEnum.stColumn2;
const stColumn3 = OnpStatisticsColPropEnum.stColumn3;
const stSpecies = OnpStatisticsColPropEnum.stSpecies;
const stRemark = OnpStatisticsColPropEnum.stRemark;
const stCreateDate = OnpStatisticsColPropEnum.stCreateDate;
const stUpdateDate = OnpStatisticsColPropEnum.stUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = OnpStatisticsTable.PRIMER_KEY;
const primerKey = OnpStatisticsTable.primerKey;