import { Column, Entity } from 'typeorm';
import { IsNotEmpty, Max } from 'class-validator';
import { Type } from 'class-transformer';

/**
 * 预测
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpForecast
 */
@Entity({
  name: 'ONP_FORECAST',
})
export class OnpForecast {
  /**
   * id-主键
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_ID', type: 'varchar', length: '255', primary: true })
  @IsNotEmpty({ message: '【id】不能为空' })
  @Max(255, { message: '【id】长度不能超过255' })
  foId: string;
  /**
   * ID
   *
   * @type { number }
   * @memberof OnpForecast
   */
  @Type(() => Number)
  @Column({ name: 'FO_ORDER', type: 'int' })
  foOrder: number;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_COLUMN1', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column1】长度不能超过255' })
  foColumn1: string;
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_COLUMN2', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column2】长度不能超过255' })
  foColumn2: string;
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_COLUMN3', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column3】长度不能超过255' })
  foColumn3: string;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_COLUMN4', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column1】长度不能超过255' })
  foColumn4: string;
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_COLUMN5', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column2】长度不能超过255' })
  foColumn5: string;
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_COLUMN6', type: 'varchar', length: '255' })
  @Max(255, { message: '【Column3】长度不能超过255' })
  foColumn6: string;
  /**
   * Species
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_SPECIES', type: 'varchar', length: '255' })
  @Max(255, { message: '【Species】长度不能超过255' })
  foSpecies: string;
  /**
   * Remark
   *
   * @type { string }
   * @memberof OnpForecast
   */
  @Column({ name: 'FO_REMARK', type: 'varchar', length: '128' })
  @Max(128, { message: '【Remark】长度不能超过128' })
  foRemark: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof OnpForecast
   */
  @Type(() => Date)
  @Column({ name: 'FO_CREATE_DATE', type: 'datetime' })
  foCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof OnpForecast
   */
  @Type(() => Date)
  @Column({ name: 'FO_UPDATE_DATE', type: 'datetime' })
  foUpdateDate: Date;
}

//声明类型定义
export declare type OnpForecastType = OnpForecast | Partial<OnpForecast>;
