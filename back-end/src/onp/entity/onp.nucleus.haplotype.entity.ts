import { Entity, Column } from "typeorm";
import { Max, IsNotEmpty } from "class-validator";
import { Type } from 'class-transformer';
/**
 * ONP_NUCLEUS_HAPLOTYPE
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export
 * @class OnpNucleusHaplotype
 */
@Entity({
    name: 'ONP_NUCLEUS_HAPLOTYPE'
})
export class OnpNucleusHaplotype {
    /**
     * id-主键
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_ID', type: 'varchar', length: '255', primary: true })
    @IsNotEmpty({ message: '【id】不能为空' })
    @Max(255, { message: '【id】长度不能超过255' })
    onhId: string;
    /**
     * Haplotype Index
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_HAPLOTYPE_INDEX', type: 'varchar', length: '255' })
    @Max(255, { message: '【Haplotype Index】长度不能超过255' })
    onhHaplotypeIndex: string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_CHR', type: 'varchar', length: '255' })
    @Max(255, { message: '【Chr】长度不能超过255' })
    onhChr: string;
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_ONP_ID', type: 'varchar', length: '255' })
    @Max(255, { message: '【ONP ID】长度不能超过255' })
    onhOnpId: string;
    /**
     * Haplotype Sequence
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_HAPLOTYPE_SEQUENCE', type: 'varchar', length: '255' })
    @Max(255, { message: '【Haplotype Sequence】长度不能超过255' })
    onhHaplotypeSequence: string;
    /**
     * Haplotype Tag
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_HAPLOTYPE_TAG_SEQUENCE', type: 'varchar', length: '255' })
    @Max(255, { message: '【Haplotype Tag】长度不能超过255' })
    onhHaplotypeTagSequence: string;
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_FREQUENCY', type: 'varchar', length: '255' })
    @Max(255, { message: '【Frequency】长度不能超过255' })
    onhFrequency: string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_COLUMN1', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column1】长度不能超过255' })
    onhColumn1: string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_COLUMN2', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column2】长度不能超过255' })
    onhColumn2: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_COLUMN3', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    onhColumn3: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpNucleusHaplotype
     */
    @Type(() => Number)
    @Column({ name: 'ONH_ORDER', type: 'int' })
    onhOrder: number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_SPECIES', type: 'varchar', length: '255' })
    @Max(255, { message: '【Species】长度不能超过255' })
    onhSpecies: string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpNucleusHaplotype
     */
    @Column({ name: 'ONH_REMARK', type: 'varchar', length: '128' })
    @Max(128, { message: '【Remark】长度不能超过128' })
    onhRemark: string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotype
     */
    @Type(() => Date)
    @Column({ name: 'ONH_CREATE_DATE', type: 'datetime' })
    onhCreateDate: Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotype
     */
    @Type(() => Date)
    @Column({ name: 'ONH_UPDATE_DATE', type: 'datetime' })
    onhUpdateDate: Date;
}
//声明类型定义
export declare type OnpNucleusHaplotypeType = OnpNucleusHaplotype | Partial < OnpNucleusHaplotype > ;