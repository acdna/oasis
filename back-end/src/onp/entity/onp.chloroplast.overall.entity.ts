import { Entity, Column } from "typeorm";
import { Max, IsNotEmpty } from "class-validator";
import { Type } from 'class-transformer';
/**
 * ONP_CHLOROPLAST_OVERALL
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastOverall
 */
@Entity({
    name: 'ONP_CHLOROPLAST_OVERALL'
})
export class OnpChloroplastOverall {
    /**
     * ID-主键
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_ID', type: 'varchar', length: '255', primary: true })
    @IsNotEmpty({ message: '【ID】不能为空' })
    @Max(255, { message: '【ID】长度不能超过255' })
    ocoId: string;
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_ONP_ID', type: 'varchar', length: '255' })
    @IsNotEmpty({ message: '【ONP ID】不能为空' })
    @Max(255, { message: '【ONP ID】长度不能超过255' })
    ocoOnpId: string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_CHR', type: 'varchar', length: '255' })
    @Max(255, { message: '【Chr】长度不能超过255' })
    ocoChr: string;
    /**
     * Start
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_START', type: 'varchar', length: '255' })
    @Max(255, { message: '【Start】长度不能超过255' })
    ocoStart: string;
    /**
     * Stop
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_STOP', type: 'varchar', length: '255' })
    @Max(255, { message: '【Stop】长度不能超过255' })
    ocoStop: string;
    /**
     * ONP
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_ONP_SIZE', type: 'varchar', length: '255' })
    @Max(255, { message: '【ONP】长度不能超过255' })
    ocoOnpSize: string;
    /**
     * Location
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_LOCATION', type: 'varchar', length: '255' })
    @Max(255, { message: '【Location】长度不能超过255' })
    ocoLocation: string;
    /**
     * ALL
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_ALL_MARKERS', type: 'varchar', length: '255' })
    @Max(255, { message: '【ALL】长度不能超过255' })
    ocoAllMarkers: string;
    /**
     * SNP
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_SNP_MARKERS', type: 'varchar', length: '255' })
    @Max(255, { message: '【SNP】长度不能超过255' })
    ocoSnpMarkers: string;
    /**
     * INDEL
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_INDEL_MARKERS', type: 'varchar', length: '255' })
    @Max(255, { message: '【INDEL】长度不能超过255' })
    ocoIndelMarkers: string;
    /**
     * Block
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_BLOCK_MARKERS', type: 'varchar', length: '255' })
    @Max(255, { message: '【Block】长度不能超过255' })
    ocoBlockMarkers: string;
    /**
     * Tags
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_TAGS', type: 'varchar', length: '255' })
    @Max(255, { message: '【Tags】长度不能超过255' })
    ocoTags: string;
    /**
     * Genotypes
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_GENOTYPES', type: 'varchar', length: '255' })
    @Max(255, { message: '【Genotypes】长度不能超过255' })
    ocoGenotypes: string;
    /**
     * Max
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_MAX_GENOTYPES_FREQ', type: 'varchar', length: '255' })
    @Max(255, { message: '【Max】长度不能超过255' })
    ocoMaxGenotypesFreq: string;
    /**
     * Min
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_MIN_GENOTYPES_FREQ', type: 'varchar', length: '255' })
    @Max(255, { message: '【Min】长度不能超过255' })
    ocoMinGenotypesFreq: string;
    /**
     * PIC
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_PIC', type: 'varchar', length: '255' })
    @Max(255, { message: '【PIC】长度不能超过255' })
    ocoPic: string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_COLUMN1', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column1】长度不能超过255' })
    ocoColumn1: string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_COLUMN2', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column2】长度不能超过255' })
    ocoColumn2: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_COLUMN3', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    ocoColumn3: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastOverall
     */
    @Type(() => Number)
    @Column({ name: 'OCO_ORDER', type: 'int' })
    ocoOrder: number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_SPECIES', type: 'varchar', length: '255' })
    @Max(255, { message: '【Species】长度不能超过255' })
    ocoSpecies: string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastOverall
     */
    @Column({ name: 'OCO_REMARK', type: 'varchar', length: '128' })
    @Max(128, { message: '【Remark】长度不能超过128' })
    ocoRemark: string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastOverall
     */
    @Type(() => Date)
    @Column({ name: 'OCO_CREATE_DATE', type: 'datetime' })
    ocoCreateDate: Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastOverall
     */
    @Type(() => Date)
    @Column({ name: 'OCO_UPDATE_DATE', type: 'datetime' })
    ocoUpdateDate: Date;
}
//声明类型定义
export declare type OnpChloroplastOverallType = OnpChloroplastOverall | Partial < OnpChloroplastOverall > ;