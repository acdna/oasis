import { Entity, Column } from "typeorm";
import { Max, IsNotEmpty } from "class-validator";
import { Type } from 'class-transformer';
/**
 * ONP_CHLOROPLAST_HAPLOTYPE
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastHaplotype
 */
@Entity({
    name: 'ONP_CHLOROPLAST_HAPLOTYPE'
})
export class OnpChloroplastHaplotype {
    /**
     * id-主键
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_ID', type: 'varchar', length: '255', primary: true })
    @IsNotEmpty({ message: '【id】不能为空' })
    @Max(255, { message: '【id】长度不能超过255' })
    ochId: string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_CHLOROPLAST_HAPLOTYPE', type: 'varchar', length: '255' })
    @Max(255, { message: '【Chloroplast】长度不能超过255' })
    ochChloroplastHaplotype: string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_CHR', type: 'varchar', length: '255' })
    @Max(255, { message: '【Chr】长度不能超过255' })
    ochChr: string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_CHLOROPLAST_ONP', type: 'varchar', length: '255' })
    @Max(255, { message: '【Chloroplast】长度不能超过255' })
    ochChloroplastOnp: string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_CHLOROPLAST_HAPLOTYPE_SEQUENCE', type: 'varchar', length: '255' })
    @Max(255, { message: '【Chloroplast】长度不能超过255' })
    ochChloroplastHaplotypeSequence: string;
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_FREQUENCY', type: 'varchar', length: '255' })
    @Max(255, { message: '【Frequency】长度不能超过255' })
    ochFrequency: string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_COLUMN1', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column1】长度不能超过255' })
    ochColumn1: string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_COLUMN2', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column2】长度不能超过255' })
    ochColumn2: string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_COLUMN3', type: 'varchar', length: '255' })
    @Max(255, { message: '【Column3】长度不能超过255' })
    ochColumn3: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastHaplotype
     */
    @Type(() => Number)
    @Column({ name: 'OCH_ORDER', type: 'int' })
    ochOrder: number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_SPECIES', type: 'varchar', length: '255' })
    @Max(255, { message: '【Species】长度不能超过255' })
    ochSpecies: string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotype
     */
    @Column({ name: 'OCH_REMARK', type: 'varchar', length: '128' })
    @Max(128, { message: '【Remark】长度不能超过128' })
    ochRemark: string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotype
     */
    @Type(() => Date)
    @Column({ name: 'OCH_CREATE_DATE', type: 'datetime' })
    ochCreateDate: Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotype
     */
    @Type(() => Date)
    @Column({ name: 'OCH_UPDATE_DATE', type: 'datetime' })
    ochUpdateDate: Date;
}
//声明类型定义
export declare type OnpChloroplastHaplotypeType = OnpChloroplastHaplotype | Partial < OnpChloroplastHaplotype > ;