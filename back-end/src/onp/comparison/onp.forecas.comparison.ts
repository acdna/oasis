import {PUBLIC_FOLDER} from "../../common/path/path.creator.utils";

let pythonFilePath=`${PUBLIC_FOLDER}/python/test.py`;
const { spawn } = require('child_process');

export const OnpForecastComparison = {
  comparison(filePath,list){
    let mailadree = list.foColumn4;
    console.log('OnpForecastComparison======>',filePath)
    const ls = spawn('python3', [pythonFilePath,mailadree, 'arg1', 'arg2', '-i', 'input','--output', 'output']);

    ls.stdout.on('data', (data) => {
      console.log(`stdout: ${data}`);
    });

    ls.stderr.on('data', (data) => {
      console.error(`stderr: ${data}`);
    });

    ls.on('close', (code) => {
      console.log(`子进程退出，退出码 ${code}`);
    });
  }
};