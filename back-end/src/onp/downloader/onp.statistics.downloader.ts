import { OnpStatisticsCDto } from '../dto/create/onp.statistics.cdto';
import { ArrayUtils } from '../../common/utils/array.utils';
import { OnpStatisticsExcelCreator } from '../excel/creator/onp.statistics.excel.creator';
import { ExcelUtils } from '../../common/excel/tools/excel.utils';
import { OnpServices } from '../onp.services';
import { PathUtils } from '../../common/path/path.creator.utils';
import { ExcelParser } from '../../common/excel/tools/excel.parser';
import { SheetDataType } from '../../common/excel/tools/sheet.data';
import { OnpExcelTitles } from '../excel/onp.excel.titles';
import { LogUtils } from '../../common/log4js/log4js.utils';

export const OnpStatisticsDownloader = {
  //条件下载
  async downloadByBins(params: Partial<OnpStatisticsCDto>): Promise<any> {
    //解析Excel
    let bins = this.parserExcel(params.stFilePath);
    if (ArrayUtils.isEmpty(bins)) {
      return null;
    }

    //查询数据
    let records = await OnpServices.onpStatisticsService.findList({ stBinList: bins });
    if (ArrayUtils.isEmpty(records)) {
      return null;
    }
    let sheets = OnpStatisticsExcelCreator.create(records);
    if (ArrayUtils.isEmpty(sheets)) {
      return null;
    }
    return ExcelUtils.save(sheets);
  },

  //解析Excel
  parserExcel(stFilePath: string) {
    let path = PathUtils.getFullPath({ relativePath: stFilePath });

    let parser = new ExcelParser();
    let sheets = parser.parse(path);

    //根据标题行格式过滤
    sheets = ExcelUtils.filter(OnpExcelTitles.ONP_STATISTICS_BINS.title, sheets);
    if (ArrayUtils.isEmpty(sheets)) {
      LogUtils.warn('OnpStatisticsParser', `Excel标题不正确:${path}`);
      return [];
    }

    let data = parser.convert<string>(sheets, (params: { sheet: SheetDataType; index: number; row: any[] }) => {
      return params.row[0];
    });
    return data;
  },

  //全部下载
  async downloadAll(params: Partial<OnpStatisticsCDto>): Promise<any> {

    //查询数据
    let records = await OnpServices.onpStatisticsService.findAll();
    if (ArrayUtils.isEmpty(records)) {
      return null;
    }
    let sheets = OnpStatisticsExcelCreator.create(records);
    if (ArrayUtils.isEmpty(sheets)) {
      return null;
    }
    return ExcelUtils.save(sheets);
  },
};
