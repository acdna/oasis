import { PmsInfo } from '../../common/pms/pms.info';
import { ONP_FORECAST_PMS } from './onp.forecast.pms';
import { ONP_STATISTICS_PMS } from './onp.statistics.pms';
import { ONP_VERIFICATION_PMS } from './onp.verification.pms';
import {ONP_CHLOROPLAST_HAPLOTYPE_PMS} from "./onp.chloroplast.haplotype.pms";
import {ONP_CHLOROPLAST_OVERALL_PMS} from "./onp.chloroplast.overall.pms";
import {ONP_NUCLEUS_HAPLOTYPE_PMS} from "./onp.nucleus.haplotype.pms";
/**
 * 集中注册ONP模块的所有权限信息，用于自动化注册后端权限功能到数据库中，以便后续的功能授权
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 **/
export const ONP_PMS: PmsInfo[] = [
    ONP_CHLOROPLAST_HAPLOTYPE_PMS,
    ONP_CHLOROPLAST_OVERALL_PMS,
    ONP_NUCLEUS_HAPLOTYPE_PMS,
    ONP_FORECAST_PMS,
    ONP_STATISTICS_PMS,
    ONP_VERIFICATION_PMS,
];