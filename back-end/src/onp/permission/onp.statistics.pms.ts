import { PmsRoot } from '../../common/pms/pms.info';
/**
 * ONP统计数据表-后端控制器权限列表
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 **/
export const ONP_STATISTICS_PMS_ROOT: PmsRoot = {
  system: 'BACK_END',
  group: 'ONP_STATISTICS',
  name: 'ONP_STATISTICS',
  nameEn: 'ONP_STATISTICS',
  type: 'back-end',
};
/**
 * ONP统计数据表-后端控制器权限列表
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 **/
export const ONP_STATISTICS_PMS = {
  FIND_ALL: { id: 'FIND_ALL', name: '查询所有', nameEn: 'Query all', enable: false, root: ONP_STATISTICS_PMS_ROOT },
  FIND_LIST: {
    id: 'FIND_LIST',
    name: '条件查询记录',
    nameEn: 'Conditional query record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  FIND_BY_ID: {
    id: 'FIND_BY_ID',
    name: '查询ID的记录',
    nameEn: 'Query the record of the ID',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  FIND_ONE: {
    id: 'FIND_ONE',
    name: '查询单条记录',
    nameEn: 'Query a single record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  FIND_BY_ID_LIST: {
    id: 'FIND_BY_ID_LIST',
    name: '查询ID列表的记录',
    nameEn: 'Query the record of the ID list',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  FIND_PAGE: {
    id: 'FIND_PAGE',
    name: '分页查询记录',
    nameEn: 'Paging query record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  FIND_PAGE_DETAIL: {
    id: 'FIND_PAGE_DETAIL',
    name: '带子记录的分页查询记录',
    nameEn: 'Paging query record and children',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  COUNT: { id: 'COUNT', name: '查询记录数', nameEn: 'Query record count', enable: false, root: ONP_STATISTICS_PMS_ROOT },
  DELETE_BY_ID: {
    id: 'DELETE_BY_ID',
    name: '删除ID的记录',
    nameEn: 'Delete the record of the ID',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  DELETE_BY_ID_LIST: {
    id: 'DELETE_BY_ID_LIST',
    name: '删除ID列表的记录',
    nameEn: 'Delete the record of the ID list',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  DELETE: {
    id: 'DELETE',
    name: '条件删除记录',
    nameEn: 'Conditional deletion record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  UPDATE: { id: 'UPDATE', name: '更新ID记录', nameEn: 'Update record', enable: false, root: ONP_STATISTICS_PMS_ROOT },
  UPDATE_LIST: {
    id: 'UPDATE_LIST',
    name: '批量更新记录列表',
    nameEn: 'Batch update the record list',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  UPDATE_BY_IDS: {
    id: 'UPDATE_BY_IDS',
    name: '批量更新ID列表记录列表',
    nameEn: 'Batch update the record of the ID list',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  UPDATE_SELECTIVE: {
    id: 'UPDATE_SELECTIVE',
    name: '选择性更新ID记录',
    nameEn: 'Selective update record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  UPDATE_LIST_SELECTIVE: {
    id: 'UPDATE_LIST_SELECTIVE',
    name: '选择性批量更新记录列表',
    nameEn: 'Selective batch updating of records',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  CREATE: { id: 'CREATE', name: '创建记录', nameEn: 'Create a record', enable: false, root: ONP_STATISTICS_PMS_ROOT },
  CREATE_LIST: {
    id: 'CREATE_LIST',
    name: '批量创建记录',
    nameEn: 'Batch record creation',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  INIT_FRONT: {
    id: 'INIT_FRONT',
    name: '初始化前端记录',
    nameEn: 'Init front record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  INIT_BACK: {
    id: 'INIT_BACK',
    name: '初始化后端记录',
    nameEn: 'Init back record',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  INIT: { id: 'INIT', name: '初始化记录', nameEn: 'Init record', enable: false, root: ONP_STATISTICS_PMS_ROOT },
  UPLOAD: {
    id: 'UPLOAD',
    name: '批量上传',
    nameEn: 'Upload Data',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  DOWNLOAD_STATISTICS_EXCEL:{
    id: 'DOWNLOAD_STATISTICS_EXCEL',
    name: '导出信息',
    nameEn: 'Download Data',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
  DOWNLOAD_All_STATISTICS_EXCEL:{
    id: 'DOWNLOAD_All_STATISTICS_EXCEL',
    name: '导出全部信息',
    nameEn: 'Download All Data',
    enable: false,
    root: ONP_STATISTICS_PMS_ROOT,
  },
};
