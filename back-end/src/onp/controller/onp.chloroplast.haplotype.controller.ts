import { Body, Controller, Get, Param, Post, Put, Delete, Query, Inject } from "@nestjs/common";
import { OnpChloroplastHaplotype } from "../entity/onp.chloroplast.haplotype.entity";
import { OnpChloroplastHaplotypeUDto } from "../dto/update/onp.chloroplast.haplotype.udto";
import { OnpChloroplastHaplotypeCDto } from "../dto/create/onp.chloroplast.haplotype.cdto";
import { OnpChloroplastHaplotypeQDto } from "../dto/query/onp.chloroplast.haplotype.qdto";
import { OnpChloroplastHaplotypeService } from '../service/onp.chloroplast.haplotype.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { ONP_CHLOROPLAST_HAPLOTYPE_PMS } from '../permission/onp.chloroplast.haplotype.pms';
/**
 * ONP_CHLOROPLAST_HAPLOTYPE表对应控制器类
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastHaplotypeController
 */
@Controller("onp/chloroplast/haplotype")
@ApiTags('访问ONP_CHLOROPLAST_HAPLOTYPE表的控制器类')
export class OnpChloroplastHaplotypeController {
    constructor(@Inject(OnpChloroplastHaplotypeService) private readonly onpChloroplastHaplotypeService: OnpChloroplastHaplotypeService) {}
    /**
     * 查询所有记录
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.FIND_ALL)
    @Get('find/all')
    @ApiOperation({ summary: '查询所有记录' })
    async findAll(): Promise < OnpChloroplastHaplotype[] > {
        return this.onpChloroplastHaplotypeService.findAll();
    }
    /**
     * 条件查询记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.FIND_LIST)
    @Get('find/list')
    @ApiOperation({ summary: '条件查询记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastHaplotypeQDto })
    async findList(@Query() dto: OnpChloroplastHaplotypeQDto): Promise < OnpChloroplastHaplotype[] > {
        return this.onpChloroplastHaplotypeService.findList(dto);
    }
    /**
     * 查询ID对应记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.FIND_BY_ID)
    @Get('find/by/id')
    @ApiOperation({ summary: '查询指定ID的记录' })
    @ApiQuery({ name: 'id', type: String })
    async findById(@Query('id') id: string): Promise < OnpChloroplastHaplotype > {
        return this.onpChloroplastHaplotypeService.findById(id);
    }
    /**
     * 查询单条记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.FIND_ONE)
    @Get('find/one')
    @ApiOperation({ summary: '查询单条记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastHaplotypeQDto })
    async findOne(@Query() dto: OnpChloroplastHaplotypeQDto): Promise < OnpChloroplastHaplotype > {
        return this.onpChloroplastHaplotypeService.findOne(dto);
    }
    /**
     * 查询ID列表对应记录
     * @param ids
     * @param cols
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.FIND_BY_ID_LIST)
    @Get('find/by/ids')
    @ApiOperation({ summary: '查询指定ID列表的记录' })
    @ApiQuery({ name: 'ids', type: [String] })
    @ApiQuery({ name: 'cols', type: [String] })
    async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise < OnpChloroplastHaplotype[] > {
        return this.onpChloroplastHaplotypeService.findByIdList(ids, cols);
    }
    /**
     * 分页查询记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.FIND_PAGE)
    @Get('find/page')
    @ApiOperation({ summary: '分页查询记录' })
    @ApiQuery({ name: 'query', type: OnpChloroplastHaplotypeQDto })
    async findPage(@Query() query: OnpChloroplastHaplotypeQDto): Promise < any > {
        return this.onpChloroplastHaplotypeService.findPage(query);
    }
    /**
     * 查询记录数
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.COUNT)
    @Get('count')
    @ApiOperation({ summary: '分页查询记录' })
    @ApiQuery({ name: 'query', type: OnpChloroplastHaplotypeQDto })
    async count(@Query() query: OnpChloroplastHaplotypeQDto): Promise < number > {
        return this.onpChloroplastHaplotypeService.count(query);
    }
    /**
     * 删除指定ID记录
     * @param id
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.DELETE_BY_ID)
    @Delete('delete/by/id')
    @ApiOperation({ summary: '删除指定ID记录' })
    @ApiQuery({ name: 'id', type: String })
    async deleteById(@Query('id') id: string): Promise < any > {
        return this.onpChloroplastHaplotypeService.deleteById(id);
    }
    /**
     * 删除指定ID记录列表
     * @param ids
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.DELETE_BY_ID_LIST)
    @Delete('delete/by/ids')
    @ApiOperation({ summary: '删除指定ID列表的记录' })
    @ApiQuery({ name: 'ids', type: [String] })
    async deleteByIdList(@Body('ids') ids: string[]): Promise < any > {
        return this.onpChloroplastHaplotypeService.deleteByIdList(ids);
    }
    /**
     * 条件删除记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.DELETE)
    @Delete('delete')
    @ApiOperation({ summary: '条件删除记录' })
    @ApiQuery({ name: 'query', type: OnpChloroplastHaplotypeQDto })
    async delete(@Query() query: OnpChloroplastHaplotypeQDto): Promise < any > {
        return this.onpChloroplastHaplotypeService.delete(query);
    }
    /**
     * 更新指定ID记录全部字段
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.UPDATE)
    @Put('update/by/id')
    @ApiOperation({ summary: '更新指定ID的记录全部字段' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastHaplotypeUDto })
    async update(@Body() dto: OnpChloroplastHaplotypeUDto): Promise < any > {
        return this.onpChloroplastHaplotypeService.update(dto);
    }
    /**
     * 更新指定ID列表记录全部字段
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.UPDATE_BY_IDS)
    @Put('update/by/ids')
    @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastHaplotypeUDto })
    async updateByIds(@Body() dto: OnpChloroplastHaplotypeUDto): Promise < any > {
        return this.onpChloroplastHaplotypeService.updateByIds(dto);
    }
    /**
     * 批量更新ID对应的记录全部字段
     * @param dtos
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.UPDATE_LIST)
    @Put('update/list')
    @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
    @ApiQuery({ name: 'dtos', type: [OnpChloroplastHaplotypeUDto] })
    async updateList(@Body() dtos: OnpChloroplastHaplotypeUDto[]): Promise < any > {
        return this.onpChloroplastHaplotypeService.updateList(dtos);
    }
    /**
     * 更新有值字段到指定ID记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.UPDATE_SELECTIVE)
    @Put('update/by/id/selective')
    @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastHaplotypeUDto })
    async updateSelective(@Body() dto: OnpChloroplastHaplotypeUDto): Promise < any > {
        return this.onpChloroplastHaplotypeService.updateSelective(dto);
    }
    /**
     * 批量更新有值字段到ID对应的记录
     * @param dtos
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.UPDATE_LIST_SELECTIVE)
    @Put('update/list/selective')
    @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
    @ApiQuery({ name: 'dtos', type: [OnpChloroplastHaplotypeUDto] })
    async updateListSelective(@Body() dtos: OnpChloroplastHaplotypeUDto[]): Promise < any > {
        return this.onpChloroplastHaplotypeService.updateListSelective(dtos);
    }
    /**
     * 创建新记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.CREATE)
    @Post('create')
    @ApiOperation({ summary: '创建新记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastHaplotypeCDto })
    async create(@Body() dto: OnpChloroplastHaplotypeCDto): Promise < OnpChloroplastHaplotypeCDto > {
        return this.onpChloroplastHaplotypeService.create(dto);
    }
    /**
     * 批量创建新记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_HAPLOTYPE_PMS.CREATE_LIST)
    @Post('create/list')
    @ApiOperation({ summary: '批量创建新记录' })
    @ApiQuery({ name: 'dtos', type: [OnpChloroplastHaplotypeCDto] })
    async createList(@Body() dtos: OnpChloroplastHaplotypeCDto[]): Promise < OnpChloroplastHaplotypeCDto[] > {
        return this.onpChloroplastHaplotypeService.createList(dtos);
    }
}