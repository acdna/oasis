import { Body, Controller, Get, Param, Post, Put, Delete, Query, Inject } from "@nestjs/common";
import { OnpNucleusHaplotype } from "../entity/onp.nucleus.haplotype.entity";
import { OnpNucleusHaplotypeUDto } from "../dto/update/onp.nucleus.haplotype.udto";
import { OnpNucleusHaplotypeCDto } from "../dto/create/onp.nucleus.haplotype.cdto";
import { OnpNucleusHaplotypeQDto } from "../dto/query/onp.nucleus.haplotype.qdto";
import { OnpNucleusHaplotypeService } from '../service/onp.nucleus.haplotype.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { ONP_NUCLEUS_HAPLOTYPE_PMS } from '../permission/onp.nucleus.haplotype.pms';
/**
 * ONP_NUCLEUS_HAPLOTYPE表对应控制器类
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export
 * @class OnpNucleusHaplotypeController
 */
@Controller("onp/nucleus/haplotype")
@ApiTags('访问ONP_NUCLEUS_HAPLOTYPE表的控制器类')
export class OnpNucleusHaplotypeController {
    constructor(@Inject(OnpNucleusHaplotypeService) private readonly onpNucleusHaplotypeService: OnpNucleusHaplotypeService) {}
    /**
     * 查询所有记录
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.FIND_ALL)
    @Get('find/all')
    @ApiOperation({ summary: '查询所有记录' })
    async findAll(): Promise < OnpNucleusHaplotype[] > {
        return this.onpNucleusHaplotypeService.findAll();
    }
    /**
     * 条件查询记录
     * @param dto
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.FIND_LIST)
    @Get('find/list')
    @ApiOperation({ summary: '条件查询记录' })
    @ApiQuery({ name: 'dto', type: OnpNucleusHaplotypeQDto })
    async findList(@Query() dto: OnpNucleusHaplotypeQDto): Promise < OnpNucleusHaplotype[] > {
        return this.onpNucleusHaplotypeService.findList(dto);
    }
    /**
     * 查询ID对应记录
     * @param query
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.FIND_BY_ID)
    @Get('find/by/id')
    @ApiOperation({ summary: '查询指定ID的记录' })
    @ApiQuery({ name: 'id', type: String })
    async findById(@Query('id') id: string): Promise < OnpNucleusHaplotype > {
        return this.onpNucleusHaplotypeService.findById(id);
    }
    /**
     * 查询单条记录
     * @param query
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.FIND_ONE)
    @Get('find/one')
    @ApiOperation({ summary: '查询单条记录' })
    @ApiQuery({ name: 'dto', type: OnpNucleusHaplotypeQDto })
    async findOne(@Query() dto: OnpNucleusHaplotypeQDto): Promise < OnpNucleusHaplotype > {
        return this.onpNucleusHaplotypeService.findOne(dto);
    }
    /**
     * 查询ID列表对应记录
     * @param ids
     * @param cols
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.FIND_BY_ID_LIST)
    @Get('find/by/ids')
    @ApiOperation({ summary: '查询指定ID列表的记录' })
    @ApiQuery({ name: 'ids', type: [String] })
    @ApiQuery({ name: 'cols', type: [String] })
    async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise < OnpNucleusHaplotype[] > {
        return this.onpNucleusHaplotypeService.findByIdList(ids, cols);
    }
    /**
     * 分页查询记录
     * @param query
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.FIND_PAGE)
    @Get('find/page')
    @ApiOperation({ summary: '分页查询记录' })
    @ApiQuery({ name: 'query', type: OnpNucleusHaplotypeQDto })
    async findPage(@Query() query: OnpNucleusHaplotypeQDto): Promise < any > {
        return this.onpNucleusHaplotypeService.findPage(query);
    }
    /**
     * 查询记录数
     * @param query
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.COUNT)
    @Get('count')
    @ApiOperation({ summary: '分页查询记录' })
    @ApiQuery({ name: 'query', type: OnpNucleusHaplotypeQDto })
    async count(@Query() query: OnpNucleusHaplotypeQDto): Promise < number > {
        return this.onpNucleusHaplotypeService.count(query);
    }
    /**
     * 删除指定ID记录
     * @param id
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.DELETE_BY_ID)
    @Delete('delete/by/id')
    @ApiOperation({ summary: '删除指定ID记录' })
    @ApiQuery({ name: 'id', type: String })
    async deleteById(@Query('id') id: string): Promise < any > {
        return this.onpNucleusHaplotypeService.deleteById(id);
    }
    /**
     * 删除指定ID记录列表
     * @param ids
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.DELETE_BY_ID_LIST)
    @Delete('delete/by/ids')
    @ApiOperation({ summary: '删除指定ID列表的记录' })
    @ApiQuery({ name: 'ids', type: [String] })
    async deleteByIdList(@Body('ids') ids: string[]): Promise < any > {
        return this.onpNucleusHaplotypeService.deleteByIdList(ids);
    }
    /**
     * 条件删除记录
     * @param query
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.DELETE)
    @Delete('delete')
    @ApiOperation({ summary: '条件删除记录' })
    @ApiQuery({ name: 'query', type: OnpNucleusHaplotypeQDto })
    async delete(@Query() query: OnpNucleusHaplotypeQDto): Promise < any > {
        return this.onpNucleusHaplotypeService.delete(query);
    }
    /**
     * 更新指定ID记录全部字段
     * @param dto
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.UPDATE)
    @Put('update/by/id')
    @ApiOperation({ summary: '更新指定ID的记录全部字段' })
    @ApiQuery({ name: 'dto', type: OnpNucleusHaplotypeUDto })
    async update(@Body() dto: OnpNucleusHaplotypeUDto): Promise < any > {
        return this.onpNucleusHaplotypeService.update(dto);
    }
    /**
     * 更新指定ID列表记录全部字段
     * @param dto
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.UPDATE_BY_IDS)
    @Put('update/by/ids')
    @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
    @ApiQuery({ name: 'dto', type: OnpNucleusHaplotypeUDto })
    async updateByIds(@Body() dto: OnpNucleusHaplotypeUDto): Promise < any > {
        return this.onpNucleusHaplotypeService.updateByIds(dto);
    }
    /**
     * 批量更新ID对应的记录全部字段
     * @param dtos
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.UPDATE_LIST)
    @Put('update/list')
    @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
    @ApiQuery({ name: 'dtos', type: [OnpNucleusHaplotypeUDto] })
    async updateList(@Body() dtos: OnpNucleusHaplotypeUDto[]): Promise < any > {
        return this.onpNucleusHaplotypeService.updateList(dtos);
    }
    /**
     * 更新有值字段到指定ID记录
     * @param dto
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.UPDATE_SELECTIVE)
    @Put('update/by/id/selective')
    @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
    @ApiQuery({ name: 'dto', type: OnpNucleusHaplotypeUDto })
    async updateSelective(@Body() dto: OnpNucleusHaplotypeUDto): Promise < any > {
        return this.onpNucleusHaplotypeService.updateSelective(dto);
    }
    /**
     * 批量更新有值字段到ID对应的记录
     * @param dtos
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.UPDATE_LIST_SELECTIVE)
    @Put('update/list/selective')
    @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
    @ApiQuery({ name: 'dtos', type: [OnpNucleusHaplotypeUDto] })
    async updateListSelective(@Body() dtos: OnpNucleusHaplotypeUDto[]): Promise < any > {
        return this.onpNucleusHaplotypeService.updateListSelective(dtos);
    }
    /**
     * 创建新记录
     * @param dto
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.CREATE)
    @Post('create')
    @ApiOperation({ summary: '创建新记录' })
    @ApiQuery({ name: 'dto', type: OnpNucleusHaplotypeCDto })
    async create(@Body() dto: OnpNucleusHaplotypeCDto): Promise < OnpNucleusHaplotypeCDto > {
        return this.onpNucleusHaplotypeService.create(dto);
    }
    /**
     * 批量创建新记录
     * @param dto
     */
    @Permission(ONP_NUCLEUS_HAPLOTYPE_PMS.CREATE_LIST)
    @Post('create/list')
    @ApiOperation({ summary: '批量创建新记录' })
    @ApiQuery({ name: 'dtos', type: [OnpNucleusHaplotypeCDto] })
    async createList(@Body() dtos: OnpNucleusHaplotypeCDto[]): Promise < OnpNucleusHaplotypeCDto[] > {
        return this.onpNucleusHaplotypeService.createList(dtos);
    }
}