import { Body, Controller, Get, Param, Post, Put, Delete, Query, Inject } from "@nestjs/common";
import { OnpChloroplastOverall } from "../entity/onp.chloroplast.overall.entity";
import { OnpChloroplastOverallUDto } from "../dto/update/onp.chloroplast.overall.udto";
import { OnpChloroplastOverallCDto } from "../dto/create/onp.chloroplast.overall.cdto";
import { OnpChloroplastOverallQDto } from "../dto/query/onp.chloroplast.overall.qdto";
import { OnpChloroplastOverallService } from '../service/onp.chloroplast.overall.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { ONP_CHLOROPLAST_OVERALL_PMS } from '../permission/onp.chloroplast.overall.pms';
/**
 * ONP_CHLOROPLAST_OVERALL表对应控制器类
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastOverallController
 */
@Controller("onp/chloroplast/overall")
@ApiTags('访问ONP_CHLOROPLAST_OVERALL表的控制器类')
export class OnpChloroplastOverallController {
    constructor(@Inject(OnpChloroplastOverallService) private readonly onpChloroplastOverallService: OnpChloroplastOverallService) {}
    /**
     * 查询所有记录
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.FIND_ALL)
    @Get('find/all')
    @ApiOperation({ summary: '查询所有记录' })
    async findAll(): Promise < OnpChloroplastOverall[] > {
        return this.onpChloroplastOverallService.findAll();
    }
    /**
     * 条件查询记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.FIND_LIST)
    @Get('find/list')
    @ApiOperation({ summary: '条件查询记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastOverallQDto })
    async findList(@Query() dto: OnpChloroplastOverallQDto): Promise < OnpChloroplastOverall[] > {
        return this.onpChloroplastOverallService.findList(dto);
    }
    /**
     * 查询ID对应记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.FIND_BY_ID)
    @Get('find/by/id')
    @ApiOperation({ summary: '查询指定ID的记录' })
    @ApiQuery({ name: 'id', type: String })
    async findById(@Query('id') id: string): Promise < OnpChloroplastOverall > {
        return this.onpChloroplastOverallService.findById(id);
    }
    /**
     * 查询单条记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.FIND_ONE)
    @Get('find/one')
    @ApiOperation({ summary: '查询单条记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastOverallQDto })
    async findOne(@Query() dto: OnpChloroplastOverallQDto): Promise < OnpChloroplastOverall > {
        return this.onpChloroplastOverallService.findOne(dto);
    }
    /**
     * 查询ID列表对应记录
     * @param ids
     * @param cols
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.FIND_BY_ID_LIST)
    @Get('find/by/ids')
    @ApiOperation({ summary: '查询指定ID列表的记录' })
    @ApiQuery({ name: 'ids', type: [String] })
    @ApiQuery({ name: 'cols', type: [String] })
    async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise < OnpChloroplastOverall[] > {
        return this.onpChloroplastOverallService.findByIdList(ids, cols);
    }
    /**
     * 分页查询记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.FIND_PAGE)
    @Get('find/page')
    @ApiOperation({ summary: '分页查询记录' })
    @ApiQuery({ name: 'query', type: OnpChloroplastOverallQDto })
    async findPage(@Query() query: OnpChloroplastOverallQDto): Promise < any > {
        return this.onpChloroplastOverallService.findPage(query);
    }
    /**
     * 查询记录数
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.COUNT)
    @Get('count')
    @ApiOperation({ summary: '分页查询记录' })
    @ApiQuery({ name: 'query', type: OnpChloroplastOverallQDto })
    async count(@Query() query: OnpChloroplastOverallQDto): Promise < number > {
        return this.onpChloroplastOverallService.count(query);
    }
    /**
     * 删除指定ID记录
     * @param id
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.DELETE_BY_ID)
    @Delete('delete/by/id')
    @ApiOperation({ summary: '删除指定ID记录' })
    @ApiQuery({ name: 'id', type: String })
    async deleteById(@Query('id') id: string): Promise < any > {
        return this.onpChloroplastOverallService.deleteById(id);
    }
    /**
     * 删除指定ID记录列表
     * @param ids
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.DELETE_BY_ID_LIST)
    @Delete('delete/by/ids')
    @ApiOperation({ summary: '删除指定ID列表的记录' })
    @ApiQuery({ name: 'ids', type: [String] })
    async deleteByIdList(@Body('ids') ids: string[]): Promise < any > {
        return this.onpChloroplastOverallService.deleteByIdList(ids);
    }
    /**
     * 条件删除记录
     * @param query
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.DELETE)
    @Delete('delete')
    @ApiOperation({ summary: '条件删除记录' })
    @ApiQuery({ name: 'query', type: OnpChloroplastOverallQDto })
    async delete(@Query() query: OnpChloroplastOverallQDto): Promise < any > {
        return this.onpChloroplastOverallService.delete(query);
    }
    /**
     * 更新指定ID记录全部字段
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.UPDATE)
    @Put('update/by/id')
    @ApiOperation({ summary: '更新指定ID的记录全部字段' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastOverallUDto })
    async update(@Body() dto: OnpChloroplastOverallUDto): Promise < any > {
        return this.onpChloroplastOverallService.update(dto);
    }
    /**
     * 更新指定ID列表记录全部字段
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.UPDATE_BY_IDS)
    @Put('update/by/ids')
    @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastOverallUDto })
    async updateByIds(@Body() dto: OnpChloroplastOverallUDto): Promise < any > {
        return this.onpChloroplastOverallService.updateByIds(dto);
    }
    /**
     * 批量更新ID对应的记录全部字段
     * @param dtos
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.UPDATE_LIST)
    @Put('update/list')
    @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
    @ApiQuery({ name: 'dtos', type: [OnpChloroplastOverallUDto] })
    async updateList(@Body() dtos: OnpChloroplastOverallUDto[]): Promise < any > {
        return this.onpChloroplastOverallService.updateList(dtos);
    }
    /**
     * 更新有值字段到指定ID记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.UPDATE_SELECTIVE)
    @Put('update/by/id/selective')
    @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastOverallUDto })
    async updateSelective(@Body() dto: OnpChloroplastOverallUDto): Promise < any > {
        return this.onpChloroplastOverallService.updateSelective(dto);
    }
    /**
     * 批量更新有值字段到ID对应的记录
     * @param dtos
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.UPDATE_LIST_SELECTIVE)
    @Put('update/list/selective')
    @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
    @ApiQuery({ name: 'dtos', type: [OnpChloroplastOverallUDto] })
    async updateListSelective(@Body() dtos: OnpChloroplastOverallUDto[]): Promise < any > {
        return this.onpChloroplastOverallService.updateListSelective(dtos);
    }
    /**
     * 创建新记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.CREATE)
    @Post('create')
    @ApiOperation({ summary: '创建新记录' })
    @ApiQuery({ name: 'dto', type: OnpChloroplastOverallCDto })
    async create(@Body() dto: OnpChloroplastOverallCDto): Promise < OnpChloroplastOverallCDto > {
        return this.onpChloroplastOverallService.create(dto);
    }
    /**
     * 批量创建新记录
     * @param dto
     */
    @Permission(ONP_CHLOROPLAST_OVERALL_PMS.CREATE_LIST)
    @Post('create/list')
    @ApiOperation({ summary: '批量创建新记录' })
    @ApiQuery({ name: 'dtos', type: [OnpChloroplastOverallCDto] })
    async createList(@Body() dtos: OnpChloroplastOverallCDto[]): Promise < OnpChloroplastOverallCDto[] > {
        return this.onpChloroplastOverallService.createList(dtos);
    }
}