import {Body, Controller, Delete, Get, Inject, Post, Put, Query} from '@nestjs/common';
import {OnpForecast} from '../entity/onp.forecast.entity';
import {OnpForecastUDto} from '../dto/update/onp.forecast.udto';
import {OnpForecastCDto} from '../dto/create/onp.forecast.cdto';
import {OnpForecastQDto} from '../dto/query/onp.forecast.qdto';
import {OnpForecastService} from '../service/onp.forecast.service';
import {ApiOperation, ApiQuery, ApiTags} from '@nestjs/swagger';
import {Permission} from '../../common/auth/guard/pms.auth';
import {ONP_FORECAST_PMS} from '../permission/onp.forecast.pms';
import {SkipAuth} from '../../common/auth/guard/skip.auth';
import {StringUtils} from "../../common/utils/string.utils";
import {OnpForecastComparison} from "../comparison/onp.forecas.comparison";

/**
 * ONP_FORECAST表对应控制器类
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpForecastController
 */
@SkipAuth()
@Controller('onp/forecast')
@ApiTags('访问ONP_FORECAST表的控制器类')
export class OnpForecastController {
  constructor(@Inject(OnpForecastService) private readonly onpForecastService: OnpForecastService) {
  }

  /**
   * 查询所有记录
   */
  @Permission(ONP_FORECAST_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({summary: '查询所有记录'})
  async findAll(): Promise<OnpForecast[]> {
    return this.onpForecastService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(ONP_FORECAST_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({summary: '条件查询记录'})
  @ApiQuery({name: 'dto', type: OnpForecastQDto})
  async findList(@Query() dto: OnpForecastQDto): Promise<OnpForecast[]> {
    return this.onpForecastService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(ONP_FORECAST_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({summary: '查询指定ID的记录'})
  @ApiQuery({name: 'id', type: String})
  async findById(@Query('id') id: string): Promise<OnpForecast> {
    return this.onpForecastService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(ONP_FORECAST_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({summary: '查询单条记录'})
  @ApiQuery({name: 'dto', type: OnpForecastQDto})
  async findOne(@Query() dto: OnpForecastQDto): Promise<OnpForecast> {
    return this.onpForecastService.findOne(dto);
  }

  /**
   * 查询ID列表对应记录
   * @param ids
   * @param cols
   */
  @Permission(ONP_FORECAST_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({summary: '查询指定ID列表的记录'})
  @ApiQuery({name: 'ids', type: [String]})
  @ApiQuery({name: 'cols', type: [String]})
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<OnpForecast[]> {
    return this.onpForecastService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(ONP_FORECAST_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({summary: '分页查询记录'})
  @ApiQuery({name: 'query', type: OnpForecastQDto})
  async findPage(@Query() query: OnpForecastQDto): Promise<any> {
    return this.onpForecastService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(ONP_FORECAST_PMS.COUNT)
  @Get('count')
  @ApiOperation({summary: '分页查询记录'})
  @ApiQuery({name: 'query', type: OnpForecastQDto})
  async count(@Query() query: OnpForecastQDto): Promise<number> {
    return this.onpForecastService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(ONP_FORECAST_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({summary: '删除指定ID记录'})
  @ApiQuery({name: 'id', type: String})
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.onpForecastService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(ONP_FORECAST_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({summary: '删除指定ID列表的记录'})
  @ApiQuery({name: 'ids', type: [String]})
  async deleteByIdList(@Body('ids') ids: string[]): Promise<any> {
    return this.onpForecastService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(ONP_FORECAST_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({summary: '条件删除记录'})
  @ApiQuery({name: 'query', type: OnpForecastQDto})
  async delete(@Query() query: OnpForecastQDto): Promise<any> {
    return this.onpForecastService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(ONP_FORECAST_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({summary: '更新指定ID的记录全部字段'})
  @ApiQuery({name: 'dto', type: OnpForecastUDto})
  async update(@Body() dto: OnpForecastUDto): Promise<any> {
    return this.onpForecastService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(ONP_FORECAST_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({summary: '更新指定ID列表的记录全部字段'})
  @ApiQuery({name: 'dto', type: OnpForecastUDto})
  async updateByIds(@Body() dto: OnpForecastUDto): Promise<any> {
    return this.onpForecastService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(ONP_FORECAST_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({summary: '批量更新ID对应的记录全部字段'})
  @ApiQuery({name: 'dtos', type: [OnpForecastUDto]})
  async updateList(@Body() dtos: OnpForecastUDto[]): Promise<any> {
    return this.onpForecastService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(ONP_FORECAST_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({summary: '更新有值字段到指定ID的记录'})
  @ApiQuery({name: 'dto', type: OnpForecastUDto})
  async updateSelective(@Body() dto: OnpForecastUDto): Promise<any> {
    return this.onpForecastService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(ONP_FORECAST_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({summary: '批量更新有值字段到ID对应的记录'})
  @ApiQuery({name: 'dtos', type: [OnpForecastUDto]})
  async updateListSelective(@Body() dtos: OnpForecastUDto[]): Promise<any> {
    return this.onpForecastService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(ONP_FORECAST_PMS.CREATE)
  @Post('create')
  @ApiOperation({summary: '创建新记录'})
  @ApiQuery({name: 'dto', type: OnpForecastCDto})
  async create(@Body() dto: OnpForecastCDto): Promise<OnpForecastCDto> {
    return this.onpForecastService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(ONP_FORECAST_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({summary: '批量创建新记录'})
  @ApiQuery({name: 'dtos', type: [OnpForecastCDto]})
  async createList(@Body() dtos: OnpForecastCDto[]): Promise<OnpForecastCDto[]> {
    return this.onpForecastService.createList(dtos);
  }

  /**********新增的方法************/
  /**
   * @Description:上传信息
   * @author zhengenze
   * @date 2021/4/6 3:56 下午
   **/
  @Permission(ONP_FORECAST_PMS.UPLOAD)
  @Post('upload')
  @ApiOperation({summary: '导入信息数据'})
  @ApiQuery({name: 'dto', type: OnpForecastCDto})
  async upload(@Body() dto: OnpForecastCDto): Promise<any> {
    return this.onpForecastService.upload(dto);
  }

  /**
   * @Description: 比对数据
   * @author zhengenze
   * @date 2021/4/14 10:19 上午
   **/
  @Permission(ONP_FORECAST_PMS.COMPARISON)
  @Post('comparison')
  @ApiOperation({summary: '比对信息数据'})
  @ApiQuery({name: 'params'})
  async comparison(@Body() params): Promise<any> {
    const {id, filePath} = params

    if (!filePath || StringUtils.isBlank(filePath)) return false;

    // 根据ID 查询数据库记录
    let list = await this.onpForecastService.findById(id);

    // 调用python脚本
    return OnpForecastComparison.comparison(filePath, list);

  }
}
