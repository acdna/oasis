import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { OnpVerification } from '../entity/onp.verification.entity';
import { OnpVerificationUDto } from '../dto/update/onp.verification.udto';
import { OnpVerificationCDto } from '../dto/create/onp.verification.cdto';
import { OnpVerificationQDto } from '../dto/query/onp.verification.qdto';
import { OnpVerificationService } from '../service/onp.verification.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { ONP_VERIFICATION_PMS } from '../permission/onp.verification.pms';
import { SkipAuth } from '../../common/auth/guard/skip.auth';

/**
 * ONP_VERIFICATION表对应控制器类
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpVerificationController
 */
@SkipAuth()
@Controller('onp/verification')
@ApiTags('访问ONP_VERIFICATION表的控制器类')
export class OnpVerificationController {
  constructor(@Inject(OnpVerificationService) private readonly onpVerificationService: OnpVerificationService) {}

  /**
   * 查询所有记录
   */
  @Permission(ONP_VERIFICATION_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<OnpVerification[]> {
    return this.onpVerificationService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(ONP_VERIFICATION_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: OnpVerificationQDto })
  async findList(@Query() dto: OnpVerificationQDto): Promise<OnpVerification[]> {
    return this.onpVerificationService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(ONP_VERIFICATION_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<OnpVerification> {
    return this.onpVerificationService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(ONP_VERIFICATION_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: OnpVerificationQDto })
  async findOne(@Query() dto: OnpVerificationQDto): Promise<OnpVerification> {
    return this.onpVerificationService.findOne(dto);
  }

  /**
   * 查询ID列表对应记录
   * @param ids
   * @param cols
   */
  @Permission(ONP_VERIFICATION_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<OnpVerification[]> {
    return this.onpVerificationService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(ONP_VERIFICATION_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: OnpVerificationQDto })
  async findPage(@Query() query: OnpVerificationQDto): Promise<any> {
    return this.onpVerificationService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(ONP_VERIFICATION_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: OnpVerificationQDto })
  async count(@Query() query: OnpVerificationQDto): Promise<number> {
    return this.onpVerificationService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(ONP_VERIFICATION_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.onpVerificationService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(ONP_VERIFICATION_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Body('ids') ids: string[]): Promise<any> {
    return this.onpVerificationService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(ONP_VERIFICATION_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: OnpVerificationQDto })
  async delete(@Query() query: OnpVerificationQDto): Promise<any> {
    return this.onpVerificationService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(ONP_VERIFICATION_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: OnpVerificationUDto })
  async update(@Body() dto: OnpVerificationUDto): Promise<any> {
    return this.onpVerificationService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(ONP_VERIFICATION_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: OnpVerificationUDto })
  async updateByIds(@Body() dto: OnpVerificationUDto): Promise<any> {
    return this.onpVerificationService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(ONP_VERIFICATION_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [OnpVerificationUDto] })
  async updateList(@Body() dtos: OnpVerificationUDto[]): Promise<any> {
    return this.onpVerificationService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(ONP_VERIFICATION_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: OnpVerificationUDto })
  async updateSelective(@Body() dto: OnpVerificationUDto): Promise<any> {
    return this.onpVerificationService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(ONP_VERIFICATION_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [OnpVerificationUDto] })
  async updateListSelective(@Body() dtos: OnpVerificationUDto[]): Promise<any> {
    return this.onpVerificationService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(ONP_VERIFICATION_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: OnpVerificationCDto })
  async create(@Body() dto: OnpVerificationCDto): Promise<OnpVerificationCDto> {
    return this.onpVerificationService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(ONP_VERIFICATION_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [OnpVerificationCDto] })
  async createList(@Body() dtos: OnpVerificationCDto[]): Promise<OnpVerificationCDto[]> {
    return this.onpVerificationService.createList(dtos);
  }
}
