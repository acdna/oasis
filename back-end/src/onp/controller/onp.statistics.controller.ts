import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { OnpStatistics } from '../entity/onp.statistics.entity';
import { OnpStatisticsUDto } from '../dto/update/onp.statistics.udto';
import { OnpStatisticsCDto } from '../dto/create/onp.statistics.cdto';
import { OnpStatisticsQDto } from '../dto/query/onp.statistics.qdto';
import { OnpStatisticsService } from '../service/onp.statistics.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { ONP_STATISTICS_PMS } from '../permission/onp.statistics.pms';
import { SkipAuth } from '../../common/auth/guard/skip.auth';
import { LogUtils } from '../../common/log4js/log4js.utils';
import {OnpStatisticsDownloader} from "../downloader/onp.statistics.downloader";

/**
 * ONP_STATISTICS表对应控制器类
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpStatisticsController
 */
@SkipAuth()
@Controller('onp/statistics')
@ApiTags('访问ONP_STATISTICS表的控制器类')
export class OnpStatisticsController {
  constructor(@Inject(OnpStatisticsService) private readonly onpStatisticsService: OnpStatisticsService) {}

  /**
   * 查询所有记录
   */
  @Permission(ONP_STATISTICS_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<OnpStatistics[]> {
    return this.onpStatisticsService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(ONP_STATISTICS_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsQDto })
  async findList(@Query() dto: OnpStatisticsQDto): Promise<OnpStatistics[]> {
    return this.onpStatisticsService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(ONP_STATISTICS_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<OnpStatistics> {
    return this.onpStatisticsService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(ONP_STATISTICS_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsQDto })
  async findOne(@Query() dto: OnpStatisticsQDto): Promise<OnpStatistics> {
    return this.onpStatisticsService.findOne(dto);
  }

  /**
   * 查询ID列表对应记录
   * @param ids
   * @param cols
   */
  @Permission(ONP_STATISTICS_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<OnpStatistics[]> {
    return this.onpStatisticsService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(ONP_STATISTICS_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: OnpStatisticsQDto })
  async findPage(@Query() query: OnpStatisticsQDto): Promise<any> {
    return this.onpStatisticsService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(ONP_STATISTICS_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: OnpStatisticsQDto })
  async count(@Query() query: OnpStatisticsQDto): Promise<number> {
    return this.onpStatisticsService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(ONP_STATISTICS_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.onpStatisticsService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(ONP_STATISTICS_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Body('ids') ids: string[]): Promise<any> {
    return this.onpStatisticsService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(ONP_STATISTICS_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: OnpStatisticsQDto })
  async delete(@Query() query: OnpStatisticsQDto): Promise<any> {
    return this.onpStatisticsService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(ONP_STATISTICS_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsUDto })
  async update(@Body() dto: OnpStatisticsUDto): Promise<any> {
    return this.onpStatisticsService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(ONP_STATISTICS_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsUDto })
  async updateByIds(@Body() dto: OnpStatisticsUDto): Promise<any> {
    return this.onpStatisticsService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(ONP_STATISTICS_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [OnpStatisticsUDto] })
  async updateList(@Body() dtos: OnpStatisticsUDto[]): Promise<any> {
    return this.onpStatisticsService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(ONP_STATISTICS_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsUDto })
  async updateSelective(@Body() dto: OnpStatisticsUDto): Promise<any> {
    return this.onpStatisticsService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(ONP_STATISTICS_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [OnpStatisticsUDto] })
  async updateListSelective(@Body() dtos: OnpStatisticsUDto[]): Promise<any> {
    return this.onpStatisticsService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(ONP_STATISTICS_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsCDto })
  async create(@Body() dto: OnpStatisticsCDto): Promise<OnpStatisticsCDto> {
    return this.onpStatisticsService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(ONP_STATISTICS_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [OnpStatisticsCDto] })
  async createList(@Body() dtos: OnpStatisticsCDto[]): Promise<OnpStatisticsCDto[]> {
    return this.onpStatisticsService.createList(dtos);
  }

  /************新增功能*************/
  /**
   * @Description: 上传
   * @param dto
   * @author zhengenze
   * @date 2021/4/8 3:23 下午
   **/
  @Permission(ONP_STATISTICS_PMS.UPLOAD)
  @Post('upload')
  @ApiOperation({ summary: '批量导入新记录' })
  @ApiQuery({ name: 'dto', type: OnpStatisticsCDto })
  async upload(@Body() dto: OnpStatisticsCDto): Promise<OnpStatisticsCDto> {
    return this.onpStatisticsService.upload(dto);
  }

  /**
   * @Description: 条件下载
   * @author zhengenze
   * @date 2021/4/9 10:56 上午
   **/
  @Permission(ONP_STATISTICS_PMS.DOWNLOAD_STATISTICS_EXCEL)
  @Post('download/statistics/excel')
  @ApiOperation({ summary: '导出信息' })
  @ApiQuery({ name: 'dto', type: String })
  async downloadByBins(@Body() dto: OnpStatisticsQDto): Promise<any> {
    LogUtils.info('APP', `stBin=>${dto}`);
    if (!dto || dto.stFilePath?.length === 0) {
      return null;
    }
    return OnpStatisticsDownloader.downloadByBins(dto);
  }

  /**
   * @Description: 下载全部
   * @author zhengenze
   * @date 2021/4/12 10:46 上午
   **/
  @Permission(ONP_STATISTICS_PMS.DOWNLOAD_All_STATISTICS_EXCEL)
  @Post('download/all/statistics/excel')
  @ApiOperation({ summary: '导出全部信息' })
  @ApiQuery({ name: 'dto', type: String })
  async downloadAll(@Body() dto: OnpStatisticsQDto): Promise<any> {
    return OnpStatisticsDownloader.downloadAll(dto);
  }
}
