import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ScheduleModule} from '@nestjs/schedule';
import {EventEmitterModule} from '@nestjs/event-emitter';
import {APP_INTERCEPTOR} from '@nestjs/core';
import {ErrorsInterceptor} from '../common/interceptor/errors.interceptor';
import {OnpForecastController} from './controller/onp.forecast.controller';
import {OnpForecastService} from './service/onp.forecast.service';
import {OnpForecast} from './entity/onp.forecast.entity';
import {OnpStatisticsController} from './controller/onp.statistics.controller';
import {OnpStatisticsService} from './service/onp.statistics.service';
import {OnpStatistics} from './entity/onp.statistics.entity';
import {OnpVerificationController} from './controller/onp.verification.controller';
import {OnpVerificationService} from './service/onp.verification.service';
import {OnpVerification} from './entity/onp.verification.entity';
import {OnpChloroplastHaplotype} from "./entity/onp.chloroplast.haplotype.entity";
import {OnpChloroplastOverall} from "./entity/onp.chloroplast.overall.entity";
import {OnpNucleusHaplotype} from "./entity/onp.nucleus.haplotype.entity";
import {OnpChloroplastHaplotypeController} from "./controller/onp.chloroplast.haplotype.controller";
import {OnpChloroplastOverallController} from "./controller/onp.chloroplast.overall.controller";
import {OnpNucleusHaplotypeController} from "./controller/onp.nucleus.haplotype.controller";
import {OnpChloroplastHaplotypeService} from "./service/onp.chloroplast.haplotype.service";
import {OnpChloroplastOverallService} from "./service/onp.chloroplast.overall.service";
import {OnpNucleusHaplotypeService} from "./service/onp.nucleus.haplotype.service";

@Module({
  imports: [ScheduleModule.forRoot(), EventEmitterModule.forRoot(), TypeOrmModule.forFeature([OnpChloroplastHaplotype, OnpChloroplastOverall, OnpNucleusHaplotype, OnpForecast, OnpStatistics, OnpVerification])],
  controllers: [OnpChloroplastHaplotypeController, OnpChloroplastOverallController, OnpNucleusHaplotypeController, OnpForecastController, OnpStatisticsController, OnpVerificationController],
  providers: [{
    provide: APP_INTERCEPTOR,
    useClass: ErrorsInterceptor
  }, OnpChloroplastHaplotypeService, OnpChloroplastOverallService, OnpNucleusHaplotypeService, OnpForecastService, OnpStatisticsService, OnpVerificationService],
  exports: [OnpChloroplastHaplotypeService, OnpChloroplastOverallService, OnpNucleusHaplotypeService, OnpForecastService, OnpStatisticsService, OnpVerificationService],
})
export class OnpModule {
}
