import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { OnpNucleusHaplotype } from "../entity/onp.nucleus.haplotype.entity";
import { OnpNucleusHaplotypeUDto } from "../dto/update/onp.nucleus.haplotype.udto";
import { OnpNucleusHaplotypeCDto } from "../dto/create/onp.nucleus.haplotype.cdto";
import { OnpNucleusHaplotypeQDto } from "../dto/query/onp.nucleus.haplotype.qdto";
import { OnpNucleusHaplotypeSQL } from '../entity/sql/onp.nucleus.haplotype.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
/**
 * ONP_NUCLEUS_HAPLOTYPE表对应服务层类
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export
 * @class OnpNucleusHaplotypeService
 */
@Injectable()
export class OnpNucleusHaplotypeService {
    constructor(@InjectRepository(OnpNucleusHaplotype) private readonly entityRepo: Repository < OnpNucleusHaplotype > ) {}
    /**
     * 获取实体类(OnpNucleusHaplotype)的Repository对象
     */
    get repository(): Repository < OnpNucleusHaplotype > {
        return this.entityRepo;
    }
    /**
     * 查询所有记录
     */
    async findAll(): Promise < OnpNucleusHaplotype[] > {
        return await this.entityRepo.find();
    }
    /**
     * 条件查询记录
     * @param query
     */
    async findList(dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): Promise < OnpNucleusHaplotype[] > {
        let querySql = OnpNucleusHaplotypeSQL.SELECT_SQL(dto);
        return JsonUtils.format(await this.entityRepo.query(querySql));
    }
    /**
     * 查询ID对应记录
     * @param id
     */
    async findById(id: string): Promise < OnpNucleusHaplotype > {
        return await this.entityRepo.findOne(id);
    }
    /**
     * 查询ID对应记录
     * @param ids
     * @param cols
     */
    async findByIdList(ids: string[], cols = []): Promise < OnpNucleusHaplotype[] > {
        let querySql = OnpNucleusHaplotypeSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
        return JsonUtils.format(await this.entityRepo.query(querySql));
    }
    /**
     * 分页查询记录
     * @param query
     */
    async findPage(query: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): Promise < any > {
        let count: number = await this.count(query);
        query.offset = +(query.offset) || 0;
        query.pageSize = +(query.pageSize) || 20;
        query.offset = count < query.offset ? 0 : query.offset;
        if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
        let querySql = OnpNucleusHaplotypeSQL.SELECT_SQL(query);
        let data = JsonUtils.format(await this.entityRepo.query(querySql));
        return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
    }
    /**
     * 查询单条记录
     * @param dto
     */
    async findOne(dto: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): Promise < OnpNucleusHaplotype > {
        dto.offset = 0;
        dto.pageSize = 1;
        let sql = OnpNucleusHaplotypeSQL.SELECT_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 查询记录数
     * @param query
     */
    async count(query: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): Promise < number > {
        let countSql = OnpNucleusHaplotypeSQL.SELECT_COUNT_SQL(query);
        let result = JsonUtils.first(await this.entityRepo.query(countSql));
        return +(result.count) || 0;
    }
    /**
     * 删除指定ID记录
     * @param id
     */
    async deleteById(id: string): Promise < any > {
        return await this.entityRepo.delete({ onhId: id });
    }
    /**
     * 删除指定ID记录列表
     * @param ids
     */
    async deleteByIdList(ids: string[]): Promise < any > {
        return await this.entityRepo.delete(ids);
    }
    /**
     * 条件查询记录
     * @param query
     */
    async delete(query: OnpNucleusHaplotypeQDto | Partial < OnpNucleusHaplotypeQDto > ): Promise < any > {
        let deleteSql = OnpNucleusHaplotypeSQL.DELETE_SELECTIVE_SQL(query);
        return JsonUtils.first(await this.entityRepo.query(deleteSql));
    }
    /**
     * 删除所有记录
     */
    async deleteAll(): Promise < any > {
        let deleteSql = OnpNucleusHaplotypeSQL.DELETE_ALL_SQL();
        return JsonUtils.first(await this.entityRepo.query(deleteSql));
    }
    /**
     * 更新指定ID记录
     * @param dto
     */
    async updateSelective(dto: OnpNucleusHaplotypeUDto | Partial < OnpNucleusHaplotypeUDto > ): Promise < any > {
        if (!dto.onhId || dto.onhId.length == 0) return {};
        let sql = OnpNucleusHaplotypeSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量更新指定ID记录，只更新给定对象中有值的属性
     * @param dtos
     */
    async updateListSelective(dtos: OnpNucleusHaplotypeUDto[] | Partial < OnpNucleusHaplotypeUDto > []): Promise < any > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.onhId) dto.onhId = uuid();
        });
        let results = [];
        await this.entityRepo.manager.transaction(async manager => {
            for (let dto of dtos) {
                let result = await this.updateSelective(dto);
                results.push(result);
            }
        });
        return results;
    }
    /**
     * 更新指定ID记录
     * @param dto
     */
    async update(dto: OnpNucleusHaplotypeUDto | Partial < OnpNucleusHaplotypeUDto > ): Promise < any > {
        if (!dto.onhId || dto.onhId.length == 0) return {};
        let sql = OnpNucleusHaplotypeSQL.UPDATE_BY_ID_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 更新指定ID列表记录
     * @param dto
     */
    async updateByIds(dto: OnpNucleusHaplotypeUDto | Partial < OnpNucleusHaplotypeUDto > ): Promise < any > {
        if (!dto.onhIdList || dto.onhIdList.length == 0) return {};
        let sql = OnpNucleusHaplotypeSQL.UPDATE_BY_IDS_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
     * @param dtos
     */
    async updateList(dtos: OnpNucleusHaplotypeUDto[] | Partial < OnpNucleusHaplotypeUDto > []): Promise < any > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.onhId) dto.onhId = uuid();
        });
        let results = [];
        await this.entityRepo.manager.transaction(async manager => {
            for (let dto of dtos) {
                let result = await this.update(dto);
                results.push(result);
            }
        });
        return results;
    }
    /**
     * 创建新记录
     * @param dto
     */
    async create(dto: OnpNucleusHaplotypeCDto | Partial < OnpNucleusHaplotypeCDto > ): Promise < OnpNucleusHaplotypeCDto > {
        if (!dto.onhId) {
            dto.onhId = uuid();
        }
        let sql = OnpNucleusHaplotypeSQL.INSERT_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量创建新记录
     * @param dto
     */
    async createList(dtos: OnpNucleusHaplotypeCDto[] | Partial < OnpNucleusHaplotypeCDto > []): Promise < OnpNucleusHaplotypeCDto[] > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.onhId) dto.onhId = uuid();
        });
        let result = [];
        await this.entityRepo.manager.transaction(async manager => {
            let sql = OnpNucleusHaplotypeSQL.BATCH_INSERT_SQL(dtos);
            result = JsonUtils.first(await this.entityRepo.query(sql));
        });
        return result;
    }
}