import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {OnpForecast} from '../entity/onp.forecast.entity';
import {OnpForecastUDto} from '../dto/update/onp.forecast.udto';
import {OnpForecastCDto} from '../dto/create/onp.forecast.cdto';
import {OnpForecastQDto} from '../dto/query/onp.forecast.qdto';
import {OnpForecastSQL} from '../entity/sql/onp.forecast.sql';
import {uuid} from '../../common/utils/uuid.utils';
import {JsonUtils} from '../../common/typeorm/json.utils';
import {OnpForecastSaver} from "../saver/onp.forecast.saver";
import {OnpServices} from "../onp.services";

/**
 * ONP_FORECAST表对应服务层类
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpForecastService
 */
@Injectable()
export class OnpForecastService {
  constructor(@InjectRepository(OnpForecast) private readonly entityRepo: Repository<OnpForecast>) {
  }

  /**
   * 获取实体类(OnpForecast)的Repository对象
   */
  get repository(): Repository<OnpForecast> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<OnpForecast[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: OnpForecastQDto | Partial<OnpForecastQDto>): Promise<OnpForecast[]> {
    let querySql = OnpForecastSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<OnpForecast> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<OnpForecast[]> {
    let querySql = OnpForecastSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: OnpForecastQDto | Partial<OnpForecastQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return {data: [], page: {count: 0, offset: query.offset, pageSize: query.pageSize}};
    let querySql = OnpForecastSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return {data, page: {count: +count, offset: query.offset, pageSize: query.pageSize}};
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: OnpForecastQDto | Partial<OnpForecastQDto>): Promise<OnpForecast> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = OnpForecastSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: OnpForecastQDto | Partial<OnpForecastQDto>): Promise<number> {
    let countSql = OnpForecastSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({foId: id});
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: OnpForecastQDto | Partial<OnpForecastQDto>): Promise<any> {
    let deleteSql = OnpForecastSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 删除所有记录
   */
  async deleteAll(): Promise<any> {
    let deleteSql = OnpForecastSQL.DELETE_ALL_SQL();
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: OnpForecastUDto | Partial<OnpForecastUDto>): Promise<any> {
    if (!dto.foId || dto.foId.length == 0) return {};
    let sql = OnpForecastSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: OnpForecastUDto[] | Partial<OnpForecastUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.foId) dto.foId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: OnpForecastUDto | Partial<OnpForecastUDto>): Promise<any> {
    if (!dto.foId || dto.foId.length == 0) return {};
    let sql = OnpForecastSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: OnpForecastUDto | Partial<OnpForecastUDto>): Promise<any> {
    if (!dto.foIdList || dto.foIdList.length == 0) return {};
    let sql = OnpForecastSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: OnpForecastUDto[] | Partial<OnpForecastUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.foId) dto.foId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: OnpForecastCDto | Partial<OnpForecastCDto>): Promise<OnpForecastCDto> {
    if (!dto.foId) {
      dto.foId = uuid();
    }
    let sql = OnpForecastSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: OnpForecastCDto[] | Partial<OnpForecastCDto>[]): Promise<OnpForecastCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.foId) dto.foId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = OnpForecastSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }

  /*************新增功能*****************/
  /**
   * @Description: 上传信息
   * @author zhengenze
   * @date 2021/4/6 3:57 下午
   **/
  async upload(dto: OnpForecastCDto | Partial<OnpForecastCDto>): Promise<any> {
    if (!dto) return false;
    if (dto.foFilePath?.length > 0) {
      return await OnpServices.onpForecastService.create(dto);
      // return await OnpForecastSaver.saver(dto);
    }
    return false;
  }
}
