import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { OnpVerification } from "../entity/onp.verification.entity";
import { OnpVerificationUDto } from "../dto/update/onp.verification.udto";
import { OnpVerificationCDto } from "../dto/create/onp.verification.cdto";
import { OnpVerificationQDto } from "../dto/query/onp.verification.qdto";
import { OnpVerificationSQL } from '../entity/sql/onp.verification.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
/**
 * ONP_VERIFICATION表对应服务层类
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpVerificationService
 */
@Injectable()
export class OnpVerificationService {
    constructor(@InjectRepository(OnpVerification) private readonly entityRepo: Repository < OnpVerification > ) {}
    /**
     * 获取实体类(OnpVerification)的Repository对象
     */
    get repository(): Repository < OnpVerification > {
        return this.entityRepo;
    }
    /**
     * 查询所有记录
     */
    async findAll(): Promise < OnpVerification[] > {
        return await this.entityRepo.find();
    }
    /**
     * 条件查询记录
     * @param query
     */
    async findList(dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): Promise < OnpVerification[] > {
        let querySql = OnpVerificationSQL.SELECT_SQL(dto);
        return JsonUtils.format(await this.entityRepo.query(querySql));
    }
    /**
     * 查询ID对应记录
     * @param id
     */
    async findById(id: string): Promise < OnpVerification > {
        return await this.entityRepo.findOne(id);
    }
    /**
     * 查询ID对应记录
     * @param ids
     * @param cols
     */
    async findByIdList(ids: string[], cols = []): Promise < OnpVerification[] > {
        let querySql = OnpVerificationSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
        return JsonUtils.format(await this.entityRepo.query(querySql));
    }
    /**
     * 分页查询记录
     * @param query
     */
    async findPage(query: OnpVerificationQDto | Partial < OnpVerificationQDto > ): Promise < any > {
        let count: number = await this.count(query);
        query.offset = +(query.offset) || 0;
        query.pageSize = +(query.pageSize) || 20;
        query.offset = count < query.offset ? 0 : query.offset;
        if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
        let querySql = OnpVerificationSQL.SELECT_SQL(query);
        let data = JsonUtils.format(await this.entityRepo.query(querySql));
        return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
    }
    /**
     * 查询单条记录
     * @param dto
     */
    async findOne(dto: OnpVerificationQDto | Partial < OnpVerificationQDto > ): Promise < OnpVerification > {
        dto.offset = 0;
        dto.pageSize = 1;
        let sql = OnpVerificationSQL.SELECT_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 查询记录数
     * @param query
     */
    async count(query: OnpVerificationQDto | Partial < OnpVerificationQDto > ): Promise < number > {
        let countSql = OnpVerificationSQL.SELECT_COUNT_SQL(query);
        let result = JsonUtils.first(await this.entityRepo.query(countSql));
        return +(result.count) || 0;
    }
    /**
     * 删除指定ID记录
     * @param id
     */
    async deleteById(id: string): Promise < any > {
        return await this.entityRepo.delete({ veId: id });
    }
    /**
     * 删除指定ID记录列表
     * @param ids
     */
    async deleteByIdList(ids: string[]): Promise < any > {
        return await this.entityRepo.delete(ids);
    }
    /**
     * 条件查询记录
     * @param query
     */
    async delete(query: OnpVerificationQDto | Partial < OnpVerificationQDto > ): Promise < any > {
        let deleteSql = OnpVerificationSQL.DELETE_SELECTIVE_SQL(query);
        return JsonUtils.first(await this.entityRepo.query(deleteSql));
    }
    /**
     * 删除所有记录
     */
    async deleteAll(): Promise < any > {
        let deleteSql = OnpVerificationSQL.DELETE_ALL_SQL();
        return JsonUtils.first(await this.entityRepo.query(deleteSql));
    }
    /**
     * 更新指定ID记录
     * @param dto
     */
    async updateSelective(dto: OnpVerificationUDto | Partial < OnpVerificationUDto > ): Promise < any > {
        if (!dto.veId || dto.veId.length == 0) return {};
        let sql = OnpVerificationSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量更新指定ID记录，只更新给定对象中有值的属性
     * @param dtos
     */
    async updateListSelective(dtos: OnpVerificationUDto[] | Partial < OnpVerificationUDto > []): Promise < any > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.veId) dto.veId = uuid();
        });
        let results = [];
        await this.entityRepo.manager.transaction(async manager => {
            for (let dto of dtos) {
                let result = await this.updateSelective(dto);
                results.push(result);
            }
        });
        return results;
    }
    /**
     * 更新指定ID记录
     * @param dto
     */
    async update(dto: OnpVerificationUDto | Partial < OnpVerificationUDto > ): Promise < any > {
        if (!dto.veId || dto.veId.length == 0) return {};
        let sql = OnpVerificationSQL.UPDATE_BY_ID_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 更新指定ID列表记录
     * @param dto
     */
    async updateByIds(dto: OnpVerificationUDto | Partial < OnpVerificationUDto > ): Promise < any > {
        if (!dto.veIdList || dto.veIdList.length == 0) return {};
        let sql = OnpVerificationSQL.UPDATE_BY_IDS_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
     * @param dtos
     */
    async updateList(dtos: OnpVerificationUDto[] | Partial < OnpVerificationUDto > []): Promise < any > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.veId) dto.veId = uuid();
        });
        let results = [];
        await this.entityRepo.manager.transaction(async manager => {
            for (let dto of dtos) {
                let result = await this.update(dto);
                results.push(result);
            }
        });
        return results;
    }
    /**
     * 创建新记录
     * @param dto
     */
    async create(dto: OnpVerificationCDto | Partial < OnpVerificationCDto > ): Promise < OnpVerificationCDto > {
        if (!dto.veId) {
            dto.veId = uuid();
        }
        let sql = OnpVerificationSQL.INSERT_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量创建新记录
     * @param dto
     */
    async createList(dtos: OnpVerificationCDto[] | Partial < OnpVerificationCDto > []): Promise < OnpVerificationCDto[] > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.veId) dto.veId = uuid();
        });
        let result = [];
        await this.entityRepo.manager.transaction(async manager => {
            let sql = OnpVerificationSQL.BATCH_INSERT_SQL(dtos);
            result = JsonUtils.first(await this.entityRepo.query(sql));
        });
        return result;
    }
}