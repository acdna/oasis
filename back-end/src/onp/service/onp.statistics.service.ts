import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OnpStatistics } from '../entity/onp.statistics.entity';
import { OnpStatisticsUDto } from '../dto/update/onp.statistics.udto';
import { OnpStatisticsCDto } from '../dto/create/onp.statistics.cdto';
import { OnpStatisticsQDto } from '../dto/query/onp.statistics.qdto';
import { OnpStatisticsSQL } from '../entity/sql/onp.statistics.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import {OnpStatisticsParser} from "../excel/parser/onp.statistics.excel.parser";
import {OnpStatisticsSaver} from "../saver/onp.statistics.saver";
import {OnpServices} from "../onp.services";
/**
 * ONP_STATISTICS表对应服务层类
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export
 * @class OnpStatisticsService
 */
@Injectable()
export class OnpStatisticsService {
  constructor(@InjectRepository(OnpStatistics) private readonly entityRepo: Repository<OnpStatistics>) {}
  /**
   * 获取实体类(OnpStatistics)的Repository对象
   */
  get repository(): Repository<OnpStatistics> {
    return this.entityRepo;
  }
  /**
   * 查询所有记录
   */
  async findAll(): Promise<OnpStatistics[]> {
    return await this.entityRepo.find();
  }
  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: OnpStatisticsQDto | Partial<OnpStatisticsQDto>): Promise<OnpStatistics[]> {
    let querySql = OnpStatisticsSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }
  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<OnpStatistics> {
    return await this.entityRepo.findOne(id);
  }
  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<OnpStatistics[]> {
    let querySql = OnpStatisticsSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }
  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: OnpStatisticsQDto | Partial<OnpStatisticsQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = OnpStatisticsSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }
  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: OnpStatisticsQDto | Partial<OnpStatisticsQDto>): Promise<OnpStatistics> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = OnpStatisticsSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 查询记录数
   * @param query
   */
  async count(query: OnpStatisticsQDto | Partial<OnpStatisticsQDto>): Promise<number> {
    let countSql = OnpStatisticsSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }
  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ stId: id });
  }
  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }
  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: OnpStatisticsQDto | Partial<OnpStatisticsQDto>): Promise<any> {
    let deleteSql = OnpStatisticsSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }
  /**
   * 删除所有记录
   */
  async deleteAll(): Promise<any> {
    let deleteSql = OnpStatisticsSQL.DELETE_ALL_SQL();
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }
  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: OnpStatisticsUDto | Partial<OnpStatisticsUDto>): Promise<any> {
    if (!dto.stId || dto.stId.length == 0) return {};
    let sql = OnpStatisticsSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: OnpStatisticsUDto[] | Partial<OnpStatisticsUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.stId) dto.stId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }
  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: OnpStatisticsUDto | Partial<OnpStatisticsUDto>): Promise<any> {
    if (!dto.stId || dto.stId.length == 0) return {};
    let sql = OnpStatisticsSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: OnpStatisticsUDto | Partial<OnpStatisticsUDto>): Promise<any> {
    if (!dto.stIdList || dto.stIdList.length == 0) return {};
    let sql = OnpStatisticsSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: OnpStatisticsUDto[] | Partial<OnpStatisticsUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.stId) dto.stId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }
  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: OnpStatisticsCDto | Partial<OnpStatisticsCDto>): Promise<OnpStatisticsCDto> {
    if (!dto.stId) {
      dto.stId = uuid();
    }
    let sql = OnpStatisticsSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: OnpStatisticsCDto[] | Partial<OnpStatisticsCDto>[]): Promise<OnpStatisticsCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.stId) dto.stId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = OnpStatisticsSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }

  /**
   * @Description: 批量上传信息
   * @author zhengenze
   * @date 2021/4/8 3:30 下午
   **/
  async upload(dto: OnpStatisticsCDto | Partial<OnpStatisticsCDto>): Promise<any> {
    if (!dto) return false;
    let statistics = OnpStatisticsParser.parse({ relativePath: dto.stFilePath });
    if (statistics?.length > 0) {
      return await OnpServices.onpStatisticsService.createList(statistics);
    }
    return false;
  }


}
