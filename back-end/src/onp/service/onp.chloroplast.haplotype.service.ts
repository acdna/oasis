import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { OnpChloroplastHaplotype } from "../entity/onp.chloroplast.haplotype.entity";
import { OnpChloroplastHaplotypeUDto } from "../dto/update/onp.chloroplast.haplotype.udto";
import { OnpChloroplastHaplotypeCDto } from "../dto/create/onp.chloroplast.haplotype.cdto";
import { OnpChloroplastHaplotypeQDto } from "../dto/query/onp.chloroplast.haplotype.qdto";
import { OnpChloroplastHaplotypeSQL } from '../entity/sql/onp.chloroplast.haplotype.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
/**
 * ONP_CHLOROPLAST_HAPLOTYPE表对应服务层类
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export
 * @class OnpChloroplastHaplotypeService
 */
@Injectable()
export class OnpChloroplastHaplotypeService {
    constructor(@InjectRepository(OnpChloroplastHaplotype) private readonly entityRepo: Repository < OnpChloroplastHaplotype > ) {}
    /**
     * 获取实体类(OnpChloroplastHaplotype)的Repository对象
     */
    get repository(): Repository < OnpChloroplastHaplotype > {
        return this.entityRepo;
    }
    /**
     * 查询所有记录
     */
    async findAll(): Promise < OnpChloroplastHaplotype[] > {
        return await this.entityRepo.find();
    }
    /**
     * 条件查询记录
     * @param query
     */
    async findList(dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): Promise < OnpChloroplastHaplotype[] > {
        let querySql = OnpChloroplastHaplotypeSQL.SELECT_SQL(dto);
        return JsonUtils.format(await this.entityRepo.query(querySql));
    }
    /**
     * 查询ID对应记录
     * @param id
     */
    async findById(id: string): Promise < OnpChloroplastHaplotype > {
        return await this.entityRepo.findOne(id);
    }
    /**
     * 查询ID对应记录
     * @param ids
     * @param cols
     */
    async findByIdList(ids: string[], cols = []): Promise < OnpChloroplastHaplotype[] > {
        let querySql = OnpChloroplastHaplotypeSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
        return JsonUtils.format(await this.entityRepo.query(querySql));
    }
    /**
     * 分页查询记录
     * @param query
     */
    async findPage(query: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): Promise < any > {
        let count: number = await this.count(query);
        query.offset = +(query.offset) || 0;
        query.pageSize = +(query.pageSize) || 20;
        query.offset = count < query.offset ? 0 : query.offset;
        if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
        let querySql = OnpChloroplastHaplotypeSQL.SELECT_SQL(query);
        let data = JsonUtils.format(await this.entityRepo.query(querySql));
        return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
    }
    /**
     * 查询单条记录
     * @param dto
     */
    async findOne(dto: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): Promise < OnpChloroplastHaplotype > {
        dto.offset = 0;
        dto.pageSize = 1;
        let sql = OnpChloroplastHaplotypeSQL.SELECT_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 查询记录数
     * @param query
     */
    async count(query: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): Promise < number > {
        let countSql = OnpChloroplastHaplotypeSQL.SELECT_COUNT_SQL(query);
        let result = JsonUtils.first(await this.entityRepo.query(countSql));
        return +(result.count) || 0;
    }
    /**
     * 删除指定ID记录
     * @param id
     */
    async deleteById(id: string): Promise < any > {
        return await this.entityRepo.delete({ ochId: id });
    }
    /**
     * 删除指定ID记录列表
     * @param ids
     */
    async deleteByIdList(ids: string[]): Promise < any > {
        return await this.entityRepo.delete(ids);
    }
    /**
     * 条件查询记录
     * @param query
     */
    async delete(query: OnpChloroplastHaplotypeQDto | Partial < OnpChloroplastHaplotypeQDto > ): Promise < any > {
        let deleteSql = OnpChloroplastHaplotypeSQL.DELETE_SELECTIVE_SQL(query);
        return JsonUtils.first(await this.entityRepo.query(deleteSql));
    }
    /**
     * 删除所有记录
     */
    async deleteAll(): Promise < any > {
        let deleteSql = OnpChloroplastHaplotypeSQL.DELETE_ALL_SQL();
        return JsonUtils.first(await this.entityRepo.query(deleteSql));
    }
    /**
     * 更新指定ID记录
     * @param dto
     */
    async updateSelective(dto: OnpChloroplastHaplotypeUDto | Partial < OnpChloroplastHaplotypeUDto > ): Promise < any > {
        if (!dto.ochId || dto.ochId.length == 0) return {};
        let sql = OnpChloroplastHaplotypeSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量更新指定ID记录，只更新给定对象中有值的属性
     * @param dtos
     */
    async updateListSelective(dtos: OnpChloroplastHaplotypeUDto[] | Partial < OnpChloroplastHaplotypeUDto > []): Promise < any > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.ochId) dto.ochId = uuid();
        });
        let results = [];
        await this.entityRepo.manager.transaction(async manager => {
            for (let dto of dtos) {
                let result = await this.updateSelective(dto);
                results.push(result);
            }
        });
        return results;
    }
    /**
     * 更新指定ID记录
     * @param dto
     */
    async update(dto: OnpChloroplastHaplotypeUDto | Partial < OnpChloroplastHaplotypeUDto > ): Promise < any > {
        if (!dto.ochId || dto.ochId.length == 0) return {};
        let sql = OnpChloroplastHaplotypeSQL.UPDATE_BY_ID_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 更新指定ID列表记录
     * @param dto
     */
    async updateByIds(dto: OnpChloroplastHaplotypeUDto | Partial < OnpChloroplastHaplotypeUDto > ): Promise < any > {
        if (!dto.ochIdList || dto.ochIdList.length == 0) return {};
        let sql = OnpChloroplastHaplotypeSQL.UPDATE_BY_IDS_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
     * @param dtos
     */
    async updateList(dtos: OnpChloroplastHaplotypeUDto[] | Partial < OnpChloroplastHaplotypeUDto > []): Promise < any > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.ochId) dto.ochId = uuid();
        });
        let results = [];
        await this.entityRepo.manager.transaction(async manager => {
            for (let dto of dtos) {
                let result = await this.update(dto);
                results.push(result);
            }
        });
        return results;
    }
    /**
     * 创建新记录
     * @param dto
     */
    async create(dto: OnpChloroplastHaplotypeCDto | Partial < OnpChloroplastHaplotypeCDto > ): Promise < OnpChloroplastHaplotypeCDto > {
        if (!dto.ochId) {
            dto.ochId = uuid();
        }
        let sql = OnpChloroplastHaplotypeSQL.INSERT_SQL(dto);
        return JsonUtils.first(await this.entityRepo.query(sql));
    }
    /**
     * 批量创建新记录
     * @param dto
     */
    async createList(dtos: OnpChloroplastHaplotypeCDto[] | Partial < OnpChloroplastHaplotypeCDto > []): Promise < OnpChloroplastHaplotypeCDto[] > {
        if (!dtos || dtos.length == 0) return [];
        dtos.forEach((dto) => {
            if (!dto.ochId) dto.ochId = uuid();
        });
        let result = [];
        await this.entityRepo.manager.transaction(async manager => {
            let sql = OnpChloroplastHaplotypeSQL.BATCH_INSERT_SQL(dtos);
            result = JsonUtils.first(await this.entityRepo.query(sql));
        });
        return result;
    }
}