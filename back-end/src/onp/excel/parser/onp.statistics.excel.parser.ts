import { PathUtils } from '../../../common/path/path.creator.utils';
import { LogUtils } from '../../../common/log4js/log4js.utils';
import { SheetDataType } from '../../../common/excel/tools/sheet.data';
import { ArrayUtils } from '../../../common/utils/array.utils';
import { OnpStatistics } from '../../entity/onp.statistics.entity';
import { OnpExcelTitles } from '../onp.excel.titles';
import { ExcelParser } from '../../../common/excel/tools/excel.parser';
import { ExcelUtils } from '../../../common/excel/tools/excel.utils';

/**
 * @Description: 数据解析
 * @author zhengenze
 * @date 2021/4/9 8:35 上午
 **/
export const OnpStatisticsParser = {
  /**
   * @Description: 解析信息
   * @param params.relativePath 样品数据文件相对路径
   * @param params.fileId 样品数据关联的指纹数据记录ID
   * @author zhengenze
   * @date 2021/4/9 8:35 上午
   **/
  parse: (params: { relativePath: string }): OnpStatistics[] => {
    //构建Excel全路径
    let path = PathUtils.getFullPath({ relativePath: params.relativePath });

    //解析Excel
    let parser = new ExcelParser();
    let sheets = parser.parse(path);
    if (ArrayUtils.isEmpty(sheets)) {
      LogUtils.warn('OnpStatisticsParser', `Excel中没有数据:${path}`);
      return [];
    }

    //根据标题行格式过滤
    sheets = ExcelUtils.filter(OnpExcelTitles.ONP_STATISTICS.title, sheets);
    if (ArrayUtils.isEmpty(sheets)) {
      LogUtils.warn('OnpStatisticsParser', `Excel标题不正确:${path}`);
      return [];
    }

    //转换成数据列表
    let data = parser.convert<OnpStatistics>(sheets, (params: { sheet: SheetDataType; index: number; row: any[] }) => {
      let { row } = params;
      let colIndex = 0;
      let statistics: OnpStatistics = {
        stId: undefined,
        stBin: row[colIndex++] || '',
        stChr: row[colIndex++] || '',
        stStart: row[colIndex++] || '',
        stStop: row[colIndex++] || '',
        stBinSize: row[colIndex++] || '',
        stDesc: row[colIndex++] || '',
        stAllMarkers: row[colIndex++] || '',
        stSnpMarkers: row[colIndex++] || '',
        stIndelMarkers: row[colIndex++] || '',
        stBlockMarkers: row[colIndex++] || '',
        stTags: row[colIndex++] || '',
        stGenotypes: row[colIndex++] || '',
        stMaxGenotypesFreq: row[colIndex++] || '',
        stMinGenotypesFreq: row[colIndex++] || '',
        stPic: row[colIndex++] || '',
        stGeneticMap: row[colIndex++] || '',
        stOrder: undefined,
        stColumn1: undefined,
        stColumn2: undefined,
        stColumn3: undefined,
        stSpecies: undefined,
        stRemark: undefined,
        stCreateDate: undefined,
        stUpdateDate: undefined,
      };
      return statistics;
    });
    return data;
  },
};
