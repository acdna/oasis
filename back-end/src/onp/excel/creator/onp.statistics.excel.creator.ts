import { OnpStatistics } from '../../entity/onp.statistics.entity';
import { SheetDataType } from '../../../common/excel/tools/sheet.data';
import { ArrayUtils } from '../../../common/utils/array.utils';
import { ExcelCreator } from '../../../common/excel/tools/excel.creator';
import { OnpExcelTitles } from '../onp.excel.titles';

export const OnpStatisticsExcelCreator = {
  create(records: OnpStatistics[]): SheetDataType[] {
    if (ArrayUtils.isEmpty(records)) {
      return null;
    }

    let creator = new ExcelCreator();
    let sheets = creator.create({
      title: OnpExcelTitles.ONP_STATISTICS,
      rows: records,
      render: (params: { titles: string[]; row: OnpStatistics }): string[] => {
        let { row } = params;
        return [
          row.stBin || '',
          row.stChr || '',
          row.stStart || '',
          row.stStop || '',
          row.stBinSize || '',
          row.stDesc || '',
          row.stAllMarkers || '',
          row.stSnpMarkers || '',
          row.stIndelMarkers || '',
          row.stBlockMarkers || '',
          row.stTags || '',
          row.stGenotypes || '',
          row.stMaxGenotypesFreq || '',
          row.stMinGenotypesFreq || '',
          row.stPic || '',
          row.stGeneticMap || '',
        ];
      },
    });

    return sheets;
  },
};
