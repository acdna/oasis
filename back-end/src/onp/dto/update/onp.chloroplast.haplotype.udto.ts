import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { OnpChloroplastHaplotypeColNames } from "../../entity/ddl/onp.chloroplast.haplotype.cols";
/**
 * ONP_CHLOROPLAST_HAPLOTYPE表的UDTO对象
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export OnpChloroplastHaplotypeUDto
 * @class OnpChloroplastHaplotypeUDto
 */
@ApiTags('ONP_CHLOROPLAST_HAPLOTYPE表的UDTO对象')
export class OnpChloroplastHaplotypeUDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiProperty({ name: 'ochId', type: 'string', description: 'id' })
    ochId: string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotype', type: 'string', description: 'Chloroplast' })
    ochChloroplastHaplotype ? : string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochChr', type: 'string', description: 'Chr' })
    ochChr ? : string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastOnp', type: 'string', description: 'Chloroplast' })
    ochChloroplastOnp ? : string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeSequence', type: 'string', description: 'Chloroplast' })
    ochChloroplastHaplotypeSequence ? : string;
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochFrequency', type: 'string', description: 'Frequency' })
    ochFrequency ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochColumn1', type: 'string', description: 'Column1' })
    ochColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochColumn2', type: 'string', description: 'Column2' })
    ochColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochColumn3', type: 'string', description: 'Column3' })
    ochColumn3 ? : string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochOrder', type: 'number', description: 'ID' })
    ochOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochSpecies', type: 'string', description: 'Species' })
    ochSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochRemark', type: 'string', description: 'Remark' })
    ochRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochCreateDate', type: 'Date', description: '创建日期' })
    ochCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'ochUpdateDate', type: 'Date', description: '更新日期' })
    ochUpdateDate ? : Date;
    /**
     * 更新列限定条件,在只想更新部分列数据时可以给定本参数
     *
     * @type { (keyof OnpChloroplastHaplotypeColNames)[] }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpChloroplastHaplotypeColNames)[];
    /**
     * ID列表
     *
     * @type { string[] }
     * @memberof OnpChloroplastHaplotypeUDto
     */
    @ApiProperty({ name: 'ochIdList', type: 'string[]', description: 'ID列表' })
    ochIdList ? : string[];
}