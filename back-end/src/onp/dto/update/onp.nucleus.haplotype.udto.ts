import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { OnpNucleusHaplotypeColNames } from "../../entity/ddl/onp.nucleus.haplotype.cols";
/**
 * ONP_NUCLEUS_HAPLOTYPE表的UDTO对象
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export OnpNucleusHaplotypeUDto
 * @class OnpNucleusHaplotypeUDto
 */
@ApiTags('ONP_NUCLEUS_HAPLOTYPE表的UDTO对象')
export class OnpNucleusHaplotypeUDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiProperty({ name: 'onhId', type: 'string', description: 'id' })
    onhId: string;
    /**
     * Haplotype Index
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeIndex', type: 'string', description: 'Haplotype Index' })
    onhHaplotypeIndex ? : string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhChr', type: 'string', description: 'Chr' })
    onhChr ? : string;
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhOnpId', type: 'string', description: 'ONP ID' })
    onhOnpId ? : string;
    /**
     * Haplotype Sequence
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeSequence', type: 'string', description: 'Haplotype Sequence' })
    onhHaplotypeSequence ? : string;
    /**
     * Haplotype Tag
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeTagSequence', type: 'string', description: 'Haplotype Tag' })
    onhHaplotypeTagSequence ? : string;
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhFrequency', type: 'string', description: 'Frequency' })
    onhFrequency ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhColumn1', type: 'string', description: 'Column1' })
    onhColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhColumn2', type: 'string', description: 'Column2' })
    onhColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhColumn3', type: 'string', description: 'Column3' })
    onhColumn3 ? : string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhOrder', type: 'number', description: 'ID' })
    onhOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhSpecies', type: 'string', description: 'Species' })
    onhSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhRemark', type: 'string', description: 'Remark' })
    onhRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhCreateDate', type: 'Date', description: '创建日期' })
    onhCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiPropertyOptional({ name: 'onhUpdateDate', type: 'Date', description: '更新日期' })
    onhUpdateDate ? : Date;
    /**
     * 更新列限定条件,在只想更新部分列数据时可以给定本参数
     *
     * @type { (keyof OnpNucleusHaplotypeColNames)[] }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpNucleusHaplotypeColNames)[];
    /**
     * ID列表
     *
     * @type { string[] }
     * @memberof OnpNucleusHaplotypeUDto
     */
    @ApiProperty({ name: 'onhIdList', type: 'string[]', description: 'ID列表' })
    onhIdList ? : string[];
}