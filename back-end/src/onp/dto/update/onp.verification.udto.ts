import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { OnpVerificationColNames } from "../../entity/ddl/onp.verification.cols";
/**
 * 验证表的UDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpVerificationUDto
 * @class OnpVerificationUDto
 */
@ApiTags('ONP_VERIFICATION表的UDTO对象')
export class OnpVerificationUDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiProperty({ name: 'veId', type: 'string', description: 'id' })
    veId: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veOrder', type: 'number', description: 'ID' })
    veOrder ? : number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn1', type: 'string', description: 'Column1' })
    veColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn2', type: 'string', description: 'Column2' })
    veColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn3', type: 'string', description: 'Column3' })
    veColumn3 ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn4', type: 'string', description: 'Column1' })
    veColumn4 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn5', type: 'string', description: 'Column2' })
    veColumn5 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn6', type: 'string', description: 'Column3' })
    veColumn6 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn7', type: 'string', description: 'Column3' })
    veColumn7 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn8', type: 'string', description: 'Column3' })
    veColumn8 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veColumn9', type: 'string', description: 'Column3' })
    veColumn9 ? : string;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veSpecies', type: 'string', description: 'Species' })
    veSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veRemark', type: 'string', description: 'Remark' })
    veRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veCreateDate', type: 'Date', description: '创建日期' })
    veCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpVerificationUDto
     */
    @ApiPropertyOptional({ name: 'veUpdateDate', type: 'Date', description: '更新日期' })
    veUpdateDate ? : Date;
    /**
     * 更新列限定条件,在只想更新部分列数据时可以给定本参数
     *
     * @type { (keyof OnpVerificationColNames)[] }
     * @memberof OnpVerificationUDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpVerificationColNames)[];
    /**
     * ID列表
     *
     * @type { string[] }
     * @memberof OnpVerificationUDto
     */
    @ApiProperty({ name: 'veIdList', type: 'string[]', description: 'ID列表' })
    veIdList ? : string[];
}