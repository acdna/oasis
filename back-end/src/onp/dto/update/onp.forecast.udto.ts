import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { OnpForecastColNames } from "../../entity/ddl/onp.forecast.cols";
/**
 * 预测表的UDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpForecastUDto
 * @class OnpForecastUDto
 */
@ApiTags('ONP_FORECAST表的UDTO对象')
export class OnpForecastUDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiProperty({ name: 'foId', type: 'string', description: 'id' })
    foId: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foOrder', type: 'number', description: 'ID' })
    foOrder ? : number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foColumn1', type: 'string', description: 'Column1' })
    foColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foColumn2', type: 'string', description: 'Column2' })
    foColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foColumn3', type: 'string', description: 'Column3' })
    foColumn3 ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foColumn4', type: 'string', description: 'Column1' })
    foColumn4 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foColumn5', type: 'string', description: 'Column2' })
    foColumn5 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foColumn6', type: 'string', description: 'Column3' })
    foColumn6 ? : string;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foSpecies', type: 'string', description: 'Species' })
    foSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foRemark', type: 'string', description: 'Remark' })
    foRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foCreateDate', type: 'Date', description: '创建日期' })
    foCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpForecastUDto
     */
    @ApiPropertyOptional({ name: 'foUpdateDate', type: 'Date', description: '更新日期' })
    foUpdateDate ? : Date;
    /**
     * 更新列限定条件,在只想更新部分列数据时可以给定本参数
     *
     * @type { (keyof OnpForecastColNames)[] }
     * @memberof OnpForecastUDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpForecastColNames)[];
    /**
     * ID列表
     *
     * @type { string[] }
     * @memberof OnpForecastUDto
     */
    @ApiProperty({ name: 'foIdList', type: 'string[]', description: 'ID列表' })
    foIdList ? : string[];
}