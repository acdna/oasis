import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { OnpStatisticsColNames } from "../../entity/ddl/onp.statistics.cols";
/**
 * ONP统计数据表表的UDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpStatisticsUDto
 * @class OnpStatisticsUDto
 */
@ApiTags('ONP_STATISTICS表的UDTO对象')
export class OnpStatisticsUDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiProperty({ name: 'stId', type: 'string', description: 'id' })
    stId: string;
    /**
     * Bin
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stBin', type: 'string', description: 'Bin' })
    stBin ? : string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stChr', type: 'string', description: 'Chr' })
    stChr ? : string;
    /**
     * Start
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stStart', type: 'string', description: 'Start' })
    stStart ? : string;
    /**
     * Stop
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stStop', type: 'string', description: 'Stop' })
    stStop ? : string;
    /**
     * Bin
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stBinSize', type: 'string', description: 'Bin' })
    stBinSize ? : string;
    /**
     * Desc
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stDesc', type: 'string', description: 'Desc' })
    stDesc ? : string;
    /**
     * ALL
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stAllMarkers', type: 'string', description: 'ALL' })
    stAllMarkers ? : string;
    /**
     * SNP
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stSnpMarkers', type: 'string', description: 'SNP' })
    stSnpMarkers ? : string;
    /**
     * INDEL
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stIndelMarkers', type: 'string', description: 'INDEL' })
    stIndelMarkers ? : string;
    /**
     * Block
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stBlockMarkers', type: 'string', description: 'Block' })
    stBlockMarkers ? : string;
    /**
     * Tags
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stTags', type: 'string', description: 'Tags' })
    stTags ? : string;
    /**
     * Genotypes
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stGenotypes', type: 'string', description: 'Genotypes' })
    stGenotypes ? : string;
    /**
     * Max
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stMaxGenotypesFreq', type: 'string', description: 'Max' })
    stMaxGenotypesFreq ? : string;
    /**
     * Min
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stMinGenotypesFreq', type: 'string', description: 'Min' })
    stMinGenotypesFreq ? : string;
    /**
     * PIC
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stPic', type: 'string', description: 'PIC' })
    stPic ? : string;
    /**
     * Genetic
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stGeneticMap', type: 'string', description: 'Genetic' })
    stGeneticMap ? : string;
    /**
     * ORDER
     *
     * @type { number }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stOrder', type: 'number', description: 'ORDER' })
    stOrder ? : number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stColumn1', type: 'string', description: 'Column1' })
    stColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stColumn2', type: 'string', description: 'Column2' })
    stColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stColumn3', type: 'string', description: 'Column3' })
    stColumn3 ? : string;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stSpecies', type: 'string', description: 'Species' })
    stSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stRemark', type: 'string', description: 'Remark' })
    stRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stCreateDate', type: 'Date', description: '创建日期' })
    stCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpStatisticsUDto
     */
    @ApiPropertyOptional({ name: 'stUpdateDate', type: 'Date', description: '更新日期' })
    stUpdateDate ? : Date;
    /**
     * 更新列限定条件,在只想更新部分列数据时可以给定本参数
     *
     * @type { (keyof OnpStatisticsColNames)[] }
     * @memberof OnpStatisticsUDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpStatisticsColNames)[];
    /**
     * ID列表
     *
     * @type { string[] }
     * @memberof OnpStatisticsUDto
     */
    @ApiProperty({ name: 'stIdList', type: 'string[]', description: 'ID列表' })
    stIdList ? : string[];
}