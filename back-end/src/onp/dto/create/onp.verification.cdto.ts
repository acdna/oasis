import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 验证 CDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpVerificationCDto
 * @class OnpVerificationCDto
 */
@ApiTags('ONP_VERIFICATION表的CDTO对象')
export class OnpVerificationCDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiProperty({ name: 'veId', type: 'string', description: 'id' })
    veId: string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veOrder', type: 'number', description: 'ID' })
    veOrder ? : number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn1', type: 'string', description: 'Column1' })
    veColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn2', type: 'string', description: 'Column2' })
    veColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn3', type: 'string', description: 'Column3' })
    veColumn3 ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn4', type: 'string', description: 'Column1' })
    veColumn4 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn5', type: 'string', description: 'Column2' })
    veColumn5 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn6', type: 'string', description: 'Column3' })
    veColumn6 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn7', type: 'string', description: 'Column3' })
    veColumn7 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn8', type: 'string', description: 'Column3' })
    veColumn8 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veColumn9', type: 'string', description: 'Column3' })
    veColumn9 ? : string;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veSpecies', type: 'string', description: 'Species' })
    veSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veRemark', type: 'string', description: 'Remark' })
    veRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veCreateDate', type: 'Date', description: '创建日期' })
    veCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpVerificationCDto
     */
    @ApiPropertyOptional({ name: 'veUpdateDate', type: 'Date', description: '更新日期' })
    veUpdateDate ? : Date;
}