import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * ONP_CHLOROPLAST_HAPLOTYPE CDTO对象
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export OnpChloroplastHaplotypeCDto
 * @class OnpChloroplastHaplotypeCDto
 */
@ApiTags('ONP_CHLOROPLAST_HAPLOTYPE表的CDTO对象')
export class OnpChloroplastHaplotypeCDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiProperty({ name: 'ochId', type: 'string', description: 'id' })
    ochId: string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotype', type: 'string', description: 'Chloroplast' })
    ochChloroplastHaplotype ? : string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochChr', type: 'string', description: 'Chr' })
    ochChr ? : string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastOnp', type: 'string', description: 'Chloroplast' })
    ochChloroplastOnp ? : string;
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeSequence', type: 'string', description: 'Chloroplast' })
    ochChloroplastHaplotypeSequence ? : string;
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochFrequency', type: 'string', description: 'Frequency' })
    ochFrequency ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochColumn1', type: 'string', description: 'Column1' })
    ochColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochColumn2', type: 'string', description: 'Column2' })
    ochColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochColumn3', type: 'string', description: 'Column3' })
    ochColumn3 ? : string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochOrder', type: 'number', description: 'ID' })
    ochOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochSpecies', type: 'string', description: 'Species' })
    ochSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochRemark', type: 'string', description: 'Remark' })
    ochRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochCreateDate', type: 'Date', description: '创建日期' })
    ochCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'ochUpdateDate', type: 'Date', description: '更新日期' })
    ochUpdateDate ? : Date;
}