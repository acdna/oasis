import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * ONP_CHLOROPLAST_OVERALL CDTO对象
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export OnpChloroplastOverallCDto
 * @class OnpChloroplastOverallCDto
 */
@ApiTags('ONP_CHLOROPLAST_OVERALL表的CDTO对象')
export class OnpChloroplastOverallCDto {
    /**
     * ID
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiProperty({ name: 'ocoId', type: 'string', description: 'ID' })
    ocoId: string;
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiProperty({ name: 'ocoOnpId', type: 'string', description: 'ONP ID' })
    ocoOnpId: string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoChr', type: 'string', description: 'Chr' })
    ocoChr ? : string;
    /**
     * Start
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoStart', type: 'string', description: 'Start' })
    ocoStart ? : string;
    /**
     * Stop
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoStop', type: 'string', description: 'Stop' })
    ocoStop ? : string;
    /**
     * ONP
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoOnpSize', type: 'string', description: 'ONP' })
    ocoOnpSize ? : string;
    /**
     * Location
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoLocation', type: 'string', description: 'Location' })
    ocoLocation ? : string;
    /**
     * ALL
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoAllMarkers', type: 'string', description: 'ALL' })
    ocoAllMarkers ? : string;
    /**
     * SNP
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoSnpMarkers', type: 'string', description: 'SNP' })
    ocoSnpMarkers ? : string;
    /**
     * INDEL
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoIndelMarkers', type: 'string', description: 'INDEL' })
    ocoIndelMarkers ? : string;
    /**
     * Block
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoBlockMarkers', type: 'string', description: 'Block' })
    ocoBlockMarkers ? : string;
    /**
     * Tags
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoTags', type: 'string', description: 'Tags' })
    ocoTags ? : string;
    /**
     * Genotypes
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoGenotypes', type: 'string', description: 'Genotypes' })
    ocoGenotypes ? : string;
    /**
     * Max
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoMaxGenotypesFreq', type: 'string', description: 'Max' })
    ocoMaxGenotypesFreq ? : string;
    /**
     * Min
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoMinGenotypesFreq', type: 'string', description: 'Min' })
    ocoMinGenotypesFreq ? : string;
    /**
     * PIC
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoPic', type: 'string', description: 'PIC' })
    ocoPic ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoColumn1', type: 'string', description: 'Column1' })
    ocoColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoColumn2', type: 'string', description: 'Column2' })
    ocoColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoColumn3', type: 'string', description: 'Column3' })
    ocoColumn3 ? : string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoOrder', type: 'number', description: 'ID' })
    ocoOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoSpecies', type: 'string', description: 'Species' })
    ocoSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoRemark', type: 'string', description: 'Remark' })
    ocoRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoCreateDate', type: 'Date', description: '创建日期' })
    ocoCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastOverallCDto
     */
    @ApiPropertyOptional({ name: 'ocoUpdateDate', type: 'Date', description: '更新日期' })
    ocoUpdateDate ? : Date;
}