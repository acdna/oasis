import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';

/**
 * ONP统计数据表 CDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpStatisticsCDto
 * @class OnpStatisticsCDto
 */
@ApiTags('ONP_STATISTICS表的CDTO对象')
export class OnpStatisticsCDto {
  /**
   * id
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiProperty({ name: 'stId', type: 'string', description: 'id' })
  stId: string;
  /**
   * Bin
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stBin', type: 'string', description: 'Bin' })
  stBin?: string;
  /**
   * Chr
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stChr', type: 'string', description: 'Chr' })
  stChr?: string;
  /**
   * Start
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stStart', type: 'string', description: 'Start' })
  stStart?: string;
  /**
   * Stop
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stStop', type: 'string', description: 'Stop' })
  stStop?: string;
  /**
   * Bin
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stBinSize', type: 'string', description: 'Bin' })
  stBinSize?: string;
  /**
   * Desc
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stDesc', type: 'string', description: 'Desc' })
  stDesc?: string;
  /**
   * ALL
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stAllMarkers', type: 'string', description: 'ALL' })
  stAllMarkers?: string;
  /**
   * SNP
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stSnpMarkers', type: 'string', description: 'SNP' })
  stSnpMarkers?: string;
  /**
   * INDEL
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stIndelMarkers', type: 'string', description: 'INDEL' })
  stIndelMarkers?: string;
  /**
   * Block
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stBlockMarkers', type: 'string', description: 'Block' })
  stBlockMarkers?: string;
  /**
   * Tags
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stTags', type: 'string', description: 'Tags' })
  stTags?: string;
  /**
   * Genotypes
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stGenotypes', type: 'string', description: 'Genotypes' })
  stGenotypes?: string;
  /**
   * Max
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stMaxGenotypesFreq', type: 'string', description: 'Max' })
  stMaxGenotypesFreq?: string;
  /**
   * Min
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stMinGenotypesFreq', type: 'string', description: 'Min' })
  stMinGenotypesFreq?: string;
  /**
   * PIC
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stPic', type: 'string', description: 'PIC' })
  stPic?: string;
  /**
   * Genetic
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stGeneticMap', type: 'string', description: 'Genetic' })
  stGeneticMap?: string;
  /**
   * ORDER
   *
   * @type { number }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stOrder', type: 'number', description: 'ORDER' })
  stOrder?: number;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stColumn1', type: 'string', description: 'Column1' })
  stColumn1?: string;
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stColumn2', type: 'string', description: 'Column2' })
  stColumn2?: string;
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stColumn3', type: 'string', description: 'Column3' })
  stColumn3?: string;
  /**
   * Species
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stSpecies', type: 'string', description: 'Species' })
  stSpecies?: string;
  /**
   * Remark
   *
   * @type { string }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stRemark', type: 'string', description: 'Remark' })
  stRemark?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stCreateDate', type: 'Date', description: '创建日期' })
  stCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof OnpStatisticsCDto
   */
  @ApiPropertyOptional({ name: 'stUpdateDate', type: 'Date', description: '更新日期' })
  stUpdateDate?: Date;
  /**
   * @Description:文件路径
   * @author zhengenze
   * @date 2021/4/8 3:29 下午
   **/
  @ApiPropertyOptional({ name: 'stFilePath', type: 'string', description: '文件路径' })
  stFilePath?: string;
}
