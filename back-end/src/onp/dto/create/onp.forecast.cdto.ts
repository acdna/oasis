import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';

/**
 * 预测 CDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpForecastCDto
 * @class OnpForecastCDto
 */
@ApiTags('ONP_FORECAST表的CDTO对象')
export class OnpForecastCDto {
  /**
   * id
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiProperty({ name: 'foId', type: 'string', description: 'id' })
  foId: string;
  /**
   * ID
   *
   * @type { number }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foOrder', type: 'number', description: 'ID' })
  foOrder?: number;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foColumn1', type: 'string', description: 'Column1' })
  foColumn1?: string;
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foColumn2', type: 'string', description: 'Column2' })
  foColumn2?: string;
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foColumn3', type: 'string', description: 'Column3' })
  foColumn3?: string;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foColumn4', type: 'string', description: 'Column1' })
  foColumn4?: string;
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foColumn5', type: 'string', description: 'Column2' })
  foColumn5?: string;
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foColumn6', type: 'string', description: 'Column3' })
  foColumn6?: string;
  /**
   * Species
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foSpecies', type: 'string', description: 'Species' })
  foSpecies?: string;
  /**
   * Remark
   *
   * @type { string }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foRemark', type: 'string', description: 'Remark' })
  foRemark?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foCreateDate', type: 'Date', description: '创建日期' })
  foCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof OnpForecastCDto
   */
  @ApiPropertyOptional({ name: 'foUpdateDate', type: 'Date', description: '更新日期' })
  foUpdateDate?: Date;
  /**
   * @Description:上传文件路径
   * @author zhengenze
   * @date 2021/4/6 3:59 下午
   **/
  @ApiPropertyOptional({ name: 'foFilePath', type: 'string', description: '上传文件路径' })
  foFilePath?: string;
}
