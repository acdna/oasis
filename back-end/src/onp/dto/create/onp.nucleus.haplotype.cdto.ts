import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * ONP_NUCLEUS_HAPLOTYPE CDTO对象
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export OnpNucleusHaplotypeCDto
 * @class OnpNucleusHaplotypeCDto
 */
@ApiTags('ONP_NUCLEUS_HAPLOTYPE表的CDTO对象')
export class OnpNucleusHaplotypeCDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiProperty({ name: 'onhId', type: 'string', description: 'id' })
    onhId: string;
    /**
     * Haplotype Index
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeIndex', type: 'string', description: 'Haplotype Index' })
    onhHaplotypeIndex ? : string;
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhChr', type: 'string', description: 'Chr' })
    onhChr ? : string;
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhOnpId', type: 'string', description: 'ONP ID' })
    onhOnpId ? : string;
    /**
     * Haplotype Sequence
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeSequence', type: 'string', description: 'Haplotype Sequence' })
    onhHaplotypeSequence ? : string;
    /**
     * Haplotype Tag
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeTagSequence', type: 'string', description: 'Haplotype Tag' })
    onhHaplotypeTagSequence ? : string;
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhFrequency', type: 'string', description: 'Frequency' })
    onhFrequency ? : string;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhColumn1', type: 'string', description: 'Column1' })
    onhColumn1 ? : string;
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhColumn2', type: 'string', description: 'Column2' })
    onhColumn2 ? : string;
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhColumn3', type: 'string', description: 'Column3' })
    onhColumn3 ? : string;
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhOrder', type: 'number', description: 'ID' })
    onhOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhSpecies', type: 'string', description: 'Species' })
    onhSpecies ? : string;
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhRemark', type: 'string', description: 'Remark' })
    onhRemark ? : string;
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhCreateDate', type: 'Date', description: '创建日期' })
    onhCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotypeCDto
     */
    @ApiPropertyOptional({ name: 'onhUpdateDate', type: 'Date', description: '更新日期' })
    onhUpdateDate ? : Date;
}