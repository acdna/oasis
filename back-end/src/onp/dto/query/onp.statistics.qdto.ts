import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OnpStatisticsColNames, OnpStatisticsColProps } from '../../entity/ddl/onp.statistics.cols';
/**
 * ONP统计数据表表的QDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpStatisticsQDto
 * @class OnpStatisticsQDto
 */
@ApiTags('ONP_STATISTICS表的QDTO对象')
export class OnpStatisticsQDto {
  /**
   * id
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'stId', type: 'string', description: 'id' })
  stId?: string;
  @ApiProperty({ name: 'nstId', type: 'string', description: 'id' })
  nstId?: string;
  @ApiPropertyOptional({ name: 'stIdList', type: 'string[]', description: 'id-列表条件' })
  stIdList?: string[];
  @ApiPropertyOptional({ name: 'nstIdList', type: 'string[]', description: 'id-列表条件' })
  nstIdList?: string[];
  /**
   * Bin
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stBin', type: 'string', description: 'Bin' })
  stBin?: string;
  @ApiPropertyOptional({ name: 'nstBin', type: 'string', description: 'Bin' })
  nstBin?: string;
  @ApiPropertyOptional({ name: 'stBinLike', type: 'string', description: 'Bin-模糊条件' })
  stBinLike?: string;
  @ApiPropertyOptional({ name: 'stBinList', type: 'string[]', description: 'Bin-列表条件' })
  stBinList?: string[];
  @ApiPropertyOptional({ name: 'nstBinLike', type: 'string', description: 'Bin-模糊条件' })
  nstBinLike?: string;
  @ApiPropertyOptional({ name: 'nstBinList', type: 'string[]', description: 'Bin-列表条件' })
  nstBinList?: string[];
  @ApiPropertyOptional({ name: 'stBinLikeList', type: 'string[]', description: 'Bin-列表模糊条件' })
  stBinLikeList?: string[];
  /**
   * Chr
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stChr', type: 'string', description: 'Chr' })
  stChr?: string;
  @ApiPropertyOptional({ name: 'nstChr', type: 'string', description: 'Chr' })
  nstChr?: string;
  @ApiPropertyOptional({ name: 'stChrLike', type: 'string', description: 'Chr-模糊条件' })
  stChrLike?: string;
  @ApiPropertyOptional({ name: 'stChrList', type: 'string[]', description: 'Chr-列表条件' })
  stChrList?: string[];
  @ApiPropertyOptional({ name: 'nstChrLike', type: 'string', description: 'Chr-模糊条件' })
  nstChrLike?: string;
  @ApiPropertyOptional({ name: 'nstChrList', type: 'string[]', description: 'Chr-列表条件' })
  nstChrList?: string[];
  @ApiPropertyOptional({ name: 'stChrLikeList', type: 'string[]', description: 'Chr-列表模糊条件' })
  stChrLikeList?: string[];
  /**
   * Start
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stStart', type: 'string', description: 'Start' })
  stStart?: string;
  @ApiPropertyOptional({ name: 'nstStart', type: 'string', description: 'Start' })
  nstStart?: string;
  @ApiPropertyOptional({ name: 'stStartLike', type: 'string', description: 'Start-模糊条件' })
  stStartLike?: string;
  @ApiPropertyOptional({ name: 'stStartList', type: 'string[]', description: 'Start-列表条件' })
  stStartList?: string[];
  @ApiPropertyOptional({ name: 'nstStartLike', type: 'string', description: 'Start-模糊条件' })
  nstStartLike?: string;
  @ApiPropertyOptional({ name: 'nstStartList', type: 'string[]', description: 'Start-列表条件' })
  nstStartList?: string[];
  @ApiPropertyOptional({ name: 'stStartLikeList', type: 'string[]', description: 'Start-列表模糊条件' })
  stStartLikeList?: string[];
  /**
   * Stop
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stStop', type: 'string', description: 'Stop' })
  stStop?: string;
  @ApiPropertyOptional({ name: 'nstStop', type: 'string', description: 'Stop' })
  nstStop?: string;
  @ApiPropertyOptional({ name: 'stStopLike', type: 'string', description: 'Stop-模糊条件' })
  stStopLike?: string;
  @ApiPropertyOptional({ name: 'stStopList', type: 'string[]', description: 'Stop-列表条件' })
  stStopList?: string[];
  @ApiPropertyOptional({ name: 'nstStopLike', type: 'string', description: 'Stop-模糊条件' })
  nstStopLike?: string;
  @ApiPropertyOptional({ name: 'nstStopList', type: 'string[]', description: 'Stop-列表条件' })
  nstStopList?: string[];
  @ApiPropertyOptional({ name: 'stStopLikeList', type: 'string[]', description: 'Stop-列表模糊条件' })
  stStopLikeList?: string[];
  /**
   * Bin
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stBinSize', type: 'string', description: 'Bin' })
  stBinSize?: string;
  @ApiPropertyOptional({ name: 'nstBinSize', type: 'string', description: 'Bin' })
  nstBinSize?: string;
  @ApiPropertyOptional({ name: 'stBinSizeLike', type: 'string', description: 'Bin-模糊条件' })
  stBinSizeLike?: string;
  @ApiPropertyOptional({ name: 'stBinSizeList', type: 'string[]', description: 'Bin-列表条件' })
  stBinSizeList?: string[];
  @ApiPropertyOptional({ name: 'nstBinSizeLike', type: 'string', description: 'Bin-模糊条件' })
  nstBinSizeLike?: string;
  @ApiPropertyOptional({ name: 'nstBinSizeList', type: 'string[]', description: 'Bin-列表条件' })
  nstBinSizeList?: string[];
  @ApiPropertyOptional({ name: 'stBinSizeLikeList', type: 'string[]', description: 'Bin-列表模糊条件' })
  stBinSizeLikeList?: string[];
  /**
   * Desc
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stDesc', type: 'string', description: 'Desc' })
  stDesc?: string;
  @ApiPropertyOptional({ name: 'nstDesc', type: 'string', description: 'Desc' })
  nstDesc?: string;
  @ApiPropertyOptional({ name: 'stDescLike', type: 'string', description: 'Desc-模糊条件' })
  stDescLike?: string;
  @ApiPropertyOptional({ name: 'stDescList', type: 'string[]', description: 'Desc-列表条件' })
  stDescList?: string[];
  @ApiPropertyOptional({ name: 'nstDescLike', type: 'string', description: 'Desc-模糊条件' })
  nstDescLike?: string;
  @ApiPropertyOptional({ name: 'nstDescList', type: 'string[]', description: 'Desc-列表条件' })
  nstDescList?: string[];
  @ApiPropertyOptional({ name: 'stDescLikeList', type: 'string[]', description: 'Desc-列表模糊条件' })
  stDescLikeList?: string[];
  /**
   * ALL
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stAllMarkers', type: 'string', description: 'ALL' })
  stAllMarkers?: string;
  @ApiPropertyOptional({ name: 'nstAllMarkers', type: 'string', description: 'ALL' })
  nstAllMarkers?: string;
  @ApiPropertyOptional({ name: 'stAllMarkersLike', type: 'string', description: 'ALL-模糊条件' })
  stAllMarkersLike?: string;
  @ApiPropertyOptional({ name: 'stAllMarkersList', type: 'string[]', description: 'ALL-列表条件' })
  stAllMarkersList?: string[];
  @ApiPropertyOptional({ name: 'nstAllMarkersLike', type: 'string', description: 'ALL-模糊条件' })
  nstAllMarkersLike?: string;
  @ApiPropertyOptional({ name: 'nstAllMarkersList', type: 'string[]', description: 'ALL-列表条件' })
  nstAllMarkersList?: string[];
  @ApiPropertyOptional({ name: 'stAllMarkersLikeList', type: 'string[]', description: 'ALL-列表模糊条件' })
  stAllMarkersLikeList?: string[];
  /**
   * SNP
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stSnpMarkers', type: 'string', description: 'SNP' })
  stSnpMarkers?: string;
  @ApiPropertyOptional({ name: 'nstSnpMarkers', type: 'string', description: 'SNP' })
  nstSnpMarkers?: string;
  @ApiPropertyOptional({ name: 'stSnpMarkersLike', type: 'string', description: 'SNP-模糊条件' })
  stSnpMarkersLike?: string;
  @ApiPropertyOptional({ name: 'stSnpMarkersList', type: 'string[]', description: 'SNP-列表条件' })
  stSnpMarkersList?: string[];
  @ApiPropertyOptional({ name: 'nstSnpMarkersLike', type: 'string', description: 'SNP-模糊条件' })
  nstSnpMarkersLike?: string;
  @ApiPropertyOptional({ name: 'nstSnpMarkersList', type: 'string[]', description: 'SNP-列表条件' })
  nstSnpMarkersList?: string[];
  @ApiPropertyOptional({ name: 'stSnpMarkersLikeList', type: 'string[]', description: 'SNP-列表模糊条件' })
  stSnpMarkersLikeList?: string[];
  /**
   * INDEL
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stIndelMarkers', type: 'string', description: 'INDEL' })
  stIndelMarkers?: string;
  @ApiPropertyOptional({ name: 'nstIndelMarkers', type: 'string', description: 'INDEL' })
  nstIndelMarkers?: string;
  @ApiPropertyOptional({ name: 'stIndelMarkersLike', type: 'string', description: 'INDEL-模糊条件' })
  stIndelMarkersLike?: string;
  @ApiPropertyOptional({ name: 'stIndelMarkersList', type: 'string[]', description: 'INDEL-列表条件' })
  stIndelMarkersList?: string[];
  @ApiPropertyOptional({ name: 'nstIndelMarkersLike', type: 'string', description: 'INDEL-模糊条件' })
  nstIndelMarkersLike?: string;
  @ApiPropertyOptional({ name: 'nstIndelMarkersList', type: 'string[]', description: 'INDEL-列表条件' })
  nstIndelMarkersList?: string[];
  @ApiPropertyOptional({ name: 'stIndelMarkersLikeList', type: 'string[]', description: 'INDEL-列表模糊条件' })
  stIndelMarkersLikeList?: string[];
  /**
   * Block
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stBlockMarkers', type: 'string', description: 'Block' })
  stBlockMarkers?: string;
  @ApiPropertyOptional({ name: 'nstBlockMarkers', type: 'string', description: 'Block' })
  nstBlockMarkers?: string;
  @ApiPropertyOptional({ name: 'stBlockMarkersLike', type: 'string', description: 'Block-模糊条件' })
  stBlockMarkersLike?: string;
  @ApiPropertyOptional({ name: 'stBlockMarkersList', type: 'string[]', description: 'Block-列表条件' })
  stBlockMarkersList?: string[];
  @ApiPropertyOptional({ name: 'nstBlockMarkersLike', type: 'string', description: 'Block-模糊条件' })
  nstBlockMarkersLike?: string;
  @ApiPropertyOptional({ name: 'nstBlockMarkersList', type: 'string[]', description: 'Block-列表条件' })
  nstBlockMarkersList?: string[];
  @ApiPropertyOptional({ name: 'stBlockMarkersLikeList', type: 'string[]', description: 'Block-列表模糊条件' })
  stBlockMarkersLikeList?: string[];
  /**
   * Tags
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stTags', type: 'string', description: 'Tags' })
  stTags?: string;
  @ApiPropertyOptional({ name: 'nstTags', type: 'string', description: 'Tags' })
  nstTags?: string;
  @ApiPropertyOptional({ name: 'stTagsLike', type: 'string', description: 'Tags-模糊条件' })
  stTagsLike?: string;
  @ApiPropertyOptional({ name: 'stTagsList', type: 'string[]', description: 'Tags-列表条件' })
  stTagsList?: string[];
  @ApiPropertyOptional({ name: 'nstTagsLike', type: 'string', description: 'Tags-模糊条件' })
  nstTagsLike?: string;
  @ApiPropertyOptional({ name: 'nstTagsList', type: 'string[]', description: 'Tags-列表条件' })
  nstTagsList?: string[];
  @ApiPropertyOptional({ name: 'stTagsLikeList', type: 'string[]', description: 'Tags-列表模糊条件' })
  stTagsLikeList?: string[];
  /**
   * Genotypes
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stGenotypes', type: 'string', description: 'Genotypes' })
  stGenotypes?: string;
  @ApiPropertyOptional({ name: 'nstGenotypes', type: 'string', description: 'Genotypes' })
  nstGenotypes?: string;
  @ApiPropertyOptional({ name: 'stGenotypesLike', type: 'string', description: 'Genotypes-模糊条件' })
  stGenotypesLike?: string;
  @ApiPropertyOptional({ name: 'stGenotypesList', type: 'string[]', description: 'Genotypes-列表条件' })
  stGenotypesList?: string[];
  @ApiPropertyOptional({ name: 'nstGenotypesLike', type: 'string', description: 'Genotypes-模糊条件' })
  nstGenotypesLike?: string;
  @ApiPropertyOptional({ name: 'nstGenotypesList', type: 'string[]', description: 'Genotypes-列表条件' })
  nstGenotypesList?: string[];
  @ApiPropertyOptional({ name: 'stGenotypesLikeList', type: 'string[]', description: 'Genotypes-列表模糊条件' })
  stGenotypesLikeList?: string[];
  /**
   * Max
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stMaxGenotypesFreq', type: 'string', description: 'Max' })
  stMaxGenotypesFreq?: string;
  @ApiPropertyOptional({ name: 'nstMaxGenotypesFreq', type: 'string', description: 'Max' })
  nstMaxGenotypesFreq?: string;
  @ApiPropertyOptional({ name: 'stMaxGenotypesFreqLike', type: 'string', description: 'Max-模糊条件' })
  stMaxGenotypesFreqLike?: string;
  @ApiPropertyOptional({ name: 'stMaxGenotypesFreqList', type: 'string[]', description: 'Max-列表条件' })
  stMaxGenotypesFreqList?: string[];
  @ApiPropertyOptional({ name: 'nstMaxGenotypesFreqLike', type: 'string', description: 'Max-模糊条件' })
  nstMaxGenotypesFreqLike?: string;
  @ApiPropertyOptional({ name: 'nstMaxGenotypesFreqList', type: 'string[]', description: 'Max-列表条件' })
  nstMaxGenotypesFreqList?: string[];
  @ApiPropertyOptional({ name: 'stMaxGenotypesFreqLikeList', type: 'string[]', description: 'Max-列表模糊条件' })
  stMaxGenotypesFreqLikeList?: string[];
  /**
   * Min
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stMinGenotypesFreq', type: 'string', description: 'Min' })
  stMinGenotypesFreq?: string;
  @ApiPropertyOptional({ name: 'nstMinGenotypesFreq', type: 'string', description: 'Min' })
  nstMinGenotypesFreq?: string;
  @ApiPropertyOptional({ name: 'stMinGenotypesFreqLike', type: 'string', description: 'Min-模糊条件' })
  stMinGenotypesFreqLike?: string;
  @ApiPropertyOptional({ name: 'stMinGenotypesFreqList', type: 'string[]', description: 'Min-列表条件' })
  stMinGenotypesFreqList?: string[];
  @ApiPropertyOptional({ name: 'nstMinGenotypesFreqLike', type: 'string', description: 'Min-模糊条件' })
  nstMinGenotypesFreqLike?: string;
  @ApiPropertyOptional({ name: 'nstMinGenotypesFreqList', type: 'string[]', description: 'Min-列表条件' })
  nstMinGenotypesFreqList?: string[];
  @ApiPropertyOptional({ name: 'stMinGenotypesFreqLikeList', type: 'string[]', description: 'Min-列表模糊条件' })
  stMinGenotypesFreqLikeList?: string[];
  /**
   * PIC
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stPic', type: 'string', description: 'PIC' })
  stPic?: string;
  @ApiPropertyOptional({ name: 'nstPic', type: 'string', description: 'PIC' })
  nstPic?: string;
  @ApiPropertyOptional({ name: 'stPicLike', type: 'string', description: 'PIC-模糊条件' })
  stPicLike?: string;
  @ApiPropertyOptional({ name: 'stPicList', type: 'string[]', description: 'PIC-列表条件' })
  stPicList?: string[];
  @ApiPropertyOptional({ name: 'nstPicLike', type: 'string', description: 'PIC-模糊条件' })
  nstPicLike?: string;
  @ApiPropertyOptional({ name: 'nstPicList', type: 'string[]', description: 'PIC-列表条件' })
  nstPicList?: string[];
  @ApiPropertyOptional({ name: 'stPicLikeList', type: 'string[]', description: 'PIC-列表模糊条件' })
  stPicLikeList?: string[];
  /**
   * Genetic
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stGeneticMap', type: 'string', description: 'Genetic' })
  stGeneticMap?: string;
  @ApiPropertyOptional({ name: 'nstGeneticMap', type: 'string', description: 'Genetic' })
  nstGeneticMap?: string;
  @ApiPropertyOptional({ name: 'stGeneticMapLike', type: 'string', description: 'Genetic-模糊条件' })
  stGeneticMapLike?: string;
  @ApiPropertyOptional({ name: 'stGeneticMapList', type: 'string[]', description: 'Genetic-列表条件' })
  stGeneticMapList?: string[];
  @ApiPropertyOptional({ name: 'nstGeneticMapLike', type: 'string', description: 'Genetic-模糊条件' })
  nstGeneticMapLike?: string;
  @ApiPropertyOptional({ name: 'nstGeneticMapList', type: 'string[]', description: 'Genetic-列表条件' })
  nstGeneticMapList?: string[];
  @ApiPropertyOptional({ name: 'stGeneticMapLikeList', type: 'string[]', description: 'Genetic-列表模糊条件' })
  stGeneticMapLikeList?: string[];
  /**
   * ORDER
   *
   * @type { number }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stOrder', type: 'number', description: 'ORDER' })
  stOrder?: number;
  @ApiPropertyOptional({ name: 'nstOrder', type: 'number', description: 'ORDER' })
  nstOrder?: number;
  @ApiPropertyOptional({ name: 'minStOrder', type: 'number', description: 'ORDER-最小值' })
  minStOrder?: number;
  @ApiPropertyOptional({ name: 'maxStOrder', type: 'number', description: 'ORDER-最大值' })
  maxStOrder?: number;
  /**
   * Column1
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stColumn1', type: 'string', description: 'Column1' })
  stColumn1?: string;
  @ApiPropertyOptional({ name: 'nstColumn1', type: 'string', description: 'Column1' })
  nstColumn1?: string;
  @ApiPropertyOptional({ name: 'stColumn1Like', type: 'string', description: 'Column1-模糊条件' })
  stColumn1Like?: string;
  @ApiPropertyOptional({ name: 'stColumn1List', type: 'string[]', description: 'Column1-列表条件' })
  stColumn1List?: string[];
  @ApiPropertyOptional({ name: 'nstColumn1Like', type: 'string', description: 'Column1-模糊条件' })
  nstColumn1Like?: string;
  @ApiPropertyOptional({ name: 'nstColumn1List', type: 'string[]', description: 'Column1-列表条件' })
  nstColumn1List?: string[];
  @ApiPropertyOptional({ name: 'stColumn1LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
  stColumn1LikeList?: string[];
  /**
   * Column2
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stColumn2', type: 'string', description: 'Column2' })
  stColumn2?: string;
  @ApiPropertyOptional({ name: 'nstColumn2', type: 'string', description: 'Column2' })
  nstColumn2?: string;
  @ApiPropertyOptional({ name: 'stColumn2Like', type: 'string', description: 'Column2-模糊条件' })
  stColumn2Like?: string;
  @ApiPropertyOptional({ name: 'stColumn2List', type: 'string[]', description: 'Column2-列表条件' })
  stColumn2List?: string[];
  @ApiPropertyOptional({ name: 'nstColumn2Like', type: 'string', description: 'Column2-模糊条件' })
  nstColumn2Like?: string;
  @ApiPropertyOptional({ name: 'nstColumn2List', type: 'string[]', description: 'Column2-列表条件' })
  nstColumn2List?: string[];
  @ApiPropertyOptional({ name: 'stColumn2LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
  stColumn2LikeList?: string[];
  /**
   * Column3
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stColumn3', type: 'string', description: 'Column3' })
  stColumn3?: string;
  @ApiPropertyOptional({ name: 'nstColumn3', type: 'string', description: 'Column3' })
  nstColumn3?: string;
  @ApiPropertyOptional({ name: 'stColumn3Like', type: 'string', description: 'Column3-模糊条件' })
  stColumn3Like?: string;
  @ApiPropertyOptional({ name: 'stColumn3List', type: 'string[]', description: 'Column3-列表条件' })
  stColumn3List?: string[];
  @ApiPropertyOptional({ name: 'nstColumn3Like', type: 'string', description: 'Column3-模糊条件' })
  nstColumn3Like?: string;
  @ApiPropertyOptional({ name: 'nstColumn3List', type: 'string[]', description: 'Column3-列表条件' })
  nstColumn3List?: string[];
  @ApiPropertyOptional({ name: 'stColumn3LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
  stColumn3LikeList?: string[];
  /**
   * Species
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stSpecies', type: 'string', description: 'Species' })
  stSpecies?: string;
  @ApiPropertyOptional({ name: 'nstSpecies', type: 'string', description: 'Species' })
  nstSpecies?: string;
  @ApiPropertyOptional({ name: 'stSpeciesLike', type: 'string', description: 'Species-模糊条件' })
  stSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'stSpeciesList', type: 'string[]', description: 'Species-列表条件' })
  stSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'nstSpeciesLike', type: 'string', description: 'Species-模糊条件' })
  nstSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'nstSpeciesList', type: 'string[]', description: 'Species-列表条件' })
  nstSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'stSpeciesLikeList', type: 'string[]', description: 'Species-列表模糊条件' })
  stSpeciesLikeList?: string[];
  /**
   * Remark
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stRemark', type: 'string', description: 'Remark' })
  stRemark?: string;
  @ApiPropertyOptional({ name: 'nstRemark', type: 'string', description: 'Remark' })
  nstRemark?: string;
  @ApiPropertyOptional({ name: 'stRemarkLike', type: 'string', description: 'Remark-模糊条件' })
  stRemarkLike?: string;
  @ApiPropertyOptional({ name: 'stRemarkList', type: 'string[]', description: 'Remark-列表条件' })
  stRemarkList?: string[];
  @ApiPropertyOptional({ name: 'nstRemarkLike', type: 'string', description: 'Remark-模糊条件' })
  nstRemarkLike?: string;
  @ApiPropertyOptional({ name: 'nstRemarkList', type: 'string[]', description: 'Remark-列表条件' })
  nstRemarkList?: string[];
  @ApiPropertyOptional({ name: 'stRemarkLikeList', type: 'string[]', description: 'Remark-列表模糊条件' })
  stRemarkLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  stCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nstCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nstCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sStCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sStCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eStCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eStCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof OnpStatisticsQDto
   */
  @ApiPropertyOptional({ name: 'stUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  stUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nstUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nstUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sStUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sStUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eStUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eStUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof OnpStatisticsColNames)[]|(keyof OnpStatisticsColProps)[] }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof OnpStatisticsColNames)[] | (keyof OnpStatisticsColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof OnpStatisticsQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;

  /**
   * @Description: 文件路径
   * @author zhengenze
   * @date 2021/4/9 3:07 下午
   **/
  @ApiProperty({ name: 'stFilePath', type: 'string', description: '文件路径' })
  stFilePath?: string;
}
