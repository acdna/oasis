import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OnpChloroplastHaplotypeColNames, OnpChloroplastHaplotypeColProps } from "../../entity/ddl/onp.chloroplast.haplotype.cols";
/**
 * ONP_CHLOROPLAST_HAPLOTYPE表的QDTO对象
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export OnpChloroplastHaplotypeQDto
 * @class OnpChloroplastHaplotypeQDto
 */
@ApiTags('ONP_CHLOROPLAST_HAPLOTYPE表的QDTO对象')
export class OnpChloroplastHaplotypeQDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'ochId', type: 'string', description: 'id' })
    ochId ? : string;
    @ApiProperty({ name: 'nochId', type: 'string', description: 'id' })
    nochId ? : string;
    @ApiPropertyOptional({ name: 'ochIdList', type: 'string[]', description: 'id-列表条件' })
    ochIdList ? : string[];
    @ApiPropertyOptional({ name: 'nochIdList', type: 'string[]', description: 'id-列表条件' })
    nochIdList ? : string[];
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotype', type: 'string', description: 'Chloroplast' })
    ochChloroplastHaplotype ? : string;
    @ApiPropertyOptional({ name: 'nochChloroplastHaplotype', type: 'string', description: 'Chloroplast' })
    nochChloroplastHaplotype ? : string;
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeLike', type: 'string', description: 'Chloroplast-模糊条件' })
    ochChloroplastHaplotypeLike ? : string;
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeList', type: 'string[]', description: 'Chloroplast-列表条件' })
    ochChloroplastHaplotypeList ? : string[];
    @ApiPropertyOptional({ name: 'nochChloroplastHaplotypeLike', type: 'string', description: 'Chloroplast-模糊条件' })
    nochChloroplastHaplotypeLike ? : string;
    @ApiPropertyOptional({ name: 'nochChloroplastHaplotypeList', type: 'string[]', description: 'Chloroplast-列表条件' })
    nochChloroplastHaplotypeList ? : string[];
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeLikeList', type: 'string[]', description: 'Chloroplast-列表模糊条件' })
    ochChloroplastHaplotypeLikeList ? : string[];
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochChr', type: 'string', description: 'Chr' })
    ochChr ? : string;
    @ApiPropertyOptional({ name: 'nochChr', type: 'string', description: 'Chr' })
    nochChr ? : string;
    @ApiPropertyOptional({ name: 'ochChrLike', type: 'string', description: 'Chr-模糊条件' })
    ochChrLike ? : string;
    @ApiPropertyOptional({ name: 'ochChrList', type: 'string[]', description: 'Chr-列表条件' })
    ochChrList ? : string[];
    @ApiPropertyOptional({ name: 'nochChrLike', type: 'string', description: 'Chr-模糊条件' })
    nochChrLike ? : string;
    @ApiPropertyOptional({ name: 'nochChrList', type: 'string[]', description: 'Chr-列表条件' })
    nochChrList ? : string[];
    @ApiPropertyOptional({ name: 'ochChrLikeList', type: 'string[]', description: 'Chr-列表模糊条件' })
    ochChrLikeList ? : string[];
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastOnp', type: 'string', description: 'Chloroplast' })
    ochChloroplastOnp ? : string;
    @ApiPropertyOptional({ name: 'nochChloroplastOnp', type: 'string', description: 'Chloroplast' })
    nochChloroplastOnp ? : string;
    @ApiPropertyOptional({ name: 'ochChloroplastOnpLike', type: 'string', description: 'Chloroplast-模糊条件' })
    ochChloroplastOnpLike ? : string;
    @ApiPropertyOptional({ name: 'ochChloroplastOnpList', type: 'string[]', description: 'Chloroplast-列表条件' })
    ochChloroplastOnpList ? : string[];
    @ApiPropertyOptional({ name: 'nochChloroplastOnpLike', type: 'string', description: 'Chloroplast-模糊条件' })
    nochChloroplastOnpLike ? : string;
    @ApiPropertyOptional({ name: 'nochChloroplastOnpList', type: 'string[]', description: 'Chloroplast-列表条件' })
    nochChloroplastOnpList ? : string[];
    @ApiPropertyOptional({ name: 'ochChloroplastOnpLikeList', type: 'string[]', description: 'Chloroplast-列表模糊条件' })
    ochChloroplastOnpLikeList ? : string[];
    /**
     * Chloroplast
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeSequence', type: 'string', description: 'Chloroplast' })
    ochChloroplastHaplotypeSequence ? : string;
    @ApiPropertyOptional({ name: 'nochChloroplastHaplotypeSequence', type: 'string', description: 'Chloroplast' })
    nochChloroplastHaplotypeSequence ? : string;
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeSequenceLike', type: 'string', description: 'Chloroplast-模糊条件' })
    ochChloroplastHaplotypeSequenceLike ? : string;
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeSequenceList', type: 'string[]', description: 'Chloroplast-列表条件' })
    ochChloroplastHaplotypeSequenceList ? : string[];
    @ApiPropertyOptional({ name: 'nochChloroplastHaplotypeSequenceLike', type: 'string', description: 'Chloroplast-模糊条件' })
    nochChloroplastHaplotypeSequenceLike ? : string;
    @ApiPropertyOptional({ name: 'nochChloroplastHaplotypeSequenceList', type: 'string[]', description: 'Chloroplast-列表条件' })
    nochChloroplastHaplotypeSequenceList ? : string[];
    @ApiPropertyOptional({ name: 'ochChloroplastHaplotypeSequenceLikeList', type: 'string[]', description: 'Chloroplast-列表模糊条件' })
    ochChloroplastHaplotypeSequenceLikeList ? : string[];
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochFrequency', type: 'string', description: 'Frequency' })
    ochFrequency ? : string;
    @ApiPropertyOptional({ name: 'nochFrequency', type: 'string', description: 'Frequency' })
    nochFrequency ? : string;
    @ApiPropertyOptional({ name: 'ochFrequencyLike', type: 'string', description: 'Frequency-模糊条件' })
    ochFrequencyLike ? : string;
    @ApiPropertyOptional({ name: 'ochFrequencyList', type: 'string[]', description: 'Frequency-列表条件' })
    ochFrequencyList ? : string[];
    @ApiPropertyOptional({ name: 'nochFrequencyLike', type: 'string', description: 'Frequency-模糊条件' })
    nochFrequencyLike ? : string;
    @ApiPropertyOptional({ name: 'nochFrequencyList', type: 'string[]', description: 'Frequency-列表条件' })
    nochFrequencyList ? : string[];
    @ApiPropertyOptional({ name: 'ochFrequencyLikeList', type: 'string[]', description: 'Frequency-列表模糊条件' })
    ochFrequencyLikeList ? : string[];
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochColumn1', type: 'string', description: 'Column1' })
    ochColumn1 ? : string;
    @ApiPropertyOptional({ name: 'nochColumn1', type: 'string', description: 'Column1' })
    nochColumn1 ? : string;
    @ApiPropertyOptional({ name: 'ochColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    ochColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'ochColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    ochColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'nochColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    nochColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'nochColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    nochColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'ochColumn1LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    ochColumn1LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochColumn2', type: 'string', description: 'Column2' })
    ochColumn2 ? : string;
    @ApiPropertyOptional({ name: 'nochColumn2', type: 'string', description: 'Column2' })
    nochColumn2 ? : string;
    @ApiPropertyOptional({ name: 'ochColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    ochColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'ochColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    ochColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'nochColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    nochColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'nochColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    nochColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'ochColumn2LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    ochColumn2LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochColumn3', type: 'string', description: 'Column3' })
    ochColumn3 ? : string;
    @ApiPropertyOptional({ name: 'nochColumn3', type: 'string', description: 'Column3' })
    nochColumn3 ? : string;
    @ApiPropertyOptional({ name: 'ochColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    ochColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'ochColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    ochColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'nochColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    nochColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'nochColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    nochColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'ochColumn3LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    ochColumn3LikeList ? : string[];
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochOrder', type: 'number', description: 'ID' })
    ochOrder ? : number;
    @ApiPropertyOptional({ name: 'nochOrder', type: 'number', description: 'ID' })
    nochOrder ? : number;
    @ApiPropertyOptional({ name: 'minOchOrder', type: 'number', description: 'ID-最小值' })
    minOchOrder ? : number;
    @ApiPropertyOptional({ name: 'maxOchOrder', type: 'number', description: 'ID-最大值' })
    maxOchOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochSpecies', type: 'string', description: 'Species' })
    ochSpecies ? : string;
    @ApiPropertyOptional({ name: 'nochSpecies', type: 'string', description: 'Species' })
    nochSpecies ? : string;
    @ApiPropertyOptional({ name: 'ochSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    ochSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'ochSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    ochSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'nochSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    nochSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'nochSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    nochSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'ochSpeciesLikeList', type: 'string[]', description: 'Species-列表模糊条件' })
    ochSpeciesLikeList ? : string[];
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochRemark', type: 'string', description: 'Remark' })
    ochRemark ? : string;
    @ApiPropertyOptional({ name: 'nochRemark', type: 'string', description: 'Remark' })
    nochRemark ? : string;
    @ApiPropertyOptional({ name: 'ochRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    ochRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'ochRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    ochRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'nochRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    nochRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'nochRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    nochRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'ochRemarkLikeList', type: 'string[]', description: 'Remark-列表模糊条件' })
    ochRemarkLikeList ? : string[];
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    ochCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'nochCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    nochCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'sOchCreateDate', type: 'Date', description: '创建日期-起始日期' })
    @Type(() => Date)
    sOchCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'eOchCreateDate', type: 'Date', description: '创建日期-终止日期' })
    @Type(() => Date)
    eOchCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'ochUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    ochUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'nochUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    nochUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'sOchUpdateDate', type: 'Date', description: '更新日期-起始日期' })
    @Type(() => Date)
    sOchUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'eOchUpdateDate', type: 'Date', description: '更新日期-终止日期' })
    @Type(() => Date)
    eOchUpdateDate ? : Date;
    /**
     * 分页查询记录数
     *
     * @type { number }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
    pageSize ? : number;
    /**
     * 记录偏移量
     *
     * @type { number }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
    offset ? : number;
    /**
     * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'order', type: 'string', description: '排序' })
    order ? : string;
    /**
     * 分组查询条件,例如：SAM_KIND
     *
     * @type { string }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'group', type: 'string', description: '排序' })
    group ? : string;
    /**
     * 结果列限定条件,在只想查询部分列数据时可以给定本参数
     *
     * @type { (keyof OnpChloroplastHaplotypeColNames)[]|(keyof OnpChloroplastHaplotypeColProps)[] }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpChloroplastHaplotypeColNames)[] | (keyof OnpChloroplastHaplotypeColProps)[];
    /**
     * 对{cols}给定的列数据进行去重
     *
     * @type { boolean }
     * @memberof OnpChloroplastHaplotypeQDto
     */
    @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
    distinct ? : boolean;
}