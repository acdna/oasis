import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OnpVerificationColNames, OnpVerificationColProps } from "../../entity/ddl/onp.verification.cols";
/**
 * 验证表的QDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpVerificationQDto
 * @class OnpVerificationQDto
 */
@ApiTags('ONP_VERIFICATION表的QDTO对象')
export class OnpVerificationQDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'veId', type: 'string', description: 'id' })
    veId ? : string;
    @ApiProperty({ name: 'nveId', type: 'string', description: 'id' })
    nveId ? : string;
    @ApiPropertyOptional({ name: 'veIdList', type: 'string[]', description: 'id-列表条件' })
    veIdList ? : string[];
    @ApiPropertyOptional({ name: 'nveIdList', type: 'string[]', description: 'id-列表条件' })
    nveIdList ? : string[];
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veOrder', type: 'number', description: 'ID' })
    veOrder ? : number;
    @ApiPropertyOptional({ name: 'nveOrder', type: 'number', description: 'ID' })
    nveOrder ? : number;
    @ApiPropertyOptional({ name: 'minVeOrder', type: 'number', description: 'ID-最小值' })
    minVeOrder ? : number;
    @ApiPropertyOptional({ name: 'maxVeOrder', type: 'number', description: 'ID-最大值' })
    maxVeOrder ? : number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn1', type: 'string', description: 'Column1' })
    veColumn1 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn1', type: 'string', description: 'Column1' })
    nveColumn1 ? : string;
    @ApiPropertyOptional({ name: 'veColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    veColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    veColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    nveColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    nveColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn1LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    veColumn1LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn2', type: 'string', description: 'Column2' })
    veColumn2 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn2', type: 'string', description: 'Column2' })
    nveColumn2 ? : string;
    @ApiPropertyOptional({ name: 'veColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    veColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    veColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    nveColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    nveColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn2LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    veColumn2LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn3', type: 'string', description: 'Column3' })
    veColumn3 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn3', type: 'string', description: 'Column3' })
    nveColumn3 ? : string;
    @ApiPropertyOptional({ name: 'veColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    veColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    veColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    nveColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    nveColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn3LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    veColumn3LikeList ? : string[];
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn4', type: 'string', description: 'Column1' })
    veColumn4 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn4', type: 'string', description: 'Column1' })
    nveColumn4 ? : string;
    @ApiPropertyOptional({ name: 'veColumn4Like', type: 'string', description: 'Column1-模糊条件' })
    veColumn4Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn4List', type: 'string[]', description: 'Column1-列表条件' })
    veColumn4List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn4Like', type: 'string', description: 'Column1-模糊条件' })
    nveColumn4Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn4List', type: 'string[]', description: 'Column1-列表条件' })
    nveColumn4List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn4LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    veColumn4LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn5', type: 'string', description: 'Column2' })
    veColumn5 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn5', type: 'string', description: 'Column2' })
    nveColumn5 ? : string;
    @ApiPropertyOptional({ name: 'veColumn5Like', type: 'string', description: 'Column2-模糊条件' })
    veColumn5Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn5List', type: 'string[]', description: 'Column2-列表条件' })
    veColumn5List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn5Like', type: 'string', description: 'Column2-模糊条件' })
    nveColumn5Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn5List', type: 'string[]', description: 'Column2-列表条件' })
    nveColumn5List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn5LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    veColumn5LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn6', type: 'string', description: 'Column3' })
    veColumn6 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn6', type: 'string', description: 'Column3' })
    nveColumn6 ? : string;
    @ApiPropertyOptional({ name: 'veColumn6Like', type: 'string', description: 'Column3-模糊条件' })
    veColumn6Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn6List', type: 'string[]', description: 'Column3-列表条件' })
    veColumn6List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn6Like', type: 'string', description: 'Column3-模糊条件' })
    nveColumn6Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn6List', type: 'string[]', description: 'Column3-列表条件' })
    nveColumn6List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn6LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    veColumn6LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn7', type: 'string', description: 'Column3' })
    veColumn7 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn7', type: 'string', description: 'Column3' })
    nveColumn7 ? : string;
    @ApiPropertyOptional({ name: 'veColumn7Like', type: 'string', description: 'Column3-模糊条件' })
    veColumn7Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn7List', type: 'string[]', description: 'Column3-列表条件' })
    veColumn7List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn7Like', type: 'string', description: 'Column3-模糊条件' })
    nveColumn7Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn7List', type: 'string[]', description: 'Column3-列表条件' })
    nveColumn7List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn7LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    veColumn7LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn8', type: 'string', description: 'Column3' })
    veColumn8 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn8', type: 'string', description: 'Column3' })
    nveColumn8 ? : string;
    @ApiPropertyOptional({ name: 'veColumn8Like', type: 'string', description: 'Column3-模糊条件' })
    veColumn8Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn8List', type: 'string[]', description: 'Column3-列表条件' })
    veColumn8List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn8Like', type: 'string', description: 'Column3-模糊条件' })
    nveColumn8Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn8List', type: 'string[]', description: 'Column3-列表条件' })
    nveColumn8List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn8LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    veColumn8LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veColumn9', type: 'string', description: 'Column3' })
    veColumn9 ? : string;
    @ApiPropertyOptional({ name: 'nveColumn9', type: 'string', description: 'Column3' })
    nveColumn9 ? : string;
    @ApiPropertyOptional({ name: 'veColumn9Like', type: 'string', description: 'Column3-模糊条件' })
    veColumn9Like ? : string;
    @ApiPropertyOptional({ name: 'veColumn9List', type: 'string[]', description: 'Column3-列表条件' })
    veColumn9List ? : string[];
    @ApiPropertyOptional({ name: 'nveColumn9Like', type: 'string', description: 'Column3-模糊条件' })
    nveColumn9Like ? : string;
    @ApiPropertyOptional({ name: 'nveColumn9List', type: 'string[]', description: 'Column3-列表条件' })
    nveColumn9List ? : string[];
    @ApiPropertyOptional({ name: 'veColumn9LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    veColumn9LikeList ? : string[];
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veSpecies', type: 'string', description: 'Species' })
    veSpecies ? : string;
    @ApiPropertyOptional({ name: 'nveSpecies', type: 'string', description: 'Species' })
    nveSpecies ? : string;
    @ApiPropertyOptional({ name: 'veSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    veSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'veSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    veSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'nveSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    nveSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'nveSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    nveSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'veSpeciesLikeList', type: 'string[]', description: 'Species-列表模糊条件' })
    veSpeciesLikeList ? : string[];
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veRemark', type: 'string', description: 'Remark' })
    veRemark ? : string;
    @ApiPropertyOptional({ name: 'nveRemark', type: 'string', description: 'Remark' })
    nveRemark ? : string;
    @ApiPropertyOptional({ name: 'veRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    veRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'veRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    veRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'nveRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    nveRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'nveRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    nveRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'veRemarkLikeList', type: 'string[]', description: 'Remark-列表模糊条件' })
    veRemarkLikeList ? : string[];
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    veCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'nveCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    nveCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'sVeCreateDate', type: 'Date', description: '创建日期-起始日期' })
    @Type(() => Date)
    sVeCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'eVeCreateDate', type: 'Date', description: '创建日期-终止日期' })
    @Type(() => Date)
    eVeCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpVerificationQDto
     */
    @ApiPropertyOptional({ name: 'veUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    veUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'nveUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    nveUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'sVeUpdateDate', type: 'Date', description: '更新日期-起始日期' })
    @Type(() => Date)
    sVeUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'eVeUpdateDate', type: 'Date', description: '更新日期-终止日期' })
    @Type(() => Date)
    eVeUpdateDate ? : Date;
    /**
     * 分页查询记录数
     *
     * @type { number }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
    pageSize ? : number;
    /**
     * 记录偏移量
     *
     * @type { number }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
    offset ? : number;
    /**
     * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'order', type: 'string', description: '排序' })
    order ? : string;
    /**
     * 分组查询条件,例如：SAM_KIND
     *
     * @type { string }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'group', type: 'string', description: '排序' })
    group ? : string;
    /**
     * 结果列限定条件,在只想查询部分列数据时可以给定本参数
     *
     * @type { (keyof OnpVerificationColNames)[]|(keyof OnpVerificationColProps)[] }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpVerificationColNames)[] | (keyof OnpVerificationColProps)[];
    /**
     * 对{cols}给定的列数据进行去重
     *
     * @type { boolean }
     * @memberof OnpVerificationQDto
     */
    @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
    distinct ? : boolean;
}