import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OnpForecastColNames, OnpForecastColProps } from "../../entity/ddl/onp.forecast.cols";
/**
 * 预测表的QDTO对象
 * @date 4/6/2021, 10:21:58 AM
 * @author jiangbin
 * @export OnpForecastQDto
 * @class OnpForecastQDto
 */
@ApiTags('ONP_FORECAST表的QDTO对象')
export class OnpForecastQDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'foId', type: 'string', description: 'id' })
    foId ? : string;
    @ApiProperty({ name: 'nfoId', type: 'string', description: 'id' })
    nfoId ? : string;
    @ApiPropertyOptional({ name: 'foIdList', type: 'string[]', description: 'id-列表条件' })
    foIdList ? : string[];
    @ApiPropertyOptional({ name: 'nfoIdList', type: 'string[]', description: 'id-列表条件' })
    nfoIdList ? : string[];
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foOrder', type: 'number', description: 'ID' })
    foOrder ? : number;
    @ApiPropertyOptional({ name: 'nfoOrder', type: 'number', description: 'ID' })
    nfoOrder ? : number;
    @ApiPropertyOptional({ name: 'minFoOrder', type: 'number', description: 'ID-最小值' })
    minFoOrder ? : number;
    @ApiPropertyOptional({ name: 'maxFoOrder', type: 'number', description: 'ID-最大值' })
    maxFoOrder ? : number;
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foColumn1', type: 'string', description: 'Column1' })
    foColumn1 ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn1', type: 'string', description: 'Column1' })
    nfoColumn1 ? : string;
    @ApiPropertyOptional({ name: 'foColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    foColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'foColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    foColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'nfoColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    nfoColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    nfoColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'foColumn1LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    foColumn1LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foColumn2', type: 'string', description: 'Column2' })
    foColumn2 ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn2', type: 'string', description: 'Column2' })
    nfoColumn2 ? : string;
    @ApiPropertyOptional({ name: 'foColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    foColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'foColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    foColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'nfoColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    nfoColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    nfoColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'foColumn2LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    foColumn2LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foColumn3', type: 'string', description: 'Column3' })
    foColumn3 ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn3', type: 'string', description: 'Column3' })
    nfoColumn3 ? : string;
    @ApiPropertyOptional({ name: 'foColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    foColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'foColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    foColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'nfoColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    nfoColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    nfoColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'foColumn3LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    foColumn3LikeList ? : string[];
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foColumn4', type: 'string', description: 'Column1' })
    foColumn4 ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn4', type: 'string', description: 'Column1' })
    nfoColumn4 ? : string;
    @ApiPropertyOptional({ name: 'foColumn4Like', type: 'string', description: 'Column1-模糊条件' })
    foColumn4Like ? : string;
    @ApiPropertyOptional({ name: 'foColumn4List', type: 'string[]', description: 'Column1-列表条件' })
    foColumn4List ? : string[];
    @ApiPropertyOptional({ name: 'nfoColumn4Like', type: 'string', description: 'Column1-模糊条件' })
    nfoColumn4Like ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn4List', type: 'string[]', description: 'Column1-列表条件' })
    nfoColumn4List ? : string[];
    @ApiPropertyOptional({ name: 'foColumn4LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    foColumn4LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foColumn5', type: 'string', description: 'Column2' })
    foColumn5 ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn5', type: 'string', description: 'Column2' })
    nfoColumn5 ? : string;
    @ApiPropertyOptional({ name: 'foColumn5Like', type: 'string', description: 'Column2-模糊条件' })
    foColumn5Like ? : string;
    @ApiPropertyOptional({ name: 'foColumn5List', type: 'string[]', description: 'Column2-列表条件' })
    foColumn5List ? : string[];
    @ApiPropertyOptional({ name: 'nfoColumn5Like', type: 'string', description: 'Column2-模糊条件' })
    nfoColumn5Like ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn5List', type: 'string[]', description: 'Column2-列表条件' })
    nfoColumn5List ? : string[];
    @ApiPropertyOptional({ name: 'foColumn5LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    foColumn5LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foColumn6', type: 'string', description: 'Column3' })
    foColumn6 ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn6', type: 'string', description: 'Column3' })
    nfoColumn6 ? : string;
    @ApiPropertyOptional({ name: 'foColumn6Like', type: 'string', description: 'Column3-模糊条件' })
    foColumn6Like ? : string;
    @ApiPropertyOptional({ name: 'foColumn6List', type: 'string[]', description: 'Column3-列表条件' })
    foColumn6List ? : string[];
    @ApiPropertyOptional({ name: 'nfoColumn6Like', type: 'string', description: 'Column3-模糊条件' })
    nfoColumn6Like ? : string;
    @ApiPropertyOptional({ name: 'nfoColumn6List', type: 'string[]', description: 'Column3-列表条件' })
    nfoColumn6List ? : string[];
    @ApiPropertyOptional({ name: 'foColumn6LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    foColumn6LikeList ? : string[];
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foSpecies', type: 'string', description: 'Species' })
    foSpecies ? : string;
    @ApiPropertyOptional({ name: 'nfoSpecies', type: 'string', description: 'Species' })
    nfoSpecies ? : string;
    @ApiPropertyOptional({ name: 'foSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    foSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'foSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    foSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'nfoSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    nfoSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'nfoSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    nfoSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'foSpeciesLikeList', type: 'string[]', description: 'Species-列表模糊条件' })
    foSpeciesLikeList ? : string[];
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foRemark', type: 'string', description: 'Remark' })
    foRemark ? : string;
    @ApiPropertyOptional({ name: 'nfoRemark', type: 'string', description: 'Remark' })
    nfoRemark ? : string;
    @ApiPropertyOptional({ name: 'foRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    foRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'foRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    foRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'nfoRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    nfoRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'nfoRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    nfoRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'foRemarkLikeList', type: 'string[]', description: 'Remark-列表模糊条件' })
    foRemarkLikeList ? : string[];
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    foCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'nfoCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    nfoCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'sFoCreateDate', type: 'Date', description: '创建日期-起始日期' })
    @Type(() => Date)
    sFoCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'eFoCreateDate', type: 'Date', description: '创建日期-终止日期' })
    @Type(() => Date)
    eFoCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpForecastQDto
     */
    @ApiPropertyOptional({ name: 'foUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    foUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'nfoUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    nfoUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'sFoUpdateDate', type: 'Date', description: '更新日期-起始日期' })
    @Type(() => Date)
    sFoUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'eFoUpdateDate', type: 'Date', description: '更新日期-终止日期' })
    @Type(() => Date)
    eFoUpdateDate ? : Date;
    /**
     * 分页查询记录数
     *
     * @type { number }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
    pageSize ? : number;
    /**
     * 记录偏移量
     *
     * @type { number }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
    offset ? : number;
    /**
     * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'order', type: 'string', description: '排序' })
    order ? : string;
    /**
     * 分组查询条件,例如：SAM_KIND
     *
     * @type { string }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'group', type: 'string', description: '排序' })
    group ? : string;
    /**
     * 结果列限定条件,在只想查询部分列数据时可以给定本参数
     *
     * @type { (keyof OnpForecastColNames)[]|(keyof OnpForecastColProps)[] }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpForecastColNames)[] | (keyof OnpForecastColProps)[];
    /**
     * 对{cols}给定的列数据进行去重
     *
     * @type { boolean }
     * @memberof OnpForecastQDto
     */
    @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
    distinct ? : boolean;
}