import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OnpNucleusHaplotypeColNames, OnpNucleusHaplotypeColProps } from "../../entity/ddl/onp.nucleus.haplotype.cols";
/**
 * ONP_NUCLEUS_HAPLOTYPE表的QDTO对象
 * @date 4/29/2021, 1:57:38 PM
 * @author jiangbin
 * @export OnpNucleusHaplotypeQDto
 * @class OnpNucleusHaplotypeQDto
 */
@ApiTags('ONP_NUCLEUS_HAPLOTYPE表的QDTO对象')
export class OnpNucleusHaplotypeQDto {
    /**
     * id
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'onhId', type: 'string', description: 'id' })
    onhId ? : string;
    @ApiProperty({ name: 'nonhId', type: 'string', description: 'id' })
    nonhId ? : string;
    @ApiPropertyOptional({ name: 'onhIdList', type: 'string[]', description: 'id-列表条件' })
    onhIdList ? : string[];
    @ApiPropertyOptional({ name: 'nonhIdList', type: 'string[]', description: 'id-列表条件' })
    nonhIdList ? : string[];
    /**
     * Haplotype Index
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeIndex', type: 'string', description: 'Haplotype Index' })
    onhHaplotypeIndex ? : string;
    @ApiPropertyOptional({ name: 'nonhHaplotypeIndex', type: 'string', description: 'Haplotype Index' })
    nonhHaplotypeIndex ? : string;
    @ApiPropertyOptional({ name: 'onhHaplotypeIndexLike', type: 'string', description: 'Haplotype Index-模糊条件' })
    onhHaplotypeIndexLike ? : string;
    @ApiPropertyOptional({ name: 'onhHaplotypeIndexList', type: 'string[]', description: 'Haplotype Index-列表条件' })
    onhHaplotypeIndexList ? : string[];
    @ApiPropertyOptional({ name: 'nonhHaplotypeIndexLike', type: 'string', description: 'Haplotype Index-模糊条件' })
    nonhHaplotypeIndexLike ? : string;
    @ApiPropertyOptional({ name: 'nonhHaplotypeIndexList', type: 'string[]', description: 'Haplotype Index-列表条件' })
    nonhHaplotypeIndexList ? : string[];
    @ApiPropertyOptional({ name: 'onhHaplotypeIndexLikeList', type: 'string[]', description: 'Haplotype Index-列表模糊条件' })
    onhHaplotypeIndexLikeList ? : string[];
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhChr', type: 'string', description: 'Chr' })
    onhChr ? : string;
    @ApiPropertyOptional({ name: 'nonhChr', type: 'string', description: 'Chr' })
    nonhChr ? : string;
    @ApiPropertyOptional({ name: 'onhChrLike', type: 'string', description: 'Chr-模糊条件' })
    onhChrLike ? : string;
    @ApiPropertyOptional({ name: 'onhChrList', type: 'string[]', description: 'Chr-列表条件' })
    onhChrList ? : string[];
    @ApiPropertyOptional({ name: 'nonhChrLike', type: 'string', description: 'Chr-模糊条件' })
    nonhChrLike ? : string;
    @ApiPropertyOptional({ name: 'nonhChrList', type: 'string[]', description: 'Chr-列表条件' })
    nonhChrList ? : string[];
    @ApiPropertyOptional({ name: 'onhChrLikeList', type: 'string[]', description: 'Chr-列表模糊条件' })
    onhChrLikeList ? : string[];
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhOnpId', type: 'string', description: 'ONP ID' })
    onhOnpId ? : string;
    @ApiPropertyOptional({ name: 'nonhOnpId', type: 'string', description: 'ONP ID' })
    nonhOnpId ? : string;
    @ApiPropertyOptional({ name: 'onhOnpIdLike', type: 'string', description: 'ONP ID-模糊条件' })
    onhOnpIdLike ? : string;
    @ApiPropertyOptional({ name: 'onhOnpIdList', type: 'string[]', description: 'ONP ID-列表条件' })
    onhOnpIdList ? : string[];
    @ApiPropertyOptional({ name: 'nonhOnpIdLike', type: 'string', description: 'ONP ID-模糊条件' })
    nonhOnpIdLike ? : string;
    @ApiPropertyOptional({ name: 'nonhOnpIdList', type: 'string[]', description: 'ONP ID-列表条件' })
    nonhOnpIdList ? : string[];
    @ApiPropertyOptional({ name: 'onhOnpIdLikeList', type: 'string[]', description: 'ONP ID-列表模糊条件' })
    onhOnpIdLikeList ? : string[];
    /**
     * Haplotype Sequence
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeSequence', type: 'string', description: 'Haplotype Sequence' })
    onhHaplotypeSequence ? : string;
    @ApiPropertyOptional({ name: 'nonhHaplotypeSequence', type: 'string', description: 'Haplotype Sequence' })
    nonhHaplotypeSequence ? : string;
    @ApiPropertyOptional({ name: 'onhHaplotypeSequenceLike', type: 'string', description: 'Haplotype Sequence-模糊条件' })
    onhHaplotypeSequenceLike ? : string;
    @ApiPropertyOptional({ name: 'onhHaplotypeSequenceList', type: 'string[]', description: 'Haplotype Sequence-列表条件' })
    onhHaplotypeSequenceList ? : string[];
    @ApiPropertyOptional({ name: 'nonhHaplotypeSequenceLike', type: 'string', description: 'Haplotype Sequence-模糊条件' })
    nonhHaplotypeSequenceLike ? : string;
    @ApiPropertyOptional({ name: 'nonhHaplotypeSequenceList', type: 'string[]', description: 'Haplotype Sequence-列表条件' })
    nonhHaplotypeSequenceList ? : string[];
    @ApiPropertyOptional({ name: 'onhHaplotypeSequenceLikeList', type: 'string[]', description: 'Haplotype Sequence-列表模糊条件' })
    onhHaplotypeSequenceLikeList ? : string[];
    /**
     * Haplotype Tag
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhHaplotypeTagSequence', type: 'string', description: 'Haplotype Tag' })
    onhHaplotypeTagSequence ? : string;
    @ApiPropertyOptional({ name: 'nonhHaplotypeTagSequence', type: 'string', description: 'Haplotype Tag' })
    nonhHaplotypeTagSequence ? : string;
    @ApiPropertyOptional({ name: 'onhHaplotypeTagSequenceLike', type: 'string', description: 'Haplotype Tag-模糊条件' })
    onhHaplotypeTagSequenceLike ? : string;
    @ApiPropertyOptional({ name: 'onhHaplotypeTagSequenceList', type: 'string[]', description: 'Haplotype Tag-列表条件' })
    onhHaplotypeTagSequenceList ? : string[];
    @ApiPropertyOptional({ name: 'nonhHaplotypeTagSequenceLike', type: 'string', description: 'Haplotype Tag-模糊条件' })
    nonhHaplotypeTagSequenceLike ? : string;
    @ApiPropertyOptional({ name: 'nonhHaplotypeTagSequenceList', type: 'string[]', description: 'Haplotype Tag-列表条件' })
    nonhHaplotypeTagSequenceList ? : string[];
    @ApiPropertyOptional({ name: 'onhHaplotypeTagSequenceLikeList', type: 'string[]', description: 'Haplotype Tag-列表模糊条件' })
    onhHaplotypeTagSequenceLikeList ? : string[];
    /**
     * Frequency
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhFrequency', type: 'string', description: 'Frequency' })
    onhFrequency ? : string;
    @ApiPropertyOptional({ name: 'nonhFrequency', type: 'string', description: 'Frequency' })
    nonhFrequency ? : string;
    @ApiPropertyOptional({ name: 'onhFrequencyLike', type: 'string', description: 'Frequency-模糊条件' })
    onhFrequencyLike ? : string;
    @ApiPropertyOptional({ name: 'onhFrequencyList', type: 'string[]', description: 'Frequency-列表条件' })
    onhFrequencyList ? : string[];
    @ApiPropertyOptional({ name: 'nonhFrequencyLike', type: 'string', description: 'Frequency-模糊条件' })
    nonhFrequencyLike ? : string;
    @ApiPropertyOptional({ name: 'nonhFrequencyList', type: 'string[]', description: 'Frequency-列表条件' })
    nonhFrequencyList ? : string[];
    @ApiPropertyOptional({ name: 'onhFrequencyLikeList', type: 'string[]', description: 'Frequency-列表模糊条件' })
    onhFrequencyLikeList ? : string[];
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhColumn1', type: 'string', description: 'Column1' })
    onhColumn1 ? : string;
    @ApiPropertyOptional({ name: 'nonhColumn1', type: 'string', description: 'Column1' })
    nonhColumn1 ? : string;
    @ApiPropertyOptional({ name: 'onhColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    onhColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'onhColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    onhColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'nonhColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    nonhColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'nonhColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    nonhColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'onhColumn1LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    onhColumn1LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhColumn2', type: 'string', description: 'Column2' })
    onhColumn2 ? : string;
    @ApiPropertyOptional({ name: 'nonhColumn2', type: 'string', description: 'Column2' })
    nonhColumn2 ? : string;
    @ApiPropertyOptional({ name: 'onhColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    onhColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'onhColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    onhColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'nonhColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    nonhColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'nonhColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    nonhColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'onhColumn2LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    onhColumn2LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhColumn3', type: 'string', description: 'Column3' })
    onhColumn3 ? : string;
    @ApiPropertyOptional({ name: 'nonhColumn3', type: 'string', description: 'Column3' })
    nonhColumn3 ? : string;
    @ApiPropertyOptional({ name: 'onhColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    onhColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'onhColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    onhColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'nonhColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    nonhColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'nonhColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    nonhColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'onhColumn3LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    onhColumn3LikeList ? : string[];
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhOrder', type: 'number', description: 'ID' })
    onhOrder ? : number;
    @ApiPropertyOptional({ name: 'nonhOrder', type: 'number', description: 'ID' })
    nonhOrder ? : number;
    @ApiPropertyOptional({ name: 'minOnhOrder', type: 'number', description: 'ID-最小值' })
    minOnhOrder ? : number;
    @ApiPropertyOptional({ name: 'maxOnhOrder', type: 'number', description: 'ID-最大值' })
    maxOnhOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhSpecies', type: 'string', description: 'Species' })
    onhSpecies ? : string;
    @ApiPropertyOptional({ name: 'nonhSpecies', type: 'string', description: 'Species' })
    nonhSpecies ? : string;
    @ApiPropertyOptional({ name: 'onhSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    onhSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'onhSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    onhSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'nonhSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    nonhSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'nonhSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    nonhSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'onhSpeciesLikeList', type: 'string[]', description: 'Species-列表模糊条件' })
    onhSpeciesLikeList ? : string[];
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhRemark', type: 'string', description: 'Remark' })
    onhRemark ? : string;
    @ApiPropertyOptional({ name: 'nonhRemark', type: 'string', description: 'Remark' })
    nonhRemark ? : string;
    @ApiPropertyOptional({ name: 'onhRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    onhRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'onhRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    onhRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'nonhRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    nonhRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'nonhRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    nonhRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'onhRemarkLikeList', type: 'string[]', description: 'Remark-列表模糊条件' })
    onhRemarkLikeList ? : string[];
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    onhCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'nonhCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    nonhCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'sOnhCreateDate', type: 'Date', description: '创建日期-起始日期' })
    @Type(() => Date)
    sOnhCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'eOnhCreateDate', type: 'Date', description: '创建日期-终止日期' })
    @Type(() => Date)
    eOnhCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiPropertyOptional({ name: 'onhUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    onhUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'nonhUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    nonhUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'sOnhUpdateDate', type: 'Date', description: '更新日期-起始日期' })
    @Type(() => Date)
    sOnhUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'eOnhUpdateDate', type: 'Date', description: '更新日期-终止日期' })
    @Type(() => Date)
    eOnhUpdateDate ? : Date;
    /**
     * 分页查询记录数
     *
     * @type { number }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
    pageSize ? : number;
    /**
     * 记录偏移量
     *
     * @type { number }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
    offset ? : number;
    /**
     * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'order', type: 'string', description: '排序' })
    order ? : string;
    /**
     * 分组查询条件,例如：SAM_KIND
     *
     * @type { string }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'group', type: 'string', description: '排序' })
    group ? : string;
    /**
     * 结果列限定条件,在只想查询部分列数据时可以给定本参数
     *
     * @type { (keyof OnpNucleusHaplotypeColNames)[]|(keyof OnpNucleusHaplotypeColProps)[] }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpNucleusHaplotypeColNames)[] | (keyof OnpNucleusHaplotypeColProps)[];
    /**
     * 对{cols}给定的列数据进行去重
     *
     * @type { boolean }
     * @memberof OnpNucleusHaplotypeQDto
     */
    @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
    distinct ? : boolean;
}