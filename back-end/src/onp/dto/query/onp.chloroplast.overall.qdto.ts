import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OnpChloroplastOverallColNames, OnpChloroplastOverallColProps } from "../../entity/ddl/onp.chloroplast.overall.cols";
/**
 * ONP_CHLOROPLAST_OVERALL表的QDTO对象
 * @date 4/29/2021, 1:57:37 PM
 * @author jiangbin
 * @export OnpChloroplastOverallQDto
 * @class OnpChloroplastOverallQDto
 */
@ApiTags('ONP_CHLOROPLAST_OVERALL表的QDTO对象')
export class OnpChloroplastOverallQDto {
    /**
     * ID
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'ocoId', type: 'string', description: 'ID' })
    ocoId ? : string;
    @ApiProperty({ name: 'nocoId', type: 'string', description: 'ID' })
    nocoId ? : string;
    @ApiPropertyOptional({ name: 'ocoIdList', type: 'string[]', description: 'ID-列表条件' })
    ocoIdList ? : string[];
    @ApiPropertyOptional({ name: 'nocoIdList', type: 'string[]', description: 'ID-列表条件' })
    nocoIdList ? : string[];
    /**
     * ONP ID
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'ocoOnpId', type: 'string', description: 'ONP ID' })
    ocoOnpId ? : string;
    @ApiProperty({ name: 'nocoOnpId', type: 'string', description: 'ONP ID' })
    nocoOnpId ? : string;
    @ApiPropertyOptional({ name: 'ocoOnpIdLike', type: 'string', description: 'ONP ID-模糊条件' })
    ocoOnpIdLike ? : string;
    @ApiPropertyOptional({ name: 'ocoOnpIdList', type: 'string[]', description: 'ONP ID-列表条件' })
    ocoOnpIdList ? : string[];
    @ApiPropertyOptional({ name: 'nocoOnpIdLike', type: 'string', description: 'ONP ID-模糊条件' })
    nocoOnpIdLike ? : string;
    @ApiPropertyOptional({ name: 'nocoOnpIdList', type: 'string[]', description: 'ONP ID-列表条件' })
    nocoOnpIdList ? : string[];
    @ApiPropertyOptional({ name: 'ocoOnpIdLikeList', type: 'string[]', description: 'ONP ID-列表模糊条件' })
    ocoOnpIdLikeList ? : string[];
    /**
     * Chr
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoChr', type: 'string', description: 'Chr' })
    ocoChr ? : string;
    @ApiPropertyOptional({ name: 'nocoChr', type: 'string', description: 'Chr' })
    nocoChr ? : string;
    @ApiPropertyOptional({ name: 'ocoChrLike', type: 'string', description: 'Chr-模糊条件' })
    ocoChrLike ? : string;
    @ApiPropertyOptional({ name: 'ocoChrList', type: 'string[]', description: 'Chr-列表条件' })
    ocoChrList ? : string[];
    @ApiPropertyOptional({ name: 'nocoChrLike', type: 'string', description: 'Chr-模糊条件' })
    nocoChrLike ? : string;
    @ApiPropertyOptional({ name: 'nocoChrList', type: 'string[]', description: 'Chr-列表条件' })
    nocoChrList ? : string[];
    @ApiPropertyOptional({ name: 'ocoChrLikeList', type: 'string[]', description: 'Chr-列表模糊条件' })
    ocoChrLikeList ? : string[];
    /**
     * Start
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoStart', type: 'string', description: 'Start' })
    ocoStart ? : string;
    @ApiPropertyOptional({ name: 'nocoStart', type: 'string', description: 'Start' })
    nocoStart ? : string;
    @ApiPropertyOptional({ name: 'ocoStartLike', type: 'string', description: 'Start-模糊条件' })
    ocoStartLike ? : string;
    @ApiPropertyOptional({ name: 'ocoStartList', type: 'string[]', description: 'Start-列表条件' })
    ocoStartList ? : string[];
    @ApiPropertyOptional({ name: 'nocoStartLike', type: 'string', description: 'Start-模糊条件' })
    nocoStartLike ? : string;
    @ApiPropertyOptional({ name: 'nocoStartList', type: 'string[]', description: 'Start-列表条件' })
    nocoStartList ? : string[];
    @ApiPropertyOptional({ name: 'ocoStartLikeList', type: 'string[]', description: 'Start-列表模糊条件' })
    ocoStartLikeList ? : string[];
    /**
     * Stop
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoStop', type: 'string', description: 'Stop' })
    ocoStop ? : string;
    @ApiPropertyOptional({ name: 'nocoStop', type: 'string', description: 'Stop' })
    nocoStop ? : string;
    @ApiPropertyOptional({ name: 'ocoStopLike', type: 'string', description: 'Stop-模糊条件' })
    ocoStopLike ? : string;
    @ApiPropertyOptional({ name: 'ocoStopList', type: 'string[]', description: 'Stop-列表条件' })
    ocoStopList ? : string[];
    @ApiPropertyOptional({ name: 'nocoStopLike', type: 'string', description: 'Stop-模糊条件' })
    nocoStopLike ? : string;
    @ApiPropertyOptional({ name: 'nocoStopList', type: 'string[]', description: 'Stop-列表条件' })
    nocoStopList ? : string[];
    @ApiPropertyOptional({ name: 'ocoStopLikeList', type: 'string[]', description: 'Stop-列表模糊条件' })
    ocoStopLikeList ? : string[];
    /**
     * ONP
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoOnpSize', type: 'string', description: 'ONP' })
    ocoOnpSize ? : string;
    @ApiPropertyOptional({ name: 'nocoOnpSize', type: 'string', description: 'ONP' })
    nocoOnpSize ? : string;
    @ApiPropertyOptional({ name: 'ocoOnpSizeLike', type: 'string', description: 'ONP-模糊条件' })
    ocoOnpSizeLike ? : string;
    @ApiPropertyOptional({ name: 'ocoOnpSizeList', type: 'string[]', description: 'ONP-列表条件' })
    ocoOnpSizeList ? : string[];
    @ApiPropertyOptional({ name: 'nocoOnpSizeLike', type: 'string', description: 'ONP-模糊条件' })
    nocoOnpSizeLike ? : string;
    @ApiPropertyOptional({ name: 'nocoOnpSizeList', type: 'string[]', description: 'ONP-列表条件' })
    nocoOnpSizeList ? : string[];
    @ApiPropertyOptional({ name: 'ocoOnpSizeLikeList', type: 'string[]', description: 'ONP-列表模糊条件' })
    ocoOnpSizeLikeList ? : string[];
    /**
     * Location
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoLocation', type: 'string', description: 'Location' })
    ocoLocation ? : string;
    @ApiPropertyOptional({ name: 'nocoLocation', type: 'string', description: 'Location' })
    nocoLocation ? : string;
    @ApiPropertyOptional({ name: 'ocoLocationLike', type: 'string', description: 'Location-模糊条件' })
    ocoLocationLike ? : string;
    @ApiPropertyOptional({ name: 'ocoLocationList', type: 'string[]', description: 'Location-列表条件' })
    ocoLocationList ? : string[];
    @ApiPropertyOptional({ name: 'nocoLocationLike', type: 'string', description: 'Location-模糊条件' })
    nocoLocationLike ? : string;
    @ApiPropertyOptional({ name: 'nocoLocationList', type: 'string[]', description: 'Location-列表条件' })
    nocoLocationList ? : string[];
    @ApiPropertyOptional({ name: 'ocoLocationLikeList', type: 'string[]', description: 'Location-列表模糊条件' })
    ocoLocationLikeList ? : string[];
    /**
     * ALL
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoAllMarkers', type: 'string', description: 'ALL' })
    ocoAllMarkers ? : string;
    @ApiPropertyOptional({ name: 'nocoAllMarkers', type: 'string', description: 'ALL' })
    nocoAllMarkers ? : string;
    @ApiPropertyOptional({ name: 'ocoAllMarkersLike', type: 'string', description: 'ALL-模糊条件' })
    ocoAllMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'ocoAllMarkersList', type: 'string[]', description: 'ALL-列表条件' })
    ocoAllMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'nocoAllMarkersLike', type: 'string', description: 'ALL-模糊条件' })
    nocoAllMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'nocoAllMarkersList', type: 'string[]', description: 'ALL-列表条件' })
    nocoAllMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'ocoAllMarkersLikeList', type: 'string[]', description: 'ALL-列表模糊条件' })
    ocoAllMarkersLikeList ? : string[];
    /**
     * SNP
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoSnpMarkers', type: 'string', description: 'SNP' })
    ocoSnpMarkers ? : string;
    @ApiPropertyOptional({ name: 'nocoSnpMarkers', type: 'string', description: 'SNP' })
    nocoSnpMarkers ? : string;
    @ApiPropertyOptional({ name: 'ocoSnpMarkersLike', type: 'string', description: 'SNP-模糊条件' })
    ocoSnpMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'ocoSnpMarkersList', type: 'string[]', description: 'SNP-列表条件' })
    ocoSnpMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'nocoSnpMarkersLike', type: 'string', description: 'SNP-模糊条件' })
    nocoSnpMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'nocoSnpMarkersList', type: 'string[]', description: 'SNP-列表条件' })
    nocoSnpMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'ocoSnpMarkersLikeList', type: 'string[]', description: 'SNP-列表模糊条件' })
    ocoSnpMarkersLikeList ? : string[];
    /**
     * INDEL
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoIndelMarkers', type: 'string', description: 'INDEL' })
    ocoIndelMarkers ? : string;
    @ApiPropertyOptional({ name: 'nocoIndelMarkers', type: 'string', description: 'INDEL' })
    nocoIndelMarkers ? : string;
    @ApiPropertyOptional({ name: 'ocoIndelMarkersLike', type: 'string', description: 'INDEL-模糊条件' })
    ocoIndelMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'ocoIndelMarkersList', type: 'string[]', description: 'INDEL-列表条件' })
    ocoIndelMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'nocoIndelMarkersLike', type: 'string', description: 'INDEL-模糊条件' })
    nocoIndelMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'nocoIndelMarkersList', type: 'string[]', description: 'INDEL-列表条件' })
    nocoIndelMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'ocoIndelMarkersLikeList', type: 'string[]', description: 'INDEL-列表模糊条件' })
    ocoIndelMarkersLikeList ? : string[];
    /**
     * Block
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoBlockMarkers', type: 'string', description: 'Block' })
    ocoBlockMarkers ? : string;
    @ApiPropertyOptional({ name: 'nocoBlockMarkers', type: 'string', description: 'Block' })
    nocoBlockMarkers ? : string;
    @ApiPropertyOptional({ name: 'ocoBlockMarkersLike', type: 'string', description: 'Block-模糊条件' })
    ocoBlockMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'ocoBlockMarkersList', type: 'string[]', description: 'Block-列表条件' })
    ocoBlockMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'nocoBlockMarkersLike', type: 'string', description: 'Block-模糊条件' })
    nocoBlockMarkersLike ? : string;
    @ApiPropertyOptional({ name: 'nocoBlockMarkersList', type: 'string[]', description: 'Block-列表条件' })
    nocoBlockMarkersList ? : string[];
    @ApiPropertyOptional({ name: 'ocoBlockMarkersLikeList', type: 'string[]', description: 'Block-列表模糊条件' })
    ocoBlockMarkersLikeList ? : string[];
    /**
     * Tags
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoTags', type: 'string', description: 'Tags' })
    ocoTags ? : string;
    @ApiPropertyOptional({ name: 'nocoTags', type: 'string', description: 'Tags' })
    nocoTags ? : string;
    @ApiPropertyOptional({ name: 'ocoTagsLike', type: 'string', description: 'Tags-模糊条件' })
    ocoTagsLike ? : string;
    @ApiPropertyOptional({ name: 'ocoTagsList', type: 'string[]', description: 'Tags-列表条件' })
    ocoTagsList ? : string[];
    @ApiPropertyOptional({ name: 'nocoTagsLike', type: 'string', description: 'Tags-模糊条件' })
    nocoTagsLike ? : string;
    @ApiPropertyOptional({ name: 'nocoTagsList', type: 'string[]', description: 'Tags-列表条件' })
    nocoTagsList ? : string[];
    @ApiPropertyOptional({ name: 'ocoTagsLikeList', type: 'string[]', description: 'Tags-列表模糊条件' })
    ocoTagsLikeList ? : string[];
    /**
     * Genotypes
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoGenotypes', type: 'string', description: 'Genotypes' })
    ocoGenotypes ? : string;
    @ApiPropertyOptional({ name: 'nocoGenotypes', type: 'string', description: 'Genotypes' })
    nocoGenotypes ? : string;
    @ApiPropertyOptional({ name: 'ocoGenotypesLike', type: 'string', description: 'Genotypes-模糊条件' })
    ocoGenotypesLike ? : string;
    @ApiPropertyOptional({ name: 'ocoGenotypesList', type: 'string[]', description: 'Genotypes-列表条件' })
    ocoGenotypesList ? : string[];
    @ApiPropertyOptional({ name: 'nocoGenotypesLike', type: 'string', description: 'Genotypes-模糊条件' })
    nocoGenotypesLike ? : string;
    @ApiPropertyOptional({ name: 'nocoGenotypesList', type: 'string[]', description: 'Genotypes-列表条件' })
    nocoGenotypesList ? : string[];
    @ApiPropertyOptional({ name: 'ocoGenotypesLikeList', type: 'string[]', description: 'Genotypes-列表模糊条件' })
    ocoGenotypesLikeList ? : string[];
    /**
     * Max
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoMaxGenotypesFreq', type: 'string', description: 'Max' })
    ocoMaxGenotypesFreq ? : string;
    @ApiPropertyOptional({ name: 'nocoMaxGenotypesFreq', type: 'string', description: 'Max' })
    nocoMaxGenotypesFreq ? : string;
    @ApiPropertyOptional({ name: 'ocoMaxGenotypesFreqLike', type: 'string', description: 'Max-模糊条件' })
    ocoMaxGenotypesFreqLike ? : string;
    @ApiPropertyOptional({ name: 'ocoMaxGenotypesFreqList', type: 'string[]', description: 'Max-列表条件' })
    ocoMaxGenotypesFreqList ? : string[];
    @ApiPropertyOptional({ name: 'nocoMaxGenotypesFreqLike', type: 'string', description: 'Max-模糊条件' })
    nocoMaxGenotypesFreqLike ? : string;
    @ApiPropertyOptional({ name: 'nocoMaxGenotypesFreqList', type: 'string[]', description: 'Max-列表条件' })
    nocoMaxGenotypesFreqList ? : string[];
    @ApiPropertyOptional({ name: 'ocoMaxGenotypesFreqLikeList', type: 'string[]', description: 'Max-列表模糊条件' })
    ocoMaxGenotypesFreqLikeList ? : string[];
    /**
     * Min
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoMinGenotypesFreq', type: 'string', description: 'Min' })
    ocoMinGenotypesFreq ? : string;
    @ApiPropertyOptional({ name: 'nocoMinGenotypesFreq', type: 'string', description: 'Min' })
    nocoMinGenotypesFreq ? : string;
    @ApiPropertyOptional({ name: 'ocoMinGenotypesFreqLike', type: 'string', description: 'Min-模糊条件' })
    ocoMinGenotypesFreqLike ? : string;
    @ApiPropertyOptional({ name: 'ocoMinGenotypesFreqList', type: 'string[]', description: 'Min-列表条件' })
    ocoMinGenotypesFreqList ? : string[];
    @ApiPropertyOptional({ name: 'nocoMinGenotypesFreqLike', type: 'string', description: 'Min-模糊条件' })
    nocoMinGenotypesFreqLike ? : string;
    @ApiPropertyOptional({ name: 'nocoMinGenotypesFreqList', type: 'string[]', description: 'Min-列表条件' })
    nocoMinGenotypesFreqList ? : string[];
    @ApiPropertyOptional({ name: 'ocoMinGenotypesFreqLikeList', type: 'string[]', description: 'Min-列表模糊条件' })
    ocoMinGenotypesFreqLikeList ? : string[];
    /**
     * PIC
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoPic', type: 'string', description: 'PIC' })
    ocoPic ? : string;
    @ApiPropertyOptional({ name: 'nocoPic', type: 'string', description: 'PIC' })
    nocoPic ? : string;
    @ApiPropertyOptional({ name: 'ocoPicLike', type: 'string', description: 'PIC-模糊条件' })
    ocoPicLike ? : string;
    @ApiPropertyOptional({ name: 'ocoPicList', type: 'string[]', description: 'PIC-列表条件' })
    ocoPicList ? : string[];
    @ApiPropertyOptional({ name: 'nocoPicLike', type: 'string', description: 'PIC-模糊条件' })
    nocoPicLike ? : string;
    @ApiPropertyOptional({ name: 'nocoPicList', type: 'string[]', description: 'PIC-列表条件' })
    nocoPicList ? : string[];
    @ApiPropertyOptional({ name: 'ocoPicLikeList', type: 'string[]', description: 'PIC-列表模糊条件' })
    ocoPicLikeList ? : string[];
    /**
     * Column1
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoColumn1', type: 'string', description: 'Column1' })
    ocoColumn1 ? : string;
    @ApiPropertyOptional({ name: 'nocoColumn1', type: 'string', description: 'Column1' })
    nocoColumn1 ? : string;
    @ApiPropertyOptional({ name: 'ocoColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    ocoColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'ocoColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    ocoColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'nocoColumn1Like', type: 'string', description: 'Column1-模糊条件' })
    nocoColumn1Like ? : string;
    @ApiPropertyOptional({ name: 'nocoColumn1List', type: 'string[]', description: 'Column1-列表条件' })
    nocoColumn1List ? : string[];
    @ApiPropertyOptional({ name: 'ocoColumn1LikeList', type: 'string[]', description: 'Column1-列表模糊条件' })
    ocoColumn1LikeList ? : string[];
    /**
     * Column2
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoColumn2', type: 'string', description: 'Column2' })
    ocoColumn2 ? : string;
    @ApiPropertyOptional({ name: 'nocoColumn2', type: 'string', description: 'Column2' })
    nocoColumn2 ? : string;
    @ApiPropertyOptional({ name: 'ocoColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    ocoColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'ocoColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    ocoColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'nocoColumn2Like', type: 'string', description: 'Column2-模糊条件' })
    nocoColumn2Like ? : string;
    @ApiPropertyOptional({ name: 'nocoColumn2List', type: 'string[]', description: 'Column2-列表条件' })
    nocoColumn2List ? : string[];
    @ApiPropertyOptional({ name: 'ocoColumn2LikeList', type: 'string[]', description: 'Column2-列表模糊条件' })
    ocoColumn2LikeList ? : string[];
    /**
     * Column3
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoColumn3', type: 'string', description: 'Column3' })
    ocoColumn3 ? : string;
    @ApiPropertyOptional({ name: 'nocoColumn3', type: 'string', description: 'Column3' })
    nocoColumn3 ? : string;
    @ApiPropertyOptional({ name: 'ocoColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    ocoColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'ocoColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    ocoColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'nocoColumn3Like', type: 'string', description: 'Column3-模糊条件' })
    nocoColumn3Like ? : string;
    @ApiPropertyOptional({ name: 'nocoColumn3List', type: 'string[]', description: 'Column3-列表条件' })
    nocoColumn3List ? : string[];
    @ApiPropertyOptional({ name: 'ocoColumn3LikeList', type: 'string[]', description: 'Column3-列表模糊条件' })
    ocoColumn3LikeList ? : string[];
    /**
     * ID
     *
     * @type { number }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoOrder', type: 'number', description: 'ID' })
    ocoOrder ? : number;
    @ApiPropertyOptional({ name: 'nocoOrder', type: 'number', description: 'ID' })
    nocoOrder ? : number;
    @ApiPropertyOptional({ name: 'minOcoOrder', type: 'number', description: 'ID-最小值' })
    minOcoOrder ? : number;
    @ApiPropertyOptional({ name: 'maxOcoOrder', type: 'number', description: 'ID-最大值' })
    maxOcoOrder ? : number;
    /**
     * Species
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoSpecies', type: 'string', description: 'Species' })
    ocoSpecies ? : string;
    @ApiPropertyOptional({ name: 'nocoSpecies', type: 'string', description: 'Species' })
    nocoSpecies ? : string;
    @ApiPropertyOptional({ name: 'ocoSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    ocoSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'ocoSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    ocoSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'nocoSpeciesLike', type: 'string', description: 'Species-模糊条件' })
    nocoSpeciesLike ? : string;
    @ApiPropertyOptional({ name: 'nocoSpeciesList', type: 'string[]', description: 'Species-列表条件' })
    nocoSpeciesList ? : string[];
    @ApiPropertyOptional({ name: 'ocoSpeciesLikeList', type: 'string[]', description: 'Species-列表模糊条件' })
    ocoSpeciesLikeList ? : string[];
    /**
     * Remark
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoRemark', type: 'string', description: 'Remark' })
    ocoRemark ? : string;
    @ApiPropertyOptional({ name: 'nocoRemark', type: 'string', description: 'Remark' })
    nocoRemark ? : string;
    @ApiPropertyOptional({ name: 'ocoRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    ocoRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'ocoRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    ocoRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'nocoRemarkLike', type: 'string', description: 'Remark-模糊条件' })
    nocoRemarkLike ? : string;
    @ApiPropertyOptional({ name: 'nocoRemarkList', type: 'string[]', description: 'Remark-列表条件' })
    nocoRemarkList ? : string[];
    @ApiPropertyOptional({ name: 'ocoRemarkLikeList', type: 'string[]', description: 'Remark-列表模糊条件' })
    ocoRemarkLikeList ? : string[];
    /**
     * 创建日期
     *
     * @type { Date }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    ocoCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'nocoCreateDate', type: 'Date', description: '创建日期' })
    @Type(() => Date)
    nocoCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'sOcoCreateDate', type: 'Date', description: '创建日期-起始日期' })
    @Type(() => Date)
    sOcoCreateDate ? : Date;
    @ApiPropertyOptional({ name: 'eOcoCreateDate', type: 'Date', description: '创建日期-终止日期' })
    @Type(() => Date)
    eOcoCreateDate ? : Date;
    /**
     * 更新日期
     *
     * @type { Date }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiPropertyOptional({ name: 'ocoUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    ocoUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'nocoUpdateDate', type: 'Date', description: '更新日期' })
    @Type(() => Date)
    nocoUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'sOcoUpdateDate', type: 'Date', description: '更新日期-起始日期' })
    @Type(() => Date)
    sOcoUpdateDate ? : Date;
    @ApiPropertyOptional({ name: 'eOcoUpdateDate', type: 'Date', description: '更新日期-终止日期' })
    @Type(() => Date)
    eOcoUpdateDate ? : Date;
    /**
     * 分页查询记录数
     *
     * @type { number }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
    pageSize ? : number;
    /**
     * 记录偏移量
     *
     * @type { number }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
    offset ? : number;
    /**
     * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'order', type: 'string', description: '排序' })
    order ? : string;
    /**
     * 分组查询条件,例如：SAM_KIND
     *
     * @type { string }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'group', type: 'string', description: '排序' })
    group ? : string;
    /**
     * 结果列限定条件,在只想查询部分列数据时可以给定本参数
     *
     * @type { (keyof OnpChloroplastOverallColNames)[]|(keyof OnpChloroplastOverallColProps)[] }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
    cols ? : (keyof OnpChloroplastOverallColNames)[] | (keyof OnpChloroplastOverallColProps)[];
    /**
     * 对{cols}给定的列数据进行去重
     *
     * @type { boolean }
     * @memberof OnpChloroplastOverallQDto
     */
    @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
    distinct ? : boolean;
}