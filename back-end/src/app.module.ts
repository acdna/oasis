import { ClassSerializerInterceptor, Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SysModule } from './sys/sys.module';
import { ErrorsInterceptor } from './common/interceptor/errors.interceptor';
import { ScheduleModule } from '@nestjs/schedule';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AuthModule } from './common/auth/audit.module';
import { UrllogInterceptor } from './common/interceptor/urllog.interceptor';
import { PmsGuard } from './common/auth/guard/pms.guard';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { JwtAuthGuard } from './common/auth/guard/jwt.auth.guard';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './common/config/configuration';
import { ApiConfig } from './common/config/api.config';
import { Log4jsModule } from './common/log4js/log4js.module';
import { FileModule } from './common/file/file.module';
import { Log4jsTypeorm } from './common/log4js/log4js.typeorm';
import { SysRequestContext } from './sys/context/sys.request.context';
import { RequestContextModule } from '@medibloc/nestjs-request-context';
import { DocxModule } from './common/docx/docx.module';
import { ExcelModule } from './common/excel/excel.module';
import { SystemModule } from './common/system/system.module';
import { OnpModule } from './onp/onp.module';
import {MailModule} from "./common/mail/mail.module";

@Module({
  imports: [
    //上下文对象
    RequestContextModule.forRoot({
      contextClass: SysRequestContext,
      isGlobal: true,
    }),
    //log4js日志模块
    Log4jsModule.forRoot(),
    //系统基础参数
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    //数据库连接
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule, Log4jsModule],
      useFactory: async (config: ConfigService, logger: Log4jsTypeorm) => {
        const host = config.get('database.host');
        const port = parseInt(process.env.MYSQL_PORT) || config.get<number>('database.port');
        const database = process.env.MYSQL_DATABASE || config.get('database.database');
        const username = process.env.MYSQL_USER || config.get('database.username');
        const password = process.env.MYSQL_PASSWORD || config.get('database.password');
        Logger.log(`DB TYPE==>mysql`);
        Logger.log(`DB HOST==>${host}`);
        Logger.log(`DB PORT==>${port}`);
        Logger.log(`DB DATABASE==>${database}`);
        Logger.log(`DB USERNAME==>${username}`);
        Logger.log(`DB PASSWORD==>${password}`);
        return {
          type: 'mysql',
          host: host,
          port: port,
          database: database,
          username: username,
          password: password,
          logging: config.get<boolean>('database.logging'),
          logger: logger,
          synchronize: config.get<boolean>('database.synchronize'),
          entities: [config.get('database.entities')],
          timezone: config.get('database.timezone'),
        };
      },
      inject: [ConfigService, Log4jsTypeorm],
    }),
    //定时任务
    ScheduleModule.forRoot(),
    //事件
    EventEmitterModule.forRoot(),
    //身份认证
    AuthModule,
    //文件模块
    FileModule,
    //Word模块
    DocxModule,
    //Excel模块
    ExcelModule,
    //系统模块
    SystemModule,
    //业务模块
    SysModule,
    //ONP业务模块
    OnpModule,
    //mail业务模块
    MailModule,
  ],
  controllers: [AppController],
  providers: [
    {
      //错误消息
      provide: APP_INTERCEPTOR,
      useClass: ErrorsInterceptor,
    },
    {
      //打印URL
      provide: APP_INTERCEPTOR,
      useClass: UrllogInterceptor,
    },
    {
      //序列化,自动过滤敏感信息，插件：class-transformer
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      //身份认证守卫
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      //权限验证
      provide: APP_GUARD,
      useClass: PmsGuard,
    },
    AppService,
    ApiConfig,
  ],
  exports: [ApiConfig],
})
export class AppModule {}
