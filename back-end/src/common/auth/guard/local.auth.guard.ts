import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

/**
 * Local授权认证守卫
 */
@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {}
