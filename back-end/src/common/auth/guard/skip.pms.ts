import { SetMetadata } from '@nestjs/common';

export const IS_PUBLIC_PMS_KEY = 'isPublicPmsKey';

/**
 * 装饰到那些不需要权限认证的控制器方法上，这样可以定义一些开放的权限
 * @constructor
 */
export const SkipPms = () => SetMetadata(IS_PUBLIC_PMS_KEY, true);
