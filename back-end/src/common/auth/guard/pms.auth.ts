import { SetMetadata } from '@nestjs/common';
import { PmsItem } from '../../pms/pms.info';

export const PMS_KEY = 'PMS_KEY';

/**
 * 装饰到那些需要功能权限认证的控制器方法上，以便指定控制器方法所需要的功能权限列表，
 * 在功能权限守卫中会自动通过反射功能进行用户功能权限认证，保证用户是合法访问相关控制器方法的
 * @constructor
 */
export const Permission = (...pms: (PmsItem | Partial<PmsItem>)[]) => SetMetadata(PMS_KEY, pms);
