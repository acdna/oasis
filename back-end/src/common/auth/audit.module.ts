import { forwardRef, Global, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './service/auth.service';
import { JwtStrategy } from './strategy/jwt.strategy';
import { AuthController } from './controller/auth.controller';
import { LocalStrategy } from './strategy/local.strategy';
import { SysModule } from '../../sys/sys.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ApiConfig } from '../config/api.config';
import { CONFIG } from '../domain/constants';

@Global()
@Module({
  imports: [
    forwardRef(() => SysModule),
    PassportModule.register({ defaultStrategy: 'jwt', session: true }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        return {
          secretOrKey: config.get(CONFIG.JWT_SECERET),
          signOptions: {
            expiresIn: config.get<number>(CONFIG.JWT_EXPIRES_IN),
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy, ApiConfig],
  exports: [AuthService, JwtStrategy, LocalStrategy],
})
export class AuthModule {}
