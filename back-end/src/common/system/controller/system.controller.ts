import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base/controller/base.controller';
import { ConfigMapper } from '../../../sys/config/config.mapper';
import { DictMapper } from '../../../sys/dictionary/dict.mapper';
import { UserMapper } from '../../../sys/user/user.mapper';
import { HTTP_CODE, HttpUtils } from '../../constants/http.code';

/**
 * 系统功能控制器
 * @author: jiangbin
 * @date: 2021-02-09 09:41:48
 **/
@Controller('system')
@ApiTags('系统功能')
export class SystemController extends BaseController {
  /**
   * 刷新缓存数据，包括：参数、字典和用户缓存
   * @author: jiangbin
   * @date: 2021-02-09 09:47:01
   **/
  @ApiOperation({ summary: '刷新缓存数据' })
  @Get('refresh/cache')
  async refreshCache() {
    ConfigMapper.reload();
    DictMapper.reload();
    UserMapper.reload();
    return HttpUtils.result(HTTP_CODE.SUCCESS);
  }
}
