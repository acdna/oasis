import { BaseController } from '../../base/controller/base.controller';
import { Body, Controller, Get, HttpCode, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { DOCX_TEMPLATES } from '../templates/docx.template.id';
import { DocxTemplate } from '../utils/docx.template';
import { PathUtils } from '../../path/path.creator.utils';
import { PathResult } from '../../path/path.creator';
import { SkipAuth } from '../../auth/guard/skip.auth';
import { DocxContext } from '../templates/docx.context';
import { HTTP_CODE, HttpUtils } from '../../constants/http.code';

/**
 * 合并Word模板和数据并生成新的Word文件，可用于生成报告等业务中
 * @author: jiangbin
 * @date: 2021-02-04 19:24:19
 **/
@Controller('docx')
@ApiTags('Word合并数据')
export class DocxController extends BaseController {
  /**
   * 获取指定ID的Word模板相对路径，这个路径是相对于项目的back-end/public目录的
   * @author: jiangbin
   * @date: 2021-02-05 14:50:29
   **/
  @SkipAuth()
  @ApiOperation({ summary: '获取指定ID的Word模板全路径' })
  @ApiQuery({ name: 'templateId', description: 'Word模板ID' })
  @Get('get/template/path')
  async getTemplatePath(@Query('templateId') templateId: string): Promise<string> {
    return DocxContext.getTemplateRelativePath(DOCX_TEMPLATES[templateId]);
  }

  /**
   * 使用指定Word模板ID文件创建Word文件
   * @param templateId Word模板ID
   * @param data 合并Word模板时使用的JSON格式数据
   * @author: jiangbin
   * @date: 2021-02-04 19:25:00
   **/
  @HttpCode(HTTP_CODE.SUCCESS)
  @SkipAuth()
  @ApiOperation({ summary: '合并ID的Word模板合并数据' })
  @ApiQuery({ name: 'templateId', description: 'Word模板ID' })
  @ApiQuery({ name: 'data', description: 'Word模板使用的JSON格式参数' })
  @Post('create/by/templateId')
  async createByTemplateId(@Body('templateId') templateId: string, @Body('data') data: any): Promise<PathResult> {
    try {
      this.logger().info(`接收到的参数=>${JSON.stringify(data)}`);
      this.logger().info(`Word模板ID=>${templateId}`);

      const filePath = DocxContext.getTemplatePath(DOCX_TEMPLATES[templateId]);
      this.logger().info(`Word模板文件路径=>${filePath}`);
      if (!filePath || filePath.length == 0) {
        this.logger().error(`Word模板ID未定义==>${templateId}`);
        return HttpUtils.result(HTTP_CODE.NO_RECORD);
      }

      //合并Word模板
      const pathResult = await DocxTemplate.create(filePath, data);
      this.logger().info(`生成的Word文件路径=>${JSON.stringify(pathResult)}`);

      return HttpUtils.result(HTTP_CODE.SUCCESS, pathResult);
    } catch (err) {
      this.logger().error(`Word模板处理失败==>${err}`);
      return HttpUtils.result(HTTP_CODE.SERVER_ERROR);
    }
  }

  /**
   * 使用指定相对路径的Word模板创建Word文件
   * @param tplPath Word模板文件的相对存储路径
   * @param data 合并Word模板时使用的JSON格式数据
   * @return
   * @author: jiangbin
   * @date: 2021-02-04 19:25:00
   **/
  @HttpCode(HTTP_CODE.SUCCESS)
  @SkipAuth()
  @ApiOperation({ summary: '基于相对路径Word模板合并数据' })
  @ApiQuery({ name: 'tplPath', description: 'Word模板文件相对路径' })
  @ApiQuery({ name: 'data', description: 'Word模板使用的JSON格式参数' })
  @Post('create/by/relative/path')
  async createByPath(@Body('tplPath') tplPath: string, @Body('data') data: any): Promise<PathResult> {
    try {
      this.logger().info(`接收到的参数=>${JSON.stringify(data)}`);
      this.logger().info(`Word模板文件相对路径=>${tplPath}`);

      if (!tplPath || tplPath.length == 0) {
        this.logger().error(`Word模板相对路径未给定==>${tplPath}`);
        return HttpUtils.result(HTTP_CODE.NO_RECORD);
      }

      //合并Word模板
      const fullPath = PathUtils.getFullPath({ relativePath: tplPath });
      const pathResult = await DocxTemplate.create(fullPath, data);

      this.logger().info(`生成的Word文件==>${JSON.stringify(pathResult)}`);
      return HttpUtils.result(HTTP_CODE.SUCCESS, pathResult);
    } catch (err) {
      this.logger().error(`Word模板处理失败==>${err}`);
      return HttpUtils.result(HTTP_CODE.SERVER_ERROR);
    }
  }

  /**
   * 基于全路径的文件模板生成Word
   * @param tplPath Word模板文件的相对存储路径
   * @param data 合并Word模板时使用的JSON格式数据
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 14:22:35
   **/
  @HttpCode(HTTP_CODE.SUCCESS)
  @SkipAuth()
  @ApiOperation({ summary: '基于全路径Word模板合并数据' })
  @ApiQuery({ name: 'tplPath', description: 'Word模板文件全路径' })
  @ApiQuery({ name: 'data', description: 'Word模板使用的JSON格式参数' })
  @Post('create/by/full/path')
  async createByFullPath(@Body('tplPath') tplPath: string, @Body('data') data: any): Promise<PathResult> {
    try {
      this.logger().info(`接收到的参数为=>${JSON.stringify(data)}`);
      this.logger().info(`Word模板文件全路径=>${tplPath}`);

      if (!tplPath || tplPath.length == 0) {
        this.logger().error(`Word模板全路径未给定==>${tplPath}`);
        return HttpUtils.result(HTTP_CODE.NO_RECORD);
      }

      //合并Word模板
      const pathResult = await DocxTemplate.create(tplPath, data);

      this.logger().info(`生成的Word文件==>${JSON.stringify(pathResult)}`);
      return HttpUtils.result(HTTP_CODE.SUCCESS, pathResult);
    } catch (err) {
      this.logger().error(`Word模板处理失败==>${err}`);
      return HttpUtils.result(HTTP_CODE.SERVER_ERROR);
    }
  }
}
