/**
 * 参数名称
 * @author: jiangbin
 * @date: 2020-12-07 18:45:39
 **/
export const CONFIG = {
  SERVER_HOST: 'server.host',
  SERVER_PORT: 'server.port',
  AUTH_ENABLED: 'auth.enabled',
  PMS_ENABLED: 'pms.enabled',
  JWT_SECERET: 'auth.jwt.seceret',
  JWT_EXPIRES_IN: 'auth.jwt.expires_in',
  LIMITRATE_MAX: 'limitrate.max',
  LIMITRATE_WINDOW_MS: 'limitrate.window_ms',
  REQ_JSON_LIMIT: 'request.limit.json',
  REQ_URLENCODED_LIMIT: 'request.limit.urlencoded',
  MAIL_HOST: 'mail.host',
  MAIL_PORT: 'mail.port',
  MAIL_USER: 'mail.auth.user',
  MAIL_PASSWORD: 'mail.auth.pass',
  MAIL_FROM: 'mail.from',
  DB_TYPE: 'database.type',
  DB_HOST: 'database.host',
  DB_PORT: 'database.port',
  DB_USERNAME: 'database.username',
  DB_PASSWORD: 'database.password',
  DB_DATABASE: 'database.database',
  DB_SYNCHRONIZE: 'database.synchronize',
  DB_LOGGING: 'database.logging',
  DB_ENTITIES: 'database.entities',
  DB_TIMEZONE: 'database.timezone',
};
