import { Configuration } from 'log4js';
import { join, resolve } from 'path';

const basePath = resolve(process.cwd(), 'logs');

/**
 * 构建默认配置参数
 * @author: jiangbin
 * @date: 2020-12-30 15:57:58
 **/
export function buildDefaultConfig(level: string): Configuration {
  return {
    appenders: {
      logToSqlFile: {
        type: 'dateFile',
        filename: join(basePath, 'sql/sql'),
        alwaysIncludePattern: true,
        pattern: 'yyyy-MM-dd.log',
        maxLogSize: 1024 * 1024 * 10,
        backups: 10,
        daysToKeep: 30,
        category: 'sql',
      },
      sqlLogger: {
        type: 'logLevelFilter',
        appender: 'logToSqlFile',
        level: level,
      },
      logToErrorFile: {
        type: 'dateFile',
        filename: join(basePath, 'err/err'),
        alwaysIncludePattern: true,
        pattern: 'yyyy-MM-dd.log',
        maxLogSize: 1024 * 1024 * 10,
        backups: 10,
        daysToKeep: 14,
      },
      errorLogger: {
        type: 'logLevelFilter',
        appender: 'logToErrorFile',
        level: 'error',
      },
      appLogger: {
        type: 'dateFile',
        filename: join(basePath, 'all/all'),
        alwaysIncludePattern: true,
        pattern: 'yyyy-MM-dd.log',
        maxLogSize: 1024 * 1024 * 10,
        backups: 10,
        daysToKeep: 14,
      },
      consoleLogger: {
        type: 'console',
        layout: {
          type: 'colored',
        },
      },
    },
    categories: {
      default: {
        appenders: ['consoleLogger', 'appLogger', 'errorLogger'],
        level: 'all',
      },
      sql: {
        appenders: ['consoleLogger', 'appLogger', 'sqlLogger', 'errorLogger'],
        level: 'all',
      },
      error: {
        appenders: ['consoleLogger', 'errorLogger'],
        level: 'error',
      },
    },
  };
}
