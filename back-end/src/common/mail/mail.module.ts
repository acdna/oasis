import {forwardRef, Global, Logger, Module} from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { MailController } from './controller/mail.controller';
import { PUBLIC_FOLDER } from '../path/path.creator.utils';
import {SysModule} from "../../sys/sys.module";

/**
 * 邮件模块
 *
 * @author jiang
 * @date 2021-04-10 10:16:23
 **/
@Global()
@Module({
  imports: [
    forwardRef(() => SysModule),
    //邮件服务
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        Logger.log(`DOCKER EMAIL SMTP HOST==>${process.env.SMTP_HOST || ''}`);
        Logger.log(`DOCKER EMAIL SMTP PORT==>${process.env.SMTP_PORT || ''}`);
        Logger.log(`DOCKER EMAIL SMTP USER==>${process.env.SMTP_USER || ''}`);
        Logger.log(`DOCKER EMAIL FROM==>${process.env.SMTP_FROM || ''}`);
        const host = process.env.SMTP_HOST || config.get('mail.host');
        const port = parseInt(process.env.SMTP_PORT) || config.get<number>('mail.port');
        const user = process.env.SMTP_USER || config.get('mail.auth.user');
        const pass = process.env.SMTP_PASSWORD || config.get('mail.auth.pass');
        const from = process.env.SMTP_FROM || config.get('mail.from');
        Logger.log(`EMAIL SMTP HOST==>${host}`);
        Logger.log(`EMAIL SMTP PORT==>${port}`);
        Logger.log(`EMAIL SMTP USER==>${user}`);
        Logger.log(`EMAIL FROM==>${from}`);
        Logger.log(`EMAIL PASS==>${pass}`);

        return {
          transport: {
            host,
            port,
            secure: true,
            auth: {
              user,
              pass,
            },
            preview: true,
            template: {
              dir: `${PUBLIC_FOLDER}/template/mail`,
              adapter: new PugAdapter(),
              options: {
                strict: true,
              },
            },
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [MailController],
  providers: [],
  exports: [],
})
export class MailModule {}
