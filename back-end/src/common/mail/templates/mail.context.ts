import * as path from 'path';
import { LocaleContext } from '../../locales/locale';
import { PUBLIC_FOLDER } from '../../path/path.creator.utils';

/**
 * 邮件模板工具类
 * @author: jiangbin
 * @date: 2021-02-05 16:20:46
 **/
export const MailContext = {
  /**
   * 获取邮件模板的相对路径，例如相对路径为：
   *
   * template/mail/template.pug
   *
   * 则该文件可通过如下URL下载该文件:
   *
   * {@link http://localhost:3000/static/template/mail/template.pug}
   *
   * @author: jiangbin
   * @date: 2021-02-05 14:54:55
   **/
  getTemplateRelativePath: (fileName: { en: string; cn: string }) => {
    if (!fileName) return undefined;
    return path.join('template/mail', LocaleContext.message(fileName));
  },

  /**
   * 获取邮件模板文件路径
   * @param fileName.en 英文模板
   * @param fileName.cn 中文模板
   * @author: jiangbin
   * @date: 2021-02-03 12:21:30
   **/
  getTemplatePath: (fileName: { en: string; cn: string }) => {
    if (!fileName) return undefined;
    return path.join(PUBLIC_FOLDER, MailContext.getTemplateRelativePath(fileName));
  },
};
