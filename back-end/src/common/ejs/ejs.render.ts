const ejs = require('ejs');

/**
 * 渲染SQL字符串，生成原生SQL脚本
 * @param str
 * @param options
 */
export function getSql(str, options) {
  return ejs.compile(str, options);
}
