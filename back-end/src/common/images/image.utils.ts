import * as path from 'path';
import * as fs from 'fs';
import { PUBLIC_FOLDER } from '../path/path.creator.utils';
import probe from 'probe-image-size';

/**
 * 图片工具类
 * @author: jiangbin
 * @date: 2021-02-05 09:04:21
 **/
export const ImageUtils = {
  /**
   * 获取公共图片目录中图片的全路径
   * @param imageId 图片ID
   * @return {@link string} 图片的实际存储路径
   * @author: jiangbin
   * @date: 2021-02-05 00:23:04
   **/
  getImagePath: (imageId: string) => {
    if (!imageId) return undefined;
    return path.join(PUBLIC_FOLDER, 'images', imageId);
  },

  /**
   * 获取图片内容
   * @param params.imageId 图片ID
   * @param params.imagePath 图片路径
   * @return {@link Buffer}
   * @author: jiangbin
   * @date: 2021-02-05 00:22:56
   **/
  getImageData: (params?: { imageId?: string; imagePath?: string }): Buffer => {
    if (params?.imageId) {
      params.imagePath = ImageUtils.getImagePath(params.imageId);
    }
    if (params?.imagePath) {
      return fs.readFileSync(params.imagePath);
    }
    return undefined;
  },

  /**
   * 获取图片信息，如宽度、高度、分辨率等
   * @param params.imageId 图片ID
   * @param params.imagePath 图片路径
   * @return {@link Buffer}
   * @author: jiangbin
   * @date: 2021-02-05 08:57:55
   **/
  getImageInfo: async (params?: { imageId?: string; imagePath?: string }) => {
    if (!params || (params && !params.imageId && !params.imagePath)) return undefined;
    return await probe(ImageUtils.getImageData(params));
  },
};
