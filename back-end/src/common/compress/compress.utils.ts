const compressing = require('compressing');

/**
 * GZIP压缩工具类
 * @author: jiangbin
 * @date: 2021-01-15 18:24:38
 **/
export const GzipUtils = {
  /**
   * 压缩文件
   * @param sPath 待压缩文件路径
   * @param tPath 压缩后文件路径
   * @author: jiangbin
   * @date: 2021-01-15 18:24:50
   **/
  compressFile: async (sPath: string, tPath: string) => {
    await compressing.gzip.compressFile(sPath, tPath);
  },
  /**
   * 解压缩文件
   * @param gzipPath gzip文件路径
   * @param tDirPath 解压到该目录
   * @author: jiangbin
   * @date: 2021-01-15 18:24:59
   **/
  uncompress: async (gzipPath: string, tDirPath: string) => {
    await compressing.gzip.uncompress(gzipPath, tDirPath);
  },
};

/**
 * ZIP压缩工具类
 * @author: jiangbin
 * @date: 2021-01-15 18:25:09
 **/
export const ZipUtils = {
  /**
   * zip压缩文件
   * @param sPath 待压缩的文件路径
   * @param zipPath 目标zip压缩文件路径
   * @author: jiangbin
   * @date: 2021-01-15 18:25:20
   **/
  compressFile: async (sPath: string, zipPath: string) => {
    await compressing.zip.compressFile(sPath, zipPath);
  },
  /**
   * zip压缩目录文件
   * @param sDirPath 待压缩的目录文件路径
   * @param zipPath 目标zip压缩文件路径
   * @author: jiangbin
   * @date: 2021-01-15 18:25:23
   **/
  compressDir: async (sDirPath: string, zipPath: string) => {
    await compressing.zip.compressDir(sDirPath, zipPath);
  },
  /**
   * 解压缩zip文件
   * @param zipPath zip压缩的文件路径
   * @param tDirPath 目标解压缩文件目录路径
   * @author: jiangbin
   * @date: 2021-01-15 18:25:26
   **/
  uncompress: async (zipPath: string, tDirPath: string) => {
    await compressing.zip.uncompress(zipPath, tDirPath);
  },
};
