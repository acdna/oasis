import { aop } from './aop.decorator';
import { PmsMapper } from '../pms/pms.mapper';

/**
 * 后端权限加载装饰器，刷新后端的权限映射表数据
 *
 * @author jiang
 * @date 2021-01-16 18:58:50
 **/
export function AopPmsLoader(sync = true) {
  return aop(sync, { afters: [PmsMapper.reload] });
}
