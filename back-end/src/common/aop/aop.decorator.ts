import { LogUtils } from '../log4js/log4js.utils';

/**
 * 回调方法参数类型
 * @author: jiangbin
 * @date: 2021-01-17 09:40:02
 **/
export interface AopCallbackType {
  /**
   * 被装饰函数引用
   * @author: jiangbin
   * @date: 2021-01-17 09:40:37
   **/
  origin: any;
  /**
   * 上一次前置切面函数、被装饰函数、后置切面函数的回调结果
   * @author: jiangbin
   * @date: 2021-01-17 09:40:52
   **/
  result: any;
  /**
   * 被装饰函数的传入参数，一般是个数组
   * @author: jiangbin
   * @date: 2021-01-17 09:41:54
   **/
  args: any;
}

/**
 * AOP回调参数类型定义
 *
 * @author jiang
 * @date 2021-01-23 09:57:45
 **/
export declare type AopCallbackParams = AopCallbackType | Partial<AopCallbackType>;

/**
 * 回调接口定义
 * @author: jiangbin
 * @date: 2021-01-17 09:40:05
 **/
export declare type AopCallback = (params?: AopCallbackParams) => any;

/**
 * 函数装饰器，允许在待装饰函数执行前后调用指定的切面函数，
 * 执行步骤：前置切面函数->被装饰函数->后置切面函数，
 * 前置和后置切面函数的参数格式如下：
 * <pre>
 * <code>callback(params?:{origin, result, args})</code>
 *  - params.origin 被装饰函数引用
 *  - params.result 上一个调用的执行结果，第一个前置函数调用时这个值为undefined,第二前置切面函数则为第一个前置函数的执行返回结果值，
 *            前置切面函数执行完的结果会传递到被装饰的方法中，存在重名参数会覆盖原参数值，被装饰方法执行完后的结果会传递给
 *            后置切面函数进行依次调用
 *  - params.args   被装饰函数传入的参数表
 *  若我们的切面业务函数不需要参数则可以空置，通过js方法的内置参数数组<code>arguments[0]</code>仍旧可以获取这个params参数对象
 * </pre>
 * @param sync 被装饰方法是同步还是异步方式函数，默认为true表示同步函数，在异步函数时一定要指定这个值，否则会产生调用逻辑错误
 * @param callbacks.befores 前置切面函数
 * @param callbacks.afters 后置切面函数
 * @param callbacks.params AOP装饰器传入的参数
 * @author jiang
 * @date 2021-01-16 18:07:59
 **/
export function aop(sync: boolean, callbacks: { befores?: AopCallback[]; afters?: AopCallback[] }, params?: Partial<any>) {
  /**
   * @param _target
   * @param name 被装饰方法的名称
   * @param descriptor <pre>被装饰的方法体描述对象，格式如下:
   * {
   *    value: [Function: actionb],
   *    writable: true,
   *    enumerable: false,
   *    configurable: true
   * }</pre>
   **/
  return (_target, name, descriptor) => {
    // 获取被装饰的函数
    const origin = descriptor.value;

    // 生成一个新函数，在待装饰函数前后分别调用装饰回调方法
    //同步AOP函数，同步执行方式中，前面执行的切面函数的结果会被几后传递
    let syncMethod = function (...args) {
      //注：args是待装饰函数传入的参数
      LogUtils.info(this.name, `函数(${name})开始同步模式执行AOP!`);

      //合并AOP传入的参数
      params && params.length > 0 && (args = { ...args, ...params });

      //执行前置的装饰函数
      let result = undefined;
      if (callbacks?.befores?.length > 0) {
        for (const callback of callbacks.befores) {
          try {
            result = callback({ origin: this, result, args }) || result;
          } catch (err) {
            LogUtils.error(this.name, `函数(${name})同步模式执行前置切面(${callback.name})失败!`, err);
            throw err;
          }
        }
      }

      //将前置装饰函数的处理结果合并到待装饰函数中,
      //若前置函数修改了待装饰函数参数值则会被更新到参数表中
      result && result.length > 0 && (args = { ...args, ...result });

      // 执行待装饰的函数,apply改变this的指向
      try {
        result = origin.apply(this, args);
      } catch (err) {
        LogUtils.error(this.name, `被装饰的函数(${name})执行失败!`, err);
        throw err;
      }

      //执行后置装饰函数
      if (callbacks?.afters?.length > 0) {
        for (const callback of callbacks.afters) {
          try {
            result = callback({ origin: this, result, args }) || result;
          } catch (err) {
            LogUtils.error(this.name, `函数(${name})同步模式执行后置切面(${callback.name})失败!`, err);
            throw err;
          }
        }
      }

      return result;
    };

    //异步AOP函数，异步执行时前置和后置切面函数均不会循环向后传递切面执行结果，
    //相当于一个勾子一样提供执行点而已
    let asyncMethod = async function (...args) {
      //注：args是待装饰函数传入的参数
      LogUtils.info(this.name, `(${name})函数开始异步模式执行AOP!`);

      //合并AOP传入的参数
      params && params.length > 0 && (args = { ...args, ...params });

      //执行前置的装饰函数
      let result = undefined;
      if (callbacks?.befores?.length > 0) {
        for (const callback of callbacks.befores) {
          try {
            result = (await callback({ origin: this, result, args })) || result;
          } catch (err) {
            LogUtils.error(this.name, `(${name})函数执行前置切面(${callback.name})失败!`, err);
            throw err;
          }
        }
      }

      //将前置装饰函数的处理结果合并到待装饰函数中,
      //若前置函数修改了待装饰函数参数值则会被更新到参数表中
      result && result.length > 0 && (args = { ...args, ...result });

      // 执行待装饰的函数,apply改变this的指向
      try {
        result = await origin.apply(this, args);
      } catch (err) {
        LogUtils.error(this.name, `被装饰函数(${name})执行失败!`, err);
        throw err;
      }

      //执行后置装饰函数
      if (callbacks?.afters?.length > 0) {
        for (const callback of callbacks.afters) {
          try {
            result = (await callback({ origin: this, result, args })) || result;
          } catch (err) {
            LogUtils.error(this.name, `(${name})函数执行后置切面(${callback.name})失败!`, err);
            throw err;
          }
        }
      }

      return result;
    };

    //判定使用同步还是异步方式
    descriptor.value = sync ? syncMethod : asyncMethod;

    return descriptor;
  };
}
