import { aop } from './aop.decorator';
import { UserMapper } from '../../sys/user/user.mapper';

/**
 * 用户加载装饰器，刷新后端的用户映射表数据
 *
 * @author jiang
 * @date 2021-01-16 18:58:50
 **/
export function AopUserLoader(sync = true) {
  return aop(sync, { afters: [UserMapper.reload] });
}
