import { aop } from './aop.decorator';
import { DictMapper } from '../../sys/dictionary/dict.mapper';

/**
 * 字典加载装饰器，刷新后端的字典映射表数据
 *
 * @author jiang
 * @date 2021-01-16 18:58:50
 **/
export function AopDictLoader(sync = true) {
  return aop(sync, { afters: [DictMapper.reload] });
}
