import { RequestContext } from '@medibloc/nestjs-request-context';
import { SysUser } from '../../sys/entity/sys.user.entity';
import { MESSAGE_CODE } from '../constants/msg.code';

/**
 * 系统请求上下文对象
 * @author: jiangbin
 * @date: 2021-01-18 22:16:18
 **/
export class SysRequestContext extends RequestContext {
  //用户信息
  user: SysUser;

  //请求过程中产生的一些消息
  messages = new Map<MESSAGE_CODE, any>();
}
