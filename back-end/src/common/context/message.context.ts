import { MESSAGE_CODE } from '../constants/msg.code';
import { SysRequestContext } from './sys.request.context';

/**
 * 消息上下文信息对象，用户可以向该上下文对象添加自己的消息，
 * 以便在任意其它地方读取这些消息，这些消息是绑定用户当前请求线程的
 * @author: jiangbin
 * @date: 2021-01-07 10:06:11
 **/
export const MessageContext = {
  /**
   * 获取消息上下文对象
   * @author: jiangbin
   * @date: 2021-02-07 10:05:15
   **/
  get context(): SysRequestContext {
    return SysRequestContext.get();
  },

  /**
   * 添加消息
   * @author: jiangbin
   * @date: 2021-02-07 11:55:09
   **/
  add(code: MESSAGE_CODE, message: any) {
    this.context.messages.set(code, message);
  },

  /**
   * 消息列表是否为空
   * @return true/false--无消息/有消息
   * @author: jiangbin
   * @date: 2021-02-07 11:54:41
   **/
  empty() {
    return this.context.messages.size == 0;
  },

  /**
   * 获取指定key值的消息
   * @author: jiangbin
   * @date: 2021-02-07 10:07:58
   **/
  get(code: MESSAGE_CODE) {
    return this.context[code] || '';
  },

  /**
   * 获取所有消息列表
   * @author: jiangbin
   * @date: 2021-02-07 11:54:26
   **/
  get all(): Map<MESSAGE_CODE, any> {
    return this.context.messages;
  },

  /**
   * 清空消息列表
   * @author: jiangbin
   * @date: 2021-02-07 11:56:40
   **/
  clear() {
    this.context.messages.clear();
  },

  /**
   * 获取消息总数
   * @author: jiangbin
   * @date: 2021-02-07 11:57:06
   **/
  size(): number {
    return this.context.messages.size;
  },
};
