import { RequestContext } from '@medibloc/nestjs-request-context';
import { MESSAGE_CODE } from '../constants/msg.code';

/**
 * 系统消息的上下文对象，用户可以在任意程序中添加消息，
 * 这样可以通过该上下文对象获取多个消息列表，并且在需要时获取消息
 * @author: jiangbin
 * @date: 2021-01-18 22:16:18
 **/
export class MessageDetails extends RequestContext {
  messages = new Map<MESSAGE_CODE, any>();
}
