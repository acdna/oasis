/**
 * 树节点实现类
 * @author jiang
 * @date 2020-12-26 22:42:19
 **/
export class TreeNode {
  /**
   * 节点ID
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  id: string;

  /**
   * 节点序号
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  order: number;

  /**
   * 父节点ID
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  parentId: string;

  /**
   * 子节点列表
   * @author jiang
   * @date 2020-12-26 23:18:27
   **/
  children?: TreeNode[];
}
