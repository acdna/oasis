import { Expose } from 'class-transformer';

/**
 * Sheet数据对象
 * @author jiang
 * @date 2020-12-07 21:51:07
 **/
export class SheetData {
  /**
   * sheet索引序号
   *
   * @author jiang
   * @date 2020-12-07 21:50:37
   **/
  index?: number;
  /**
   * sheet名称
   *
   * @author jiang
   * @date 2020-12-07 21:50:37
   **/
  @Expose()
  name: string;
  /**
   * sheet全部行数据，包含标题行和数据行
   *
   * @author jiang
   * @date 2020-12-07 21:50:37
   **/
  @Expose()
  data: Array<string[]>;
  /**
   * Excel操作参数，例如控制合并单元格等参数，参看：https://github.com/mgcrea/node-xlsx
   * @author jiang
   * @date 2020-12-07 22:13:47
   **/
  options?: any;
  /**
   * sheet总列数
   *
   * @author jiang
   * @date 2020-12-07 21:50:37
   **/
  colCount?: number;
  /**
   * sheet总行数
   *
   * @author jiang
   * @date 2020-12-07 21:50:37
   **/
  rowCount?: number;
}

/**
 * Sheet数据类型定义
 * @author: jiangbin
 * @date: 2021-02-19 13:49:43
 **/
export declare type SheetDataType = SheetData | Partial<SheetData>;
