import { SheetData } from './sheet.data';
import { SpliterUtils } from '../../utils/spliter.utils';
import { LocaleContext } from '../../locales/locale';
import { ExcelTitle } from './excel.title';

/**
 * Excel行数据渲染方法类型
 * @param params.titles 标题行
 * @param params.row 行数据
 * @return 构建的单行列数据 [string]
 * @author: jiangbin
 * @date: 2021-03-25 08:30:09
 **/
export declare type ExcelRowRender<S> = (params: { titles: string[]; row: S }) => string[];

/**
 * Excel创建器接口
 * @author: jiangbin
 * @date: 2021-03-25 08:29:43
 **/
export interface IExcelCreator<S> {
  /**
   * 创建Excel
   * @param params.sheetName sheet名
   * @param params.title 标题行
   * @param params.rows 行数据
   * @param params.render 行渲染器
   * @return
   * @author: jiangbin
   * @date: 2021-03-25 08:28:54
   **/
  create(params: { title: ExcelTitle; rows: S[]; render: ExcelRowRender<S> }): SheetData[];
}

/**
 * Excel创建器实现类
 * @author: jiangbin
 * @date: 2021-03-25 08:29:56
 **/
export class ExcelCreator<S> implements IExcelCreator<S> {
  /**
   * 创建Excel
   * @param params.sheetName sheet名-{ExcelSheetName}
   * @param params.title 标题行-{ExcelTitle}
   * @param params.rows 行数据-[any]
   * @param params.render 行渲染器{ExcelRowRender<S>}
   * @return
   * @author: jiangbin
   * @date: 2021-03-25 08:28:54
   **/
  create(params: { title: ExcelTitle; rows: S[]; render: ExcelRowRender<S> }): SheetData[] {
    let { rows, title, render } = params;
    let groups = SpliterUtils.group(rows, 60000);
    let count = groups.length;
    let sheets = new Array<SheetData>();
    let cSheetName = LocaleContext.message(title.sheetName);
    let cTitles = LocaleContext.data<string[]>(title.title);
    groups.forEach((group, index) => {
      let sheet = new SheetData();
      let data = [];
      data.push(cTitles);
      sheet.index = index + 1;
      sheet.name = count > 0 ? `${cSheetName}${index + 1}` : '1';
      let rows = group.map((row) => {
        return render({ titles: cTitles, row });
      });
      if (rows?.length > 0) {
        data = data.concat(rows);
      }
      sheet.data = data;
      sheet.colCount = cTitles?.length || 0;
      sheet.rowCount = group?.length || 0;
      sheets.push(sheet);
    });
    return sheets;
  }
}
