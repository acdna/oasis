import { SheetData, SheetDataType } from './sheet.data';

/**
 * Excel类，主要是包装了node-xlsx模块的功能，以适应大多数常规操作环境，
 * 如果本类功能不适应需要也可以参考类中的代码自行操作Excel完成更加复杂的效果
 * @author jiang
 * @date 2020-12-07 22:09:52
 **/
export interface IExcel {
  /**
   * 解析Excel文件
   * @param path
   * @return { SheetData[] }
   * @author jiang
   * @date 2020-12-07 22:06:18
   **/
  parse(path): SheetDataType[];

  /**
   * 解析Sheet信息
   * @param sheet sheet对象
   * @param index sheet的索引序号
   * @return { SheetData }
   * @author jiang
   * @date 2020-12-07 22:33:18
   **/
  parseSheet(sheet, index: number): SheetDataType;

  /**
   * 保存数据到Excel文件中
   * @param sheets sheet数据数组
   * @param path excel文件路径
   * @param options excel构建参数，例如控制列宽，颜色，合并单元格等，参看：https://www.npmjs.com/package/xlsx-style
   * @return
   * @author jiang
   * @date 2020-12-07 22:02:55
   **/
  write(sheets: SheetDataType[], path: string, options?);

  /**
   * 创建Sheet列表，会自动按给定属性
   *
   * @param sheetName sheet名，若存在多个sheet会自动加序号进行区分
   * @param titles 标题行数据
   * @param keys 转换数据时数据对象属性名顺序
   * @param objs 数据对象列表
   * @param pageSize <pre>分页记录数，即每个sheet最多写入的记录数，默认按照Excel2007的最大限制数：
   *          Excel2003版最大行数是65536行
   *          Excel2007开始的版本最大行数是1048576行</pre>
   * @return
   * @author jiang
   * @date 2020-12-08 00:00:08
   **/
  createSheets(sheetName: string, titles: string[], keys: string[], objs: any[], pageSize?: number): SheetDataType[];

  /**
   * 转换数据对象为了行Excel数据
   * @param keys 数据属性转换顺序
   * @param obj 被转换的数据对象
   * @return
   * @author jiang
   * @date 2020-12-07 23:20:03
   **/
  toRow(keys: string[], obj: any): any[];

  /**
   * 转换数据对象列表为了行Excel数据
   * @param keys 数据属性转换顺序
   * @param objs 被转换的数据对象列表
   * @return
   * @author jiang
   * @date 2020-12-07 23:23:30
   **/
  toRows(keys: string[], objs: any[]): any[];

  /**
   * 添加一个Sheet
   * @param sheet
   * @return
   * @author jiang
   * @date 2020-12-07 23:13:13
   **/
  addSheet(sheet: SheetDataType, sheets: SheetDataType[]);

  /**
   * 为各个sheet添加标题行数据
   * @param titles 标题行数据
   * @return
   * @author jiang
   * @date 2020-12-07 23:12:06
   **/
  setTitles(titles: string[], sheets: SheetDataType[]);
}
