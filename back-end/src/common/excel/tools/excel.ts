import { SheetData, SheetDataType } from './sheet.data';
import { SpliterUtils } from '../../utils/spliter.utils';
import { IExcel } from './i.excel';

const xlsx = require('node-xlsx');
const fs = require('fs');

/**
 * Excel类，主要是包装了node-xlsx模块的功能，以适应大多数常规操作环境，
 * 如果本类功能不适应需要也可以参考类中的代码自行操作Excel完成更加复杂的效果
 * @author jiang
 * @date 2020-12-07 22:09:52
 **/
export class Excel implements IExcel {
  /**
   * 解析Excel文件
   * @param path
   * @return { SheetData[] }
   * @author jiang
   * @date 2020-12-07 22:06:18
   **/
  parse(path): SheetDataType[] {
    const sheets = xlsx.parse(path);
    if (!sheets || sheets.length == 0) return;
    const dataList = [];
    sheets.forEach((sheet, index) => {
      const data = this.parseSheet(sheet, index);
      if (data) {
        dataList.push(data);
      }
    });
    return dataList;
  }

  /**
   * 解析Sheet信息
   * @param sheet sheet对象
   * @param index sheet的索引序号
   * @return { SheetData }
   * @author jiang
   * @date 2020-12-07 22:33:18
   **/
  parseSheet(sheet, index: number): SheetDataType {
    if (!sheet || sheet.length == 0) return;
    const rows = [];
    let maxColIndex = 0;
    let maxRowIndex = 0;
    sheet.data.forEach((row, rowIndex) => {
      if (maxRowIndex < rowIndex) maxRowIndex = rowIndex;
      const cells = [];
      row.forEach((col, colIndex) => {
        if (maxColIndex < colIndex) maxColIndex = colIndex;
        cells.push(col);
      });
      rows.push(cells);
    });

    if (maxColIndex == 0 || maxRowIndex == 0) return;

    //创建sheet数据对象
    const data = new SheetData();
    data.index = index;
    data.name = sheet.name;
    data.data = rows;
    data.rowCount = maxRowIndex + 1;
    data.colCount = maxColIndex + 1;
    return data;
  }

  /**
   * 保存数据到Excel文件中
   * @param sheets sheet数据数组
   * @param path excel文件路径
   * @param options excel构建参数，例如控制列宽，颜色，合并单元格等
   * @return
   * @author jiang
   * @date 2020-12-07 22:02:55
   **/
  write(sheets: SheetDataType[], path: string, options?) {
    if (!path || path.length == 0) return;
    if (path.endsWith('.xls')) {
      const index = path.lastIndexOf('.xls');
      path = index > -1 ? `${path.substr(0, index)}.xlsx` : path;
    }
    const buffer = xlsx.build(sheets, options);
    fs.writeFileSync(path, buffer, { flag: 'w' });
  }

  /**
   * 创建Sheet列表，会自动按给定属性
   *
   * @param sheetName sheet名，若存在多个sheet会自动加序号进行区分
   * @param titles 标题行数据
   * @param keys 转换数据时数据对象属性名顺序
   * @param objs 数据对象列表
   * @param pageSize <pre>分页记录数，即每个sheet最多写入的记录数，默认按照Excel2007的最大限制数：
   *          Excel2003版最大行数是65536行
   *          Excel2007开始的版本最大行数是1048576行</pre>
   * @return
   * @author jiang
   * @date 2020-12-08 00:00:08
   **/
  createSheets(sheetName: string, titles: string[], keys: string[], objs: any[], pageSize = 1048576): SheetDataType[] {
    if (!sheetName || !titles || titles.length == 0 || !keys || keys.length == 0 || !objs || objs.length == 0 || titles.length != keys.length) return;
    const rows = this.toRows(keys, objs);
    if (!rows || rows.length == 0) return;
    const items = SpliterUtils.split(rows, pageSize);
    const sheets = [];
    items.forEach((item, index) => {
      const sheet = new SheetData();
      sheet.index = index;
      if (items.length == 1) {
        sheet.name = sheetName;
      } else {
        sheet.name = `${sheetName}${index + 1}`;
      }
      //获取sheet数据
      const sheetData = rows.slice(item.startIndex, item.endIndex);
      sheet.data = sheetData;
      sheet.rowCount = sheetData.length;
      sheet.colCount = titles.length;
      sheets.push(sheet);
    });
    //插入标题行
    this.setTitles(titles, sheets);
    return sheets;
  }

  /**
   * 转换数据对象为了行Excel数据
   * @param keys 数据属性转换顺序
   * @param obj 被转换的数据对象
   * @return
   * @author jiang
   * @date 2020-12-07 23:20:03
   **/
  toRow(keys: string[], obj: any): any[] {
    if (!keys || keys.length == 0 || !obj) return;
    const cells = [];
    keys.forEach((key, index) => {
      cells.push(obj[key]);
    });
    return cells;
  }

  /**
   * 转换数据对象列表为了行Excel数据
   * @param keys 数据属性转换顺序
   * @param objs 被转换的数据对象列表
   * @return
   * @author jiang
   * @date 2020-12-07 23:23:30
   **/
  toRows(keys: string[], objs: any[]): any[] {
    const rows = [];
    objs.forEach((obj) => {
      if (!obj) return;
      const cells = this.toRow(keys, obj);
      if (cells) rows.push(cells);
    });
    return rows;
  }

  /**
   * 添加一个Sheet
   * @param sheet
   * @return
   * @author jiang
   * @date 2020-12-07 23:13:13
   **/
  addSheet(sheet: SheetDataType, sheets: SheetDataType[]) {
    sheet.index = sheets.length || 0;
    sheets.push(sheet);
  }

  /**
   * 为各个sheet添加标题行数据
   * @param titles 标题行数据
   * @return
   * @author jiang
   * @date 2020-12-07 23:12:06
   **/
  setTitles(titles: string[], sheets: SheetDataType[]) {
    if (!titles || titles.length == 0) return;
    sheets.forEach((sheet, index) => {
      sheet.data.splice(0, 0, titles);
      sheet.rowCount = sheet.data.length;
      sheet.colCount = titles.length;
    });
  }
}
