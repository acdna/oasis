import { SheetDataType } from './sheet.data';
import { FileUtils } from '../../utils/file.utils';
import { LocaleContext, LocaleDataArray } from '../../locales/locale';
import { TitleUtils } from '../../utils/title.utils';
import { PathUtils } from '../../path/path.creator.utils';
import { PathResult } from '../../path/path.creator';
import { ArrayUtils } from '../../utils/array.utils';

const xlsx = require('node-xlsx');
const fs = require('fs');

/**
 * Excel的一些工具方法
 * @author: jiangbin
 * @date: 2021-03-24 17:47:27
 **/
export const ExcelUtils = {
  /**
   * 保存数据到Excel文件中
   * @param sheets sheet数据数组
   * @param path excel文件路径
   * @param options excel构建参数，例如控制列宽，颜色，合并单元格等
   * @return
   * @author jiang
   * @date 2020-12-07 22:02:55
   **/
  write(sheets: SheetDataType[], path: string, options?) {
    if (!path || path.length == 0) return;
    if (path.endsWith('.xls')) {
      const index = path.lastIndexOf('.xls');
      path = index > -1 ? `${path.substr(0, index)}.xlsx` : path;
    }
    FileUtils.createDir(path);
    const buffer = xlsx.build(sheets, options);
    fs.writeFileSync(path, buffer, { flag: 'w' });
  },

  /**
   * 添加一个Sheet
   * @param sheet
   * @return
   * @author jiang
   * @date 2020-12-07 23:13:13
   **/
  addSheet(sheet: SheetDataType, sheets: SheetDataType[]) {
    sheet.index = sheets?.length || 0;
    sheet && sheets.push(sheet);
  },

  /**
   * 获取所有sheet中包含的数据
   * @author: jiangbin
   * @date: 2021-02-19 13:56:17
   **/
  getAllRows(sheets: SheetDataType[]) {
    let all = [];
    sheets.forEach((sheet) => {
      let rows = ExcelUtils.getRows(sheet);
      rows?.length > 0 && (all = all.concat(rows));
    });
    return all;
  },

  /**
   * 过滤出与给定标题行数据相符的sheet列表
   * @param defaults 默认标题行
   * @param sheets sheet数据列表
   * @return 符合默认标题行定义的sheet
   * @author: jiangbin
   * @date: 2021-03-25 17:32:49
   **/
  filter: (defaults: LocaleDataArray<string>, sheets: SheetDataType[]) => {
    let titles = LocaleContext.data(defaults);
    return sheets.filter((sheet) => {
      return TitleUtils.check(titles, ExcelUtils.getTitle(sheet))['status'];
    });
  },

  /**
   * 保存Excel数据到通用目录中，扩展名采用xlsx
   *
   * @param sheets
   * @return
   * @author jiang
   * @date 2021-03-27 13:32:09
   **/
  save(sheets: SheetDataType[]): PathResult {
    if (ArrayUtils.isEmpty(sheets)) {
      return null;
    }

    let pathResult = PathUtils.getCommonPath({ fileExt: 'xlsx' });
    ExcelUtils.write(sheets, pathResult.path);

    return pathResult;
  },

  /**
   * 获取标题行
   * @author: jiangbin
   * @date: 2021-02-19 13:44:52
   **/
  getTitle(sheet: SheetDataType) {
    return sheet?.data[0] || undefined;
  },

  /**
   * 获取数据行
   * @author: jiangbin
   * @date: 2021-02-19 13:45:00
   **/
  getRows(sheet: SheetDataType) {
    return sheet?.data?.splice(1) || undefined;
  },
};
