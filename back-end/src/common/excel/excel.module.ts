import { forwardRef, Global, Module } from '@nestjs/common';
import { SysModule } from '../../sys/sys.module';
import { ExcelController } from './controller/excel.controller';

@Global()
@Module({
  imports: [forwardRef(() => SysModule)],
  controllers: [ExcelController],
  providers: [],
  exports: [],
})
export class ExcelModule {}
