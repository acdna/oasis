/**
 * Excel文件模板列表
 * @author: jiangbin
 * @date: 2021-02-03 12:21:40
 **/
export const EXCEL_TEMPLATES = {
  onp_statistics: {
    cn: 'onp/模板-20201015.xlsx',
    en: 'onp/StatisticsTemplate-20201015.xlsx',
  },
};
