import * as fs from 'fs';
import * as path from 'path';
import * as readline from 'readline';
import { LogUtils } from '../log4js/log4js.utils';

/**
 * 文件工具类
 * @Date 2020/12/5 下午6:56:17
 * @author jiangbin
 * @export
 * @class FileUtils
 */
export const FileUtils = {
  /**
   * 递归删除目录及目录包含的子文件
   * @param file 目录路径
   */
  deleteDir: (file: string) => {
    const arr = fs.readdirSync(file);
    for (const i in arr) {
      //读取文件信息，以便于判断是否是一个文件或目录
      const stats = fs.statSync(file + '/' + arr[i]);

      if (stats.isFile()) {
        //判断为真，是文件则执行删除文件
        fs.unlinkSync(file + '/' + arr[i]);
      } else {
        //判断为假就是文件夹，就调用自己，递归的入口
        FileUtils.deleteDir(file + '/' + arr[i]);
      }
    }
    //删除空目录
    fs.rmdirSync(file);
  },

  /**
   * 获取文件的上级目录路径
   * @param filePath 文件路径
   * @author: jiangbin
   * @date: 2021-01-14 09:59:42
   **/
  getParentPath: (filePath: string) => {
    return path.dirname(filePath);
  },

  /**
   * 同步方式获取给定文件路径的目录并递归创建之
   * @param filePath 文件路径
   * @return {string} 创建的目录路径
   * @author: jiangbin
   * @date: 2021-01-14 09:58:34
   **/
  createDir: (filePath: string) => {
    const dir = FileUtils.getParentPath(filePath);
    return fs.mkdirSync(dir, { recursive: true });
  },

  /**
   * 创建文件并同步写文件内容，会自动递归创建文件目录，以'utf-8'编码写文件
   * @param filePath 文件路径
   * @param content 文件内容
   * @return {boolean} true/false-操作成功/失败
   * @author: jiangbin
   * @date: 2021-01-14 10:19:56
   **/
  saveFile: (filePath: string, content: any) => {
    if (!filePath || filePath.length == 0 || !content || content.length == 0) return false;
    FileUtils.createDir(filePath);
    fs.writeFileSync(filePath, content);
    return true;
  },

  /**
   * 获取文件扩展名
   * @param filePath 文件路径
   * @return {string} 扩展名，例如：xlsx，若无扩展名则返回空字符串
   * @author: jiangbin
   * @date: 2021-01-14 12:53:55
   **/
  getFileExt: (filePath: string) => {
    if (!filePath || filePath.length == 0) return '';
    return path.extname(filePath);
  },

  /**
   * 获取文件路径中包含的文件名，例如：/common/2021/01/17/样品信息表.docx==>样品信息表.docx
   * @param filePath 文件路径
   * @return 文件名，带扩展名
   * @author: jiangbin
   * @date: 2021-01-17 16:25:41
   **/
  getFileName: (filePath: string) => {
    if (!filePath || filePath.length == 0) return '';
    filePath = filePath.replace(/\\/g, '/');
    if (filePath.indexOf('/') == -1) return filePath;
    if (filePath.endsWith('/')) {
      filePath = filePath.substring(0, filePath.length - 1);
    }
    return filePath.substring(filePath.lastIndexOf('/') + 1);
  },

  /**
   * 获取文件路径中包含的简单文件名，例如：/common/2021/01/17/样品信息表.docx==>样品信息表
   * @param filePath 文件路径
   * @return 文件名，不带扩展名
   * @author: jiangbin
   * @date: 2021-01-17 16:25:41
   **/
  getSimpleFileName: (filePath: string) => {
    let fileName = FileUtils.getFileName(filePath);
    if (!fileName || fileName.length == 0 || fileName.lastIndexOf('.') == -1) return fileName;
    return fileName.substring(0, fileName.lastIndexOf('.'));
  },

  /**
   * 按行读取数据
   * @param path 文件路径
   * @param callback.line 数据读取完毕时用来处理数据的回调函数，例如解析和写数据库操作
   * @param callback.close 关闭文件事件的回调函数
   * @author: jiangbin
   * @date: 2020-12-17 18:13:03
   **/
  readLine: (path: string | Buffer | URL, callback: { line: (string, []) => any; close?: (err, []) => void; results: any[] }) => {
    const start = Date.now();

    //创建读取流对象
    let stream = fs.createReadStream(path, { autoClose: true, encoding: 'utf-8' });

    //创建行读取接口
    let reader = readline.createInterface({ input: stream });

    //按行读取数据事件
    reader.on('line', (line: string) => {
      let result = callback.line(line, callback.results);
      if (result) {
        callback.results.push(result);
      }
    });

    //关闭事件
    reader.on('close', (err) => {
      !!callback && callback.close(err, callback.results);
      if (err) {
        LogUtils.error('FileUtils', '关闭读取流失败!', err);
      } else {
        LogUtils.info('FileUtils', `读取文件成功，共耗时==>${(Date.now() - start) / 1000} s`);
      }
    });
  },
};
