import { LogUtils } from '../log4js/log4js.utils';

/**
 * Excel、CSV等的标题行数据验证工具类
 * @author zhengenze
 * @date 2021/3/24 5:27 下午
 */
export const TitleUtils = {
  /**
   * <pre>检测给定默认列名和数据文件中的列名是否一致：
   * 1、若不一致则返回具体不匹配的列信息：{ status: false, index, default: dcol, col: scol }
   * 2、若一致则直接返回true</pre>
   * @param defaults 默认定义的列名
   * @param targets 数据文件中的列名
   * @return
   * @author: jiangbin
   * @date: 2021-03-25 17:03:14
   **/
  check: (
    defaults: string[],
    targets: string[],
  ): {
    status: boolean;
    message?: string;
    index?: number;
    default?: string;
    col?: string;
  } => {
    let category = 'TitleUtils';
    if (!defaults || defaults.length == 0) {
      LogUtils.error(category, `默认标题未给定!`);
      return { status: true, message: '默认标题未给定!' };
    }
    LogUtils.info(category, `待检测标题与默认标题分别为=>${JSON.stringify(targets)}==>${JSON.stringify(defaults)}!`);
    if (!targets || defaults.length != targets.length) {
      LogUtils.error(category, `待检测标题未给定或与默认标题长度不符=>${JSON.stringify(targets)}==>${JSON.stringify(defaults)}!`);
      return { status: false, message: '待检测标题未给定或与默认标题长度不符!' };
    }
    for (let index = 0; index < defaults.length; index++) {
      let dcol = defaults[index];
      let scol = targets[index];
      if (dcol !== scol) {
        LogUtils.error(category, `第${index + 1}列标题不正确:模板'${dcol}',实际'${scol}'!`);
        return {
          status: false,
          index, //不匹配的列索引号
          default: dcol, //不匹配列在模板中定义的列名
          col: scol, //不匹配列在给定数据文件中的定义的列名
        };
      }
    }
    LogUtils.info(category, `待检测标题未给定或与默认标题匹配成功!`);
    return { status: true };
  },
};
