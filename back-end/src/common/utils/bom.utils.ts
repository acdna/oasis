/**
 * BOM是用来判断文本文件是哪一种Unicode编码的标记，其本身是一个Unicode字符（"\uFEFF"），位于文本文件头部。
 * 在不同的Unicode编码中，对应的bom的二进制字节如下：
 * Bytes Encoding
 * FE FF UTF16BE
 * FF FE UTF16LE
 * EF BB BF UTF8
 * 所以我们可以根据文件头部的几个字节和上面的表格对应来判断该文件是哪种编码形式。
 * 在Windows中，如果未加BOM信息，则类似于csv文件中的中文可能会显示成乱码。
 * @author: jiangbin
 * @date: 2021-03-15 16:26:17
 **/
export const BOMUtils = {
  UTF_8: { charsetName: 'UTF-8', bytes: [239, 187, 191] },
  UTF_16BE: { charsetName: 'UTF-16BE', bytes: [254, 255] },
  UTF_16LE: { charsetName: 'UTF-16LE', bytes: [255, 254] },
  UTF_32BE: { charsetName: 'UTF-32BE', bytes: [0, 0, 254, 255] },
  UTF_32LE: { charsetName: 'UTF-32LE', bytes: [255, 254, 0, 0] },
  /**
   * 添加编码头信息
   * @param str 待添加BOM信息的字符串
   * @param bom BOM头信息，默认为UTF-8编码
   * @author: jiangbin
   * @date: 2021-03-15 16:25:11
   **/
  addBOM: (str: string, bom = BOMUtils.UTF_8): string => {
    return bom.bytes.toString() + str;
  },
};
