/**
 * 数组分割工具类
 * @author jiang
 * @date 2020-12-07 23:51:11
 **/
export const SpliterUtils = {
  /**
   * 按照给定步进值分割数组元素
   * @param sources 待分割的数组
   * @param step 步进值，分割元素数
   * @return { any[{ startIndex: number, endIndex: number }] }
   * @author jiang
   * @date 2020-12-07 23:49:51
   **/
  split(sources: any[], step: number): any[] {
    if (!sources || sources.length == 0) return;
    if (step == null || step >= sources.length || step < 1) {
      return [{ startIndex: 0, endIndex: sources.length }];
    }
    let targets = [];
    let fromIndex = 0;
    while (true) {
      let endIndex = fromIndex + step;
      if (endIndex >= sources.length) {
        endIndex = sources.length;
      }
      targets.push({ startIndex: fromIndex, endIndex: endIndex });
      fromIndex = endIndex;
      if (fromIndex >= sources.length) {
        break;
      }
    }
    return targets;
  },

  /**
   * 将数据按给定的步进值分组成二维数组
   * @param sources 待分组数据列表
   * @param step 步进值，分割元素数
   * @return {any[]}
   * @author: jiangbin
   * @date: 2021-01-01 15:49:15
   **/
  group(sources: any[], step: number): any[] {
    let arrs = SpliterUtils.split(sources, step);
    if (arrs?.length == 0) return;
    let targets = [];
    for (let i = 0; i < arrs.length; i++) {
      let row = [];
      const { startIndex, endIndex } = arrs[i];
      for (let j = startIndex; j < endIndex; j++) {
        row.push(sources[j]);
      }
      targets.push(row);
    }
    return targets;
  },
};
