/**
 * 孔位位置
 * @author: jiangbin
 * @date: 2021-04-07 16:19:27
 **/
export interface Plate {
  /**
   * 总行数，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 14:59:59
   **/
  rowCount: number;
  /**
   * 总列数，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 15:00:01
   **/
  colCount: number;
}

/**
 * 电泳板规格
 * @author: jiangbin
 * @date: 2021-04-07 16:32:18
 **/
export const PlateSpecs = {
  P96: { rowCount: 8, colCount: 12 },
  P384: { rowCount: 8 * 2, colCount: 12 * 2 },
  P1536: { rowCount: 8 * 4, colCount: 12 * 4 },
};

/**
 * 孔位位置
 * @author: jiangbin
 * @date: 2021-04-07 16:19:27
 **/
export interface WellPosition {
  /**
   * 行顺序号，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 14:59:59
   **/
  row: number;
  /**
   * 列顺序号，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 15:00:01
   **/
  col: number;
}

/**
 * 孔位信息对象
 * @author: jiangbin
 * @date: 2021-03-18 14:23:15
 **/
export interface Well extends WellPosition {
  /**
   * 行号名，如A01孔位号中的A
   * @author: jiangbin
   * @date: 2021-03-18 14:59:56
   **/
  rowName: string;
}

/**
 * 孔位号工具类，用于解析孔位号的行和列信息
 * @author: jiangbin
 * @date: 2021-03-18 14:07:33
 **/
export const WellUtils = {
  /**
   * 孔位号正则式
   * @author: jiangbin
   * @date: 2021-03-18 14:07:53
   **/
  pattern: /^([a-zA-Z]+)([0-9]{1,2})$/,
  /**
   * 解析孔位号
   * @param well 孔位号字符串，如:A1或A01
   * @return 孔位信息
   * @author: jiangbin
   * @date: 2021-03-18 14:07:56
   **/
  parse(well: string): Well | null {
    let matchs = well.match(this.pattern);
    if (matchs) {
      let rowName = matchs[1];
      let row = this.getRowNum(rowName);
      return { rowName, row, col: parseInt(matchs[2], 10) };
    }
    return null;
  },
  /**
   * 获取行号名称
   * @param row
   * @return
   * @author: jiangbin
   * @date: 2021-03-18 15:09:43
   **/
  getRowName(row: number): string {
    return String.fromCharCode(row + 'A'.charCodeAt(0) - 1);
  },
  /**
   * 获取行顺序号
   * @param rowName
   * @return
   * @author: jiangbin
   * @date: 2021-03-18 15:09:48
   **/
  getRowNum(rowName: string): number {
    return rowName.charCodeAt(0) - 'A'.charCodeAt(0) + 1;
  },
  /**
   * 格式化成孔位号字符串
   * @param well 孔位信息
   * @param doubleCol true/false-是否为双列列号，例如：A01，默认为双列的
   * @return 格式化后的孔位号字符串，例如:A1或A01
   * @author: jiangbin
   * @date: 2021-03-18 14:16:59
   **/
  format(well: { row: number; col: number }, doubleCol = true): string {
    return doubleCol ? `${this.getRowName(well.row) || ''}${well?.col?.toString().padStart(2, '0') || ''}` : `${this.getRowName(well.row) || ''}${well?.col || ''}`;
  },

  /**
   * 获取指定孔位序号对应的孔位名称，例如：孔位序号为2对应孔位号为A02
   * @param plate 板规格
   * @param wellOrder 板序号
   * @return
   * @author: jiangbin
   * @date: 2021-04-07 16:18:09
   **/
  getWellName(plate: Plate | Required<Plate>, wellOrder: number): string {
    let row = (wellOrder - 1) / plate.colCount + 1;
    let col = ((wellOrder - 1) % plate.colCount) + 1;
    return this.format({ row, col });
  },
};
