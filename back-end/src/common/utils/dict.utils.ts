import {LocaleContext} from "../locales/locale";

/**
 * 字典工具类，用于提取字典定义中的字典项值列表和字典项名列表
 * @author: jiangbin
 * @date: 2021-04-12 16:25:44
 **/
export const DictUtils = {
  /**
   * 获取值列表
   * @author: jiangbin
   * @date: 2021-04-12 16:25:55
   **/
  values: (dict: any) => {
    return Object.keys(dict)?.map(key => {
      return dict[key]?.value;
    });
  },

  /**
   * 获取值列表
   * @author: jiangbin
   * @date: 2021-04-12 16:25:55
   **/
  names: (dict: any) => {
    return Object.keys(dict)?.map((key) => {
      return LocaleContext.message({cn: dict[key]?.nameCn, en: dict[key]?.nameEn});
    });
  },
};