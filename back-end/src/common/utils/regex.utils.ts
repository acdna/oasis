/**
 * 正则式工具类
 * @author: jiangbin
 * @date: 2021-03-26 12:32:17
 **/
export const RegexUtils = {
  /**
   * 匹配正则式，提取匹配项列表
   * @param regex
   * @param source
   * @return
   * @author: jiangbin
   * @date: 2021-03-26 12:32:25
   **/
  match: (regex: any, source: string) => {
    if (!regex || !source || source.length === 0) {
      return null;
    }
    let items = regex.exec(source);
    if (!items || items.length < 2) {
      return null;
    }
    let results = new Array<string>();
    for (let index = 1; index < items.length; index++) {
      results.push(items[index]);
    }
    return results;
  },

  /**
   * 匹配正则式，提取匹配项的第一项
   * @param regex
   * @param source
   * @return
   * @author: jiangbin
   * @date: 2021-03-26 12:32:25
   **/
  first: (regex: any, source: string) => {
    let items = RegexUtils.match(regex, source);
    return items?.length > 0 ? items[0] : null;
  },
};
