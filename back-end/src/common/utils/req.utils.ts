/**
 * 获取请求的URL地址
 * @author: jiangbin
 * @date: 2021-01-19 09:45:39
 **/
export const getReqUrl = (req) => {
  return `${req.protocol}://${req.ip || 'localhost'}:${req.port || 3000}${req.url}`;
};
