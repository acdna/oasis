import { Controller, Get, NotFoundException, Query, Res } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import * as fs from 'fs';
import { BaseController } from '../../base/controller/base.controller';
import { PathUtils } from '../../path/path.creator.utils';
import { SysServices } from '../../../sys/sys.services';

/**
 * @description 文件下载控制器
 * @author jiang
 * @date 2020-12-06 10:45:35
 **/
@Controller('file')
@ApiTags('文件下载')
export class FileDownloadController extends BaseController {
  /**
   * 下载指定相对路径的文件
   *
   * @param Response
   * @param path 文件相对路径
   * @return 返回文件流信息
   * @author jiang
   * @date 2020-12-06 10:46:22
   **/
  @ApiOperation({ summary: '下载指定相对路径的文件' })
  @ApiQuery({ name: 'path', description: '文件相对路径' })
  @Get('download/by/path')
  async downloadByPath(@Res() res: Response, @Query('path') path: string) {
    const filePath = PathUtils.getFullPath({ relativePath: path });
    if (fs.existsSync(filePath)) {
      this.logger().info(`开始下载文件==>${filePath}`);
      return res.sendFile(filePath);
    } else {
      this.logger().error(`文件不存在==>${filePath}`);
      throw new NotFoundException('文件不存在!');
    }
  }

  /**
   * 根据文件ID号下载指定sys_files表记录中的文件
   *
   * @param fileId 文件ID号
   * @return
   * @author jiang
   * @date 2021-01-25 00:32:08
   **/
  @ApiOperation({ summary: '下载指定ID的文件' })
  @ApiQuery({ name: 'fileId', description: '文件ID' })
  @Get('download/by/id')
  async downloadById(@Res() res: Response, @Query('fileId') fileId: string) {
    //查询文件记录
    const record = await SysServices.sysFilesService.findById(fileId);

    if (!record) {
      this.logger().error(`文件ID对应记录不存在==>${fileId}`);
      throw new NotFoundException('文件ID对应记录不存在!');
    }

    if (!record.filePath) {
      this.logger().error(`文件ID对应文件不存在==>${record.filePath}`);
      throw new NotFoundException('文件ID对应文件不存在!');
    }

    //下载文件
    return await this.downloadByPath(res, record.filePath);
  }
}
