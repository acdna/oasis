import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CONFIG } from '../domain/constants';

/**
 * 系统基础参数的定义，用于方便取用参数
 * @author jiang
 * @date 2020-12-06 13:34:07
 **/
@Injectable()
export class ApiConfig {
  constructor(private configService: ConfigService) {}

  /**
   * 获取服务器端口参数
   * @return
   * @author jiang
   * @date 2020-12-06 13:39:28
   **/
  get serverPort(): number {
    return this.configService.get<number>(CONFIG.SERVER_PORT) || 3000;
  }

  /**
   * 获取服务器主机参数
   * @return
   * @author jiang
   * @date 2020-12-06 13:39:28
   **/
  get serverHost(): string {
    return this.configService.get(CONFIG.SERVER_HOST) || 'localhost';
  }

  /**
   * 获取限速的时间
   * @return
   * @author jiang
   * @date 2020-12-06 13:39:28
   **/
  get limitRateWindowMs(): number {
    return this.configService.get<number>(CONFIG.LIMITRATE_WINDOW_MS) || 15 * 60 * 1000;
  }

  /**
   * 获取最大限速请求数
   * @return
   * @author jiang
   * @date 2020-12-06 13:39:28
   **/
  get limitRateMax(): number {
    return this.configService.get(CONFIG.LIMITRATE_MAX) || 100;
  }

  /**
   * 是否启用用户身份认证
   * @return
   * @author jiang
   * @date 2020-12-06 13:35:23
   **/
  get isAuthEnabled(): boolean {
    return this.configService.get(CONFIG.AUTH_ENABLED);
  }

  /**
   * 是否启用功能权限认证
   * @return
   * @author: jiangbin
   * @date: 2020-12-21 14:55:34
   **/
  get isPmsEnabled(): boolean {
    return this.configService.get(CONFIG.PMS_ENABLED);
  }

  /**
   * 获取JWT的密码
   * @return
   * @author: jiangbin
   * @date: 2020-12-07 17:47:09
   **/
  get jwtSeceret(): string {
    return this.configService.get(CONFIG.JWT_SECERET);
  }

  /**
   * 获取JWT的令牌失效时间
   * @return
   * @author: jiangbin
   * @date: 2020-12-07 17:47:23
   **/
  get jwtExpiresIn(): number {
    return this.configService.get<number>(CONFIG.JWT_EXPIRES_IN);
  }

  /**
   * 获取请求时JSON的限制大小，nodejs默认为100kb，有时候可能会因为这个值太小报错
   * @return
   * @author: jiangbin
   * @date: 2020-12-07 17:47:23
   **/
  get reqJsonLimit(): string {
    return this.configService.get<string>(CONFIG.REQ_JSON_LIMIT);
  }
  /**
   * 获取请求时JSON的限制大小，nodejs默认为100kb，有时候可能会因为这个值太小报错
   * @return
   * @author: jiangbin
   * @date: 2020-12-07 17:47:23
   **/
  get reqUrlencodedLimit(): string {
    return this.configService.get<string>(CONFIG.REQ_URLENCODED_LIMIT);
  }
}
