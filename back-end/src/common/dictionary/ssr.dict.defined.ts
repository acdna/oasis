import { DictGroup } from './sys.dict.info';

export const SSR_COMPARE_RESULT_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '比对结果类型',
  nameEn: 'Ssr Compare Result Type',
  group: 'SSR_COMPARE_RESULT_TYPE',
  enable: 'ON',
};
export const SSR_COMPARE_RESULT_TYPE_DICT = {
  SAME_NAME: {
    nameCn: '同名比对结果',
    nameEn: 'SAME NAME',
    value: '1',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_COMPARE_RESULT_TYPE_DICT_GROUP,
  },

  SUSPECTED: {
    nameCn: '疑似比对结果',
    nameEn: 'SUSPECTED ',
    value: '0',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_COMPARE_RESULT_TYPE_DICT_GROUP,
  },
};
export const SSR_IS_SHARE_DICT_GROUP: DictGroup = {
  nameCn: '是否共享',
  nameEn: 'Is Share',
  group: 'SSR_IS_SHARE',
  enable: 'ON',
};
export const SSR_IS_SHARE_DICT = {
  UNSHARE: {
    nameCn: '不共享',
    nameEn: 'UNSHARE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IS_SHARE_DICT_GROUP,
  },

  SHARE: {
    nameCn: '共享',
    nameEn: 'SHARE',
    value: '1',
    order: 2,
    type: 'number',
    species: '共享',
    group: SSR_IS_SHARE_DICT_GROUP,
  },
};
export const SSR_DEVICE_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '仪器分类',
  nameEn: 'Device Type',
  group: 'SSR_DEVICE_TYPE',
  enable: 'ON',
};
export const SSR_DEVICE_TYPE_DICT = {
  CE: {
    nameCn: 'DNA电泳分析仪',
    nameEn: 'CE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_DEVICE_TYPE_DICT_GROUP,
  },

  PCR: {
    nameCn: 'PCR仪',
    nameEn: 'PCR',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_DEVICE_TYPE_DICT_GROUP,
  },

  DNA_WORKSTATION: {
    nameCn: 'DNA全自动提取工作站',
    nameEn: 'DNA Workstation',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_DEVICE_TYPE_DICT_GROUP,
  },
};
export const SSR_GEL_DEVICE_MODEL_DICT_GROUP: DictGroup = {
  nameCn: '电泳分析仪型号',
  nameEn: 'Gel Device Model',
  group: 'SSR_GEL_DEVICE_MODEL',
  enable: 'ON',
};
export const SSR_GEL_DEVICE_MODEL_DICT = {
  ABI3730XL: {
    nameCn: 'ABI3730XL',
    nameEn: 'ABI3730XL',
    value: '96',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GEL_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3730: {
    nameCn: 'ABI3730',
    nameEn: 'ABI3730',
    value: '48',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GEL_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3500XL: {
    nameCn: 'ABI3500XL',
    nameEn: 'ABI3500XL',
    value: '24',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_GEL_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3130XL: {
    nameCn: 'ABI3130XL',
    nameEn: 'ABI3130XL',
    value: '16',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_GEL_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3500: {
    nameCn: 'ABI3500',
    nameEn: 'ABI3500',
    value: '8',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_GEL_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3130: {
    nameCn: 'ABI3130',
    nameEn: 'ABI3130',
    value: '4',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_GEL_DEVICE_MODEL_DICT_GROUP,
  },
};
export const SSR_DEVICE_MODEL_DICT_GROUP: DictGroup = {
  nameCn: '仪器型号',
  nameEn: 'Device Model',
  group: 'SSR_DEVICE_MODEL',
  enable: 'ON',
};
export const SSR_DEVICE_MODEL_DICT = {
  ABI3730XL: {
    nameCn: 'ABI3730XL',
    nameEn: 'ABI3730XL',
    value: '96',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3730: {
    nameCn: 'ABI3730',
    nameEn: 'ABI3730',
    value: '48',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3500XL: {
    nameCn: 'ABI3500XL',
    nameEn: 'ABI3500XL',
    value: '24',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3130XL: {
    nameCn: 'ABI3130XL',
    nameEn: 'ABI3130XL',
    value: '16',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3500: {
    nameCn: 'ABI3500',
    nameEn: 'ABI3500',
    value: '8',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_DEVICE_MODEL_DICT_GROUP,
  },

  ABI3130: {
    nameCn: 'ABI3130',
    nameEn: 'ABI3130',
    value: '4',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_DEVICE_MODEL_DICT_GROUP,
  },
};
export const SSR_REDIS_STATUS_DICT_GROUP: DictGroup = {
  nameCn: 'Redis服务器状态',
  nameEn: 'Redis Status',
  group: 'SSR_REDIS_STATUS',
  enable: 'ON',
};
export const SSR_REDIS_STATUS_DICT = {
  NORMAL: {
    nameCn: '正常',
    nameEn: 'NORMAL',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_REDIS_STATUS_DICT_GROUP,
  },

  FAILURE: {
    nameCn: '故障',
    nameEn: 'FAILURE',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_REDIS_STATUS_DICT_GROUP,
  },

  DISABLE: {
    nameCn: '停用',
    nameEn: 'DISABLE',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_REDIS_STATUS_DICT_GROUP,
  },
};
export const SSR_CLUSTER_TREE_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '聚类分析树类型',
  nameEn: 'Cluster Tree Type',
  group: 'SSR_CLUSTER_TREE_TYPE',
  enable: 'ON',
};
export const SSR_CLUSTER_TREE_TYPE_DICT = {
  fan: {
    nameCn: 'fan',
    nameEn: 'fan',
    value: 'fan',
    order: 1,
    type: 'string',
    species: 'fan',
    group: SSR_CLUSTER_TREE_TYPE_DICT_GROUP,
  },

  cladogram: {
    nameCn: 'cladogram',
    nameEn: 'cladogram',
    value: 'cladogram',
    order: 2,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_TREE_TYPE_DICT_GROUP,
  },

  phylogram: {
    nameCn: 'phylogram',
    nameEn: 'phylogram',
    value: 'phylogram',
    order: 3,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_TREE_TYPE_DICT_GROUP,
  },

  radial: {
    nameCn: 'radial',
    nameEn: 'radial',
    value: 'radial',
    order: 4,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_TREE_TYPE_DICT_GROUP,
  },

  unrooted: {
    nameCn: 'unrooted',
    nameEn: 'unrooted',
    value: 'unrooted',
    order: 5,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_TREE_TYPE_DICT_GROUP,
  },
};
export const SSR_CLUSTER_ALGORITHM_DICT_GROUP: DictGroup = {
  nameCn: '聚类分析算法类型',
  nameEn: 'Cluster Algorithm',
  group: 'SSR_CLUSTER_ALGORITHM',
  enable: 'ON',
};
export const SSR_CLUSTER_ALGORITHM_DICT = {
  UPGMA: {
    nameCn: 'UPGMA',
    nameEn: 'UPGMA',
    value: 'UPGMA',
    order: 1,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_ALGORITHM_DICT_GROUP,
  },

  NJ: {
    nameCn: 'NJ',
    nameEn: 'NJ',
    value: 'NJ',
    order: 2,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_ALGORITHM_DICT_GROUP,
  },

  BIONJ: {
    nameCn: 'BIONJ',
    nameEn: 'BIONJ',
    value: 'BIONJ',
    order: 3,
    type: 'string',
    species: '',
    group: SSR_CLUSTER_ALGORITHM_DICT_GROUP,
  },
};
export const SSR_IS_MANUAL_DICT_GROUP: DictGroup = {
  nameCn: '手动审核',
  nameEn: 'Manual Audit',
  group: 'SSR_IS_MANUAL',
  enable: 'ON',
};
export const SSR_IS_MANUAL_DICT = {
  YES: {
    nameCn: '是',
    nameEn: 'YES',
    value: 'YES',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IS_MANUAL_DICT_GROUP,
  },

  NO: {
    nameCn: '否',
    nameEn: 'NO',
    value: 'NO',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_IS_MANUAL_DICT_GROUP,
  },
};
export const SSR_HAS_RESULT_DICT_GROUP: DictGroup = {
  nameCn: '是否已入库',
  nameEn: 'Has Result',
  group: 'SSR_HAS_RESULT',
  enable: 'ON',
};
export const SSR_HAS_RESULT_DICT = {
  NOT: {
    nameCn: '未入库',
    nameEn: 'NOT IN WAREHOUSE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_HAS_RESULT_DICT_GROUP,
  },

  YES: {
    nameCn: '已入库',
    nameEn: 'IN WAREHOUSE',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_HAS_RESULT_DICT_GROUP,
  },
};
export const SSR_PURITY_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '纯度鉴定结果类型',
  nameEn: 'Purity Result Type',
  group: 'SSR_PURITY_TYPE',
  enable: 'ON',
};
export const SSR_PURITY_TYPE_DICT = {
  NORMAL: {
    nameCn: '正常株',
    nameEn: 'NORMAL',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  PARENT: {
    nameCn: '亲本株',
    nameEn: 'PARENT',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  FATHER: {
    nameCn: '父本',
    nameEn: 'FATHER',
    value: '5',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  MOTHER: {
    nameCn: '母本',
    nameEn: 'MOTHER',
    value: '6',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  OTHER: {
    nameCn: '其他自交苗',
    nameEn: 'OTHER',
    value: '7',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  Heterozygous: {
    nameCn: '异型株',
    nameEn: 'HETEROZYGOUS',
    value: '2',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  MISS: {
    nameCn: '缺失',
    nameEn: 'MISS',
    value: '3',
    order: 7,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },

  UNJUDGMENT: {
    nameCn: '无法判定',
    nameEn: 'UNJUDGMENT',
    value: '4',
    order: 7,
    type: 'number',
    species: '',
    group: SSR_PURITY_TYPE_DICT_GROUP,
  },
};
export const SSR_GENE_DISPLAY_DICT_GROUP: DictGroup = {
  nameCn: '指纹数据显示类型',
  nameEn: 'Ssr Gene Display',
  group: 'SSR_GENE_DISPLAY',
  enable: 'ON',
};
export const SSR_GENE_DISPLAY_DICT = {
  DIFF: {
    nameCn: '差异',
    nameEn: 'DIFF',
    value: '3',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_DISPLAY_DICT_GROUP,
  },

  NO_DIFF: {
    nameCn: '无差异',
    nameEn: 'NO DIFF',
    value: '2',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_DISPLAY_DICT_GROUP,
  },

  MISS: {
    nameCn: '缺失',
    nameEn: 'MISS',
    value: '4',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_GENE_DISPLAY_DICT_GROUP,
  },

  UNJUDGMENT: {
    nameCn: '无法判定',
    nameEn: 'UNJUDGMENT',
    value: '5',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_GENE_DISPLAY_DICT_GROUP,
  },
};
export const SSR_GENE_AUDITED_DICT_GROUP: DictGroup = {
  nameCn: '指纹审核操作',
  nameEn: 'Gene Audited',
  group: 'SSR_GENE_AUDITED',
  enable: 'ON',
};
export const SSR_GENE_AUDITED_DICT = {
  UNAUDITED: {
    nameCn: '未审',
    nameEn: 'UNAUDITED',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_AUDITED_DICT_GROUP,
  },

  AUDITED: {
    nameCn: '已审',
    nameEn: 'AUDITED',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_AUDITED_DICT_GROUP,
  },
};
export const SSR_AUDIT_WAY_DICT_GROUP: DictGroup = {
  nameCn: '指纹审核方式',
  nameEn: 'Audit Way',
  group: 'SSR_AUDIT_WAY',
  enable: 'ON',
};
export const SSR_AUDIT_WAY_DICT = {
  AUTO: {
    nameCn: '自动审核',
    nameEn: 'AUTO',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_AUDIT_WAY_DICT_GROUP,
  },

  MANUAL: {
    nameCn: '手动审核',
    nameEn: 'MANUAL',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_AUDIT_WAY_DICT_GROUP,
  },
};
export const SSR_GENE_STATUS_DICT_GROUP: DictGroup = {
  nameCn: '指纹审核状态',
  nameEn: 'Gene Status',
  group: 'SSR_GENE_STATUS',
  enable: 'ON',
};
export const SSR_GENE_STATUS_DICT = {
  UNAUDITED: {
    nameCn: '未审核',
    nameEn: 'UNAUDITED',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_STATUS_DICT_GROUP,
  },

  TEMP_AUDITED: {
    nameCn: '临时审核',
    nameEn: 'TEMP_AUDITED',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_STATUS_DICT_GROUP,
  },

  AUDITED: {
    nameCn: '正式审核',
    nameEn: 'AUDITED',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_GENE_STATUS_DICT_GROUP,
  },
};
export const SSR_GENE_COMPARE_STATUS_DICT_GROUP: DictGroup = {
  nameCn: '比对指纹审核状态',
  nameEn: 'Compare Gene Audit status',
  group: 'SSR_GENE_COMPARE_STATUS',
  enable: 'ON',
};
export const SSR_GENE_COMPARE_STATUS_DICT = {
  AUTO: {
    nameCn: '自动择优',
    nameEn: 'AUTO',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_COMPARE_STATUS_DICT_GROUP,
  },

  TEMP_AUDIT: {
    nameCn: '临时审核',
    nameEn: 'TEMP AUDIT',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_COMPARE_STATUS_DICT_GROUP,
  },

  FORMAL_AUDIT: {
    nameCn: '正式审核',
    nameEn: 'FORMAL AUDIT',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_GENE_COMPARE_STATUS_DICT_GROUP,
  },
};
export const SSR_SAM_KIND_DICT_GROUP: DictGroup = {
  nameCn: '样品类型',
  nameEn: 'Sam Kind',
  group: 'SSR_SAM_KIND',
  enable: 'ON',
};
export const SSR_SAM_KIND_DICT = {
  INBRED: {
    nameCn: '自交系',
    nameEn: 'INBRED LINE',
    value: '0',
    order: 1,
    type: 'number',
    species: '自交系',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  HYBRIDS: {
    nameCn: '杂交种',
    nameEn: 'HYBRIDS',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  MAINTAINER_LINE: {
    nameCn: '保持系',
    nameEn: 'MAINTAINER LINE',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  RESTORER_LINE: {
    nameCn: '恢复系',
    nameEn: 'RESTORER LINE',
    value: '3',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  STERILE_LINE: {
    nameCn: '不育系',
    nameEn: 'STERILE LINE',
    value: '4',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  LANDRACE: {
    nameCn: '农家种',
    nameEn: 'LANDRACE',
    value: '5',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  BREEDING: {
    nameCn: '育种中间材料',
    nameEn: 'BREEDING INTERMEDIATE MATERIAL',
    value: '6',
    order: 7,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },

  OTHER: {
    nameCn: '其它',
    nameEn: 'OTHER',
    value: '7',
    order: 8,
    type: 'number',
    species: '',
    group: SSR_SAM_KIND_DICT_GROUP,
  },
};
export const SSR_IS_DELETED_DICT_GROUP: DictGroup = {
  nameCn: '是否删除',
  nameEn: 'Is Deleted',
  group: 'SSR_IS_DELETED',
  enable: 'ON',
};
export const SSR_IS_DELETED_DICT = {
  YES: {
    nameCn: '是',
    nameEn: 'YES',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IS_DELETED_DICT_GROUP,
  },

  NO: {
    nameCn: '否',
    nameEn: 'NO',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_IS_DELETED_DICT_GROUP,
  },
};
export const SSR_EXTRACT_TYPE_DICT_GROUP: DictGroup = {
  nameCn: 'DNA提取方式',
  nameEn: 'Extract Type',
  group: 'SSR_EXTRACT_TYPE',
  enable: 'ON',
};
export const SSR_EXTRACT_TYPE_DICT = {
  MIXED: {
    nameCn: '混株提取',
    nameEn: 'MIXED',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_EXTRACT_TYPE_DICT_GROUP,
  },

  UNMIXED: {
    nameCn: '单株提取',
    nameEn: 'UNMIXED',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_EXTRACT_TYPE_DICT_GROUP,
  },
};
export const SSR_GENE_LIB_DICT_GROUP: DictGroup = {
  nameCn: '指纹库类型',
  nameEn: 'Gene Lib',
  group: 'SSR_GENE_LIB',
  enable: 'ON',
};
export const SSR_GENE_LIB_DICT = {
  ORIGINAL: {
    nameCn: '原始指纹库',
    nameEn: 'ORIGINAL GENE LIB',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_LIB_DICT_GROUP,
  },

  EXPERIMENTAL: {
    nameCn: '实验指纹库',
    nameEn: 'EXPERIMENTAL GENE LIB',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_LIB_DICT_GROUP,
  },

  SAMPLE: {
    nameCn: '样品指纹库',
    nameEn: 'SAMPLE GENE LIB',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_GENE_LIB_DICT_GROUP,
  },

  LOCAL: {
    nameCn: '本地指纹库',
    nameEn: 'LOCAL GENE LIB',
    value: '3',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_GENE_LIB_DICT_GROUP,
  },
};
export const SSR_GENE_COMPARE_LIB_DICT_GROUP: DictGroup = {
  nameCn: '比对指纹库类型',
  nameEn: 'Gene Compare Lib',
  group: 'SSR_GENE_COMPARE_LIB',
  enable: 'ON',
};
export const SSR_GENE_COMPARE_LIB_DICT = {
  LOCAL: {
    nameCn: '本地指纹库',
    nameEn: 'LOCAL GENE LIB',
    value: '3',
    order: 1,
    type: 'number',
    species: '本地指纹库',
    group: SSR_GENE_COMPARE_LIB_DICT_GROUP,
  },

  SAMPLE: {
    nameCn: '样品指纹库',
    nameEn: 'SAMPLE GENE LIB',
    value: '2',
    order: 2,
    type: 'number',
    species: '样品指纹库',
    group: SSR_GENE_COMPARE_LIB_DICT_GROUP,
  },

  EXPERIMENTAL: {
    nameCn: '实验指纹库',
    nameEn: 'EXPERIMENTAL GENE LIB',
    value: '1',
    order: 3,
    type: 'number',
    species: '实验指纹库',
    group: SSR_GENE_COMPARE_LIB_DICT_GROUP,
  },
};
export const SSR_GENE_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '指纹类型',
  nameEn: 'Gene Type',
  group: 'SSR_GENE_TYPE',
  enable: 'ON',
};
export const SSR_GENE_TYPE_DICT = {
  SSR: {
    nameCn: 'SSR',
    nameEn: 'SSR',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_TYPE_DICT_GROUP,
  },

  SNP: {
    nameCn: 'SNP',
    nameEn: 'SNP',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_TYPE_DICT_GROUP,
  },
};
export const SSR_IS_LOCKED_DICT_GROUP: DictGroup = {
  nameCn: '锁定状态',
  nameEn: 'Locked State',
  group: 'SSR_IS_LOCKED',
  enable: 'ON',
};
export const SSR_IS_LOCKED_DICT = {
  LOCKED: {
    nameCn: '未锁定',
    nameEn: 'LOCKED',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IS_LOCKED_DICT_GROUP,
  },

  UNLOCKED: {
    nameCn: '锁定',
    nameEn: 'UNLOCKED',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_IS_LOCKED_DICT_GROUP,
  },
};
export const SSR_IS_UPLOADED_DICT_GROUP: DictGroup = {
  nameCn: '是否上传',
  nameEn: 'Is Uploaded',
  group: 'SSR_IS_UPLOADED',
  enable: 'ON',
};
export const SSR_IS_UPLOADED_DICT = {
  UPLOADED: {
    nameCn: '未上传',
    nameEn: 'UPLOADED',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IS_UPLOADED_DICT_GROUP,
  },

  UNUPLOADED: {
    nameCn: '已上传',
    nameEn: 'UNUPLOADED',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_IS_UPLOADED_DICT_GROUP,
  },
};
export const SSR_GENE_IS_SUBMITED_DICT_GROUP: DictGroup = {
  nameCn: '指纹是否提交',
  nameEn: 'Gene Is Submited',
  group: 'SSR_GENE_IS_SUBMITED',
  enable: 'ON',
};
export const SSR_GENE_IS_SUBMITED_DICT = {
  UNSUBMIT: {
    nameCn: '未提交',
    nameEn: 'UNSUBMIT',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GENE_IS_SUBMITED_DICT_GROUP,
  },

  SUBMITED: {
    nameCn: '已提交',
    nameEn: 'SUBMITED',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GENE_IS_SUBMITED_DICT_GROUP,
  },
};
export const SSR_IN_COMPARE_DICT_GROUP: DictGroup = {
  nameCn: '是否参与比对',
  nameEn: 'In Compare',
  group: 'SSR_IN_COMPARE',
  enable: 'ON',
};
export const SSR_IN_COMPARE_DICT = {
  UNPARTAKE: {
    nameCn: '不参与',
    nameEn: 'UNPARTAKE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IN_COMPARE_DICT_GROUP,
  },

  PARTAKE: {
    nameCn: '参与',
    nameEn: 'PARTAKE',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_IN_COMPARE_DICT_GROUP,
  },
};
export const SSR_DYE_COLORS_DICT_GROUP: DictGroup = {
  nameCn: '荧光颜色',
  nameEn: 'Dye Colors',
  group: 'SSR_DYE_COLORS',
  enable: 'ON',
};
export const SSR_DYE_COLORS_DICT = {
  NED: {
    nameCn: 'NED',
    nameEn: 'NED',
    value: '黑',
    order: 1,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_DICT_GROUP,
  },

  PET: {
    nameCn: 'PET',
    nameEn: 'PET',
    value: '红',
    order: 2,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_DICT_GROUP,
  },

  FAM: {
    nameCn: 'FAM',
    nameEn: 'FAM',
    value: '蓝',
    order: 3,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_DICT_GROUP,
  },

  VIC: {
    nameCn: 'VIC',
    nameEn: 'VIC',
    value: '绿',
    order: 4,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_DICT_GROUP,
  },
};
export const SSR_DYE_COLORS_CSS_DICT_GROUP: DictGroup = {
  nameCn: '荧光颜色CSS值',
  nameEn: 'Dye Colors Css',
  group: 'SSR_DYE_COLORS_CSS',
  enable: 'ON',
};
export const SSR_DYE_COLORS_CSS_DICT = {
  BLACK: {
    nameCn: '黑',
    nameEn: 'BLACK',
    value: '#000000',
    order: 1,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_CSS_DICT_GROUP,
  },

  RED: {
    nameCn: '红',
    nameEn: 'RED',
    value: '#ED561B',
    order: 2,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_CSS_DICT_GROUP,
  },

  BLUE: {
    nameCn: '蓝',
    nameEn: 'BLUE',
    value: '#058DC7',
    order: 3,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_CSS_DICT_GROUP,
  },

  GREEN: {
    nameCn: '绿',
    nameEn: 'GREEN',
    value: '#64E572',
    order: 4,
    type: 'string',
    species: '',
    group: SSR_DYE_COLORS_CSS_DICT_GROUP,
  },
};
export const SSR_PRODUCE_PICTURE_DICT_GROUP: DictGroup = {
  nameCn: '是否生成图谱',
  nameEn: 'Produce Picture',
  group: 'SSR_PRODUCE_PICTURE',
  enable: 'ON',
};
export const SSR_PRODUCE_PICTURE_DICT = {
  NO: {
    nameCn: '否',
    nameEn: 'NO',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_PRODUCE_PICTURE_DICT_GROUP,
  },

  YES: {
    nameCn: '是',
    nameEn: 'YES',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_PRODUCE_PICTURE_DICT_GROUP,
  },
};
export const SSR_AUDIT_INTERVAL_DICT_GROUP: DictGroup = {
  nameCn: '审核时间间隔',
  nameEn: 'Audit Interval',
  group: 'SSR_AUDIT_INTERVAL',
  enable: 'ON',
};
export const SSR_AUDIT_INTERVAL_DICT = {
  ONE_HOUR: {
    nameCn: '1小时',
    nameEn: 'ONE HOUR',
    value: '1',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  THREE_HOUR: {
    nameCn: '3小时',
    nameEn: 'THREE HOUR',
    value: '3',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  FIVE_HOUR: {
    nameCn: '5小时',
    nameEn: 'FIVE HOUR',
    value: '5',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  EIGHT_HOUR: {
    nameCn: '8小时',
    nameEn: 'EIGHT HOUR',
    value: '8',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  TWO_DAYS: {
    nameCn: '2天',
    nameEn: 'TWO DAYS',
    value: '48',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  THREE_DAYS: {
    nameCn: '3天',
    nameEn: 'THREE DAYS',
    value: '72',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  SEVEN_DAYS: {
    nameCn: '7天',
    nameEn: 'SEVEN DAYS',
    value: '168',
    order: 7,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  ONE_WEEK: {
    nameCn: '本周',
    nameEn: 'ONE WEEK',
    value: '56',
    order: 7,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  THIRTY_DAYS: {
    nameCn: '30天',
    nameEn: 'THIRTY DAYS',
    value: '720',
    order: 8,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  THIS_MONTH: {
    nameCn: '本月',
    nameEn: 'THIS MONTH',
    value: '240',
    order: 8,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },

  ALL: {
    nameCn: '所有',
    nameEn: 'ALL',
    value: '-1',
    order: 9,
    type: 'number',
    species: '',
    group: SSR_AUDIT_INTERVAL_DICT_GROUP,
  },
};
export const SSR_PLATE_CODE_DICT_GROUP: DictGroup = {
  nameCn: '上样板代号',
  nameEn: 'Plate Code',
  group: 'SSR_PLATE_CODE',
  enable: 'ON',
};
export const SSR_PLATE_CODE_DICT = {
  EXTERNAL_TEST: {
    nameCn: '对外检测',
    nameEn: 'EXTERNAL TEST',
    value: 'A',
    order: 1,
    type: 'string',
    species: '',
    group: SSR_PLATE_CODE_DICT_GROUP,
  },

  STANDARD_SAMPLE: {
    nameCn: '标准样品',
    nameEn: 'STANDARD SAMPLE',
    value: 'P',
    order: 2,
    type: 'string',
    species: '',
    group: SSR_PLATE_CODE_DICT_GROUP,
  },

  REGIONAL_TEST: {
    nameCn: '区试',
    nameEn: 'REGIONAL TEST',
    value: 'H',
    order: 3,
    type: 'string',
    species: '',
    group: SSR_PLATE_CODE_DICT_GROUP,
  },

  MARKET_SAMPLING: {
    nameCn: '市场抽检',
    nameEn: 'MARKET SAMPLING',
    value: 'K',
    order: 4,
    type: 'string',
    species: '',
    group: SSR_PLATE_CODE_DICT_GROUP,
  },
};
export const SSR_PANEL_TYPE_DICT_GROUP: DictGroup = {
  nameCn: 'panel类型',
  nameEn: 'Panel Type',
  group: 'SSR_PANEL_TYPE',
  enable: 'ON',
};
export const SSR_PANEL_TYPE_DICT = {
  SYSTEM: {
    nameCn: '系统默认',
    nameEn: 'SYSTEM DEFAULT',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_PANEL_TYPE_DICT_GROUP,
  },

  CUSTOM: {
    nameCn: '自定义',
    nameEn: 'CUSTOM',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_PANEL_TYPE_DICT_GROUP,
  },
};
export const SSR_PLATE_SAMPLE_NUM_DICT_GROUP: DictGroup = {
  nameCn: '上样数量',
  nameEn: 'PLATE_BARCODE_NUM',
  group: 'SSR_PLATE_BARCODE_NUM',
  enable: 'ON',
};
export const SSR_PLATE_SAMPLE_NUM_DICT = {
  HOLES_96: {
    nameCn: '96孔',
    nameEn: '96 HOLES',
    value: '96',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_PLATE_SAMPLE_NUM_DICT_GROUP,
  },

  HOLES_48: {
    nameCn: '48孔',
    nameEn: '48 HOLES',
    value: '48',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_PLATE_SAMPLE_NUM_DICT_GROUP,
  },

  HOLES_24: {
    nameCn: '24孔',
    nameEn: '24 HOLES',
    value: '24',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_PLATE_SAMPLE_NUM_DICT_GROUP,
  },
};
export const SSR_DNA_TEST_DEVICE_DICT_GROUP: DictGroup = {
  nameCn: 'DNA提取实验设备',
  nameEn: 'Dna Test Device',
  group: 'SSR_DNA_TEST_DEVICE',
  enable: 'ON',
};
export const SSR_DNA_TEST_DEVICE_DICT = {
  CENTRIFUGE_TUBE: {
    nameCn: '离心管',
    nameEn: 'CENTRIFUGE TUBE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_DNA_TEST_DEVICE_DICT_GROUP,
  },

  CENTRIFUGE: {
    nameCn: '离心机',
    nameEn: 'CENTRIFUGE',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_DNA_TEST_DEVICE_DICT_GROUP,
  },

  DNA_EXTRACTOR: {
    nameCn: 'DNA提取器',
    nameEn: 'DNA EXTRACTOR',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_DNA_TEST_DEVICE_DICT_GROUP,
  },
};
export const SSR_ELECT_TEST_DEVICE_DICT_GROUP: DictGroup = {
  nameCn: '电泳检测实验设备',
  nameEn: 'Electrophoresis Equipment',
  group: 'SSR_ELECT_TEST_DEVICE',
  enable: 'ON',
};
export const SSR_ELECT_TEST_DEVICE_DICT = {
  CE: {
    nameCn: '电泳仪',
    nameEn: 'CE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_ELECT_TEST_DEVICE_DICT_GROUP,
  },

  ET: {
    nameCn: '电泳槽',
    nameEn: 'ELECTROPHORESIS TANK',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_ELECT_TEST_DEVICE_DICT_GROUP,
  },

  CENTRIFUGE: {
    nameCn: '离心机',
    nameEn: 'CENTRIFUGE',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_ELECT_TEST_DEVICE_DICT_GROUP,
  },
};
export const SSR_PATERNITY_GENE_LIB_DICT_GROUP: DictGroup = {
  nameCn: '亲子鉴定指纹库类型',
  nameEn: 'Paternity Gene Lib',
  group: 'SSR_PATERNITY_GENE_LIB',
  enable: 'ON',
};
export const SSR_PATERNITY_GENE_LIB_DICT = {
  LOCAL: {
    nameCn: '本地指纹库',
    nameEn: 'LOCAL GENE LIB',
    value: '3',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_GENE_LIB_DICT_GROUP,
  },

  SAMPLE: {
    nameCn: '样品指纹库',
    nameEn: 'SAMPLE GENE LIB',
    value: '2',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_GENE_LIB_DICT_GROUP,
  },

  EXPERIMENTAL: {
    nameCn: '实验指纹库',
    nameEn: 'EXPERIMENTAL GENE LIB',
    value: '1',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_GENE_LIB_DICT_GROUP,
  },
};
export const SSR_PATERNITY_MATCH_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '亲子鉴定指纹匹配类型',
  nameEn: 'Paternity Match Type',
  group: 'SSR_PATERNITY_MATCH_TYPE',
  enable: 'ON',
};
export const SSR_PATERNITY_MATCH_TYPE_DICT = {
  UNMATCH: {
    nameCn: '不匹配',
    nameEn: 'UNMATCH',
    value: '3',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_MATCH_TYPE_DICT_GROUP,
  },

  MATCH: {
    nameCn: '匹配',
    nameEn: 'MATCH',
    value: '2',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_MATCH_TYPE_DICT_GROUP,
  },

  MISS: {
    nameCn: '缺失',
    nameEn: 'MISS',
    value: '4',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_MATCH_TYPE_DICT_GROUP,
  },

  UNJUDGMENT: {
    nameCn: '无法判定',
    nameEn: 'UNJUDGMENT',
    value: '5',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_PATERNITY_MATCH_TYPE_DICT_GROUP,
  },
};
export const SSR_COMPARE_HISTORY_STATUS_DICT_GROUP: DictGroup = {
  nameCn: '指纹比对历史记录状态',
  nameEn: 'Compare History Status',
  group: 'SSR_COMPARE_HISTORY_STATUS',
  enable: 'ON',
};
export const SSR_COMPARE_HISTORY_STATUS_DICT = {
  SUCCESS: {
    nameCn: '成功',
    nameEn: 'SUCCESS',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_COMPARE_HISTORY_STATUS_DICT_GROUP,
  },

  NO_SOURCE_GENES: {
    nameCn: '无待比指纹',
    nameEn: 'NO SOURCE GENES',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_COMPARE_HISTORY_STATUS_DICT_GROUP,
  },

  NO_TARGET_GENES: {
    nameCn: '无对照指纹',
    nameEn: 'NO TARGET GENES',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_COMPARE_HISTORY_STATUS_DICT_GROUP,
  },

  NO_COMPARISON_RESULT: {
    nameCn: '无比对结果',
    nameEn: 'NO COMPARISON RESULT',
    value: '3',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_COMPARE_HISTORY_STATUS_DICT_GROUP,
  },

  PROGRAM_EXCEPTION: {
    nameCn: '程序异常',
    nameEn: 'PROGRAM EXCEPTION',
    value: '4',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_COMPARE_HISTORY_STATUS_DICT_GROUP,
  },

  EXCEEDS_LIMIT: {
    nameCn: '结果数超限',
    nameEn: 'EXCEEDS LIMIT',
    value: '5',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_COMPARE_HISTORY_STATUS_DICT_GROUP,
  },
};
export const SSR_COMPARE_TYPE_DICT_GROUP: DictGroup = {
  nameCn: '指纹比对类型',
  nameEn: 'Compare Type',
  group: 'SSR_COMPARE_TYPE',
  enable: 'ON',
};
export const SSR_COMPARE_TYPE_DICT = {
  WHOLE: {
    nameCn: '全库比对',
    nameEn: 'WHOLE COMPARISON',
    value: '0',
    order: 1,
    type: 'number',
    species: '全库比对',
    group: SSR_COMPARE_TYPE_DICT_GROUP,
  },

  SUSPECTED: {
    nameCn: '疑似比对',
    nameEn: 'SUSPECTED COMPARISON',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_COMPARE_TYPE_DICT_GROUP,
  },

  HOMONYMY: {
    nameCn: '同名比对',
    nameEn: 'HOMONYMY COMPARISON',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_COMPARE_TYPE_DICT_GROUP,
  },

  RANGE: {
    nameCn: '范围内比对',
    nameEn: 'RANGE COMPARISON',
    value: '3',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_COMPARE_TYPE_DICT_GROUP,
  },

  CUSTOM: {
    nameCn: '自定义比对',
    nameEn: 'CUSTOM COMPARISON',
    value: '4',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_COMPARE_TYPE_DICT_GROUP,
  },
};
export const SSR_DNA_DEVICE_DICT_GROUP: DictGroup = {
  nameCn: 'DNA分析仪',
  nameEn: 'DNA Analysis Equipment',
  group: 'SSR_DNA_DEVICE',
  enable: 'ON',
};
export const SSR_DNA_DEVICE_DICT = {
  ABI3730XL: {
    nameCn: 'ABI3730XL',
    nameEn: 'ABI3730XL',
    value: '96',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_DNA_DEVICE_DICT_GROUP,
  },

  ABI3730: {
    nameCn: 'ABI3730',
    nameEn: 'ABI3730',
    value: '48',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_DNA_DEVICE_DICT_GROUP,
  },

  ABI3500XL: {
    nameCn: 'ABI3500XL',
    nameEn: 'ABI3500XL',
    value: '24',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_DNA_DEVICE_DICT_GROUP,
  },

  ABI3130XL: {
    nameCn: 'ABI3130XL',
    nameEn: 'ABI3130XL',
    value: '16',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_DNA_DEVICE_DICT_GROUP,
  },

  ABI3500: {
    nameCn: 'ABI3500',
    nameEn: 'ABI3500',
    value: '8',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_DNA_DEVICE_DICT_GROUP,
  },

  ABI3130: {
    nameCn: 'ABI3130',
    nameEn: 'ABI3130',
    value: '4',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_DNA_DEVICE_DICT_GROUP,
  },
};
export const SSR_GEL_STANDARS_DICT_GROUP: DictGroup = {
  nameCn: '电泳板规格',
  nameEn: 'Gel Standars',
  group: 'SSR_GEL_STANDARS',
  enable: 'ON',
};
export const SSR_GEL_STANDARS_DICT = {
  PLATE_96: {
    nameCn: '96孔板',
    nameEn: 'PLATE 96',
    value: '96',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GEL_STANDARS_DICT_GROUP,
  },

  PLATE_384: {
    nameCn: '384孔板',
    nameEn: 'PLATE 384',
    value: '384',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GEL_STANDARS_DICT_GROUP,
  },
};
export const SSR_GEL_DIRECTION_DICT_GROUP: DictGroup = {
  nameCn: '电泳排版方向',
  nameEn: 'Gel Direction',
  group: 'SSR_GEL_DIRECTION',
  enable: 'ON',
};
export const SSR_GEL_DIRECTION_DICT = {
  HORIZONTAL: {
    nameCn: '横向排版',
    nameEn: 'HORIZONTAL',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_GEL_DIRECTION_DICT_GROUP,
  },

  VERTICAL: {
    nameCn: '纵向排版',
    nameEn: 'VERTICAL',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_GEL_DIRECTION_DICT_GROUP,
  },
};
export const SSR_AUDIT_HISTORY_STATUS_DICT_GROUP: DictGroup = {
  nameCn: '指纹审核历史记录状态',
  nameEn: 'Audit History Status',
  group: 'SSR_AUDIT_HISTORY_STATUS',
  enable: 'ON',
};
export const SSR_AUDIT_HISTORY_STATUS_DICT = {
  SUCCESS: {
    nameCn: '成功',
    nameEn: 'SUCCESS',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_AUDIT_HISTORY_STATUS_DICT_GROUP,
  },

  NO_AUDIT_GENE: {
    nameCn: '无审核指纹',
    nameEn: 'NO AUDIT GENE',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_AUDIT_HISTORY_STATUS_DICT_GROUP,
  },

  NO_AUDIT_RESULT: {
    nameCn: '无审核结果',
    nameEn: 'NO AUDIT RESULT',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_AUDIT_HISTORY_STATUS_DICT_GROUP,
  },

  PROGRAM_EXCEPTION: {
    nameCn: '程序异常',
    nameEn: 'PROGRAM EXCEPTION',
    value: '3',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_AUDIT_HISTORY_STATUS_DICT_GROUP,
  },
};
export const SSR_CACHE_TRACE_CODES_DICT_GROUP: DictGroup = {
  nameCn: '缓存追踪表类型代码',
  nameEn: 'Cache Trace Codes',
  group: 'SSR_CACHE_TRACE_CODES',
  enable: 'ON',
};
export const SSR_CACHE_TRACE_CODES_DICT = {
  SAMPLE_RECORD_CACHE: {
    nameCn: '样品信息库记录缓存',
    nameEn: 'SAMPLE RECORD CACHE',
    value: '0',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  ORIGINAL_GENE_CACHE: {
    nameCn: '原始指纹库指纹缓存',
    nameEn: 'ORIGINAL GENE CACHE',
    value: '1',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  EXPERIMENTAL_GENE_CACHE: {
    nameCn: '实验指纹库指纹缓存',
    nameEn: 'EXPERIMENTAL GENE CACHE',
    value: '2',
    order: 3,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  SAMPLE_GENE_CACHE: {
    nameCn: '样品指纹库指纹缓存',
    nameEn: 'SAMPLE GENE CACHE',
    value: '3',
    order: 4,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  LOCAL_GENE_CACHE: {
    nameCn: '本地库指纹库指纹缓存',
    nameEn: 'LOCAL GENE CACHE',
    value: '4',
    order: 5,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  COMPARE_RESULT_GENE_CACHE: {
    nameCn: '结果指纹库指纹缓存',
    nameEn: 'COMPARE RESULT GENE CACHE',
    value: '5',
    order: 6,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  ORIGINAL_RECORD_CACHE: {
    nameCn: '原始指纹库记录缓存',
    nameEn: 'ORIGINAL RECORD CACHE',
    value: '101',
    order: 7,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  EXPERIMENTAL_GENE_RECORD_CACHE: {
    nameCn: '实验指纹库记录缓存',
    nameEn: 'EXPERIMENTAL RECORD CACHE',
    value: '102',
    order: 8,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  SAMPLE_GENE_RECORD_CACHE: {
    nameCn: '样品指纹库记录缓存',
    nameEn: 'SAMPLE RECORD CACHE',
    value: '103',
    order: 9,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  LOCAL_GENE_RECORD_CACHE: {
    nameCn: '本地库指纹库记录缓存',
    nameEn: 'LOCAL RECORD CACHE',
    value: '104',
    order: 10,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },

  COMPARE_RESULT_RECORD_CACHE: {
    nameCn: '结果指纹库记录缓存',
    nameEn: 'COMPARE RESULT RECORD CACHE',
    value: '105',
    order: 11,
    type: 'number',
    species: '',
    group: SSR_CACHE_TRACE_CODES_DICT_GROUP,
  },
};
export const SSR_IS_PASS_DICT_GROUP: DictGroup = {
  nameCn: 'Pass/Fail',
  nameEn: 'Pass/Fail',
  group: 'SSR_IS_PASS',
  enable: 'ON',
};
export const SSR_IS_PASS_DICT = {
  PASS: {
    nameCn: 'PASS',
    nameEn: 'PASS',
    value: '1',
    order: 1,
    type: 'number',
    species: '',
    group: SSR_IS_PASS_DICT_GROUP,
  },

  FAIL: {
    nameCn: 'FAIL',
    nameEn: 'FAIL',
    value: '0',
    order: 2,
    type: 'number',
    species: '',
    group: SSR_IS_PASS_DICT_GROUP,
  },
};
export const SSR_DICT_GROUPS = [
  SSR_COMPARE_RESULT_TYPE_DICT,
  SSR_IS_SHARE_DICT,
  SSR_DEVICE_TYPE_DICT,
  SSR_GEL_DEVICE_MODEL_DICT,
  SSR_DEVICE_MODEL_DICT,
  SSR_REDIS_STATUS_DICT,
  SSR_CLUSTER_TREE_TYPE_DICT,
  SSR_CLUSTER_ALGORITHM_DICT,
  SSR_IS_MANUAL_DICT,
  SSR_HAS_RESULT_DICT,
  SSR_PURITY_TYPE_DICT,
  SSR_GENE_DISPLAY_DICT,
  SSR_GENE_AUDITED_DICT,
  SSR_AUDIT_WAY_DICT,
  SSR_GENE_STATUS_DICT,
  SSR_GENE_COMPARE_STATUS_DICT,
  SSR_SAM_KIND_DICT,
  SSR_IS_DELETED_DICT,
  SSR_EXTRACT_TYPE_DICT,
  SSR_GENE_LIB_DICT,
  SSR_GENE_COMPARE_LIB_DICT,
  SSR_GENE_TYPE_DICT,
  SSR_IS_LOCKED_DICT,
  SSR_IS_UPLOADED_DICT,
  SSR_GENE_IS_SUBMITED_DICT,
  SSR_IN_COMPARE_DICT,
  SSR_DYE_COLORS_DICT,
  SSR_DYE_COLORS_CSS_DICT,
  SSR_PRODUCE_PICTURE_DICT,
  SSR_AUDIT_INTERVAL_DICT,
  SSR_PLATE_CODE_DICT,
  SSR_PANEL_TYPE_DICT,
  SSR_PLATE_SAMPLE_NUM_DICT,
  SSR_DNA_TEST_DEVICE_DICT,
  SSR_ELECT_TEST_DEVICE_DICT,
  SSR_PATERNITY_GENE_LIB_DICT,
  SSR_PATERNITY_MATCH_TYPE_DICT,
  SSR_PATERNITY_MATCH_TYPE_DICT,
  SSR_COMPARE_HISTORY_STATUS_DICT,
  SSR_COMPARE_TYPE_DICT,
  SSR_DNA_DEVICE_DICT,
  SSR_GEL_STANDARS_DICT,
  SSR_GEL_DIRECTION_DICT,
  SSR_AUDIT_HISTORY_STATUS_DICT,
  SSR_CACHE_TRACE_CODES_DICT,
  SSR_IS_PASS_DICT,
];
export const SSR_DICT_MODULE = 'SSR';
