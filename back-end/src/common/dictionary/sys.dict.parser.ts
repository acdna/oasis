import { SysDictionary } from '../../sys/entity/sys.dictionary.entity';
import { SYS_DICT_GROUPS, SYS_DICT_MODULE } from './sys.dict.defined';
import { SSR_DICT_GROUPS, SSR_DICT_MODULE } from './ssr.dict.defined';
import { uuid } from '../utils/uuid.utils';

/**
 * 字典解析器
 * @author: jiangbin
 * @date: 2021-01-21 14:48:29
 **/
export const SysDictParser = {
  /**
   * 解析模块的所有字典
   * @param moduleName 模块名
   * @param dicts 字典列表
   * @return {SysDictionary[]}
   * @author: jiangbin
   * @date: 2021-01-21 14:47:09
   **/
  parser(moduleName: string, dicts: any): SysDictionary[] {
    if (!moduleName || dicts?.length == 0) return [];
    const records = new Array<SysDictionary>();
    const parents = new Array<SysDictionary>();
    dicts.forEach((dict: any, index: number) => {
      if (!dict || dict.length == 0) return;
      let parent;
      const keys = Object.keys(dict);
      const parentId = uuid();
      keys.forEach((key) => {
        const value = dict[key];
        if (!value.nameCn) {
          console.log('无字典项名==>' + key);
        }
        parent = value.group;
        //字典节点
        const record = {
          dicId: uuid(),
          dicName: value.nameCn,
          dicNameEn: value.nameEn ? value.nameEn : value.nameCn,
          dicValue: value.value,
          dicGroup: parent.group,
          dicParentId: parentId,
          dicSpecies: value.species,
          dicModule: moduleName,
          dicType: 'DATA',
          dicOrder: value.order,
          dicState: parent.enable || 'ON',
          dicParams: '',
          dicCreateDate: new Date(),
          dicUpdateDate: new Date(),
        };
        records.push(record);
      });

      //字典父节点
      parent &&
        parents.push({
          dicId: parentId,
          dicName: parent.nameCn,
          dicNameEn: parent.nameEn ? parent.nameEn : parent.nameCn,
          dicGroup: parent.group,
          dicParentId: '1',
          dicModule: moduleName,
          dicType: 'DIR',
          dicOrder: index + 1,
          dicState: parent.enable || 'ON',
          dicValue: '',
          dicParams: '',
          dicSpecies: '',
          dicCreateDate: new Date(),
          dicUpdateDate: new Date(),
        });
    });

    return parents.concat(records);
  },

  /**
   * 解析所有字典数据
   * @return {SysDictionary[]}
   * @author: jiangbin
   * @date: 2021-01-21 14:46:22
   **/
  parserAll(): SysDictionary[] {
    const sys: SysDictionary[] = SysDictParser.parser(SYS_DICT_MODULE, SYS_DICT_GROUPS);
    const ssr: SysDictionary[] = SysDictParser.parser(SSR_DICT_MODULE, SSR_DICT_GROUPS);
    return sys.concat(ssr);
  },
};
