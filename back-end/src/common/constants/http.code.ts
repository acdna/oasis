/**
 * HTTP请求代码枚举定义
 * @author: jiangbin
 * @date: 2021-02-07 09:11:41
 **/
export enum HTTP_CODE {
  /**
   * 服务器成功返回请求的数据
   * @author: jiangbin
   * @date: 2021-02-07 09:28:26
   **/
  SUCCESS = 200,
  /**
   * 新建或修改数据成功
   * @author: jiangbin
   * @date: 2021-02-07 09:28:43
   **/
  MODIFY_SUCCESS = 201,
  /**
   * 一个请求已经进入后台排队（异步任务）
   * @author: jiangbin
   * @date: 2021-02-07 09:28:51
   **/
  QUEUE_SUCCESS = 202,
  /**
   * 删除数据成功
   * @author: jiangbin
   * @date: 2021-02-07 09:29:37
   **/
  DELETE_SUCCESS = 204,
  /**
   * 发出的请求有错误，服务器没有进行新建或修改数据的操作
   * @author: jiangbin
   * @date: 2021-02-07 09:29:45
   **/
  MODIFY_FAIL = 400,
  /**
   * 用户没有权限（令牌、用户名、密码错误）
   * @author: jiangbin
   * @date: 2021-02-07 09:29:52
   **/
  AUTH_FAIL = 401,
  /**
   * 用户得到授权，但是功能访问是被禁止的
   * @author: jiangbin
   * @date: 2021-02-07 09:29:59
   **/
  PMS_FAIL = 403,
  /**
   * 发出的请求针对的是不存在的记录，服务器没有进行操作
   * @author: jiangbin
   * @date: 2021-02-07 09:30:07
   **/
  NO_RECORD = 404,
  /**
   * 请求的格式错误
   * @author: jiangbin
   * @date: 2021-02-07 09:30:14
   **/
  FORMAT_ERROR = 406,
  /**
   * 永久删除资源
   * @author: jiangbin
   * @date: 2021-02-07 09:30:24
   **/
  SHRED_SOURCE = 410,
  /**
   * 数据验证失败
   * @author: jiangbin
   * @date: 2021-02-07 09:30:30
   **/
  VALID_FAIL = 422,
  /**
   * 服务器发生错误，请检查服务器
   * @author: jiangbin
   * @date: 2021-02-07 09:30:38
   **/
  SERVER_ERROR = 500,
  /**
   * 网关错误
   * @author: jiangbin
   * @date: 2021-02-07 09:30:45
   **/
  BAD_GATEWAY = 502,
  /**
   * 服务不可用，服务器暂时过载或维护
   * @author: jiangbin
   * @date: 2021-02-07 09:30:53
   **/
  SERVICE_UNAVAILABLE = 503,
  /**
   * 网关超时
   * @author: jiangbin
   * @date: 2021-02-07 09:31:00
   **/
  GATEWAY_TIMEOUT = 504,
}

/**
 * HTTP工具类
 * @author: jiangbin
 * @date: 2021-02-09 09:46:02
 **/
export const HttpUtils = {
  /**
   * 添加HTTP请求代码
   * @param httpCode 请求结果代码
   * @param result 请求结果数据
   * @return 在请求结果数据中增加一个代码参数"code"
   * @author: jiangbin
   * @date: 2021-02-07 09:25:03
   **/
  result: (httpCode: HTTP_CODE, result?: any | Partial<any>) => {
    return { code: httpCode, ...result };
  },
};
