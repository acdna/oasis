#!/bin/bash

TIME_STR=`date +"%Y%m%d-%H%M%S"`

DATABASE=scdms

ROOT_DIR=../../../
cd $ROOT_DIR
ROOT_DIR=`pwd`

BACKUP_DIR=$ROOT_DIR/sql/backup

echo "SQL备份目录==>$BACKUP_DIR"

BACKUP_FILE=$BACKUP_DIR/$DATABASE-db-$TIME_STR.sql

CONTAINER_NAME=$DATABASE-mysql-server

docker exec -it $CONTAINER_NAME mysqldump -uroot -proot $DATABASE > $BACKUP_FILE

