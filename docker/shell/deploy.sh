#!/bin/bash

echo "本脚本用于创建前后端代码的链接!"
SHELL_DIR=`pwd`

#获取整个项目的根目录
cd ../../
CURR_ROOT_DIR=`pwd`
echo "当前新版本代码的根目录路径==>$CURR_ROOT_DIR"

#程序等文件将通过链接方式部署到这个目录里
cd ../
ROOT_DIR=`pwd`
echo "部署代码的根目录路径==>$ROOT_DIR"

BACKUP_DIR=$ROOT_DIR/sql/backup
if [ ! -f "$BACKUP_DIR" ]; then
  mkdir -pv $BACKUP_DIR
  echo "创建数据库SQL备份目录==>$BACKUP_DIR"
fi

MYSQL_DATA=$ROOT_DIR/mysql/data
if [ ! -f "$MYSQL_DATA" ]; then
  mkdir -pv $MYSQL_DATA
  echo "创建MYSQL数据库表文件存储目录==>$MYSQL_DATA"
fi

#修改为可执行脚本
cd $SHELL_DIR
chmod +x *.sh

echo "开始备份数据库。。。"
./backup-db.sh
echo "备份数据库成功!"

echo "开始停止docker-compose相关容器实例..."
cd $ROOT_DIR
docker-compose stop
echo "停止docker-compose相关容器实例成功！"

cd $CURR_ROOT_DIR
if [ ! -f "back-end" ]; then
  echo "开始解压后端的程序文件..."
  tar -zxf back-end.tar.gz
  echo "解压后端的程序文件成功！"
fi

if [ ! -f "front-end" ]; then
  echo "开始解压前端的程序文件..."
  tar -zxf front-end.tar.gz
  echo "解压前端的程序文件成功！"
fi

echo "开始创建前后端程序文件和配置文件的链接..."
cd $CURR_ROOT_DIR
rm -rfv $ROOT_DIR/front-end
rm -rfv $ROOT_DIR/back-end
rm -rfv $ROOT_DIR/docker
rm -rfv $ROOT_DIR/sql/init
rm -rfv $ROOT_DIR/.env
rm -rfv $ROOT_DIR/docker-compose.yml
rm -rfv $ROOT_DIR/README.md
ln -s $CURR_ROOT_DIR/front-end $ROOT_DIR/front-end
ln -s $CURR_ROOT_DIR/back-end $ROOT_DIR/back-end
ln -s $CURR_ROOT_DIR/docker $ROOT_DIR/docker
ln -s $CURR_ROOT_DIR/sql/init $ROOT_DIR/sql/init
\cp -fv $CURR_ROOT_DIR/.env $ROOT_DIR/.env
ln -s $CURR_ROOT_DIR/docker-compose.yml $ROOT_DIR/docker-compose.yml
ln -s $CURR_ROOT_DIR/README.md $ROOT_DIR/README.md
echo "创建前后端程序文件和配置文件的链接成功!"

echo "相关docker-compose命令如下："
echo "初始化容器：docker-compose up -d"
echo "停止容器：docker-compose stop"
echo "启动容器：docker-compose start"
echo "重启容器：docker-compose restart"
echo "移除容器：docker-compose rm"

