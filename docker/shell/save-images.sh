#!/bin/bash

## 备份镜像
docker save mysql:5.7.7 | gzip > ../images/image-mysql-5.7.7
docker save node:14.15.4 | gzip > ../images/image-node-14.15.4
docker save nginx:latest | gzip > ../images/image-nginx